<?php

use App\Models\oc_coupon_history;
use App\Repositories\oc_coupon_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_historyRepositoryTest extends TestCase
{
    use Makeoc_coupon_historyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_coupon_historyRepository
     */
    protected $ocCouponHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCouponHistoryRepo = App::make(oc_coupon_historyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_coupon_history()
    {
        $ocCouponHistory = $this->fakeoc_coupon_historyData();
        $createdoc_coupon_history = $this->ocCouponHistoryRepo->create($ocCouponHistory);
        $createdoc_coupon_history = $createdoc_coupon_history->toArray();
        $this->assertArrayHasKey('id', $createdoc_coupon_history);
        $this->assertNotNull($createdoc_coupon_history['id'], 'Created oc_coupon_history must have id specified');
        $this->assertNotNull(oc_coupon_history::find($createdoc_coupon_history['id']), 'oc_coupon_history with given id must be in DB');
        $this->assertModelData($ocCouponHistory, $createdoc_coupon_history);
    }

    /**
     * @test read
     */
    public function testReadoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $dboc_coupon_history = $this->ocCouponHistoryRepo->find($ocCouponHistory->id);
        $dboc_coupon_history = $dboc_coupon_history->toArray();
        $this->assertModelData($ocCouponHistory->toArray(), $dboc_coupon_history);
    }

    /**
     * @test update
     */
    public function testUpdateoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $fakeoc_coupon_history = $this->fakeoc_coupon_historyData();
        $updatedoc_coupon_history = $this->ocCouponHistoryRepo->update($fakeoc_coupon_history, $ocCouponHistory->id);
        $this->assertModelData($fakeoc_coupon_history, $updatedoc_coupon_history->toArray());
        $dboc_coupon_history = $this->ocCouponHistoryRepo->find($ocCouponHistory->id);
        $this->assertModelData($fakeoc_coupon_history, $dboc_coupon_history->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $resp = $this->ocCouponHistoryRepo->delete($ocCouponHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_coupon_history::find($ocCouponHistory->id), 'oc_coupon_history should not exist in DB');
    }
}
