<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_productApiTest extends TestCase
{
    use Makeoc_coupon_productTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_coupon_product()
    {
        $ocCouponProduct = $this->fakeoc_coupon_productData();
        $this->json('POST', '/api/v1/ocCouponProducts', $ocCouponProduct);

        $this->assertApiResponse($ocCouponProduct);
    }

    /**
     * @test
     */
    public function testReadoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $this->json('GET', '/api/v1/ocCouponProducts/'.$ocCouponProduct->id);

        $this->assertApiResponse($ocCouponProduct->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $editedoc_coupon_product = $this->fakeoc_coupon_productData();

        $this->json('PUT', '/api/v1/ocCouponProducts/'.$ocCouponProduct->id, $editedoc_coupon_product);

        $this->assertApiResponse($editedoc_coupon_product);
    }

    /**
     * @test
     */
    public function testDeleteoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $this->json('DELETE', '/api/v1/ocCouponProducts/'.$ocCouponProduct->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCouponProducts/'.$ocCouponProduct->id);

        $this->assertResponseStatus(404);
    }
}
