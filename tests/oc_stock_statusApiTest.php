<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_stock_statusApiTest extends TestCase
{
    use Makeoc_stock_statusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_stock_status()
    {
        $ocStockStatus = $this->fakeoc_stock_statusData();
        $this->json('POST', '/api/v1/ocStockStatuses', $ocStockStatus);

        $this->assertApiResponse($ocStockStatus);
    }

    /**
     * @test
     */
    public function testReadoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $this->json('GET', '/api/v1/ocStockStatuses/'.$ocStockStatus->id);

        $this->assertApiResponse($ocStockStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $editedoc_stock_status = $this->fakeoc_stock_statusData();

        $this->json('PUT', '/api/v1/ocStockStatuses/'.$ocStockStatus->id, $editedoc_stock_status);

        $this->assertApiResponse($editedoc_stock_status);
    }

    /**
     * @test
     */
    public function testDeleteoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $this->json('DELETE', '/api/v1/ocStockStatuses/'.$ocStockStatus->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocStockStatuses/'.$ocStockStatus->id);

        $this->assertResponseStatus(404);
    }
}
