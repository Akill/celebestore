<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customerApiTest extends TestCase
{
    use Makeoc_customerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer()
    {
        $ocCustomer = $this->fakeoc_customerData();
        $this->json('POST', '/api/v1/ocCustomers', $ocCustomer);

        $this->assertApiResponse($ocCustomer);
    }

    /**
     * @test
     */
    public function testReadoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $this->json('GET', '/api/v1/ocCustomers/'.$ocCustomer->id);

        $this->assertApiResponse($ocCustomer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $editedoc_customer = $this->fakeoc_customerData();

        $this->json('PUT', '/api/v1/ocCustomers/'.$ocCustomer->id, $editedoc_customer);

        $this->assertApiResponse($editedoc_customer);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $this->json('DELETE', '/api/v1/ocCustomers/'.$ocCustomer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomers/'.$ocCustomer->id);

        $this->assertResponseStatus(404);
    }
}
