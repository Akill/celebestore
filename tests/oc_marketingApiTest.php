<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_marketingApiTest extends TestCase
{
    use Makeoc_marketingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_marketing()
    {
        $ocMarketing = $this->fakeoc_marketingData();
        $this->json('POST', '/api/v1/ocMarketings', $ocMarketing);

        $this->assertApiResponse($ocMarketing);
    }

    /**
     * @test
     */
    public function testReadoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $this->json('GET', '/api/v1/ocMarketings/'.$ocMarketing->id);

        $this->assertApiResponse($ocMarketing->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $editedoc_marketing = $this->fakeoc_marketingData();

        $this->json('PUT', '/api/v1/ocMarketings/'.$ocMarketing->id, $editedoc_marketing);

        $this->assertApiResponse($editedoc_marketing);
    }

    /**
     * @test
     */
    public function testDeleteoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $this->json('DELETE', '/api/v1/ocMarketings/'.$ocMarketing->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocMarketings/'.$ocMarketing->id);

        $this->assertResponseStatus(404);
    }
}
