<?php

use App\Models\oc_download_description;
use App\Repositories\oc_download_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_download_descriptionRepositoryTest extends TestCase
{
    use Makeoc_download_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_download_descriptionRepository
     */
    protected $ocDownloadDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocDownloadDescriptionRepo = App::make(oc_download_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_download_description()
    {
        $ocDownloadDescription = $this->fakeoc_download_descriptionData();
        $createdoc_download_description = $this->ocDownloadDescriptionRepo->create($ocDownloadDescription);
        $createdoc_download_description = $createdoc_download_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_download_description);
        $this->assertNotNull($createdoc_download_description['id'], 'Created oc_download_description must have id specified');
        $this->assertNotNull(oc_download_description::find($createdoc_download_description['id']), 'oc_download_description with given id must be in DB');
        $this->assertModelData($ocDownloadDescription, $createdoc_download_description);
    }

    /**
     * @test read
     */
    public function testReadoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $dboc_download_description = $this->ocDownloadDescriptionRepo->find($ocDownloadDescription->id);
        $dboc_download_description = $dboc_download_description->toArray();
        $this->assertModelData($ocDownloadDescription->toArray(), $dboc_download_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $fakeoc_download_description = $this->fakeoc_download_descriptionData();
        $updatedoc_download_description = $this->ocDownloadDescriptionRepo->update($fakeoc_download_description, $ocDownloadDescription->id);
        $this->assertModelData($fakeoc_download_description, $updatedoc_download_description->toArray());
        $dboc_download_description = $this->ocDownloadDescriptionRepo->find($ocDownloadDescription->id);
        $this->assertModelData($fakeoc_download_description, $dboc_download_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $resp = $this->ocDownloadDescriptionRepo->delete($ocDownloadDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_download_description::find($ocDownloadDescription->id), 'oc_download_description should not exist in DB');
    }
}
