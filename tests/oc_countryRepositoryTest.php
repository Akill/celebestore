<?php

use App\Models\oc_country;
use App\Repositories\oc_countryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_countryRepositoryTest extends TestCase
{
    use Makeoc_countryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_countryRepository
     */
    protected $ocCountryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCountryRepo = App::make(oc_countryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_country()
    {
        $ocCountry = $this->fakeoc_countryData();
        $createdoc_country = $this->ocCountryRepo->create($ocCountry);
        $createdoc_country = $createdoc_country->toArray();
        $this->assertArrayHasKey('id', $createdoc_country);
        $this->assertNotNull($createdoc_country['id'], 'Created oc_country must have id specified');
        $this->assertNotNull(oc_country::find($createdoc_country['id']), 'oc_country with given id must be in DB');
        $this->assertModelData($ocCountry, $createdoc_country);
    }

    /**
     * @test read
     */
    public function testReadoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $dboc_country = $this->ocCountryRepo->find($ocCountry->id);
        $dboc_country = $dboc_country->toArray();
        $this->assertModelData($ocCountry->toArray(), $dboc_country);
    }

    /**
     * @test update
     */
    public function testUpdateoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $fakeoc_country = $this->fakeoc_countryData();
        $updatedoc_country = $this->ocCountryRepo->update($fakeoc_country, $ocCountry->id);
        $this->assertModelData($fakeoc_country, $updatedoc_country->toArray());
        $dboc_country = $this->ocCountryRepo->find($ocCountry->id);
        $this->assertModelData($fakeoc_country, $dboc_country->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $resp = $this->ocCountryRepo->delete($ocCountry->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_country::find($ocCountry->id), 'oc_country should not exist in DB');
    }
}
