<?php

use App\Models\oc_tax_class;
use App\Repositories\oc_tax_classRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_classRepositoryTest extends TestCase
{
    use Makeoc_tax_classTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_tax_classRepository
     */
    protected $ocTaxClassRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocTaxClassRepo = App::make(oc_tax_classRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_tax_class()
    {
        $ocTaxClass = $this->fakeoc_tax_classData();
        $createdoc_tax_class = $this->ocTaxClassRepo->create($ocTaxClass);
        $createdoc_tax_class = $createdoc_tax_class->toArray();
        $this->assertArrayHasKey('id', $createdoc_tax_class);
        $this->assertNotNull($createdoc_tax_class['id'], 'Created oc_tax_class must have id specified');
        $this->assertNotNull(oc_tax_class::find($createdoc_tax_class['id']), 'oc_tax_class with given id must be in DB');
        $this->assertModelData($ocTaxClass, $createdoc_tax_class);
    }

    /**
     * @test read
     */
    public function testReadoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $dboc_tax_class = $this->ocTaxClassRepo->find($ocTaxClass->id);
        $dboc_tax_class = $dboc_tax_class->toArray();
        $this->assertModelData($ocTaxClass->toArray(), $dboc_tax_class);
    }

    /**
     * @test update
     */
    public function testUpdateoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $fakeoc_tax_class = $this->fakeoc_tax_classData();
        $updatedoc_tax_class = $this->ocTaxClassRepo->update($fakeoc_tax_class, $ocTaxClass->id);
        $this->assertModelData($fakeoc_tax_class, $updatedoc_tax_class->toArray());
        $dboc_tax_class = $this->ocTaxClassRepo->find($ocTaxClass->id);
        $this->assertModelData($fakeoc_tax_class, $dboc_tax_class->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $resp = $this->ocTaxClassRepo->delete($ocTaxClass->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_tax_class::find($ocTaxClass->id), 'oc_tax_class should not exist in DB');
    }
}
