<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_historyApiTest extends TestCase
{
    use Makeoc_voucher_historyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_voucher_history()
    {
        $ocVoucherHistory = $this->fakeoc_voucher_historyData();
        $this->json('POST', '/api/v1/ocVoucherHistories', $ocVoucherHistory);

        $this->assertApiResponse($ocVoucherHistory);
    }

    /**
     * @test
     */
    public function testReadoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $this->json('GET', '/api/v1/ocVoucherHistories/'.$ocVoucherHistory->id);

        $this->assertApiResponse($ocVoucherHistory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $editedoc_voucher_history = $this->fakeoc_voucher_historyData();

        $this->json('PUT', '/api/v1/ocVoucherHistories/'.$ocVoucherHistory->id, $editedoc_voucher_history);

        $this->assertApiResponse($editedoc_voucher_history);
    }

    /**
     * @test
     */
    public function testDeleteoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $this->json('DELETE', '/api/v1/ocVoucherHistories/'.$ocVoucherHistory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocVoucherHistories/'.$ocVoucherHistory->id);

        $this->assertResponseStatus(404);
    }
}
