<?php

use App\Models\oc_product_image;
use App\Repositories\oc_product_imageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_imageRepositoryTest extends TestCase
{
    use Makeoc_product_imageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_imageRepository
     */
    protected $ocProductImageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductImageRepo = App::make(oc_product_imageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_image()
    {
        $ocProductImage = $this->fakeoc_product_imageData();
        $createdoc_product_image = $this->ocProductImageRepo->create($ocProductImage);
        $createdoc_product_image = $createdoc_product_image->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_image);
        $this->assertNotNull($createdoc_product_image['id'], 'Created oc_product_image must have id specified');
        $this->assertNotNull(oc_product_image::find($createdoc_product_image['id']), 'oc_product_image with given id must be in DB');
        $this->assertModelData($ocProductImage, $createdoc_product_image);
    }

    /**
     * @test read
     */
    public function testReadoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $dboc_product_image = $this->ocProductImageRepo->find($ocProductImage->id);
        $dboc_product_image = $dboc_product_image->toArray();
        $this->assertModelData($ocProductImage->toArray(), $dboc_product_image);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $fakeoc_product_image = $this->fakeoc_product_imageData();
        $updatedoc_product_image = $this->ocProductImageRepo->update($fakeoc_product_image, $ocProductImage->id);
        $this->assertModelData($fakeoc_product_image, $updatedoc_product_image->toArray());
        $dboc_product_image = $this->ocProductImageRepo->find($ocProductImage->id);
        $this->assertModelData($fakeoc_product_image, $dboc_product_image->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $resp = $this->ocProductImageRepo->delete($ocProductImage->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_image::find($ocProductImage->id), 'oc_product_image should not exist in DB');
    }
}
