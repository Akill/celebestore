<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_group_descriptionApiTest extends TestCase
{
    use Makeoc_attribute_group_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->fakeoc_attribute_group_descriptionData();
        $this->json('POST', '/api/v1/ocAttributeGroupDescriptions', $ocAttributeGroupDescription);

        $this->assertApiResponse($ocAttributeGroupDescription);
    }

    /**
     * @test
     */
    public function testReadoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $this->json('GET', '/api/v1/ocAttributeGroupDescriptions/'.$ocAttributeGroupDescription->id);

        $this->assertApiResponse($ocAttributeGroupDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $editedoc_attribute_group_description = $this->fakeoc_attribute_group_descriptionData();

        $this->json('PUT', '/api/v1/ocAttributeGroupDescriptions/'.$ocAttributeGroupDescription->id, $editedoc_attribute_group_description);

        $this->assertApiResponse($editedoc_attribute_group_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $this->json('DELETE', '/api/v1/ocAttributeGroupDescriptions/'.$ocAttributeGroupDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAttributeGroupDescriptions/'.$ocAttributeGroupDescription->id);

        $this->assertResponseStatus(404);
    }
}
