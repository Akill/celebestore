<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_optionApiTest extends TestCase
{
    use Makeoc_order_optionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_option()
    {
        $ocOrderOption = $this->fakeoc_order_optionData();
        $this->json('POST', '/api/v1/ocOrderOptions', $ocOrderOption);

        $this->assertApiResponse($ocOrderOption);
    }

    /**
     * @test
     */
    public function testReadoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $this->json('GET', '/api/v1/ocOrderOptions/'.$ocOrderOption->id);

        $this->assertApiResponse($ocOrderOption->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $editedoc_order_option = $this->fakeoc_order_optionData();

        $this->json('PUT', '/api/v1/ocOrderOptions/'.$ocOrderOption->id, $editedoc_order_option);

        $this->assertApiResponse($editedoc_order_option);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $this->json('DELETE', '/api/v1/ocOrderOptions/'.$ocOrderOption->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderOptions/'.$ocOrderOption->id);

        $this->assertResponseStatus(404);
    }
}
