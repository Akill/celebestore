<?php

use App\Models\oc_return_reason;
use App\Repositories\oc_return_reasonRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_reasonRepositoryTest extends TestCase
{
    use Makeoc_return_reasonTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_return_reasonRepository
     */
    protected $ocReturnReasonRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReturnReasonRepo = App::make(oc_return_reasonRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_return_reason()
    {
        $ocReturnReason = $this->fakeoc_return_reasonData();
        $createdoc_return_reason = $this->ocReturnReasonRepo->create($ocReturnReason);
        $createdoc_return_reason = $createdoc_return_reason->toArray();
        $this->assertArrayHasKey('id', $createdoc_return_reason);
        $this->assertNotNull($createdoc_return_reason['id'], 'Created oc_return_reason must have id specified');
        $this->assertNotNull(oc_return_reason::find($createdoc_return_reason['id']), 'oc_return_reason with given id must be in DB');
        $this->assertModelData($ocReturnReason, $createdoc_return_reason);
    }

    /**
     * @test read
     */
    public function testReadoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $dboc_return_reason = $this->ocReturnReasonRepo->find($ocReturnReason->id);
        $dboc_return_reason = $dboc_return_reason->toArray();
        $this->assertModelData($ocReturnReason->toArray(), $dboc_return_reason);
    }

    /**
     * @test update
     */
    public function testUpdateoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $fakeoc_return_reason = $this->fakeoc_return_reasonData();
        $updatedoc_return_reason = $this->ocReturnReasonRepo->update($fakeoc_return_reason, $ocReturnReason->id);
        $this->assertModelData($fakeoc_return_reason, $updatedoc_return_reason->toArray());
        $dboc_return_reason = $this->ocReturnReasonRepo->find($ocReturnReason->id);
        $this->assertModelData($fakeoc_return_reason, $dboc_return_reason->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $resp = $this->ocReturnReasonRepo->delete($ocReturnReason->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_return_reason::find($ocReturnReason->id), 'oc_return_reason should not exist in DB');
    }
}
