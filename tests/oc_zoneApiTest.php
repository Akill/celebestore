<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_zoneApiTest extends TestCase
{
    use Makeoc_zoneTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_zone()
    {
        $ocZone = $this->fakeoc_zoneData();
        $this->json('POST', '/api/v1/ocZones', $ocZone);

        $this->assertApiResponse($ocZone);
    }

    /**
     * @test
     */
    public function testReadoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $this->json('GET', '/api/v1/ocZones/'.$ocZone->id);

        $this->assertApiResponse($ocZone->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $editedoc_zone = $this->fakeoc_zoneData();

        $this->json('PUT', '/api/v1/ocZones/'.$ocZone->id, $editedoc_zone);

        $this->assertApiResponse($editedoc_zone);
    }

    /**
     * @test
     */
    public function testDeleteoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $this->json('DELETE', '/api/v1/ocZones/'.$ocZone->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocZones/'.$ocZone->id);

        $this->assertResponseStatus(404);
    }
}
