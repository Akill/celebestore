<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_productApiTest extends TestCase
{
    use Makeoc_productTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product()
    {
        $ocProduct = $this->fakeoc_productData();
        $this->json('POST', '/api/v1/ocProducts', $ocProduct);

        $this->assertApiResponse($ocProduct);
    }

    /**
     * @test
     */
    public function testReadoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $this->json('GET', '/api/v1/ocProducts/'.$ocProduct->id);

        $this->assertApiResponse($ocProduct->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $editedoc_product = $this->fakeoc_productData();

        $this->json('PUT', '/api/v1/ocProducts/'.$ocProduct->id, $editedoc_product);

        $this->assertApiResponse($editedoc_product);
    }

    /**
     * @test
     */
    public function testDeleteoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $this->json('DELETE', '/api/v1/ocProducts/'.$ocProduct->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProducts/'.$ocProduct->id);

        $this->assertResponseStatus(404);
    }
}
