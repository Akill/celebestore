<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_totalApiTest extends TestCase
{
    use Makeoc_order_totalTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_total()
    {
        $ocOrderTotal = $this->fakeoc_order_totalData();
        $this->json('POST', '/api/v1/ocOrderTotals', $ocOrderTotal);

        $this->assertApiResponse($ocOrderTotal);
    }

    /**
     * @test
     */
    public function testReadoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $this->json('GET', '/api/v1/ocOrderTotals/'.$ocOrderTotal->id);

        $this->assertApiResponse($ocOrderTotal->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $editedoc_order_total = $this->fakeoc_order_totalData();

        $this->json('PUT', '/api/v1/ocOrderTotals/'.$ocOrderTotal->id, $editedoc_order_total);

        $this->assertApiResponse($editedoc_order_total);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $this->json('DELETE', '/api/v1/ocOrderTotals/'.$ocOrderTotal->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderTotals/'.$ocOrderTotal->id);

        $this->assertResponseStatus(404);
    }
}
