<?php

use App\Models\oc_user;
use App\Repositories\oc_userRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_userRepositoryTest extends TestCase
{
    use Makeoc_userTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_userRepository
     */
    protected $ocUserRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocUserRepo = App::make(oc_userRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_user()
    {
        $ocUser = $this->fakeoc_userData();
        $createdoc_user = $this->ocUserRepo->create($ocUser);
        $createdoc_user = $createdoc_user->toArray();
        $this->assertArrayHasKey('id', $createdoc_user);
        $this->assertNotNull($createdoc_user['id'], 'Created oc_user must have id specified');
        $this->assertNotNull(oc_user::find($createdoc_user['id']), 'oc_user with given id must be in DB');
        $this->assertModelData($ocUser, $createdoc_user);
    }

    /**
     * @test read
     */
    public function testReadoc_user()
    {
        $ocUser = $this->makeoc_user();
        $dboc_user = $this->ocUserRepo->find($ocUser->id);
        $dboc_user = $dboc_user->toArray();
        $this->assertModelData($ocUser->toArray(), $dboc_user);
    }

    /**
     * @test update
     */
    public function testUpdateoc_user()
    {
        $ocUser = $this->makeoc_user();
        $fakeoc_user = $this->fakeoc_userData();
        $updatedoc_user = $this->ocUserRepo->update($fakeoc_user, $ocUser->id);
        $this->assertModelData($fakeoc_user, $updatedoc_user->toArray());
        $dboc_user = $this->ocUserRepo->find($ocUser->id);
        $this->assertModelData($fakeoc_user, $dboc_user->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_user()
    {
        $ocUser = $this->makeoc_user();
        $resp = $this->ocUserRepo->delete($ocUser->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_user::find($ocUser->id), 'oc_user should not exist in DB');
    }
}
