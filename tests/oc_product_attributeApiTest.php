<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_attributeApiTest extends TestCase
{
    use Makeoc_product_attributeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_attribute()
    {
        $ocProductAttribute = $this->fakeoc_product_attributeData();
        $this->json('POST', '/api/v1/ocProductAttributes', $ocProductAttribute);

        $this->assertApiResponse($ocProductAttribute);
    }

    /**
     * @test
     */
    public function testReadoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $this->json('GET', '/api/v1/ocProductAttributes/'.$ocProductAttribute->id);

        $this->assertApiResponse($ocProductAttribute->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $editedoc_product_attribute = $this->fakeoc_product_attributeData();

        $this->json('PUT', '/api/v1/ocProductAttributes/'.$ocProductAttribute->id, $editedoc_product_attribute);

        $this->assertApiResponse($editedoc_product_attribute);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $this->json('DELETE', '/api/v1/ocProductAttributes/'.$ocProductAttribute->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductAttributes/'.$ocProductAttribute->id);

        $this->assertResponseStatus(404);
    }
}
