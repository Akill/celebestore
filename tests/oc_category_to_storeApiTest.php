<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_to_storeApiTest extends TestCase
{
    use Makeoc_category_to_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category_to_store()
    {
        $ocCategoryToStore = $this->fakeoc_category_to_storeData();
        $this->json('POST', '/api/v1/ocCategoryToStores', $ocCategoryToStore);

        $this->assertApiResponse($ocCategoryToStore);
    }

    /**
     * @test
     */
    public function testReadoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $this->json('GET', '/api/v1/ocCategoryToStores/'.$ocCategoryToStore->id);

        $this->assertApiResponse($ocCategoryToStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $editedoc_category_to_store = $this->fakeoc_category_to_storeData();

        $this->json('PUT', '/api/v1/ocCategoryToStores/'.$ocCategoryToStore->id, $editedoc_category_to_store);

        $this->assertApiResponse($editedoc_category_to_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $this->json('DELETE', '/api/v1/ocCategoryToStores/'.$ocCategoryToStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategoryToStores/'.$ocCategoryToStore->id);

        $this->assertResponseStatus(404);
    }
}
