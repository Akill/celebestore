<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_manufacturerApiTest extends TestCase
{
    use Makeoc_manufacturerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_manufacturer()
    {
        $ocManufacturer = $this->fakeoc_manufacturerData();
        $this->json('POST', '/api/v1/ocManufacturers', $ocManufacturer);

        $this->assertApiResponse($ocManufacturer);
    }

    /**
     * @test
     */
    public function testReadoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $this->json('GET', '/api/v1/ocManufacturers/'.$ocManufacturer->id);

        $this->assertApiResponse($ocManufacturer->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $editedoc_manufacturer = $this->fakeoc_manufacturerData();

        $this->json('PUT', '/api/v1/ocManufacturers/'.$ocManufacturer->id, $editedoc_manufacturer);

        $this->assertApiResponse($editedoc_manufacturer);
    }

    /**
     * @test
     */
    public function testDeleteoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $this->json('DELETE', '/api/v1/ocManufacturers/'.$ocManufacturer->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocManufacturers/'.$ocManufacturer->id);

        $this->assertResponseStatus(404);
    }
}
