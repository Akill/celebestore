<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_rewardApiTest extends TestCase
{
    use Makeoc_product_rewardTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_reward()
    {
        $ocProductReward = $this->fakeoc_product_rewardData();
        $this->json('POST', '/api/v1/ocProductRewards', $ocProductReward);

        $this->assertApiResponse($ocProductReward);
    }

    /**
     * @test
     */
    public function testReadoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $this->json('GET', '/api/v1/ocProductRewards/'.$ocProductReward->id);

        $this->assertApiResponse($ocProductReward->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $editedoc_product_reward = $this->fakeoc_product_rewardData();

        $this->json('PUT', '/api/v1/ocProductRewards/'.$ocProductReward->id, $editedoc_product_reward);

        $this->assertApiResponse($editedoc_product_reward);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $this->json('DELETE', '/api/v1/ocProductRewards/'.$ocProductReward->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductRewards/'.$ocProductReward->id);

        $this->assertResponseStatus(404);
    }
}
