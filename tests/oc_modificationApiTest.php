<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_modificationApiTest extends TestCase
{
    use Makeoc_modificationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_modification()
    {
        $ocModification = $this->fakeoc_modificationData();
        $this->json('POST', '/api/v1/ocModifications', $ocModification);

        $this->assertApiResponse($ocModification);
    }

    /**
     * @test
     */
    public function testReadoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $this->json('GET', '/api/v1/ocModifications/'.$ocModification->id);

        $this->assertApiResponse($ocModification->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $editedoc_modification = $this->fakeoc_modificationData();

        $this->json('PUT', '/api/v1/ocModifications/'.$ocModification->id, $editedoc_modification);

        $this->assertApiResponse($editedoc_modification);
    }

    /**
     * @test
     */
    public function testDeleteoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $this->json('DELETE', '/api/v1/ocModifications/'.$ocModification->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocModifications/'.$ocModification->id);

        $this->assertResponseStatus(404);
    }
}
