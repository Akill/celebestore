<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_loginApiTest extends TestCase
{
    use Makeoc_customer_loginTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_login()
    {
        $ocCustomerLogin = $this->fakeoc_customer_loginData();
        $this->json('POST', '/api/v1/ocCustomerLogins', $ocCustomerLogin);

        $this->assertApiResponse($ocCustomerLogin);
    }

    /**
     * @test
     */
    public function testReadoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $this->json('GET', '/api/v1/ocCustomerLogins/'.$ocCustomerLogin->id);

        $this->assertApiResponse($ocCustomerLogin->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $editedoc_customer_login = $this->fakeoc_customer_loginData();

        $this->json('PUT', '/api/v1/ocCustomerLogins/'.$ocCustomerLogin->id, $editedoc_customer_login);

        $this->assertApiResponse($editedoc_customer_login);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $this->json('DELETE', '/api/v1/ocCustomerLogins/'.$ocCustomerLogin->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerLogins/'.$ocCustomerLogin->id);

        $this->assertResponseStatus(404);
    }
}
