<?php

use App\Models\oc_banner_image;
use App\Repositories\oc_banner_imageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_banner_imageRepositoryTest extends TestCase
{
    use Makeoc_banner_imageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_banner_imageRepository
     */
    protected $ocBannerImageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocBannerImageRepo = App::make(oc_banner_imageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_banner_image()
    {
        $ocBannerImage = $this->fakeoc_banner_imageData();
        $createdoc_banner_image = $this->ocBannerImageRepo->create($ocBannerImage);
        $createdoc_banner_image = $createdoc_banner_image->toArray();
        $this->assertArrayHasKey('id', $createdoc_banner_image);
        $this->assertNotNull($createdoc_banner_image['id'], 'Created oc_banner_image must have id specified');
        $this->assertNotNull(oc_banner_image::find($createdoc_banner_image['id']), 'oc_banner_image with given id must be in DB');
        $this->assertModelData($ocBannerImage, $createdoc_banner_image);
    }

    /**
     * @test read
     */
    public function testReadoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $dboc_banner_image = $this->ocBannerImageRepo->find($ocBannerImage->id);
        $dboc_banner_image = $dboc_banner_image->toArray();
        $this->assertModelData($ocBannerImage->toArray(), $dboc_banner_image);
    }

    /**
     * @test update
     */
    public function testUpdateoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $fakeoc_banner_image = $this->fakeoc_banner_imageData();
        $updatedoc_banner_image = $this->ocBannerImageRepo->update($fakeoc_banner_image, $ocBannerImage->id);
        $this->assertModelData($fakeoc_banner_image, $updatedoc_banner_image->toArray());
        $dboc_banner_image = $this->ocBannerImageRepo->find($ocBannerImage->id);
        $this->assertModelData($fakeoc_banner_image, $dboc_banner_image->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $resp = $this->ocBannerImageRepo->delete($ocBannerImage->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_banner_image::find($ocBannerImage->id), 'oc_banner_image should not exist in DB');
    }
}
