<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_descriptionApiTest extends TestCase
{
    use Makeoc_custom_field_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->fakeoc_custom_field_descriptionData();
        $this->json('POST', '/api/v1/ocCustomFieldDescriptions', $ocCustomFieldDescription);

        $this->assertApiResponse($ocCustomFieldDescription);
    }

    /**
     * @test
     */
    public function testReadoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $this->json('GET', '/api/v1/ocCustomFieldDescriptions/'.$ocCustomFieldDescription->id);

        $this->assertApiResponse($ocCustomFieldDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $editedoc_custom_field_description = $this->fakeoc_custom_field_descriptionData();

        $this->json('PUT', '/api/v1/ocCustomFieldDescriptions/'.$ocCustomFieldDescription->id, $editedoc_custom_field_description);

        $this->assertApiResponse($editedoc_custom_field_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $this->json('DELETE', '/api/v1/ocCustomFieldDescriptions/'.$ocCustomFieldDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomFieldDescriptions/'.$ocCustomFieldDescription->id);

        $this->assertResponseStatus(404);
    }
}
