<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_historyApiTest extends TestCase
{
    use Makeoc_order_historyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_history()
    {
        $ocOrderHistory = $this->fakeoc_order_historyData();
        $this->json('POST', '/api/v1/ocOrderHistories', $ocOrderHistory);

        $this->assertApiResponse($ocOrderHistory);
    }

    /**
     * @test
     */
    public function testReadoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $this->json('GET', '/api/v1/ocOrderHistories/'.$ocOrderHistory->id);

        $this->assertApiResponse($ocOrderHistory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $editedoc_order_history = $this->fakeoc_order_historyData();

        $this->json('PUT', '/api/v1/ocOrderHistories/'.$ocOrderHistory->id, $editedoc_order_history);

        $this->assertApiResponse($editedoc_order_history);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $this->json('DELETE', '/api/v1/ocOrderHistories/'.$ocOrderHistory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderHistories/'.$ocOrderHistory->id);

        $this->assertResponseStatus(404);
    }
}
