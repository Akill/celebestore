<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_reviewApiTest extends TestCase
{
    use Makeoc_reviewTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_review()
    {
        $ocReview = $this->fakeoc_reviewData();
        $this->json('POST', '/api/v1/ocReviews', $ocReview);

        $this->assertApiResponse($ocReview);
    }

    /**
     * @test
     */
    public function testReadoc_review()
    {
        $ocReview = $this->makeoc_review();
        $this->json('GET', '/api/v1/ocReviews/'.$ocReview->id);

        $this->assertApiResponse($ocReview->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_review()
    {
        $ocReview = $this->makeoc_review();
        $editedoc_review = $this->fakeoc_reviewData();

        $this->json('PUT', '/api/v1/ocReviews/'.$ocReview->id, $editedoc_review);

        $this->assertApiResponse($editedoc_review);
    }

    /**
     * @test
     */
    public function testDeleteoc_review()
    {
        $ocReview = $this->makeoc_review();
        $this->json('DELETE', '/api/v1/ocReviews/'.$ocReview->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReviews/'.$ocReview->id);

        $this->assertResponseStatus(404);
    }
}
