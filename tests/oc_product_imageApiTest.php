<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_imageApiTest extends TestCase
{
    use Makeoc_product_imageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_image()
    {
        $ocProductImage = $this->fakeoc_product_imageData();
        $this->json('POST', '/api/v1/ocProductImages', $ocProductImage);

        $this->assertApiResponse($ocProductImage);
    }

    /**
     * @test
     */
    public function testReadoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $this->json('GET', '/api/v1/ocProductImages/'.$ocProductImage->id);

        $this->assertApiResponse($ocProductImage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $editedoc_product_image = $this->fakeoc_product_imageData();

        $this->json('PUT', '/api/v1/ocProductImages/'.$ocProductImage->id, $editedoc_product_image);

        $this->assertApiResponse($editedoc_product_image);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_image()
    {
        $ocProductImage = $this->makeoc_product_image();
        $this->json('DELETE', '/api/v1/ocProductImages/'.$ocProductImage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductImages/'.$ocProductImage->id);

        $this->assertResponseStatus(404);
    }
}
