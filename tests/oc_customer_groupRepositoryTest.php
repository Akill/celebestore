<?php

use App\Models\oc_customer_group;
use App\Repositories\oc_customer_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_groupRepositoryTest extends TestCase
{
    use Makeoc_customer_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_groupRepository
     */
    protected $ocCustomerGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerGroupRepo = App::make(oc_customer_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_group()
    {
        $ocCustomerGroup = $this->fakeoc_customer_groupData();
        $createdoc_customer_group = $this->ocCustomerGroupRepo->create($ocCustomerGroup);
        $createdoc_customer_group = $createdoc_customer_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_group);
        $this->assertNotNull($createdoc_customer_group['id'], 'Created oc_customer_group must have id specified');
        $this->assertNotNull(oc_customer_group::find($createdoc_customer_group['id']), 'oc_customer_group with given id must be in DB');
        $this->assertModelData($ocCustomerGroup, $createdoc_customer_group);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $dboc_customer_group = $this->ocCustomerGroupRepo->find($ocCustomerGroup->id);
        $dboc_customer_group = $dboc_customer_group->toArray();
        $this->assertModelData($ocCustomerGroup->toArray(), $dboc_customer_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $fakeoc_customer_group = $this->fakeoc_customer_groupData();
        $updatedoc_customer_group = $this->ocCustomerGroupRepo->update($fakeoc_customer_group, $ocCustomerGroup->id);
        $this->assertModelData($fakeoc_customer_group, $updatedoc_customer_group->toArray());
        $dboc_customer_group = $this->ocCustomerGroupRepo->find($ocCustomerGroup->id);
        $this->assertModelData($fakeoc_customer_group, $dboc_customer_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $resp = $this->ocCustomerGroupRepo->delete($ocCustomerGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_group::find($ocCustomerGroup->id), 'oc_customer_group should not exist in DB');
    }
}
