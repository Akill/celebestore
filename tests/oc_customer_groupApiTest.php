<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_groupApiTest extends TestCase
{
    use Makeoc_customer_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_group()
    {
        $ocCustomerGroup = $this->fakeoc_customer_groupData();
        $this->json('POST', '/api/v1/ocCustomerGroups', $ocCustomerGroup);

        $this->assertApiResponse($ocCustomerGroup);
    }

    /**
     * @test
     */
    public function testReadoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $this->json('GET', '/api/v1/ocCustomerGroups/'.$ocCustomerGroup->id);

        $this->assertApiResponse($ocCustomerGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $editedoc_customer_group = $this->fakeoc_customer_groupData();

        $this->json('PUT', '/api/v1/ocCustomerGroups/'.$ocCustomerGroup->id, $editedoc_customer_group);

        $this->assertApiResponse($editedoc_customer_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_group()
    {
        $ocCustomerGroup = $this->makeoc_customer_group();
        $this->json('DELETE', '/api/v1/ocCustomerGroups/'.$ocCustomerGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerGroups/'.$ocCustomerGroup->id);

        $this->assertResponseStatus(404);
    }
}
