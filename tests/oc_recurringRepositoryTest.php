<?php

use App\Models\oc_recurring;
use App\Repositories\oc_recurringRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_recurringRepositoryTest extends TestCase
{
    use Makeoc_recurringTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_recurringRepository
     */
    protected $ocRecurringRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocRecurringRepo = App::make(oc_recurringRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_recurring()
    {
        $ocRecurring = $this->fakeoc_recurringData();
        $createdoc_recurring = $this->ocRecurringRepo->create($ocRecurring);
        $createdoc_recurring = $createdoc_recurring->toArray();
        $this->assertArrayHasKey('id', $createdoc_recurring);
        $this->assertNotNull($createdoc_recurring['id'], 'Created oc_recurring must have id specified');
        $this->assertNotNull(oc_recurring::find($createdoc_recurring['id']), 'oc_recurring with given id must be in DB');
        $this->assertModelData($ocRecurring, $createdoc_recurring);
    }

    /**
     * @test read
     */
    public function testReadoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $dboc_recurring = $this->ocRecurringRepo->find($ocRecurring->id);
        $dboc_recurring = $dboc_recurring->toArray();
        $this->assertModelData($ocRecurring->toArray(), $dboc_recurring);
    }

    /**
     * @test update
     */
    public function testUpdateoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $fakeoc_recurring = $this->fakeoc_recurringData();
        $updatedoc_recurring = $this->ocRecurringRepo->update($fakeoc_recurring, $ocRecurring->id);
        $this->assertModelData($fakeoc_recurring, $updatedoc_recurring->toArray());
        $dboc_recurring = $this->ocRecurringRepo->find($ocRecurring->id);
        $this->assertModelData($fakeoc_recurring, $dboc_recurring->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $resp = $this->ocRecurringRepo->delete($ocRecurring->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_recurring::find($ocRecurring->id), 'oc_recurring should not exist in DB');
    }
}
