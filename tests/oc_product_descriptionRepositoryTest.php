<?php

use App\Models\oc_product_description;
use App\Repositories\oc_product_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_descriptionRepositoryTest extends TestCase
{
    use Makeoc_product_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_descriptionRepository
     */
    protected $ocProductDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductDescriptionRepo = App::make(oc_product_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_description()
    {
        $ocProductDescription = $this->fakeoc_product_descriptionData();
        $createdoc_product_description = $this->ocProductDescriptionRepo->create($ocProductDescription);
        $createdoc_product_description = $createdoc_product_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_description);
        $this->assertNotNull($createdoc_product_description['id'], 'Created oc_product_description must have id specified');
        $this->assertNotNull(oc_product_description::find($createdoc_product_description['id']), 'oc_product_description with given id must be in DB');
        $this->assertModelData($ocProductDescription, $createdoc_product_description);
    }

    /**
     * @test read
     */
    public function testReadoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $dboc_product_description = $this->ocProductDescriptionRepo->find($ocProductDescription->id);
        $dboc_product_description = $dboc_product_description->toArray();
        $this->assertModelData($ocProductDescription->toArray(), $dboc_product_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $fakeoc_product_description = $this->fakeoc_product_descriptionData();
        $updatedoc_product_description = $this->ocProductDescriptionRepo->update($fakeoc_product_description, $ocProductDescription->id);
        $this->assertModelData($fakeoc_product_description, $updatedoc_product_description->toArray());
        $dboc_product_description = $this->ocProductDescriptionRepo->find($ocProductDescription->id);
        $this->assertModelData($fakeoc_product_description, $dboc_product_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $resp = $this->ocProductDescriptionRepo->delete($ocProductDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_description::find($ocProductDescription->id), 'oc_product_description should not exist in DB');
    }
}
