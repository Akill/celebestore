<?php

use App\Models\oc_weight_class;
use App\Repositories\oc_weight_classRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_weight_classRepositoryTest extends TestCase
{
    use Makeoc_weight_classTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_weight_classRepository
     */
    protected $ocWeightClassRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocWeightClassRepo = App::make(oc_weight_classRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_weight_class()
    {
        $ocWeightClass = $this->fakeoc_weight_classData();
        $createdoc_weight_class = $this->ocWeightClassRepo->create($ocWeightClass);
        $createdoc_weight_class = $createdoc_weight_class->toArray();
        $this->assertArrayHasKey('id', $createdoc_weight_class);
        $this->assertNotNull($createdoc_weight_class['id'], 'Created oc_weight_class must have id specified');
        $this->assertNotNull(oc_weight_class::find($createdoc_weight_class['id']), 'oc_weight_class with given id must be in DB');
        $this->assertModelData($ocWeightClass, $createdoc_weight_class);
    }

    /**
     * @test read
     */
    public function testReadoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $dboc_weight_class = $this->ocWeightClassRepo->find($ocWeightClass->id);
        $dboc_weight_class = $dboc_weight_class->toArray();
        $this->assertModelData($ocWeightClass->toArray(), $dboc_weight_class);
    }

    /**
     * @test update
     */
    public function testUpdateoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $fakeoc_weight_class = $this->fakeoc_weight_classData();
        $updatedoc_weight_class = $this->ocWeightClassRepo->update($fakeoc_weight_class, $ocWeightClass->id);
        $this->assertModelData($fakeoc_weight_class, $updatedoc_weight_class->toArray());
        $dboc_weight_class = $this->ocWeightClassRepo->find($ocWeightClass->id);
        $this->assertModelData($fakeoc_weight_class, $dboc_weight_class->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $resp = $this->ocWeightClassRepo->delete($ocWeightClass->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_weight_class::find($ocWeightClass->id), 'oc_weight_class should not exist in DB');
    }
}
