<?php

use App\Models\oc_zone;
use App\Repositories\oc_zoneRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_zoneRepositoryTest extends TestCase
{
    use Makeoc_zoneTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_zoneRepository
     */
    protected $ocZoneRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocZoneRepo = App::make(oc_zoneRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_zone()
    {
        $ocZone = $this->fakeoc_zoneData();
        $createdoc_zone = $this->ocZoneRepo->create($ocZone);
        $createdoc_zone = $createdoc_zone->toArray();
        $this->assertArrayHasKey('id', $createdoc_zone);
        $this->assertNotNull($createdoc_zone['id'], 'Created oc_zone must have id specified');
        $this->assertNotNull(oc_zone::find($createdoc_zone['id']), 'oc_zone with given id must be in DB');
        $this->assertModelData($ocZone, $createdoc_zone);
    }

    /**
     * @test read
     */
    public function testReadoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $dboc_zone = $this->ocZoneRepo->find($ocZone->id);
        $dboc_zone = $dboc_zone->toArray();
        $this->assertModelData($ocZone->toArray(), $dboc_zone);
    }

    /**
     * @test update
     */
    public function testUpdateoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $fakeoc_zone = $this->fakeoc_zoneData();
        $updatedoc_zone = $this->ocZoneRepo->update($fakeoc_zone, $ocZone->id);
        $this->assertModelData($fakeoc_zone, $updatedoc_zone->toArray());
        $dboc_zone = $this->ocZoneRepo->find($ocZone->id);
        $this->assertModelData($fakeoc_zone, $dboc_zone->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_zone()
    {
        $ocZone = $this->makeoc_zone();
        $resp = $this->ocZoneRepo->delete($ocZone->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_zone::find($ocZone->id), 'oc_zone should not exist in DB');
    }
}
