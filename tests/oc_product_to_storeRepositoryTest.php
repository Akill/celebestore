<?php

use App\Models\oc_product_to_store;
use App\Repositories\oc_product_to_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_storeRepositoryTest extends TestCase
{
    use Makeoc_product_to_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_to_storeRepository
     */
    protected $ocProductToStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductToStoreRepo = App::make(oc_product_to_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_to_store()
    {
        $ocProductToStore = $this->fakeoc_product_to_storeData();
        $createdoc_product_to_store = $this->ocProductToStoreRepo->create($ocProductToStore);
        $createdoc_product_to_store = $createdoc_product_to_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_to_store);
        $this->assertNotNull($createdoc_product_to_store['id'], 'Created oc_product_to_store must have id specified');
        $this->assertNotNull(oc_product_to_store::find($createdoc_product_to_store['id']), 'oc_product_to_store with given id must be in DB');
        $this->assertModelData($ocProductToStore, $createdoc_product_to_store);
    }

    /**
     * @test read
     */
    public function testReadoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $dboc_product_to_store = $this->ocProductToStoreRepo->find($ocProductToStore->id);
        $dboc_product_to_store = $dboc_product_to_store->toArray();
        $this->assertModelData($ocProductToStore->toArray(), $dboc_product_to_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $fakeoc_product_to_store = $this->fakeoc_product_to_storeData();
        $updatedoc_product_to_store = $this->ocProductToStoreRepo->update($fakeoc_product_to_store, $ocProductToStore->id);
        $this->assertModelData($fakeoc_product_to_store, $updatedoc_product_to_store->toArray());
        $dboc_product_to_store = $this->ocProductToStoreRepo->find($ocProductToStore->id);
        $this->assertModelData($fakeoc_product_to_store, $dboc_product_to_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $resp = $this->ocProductToStoreRepo->delete($ocProductToStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_to_store::find($ocProductToStore->id), 'oc_product_to_store should not exist in DB');
    }
}
