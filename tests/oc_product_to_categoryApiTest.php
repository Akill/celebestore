<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_categoryApiTest extends TestCase
{
    use Makeoc_product_to_categoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_to_category()
    {
        $ocProductToCategory = $this->fakeoc_product_to_categoryData();
        $this->json('POST', '/api/v1/ocProductToCategories', $ocProductToCategory);

        $this->assertApiResponse($ocProductToCategory);
    }

    /**
     * @test
     */
    public function testReadoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $this->json('GET', '/api/v1/ocProductToCategories/'.$ocProductToCategory->id);

        $this->assertApiResponse($ocProductToCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $editedoc_product_to_category = $this->fakeoc_product_to_categoryData();

        $this->json('PUT', '/api/v1/ocProductToCategories/'.$ocProductToCategory->id, $editedoc_product_to_category);

        $this->assertApiResponse($editedoc_product_to_category);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $this->json('DELETE', '/api/v1/ocProductToCategories/'.$ocProductToCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductToCategories/'.$ocProductToCategory->id);

        $this->assertResponseStatus(404);
    }
}
