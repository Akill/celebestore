<?php

use App\Models\oc_customer_history;
use App\Repositories\oc_customer_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_historyRepositoryTest extends TestCase
{
    use Makeoc_customer_historyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_historyRepository
     */
    protected $ocCustomerHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerHistoryRepo = App::make(oc_customer_historyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_history()
    {
        $ocCustomerHistory = $this->fakeoc_customer_historyData();
        $createdoc_customer_history = $this->ocCustomerHistoryRepo->create($ocCustomerHistory);
        $createdoc_customer_history = $createdoc_customer_history->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_history);
        $this->assertNotNull($createdoc_customer_history['id'], 'Created oc_customer_history must have id specified');
        $this->assertNotNull(oc_customer_history::find($createdoc_customer_history['id']), 'oc_customer_history with given id must be in DB');
        $this->assertModelData($ocCustomerHistory, $createdoc_customer_history);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $dboc_customer_history = $this->ocCustomerHistoryRepo->find($ocCustomerHistory->id);
        $dboc_customer_history = $dboc_customer_history->toArray();
        $this->assertModelData($ocCustomerHistory->toArray(), $dboc_customer_history);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $fakeoc_customer_history = $this->fakeoc_customer_historyData();
        $updatedoc_customer_history = $this->ocCustomerHistoryRepo->update($fakeoc_customer_history, $ocCustomerHistory->id);
        $this->assertModelData($fakeoc_customer_history, $updatedoc_customer_history->toArray());
        $dboc_customer_history = $this->ocCustomerHistoryRepo->find($ocCustomerHistory->id);
        $this->assertModelData($fakeoc_customer_history, $dboc_customer_history->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $resp = $this->ocCustomerHistoryRepo->delete($ocCustomerHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_history::find($ocCustomerHistory->id), 'oc_customer_history should not exist in DB');
    }
}
