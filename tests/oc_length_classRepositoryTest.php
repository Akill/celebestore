<?php

use App\Models\oc_length_class;
use App\Repositories\oc_length_classRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_length_classRepositoryTest extends TestCase
{
    use Makeoc_length_classTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_length_classRepository
     */
    protected $ocLengthClassRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLengthClassRepo = App::make(oc_length_classRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_length_class()
    {
        $ocLengthClass = $this->fakeoc_length_classData();
        $createdoc_length_class = $this->ocLengthClassRepo->create($ocLengthClass);
        $createdoc_length_class = $createdoc_length_class->toArray();
        $this->assertArrayHasKey('id', $createdoc_length_class);
        $this->assertNotNull($createdoc_length_class['id'], 'Created oc_length_class must have id specified');
        $this->assertNotNull(oc_length_class::find($createdoc_length_class['id']), 'oc_length_class with given id must be in DB');
        $this->assertModelData($ocLengthClass, $createdoc_length_class);
    }

    /**
     * @test read
     */
    public function testReadoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $dboc_length_class = $this->ocLengthClassRepo->find($ocLengthClass->id);
        $dboc_length_class = $dboc_length_class->toArray();
        $this->assertModelData($ocLengthClass->toArray(), $dboc_length_class);
    }

    /**
     * @test update
     */
    public function testUpdateoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $fakeoc_length_class = $this->fakeoc_length_classData();
        $updatedoc_length_class = $this->ocLengthClassRepo->update($fakeoc_length_class, $ocLengthClass->id);
        $this->assertModelData($fakeoc_length_class, $updatedoc_length_class->toArray());
        $dboc_length_class = $this->ocLengthClassRepo->find($ocLengthClass->id);
        $this->assertModelData($fakeoc_length_class, $dboc_length_class->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $resp = $this->ocLengthClassRepo->delete($ocLengthClass->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_length_class::find($ocLengthClass->id), 'oc_length_class should not exist in DB');
    }
}
