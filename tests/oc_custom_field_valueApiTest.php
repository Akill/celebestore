<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_valueApiTest extends TestCase
{
    use Makeoc_custom_field_valueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->fakeoc_custom_field_valueData();
        $this->json('POST', '/api/v1/ocCustomFieldValues', $ocCustomFieldValue);

        $this->assertApiResponse($ocCustomFieldValue);
    }

    /**
     * @test
     */
    public function testReadoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $this->json('GET', '/api/v1/ocCustomFieldValues/'.$ocCustomFieldValue->id);

        $this->assertApiResponse($ocCustomFieldValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $editedoc_custom_field_value = $this->fakeoc_custom_field_valueData();

        $this->json('PUT', '/api/v1/ocCustomFieldValues/'.$ocCustomFieldValue->id, $editedoc_custom_field_value);

        $this->assertApiResponse($editedoc_custom_field_value);
    }

    /**
     * @test
     */
    public function testDeleteoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $this->json('DELETE', '/api/v1/ocCustomFieldValues/'.$ocCustomFieldValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomFieldValues/'.$ocCustomFieldValue->id);

        $this->assertResponseStatus(404);
    }
}
