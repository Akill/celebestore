<?php

use App\Models\oc_custom_field_value_description;
use App\Repositories\oc_custom_field_value_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_value_descriptionRepositoryTest extends TestCase
{
    use Makeoc_custom_field_value_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_custom_field_value_descriptionRepository
     */
    protected $ocCustomFieldValueDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomFieldValueDescriptionRepo = App::make(oc_custom_field_value_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->fakeoc_custom_field_value_descriptionData();
        $createdoc_custom_field_value_description = $this->ocCustomFieldValueDescriptionRepo->create($ocCustomFieldValueDescription);
        $createdoc_custom_field_value_description = $createdoc_custom_field_value_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_custom_field_value_description);
        $this->assertNotNull($createdoc_custom_field_value_description['id'], 'Created oc_custom_field_value_description must have id specified');
        $this->assertNotNull(oc_custom_field_value_description::find($createdoc_custom_field_value_description['id']), 'oc_custom_field_value_description with given id must be in DB');
        $this->assertModelData($ocCustomFieldValueDescription, $createdoc_custom_field_value_description);
    }

    /**
     * @test read
     */
    public function testReadoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $dboc_custom_field_value_description = $this->ocCustomFieldValueDescriptionRepo->find($ocCustomFieldValueDescription->id);
        $dboc_custom_field_value_description = $dboc_custom_field_value_description->toArray();
        $this->assertModelData($ocCustomFieldValueDescription->toArray(), $dboc_custom_field_value_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $fakeoc_custom_field_value_description = $this->fakeoc_custom_field_value_descriptionData();
        $updatedoc_custom_field_value_description = $this->ocCustomFieldValueDescriptionRepo->update($fakeoc_custom_field_value_description, $ocCustomFieldValueDescription->id);
        $this->assertModelData($fakeoc_custom_field_value_description, $updatedoc_custom_field_value_description->toArray());
        $dboc_custom_field_value_description = $this->ocCustomFieldValueDescriptionRepo->find($ocCustomFieldValueDescription->id);
        $this->assertModelData($fakeoc_custom_field_value_description, $dboc_custom_field_value_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $resp = $this->ocCustomFieldValueDescriptionRepo->delete($ocCustomFieldValueDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_custom_field_value_description::find($ocCustomFieldValueDescription->id), 'oc_custom_field_value_description should not exist in DB');
    }
}
