<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_manufacturer_to_storeApiTest extends TestCase
{
    use Makeoc_manufacturer_to_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->fakeoc_manufacturer_to_storeData();
        $this->json('POST', '/api/v1/ocManufacturerToStores', $ocManufacturerToStore);

        $this->assertApiResponse($ocManufacturerToStore);
    }

    /**
     * @test
     */
    public function testReadoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $this->json('GET', '/api/v1/ocManufacturerToStores/'.$ocManufacturerToStore->id);

        $this->assertApiResponse($ocManufacturerToStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $editedoc_manufacturer_to_store = $this->fakeoc_manufacturer_to_storeData();

        $this->json('PUT', '/api/v1/ocManufacturerToStores/'.$ocManufacturerToStore->id, $editedoc_manufacturer_to_store);

        $this->assertApiResponse($editedoc_manufacturer_to_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $this->json('DELETE', '/api/v1/ocManufacturerToStores/'.$ocManufacturerToStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocManufacturerToStores/'.$ocManufacturerToStore->id);

        $this->assertResponseStatus(404);
    }
}
