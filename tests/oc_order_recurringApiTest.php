<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_recurringApiTest extends TestCase
{
    use Makeoc_order_recurringTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_recurring()
    {
        $ocOrderRecurring = $this->fakeoc_order_recurringData();
        $this->json('POST', '/api/v1/ocOrderRecurrings', $ocOrderRecurring);

        $this->assertApiResponse($ocOrderRecurring);
    }

    /**
     * @test
     */
    public function testReadoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $this->json('GET', '/api/v1/ocOrderRecurrings/'.$ocOrderRecurring->id);

        $this->assertApiResponse($ocOrderRecurring->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $editedoc_order_recurring = $this->fakeoc_order_recurringData();

        $this->json('PUT', '/api/v1/ocOrderRecurrings/'.$ocOrderRecurring->id, $editedoc_order_recurring);

        $this->assertApiResponse($editedoc_order_recurring);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $this->json('DELETE', '/api/v1/ocOrderRecurrings/'.$ocOrderRecurring->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderRecurrings/'.$ocOrderRecurring->id);

        $this->assertResponseStatus(404);
    }
}
