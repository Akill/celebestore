<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_addressApiTest extends TestCase
{
    use Makeoc_addressTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_address()
    {
        $ocAddress = $this->fakeoc_addressData();
        $this->json('POST', '/api/v1/ocAddresses', $ocAddress);

        $this->assertApiResponse($ocAddress);
    }

    /**
     * @test
     */
    public function testReadoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $this->json('GET', '/api/v1/ocAddresses/'.$ocAddress->id);

        $this->assertApiResponse($ocAddress->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $editedoc_address = $this->fakeoc_addressData();

        $this->json('PUT', '/api/v1/ocAddresses/'.$ocAddress->id, $editedoc_address);

        $this->assertApiResponse($editedoc_address);
    }

    /**
     * @test
     */
    public function testDeleteoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $this->json('DELETE', '/api/v1/ocAddresses/'.$ocAddress->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAddresses/'.$ocAddress->id);

        $this->assertResponseStatus(404);
    }
}
