<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_settingApiTest extends TestCase
{
    use Makeoc_settingTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_setting()
    {
        $ocSetting = $this->fakeoc_settingData();
        $this->json('POST', '/api/v1/ocSettings', $ocSetting);

        $this->assertApiResponse($ocSetting);
    }

    /**
     * @test
     */
    public function testReadoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $this->json('GET', '/api/v1/ocSettings/'.$ocSetting->id);

        $this->assertApiResponse($ocSetting->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $editedoc_setting = $this->fakeoc_settingData();

        $this->json('PUT', '/api/v1/ocSettings/'.$ocSetting->id, $editedoc_setting);

        $this->assertApiResponse($editedoc_setting);
    }

    /**
     * @test
     */
    public function testDeleteoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $this->json('DELETE', '/api/v1/ocSettings/'.$ocSetting->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocSettings/'.$ocSetting->id);

        $this->assertResponseStatus(404);
    }
}
