<?php

use App\Models\oc_product;
use App\Repositories\oc_productRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_productRepositoryTest extends TestCase
{
    use Makeoc_productTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_productRepository
     */
    protected $ocProductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductRepo = App::make(oc_productRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product()
    {
        $ocProduct = $this->fakeoc_productData();
        $createdoc_product = $this->ocProductRepo->create($ocProduct);
        $createdoc_product = $createdoc_product->toArray();
        $this->assertArrayHasKey('id', $createdoc_product);
        $this->assertNotNull($createdoc_product['id'], 'Created oc_product must have id specified');
        $this->assertNotNull(oc_product::find($createdoc_product['id']), 'oc_product with given id must be in DB');
        $this->assertModelData($ocProduct, $createdoc_product);
    }

    /**
     * @test read
     */
    public function testReadoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $dboc_product = $this->ocProductRepo->find($ocProduct->id);
        $dboc_product = $dboc_product->toArray();
        $this->assertModelData($ocProduct->toArray(), $dboc_product);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $fakeoc_product = $this->fakeoc_productData();
        $updatedoc_product = $this->ocProductRepo->update($fakeoc_product, $ocProduct->id);
        $this->assertModelData($fakeoc_product, $updatedoc_product->toArray());
        $dboc_product = $this->ocProductRepo->find($ocProduct->id);
        $this->assertModelData($fakeoc_product, $dboc_product->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product()
    {
        $ocProduct = $this->makeoc_product();
        $resp = $this->ocProductRepo->delete($ocProduct->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product::find($ocProduct->id), 'oc_product should not exist in DB');
    }
}
