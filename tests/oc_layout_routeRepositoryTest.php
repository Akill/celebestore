<?php

use App\Models\oc_layout_route;
use App\Repositories\oc_layout_routeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layout_routeRepositoryTest extends TestCase
{
    use Makeoc_layout_routeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_layout_routeRepository
     */
    protected $ocLayoutRouteRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLayoutRouteRepo = App::make(oc_layout_routeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_layout_route()
    {
        $ocLayoutRoute = $this->fakeoc_layout_routeData();
        $createdoc_layout_route = $this->ocLayoutRouteRepo->create($ocLayoutRoute);
        $createdoc_layout_route = $createdoc_layout_route->toArray();
        $this->assertArrayHasKey('id', $createdoc_layout_route);
        $this->assertNotNull($createdoc_layout_route['id'], 'Created oc_layout_route must have id specified');
        $this->assertNotNull(oc_layout_route::find($createdoc_layout_route['id']), 'oc_layout_route with given id must be in DB');
        $this->assertModelData($ocLayoutRoute, $createdoc_layout_route);
    }

    /**
     * @test read
     */
    public function testReadoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $dboc_layout_route = $this->ocLayoutRouteRepo->find($ocLayoutRoute->id);
        $dboc_layout_route = $dboc_layout_route->toArray();
        $this->assertModelData($ocLayoutRoute->toArray(), $dboc_layout_route);
    }

    /**
     * @test update
     */
    public function testUpdateoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $fakeoc_layout_route = $this->fakeoc_layout_routeData();
        $updatedoc_layout_route = $this->ocLayoutRouteRepo->update($fakeoc_layout_route, $ocLayoutRoute->id);
        $this->assertModelData($fakeoc_layout_route, $updatedoc_layout_route->toArray());
        $dboc_layout_route = $this->ocLayoutRouteRepo->find($ocLayoutRoute->id);
        $this->assertModelData($fakeoc_layout_route, $dboc_layout_route->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $resp = $this->ocLayoutRouteRepo->delete($ocLayoutRoute->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_layout_route::find($ocLayoutRoute->id), 'oc_layout_route should not exist in DB');
    }
}
