<?php

use App\Models\oc_return_status;
use App\Repositories\oc_return_statusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_statusRepositoryTest extends TestCase
{
    use Makeoc_return_statusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_return_statusRepository
     */
    protected $ocReturnStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReturnStatusRepo = App::make(oc_return_statusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_return_status()
    {
        $ocReturnStatus = $this->fakeoc_return_statusData();
        $createdoc_return_status = $this->ocReturnStatusRepo->create($ocReturnStatus);
        $createdoc_return_status = $createdoc_return_status->toArray();
        $this->assertArrayHasKey('id', $createdoc_return_status);
        $this->assertNotNull($createdoc_return_status['id'], 'Created oc_return_status must have id specified');
        $this->assertNotNull(oc_return_status::find($createdoc_return_status['id']), 'oc_return_status with given id must be in DB');
        $this->assertModelData($ocReturnStatus, $createdoc_return_status);
    }

    /**
     * @test read
     */
    public function testReadoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $dboc_return_status = $this->ocReturnStatusRepo->find($ocReturnStatus->id);
        $dboc_return_status = $dboc_return_status->toArray();
        $this->assertModelData($ocReturnStatus->toArray(), $dboc_return_status);
    }

    /**
     * @test update
     */
    public function testUpdateoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $fakeoc_return_status = $this->fakeoc_return_statusData();
        $updatedoc_return_status = $this->ocReturnStatusRepo->update($fakeoc_return_status, $ocReturnStatus->id);
        $this->assertModelData($fakeoc_return_status, $updatedoc_return_status->toArray());
        $dboc_return_status = $this->ocReturnStatusRepo->find($ocReturnStatus->id);
        $this->assertModelData($fakeoc_return_status, $dboc_return_status->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $resp = $this->ocReturnStatusRepo->delete($ocReturnStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_return_status::find($ocReturnStatus->id), 'oc_return_status should not exist in DB');
    }
}
