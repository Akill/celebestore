<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_layoutApiTest extends TestCase
{
    use Makeoc_product_to_layoutTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_to_layout()
    {
        $ocProductToLayout = $this->fakeoc_product_to_layoutData();
        $this->json('POST', '/api/v1/ocProductToLayouts', $ocProductToLayout);

        $this->assertApiResponse($ocProductToLayout);
    }

    /**
     * @test
     */
    public function testReadoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $this->json('GET', '/api/v1/ocProductToLayouts/'.$ocProductToLayout->id);

        $this->assertApiResponse($ocProductToLayout->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $editedoc_product_to_layout = $this->fakeoc_product_to_layoutData();

        $this->json('PUT', '/api/v1/ocProductToLayouts/'.$ocProductToLayout->id, $editedoc_product_to_layout);

        $this->assertApiResponse($editedoc_product_to_layout);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $this->json('DELETE', '/api/v1/ocProductToLayouts/'.$ocProductToLayout->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductToLayouts/'.$ocProductToLayout->id);

        $this->assertResponseStatus(404);
    }
}
