<?php

use App\Models\oc_category;
use App\Repositories\oc_categoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_categoryRepositoryTest extends TestCase
{
    use Makeoc_categoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_categoryRepository
     */
    protected $ocCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryRepo = App::make(oc_categoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category()
    {
        $ocCategory = $this->fakeoc_categoryData();
        $createdoc_category = $this->ocCategoryRepo->create($ocCategory);
        $createdoc_category = $createdoc_category->toArray();
        $this->assertArrayHasKey('id', $createdoc_category);
        $this->assertNotNull($createdoc_category['id'], 'Created oc_category must have id specified');
        $this->assertNotNull(oc_category::find($createdoc_category['id']), 'oc_category with given id must be in DB');
        $this->assertModelData($ocCategory, $createdoc_category);
    }

    /**
     * @test read
     */
    public function testReadoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $dboc_category = $this->ocCategoryRepo->find($ocCategory->id);
        $dboc_category = $dboc_category->toArray();
        $this->assertModelData($ocCategory->toArray(), $dboc_category);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $fakeoc_category = $this->fakeoc_categoryData();
        $updatedoc_category = $this->ocCategoryRepo->update($fakeoc_category, $ocCategory->id);
        $this->assertModelData($fakeoc_category, $updatedoc_category->toArray());
        $dboc_category = $this->ocCategoryRepo->find($ocCategory->id);
        $this->assertModelData($fakeoc_category, $dboc_category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $resp = $this->ocCategoryRepo->delete($ocCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category::find($ocCategory->id), 'oc_category should not exist in DB');
    }
}
