<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_activityApiTest extends TestCase
{
    use Makeoc_customer_activityTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_activity()
    {
        $ocCustomerActivity = $this->fakeoc_customer_activityData();
        $this->json('POST', '/api/v1/ocCustomerActivities', $ocCustomerActivity);

        $this->assertApiResponse($ocCustomerActivity);
    }

    /**
     * @test
     */
    public function testReadoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $this->json('GET', '/api/v1/ocCustomerActivities/'.$ocCustomerActivity->id);

        $this->assertApiResponse($ocCustomerActivity->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $editedoc_customer_activity = $this->fakeoc_customer_activityData();

        $this->json('PUT', '/api/v1/ocCustomerActivities/'.$ocCustomerActivity->id, $editedoc_customer_activity);

        $this->assertApiResponse($editedoc_customer_activity);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $this->json('DELETE', '/api/v1/ocCustomerActivities/'.$ocCustomerActivity->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerActivities/'.$ocCustomerActivity->id);

        $this->assertResponseStatus(404);
    }
}
