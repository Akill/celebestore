<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layoutApiTest extends TestCase
{
    use Makeoc_layoutTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_layout()
    {
        $ocLayout = $this->fakeoc_layoutData();
        $this->json('POST', '/api/v1/ocLayouts', $ocLayout);

        $this->assertApiResponse($ocLayout);
    }

    /**
     * @test
     */
    public function testReadoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $this->json('GET', '/api/v1/ocLayouts/'.$ocLayout->id);

        $this->assertApiResponse($ocLayout->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $editedoc_layout = $this->fakeoc_layoutData();

        $this->json('PUT', '/api/v1/ocLayouts/'.$ocLayout->id, $editedoc_layout);

        $this->assertApiResponse($editedoc_layout);
    }

    /**
     * @test
     */
    public function testDeleteoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $this->json('DELETE', '/api/v1/ocLayouts/'.$ocLayout->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLayouts/'.$ocLayout->id);

        $this->assertResponseStatus(404);
    }
}
