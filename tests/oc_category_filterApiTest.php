<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_filterApiTest extends TestCase
{
    use Makeoc_category_filterTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category_filter()
    {
        $ocCategoryFilter = $this->fakeoc_category_filterData();
        $this->json('POST', '/api/v1/ocCategoryFilters', $ocCategoryFilter);

        $this->assertApiResponse($ocCategoryFilter);
    }

    /**
     * @test
     */
    public function testReadoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $this->json('GET', '/api/v1/ocCategoryFilters/'.$ocCategoryFilter->id);

        $this->assertApiResponse($ocCategoryFilter->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $editedoc_category_filter = $this->fakeoc_category_filterData();

        $this->json('PUT', '/api/v1/ocCategoryFilters/'.$ocCategoryFilter->id, $editedoc_category_filter);

        $this->assertApiResponse($editedoc_category_filter);
    }

    /**
     * @test
     */
    public function testDeleteoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $this->json('DELETE', '/api/v1/ocCategoryFilters/'.$ocCategoryFilter->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategoryFilters/'.$ocCategoryFilter->id);

        $this->assertResponseStatus(404);
    }
}
