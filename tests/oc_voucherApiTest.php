<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucherApiTest extends TestCase
{
    use Makeoc_voucherTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_voucher()
    {
        $ocVoucher = $this->fakeoc_voucherData();
        $this->json('POST', '/api/v1/ocVouchers', $ocVoucher);

        $this->assertApiResponse($ocVoucher);
    }

    /**
     * @test
     */
    public function testReadoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $this->json('GET', '/api/v1/ocVouchers/'.$ocVoucher->id);

        $this->assertApiResponse($ocVoucher->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $editedoc_voucher = $this->fakeoc_voucherData();

        $this->json('PUT', '/api/v1/ocVouchers/'.$ocVoucher->id, $editedoc_voucher);

        $this->assertApiResponse($editedoc_voucher);
    }

    /**
     * @test
     */
    public function testDeleteoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $this->json('DELETE', '/api/v1/ocVouchers/'.$ocVoucher->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocVouchers/'.$ocVoucher->id);

        $this->assertResponseStatus(404);
    }
}
