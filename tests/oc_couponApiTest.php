<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_couponApiTest extends TestCase
{
    use Makeoc_couponTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_coupon()
    {
        $ocCoupon = $this->fakeoc_couponData();
        $this->json('POST', '/api/v1/ocCoupons', $ocCoupon);

        $this->assertApiResponse($ocCoupon);
    }

    /**
     * @test
     */
    public function testReadoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $this->json('GET', '/api/v1/ocCoupons/'.$ocCoupon->id);

        $this->assertApiResponse($ocCoupon->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $editedoc_coupon = $this->fakeoc_couponData();

        $this->json('PUT', '/api/v1/ocCoupons/'.$ocCoupon->id, $editedoc_coupon);

        $this->assertApiResponse($editedoc_coupon);
    }

    /**
     * @test
     */
    public function testDeleteoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $this->json('DELETE', '/api/v1/ocCoupons/'.$ocCoupon->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCoupons/'.$ocCoupon->id);

        $this->assertResponseStatus(404);
    }
}
