<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_groupApiTest extends TestCase
{
    use Makeoc_filter_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_filter_group()
    {
        $ocFilterGroup = $this->fakeoc_filter_groupData();
        $this->json('POST', '/api/v1/ocFilterGroups', $ocFilterGroup);

        $this->assertApiResponse($ocFilterGroup);
    }

    /**
     * @test
     */
    public function testReadoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $this->json('GET', '/api/v1/ocFilterGroups/'.$ocFilterGroup->id);

        $this->assertApiResponse($ocFilterGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $editedoc_filter_group = $this->fakeoc_filter_groupData();

        $this->json('PUT', '/api/v1/ocFilterGroups/'.$ocFilterGroup->id, $editedoc_filter_group);

        $this->assertApiResponse($editedoc_filter_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $this->json('DELETE', '/api/v1/ocFilterGroups/'.$ocFilterGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocFilterGroups/'.$ocFilterGroup->id);

        $this->assertResponseStatus(404);
    }
}
