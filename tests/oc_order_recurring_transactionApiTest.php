<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_recurring_transactionApiTest extends TestCase
{
    use Makeoc_order_recurring_transactionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->fakeoc_order_recurring_transactionData();
        $this->json('POST', '/api/v1/ocOrderRecurringTransactions', $ocOrderRecurringTransaction);

        $this->assertApiResponse($ocOrderRecurringTransaction);
    }

    /**
     * @test
     */
    public function testReadoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $this->json('GET', '/api/v1/ocOrderRecurringTransactions/'.$ocOrderRecurringTransaction->id);

        $this->assertApiResponse($ocOrderRecurringTransaction->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $editedoc_order_recurring_transaction = $this->fakeoc_order_recurring_transactionData();

        $this->json('PUT', '/api/v1/ocOrderRecurringTransactions/'.$ocOrderRecurringTransaction->id, $editedoc_order_recurring_transaction);

        $this->assertApiResponse($editedoc_order_recurring_transaction);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $this->json('DELETE', '/api/v1/ocOrderRecurringTransactions/'.$ocOrderRecurringTransaction->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderRecurringTransactions/'.$ocOrderRecurringTransaction->id);

        $this->assertResponseStatus(404);
    }
}
