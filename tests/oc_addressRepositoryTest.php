<?php

use App\Models\oc_address;
use App\Repositories\oc_addressRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_addressRepositoryTest extends TestCase
{
    use Makeoc_addressTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_addressRepository
     */
    protected $ocAddressRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAddressRepo = App::make(oc_addressRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_address()
    {
        $ocAddress = $this->fakeoc_addressData();
        $createdoc_address = $this->ocAddressRepo->create($ocAddress);
        $createdoc_address = $createdoc_address->toArray();
        $this->assertArrayHasKey('id', $createdoc_address);
        $this->assertNotNull($createdoc_address['id'], 'Created oc_address must have id specified');
        $this->assertNotNull(oc_address::find($createdoc_address['id']), 'oc_address with given id must be in DB');
        $this->assertModelData($ocAddress, $createdoc_address);
    }

    /**
     * @test read
     */
    public function testReadoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $dboc_address = $this->ocAddressRepo->find($ocAddress->id);
        $dboc_address = $dboc_address->toArray();
        $this->assertModelData($ocAddress->toArray(), $dboc_address);
    }

    /**
     * @test update
     */
    public function testUpdateoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $fakeoc_address = $this->fakeoc_addressData();
        $updatedoc_address = $this->ocAddressRepo->update($fakeoc_address, $ocAddress->id);
        $this->assertModelData($fakeoc_address, $updatedoc_address->toArray());
        $dboc_address = $this->ocAddressRepo->find($ocAddress->id);
        $this->assertModelData($fakeoc_address, $dboc_address->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_address()
    {
        $ocAddress = $this->makeoc_address();
        $resp = $this->ocAddressRepo->delete($ocAddress->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_address::find($ocAddress->id), 'oc_address should not exist in DB');
    }
}
