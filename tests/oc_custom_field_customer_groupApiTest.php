<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_customer_groupApiTest extends TestCase
{
    use Makeoc_custom_field_customer_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->fakeoc_custom_field_customer_groupData();
        $this->json('POST', '/api/v1/ocCustomFieldCustomerGroups', $ocCustomFieldCustomerGroup);

        $this->assertApiResponse($ocCustomFieldCustomerGroup);
    }

    /**
     * @test
     */
    public function testReadoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $this->json('GET', '/api/v1/ocCustomFieldCustomerGroups/'.$ocCustomFieldCustomerGroup->id);

        $this->assertApiResponse($ocCustomFieldCustomerGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $editedoc_custom_field_customer_group = $this->fakeoc_custom_field_customer_groupData();

        $this->json('PUT', '/api/v1/ocCustomFieldCustomerGroups/'.$ocCustomFieldCustomerGroup->id, $editedoc_custom_field_customer_group);

        $this->assertApiResponse($editedoc_custom_field_customer_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $this->json('DELETE', '/api/v1/ocCustomFieldCustomerGroups/'.$ocCustomFieldCustomerGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomFieldCustomerGroups/'.$ocCustomFieldCustomerGroup->id);

        $this->assertResponseStatus(404);
    }
}
