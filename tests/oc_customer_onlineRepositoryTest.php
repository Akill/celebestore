<?php

use App\Models\oc_customer_online;
use App\Repositories\oc_customer_onlineRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_onlineRepositoryTest extends TestCase
{
    use Makeoc_customer_onlineTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_onlineRepository
     */
    protected $ocCustomerOnlineRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerOnlineRepo = App::make(oc_customer_onlineRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_online()
    {
        $ocCustomerOnline = $this->fakeoc_customer_onlineData();
        $createdoc_customer_online = $this->ocCustomerOnlineRepo->create($ocCustomerOnline);
        $createdoc_customer_online = $createdoc_customer_online->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_online);
        $this->assertNotNull($createdoc_customer_online['id'], 'Created oc_customer_online must have id specified');
        $this->assertNotNull(oc_customer_online::find($createdoc_customer_online['id']), 'oc_customer_online with given id must be in DB');
        $this->assertModelData($ocCustomerOnline, $createdoc_customer_online);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $dboc_customer_online = $this->ocCustomerOnlineRepo->find($ocCustomerOnline->id);
        $dboc_customer_online = $dboc_customer_online->toArray();
        $this->assertModelData($ocCustomerOnline->toArray(), $dboc_customer_online);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $fakeoc_customer_online = $this->fakeoc_customer_onlineData();
        $updatedoc_customer_online = $this->ocCustomerOnlineRepo->update($fakeoc_customer_online, $ocCustomerOnline->id);
        $this->assertModelData($fakeoc_customer_online, $updatedoc_customer_online->toArray());
        $dboc_customer_online = $this->ocCustomerOnlineRepo->find($ocCustomerOnline->id);
        $this->assertModelData($fakeoc_customer_online, $dboc_customer_online->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $resp = $this->ocCustomerOnlineRepo->delete($ocCustomerOnline->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_online::find($ocCustomerOnline->id), 'oc_customer_online should not exist in DB');
    }
}
