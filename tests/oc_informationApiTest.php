<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_informationApiTest extends TestCase
{
    use Makeoc_informationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_information()
    {
        $ocInformation = $this->fakeoc_informationData();
        $this->json('POST', '/api/v1/ocInformations', $ocInformation);

        $this->assertApiResponse($ocInformation);
    }

    /**
     * @test
     */
    public function testReadoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $this->json('GET', '/api/v1/ocInformations/'.$ocInformation->id);

        $this->assertApiResponse($ocInformation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $editedoc_information = $this->fakeoc_informationData();

        $this->json('PUT', '/api/v1/ocInformations/'.$ocInformation->id, $editedoc_information);

        $this->assertApiResponse($editedoc_information);
    }

    /**
     * @test
     */
    public function testDeleteoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $this->json('DELETE', '/api/v1/ocInformations/'.$ocInformation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocInformations/'.$ocInformation->id);

        $this->assertResponseStatus(404);
    }
}
