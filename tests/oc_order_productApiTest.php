<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_productApiTest extends TestCase
{
    use Makeoc_order_productTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_product()
    {
        $ocOrderProduct = $this->fakeoc_order_productData();
        $this->json('POST', '/api/v1/ocOrderProducts', $ocOrderProduct);

        $this->assertApiResponse($ocOrderProduct);
    }

    /**
     * @test
     */
    public function testReadoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $this->json('GET', '/api/v1/ocOrderProducts/'.$ocOrderProduct->id);

        $this->assertApiResponse($ocOrderProduct->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $editedoc_order_product = $this->fakeoc_order_productData();

        $this->json('PUT', '/api/v1/ocOrderProducts/'.$ocOrderProduct->id, $editedoc_order_product);

        $this->assertApiResponse($editedoc_order_product);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $this->json('DELETE', '/api/v1/ocOrderProducts/'.$ocOrderProduct->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderProducts/'.$ocOrderProduct->id);

        $this->assertResponseStatus(404);
    }
}
