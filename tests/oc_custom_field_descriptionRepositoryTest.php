<?php

use App\Models\oc_custom_field_description;
use App\Repositories\oc_custom_field_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_descriptionRepositoryTest extends TestCase
{
    use Makeoc_custom_field_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_custom_field_descriptionRepository
     */
    protected $ocCustomFieldDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomFieldDescriptionRepo = App::make(oc_custom_field_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->fakeoc_custom_field_descriptionData();
        $createdoc_custom_field_description = $this->ocCustomFieldDescriptionRepo->create($ocCustomFieldDescription);
        $createdoc_custom_field_description = $createdoc_custom_field_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_custom_field_description);
        $this->assertNotNull($createdoc_custom_field_description['id'], 'Created oc_custom_field_description must have id specified');
        $this->assertNotNull(oc_custom_field_description::find($createdoc_custom_field_description['id']), 'oc_custom_field_description with given id must be in DB');
        $this->assertModelData($ocCustomFieldDescription, $createdoc_custom_field_description);
    }

    /**
     * @test read
     */
    public function testReadoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $dboc_custom_field_description = $this->ocCustomFieldDescriptionRepo->find($ocCustomFieldDescription->id);
        $dboc_custom_field_description = $dboc_custom_field_description->toArray();
        $this->assertModelData($ocCustomFieldDescription->toArray(), $dboc_custom_field_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $fakeoc_custom_field_description = $this->fakeoc_custom_field_descriptionData();
        $updatedoc_custom_field_description = $this->ocCustomFieldDescriptionRepo->update($fakeoc_custom_field_description, $ocCustomFieldDescription->id);
        $this->assertModelData($fakeoc_custom_field_description, $updatedoc_custom_field_description->toArray());
        $dboc_custom_field_description = $this->ocCustomFieldDescriptionRepo->find($ocCustomFieldDescription->id);
        $this->assertModelData($fakeoc_custom_field_description, $dboc_custom_field_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_custom_field_description()
    {
        $ocCustomFieldDescription = $this->makeoc_custom_field_description();
        $resp = $this->ocCustomFieldDescriptionRepo->delete($ocCustomFieldDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_custom_field_description::find($ocCustomFieldDescription->id), 'oc_custom_field_description should not exist in DB');
    }
}
