<?php

use App\Models\oc_return_history;
use App\Repositories\oc_return_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_historyRepositoryTest extends TestCase
{
    use Makeoc_return_historyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_return_historyRepository
     */
    protected $ocReturnHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReturnHistoryRepo = App::make(oc_return_historyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_return_history()
    {
        $ocReturnHistory = $this->fakeoc_return_historyData();
        $createdoc_return_history = $this->ocReturnHistoryRepo->create($ocReturnHistory);
        $createdoc_return_history = $createdoc_return_history->toArray();
        $this->assertArrayHasKey('id', $createdoc_return_history);
        $this->assertNotNull($createdoc_return_history['id'], 'Created oc_return_history must have id specified');
        $this->assertNotNull(oc_return_history::find($createdoc_return_history['id']), 'oc_return_history with given id must be in DB');
        $this->assertModelData($ocReturnHistory, $createdoc_return_history);
    }

    /**
     * @test read
     */
    public function testReadoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $dboc_return_history = $this->ocReturnHistoryRepo->find($ocReturnHistory->id);
        $dboc_return_history = $dboc_return_history->toArray();
        $this->assertModelData($ocReturnHistory->toArray(), $dboc_return_history);
    }

    /**
     * @test update
     */
    public function testUpdateoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $fakeoc_return_history = $this->fakeoc_return_historyData();
        $updatedoc_return_history = $this->ocReturnHistoryRepo->update($fakeoc_return_history, $ocReturnHistory->id);
        $this->assertModelData($fakeoc_return_history, $updatedoc_return_history->toArray());
        $dboc_return_history = $this->ocReturnHistoryRepo->find($ocReturnHistory->id);
        $this->assertModelData($fakeoc_return_history, $dboc_return_history->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $resp = $this->ocReturnHistoryRepo->delete($ocReturnHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_return_history::find($ocReturnHistory->id), 'oc_return_history should not exist in DB');
    }
}
