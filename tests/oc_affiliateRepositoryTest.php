<?php

use App\Models\oc_affiliate;
use App\Repositories\oc_affiliateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliateRepositoryTest extends TestCase
{
    use Makeoc_affiliateTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_affiliateRepository
     */
    protected $ocAffiliateRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAffiliateRepo = App::make(oc_affiliateRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_affiliate()
    {
        $ocAffiliate = $this->fakeoc_affiliateData();
        $createdoc_affiliate = $this->ocAffiliateRepo->create($ocAffiliate);
        $createdoc_affiliate = $createdoc_affiliate->toArray();
        $this->assertArrayHasKey('id', $createdoc_affiliate);
        $this->assertNotNull($createdoc_affiliate['id'], 'Created oc_affiliate must have id specified');
        $this->assertNotNull(oc_affiliate::find($createdoc_affiliate['id']), 'oc_affiliate with given id must be in DB');
        $this->assertModelData($ocAffiliate, $createdoc_affiliate);
    }

    /**
     * @test read
     */
    public function testReadoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $dboc_affiliate = $this->ocAffiliateRepo->find($ocAffiliate->id);
        $dboc_affiliate = $dboc_affiliate->toArray();
        $this->assertModelData($ocAffiliate->toArray(), $dboc_affiliate);
    }

    /**
     * @test update
     */
    public function testUpdateoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $fakeoc_affiliate = $this->fakeoc_affiliateData();
        $updatedoc_affiliate = $this->ocAffiliateRepo->update($fakeoc_affiliate, $ocAffiliate->id);
        $this->assertModelData($fakeoc_affiliate, $updatedoc_affiliate->toArray());
        $dboc_affiliate = $this->ocAffiliateRepo->find($ocAffiliate->id);
        $this->assertModelData($fakeoc_affiliate, $dboc_affiliate->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $resp = $this->ocAffiliateRepo->delete($ocAffiliate->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_affiliate::find($ocAffiliate->id), 'oc_affiliate should not exist in DB');
    }
}
