<?php

use App\Models\oc_order;
use App\Repositories\oc_orderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_orderRepositoryTest extends TestCase
{
    use Makeoc_orderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_orderRepository
     */
    protected $ocOrderRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderRepo = App::make(oc_orderRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order()
    {
        $ocOrder = $this->fakeoc_orderData();
        $createdoc_order = $this->ocOrderRepo->create($ocOrder);
        $createdoc_order = $createdoc_order->toArray();
        $this->assertArrayHasKey('id', $createdoc_order);
        $this->assertNotNull($createdoc_order['id'], 'Created oc_order must have id specified');
        $this->assertNotNull(oc_order::find($createdoc_order['id']), 'oc_order with given id must be in DB');
        $this->assertModelData($ocOrder, $createdoc_order);
    }

    /**
     * @test read
     */
    public function testReadoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $dboc_order = $this->ocOrderRepo->find($ocOrder->id);
        $dboc_order = $dboc_order->toArray();
        $this->assertModelData($ocOrder->toArray(), $dboc_order);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $fakeoc_order = $this->fakeoc_orderData();
        $updatedoc_order = $this->ocOrderRepo->update($fakeoc_order, $ocOrder->id);
        $this->assertModelData($fakeoc_order, $updatedoc_order->toArray());
        $dboc_order = $this->ocOrderRepo->find($ocOrder->id);
        $this->assertModelData($fakeoc_order, $dboc_order->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $resp = $this->ocOrderRepo->delete($ocOrder->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order::find($ocOrder->id), 'oc_order should not exist in DB');
    }
}
