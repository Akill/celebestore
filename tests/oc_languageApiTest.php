<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_languageApiTest extends TestCase
{
    use Makeoc_languageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_language()
    {
        $ocLanguage = $this->fakeoc_languageData();
        $this->json('POST', '/api/v1/ocLanguages', $ocLanguage);

        $this->assertApiResponse($ocLanguage);
    }

    /**
     * @test
     */
    public function testReadoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $this->json('GET', '/api/v1/ocLanguages/'.$ocLanguage->id);

        $this->assertApiResponse($ocLanguage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $editedoc_language = $this->fakeoc_languageData();

        $this->json('PUT', '/api/v1/ocLanguages/'.$ocLanguage->id, $editedoc_language);

        $this->assertApiResponse($editedoc_language);
    }

    /**
     * @test
     */
    public function testDeleteoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $this->json('DELETE', '/api/v1/ocLanguages/'.$ocLanguage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLanguages/'.$ocLanguage->id);

        $this->assertResponseStatus(404);
    }
}
