<?php

use App\Models\oc_information_to_layout;
use App\Repositories\oc_information_to_layoutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_to_layoutRepositoryTest extends TestCase
{
    use Makeoc_information_to_layoutTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_information_to_layoutRepository
     */
    protected $ocInformationToLayoutRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocInformationToLayoutRepo = App::make(oc_information_to_layoutRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_information_to_layout()
    {
        $ocInformationToLayout = $this->fakeoc_information_to_layoutData();
        $createdoc_information_to_layout = $this->ocInformationToLayoutRepo->create($ocInformationToLayout);
        $createdoc_information_to_layout = $createdoc_information_to_layout->toArray();
        $this->assertArrayHasKey('id', $createdoc_information_to_layout);
        $this->assertNotNull($createdoc_information_to_layout['id'], 'Created oc_information_to_layout must have id specified');
        $this->assertNotNull(oc_information_to_layout::find($createdoc_information_to_layout['id']), 'oc_information_to_layout with given id must be in DB');
        $this->assertModelData($ocInformationToLayout, $createdoc_information_to_layout);
    }

    /**
     * @test read
     */
    public function testReadoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $dboc_information_to_layout = $this->ocInformationToLayoutRepo->find($ocInformationToLayout->id);
        $dboc_information_to_layout = $dboc_information_to_layout->toArray();
        $this->assertModelData($ocInformationToLayout->toArray(), $dboc_information_to_layout);
    }

    /**
     * @test update
     */
    public function testUpdateoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $fakeoc_information_to_layout = $this->fakeoc_information_to_layoutData();
        $updatedoc_information_to_layout = $this->ocInformationToLayoutRepo->update($fakeoc_information_to_layout, $ocInformationToLayout->id);
        $this->assertModelData($fakeoc_information_to_layout, $updatedoc_information_to_layout->toArray());
        $dboc_information_to_layout = $this->ocInformationToLayoutRepo->find($ocInformationToLayout->id);
        $this->assertModelData($fakeoc_information_to_layout, $dboc_information_to_layout->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $resp = $this->ocInformationToLayoutRepo->delete($ocInformationToLayout->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_information_to_layout::find($ocInformationToLayout->id), 'oc_information_to_layout should not exist in DB');
    }
}
