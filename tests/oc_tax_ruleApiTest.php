<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_ruleApiTest extends TestCase
{
    use Makeoc_tax_ruleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_tax_rule()
    {
        $ocTaxRule = $this->fakeoc_tax_ruleData();
        $this->json('POST', '/api/v1/ocTaxRules', $ocTaxRule);

        $this->assertApiResponse($ocTaxRule);
    }

    /**
     * @test
     */
    public function testReadoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $this->json('GET', '/api/v1/ocTaxRules/'.$ocTaxRule->id);

        $this->assertApiResponse($ocTaxRule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $editedoc_tax_rule = $this->fakeoc_tax_ruleData();

        $this->json('PUT', '/api/v1/ocTaxRules/'.$ocTaxRule->id, $editedoc_tax_rule);

        $this->assertApiResponse($editedoc_tax_rule);
    }

    /**
     * @test
     */
    public function testDeleteoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $this->json('DELETE', '/api/v1/ocTaxRules/'.$ocTaxRule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocTaxRules/'.$ocTaxRule->id);

        $this->assertResponseStatus(404);
    }
}
