<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_activityApiTest extends TestCase
{
    use Makeoc_affiliate_activityTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->fakeoc_affiliate_activityData();
        $this->json('POST', '/api/v1/ocAffiliateActivities', $ocAffiliateActivity);

        $this->assertApiResponse($ocAffiliateActivity);
    }

    /**
     * @test
     */
    public function testReadoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $this->json('GET', '/api/v1/ocAffiliateActivities/'.$ocAffiliateActivity->id);

        $this->assertApiResponse($ocAffiliateActivity->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $editedoc_affiliate_activity = $this->fakeoc_affiliate_activityData();

        $this->json('PUT', '/api/v1/ocAffiliateActivities/'.$ocAffiliateActivity->id, $editedoc_affiliate_activity);

        $this->assertApiResponse($editedoc_affiliate_activity);
    }

    /**
     * @test
     */
    public function testDeleteoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $this->json('DELETE', '/api/v1/ocAffiliateActivities/'.$ocAffiliateActivity->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAffiliateActivities/'.$ocAffiliateActivity->id);

        $this->assertResponseStatus(404);
    }
}
