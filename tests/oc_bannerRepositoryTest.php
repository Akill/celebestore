<?php

use App\Models\oc_banner;
use App\Repositories\oc_bannerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_bannerRepositoryTest extends TestCase
{
    use Makeoc_bannerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_bannerRepository
     */
    protected $ocBannerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocBannerRepo = App::make(oc_bannerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_banner()
    {
        $ocBanner = $this->fakeoc_bannerData();
        $createdoc_banner = $this->ocBannerRepo->create($ocBanner);
        $createdoc_banner = $createdoc_banner->toArray();
        $this->assertArrayHasKey('id', $createdoc_banner);
        $this->assertNotNull($createdoc_banner['id'], 'Created oc_banner must have id specified');
        $this->assertNotNull(oc_banner::find($createdoc_banner['id']), 'oc_banner with given id must be in DB');
        $this->assertModelData($ocBanner, $createdoc_banner);
    }

    /**
     * @test read
     */
    public function testReadoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $dboc_banner = $this->ocBannerRepo->find($ocBanner->id);
        $dboc_banner = $dboc_banner->toArray();
        $this->assertModelData($ocBanner->toArray(), $dboc_banner);
    }

    /**
     * @test update
     */
    public function testUpdateoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $fakeoc_banner = $this->fakeoc_bannerData();
        $updatedoc_banner = $this->ocBannerRepo->update($fakeoc_banner, $ocBanner->id);
        $this->assertModelData($fakeoc_banner, $updatedoc_banner->toArray());
        $dboc_banner = $this->ocBannerRepo->find($ocBanner->id);
        $this->assertModelData($fakeoc_banner, $dboc_banner->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $resp = $this->ocBannerRepo->delete($ocBanner->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_banner::find($ocBanner->id), 'oc_banner should not exist in DB');
    }
}
