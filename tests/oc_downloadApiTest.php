<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_downloadApiTest extends TestCase
{
    use Makeoc_downloadTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_download()
    {
        $ocDownload = $this->fakeoc_downloadData();
        $this->json('POST', '/api/v1/ocDownloads', $ocDownload);

        $this->assertApiResponse($ocDownload);
    }

    /**
     * @test
     */
    public function testReadoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $this->json('GET', '/api/v1/ocDownloads/'.$ocDownload->id);

        $this->assertApiResponse($ocDownload->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $editedoc_download = $this->fakeoc_downloadData();

        $this->json('PUT', '/api/v1/ocDownloads/'.$ocDownload->id, $editedoc_download);

        $this->assertApiResponse($editedoc_download);
    }

    /**
     * @test
     */
    public function testDeleteoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $this->json('DELETE', '/api/v1/ocDownloads/'.$ocDownload->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocDownloads/'.$ocDownload->id);

        $this->assertResponseStatus(404);
    }
}
