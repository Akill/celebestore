<?php

use App\Models\oc_product_option_value;
use App\Repositories\oc_product_option_valueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_option_valueRepositoryTest extends TestCase
{
    use Makeoc_product_option_valueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_option_valueRepository
     */
    protected $ocProductOptionValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductOptionValueRepo = App::make(oc_product_option_valueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_option_value()
    {
        $ocProductOptionValue = $this->fakeoc_product_option_valueData();
        $createdoc_product_option_value = $this->ocProductOptionValueRepo->create($ocProductOptionValue);
        $createdoc_product_option_value = $createdoc_product_option_value->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_option_value);
        $this->assertNotNull($createdoc_product_option_value['id'], 'Created oc_product_option_value must have id specified');
        $this->assertNotNull(oc_product_option_value::find($createdoc_product_option_value['id']), 'oc_product_option_value with given id must be in DB');
        $this->assertModelData($ocProductOptionValue, $createdoc_product_option_value);
    }

    /**
     * @test read
     */
    public function testReadoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $dboc_product_option_value = $this->ocProductOptionValueRepo->find($ocProductOptionValue->id);
        $dboc_product_option_value = $dboc_product_option_value->toArray();
        $this->assertModelData($ocProductOptionValue->toArray(), $dboc_product_option_value);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $fakeoc_product_option_value = $this->fakeoc_product_option_valueData();
        $updatedoc_product_option_value = $this->ocProductOptionValueRepo->update($fakeoc_product_option_value, $ocProductOptionValue->id);
        $this->assertModelData($fakeoc_product_option_value, $updatedoc_product_option_value->toArray());
        $dboc_product_option_value = $this->ocProductOptionValueRepo->find($ocProductOptionValue->id);
        $this->assertModelData($fakeoc_product_option_value, $dboc_product_option_value->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $resp = $this->ocProductOptionValueRepo->delete($ocProductOptionValue->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_option_value::find($ocProductOptionValue->id), 'oc_product_option_value should not exist in DB');
    }
}
