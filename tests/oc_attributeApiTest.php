<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attributeApiTest extends TestCase
{
    use Makeoc_attributeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_attribute()
    {
        $ocAttribute = $this->fakeoc_attributeData();
        $this->json('POST', '/api/v1/ocAttributes', $ocAttribute);

        $this->assertApiResponse($ocAttribute);
    }

    /**
     * @test
     */
    public function testReadoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $this->json('GET', '/api/v1/ocAttributes/'.$ocAttribute->id);

        $this->assertApiResponse($ocAttribute->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $editedoc_attribute = $this->fakeoc_attributeData();

        $this->json('PUT', '/api/v1/ocAttributes/'.$ocAttribute->id, $editedoc_attribute);

        $this->assertApiResponse($editedoc_attribute);
    }

    /**
     * @test
     */
    public function testDeleteoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $this->json('DELETE', '/api/v1/ocAttributes/'.$ocAttribute->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAttributes/'.$ocAttribute->id);

        $this->assertResponseStatus(404);
    }
}
