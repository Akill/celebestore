<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_currencyApiTest extends TestCase
{
    use Makeoc_currencyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_currency()
    {
        $ocCurrency = $this->fakeoc_currencyData();
        $this->json('POST', '/api/v1/ocCurrencies', $ocCurrency);

        $this->assertApiResponse($ocCurrency);
    }

    /**
     * @test
     */
    public function testReadoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $this->json('GET', '/api/v1/ocCurrencies/'.$ocCurrency->id);

        $this->assertApiResponse($ocCurrency->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $editedoc_currency = $this->fakeoc_currencyData();

        $this->json('PUT', '/api/v1/ocCurrencies/'.$ocCurrency->id, $editedoc_currency);

        $this->assertApiResponse($editedoc_currency);
    }

    /**
     * @test
     */
    public function testDeleteoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $this->json('DELETE', '/api/v1/ocCurrencies/'.$ocCurrency->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCurrencies/'.$ocCurrency->id);

        $this->assertResponseStatus(404);
    }
}
