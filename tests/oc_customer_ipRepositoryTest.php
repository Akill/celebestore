<?php

use App\Models\oc_customer_ip;
use App\Repositories\oc_customer_ipRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_ipRepositoryTest extends TestCase
{
    use Makeoc_customer_ipTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_ipRepository
     */
    protected $ocCustomerIpRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerIpRepo = App::make(oc_customer_ipRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_ip()
    {
        $ocCustomerIp = $this->fakeoc_customer_ipData();
        $createdoc_customer_ip = $this->ocCustomerIpRepo->create($ocCustomerIp);
        $createdoc_customer_ip = $createdoc_customer_ip->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_ip);
        $this->assertNotNull($createdoc_customer_ip['id'], 'Created oc_customer_ip must have id specified');
        $this->assertNotNull(oc_customer_ip::find($createdoc_customer_ip['id']), 'oc_customer_ip with given id must be in DB');
        $this->assertModelData($ocCustomerIp, $createdoc_customer_ip);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $dboc_customer_ip = $this->ocCustomerIpRepo->find($ocCustomerIp->id);
        $dboc_customer_ip = $dboc_customer_ip->toArray();
        $this->assertModelData($ocCustomerIp->toArray(), $dboc_customer_ip);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $fakeoc_customer_ip = $this->fakeoc_customer_ipData();
        $updatedoc_customer_ip = $this->ocCustomerIpRepo->update($fakeoc_customer_ip, $ocCustomerIp->id);
        $this->assertModelData($fakeoc_customer_ip, $updatedoc_customer_ip->toArray());
        $dboc_customer_ip = $this->ocCustomerIpRepo->find($ocCustomerIp->id);
        $this->assertModelData($fakeoc_customer_ip, $dboc_customer_ip->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $resp = $this->ocCustomerIpRepo->delete($ocCustomerIp->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_ip::find($ocCustomerIp->id), 'oc_customer_ip should not exist in DB');
    }
}
