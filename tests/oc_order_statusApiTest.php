<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_statusApiTest extends TestCase
{
    use Makeoc_order_statusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_status()
    {
        $ocOrderStatus = $this->fakeoc_order_statusData();
        $this->json('POST', '/api/v1/ocOrderStatuses', $ocOrderStatus);

        $this->assertApiResponse($ocOrderStatus);
    }

    /**
     * @test
     */
    public function testReadoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $this->json('GET', '/api/v1/ocOrderStatuses/'.$ocOrderStatus->id);

        $this->assertApiResponse($ocOrderStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $editedoc_order_status = $this->fakeoc_order_statusData();

        $this->json('PUT', '/api/v1/ocOrderStatuses/'.$ocOrderStatus->id, $editedoc_order_status);

        $this->assertApiResponse($editedoc_order_status);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $this->json('DELETE', '/api/v1/ocOrderStatuses/'.$ocOrderStatus->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderStatuses/'.$ocOrderStatus->id);

        $this->assertResponseStatus(404);
    }
}
