<?php

use App\Models\oc_location;
use App\Repositories\oc_locationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_locationRepositoryTest extends TestCase
{
    use Makeoc_locationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_locationRepository
     */
    protected $ocLocationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLocationRepo = App::make(oc_locationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_location()
    {
        $ocLocation = $this->fakeoc_locationData();
        $createdoc_location = $this->ocLocationRepo->create($ocLocation);
        $createdoc_location = $createdoc_location->toArray();
        $this->assertArrayHasKey('id', $createdoc_location);
        $this->assertNotNull($createdoc_location['id'], 'Created oc_location must have id specified');
        $this->assertNotNull(oc_location::find($createdoc_location['id']), 'oc_location with given id must be in DB');
        $this->assertModelData($ocLocation, $createdoc_location);
    }

    /**
     * @test read
     */
    public function testReadoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $dboc_location = $this->ocLocationRepo->find($ocLocation->id);
        $dboc_location = $dboc_location->toArray();
        $this->assertModelData($ocLocation->toArray(), $dboc_location);
    }

    /**
     * @test update
     */
    public function testUpdateoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $fakeoc_location = $this->fakeoc_locationData();
        $updatedoc_location = $this->ocLocationRepo->update($fakeoc_location, $ocLocation->id);
        $this->assertModelData($fakeoc_location, $updatedoc_location->toArray());
        $dboc_location = $this->ocLocationRepo->find($ocLocation->id);
        $this->assertModelData($fakeoc_location, $dboc_location->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $resp = $this->ocLocationRepo->delete($ocLocation->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_location::find($ocLocation->id), 'oc_location should not exist in DB');
    }
}
