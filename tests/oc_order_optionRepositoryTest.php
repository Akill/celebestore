<?php

use App\Models\oc_order_option;
use App\Repositories\oc_order_optionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_optionRepositoryTest extends TestCase
{
    use Makeoc_order_optionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_optionRepository
     */
    protected $ocOrderOptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderOptionRepo = App::make(oc_order_optionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_option()
    {
        $ocOrderOption = $this->fakeoc_order_optionData();
        $createdoc_order_option = $this->ocOrderOptionRepo->create($ocOrderOption);
        $createdoc_order_option = $createdoc_order_option->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_option);
        $this->assertNotNull($createdoc_order_option['id'], 'Created oc_order_option must have id specified');
        $this->assertNotNull(oc_order_option::find($createdoc_order_option['id']), 'oc_order_option with given id must be in DB');
        $this->assertModelData($ocOrderOption, $createdoc_order_option);
    }

    /**
     * @test read
     */
    public function testReadoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $dboc_order_option = $this->ocOrderOptionRepo->find($ocOrderOption->id);
        $dboc_order_option = $dboc_order_option->toArray();
        $this->assertModelData($ocOrderOption->toArray(), $dboc_order_option);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $fakeoc_order_option = $this->fakeoc_order_optionData();
        $updatedoc_order_option = $this->ocOrderOptionRepo->update($fakeoc_order_option, $ocOrderOption->id);
        $this->assertModelData($fakeoc_order_option, $updatedoc_order_option->toArray());
        $dboc_order_option = $this->ocOrderOptionRepo->find($ocOrderOption->id);
        $this->assertModelData($fakeoc_order_option, $dboc_order_option->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_option()
    {
        $ocOrderOption = $this->makeoc_order_option();
        $resp = $this->ocOrderOptionRepo->delete($ocOrderOption->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_option::find($ocOrderOption->id), 'oc_order_option should not exist in DB');
    }
}
