<?php

use App\Models\oc_tax_rate;
use App\Repositories\oc_tax_rateRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_rateRepositoryTest extends TestCase
{
    use Makeoc_tax_rateTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_tax_rateRepository
     */
    protected $ocTaxRateRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocTaxRateRepo = App::make(oc_tax_rateRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_tax_rate()
    {
        $ocTaxRate = $this->fakeoc_tax_rateData();
        $createdoc_tax_rate = $this->ocTaxRateRepo->create($ocTaxRate);
        $createdoc_tax_rate = $createdoc_tax_rate->toArray();
        $this->assertArrayHasKey('id', $createdoc_tax_rate);
        $this->assertNotNull($createdoc_tax_rate['id'], 'Created oc_tax_rate must have id specified');
        $this->assertNotNull(oc_tax_rate::find($createdoc_tax_rate['id']), 'oc_tax_rate with given id must be in DB');
        $this->assertModelData($ocTaxRate, $createdoc_tax_rate);
    }

    /**
     * @test read
     */
    public function testReadoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $dboc_tax_rate = $this->ocTaxRateRepo->find($ocTaxRate->id);
        $dboc_tax_rate = $dboc_tax_rate->toArray();
        $this->assertModelData($ocTaxRate->toArray(), $dboc_tax_rate);
    }

    /**
     * @test update
     */
    public function testUpdateoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $fakeoc_tax_rate = $this->fakeoc_tax_rateData();
        $updatedoc_tax_rate = $this->ocTaxRateRepo->update($fakeoc_tax_rate, $ocTaxRate->id);
        $this->assertModelData($fakeoc_tax_rate, $updatedoc_tax_rate->toArray());
        $dboc_tax_rate = $this->ocTaxRateRepo->find($ocTaxRate->id);
        $this->assertModelData($fakeoc_tax_rate, $dboc_tax_rate->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $resp = $this->ocTaxRateRepo->delete($ocTaxRate->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_tax_rate::find($ocTaxRate->id), 'oc_tax_rate should not exist in DB');
    }
}
