<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layout_routeApiTest extends TestCase
{
    use Makeoc_layout_routeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_layout_route()
    {
        $ocLayoutRoute = $this->fakeoc_layout_routeData();
        $this->json('POST', '/api/v1/ocLayoutRoutes', $ocLayoutRoute);

        $this->assertApiResponse($ocLayoutRoute);
    }

    /**
     * @test
     */
    public function testReadoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $this->json('GET', '/api/v1/ocLayoutRoutes/'.$ocLayoutRoute->id);

        $this->assertApiResponse($ocLayoutRoute->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $editedoc_layout_route = $this->fakeoc_layout_routeData();

        $this->json('PUT', '/api/v1/ocLayoutRoutes/'.$ocLayoutRoute->id, $editedoc_layout_route);

        $this->assertApiResponse($editedoc_layout_route);
    }

    /**
     * @test
     */
    public function testDeleteoc_layout_route()
    {
        $ocLayoutRoute = $this->makeoc_layout_route();
        $this->json('DELETE', '/api/v1/ocLayoutRoutes/'.$ocLayoutRoute->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLayoutRoutes/'.$ocLayoutRoute->id);

        $this->assertResponseStatus(404);
    }
}
