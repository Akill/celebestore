<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_to_layoutApiTest extends TestCase
{
    use Makeoc_information_to_layoutTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_information_to_layout()
    {
        $ocInformationToLayout = $this->fakeoc_information_to_layoutData();
        $this->json('POST', '/api/v1/ocInformationToLayouts', $ocInformationToLayout);

        $this->assertApiResponse($ocInformationToLayout);
    }

    /**
     * @test
     */
    public function testReadoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $this->json('GET', '/api/v1/ocInformationToLayouts/'.$ocInformationToLayout->id);

        $this->assertApiResponse($ocInformationToLayout->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $editedoc_information_to_layout = $this->fakeoc_information_to_layoutData();

        $this->json('PUT', '/api/v1/ocInformationToLayouts/'.$ocInformationToLayout->id, $editedoc_information_to_layout);

        $this->assertApiResponse($editedoc_information_to_layout);
    }

    /**
     * @test
     */
    public function testDeleteoc_information_to_layout()
    {
        $ocInformationToLayout = $this->makeoc_information_to_layout();
        $this->json('DELETE', '/api/v1/ocInformationToLayouts/'.$ocInformationToLayout->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocInformationToLayouts/'.$ocInformationToLayout->id);

        $this->assertResponseStatus(404);
    }
}
