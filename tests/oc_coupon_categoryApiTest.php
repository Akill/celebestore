<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_categoryApiTest extends TestCase
{
    use Makeoc_coupon_categoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_coupon_category()
    {
        $ocCouponCategory = $this->fakeoc_coupon_categoryData();
        $this->json('POST', '/api/v1/ocCouponCategories', $ocCouponCategory);

        $this->assertApiResponse($ocCouponCategory);
    }

    /**
     * @test
     */
    public function testReadoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $this->json('GET', '/api/v1/ocCouponCategories/'.$ocCouponCategory->id);

        $this->assertApiResponse($ocCouponCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $editedoc_coupon_category = $this->fakeoc_coupon_categoryData();

        $this->json('PUT', '/api/v1/ocCouponCategories/'.$ocCouponCategory->id, $editedoc_coupon_category);

        $this->assertApiResponse($editedoc_coupon_category);
    }

    /**
     * @test
     */
    public function testDeleteoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $this->json('DELETE', '/api/v1/ocCouponCategories/'.$ocCouponCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCouponCategories/'.$ocCouponCategory->id);

        $this->assertResponseStatus(404);
    }
}
