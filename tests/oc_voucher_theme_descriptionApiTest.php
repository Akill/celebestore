<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_theme_descriptionApiTest extends TestCase
{
    use Makeoc_voucher_theme_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->fakeoc_voucher_theme_descriptionData();
        $this->json('POST', '/api/v1/ocVoucherThemeDescriptions', $ocVoucherThemeDescription);

        $this->assertApiResponse($ocVoucherThemeDescription);
    }

    /**
     * @test
     */
    public function testReadoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $this->json('GET', '/api/v1/ocVoucherThemeDescriptions/'.$ocVoucherThemeDescription->id);

        $this->assertApiResponse($ocVoucherThemeDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $editedoc_voucher_theme_description = $this->fakeoc_voucher_theme_descriptionData();

        $this->json('PUT', '/api/v1/ocVoucherThemeDescriptions/'.$ocVoucherThemeDescription->id, $editedoc_voucher_theme_description);

        $this->assertApiResponse($editedoc_voucher_theme_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $this->json('DELETE', '/api/v1/ocVoucherThemeDescriptions/'.$ocVoucherThemeDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocVoucherThemeDescriptions/'.$ocVoucherThemeDescription->id);

        $this->assertResponseStatus(404);
    }
}
