<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_historyApiTest extends TestCase
{
    use Makeoc_return_historyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_return_history()
    {
        $ocReturnHistory = $this->fakeoc_return_historyData();
        $this->json('POST', '/api/v1/ocReturnHistories', $ocReturnHistory);

        $this->assertApiResponse($ocReturnHistory);
    }

    /**
     * @test
     */
    public function testReadoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $this->json('GET', '/api/v1/ocReturnHistories/'.$ocReturnHistory->id);

        $this->assertApiResponse($ocReturnHistory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $editedoc_return_history = $this->fakeoc_return_historyData();

        $this->json('PUT', '/api/v1/ocReturnHistories/'.$ocReturnHistory->id, $editedoc_return_history);

        $this->assertApiResponse($editedoc_return_history);
    }

    /**
     * @test
     */
    public function testDeleteoc_return_history()
    {
        $ocReturnHistory = $this->makeoc_return_history();
        $this->json('DELETE', '/api/v1/ocReturnHistories/'.$ocReturnHistory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReturnHistories/'.$ocReturnHistory->id);

        $this->assertResponseStatus(404);
    }
}
