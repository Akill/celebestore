<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_to_storeApiTest extends TestCase
{
    use Makeoc_information_to_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_information_to_store()
    {
        $ocInformationToStore = $this->fakeoc_information_to_storeData();
        $this->json('POST', '/api/v1/ocInformationToStores', $ocInformationToStore);

        $this->assertApiResponse($ocInformationToStore);
    }

    /**
     * @test
     */
    public function testReadoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $this->json('GET', '/api/v1/ocInformationToStores/'.$ocInformationToStore->id);

        $this->assertApiResponse($ocInformationToStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $editedoc_information_to_store = $this->fakeoc_information_to_storeData();

        $this->json('PUT', '/api/v1/ocInformationToStores/'.$ocInformationToStore->id, $editedoc_information_to_store);

        $this->assertApiResponse($editedoc_information_to_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $this->json('DELETE', '/api/v1/ocInformationToStores/'.$ocInformationToStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocInformationToStores/'.$ocInformationToStore->id);

        $this->assertResponseStatus(404);
    }
}
