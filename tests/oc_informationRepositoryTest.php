<?php

use App\Models\oc_information;
use App\Repositories\oc_informationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_informationRepositoryTest extends TestCase
{
    use Makeoc_informationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_informationRepository
     */
    protected $ocInformationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocInformationRepo = App::make(oc_informationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_information()
    {
        $ocInformation = $this->fakeoc_informationData();
        $createdoc_information = $this->ocInformationRepo->create($ocInformation);
        $createdoc_information = $createdoc_information->toArray();
        $this->assertArrayHasKey('id', $createdoc_information);
        $this->assertNotNull($createdoc_information['id'], 'Created oc_information must have id specified');
        $this->assertNotNull(oc_information::find($createdoc_information['id']), 'oc_information with given id must be in DB');
        $this->assertModelData($ocInformation, $createdoc_information);
    }

    /**
     * @test read
     */
    public function testReadoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $dboc_information = $this->ocInformationRepo->find($ocInformation->id);
        $dboc_information = $dboc_information->toArray();
        $this->assertModelData($ocInformation->toArray(), $dboc_information);
    }

    /**
     * @test update
     */
    public function testUpdateoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $fakeoc_information = $this->fakeoc_informationData();
        $updatedoc_information = $this->ocInformationRepo->update($fakeoc_information, $ocInformation->id);
        $this->assertModelData($fakeoc_information, $updatedoc_information->toArray());
        $dboc_information = $this->ocInformationRepo->find($ocInformation->id);
        $this->assertModelData($fakeoc_information, $dboc_information->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_information()
    {
        $ocInformation = $this->makeoc_information();
        $resp = $this->ocInformationRepo->delete($ocInformation->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_information::find($ocInformation->id), 'oc_information should not exist in DB');
    }
}
