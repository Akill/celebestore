<?php

use App\Models\oc_manufacturer_to_store;
use App\Repositories\oc_manufacturer_to_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_manufacturer_to_storeRepositoryTest extends TestCase
{
    use Makeoc_manufacturer_to_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_manufacturer_to_storeRepository
     */
    protected $ocManufacturerToStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocManufacturerToStoreRepo = App::make(oc_manufacturer_to_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->fakeoc_manufacturer_to_storeData();
        $createdoc_manufacturer_to_store = $this->ocManufacturerToStoreRepo->create($ocManufacturerToStore);
        $createdoc_manufacturer_to_store = $createdoc_manufacturer_to_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_manufacturer_to_store);
        $this->assertNotNull($createdoc_manufacturer_to_store['id'], 'Created oc_manufacturer_to_store must have id specified');
        $this->assertNotNull(oc_manufacturer_to_store::find($createdoc_manufacturer_to_store['id']), 'oc_manufacturer_to_store with given id must be in DB');
        $this->assertModelData($ocManufacturerToStore, $createdoc_manufacturer_to_store);
    }

    /**
     * @test read
     */
    public function testReadoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $dboc_manufacturer_to_store = $this->ocManufacturerToStoreRepo->find($ocManufacturerToStore->id);
        $dboc_manufacturer_to_store = $dboc_manufacturer_to_store->toArray();
        $this->assertModelData($ocManufacturerToStore->toArray(), $dboc_manufacturer_to_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $fakeoc_manufacturer_to_store = $this->fakeoc_manufacturer_to_storeData();
        $updatedoc_manufacturer_to_store = $this->ocManufacturerToStoreRepo->update($fakeoc_manufacturer_to_store, $ocManufacturerToStore->id);
        $this->assertModelData($fakeoc_manufacturer_to_store, $updatedoc_manufacturer_to_store->toArray());
        $dboc_manufacturer_to_store = $this->ocManufacturerToStoreRepo->find($ocManufacturerToStore->id);
        $this->assertModelData($fakeoc_manufacturer_to_store, $dboc_manufacturer_to_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_manufacturer_to_store()
    {
        $ocManufacturerToStore = $this->makeoc_manufacturer_to_store();
        $resp = $this->ocManufacturerToStoreRepo->delete($ocManufacturerToStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_manufacturer_to_store::find($ocManufacturerToStore->id), 'oc_manufacturer_to_store should not exist in DB');
    }
}
