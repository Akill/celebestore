<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_classApiTest extends TestCase
{
    use Makeoc_tax_classTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_tax_class()
    {
        $ocTaxClass = $this->fakeoc_tax_classData();
        $this->json('POST', '/api/v1/ocTaxClasses', $ocTaxClass);

        $this->assertApiResponse($ocTaxClass);
    }

    /**
     * @test
     */
    public function testReadoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $this->json('GET', '/api/v1/ocTaxClasses/'.$ocTaxClass->id);

        $this->assertApiResponse($ocTaxClass->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $editedoc_tax_class = $this->fakeoc_tax_classData();

        $this->json('PUT', '/api/v1/ocTaxClasses/'.$ocTaxClass->id, $editedoc_tax_class);

        $this->assertApiResponse($editedoc_tax_class);
    }

    /**
     * @test
     */
    public function testDeleteoc_tax_class()
    {
        $ocTaxClass = $this->makeoc_tax_class();
        $this->json('DELETE', '/api/v1/ocTaxClasses/'.$ocTaxClass->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocTaxClasses/'.$ocTaxClass->id);

        $this->assertResponseStatus(404);
    }
}
