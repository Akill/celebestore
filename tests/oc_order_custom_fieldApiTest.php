<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_custom_fieldApiTest extends TestCase
{
    use Makeoc_order_custom_fieldTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_custom_field()
    {
        $ocOrderCustomField = $this->fakeoc_order_custom_fieldData();
        $this->json('POST', '/api/v1/ocOrderCustomFields', $ocOrderCustomField);

        $this->assertApiResponse($ocOrderCustomField);
    }

    /**
     * @test
     */
    public function testReadoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $this->json('GET', '/api/v1/ocOrderCustomFields/'.$ocOrderCustomField->id);

        $this->assertApiResponse($ocOrderCustomField->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $editedoc_order_custom_field = $this->fakeoc_order_custom_fieldData();

        $this->json('PUT', '/api/v1/ocOrderCustomFields/'.$ocOrderCustomField->id, $editedoc_order_custom_field);

        $this->assertApiResponse($editedoc_order_custom_field);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $this->json('DELETE', '/api/v1/ocOrderCustomFields/'.$ocOrderCustomField->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderCustomFields/'.$ocOrderCustomField->id);

        $this->assertResponseStatus(404);
    }
}
