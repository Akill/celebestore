<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_country_hpwdApiTest extends TestCase
{
    use Makeoc_country_hpwdTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_country_hpwd()
    {
        $ocCountryHpwd = $this->fakeoc_country_hpwdData();
        $this->json('POST', '/api/v1/ocCountryHpwds', $ocCountryHpwd);

        $this->assertApiResponse($ocCountryHpwd);
    }

    /**
     * @test
     */
    public function testReadoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $this->json('GET', '/api/v1/ocCountryHpwds/'.$ocCountryHpwd->id);

        $this->assertApiResponse($ocCountryHpwd->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $editedoc_country_hpwd = $this->fakeoc_country_hpwdData();

        $this->json('PUT', '/api/v1/ocCountryHpwds/'.$ocCountryHpwd->id, $editedoc_country_hpwd);

        $this->assertApiResponse($editedoc_country_hpwd);
    }

    /**
     * @test
     */
    public function testDeleteoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $this->json('DELETE', '/api/v1/ocCountryHpwds/'.$ocCountryHpwd->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCountryHpwds/'.$ocCountryHpwd->id);

        $this->assertResponseStatus(404);
    }
}
