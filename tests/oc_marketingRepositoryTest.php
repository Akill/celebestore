<?php

use App\Models\oc_marketing;
use App\Repositories\oc_marketingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_marketingRepositoryTest extends TestCase
{
    use Makeoc_marketingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_marketingRepository
     */
    protected $ocMarketingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocMarketingRepo = App::make(oc_marketingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_marketing()
    {
        $ocMarketing = $this->fakeoc_marketingData();
        $createdoc_marketing = $this->ocMarketingRepo->create($ocMarketing);
        $createdoc_marketing = $createdoc_marketing->toArray();
        $this->assertArrayHasKey('id', $createdoc_marketing);
        $this->assertNotNull($createdoc_marketing['id'], 'Created oc_marketing must have id specified');
        $this->assertNotNull(oc_marketing::find($createdoc_marketing['id']), 'oc_marketing with given id must be in DB');
        $this->assertModelData($ocMarketing, $createdoc_marketing);
    }

    /**
     * @test read
     */
    public function testReadoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $dboc_marketing = $this->ocMarketingRepo->find($ocMarketing->id);
        $dboc_marketing = $dboc_marketing->toArray();
        $this->assertModelData($ocMarketing->toArray(), $dboc_marketing);
    }

    /**
     * @test update
     */
    public function testUpdateoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $fakeoc_marketing = $this->fakeoc_marketingData();
        $updatedoc_marketing = $this->ocMarketingRepo->update($fakeoc_marketing, $ocMarketing->id);
        $this->assertModelData($fakeoc_marketing, $updatedoc_marketing->toArray());
        $dboc_marketing = $this->ocMarketingRepo->find($ocMarketing->id);
        $this->assertModelData($fakeoc_marketing, $dboc_marketing->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_marketing()
    {
        $ocMarketing = $this->makeoc_marketing();
        $resp = $this->ocMarketingRepo->delete($ocMarketing->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_marketing::find($ocMarketing->id), 'oc_marketing should not exist in DB');
    }
}
