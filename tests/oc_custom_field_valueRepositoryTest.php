<?php

use App\Models\oc_custom_field_value;
use App\Repositories\oc_custom_field_valueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_valueRepositoryTest extends TestCase
{
    use Makeoc_custom_field_valueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_custom_field_valueRepository
     */
    protected $ocCustomFieldValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomFieldValueRepo = App::make(oc_custom_field_valueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->fakeoc_custom_field_valueData();
        $createdoc_custom_field_value = $this->ocCustomFieldValueRepo->create($ocCustomFieldValue);
        $createdoc_custom_field_value = $createdoc_custom_field_value->toArray();
        $this->assertArrayHasKey('id', $createdoc_custom_field_value);
        $this->assertNotNull($createdoc_custom_field_value['id'], 'Created oc_custom_field_value must have id specified');
        $this->assertNotNull(oc_custom_field_value::find($createdoc_custom_field_value['id']), 'oc_custom_field_value with given id must be in DB');
        $this->assertModelData($ocCustomFieldValue, $createdoc_custom_field_value);
    }

    /**
     * @test read
     */
    public function testReadoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $dboc_custom_field_value = $this->ocCustomFieldValueRepo->find($ocCustomFieldValue->id);
        $dboc_custom_field_value = $dboc_custom_field_value->toArray();
        $this->assertModelData($ocCustomFieldValue->toArray(), $dboc_custom_field_value);
    }

    /**
     * @test update
     */
    public function testUpdateoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $fakeoc_custom_field_value = $this->fakeoc_custom_field_valueData();
        $updatedoc_custom_field_value = $this->ocCustomFieldValueRepo->update($fakeoc_custom_field_value, $ocCustomFieldValue->id);
        $this->assertModelData($fakeoc_custom_field_value, $updatedoc_custom_field_value->toArray());
        $dboc_custom_field_value = $this->ocCustomFieldValueRepo->find($ocCustomFieldValue->id);
        $this->assertModelData($fakeoc_custom_field_value, $dboc_custom_field_value->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_custom_field_value()
    {
        $ocCustomFieldValue = $this->makeoc_custom_field_value();
        $resp = $this->ocCustomFieldValueRepo->delete($ocCustomFieldValue->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_custom_field_value::find($ocCustomFieldValue->id), 'oc_custom_field_value should not exist in DB');
    }
}
