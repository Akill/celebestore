<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_geo_zoneApiTest extends TestCase
{
    use Makeoc_geo_zoneTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_geo_zone()
    {
        $ocGeoZone = $this->fakeoc_geo_zoneData();
        $this->json('POST', '/api/v1/ocGeoZones', $ocGeoZone);

        $this->assertApiResponse($ocGeoZone);
    }

    /**
     * @test
     */
    public function testReadoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $this->json('GET', '/api/v1/ocGeoZones/'.$ocGeoZone->id);

        $this->assertApiResponse($ocGeoZone->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $editedoc_geo_zone = $this->fakeoc_geo_zoneData();

        $this->json('PUT', '/api/v1/ocGeoZones/'.$ocGeoZone->id, $editedoc_geo_zone);

        $this->assertApiResponse($editedoc_geo_zone);
    }

    /**
     * @test
     */
    public function testDeleteoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $this->json('DELETE', '/api/v1/ocGeoZones/'.$ocGeoZone->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocGeoZones/'.$ocGeoZone->id);

        $this->assertResponseStatus(404);
    }
}
