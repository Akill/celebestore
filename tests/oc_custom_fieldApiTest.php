<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_fieldApiTest extends TestCase
{
    use Makeoc_custom_fieldTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_custom_field()
    {
        $ocCustomField = $this->fakeoc_custom_fieldData();
        $this->json('POST', '/api/v1/ocCustomFields', $ocCustomField);

        $this->assertApiResponse($ocCustomField);
    }

    /**
     * @test
     */
    public function testReadoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $this->json('GET', '/api/v1/ocCustomFields/'.$ocCustomField->id);

        $this->assertApiResponse($ocCustomField->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $editedoc_custom_field = $this->fakeoc_custom_fieldData();

        $this->json('PUT', '/api/v1/ocCustomFields/'.$ocCustomField->id, $editedoc_custom_field);

        $this->assertApiResponse($editedoc_custom_field);
    }

    /**
     * @test
     */
    public function testDeleteoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $this->json('DELETE', '/api/v1/ocCustomFields/'.$ocCustomField->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomFields/'.$ocCustomField->id);

        $this->assertResponseStatus(404);
    }
}
