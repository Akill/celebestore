<?php

use App\Models\oc_attribute;
use App\Repositories\oc_attributeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attributeRepositoryTest extends TestCase
{
    use Makeoc_attributeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_attributeRepository
     */
    protected $ocAttributeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAttributeRepo = App::make(oc_attributeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_attribute()
    {
        $ocAttribute = $this->fakeoc_attributeData();
        $createdoc_attribute = $this->ocAttributeRepo->create($ocAttribute);
        $createdoc_attribute = $createdoc_attribute->toArray();
        $this->assertArrayHasKey('id', $createdoc_attribute);
        $this->assertNotNull($createdoc_attribute['id'], 'Created oc_attribute must have id specified');
        $this->assertNotNull(oc_attribute::find($createdoc_attribute['id']), 'oc_attribute with given id must be in DB');
        $this->assertModelData($ocAttribute, $createdoc_attribute);
    }

    /**
     * @test read
     */
    public function testReadoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $dboc_attribute = $this->ocAttributeRepo->find($ocAttribute->id);
        $dboc_attribute = $dboc_attribute->toArray();
        $this->assertModelData($ocAttribute->toArray(), $dboc_attribute);
    }

    /**
     * @test update
     */
    public function testUpdateoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $fakeoc_attribute = $this->fakeoc_attributeData();
        $updatedoc_attribute = $this->ocAttributeRepo->update($fakeoc_attribute, $ocAttribute->id);
        $this->assertModelData($fakeoc_attribute, $updatedoc_attribute->toArray());
        $dboc_attribute = $this->ocAttributeRepo->find($ocAttribute->id);
        $this->assertModelData($fakeoc_attribute, $dboc_attribute->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_attribute()
    {
        $ocAttribute = $this->makeoc_attribute();
        $resp = $this->ocAttributeRepo->delete($ocAttribute->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_attribute::find($ocAttribute->id), 'oc_attribute should not exist in DB');
    }
}
