<?php

use App\Models\oc_coupon_category;
use App\Repositories\oc_coupon_categoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_categoryRepositoryTest extends TestCase
{
    use Makeoc_coupon_categoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_coupon_categoryRepository
     */
    protected $ocCouponCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCouponCategoryRepo = App::make(oc_coupon_categoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_coupon_category()
    {
        $ocCouponCategory = $this->fakeoc_coupon_categoryData();
        $createdoc_coupon_category = $this->ocCouponCategoryRepo->create($ocCouponCategory);
        $createdoc_coupon_category = $createdoc_coupon_category->toArray();
        $this->assertArrayHasKey('id', $createdoc_coupon_category);
        $this->assertNotNull($createdoc_coupon_category['id'], 'Created oc_coupon_category must have id specified');
        $this->assertNotNull(oc_coupon_category::find($createdoc_coupon_category['id']), 'oc_coupon_category with given id must be in DB');
        $this->assertModelData($ocCouponCategory, $createdoc_coupon_category);
    }

    /**
     * @test read
     */
    public function testReadoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $dboc_coupon_category = $this->ocCouponCategoryRepo->find($ocCouponCategory->id);
        $dboc_coupon_category = $dboc_coupon_category->toArray();
        $this->assertModelData($ocCouponCategory->toArray(), $dboc_coupon_category);
    }

    /**
     * @test update
     */
    public function testUpdateoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $fakeoc_coupon_category = $this->fakeoc_coupon_categoryData();
        $updatedoc_coupon_category = $this->ocCouponCategoryRepo->update($fakeoc_coupon_category, $ocCouponCategory->id);
        $this->assertModelData($fakeoc_coupon_category, $updatedoc_coupon_category->toArray());
        $dboc_coupon_category = $this->ocCouponCategoryRepo->find($ocCouponCategory->id);
        $this->assertModelData($fakeoc_coupon_category, $dboc_coupon_category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_coupon_category()
    {
        $ocCouponCategory = $this->makeoc_coupon_category();
        $resp = $this->ocCouponCategoryRepo->delete($ocCouponCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_coupon_category::find($ocCouponCategory->id), 'oc_coupon_category should not exist in DB');
    }
}
