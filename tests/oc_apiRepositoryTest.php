<?php

use App\Models\oc_api;
use App\Repositories\oc_apiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_apiRepositoryTest extends TestCase
{
    use Makeoc_apiTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_apiRepository
     */
    protected $ocApiRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocApiRepo = App::make(oc_apiRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_api()
    {
        $ocApi = $this->fakeoc_apiData();
        $createdoc_api = $this->ocApiRepo->create($ocApi);
        $createdoc_api = $createdoc_api->toArray();
        $this->assertArrayHasKey('id', $createdoc_api);
        $this->assertNotNull($createdoc_api['id'], 'Created oc_api must have id specified');
        $this->assertNotNull(oc_api::find($createdoc_api['id']), 'oc_api with given id must be in DB');
        $this->assertModelData($ocApi, $createdoc_api);
    }

    /**
     * @test read
     */
    public function testReadoc_api()
    {
        $ocApi = $this->makeoc_api();
        $dboc_api = $this->ocApiRepo->find($ocApi->id);
        $dboc_api = $dboc_api->toArray();
        $this->assertModelData($ocApi->toArray(), $dboc_api);
    }

    /**
     * @test update
     */
    public function testUpdateoc_api()
    {
        $ocApi = $this->makeoc_api();
        $fakeoc_api = $this->fakeoc_apiData();
        $updatedoc_api = $this->ocApiRepo->update($fakeoc_api, $ocApi->id);
        $this->assertModelData($fakeoc_api, $updatedoc_api->toArray());
        $dboc_api = $this->ocApiRepo->find($ocApi->id);
        $this->assertModelData($fakeoc_api, $dboc_api->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_api()
    {
        $ocApi = $this->makeoc_api();
        $resp = $this->ocApiRepo->delete($ocApi->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_api::find($ocApi->id), 'oc_api should not exist in DB');
    }
}
