<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_group_descriptionApiTest extends TestCase
{
    use Makeoc_filter_group_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->fakeoc_filter_group_descriptionData();
        $this->json('POST', '/api/v1/ocFilterGroupDescriptions', $ocFilterGroupDescription);

        $this->assertApiResponse($ocFilterGroupDescription);
    }

    /**
     * @test
     */
    public function testReadoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $this->json('GET', '/api/v1/ocFilterGroupDescriptions/'.$ocFilterGroupDescription->id);

        $this->assertApiResponse($ocFilterGroupDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $editedoc_filter_group_description = $this->fakeoc_filter_group_descriptionData();

        $this->json('PUT', '/api/v1/ocFilterGroupDescriptions/'.$ocFilterGroupDescription->id, $editedoc_filter_group_description);

        $this->assertApiResponse($editedoc_filter_group_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $this->json('DELETE', '/api/v1/ocFilterGroupDescriptions/'.$ocFilterGroupDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocFilterGroupDescriptions/'.$ocFilterGroupDescription->id);

        $this->assertResponseStatus(404);
    }
}
