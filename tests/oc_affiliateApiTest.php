<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliateApiTest extends TestCase
{
    use Makeoc_affiliateTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_affiliate()
    {
        $ocAffiliate = $this->fakeoc_affiliateData();
        $this->json('POST', '/api/v1/ocAffiliates', $ocAffiliate);

        $this->assertApiResponse($ocAffiliate);
    }

    /**
     * @test
     */
    public function testReadoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $this->json('GET', '/api/v1/ocAffiliates/'.$ocAffiliate->id);

        $this->assertApiResponse($ocAffiliate->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $editedoc_affiliate = $this->fakeoc_affiliateData();

        $this->json('PUT', '/api/v1/ocAffiliates/'.$ocAffiliate->id, $editedoc_affiliate);

        $this->assertApiResponse($editedoc_affiliate);
    }

    /**
     * @test
     */
    public function testDeleteoc_affiliate()
    {
        $ocAffiliate = $this->makeoc_affiliate();
        $this->json('DELETE', '/api/v1/ocAffiliates/'.$ocAffiliate->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAffiliates/'.$ocAffiliate->id);

        $this->assertResponseStatus(404);
    }
}
