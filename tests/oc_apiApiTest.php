<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_apiApiTest extends TestCase
{
    use Makeoc_apiTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_api()
    {
        $ocApi = $this->fakeoc_apiData();
        $this->json('POST', '/api/v1/ocApis', $ocApi);

        $this->assertApiResponse($ocApi);
    }

    /**
     * @test
     */
    public function testReadoc_api()
    {
        $ocApi = $this->makeoc_api();
        $this->json('GET', '/api/v1/ocApis/'.$ocApi->id);

        $this->assertApiResponse($ocApi->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_api()
    {
        $ocApi = $this->makeoc_api();
        $editedoc_api = $this->fakeoc_apiData();

        $this->json('PUT', '/api/v1/ocApis/'.$ocApi->id, $editedoc_api);

        $this->assertApiResponse($editedoc_api);
    }

    /**
     * @test
     */
    public function testDeleteoc_api()
    {
        $ocApi = $this->makeoc_api();
        $this->json('DELETE', '/api/v1/ocApis/'.$ocApi->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocApis/'.$ocApi->id);

        $this->assertResponseStatus(404);
    }
}
