<?php

use App\Models\oc_confirm;
use App\Repositories\oc_confirmRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_confirmRepositoryTest extends TestCase
{
    use Makeoc_confirmTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_confirmRepository
     */
    protected $ocConfirmRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocConfirmRepo = App::make(oc_confirmRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_confirm()
    {
        $ocConfirm = $this->fakeoc_confirmData();
        $createdoc_confirm = $this->ocConfirmRepo->create($ocConfirm);
        $createdoc_confirm = $createdoc_confirm->toArray();
        $this->assertArrayHasKey('id', $createdoc_confirm);
        $this->assertNotNull($createdoc_confirm['id'], 'Created oc_confirm must have id specified');
        $this->assertNotNull(oc_confirm::find($createdoc_confirm['id']), 'oc_confirm with given id must be in DB');
        $this->assertModelData($ocConfirm, $createdoc_confirm);
    }

    /**
     * @test read
     */
    public function testReadoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $dboc_confirm = $this->ocConfirmRepo->find($ocConfirm->id);
        $dboc_confirm = $dboc_confirm->toArray();
        $this->assertModelData($ocConfirm->toArray(), $dboc_confirm);
    }

    /**
     * @test update
     */
    public function testUpdateoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $fakeoc_confirm = $this->fakeoc_confirmData();
        $updatedoc_confirm = $this->ocConfirmRepo->update($fakeoc_confirm, $ocConfirm->id);
        $this->assertModelData($fakeoc_confirm, $updatedoc_confirm->toArray());
        $dboc_confirm = $this->ocConfirmRepo->find($ocConfirm->id);
        $this->assertModelData($fakeoc_confirm, $dboc_confirm->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $resp = $this->ocConfirmRepo->delete($ocConfirm->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_confirm::find($ocConfirm->id), 'oc_confirm should not exist in DB');
    }
}
