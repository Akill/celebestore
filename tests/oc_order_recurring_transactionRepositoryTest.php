<?php

use App\Models\oc_order_recurring_transaction;
use App\Repositories\oc_order_recurring_transactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_recurring_transactionRepositoryTest extends TestCase
{
    use Makeoc_order_recurring_transactionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_recurring_transactionRepository
     */
    protected $ocOrderRecurringTransactionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderRecurringTransactionRepo = App::make(oc_order_recurring_transactionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->fakeoc_order_recurring_transactionData();
        $createdoc_order_recurring_transaction = $this->ocOrderRecurringTransactionRepo->create($ocOrderRecurringTransaction);
        $createdoc_order_recurring_transaction = $createdoc_order_recurring_transaction->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_recurring_transaction);
        $this->assertNotNull($createdoc_order_recurring_transaction['id'], 'Created oc_order_recurring_transaction must have id specified');
        $this->assertNotNull(oc_order_recurring_transaction::find($createdoc_order_recurring_transaction['id']), 'oc_order_recurring_transaction with given id must be in DB');
        $this->assertModelData($ocOrderRecurringTransaction, $createdoc_order_recurring_transaction);
    }

    /**
     * @test read
     */
    public function testReadoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $dboc_order_recurring_transaction = $this->ocOrderRecurringTransactionRepo->find($ocOrderRecurringTransaction->id);
        $dboc_order_recurring_transaction = $dboc_order_recurring_transaction->toArray();
        $this->assertModelData($ocOrderRecurringTransaction->toArray(), $dboc_order_recurring_transaction);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $fakeoc_order_recurring_transaction = $this->fakeoc_order_recurring_transactionData();
        $updatedoc_order_recurring_transaction = $this->ocOrderRecurringTransactionRepo->update($fakeoc_order_recurring_transaction, $ocOrderRecurringTransaction->id);
        $this->assertModelData($fakeoc_order_recurring_transaction, $updatedoc_order_recurring_transaction->toArray());
        $dboc_order_recurring_transaction = $this->ocOrderRecurringTransactionRepo->find($ocOrderRecurringTransaction->id);
        $this->assertModelData($fakeoc_order_recurring_transaction, $dboc_order_recurring_transaction->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_recurring_transaction()
    {
        $ocOrderRecurringTransaction = $this->makeoc_order_recurring_transaction();
        $resp = $this->ocOrderRecurringTransactionRepo->delete($ocOrderRecurringTransaction->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_recurring_transaction::find($ocOrderRecurringTransaction->id), 'oc_order_recurring_transaction should not exist in DB');
    }
}
