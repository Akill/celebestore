<?php

use App\Models\oc_voucher_theme_description;
use App\Repositories\oc_voucher_theme_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_theme_descriptionRepositoryTest extends TestCase
{
    use Makeoc_voucher_theme_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_voucher_theme_descriptionRepository
     */
    protected $ocVoucherThemeDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocVoucherThemeDescriptionRepo = App::make(oc_voucher_theme_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->fakeoc_voucher_theme_descriptionData();
        $createdoc_voucher_theme_description = $this->ocVoucherThemeDescriptionRepo->create($ocVoucherThemeDescription);
        $createdoc_voucher_theme_description = $createdoc_voucher_theme_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_voucher_theme_description);
        $this->assertNotNull($createdoc_voucher_theme_description['id'], 'Created oc_voucher_theme_description must have id specified');
        $this->assertNotNull(oc_voucher_theme_description::find($createdoc_voucher_theme_description['id']), 'oc_voucher_theme_description with given id must be in DB');
        $this->assertModelData($ocVoucherThemeDescription, $createdoc_voucher_theme_description);
    }

    /**
     * @test read
     */
    public function testReadoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $dboc_voucher_theme_description = $this->ocVoucherThemeDescriptionRepo->find($ocVoucherThemeDescription->id);
        $dboc_voucher_theme_description = $dboc_voucher_theme_description->toArray();
        $this->assertModelData($ocVoucherThemeDescription->toArray(), $dboc_voucher_theme_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $fakeoc_voucher_theme_description = $this->fakeoc_voucher_theme_descriptionData();
        $updatedoc_voucher_theme_description = $this->ocVoucherThemeDescriptionRepo->update($fakeoc_voucher_theme_description, $ocVoucherThemeDescription->id);
        $this->assertModelData($fakeoc_voucher_theme_description, $updatedoc_voucher_theme_description->toArray());
        $dboc_voucher_theme_description = $this->ocVoucherThemeDescriptionRepo->find($ocVoucherThemeDescription->id);
        $this->assertModelData($fakeoc_voucher_theme_description, $dboc_voucher_theme_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_voucher_theme_description()
    {
        $ocVoucherThemeDescription = $this->makeoc_voucher_theme_description();
        $resp = $this->ocVoucherThemeDescriptionRepo->delete($ocVoucherThemeDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_voucher_theme_description::find($ocVoucherThemeDescription->id), 'oc_voucher_theme_description should not exist in DB');
    }
}
