<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_filterApiTest extends TestCase
{
    use Makeoc_product_filterTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_filter()
    {
        $ocProductFilter = $this->fakeoc_product_filterData();
        $this->json('POST', '/api/v1/ocProductFilters', $ocProductFilter);

        $this->assertApiResponse($ocProductFilter);
    }

    /**
     * @test
     */
    public function testReadoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $this->json('GET', '/api/v1/ocProductFilters/'.$ocProductFilter->id);

        $this->assertApiResponse($ocProductFilter->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $editedoc_product_filter = $this->fakeoc_product_filterData();

        $this->json('PUT', '/api/v1/ocProductFilters/'.$ocProductFilter->id, $editedoc_product_filter);

        $this->assertApiResponse($editedoc_product_filter);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $this->json('DELETE', '/api/v1/ocProductFilters/'.$ocProductFilter->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductFilters/'.$ocProductFilter->id);

        $this->assertResponseStatus(404);
    }
}
