<?php

use App\Models\oc_recurring_description;
use App\Repositories\oc_recurring_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_recurring_descriptionRepositoryTest extends TestCase
{
    use Makeoc_recurring_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_recurring_descriptionRepository
     */
    protected $ocRecurringDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocRecurringDescriptionRepo = App::make(oc_recurring_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_recurring_description()
    {
        $ocRecurringDescription = $this->fakeoc_recurring_descriptionData();
        $createdoc_recurring_description = $this->ocRecurringDescriptionRepo->create($ocRecurringDescription);
        $createdoc_recurring_description = $createdoc_recurring_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_recurring_description);
        $this->assertNotNull($createdoc_recurring_description['id'], 'Created oc_recurring_description must have id specified');
        $this->assertNotNull(oc_recurring_description::find($createdoc_recurring_description['id']), 'oc_recurring_description with given id must be in DB');
        $this->assertModelData($ocRecurringDescription, $createdoc_recurring_description);
    }

    /**
     * @test read
     */
    public function testReadoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $dboc_recurring_description = $this->ocRecurringDescriptionRepo->find($ocRecurringDescription->id);
        $dboc_recurring_description = $dboc_recurring_description->toArray();
        $this->assertModelData($ocRecurringDescription->toArray(), $dboc_recurring_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $fakeoc_recurring_description = $this->fakeoc_recurring_descriptionData();
        $updatedoc_recurring_description = $this->ocRecurringDescriptionRepo->update($fakeoc_recurring_description, $ocRecurringDescription->id);
        $this->assertModelData($fakeoc_recurring_description, $updatedoc_recurring_description->toArray());
        $dboc_recurring_description = $this->ocRecurringDescriptionRepo->find($ocRecurringDescription->id);
        $this->assertModelData($fakeoc_recurring_description, $dboc_recurring_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $resp = $this->ocRecurringDescriptionRepo->delete($ocRecurringDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_recurring_description::find($ocRecurringDescription->id), 'oc_recurring_description should not exist in DB');
    }
}
