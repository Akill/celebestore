<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_locationApiTest extends TestCase
{
    use Makeoc_locationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_location()
    {
        $ocLocation = $this->fakeoc_locationData();
        $this->json('POST', '/api/v1/ocLocations', $ocLocation);

        $this->assertApiResponse($ocLocation);
    }

    /**
     * @test
     */
    public function testReadoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $this->json('GET', '/api/v1/ocLocations/'.$ocLocation->id);

        $this->assertApiResponse($ocLocation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $editedoc_location = $this->fakeoc_locationData();

        $this->json('PUT', '/api/v1/ocLocations/'.$ocLocation->id, $editedoc_location);

        $this->assertApiResponse($editedoc_location);
    }

    /**
     * @test
     */
    public function testDeleteoc_location()
    {
        $ocLocation = $this->makeoc_location();
        $this->json('DELETE', '/api/v1/ocLocations/'.$ocLocation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLocations/'.$ocLocation->id);

        $this->assertResponseStatus(404);
    }
}
