<?php

use App\Models\oc_attribute_description;
use App\Repositories\oc_attribute_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_descriptionRepositoryTest extends TestCase
{
    use Makeoc_attribute_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_attribute_descriptionRepository
     */
    protected $ocAttributeDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAttributeDescriptionRepo = App::make(oc_attribute_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_attribute_description()
    {
        $ocAttributeDescription = $this->fakeoc_attribute_descriptionData();
        $createdoc_attribute_description = $this->ocAttributeDescriptionRepo->create($ocAttributeDescription);
        $createdoc_attribute_description = $createdoc_attribute_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_attribute_description);
        $this->assertNotNull($createdoc_attribute_description['id'], 'Created oc_attribute_description must have id specified');
        $this->assertNotNull(oc_attribute_description::find($createdoc_attribute_description['id']), 'oc_attribute_description with given id must be in DB');
        $this->assertModelData($ocAttributeDescription, $createdoc_attribute_description);
    }

    /**
     * @test read
     */
    public function testReadoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $dboc_attribute_description = $this->ocAttributeDescriptionRepo->find($ocAttributeDescription->id);
        $dboc_attribute_description = $dboc_attribute_description->toArray();
        $this->assertModelData($ocAttributeDescription->toArray(), $dboc_attribute_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $fakeoc_attribute_description = $this->fakeoc_attribute_descriptionData();
        $updatedoc_attribute_description = $this->ocAttributeDescriptionRepo->update($fakeoc_attribute_description, $ocAttributeDescription->id);
        $this->assertModelData($fakeoc_attribute_description, $updatedoc_attribute_description->toArray());
        $dboc_attribute_description = $this->ocAttributeDescriptionRepo->find($ocAttributeDescription->id);
        $this->assertModelData($fakeoc_attribute_description, $dboc_attribute_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $resp = $this->ocAttributeDescriptionRepo->delete($ocAttributeDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_attribute_description::find($ocAttributeDescription->id), 'oc_attribute_description should not exist in DB');
    }
}
