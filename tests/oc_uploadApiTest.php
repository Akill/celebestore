<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_uploadApiTest extends TestCase
{
    use Makeoc_uploadTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_upload()
    {
        $ocUpload = $this->fakeoc_uploadData();
        $this->json('POST', '/api/v1/ocUploads', $ocUpload);

        $this->assertApiResponse($ocUpload);
    }

    /**
     * @test
     */
    public function testReadoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $this->json('GET', '/api/v1/ocUploads/'.$ocUpload->id);

        $this->assertApiResponse($ocUpload->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $editedoc_upload = $this->fakeoc_uploadData();

        $this->json('PUT', '/api/v1/ocUploads/'.$ocUpload->id, $editedoc_upload);

        $this->assertApiResponse($editedoc_upload);
    }

    /**
     * @test
     */
    public function testDeleteoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $this->json('DELETE', '/api/v1/ocUploads/'.$ocUpload->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocUploads/'.$ocUpload->id);

        $this->assertResponseStatus(404);
    }
}
