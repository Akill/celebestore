<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_length_class_descriptionApiTest extends TestCase
{
    use Makeoc_length_class_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_length_class_description()
    {
        $ocLengthClassDescription = $this->fakeoc_length_class_descriptionData();
        $this->json('POST', '/api/v1/ocLengthClassDescriptions', $ocLengthClassDescription);

        $this->assertApiResponse($ocLengthClassDescription);
    }

    /**
     * @test
     */
    public function testReadoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $this->json('GET', '/api/v1/ocLengthClassDescriptions/'.$ocLengthClassDescription->id);

        $this->assertApiResponse($ocLengthClassDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $editedoc_length_class_description = $this->fakeoc_length_class_descriptionData();

        $this->json('PUT', '/api/v1/ocLengthClassDescriptions/'.$ocLengthClassDescription->id, $editedoc_length_class_description);

        $this->assertApiResponse($editedoc_length_class_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $this->json('DELETE', '/api/v1/ocLengthClassDescriptions/'.$ocLengthClassDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLengthClassDescriptions/'.$ocLengthClassDescription->id);

        $this->assertResponseStatus(404);
    }
}
