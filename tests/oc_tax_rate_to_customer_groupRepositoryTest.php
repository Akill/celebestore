<?php

use App\Models\oc_tax_rate_to_customer_group;
use App\Repositories\oc_tax_rate_to_customer_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_rate_to_customer_groupRepositoryTest extends TestCase
{
    use Makeoc_tax_rate_to_customer_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_tax_rate_to_customer_groupRepository
     */
    protected $ocTaxRateToCustomerGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocTaxRateToCustomerGroupRepo = App::make(oc_tax_rate_to_customer_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->fakeoc_tax_rate_to_customer_groupData();
        $createdoc_tax_rate_to_customer_group = $this->ocTaxRateToCustomerGroupRepo->create($ocTaxRateToCustomerGroup);
        $createdoc_tax_rate_to_customer_group = $createdoc_tax_rate_to_customer_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_tax_rate_to_customer_group);
        $this->assertNotNull($createdoc_tax_rate_to_customer_group['id'], 'Created oc_tax_rate_to_customer_group must have id specified');
        $this->assertNotNull(oc_tax_rate_to_customer_group::find($createdoc_tax_rate_to_customer_group['id']), 'oc_tax_rate_to_customer_group with given id must be in DB');
        $this->assertModelData($ocTaxRateToCustomerGroup, $createdoc_tax_rate_to_customer_group);
    }

    /**
     * @test read
     */
    public function testReadoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $dboc_tax_rate_to_customer_group = $this->ocTaxRateToCustomerGroupRepo->find($ocTaxRateToCustomerGroup->id);
        $dboc_tax_rate_to_customer_group = $dboc_tax_rate_to_customer_group->toArray();
        $this->assertModelData($ocTaxRateToCustomerGroup->toArray(), $dboc_tax_rate_to_customer_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $fakeoc_tax_rate_to_customer_group = $this->fakeoc_tax_rate_to_customer_groupData();
        $updatedoc_tax_rate_to_customer_group = $this->ocTaxRateToCustomerGroupRepo->update($fakeoc_tax_rate_to_customer_group, $ocTaxRateToCustomerGroup->id);
        $this->assertModelData($fakeoc_tax_rate_to_customer_group, $updatedoc_tax_rate_to_customer_group->toArray());
        $dboc_tax_rate_to_customer_group = $this->ocTaxRateToCustomerGroupRepo->find($ocTaxRateToCustomerGroup->id);
        $this->assertModelData($fakeoc_tax_rate_to_customer_group, $dboc_tax_rate_to_customer_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $resp = $this->ocTaxRateToCustomerGroupRepo->delete($ocTaxRateToCustomerGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_tax_rate_to_customer_group::find($ocTaxRateToCustomerGroup->id), 'oc_tax_rate_to_customer_group should not exist in DB');
    }
}
