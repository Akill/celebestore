<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_ipApiTest extends TestCase
{
    use Makeoc_customer_ipTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_ip()
    {
        $ocCustomerIp = $this->fakeoc_customer_ipData();
        $this->json('POST', '/api/v1/ocCustomerIps', $ocCustomerIp);

        $this->assertApiResponse($ocCustomerIp);
    }

    /**
     * @test
     */
    public function testReadoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $this->json('GET', '/api/v1/ocCustomerIps/'.$ocCustomerIp->id);

        $this->assertApiResponse($ocCustomerIp->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $editedoc_customer_ip = $this->fakeoc_customer_ipData();

        $this->json('PUT', '/api/v1/ocCustomerIps/'.$ocCustomerIp->id, $editedoc_customer_ip);

        $this->assertApiResponse($editedoc_customer_ip);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_ip()
    {
        $ocCustomerIp = $this->makeoc_customer_ip();
        $this->json('DELETE', '/api/v1/ocCustomerIps/'.$ocCustomerIp->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerIps/'.$ocCustomerIp->id);

        $this->assertResponseStatus(404);
    }
}
