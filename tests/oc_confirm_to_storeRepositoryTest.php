<?php

use App\Models\oc_confirm_to_store;
use App\Repositories\oc_confirm_to_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_confirm_to_storeRepositoryTest extends TestCase
{
    use Makeoc_confirm_to_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_confirm_to_storeRepository
     */
    protected $ocConfirmToStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocConfirmToStoreRepo = App::make(oc_confirm_to_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->fakeoc_confirm_to_storeData();
        $createdoc_confirm_to_store = $this->ocConfirmToStoreRepo->create($ocConfirmToStore);
        $createdoc_confirm_to_store = $createdoc_confirm_to_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_confirm_to_store);
        $this->assertNotNull($createdoc_confirm_to_store['id'], 'Created oc_confirm_to_store must have id specified');
        $this->assertNotNull(oc_confirm_to_store::find($createdoc_confirm_to_store['id']), 'oc_confirm_to_store with given id must be in DB');
        $this->assertModelData($ocConfirmToStore, $createdoc_confirm_to_store);
    }

    /**
     * @test read
     */
    public function testReadoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $dboc_confirm_to_store = $this->ocConfirmToStoreRepo->find($ocConfirmToStore->id);
        $dboc_confirm_to_store = $dboc_confirm_to_store->toArray();
        $this->assertModelData($ocConfirmToStore->toArray(), $dboc_confirm_to_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $fakeoc_confirm_to_store = $this->fakeoc_confirm_to_storeData();
        $updatedoc_confirm_to_store = $this->ocConfirmToStoreRepo->update($fakeoc_confirm_to_store, $ocConfirmToStore->id);
        $this->assertModelData($fakeoc_confirm_to_store, $updatedoc_confirm_to_store->toArray());
        $dboc_confirm_to_store = $this->ocConfirmToStoreRepo->find($ocConfirmToStore->id);
        $this->assertModelData($fakeoc_confirm_to_store, $dboc_confirm_to_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $resp = $this->ocConfirmToStoreRepo->delete($ocConfirmToStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_confirm_to_store::find($ocConfirmToStore->id), 'oc_confirm_to_store should not exist in DB');
    }
}
