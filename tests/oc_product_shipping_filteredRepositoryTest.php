<?php

use App\Models\oc_product_shipping_filtered;
use App\Repositories\oc_product_shipping_filteredRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_shipping_filteredRepositoryTest extends TestCase
{
    use Makeoc_product_shipping_filteredTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_shipping_filteredRepository
     */
    protected $ocProductShippingFilteredRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductShippingFilteredRepo = App::make(oc_product_shipping_filteredRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->fakeoc_product_shipping_filteredData();
        $createdoc_product_shipping_filtered = $this->ocProductShippingFilteredRepo->create($ocProductShippingFiltered);
        $createdoc_product_shipping_filtered = $createdoc_product_shipping_filtered->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_shipping_filtered);
        $this->assertNotNull($createdoc_product_shipping_filtered['id'], 'Created oc_product_shipping_filtered must have id specified');
        $this->assertNotNull(oc_product_shipping_filtered::find($createdoc_product_shipping_filtered['id']), 'oc_product_shipping_filtered with given id must be in DB');
        $this->assertModelData($ocProductShippingFiltered, $createdoc_product_shipping_filtered);
    }

    /**
     * @test read
     */
    public function testReadoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $dboc_product_shipping_filtered = $this->ocProductShippingFilteredRepo->find($ocProductShippingFiltered->id);
        $dboc_product_shipping_filtered = $dboc_product_shipping_filtered->toArray();
        $this->assertModelData($ocProductShippingFiltered->toArray(), $dboc_product_shipping_filtered);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $fakeoc_product_shipping_filtered = $this->fakeoc_product_shipping_filteredData();
        $updatedoc_product_shipping_filtered = $this->ocProductShippingFilteredRepo->update($fakeoc_product_shipping_filtered, $ocProductShippingFiltered->id);
        $this->assertModelData($fakeoc_product_shipping_filtered, $updatedoc_product_shipping_filtered->toArray());
        $dboc_product_shipping_filtered = $this->ocProductShippingFilteredRepo->find($ocProductShippingFiltered->id);
        $this->assertModelData($fakeoc_product_shipping_filtered, $dboc_product_shipping_filtered->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $resp = $this->ocProductShippingFilteredRepo->delete($ocProductShippingFiltered->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_shipping_filtered::find($ocProductShippingFiltered->id), 'oc_product_shipping_filtered should not exist in DB');
    }
}
