<?php

use App\Models\oc_download;
use App\Repositories\oc_downloadRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_downloadRepositoryTest extends TestCase
{
    use Makeoc_downloadTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_downloadRepository
     */
    protected $ocDownloadRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocDownloadRepo = App::make(oc_downloadRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_download()
    {
        $ocDownload = $this->fakeoc_downloadData();
        $createdoc_download = $this->ocDownloadRepo->create($ocDownload);
        $createdoc_download = $createdoc_download->toArray();
        $this->assertArrayHasKey('id', $createdoc_download);
        $this->assertNotNull($createdoc_download['id'], 'Created oc_download must have id specified');
        $this->assertNotNull(oc_download::find($createdoc_download['id']), 'oc_download with given id must be in DB');
        $this->assertModelData($ocDownload, $createdoc_download);
    }

    /**
     * @test read
     */
    public function testReadoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $dboc_download = $this->ocDownloadRepo->find($ocDownload->id);
        $dboc_download = $dboc_download->toArray();
        $this->assertModelData($ocDownload->toArray(), $dboc_download);
    }

    /**
     * @test update
     */
    public function testUpdateoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $fakeoc_download = $this->fakeoc_downloadData();
        $updatedoc_download = $this->ocDownloadRepo->update($fakeoc_download, $ocDownload->id);
        $this->assertModelData($fakeoc_download, $updatedoc_download->toArray());
        $dboc_download = $this->ocDownloadRepo->find($ocDownload->id);
        $this->assertModelData($fakeoc_download, $dboc_download->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_download()
    {
        $ocDownload = $this->makeoc_download();
        $resp = $this->ocDownloadRepo->delete($ocDownload->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_download::find($ocDownload->id), 'oc_download should not exist in DB');
    }
}
