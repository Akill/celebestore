<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_eventApiTest extends TestCase
{
    use Makeoc_eventTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_event()
    {
        $ocEvent = $this->fakeoc_eventData();
        $this->json('POST', '/api/v1/ocEvents', $ocEvent);

        $this->assertApiResponse($ocEvent);
    }

    /**
     * @test
     */
    public function testReadoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $this->json('GET', '/api/v1/ocEvents/'.$ocEvent->id);

        $this->assertApiResponse($ocEvent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $editedoc_event = $this->fakeoc_eventData();

        $this->json('PUT', '/api/v1/ocEvents/'.$ocEvent->id, $editedoc_event);

        $this->assertApiResponse($editedoc_event);
    }

    /**
     * @test
     */
    public function testDeleteoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $this->json('DELETE', '/api/v1/ocEvents/'.$ocEvent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocEvents/'.$ocEvent->id);

        $this->assertResponseStatus(404);
    }
}
