<?php

use App\Models\oc_customer_reward;
use App\Repositories\oc_customer_rewardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_rewardRepositoryTest extends TestCase
{
    use Makeoc_customer_rewardTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_rewardRepository
     */
    protected $ocCustomerRewardRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerRewardRepo = App::make(oc_customer_rewardRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_reward()
    {
        $ocCustomerReward = $this->fakeoc_customer_rewardData();
        $createdoc_customer_reward = $this->ocCustomerRewardRepo->create($ocCustomerReward);
        $createdoc_customer_reward = $createdoc_customer_reward->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_reward);
        $this->assertNotNull($createdoc_customer_reward['id'], 'Created oc_customer_reward must have id specified');
        $this->assertNotNull(oc_customer_reward::find($createdoc_customer_reward['id']), 'oc_customer_reward with given id must be in DB');
        $this->assertModelData($ocCustomerReward, $createdoc_customer_reward);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $dboc_customer_reward = $this->ocCustomerRewardRepo->find($ocCustomerReward->id);
        $dboc_customer_reward = $dboc_customer_reward->toArray();
        $this->assertModelData($ocCustomerReward->toArray(), $dboc_customer_reward);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $fakeoc_customer_reward = $this->fakeoc_customer_rewardData();
        $updatedoc_customer_reward = $this->ocCustomerRewardRepo->update($fakeoc_customer_reward, $ocCustomerReward->id);
        $this->assertModelData($fakeoc_customer_reward, $updatedoc_customer_reward->toArray());
        $dboc_customer_reward = $this->ocCustomerRewardRepo->find($ocCustomerReward->id);
        $this->assertModelData($fakeoc_customer_reward, $dboc_customer_reward->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $resp = $this->ocCustomerRewardRepo->delete($ocCustomerReward->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_reward::find($ocCustomerReward->id), 'oc_customer_reward should not exist in DB');
    }
}
