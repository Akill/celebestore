<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_option_valueApiTest extends TestCase
{
    use Makeoc_product_option_valueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_option_value()
    {
        $ocProductOptionValue = $this->fakeoc_product_option_valueData();
        $this->json('POST', '/api/v1/ocProductOptionValues', $ocProductOptionValue);

        $this->assertApiResponse($ocProductOptionValue);
    }

    /**
     * @test
     */
    public function testReadoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $this->json('GET', '/api/v1/ocProductOptionValues/'.$ocProductOptionValue->id);

        $this->assertApiResponse($ocProductOptionValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $editedoc_product_option_value = $this->fakeoc_product_option_valueData();

        $this->json('PUT', '/api/v1/ocProductOptionValues/'.$ocProductOptionValue->id, $editedoc_product_option_value);

        $this->assertApiResponse($editedoc_product_option_value);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_option_value()
    {
        $ocProductOptionValue = $this->makeoc_product_option_value();
        $this->json('DELETE', '/api/v1/ocProductOptionValues/'.$ocProductOptionValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductOptionValues/'.$ocProductOptionValue->id);

        $this->assertResponseStatus(404);
    }
}
