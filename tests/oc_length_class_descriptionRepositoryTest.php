<?php

use App\Models\oc_length_class_description;
use App\Repositories\oc_length_class_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_length_class_descriptionRepositoryTest extends TestCase
{
    use Makeoc_length_class_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_length_class_descriptionRepository
     */
    protected $ocLengthClassDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLengthClassDescriptionRepo = App::make(oc_length_class_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_length_class_description()
    {
        $ocLengthClassDescription = $this->fakeoc_length_class_descriptionData();
        $createdoc_length_class_description = $this->ocLengthClassDescriptionRepo->create($ocLengthClassDescription);
        $createdoc_length_class_description = $createdoc_length_class_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_length_class_description);
        $this->assertNotNull($createdoc_length_class_description['id'], 'Created oc_length_class_description must have id specified');
        $this->assertNotNull(oc_length_class_description::find($createdoc_length_class_description['id']), 'oc_length_class_description with given id must be in DB');
        $this->assertModelData($ocLengthClassDescription, $createdoc_length_class_description);
    }

    /**
     * @test read
     */
    public function testReadoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $dboc_length_class_description = $this->ocLengthClassDescriptionRepo->find($ocLengthClassDescription->id);
        $dboc_length_class_description = $dboc_length_class_description->toArray();
        $this->assertModelData($ocLengthClassDescription->toArray(), $dboc_length_class_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $fakeoc_length_class_description = $this->fakeoc_length_class_descriptionData();
        $updatedoc_length_class_description = $this->ocLengthClassDescriptionRepo->update($fakeoc_length_class_description, $ocLengthClassDescription->id);
        $this->assertModelData($fakeoc_length_class_description, $updatedoc_length_class_description->toArray());
        $dboc_length_class_description = $this->ocLengthClassDescriptionRepo->find($ocLengthClassDescription->id);
        $this->assertModelData($fakeoc_length_class_description, $dboc_length_class_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_length_class_description()
    {
        $ocLengthClassDescription = $this->makeoc_length_class_description();
        $resp = $this->ocLengthClassDescriptionRepo->delete($ocLengthClassDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_length_class_description::find($ocLengthClassDescription->id), 'oc_length_class_description should not exist in DB');
    }
}
