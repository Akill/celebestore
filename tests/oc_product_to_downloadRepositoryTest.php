<?php

use App\Models\oc_product_to_download;
use App\Repositories\oc_product_to_downloadRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_downloadRepositoryTest extends TestCase
{
    use Makeoc_product_to_downloadTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_to_downloadRepository
     */
    protected $ocProductToDownloadRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductToDownloadRepo = App::make(oc_product_to_downloadRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_to_download()
    {
        $ocProductToDownload = $this->fakeoc_product_to_downloadData();
        $createdoc_product_to_download = $this->ocProductToDownloadRepo->create($ocProductToDownload);
        $createdoc_product_to_download = $createdoc_product_to_download->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_to_download);
        $this->assertNotNull($createdoc_product_to_download['id'], 'Created oc_product_to_download must have id specified');
        $this->assertNotNull(oc_product_to_download::find($createdoc_product_to_download['id']), 'oc_product_to_download with given id must be in DB');
        $this->assertModelData($ocProductToDownload, $createdoc_product_to_download);
    }

    /**
     * @test read
     */
    public function testReadoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $dboc_product_to_download = $this->ocProductToDownloadRepo->find($ocProductToDownload->id);
        $dboc_product_to_download = $dboc_product_to_download->toArray();
        $this->assertModelData($ocProductToDownload->toArray(), $dboc_product_to_download);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $fakeoc_product_to_download = $this->fakeoc_product_to_downloadData();
        $updatedoc_product_to_download = $this->ocProductToDownloadRepo->update($fakeoc_product_to_download, $ocProductToDownload->id);
        $this->assertModelData($fakeoc_product_to_download, $updatedoc_product_to_download->toArray());
        $dboc_product_to_download = $this->ocProductToDownloadRepo->find($ocProductToDownload->id);
        $this->assertModelData($fakeoc_product_to_download, $dboc_product_to_download->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $resp = $this->ocProductToDownloadRepo->delete($ocProductToDownload->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_to_download::find($ocProductToDownload->id), 'oc_product_to_download should not exist in DB');
    }
}
