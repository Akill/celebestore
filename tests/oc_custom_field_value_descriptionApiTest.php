<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_value_descriptionApiTest extends TestCase
{
    use Makeoc_custom_field_value_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->fakeoc_custom_field_value_descriptionData();
        $this->json('POST', '/api/v1/ocCustomFieldValueDescriptions', $ocCustomFieldValueDescription);

        $this->assertApiResponse($ocCustomFieldValueDescription);
    }

    /**
     * @test
     */
    public function testReadoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $this->json('GET', '/api/v1/ocCustomFieldValueDescriptions/'.$ocCustomFieldValueDescription->id);

        $this->assertApiResponse($ocCustomFieldValueDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $editedoc_custom_field_value_description = $this->fakeoc_custom_field_value_descriptionData();

        $this->json('PUT', '/api/v1/ocCustomFieldValueDescriptions/'.$ocCustomFieldValueDescription->id, $editedoc_custom_field_value_description);

        $this->assertApiResponse($editedoc_custom_field_value_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_custom_field_value_description()
    {
        $ocCustomFieldValueDescription = $this->makeoc_custom_field_value_description();
        $this->json('DELETE', '/api/v1/ocCustomFieldValueDescriptions/'.$ocCustomFieldValueDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomFieldValueDescriptions/'.$ocCustomFieldValueDescription->id);

        $this->assertResponseStatus(404);
    }
}
