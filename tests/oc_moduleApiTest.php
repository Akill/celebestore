<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_moduleApiTest extends TestCase
{
    use Makeoc_moduleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_module()
    {
        $ocModule = $this->fakeoc_moduleData();
        $this->json('POST', '/api/v1/ocModules', $ocModule);

        $this->assertApiResponse($ocModule);
    }

    /**
     * @test
     */
    public function testReadoc_module()
    {
        $ocModule = $this->makeoc_module();
        $this->json('GET', '/api/v1/ocModules/'.$ocModule->id);

        $this->assertApiResponse($ocModule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_module()
    {
        $ocModule = $this->makeoc_module();
        $editedoc_module = $this->fakeoc_moduleData();

        $this->json('PUT', '/api/v1/ocModules/'.$ocModule->id, $editedoc_module);

        $this->assertApiResponse($editedoc_module);
    }

    /**
     * @test
     */
    public function testDeleteoc_module()
    {
        $ocModule = $this->makeoc_module();
        $this->json('DELETE', '/api/v1/ocModules/'.$ocModule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocModules/'.$ocModule->id);

        $this->assertResponseStatus(404);
    }
}
