<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_discountApiTest extends TestCase
{
    use Makeoc_product_discountTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_discount()
    {
        $ocProductDiscount = $this->fakeoc_product_discountData();
        $this->json('POST', '/api/v1/ocProductDiscounts', $ocProductDiscount);

        $this->assertApiResponse($ocProductDiscount);
    }

    /**
     * @test
     */
    public function testReadoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $this->json('GET', '/api/v1/ocProductDiscounts/'.$ocProductDiscount->id);

        $this->assertApiResponse($ocProductDiscount->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $editedoc_product_discount = $this->fakeoc_product_discountData();

        $this->json('PUT', '/api/v1/ocProductDiscounts/'.$ocProductDiscount->id, $editedoc_product_discount);

        $this->assertApiResponse($editedoc_product_discount);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $this->json('DELETE', '/api/v1/ocProductDiscounts/'.$ocProductDiscount->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductDiscounts/'.$ocProductDiscount->id);

        $this->assertResponseStatus(404);
    }
}
