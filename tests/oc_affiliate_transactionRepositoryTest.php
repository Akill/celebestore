<?php

use App\Models\oc_affiliate_transaction;
use App\Repositories\oc_affiliate_transactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_transactionRepositoryTest extends TestCase
{
    use Makeoc_affiliate_transactionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_affiliate_transactionRepository
     */
    protected $ocAffiliateTransactionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAffiliateTransactionRepo = App::make(oc_affiliate_transactionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->fakeoc_affiliate_transactionData();
        $createdoc_affiliate_transaction = $this->ocAffiliateTransactionRepo->create($ocAffiliateTransaction);
        $createdoc_affiliate_transaction = $createdoc_affiliate_transaction->toArray();
        $this->assertArrayHasKey('id', $createdoc_affiliate_transaction);
        $this->assertNotNull($createdoc_affiliate_transaction['id'], 'Created oc_affiliate_transaction must have id specified');
        $this->assertNotNull(oc_affiliate_transaction::find($createdoc_affiliate_transaction['id']), 'oc_affiliate_transaction with given id must be in DB');
        $this->assertModelData($ocAffiliateTransaction, $createdoc_affiliate_transaction);
    }

    /**
     * @test read
     */
    public function testReadoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $dboc_affiliate_transaction = $this->ocAffiliateTransactionRepo->find($ocAffiliateTransaction->id);
        $dboc_affiliate_transaction = $dboc_affiliate_transaction->toArray();
        $this->assertModelData($ocAffiliateTransaction->toArray(), $dboc_affiliate_transaction);
    }

    /**
     * @test update
     */
    public function testUpdateoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $fakeoc_affiliate_transaction = $this->fakeoc_affiliate_transactionData();
        $updatedoc_affiliate_transaction = $this->ocAffiliateTransactionRepo->update($fakeoc_affiliate_transaction, $ocAffiliateTransaction->id);
        $this->assertModelData($fakeoc_affiliate_transaction, $updatedoc_affiliate_transaction->toArray());
        $dboc_affiliate_transaction = $this->ocAffiliateTransactionRepo->find($ocAffiliateTransaction->id);
        $this->assertModelData($fakeoc_affiliate_transaction, $dboc_affiliate_transaction->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $resp = $this->ocAffiliateTransactionRepo->delete($ocAffiliateTransaction->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_affiliate_transaction::find($ocAffiliateTransaction->id), 'oc_affiliate_transaction should not exist in DB');
    }
}
