<?php

use App\Models\oc_product_related;
use App\Repositories\oc_product_relatedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_relatedRepositoryTest extends TestCase
{
    use Makeoc_product_relatedTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_relatedRepository
     */
    protected $ocProductRelatedRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductRelatedRepo = App::make(oc_product_relatedRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_related()
    {
        $ocProductRelated = $this->fakeoc_product_relatedData();
        $createdoc_product_related = $this->ocProductRelatedRepo->create($ocProductRelated);
        $createdoc_product_related = $createdoc_product_related->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_related);
        $this->assertNotNull($createdoc_product_related['id'], 'Created oc_product_related must have id specified');
        $this->assertNotNull(oc_product_related::find($createdoc_product_related['id']), 'oc_product_related with given id must be in DB');
        $this->assertModelData($ocProductRelated, $createdoc_product_related);
    }

    /**
     * @test read
     */
    public function testReadoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $dboc_product_related = $this->ocProductRelatedRepo->find($ocProductRelated->id);
        $dboc_product_related = $dboc_product_related->toArray();
        $this->assertModelData($ocProductRelated->toArray(), $dboc_product_related);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $fakeoc_product_related = $this->fakeoc_product_relatedData();
        $updatedoc_product_related = $this->ocProductRelatedRepo->update($fakeoc_product_related, $ocProductRelated->id);
        $this->assertModelData($fakeoc_product_related, $updatedoc_product_related->toArray());
        $dboc_product_related = $this->ocProductRelatedRepo->find($ocProductRelated->id);
        $this->assertModelData($fakeoc_product_related, $dboc_product_related->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $resp = $this->ocProductRelatedRepo->delete($ocProductRelated->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_related::find($ocProductRelated->id), 'oc_product_related should not exist in DB');
    }
}
