<?php

use App\Models\oc_return_action;
use App\Repositories\oc_return_actionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_actionRepositoryTest extends TestCase
{
    use Makeoc_return_actionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_return_actionRepository
     */
    protected $ocReturnActionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReturnActionRepo = App::make(oc_return_actionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_return_action()
    {
        $ocReturnAction = $this->fakeoc_return_actionData();
        $createdoc_return_action = $this->ocReturnActionRepo->create($ocReturnAction);
        $createdoc_return_action = $createdoc_return_action->toArray();
        $this->assertArrayHasKey('id', $createdoc_return_action);
        $this->assertNotNull($createdoc_return_action['id'], 'Created oc_return_action must have id specified');
        $this->assertNotNull(oc_return_action::find($createdoc_return_action['id']), 'oc_return_action with given id must be in DB');
        $this->assertModelData($ocReturnAction, $createdoc_return_action);
    }

    /**
     * @test read
     */
    public function testReadoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $dboc_return_action = $this->ocReturnActionRepo->find($ocReturnAction->id);
        $dboc_return_action = $dboc_return_action->toArray();
        $this->assertModelData($ocReturnAction->toArray(), $dboc_return_action);
    }

    /**
     * @test update
     */
    public function testUpdateoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $fakeoc_return_action = $this->fakeoc_return_actionData();
        $updatedoc_return_action = $this->ocReturnActionRepo->update($fakeoc_return_action, $ocReturnAction->id);
        $this->assertModelData($fakeoc_return_action, $updatedoc_return_action->toArray());
        $dboc_return_action = $this->ocReturnActionRepo->find($ocReturnAction->id);
        $this->assertModelData($fakeoc_return_action, $dboc_return_action->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $resp = $this->ocReturnActionRepo->delete($ocReturnAction->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_return_action::find($ocReturnAction->id), 'oc_return_action should not exist in DB');
    }
}
