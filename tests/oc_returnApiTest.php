<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_returnApiTest extends TestCase
{
    use Makeoc_returnTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_return()
    {
        $ocReturn = $this->fakeoc_returnData();
        $this->json('POST', '/api/v1/ocReturns', $ocReturn);

        $this->assertApiResponse($ocReturn);
    }

    /**
     * @test
     */
    public function testReadoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $this->json('GET', '/api/v1/ocReturns/'.$ocReturn->id);

        $this->assertApiResponse($ocReturn->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $editedoc_return = $this->fakeoc_returnData();

        $this->json('PUT', '/api/v1/ocReturns/'.$ocReturn->id, $editedoc_return);

        $this->assertApiResponse($editedoc_return);
    }

    /**
     * @test
     */
    public function testDeleteoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $this->json('DELETE', '/api/v1/ocReturns/'.$ocReturn->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReturns/'.$ocReturn->id);

        $this->assertResponseStatus(404);
    }
}
