<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_reasonApiTest extends TestCase
{
    use Makeoc_return_reasonTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_return_reason()
    {
        $ocReturnReason = $this->fakeoc_return_reasonData();
        $this->json('POST', '/api/v1/ocReturnReasons', $ocReturnReason);

        $this->assertApiResponse($ocReturnReason);
    }

    /**
     * @test
     */
    public function testReadoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $this->json('GET', '/api/v1/ocReturnReasons/'.$ocReturnReason->id);

        $this->assertApiResponse($ocReturnReason->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $editedoc_return_reason = $this->fakeoc_return_reasonData();

        $this->json('PUT', '/api/v1/ocReturnReasons/'.$ocReturnReason->id, $editedoc_return_reason);

        $this->assertApiResponse($editedoc_return_reason);
    }

    /**
     * @test
     */
    public function testDeleteoc_return_reason()
    {
        $ocReturnReason = $this->makeoc_return_reason();
        $this->json('DELETE', '/api/v1/ocReturnReasons/'.$ocReturnReason->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReturnReasons/'.$ocReturnReason->id);

        $this->assertResponseStatus(404);
    }
}
