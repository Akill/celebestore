<?php

use App\Models\oc_order_recurring;
use App\Repositories\oc_order_recurringRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_recurringRepositoryTest extends TestCase
{
    use Makeoc_order_recurringTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_recurringRepository
     */
    protected $ocOrderRecurringRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderRecurringRepo = App::make(oc_order_recurringRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_recurring()
    {
        $ocOrderRecurring = $this->fakeoc_order_recurringData();
        $createdoc_order_recurring = $this->ocOrderRecurringRepo->create($ocOrderRecurring);
        $createdoc_order_recurring = $createdoc_order_recurring->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_recurring);
        $this->assertNotNull($createdoc_order_recurring['id'], 'Created oc_order_recurring must have id specified');
        $this->assertNotNull(oc_order_recurring::find($createdoc_order_recurring['id']), 'oc_order_recurring with given id must be in DB');
        $this->assertModelData($ocOrderRecurring, $createdoc_order_recurring);
    }

    /**
     * @test read
     */
    public function testReadoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $dboc_order_recurring = $this->ocOrderRecurringRepo->find($ocOrderRecurring->id);
        $dboc_order_recurring = $dboc_order_recurring->toArray();
        $this->assertModelData($ocOrderRecurring->toArray(), $dboc_order_recurring);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $fakeoc_order_recurring = $this->fakeoc_order_recurringData();
        $updatedoc_order_recurring = $this->ocOrderRecurringRepo->update($fakeoc_order_recurring, $ocOrderRecurring->id);
        $this->assertModelData($fakeoc_order_recurring, $updatedoc_order_recurring->toArray());
        $dboc_order_recurring = $this->ocOrderRecurringRepo->find($ocOrderRecurring->id);
        $this->assertModelData($fakeoc_order_recurring, $dboc_order_recurring->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_recurring()
    {
        $ocOrderRecurring = $this->makeoc_order_recurring();
        $resp = $this->ocOrderRecurringRepo->delete($ocOrderRecurring->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_recurring::find($ocOrderRecurring->id), 'oc_order_recurring should not exist in DB');
    }
}
