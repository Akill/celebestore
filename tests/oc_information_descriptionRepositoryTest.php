<?php

use App\Models\oc_information_description;
use App\Repositories\oc_information_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_descriptionRepositoryTest extends TestCase
{
    use Makeoc_information_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_information_descriptionRepository
     */
    protected $ocInformationDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocInformationDescriptionRepo = App::make(oc_information_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_information_description()
    {
        $ocInformationDescription = $this->fakeoc_information_descriptionData();
        $createdoc_information_description = $this->ocInformationDescriptionRepo->create($ocInformationDescription);
        $createdoc_information_description = $createdoc_information_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_information_description);
        $this->assertNotNull($createdoc_information_description['id'], 'Created oc_information_description must have id specified');
        $this->assertNotNull(oc_information_description::find($createdoc_information_description['id']), 'oc_information_description with given id must be in DB');
        $this->assertModelData($ocInformationDescription, $createdoc_information_description);
    }

    /**
     * @test read
     */
    public function testReadoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $dboc_information_description = $this->ocInformationDescriptionRepo->find($ocInformationDescription->id);
        $dboc_information_description = $dboc_information_description->toArray();
        $this->assertModelData($ocInformationDescription->toArray(), $dboc_information_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $fakeoc_information_description = $this->fakeoc_information_descriptionData();
        $updatedoc_information_description = $this->ocInformationDescriptionRepo->update($fakeoc_information_description, $ocInformationDescription->id);
        $this->assertModelData($fakeoc_information_description, $updatedoc_information_description->toArray());
        $dboc_information_description = $this->ocInformationDescriptionRepo->find($ocInformationDescription->id);
        $this->assertModelData($fakeoc_information_description, $dboc_information_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $resp = $this->ocInformationDescriptionRepo->delete($ocInformationDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_information_description::find($ocInformationDescription->id), 'oc_information_description should not exist in DB');
    }
}
