<?php

use App\Models\oc_option_description;
use App\Repositories\oc_option_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_descriptionRepositoryTest extends TestCase
{
    use Makeoc_option_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_option_descriptionRepository
     */
    protected $ocOptionDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOptionDescriptionRepo = App::make(oc_option_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_option_description()
    {
        $ocOptionDescription = $this->fakeoc_option_descriptionData();
        $createdoc_option_description = $this->ocOptionDescriptionRepo->create($ocOptionDescription);
        $createdoc_option_description = $createdoc_option_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_option_description);
        $this->assertNotNull($createdoc_option_description['id'], 'Created oc_option_description must have id specified');
        $this->assertNotNull(oc_option_description::find($createdoc_option_description['id']), 'oc_option_description with given id must be in DB');
        $this->assertModelData($ocOptionDescription, $createdoc_option_description);
    }

    /**
     * @test read
     */
    public function testReadoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $dboc_option_description = $this->ocOptionDescriptionRepo->find($ocOptionDescription->id);
        $dboc_option_description = $dboc_option_description->toArray();
        $this->assertModelData($ocOptionDescription->toArray(), $dboc_option_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $fakeoc_option_description = $this->fakeoc_option_descriptionData();
        $updatedoc_option_description = $this->ocOptionDescriptionRepo->update($fakeoc_option_description, $ocOptionDescription->id);
        $this->assertModelData($fakeoc_option_description, $updatedoc_option_description->toArray());
        $dboc_option_description = $this->ocOptionDescriptionRepo->find($ocOptionDescription->id);
        $this->assertModelData($fakeoc_option_description, $dboc_option_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $resp = $this->ocOptionDescriptionRepo->delete($ocOptionDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_option_description::find($ocOptionDescription->id), 'oc_option_description should not exist in DB');
    }
}
