<?php

use App\Models\oc_user_group;
use App\Repositories\oc_user_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_user_groupRepositoryTest extends TestCase
{
    use Makeoc_user_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_user_groupRepository
     */
    protected $ocUserGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocUserGroupRepo = App::make(oc_user_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_user_group()
    {
        $ocUserGroup = $this->fakeoc_user_groupData();
        $createdoc_user_group = $this->ocUserGroupRepo->create($ocUserGroup);
        $createdoc_user_group = $createdoc_user_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_user_group);
        $this->assertNotNull($createdoc_user_group['id'], 'Created oc_user_group must have id specified');
        $this->assertNotNull(oc_user_group::find($createdoc_user_group['id']), 'oc_user_group with given id must be in DB');
        $this->assertModelData($ocUserGroup, $createdoc_user_group);
    }

    /**
     * @test read
     */
    public function testReadoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $dboc_user_group = $this->ocUserGroupRepo->find($ocUserGroup->id);
        $dboc_user_group = $dboc_user_group->toArray();
        $this->assertModelData($ocUserGroup->toArray(), $dboc_user_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $fakeoc_user_group = $this->fakeoc_user_groupData();
        $updatedoc_user_group = $this->ocUserGroupRepo->update($fakeoc_user_group, $ocUserGroup->id);
        $this->assertModelData($fakeoc_user_group, $updatedoc_user_group->toArray());
        $dboc_user_group = $this->ocUserGroupRepo->find($ocUserGroup->id);
        $this->assertModelData($fakeoc_user_group, $dboc_user_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $resp = $this->ocUserGroupRepo->delete($ocUserGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_user_group::find($ocUserGroup->id), 'oc_user_group should not exist in DB');
    }
}
