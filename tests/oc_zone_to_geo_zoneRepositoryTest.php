<?php

use App\Models\oc_zone_to_geo_zone;
use App\Repositories\oc_zone_to_geo_zoneRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_zone_to_geo_zoneRepositoryTest extends TestCase
{
    use Makeoc_zone_to_geo_zoneTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_zone_to_geo_zoneRepository
     */
    protected $ocZoneToGeoZoneRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocZoneToGeoZoneRepo = App::make(oc_zone_to_geo_zoneRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->fakeoc_zone_to_geo_zoneData();
        $createdoc_zone_to_geo_zone = $this->ocZoneToGeoZoneRepo->create($ocZoneToGeoZone);
        $createdoc_zone_to_geo_zone = $createdoc_zone_to_geo_zone->toArray();
        $this->assertArrayHasKey('id', $createdoc_zone_to_geo_zone);
        $this->assertNotNull($createdoc_zone_to_geo_zone['id'], 'Created oc_zone_to_geo_zone must have id specified');
        $this->assertNotNull(oc_zone_to_geo_zone::find($createdoc_zone_to_geo_zone['id']), 'oc_zone_to_geo_zone with given id must be in DB');
        $this->assertModelData($ocZoneToGeoZone, $createdoc_zone_to_geo_zone);
    }

    /**
     * @test read
     */
    public function testReadoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $dboc_zone_to_geo_zone = $this->ocZoneToGeoZoneRepo->find($ocZoneToGeoZone->id);
        $dboc_zone_to_geo_zone = $dboc_zone_to_geo_zone->toArray();
        $this->assertModelData($ocZoneToGeoZone->toArray(), $dboc_zone_to_geo_zone);
    }

    /**
     * @test update
     */
    public function testUpdateoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $fakeoc_zone_to_geo_zone = $this->fakeoc_zone_to_geo_zoneData();
        $updatedoc_zone_to_geo_zone = $this->ocZoneToGeoZoneRepo->update($fakeoc_zone_to_geo_zone, $ocZoneToGeoZone->id);
        $this->assertModelData($fakeoc_zone_to_geo_zone, $updatedoc_zone_to_geo_zone->toArray());
        $dboc_zone_to_geo_zone = $this->ocZoneToGeoZoneRepo->find($ocZoneToGeoZone->id);
        $this->assertModelData($fakeoc_zone_to_geo_zone, $dboc_zone_to_geo_zone->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $resp = $this->ocZoneToGeoZoneRepo->delete($ocZoneToGeoZone->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_zone_to_geo_zone::find($ocZoneToGeoZone->id), 'oc_zone_to_geo_zone should not exist in DB');
    }
}
