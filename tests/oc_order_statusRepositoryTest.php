<?php

use App\Models\oc_order_status;
use App\Repositories\oc_order_statusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_statusRepositoryTest extends TestCase
{
    use Makeoc_order_statusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_statusRepository
     */
    protected $ocOrderStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderStatusRepo = App::make(oc_order_statusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_status()
    {
        $ocOrderStatus = $this->fakeoc_order_statusData();
        $createdoc_order_status = $this->ocOrderStatusRepo->create($ocOrderStatus);
        $createdoc_order_status = $createdoc_order_status->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_status);
        $this->assertNotNull($createdoc_order_status['id'], 'Created oc_order_status must have id specified');
        $this->assertNotNull(oc_order_status::find($createdoc_order_status['id']), 'oc_order_status with given id must be in DB');
        $this->assertModelData($ocOrderStatus, $createdoc_order_status);
    }

    /**
     * @test read
     */
    public function testReadoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $dboc_order_status = $this->ocOrderStatusRepo->find($ocOrderStatus->id);
        $dboc_order_status = $dboc_order_status->toArray();
        $this->assertModelData($ocOrderStatus->toArray(), $dboc_order_status);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $fakeoc_order_status = $this->fakeoc_order_statusData();
        $updatedoc_order_status = $this->ocOrderStatusRepo->update($fakeoc_order_status, $ocOrderStatus->id);
        $this->assertModelData($fakeoc_order_status, $updatedoc_order_status->toArray());
        $dboc_order_status = $this->ocOrderStatusRepo->find($ocOrderStatus->id);
        $this->assertModelData($fakeoc_order_status, $dboc_order_status->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_status()
    {
        $ocOrderStatus = $this->makeoc_order_status();
        $resp = $this->ocOrderStatusRepo->delete($ocOrderStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_status::find($ocOrderStatus->id), 'oc_order_status should not exist in DB');
    }
}
