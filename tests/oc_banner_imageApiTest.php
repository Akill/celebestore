<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_banner_imageApiTest extends TestCase
{
    use Makeoc_banner_imageTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_banner_image()
    {
        $ocBannerImage = $this->fakeoc_banner_imageData();
        $this->json('POST', '/api/v1/ocBannerImages', $ocBannerImage);

        $this->assertApiResponse($ocBannerImage);
    }

    /**
     * @test
     */
    public function testReadoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $this->json('GET', '/api/v1/ocBannerImages/'.$ocBannerImage->id);

        $this->assertApiResponse($ocBannerImage->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $editedoc_banner_image = $this->fakeoc_banner_imageData();

        $this->json('PUT', '/api/v1/ocBannerImages/'.$ocBannerImage->id, $editedoc_banner_image);

        $this->assertApiResponse($editedoc_banner_image);
    }

    /**
     * @test
     */
    public function testDeleteoc_banner_image()
    {
        $ocBannerImage = $this->makeoc_banner_image();
        $this->json('DELETE', '/api/v1/ocBannerImages/'.$ocBannerImage->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocBannerImages/'.$ocBannerImage->id);

        $this->assertResponseStatus(404);
    }
}
