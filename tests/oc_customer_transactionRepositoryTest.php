<?php

use App\Models\oc_customer_transaction;
use App\Repositories\oc_customer_transactionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_transactionRepositoryTest extends TestCase
{
    use Makeoc_customer_transactionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_transactionRepository
     */
    protected $ocCustomerTransactionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerTransactionRepo = App::make(oc_customer_transactionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->fakeoc_customer_transactionData();
        $createdoc_customer_transaction = $this->ocCustomerTransactionRepo->create($ocCustomerTransaction);
        $createdoc_customer_transaction = $createdoc_customer_transaction->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_transaction);
        $this->assertNotNull($createdoc_customer_transaction['id'], 'Created oc_customer_transaction must have id specified');
        $this->assertNotNull(oc_customer_transaction::find($createdoc_customer_transaction['id']), 'oc_customer_transaction with given id must be in DB');
        $this->assertModelData($ocCustomerTransaction, $createdoc_customer_transaction);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $dboc_customer_transaction = $this->ocCustomerTransactionRepo->find($ocCustomerTransaction->id);
        $dboc_customer_transaction = $dboc_customer_transaction->toArray();
        $this->assertModelData($ocCustomerTransaction->toArray(), $dboc_customer_transaction);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $fakeoc_customer_transaction = $this->fakeoc_customer_transactionData();
        $updatedoc_customer_transaction = $this->ocCustomerTransactionRepo->update($fakeoc_customer_transaction, $ocCustomerTransaction->id);
        $this->assertModelData($fakeoc_customer_transaction, $updatedoc_customer_transaction->toArray());
        $dboc_customer_transaction = $this->ocCustomerTransactionRepo->find($ocCustomerTransaction->id);
        $this->assertModelData($fakeoc_customer_transaction, $dboc_customer_transaction->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $resp = $this->ocCustomerTransactionRepo->delete($ocCustomerTransaction->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_transaction::find($ocCustomerTransaction->id), 'oc_customer_transaction should not exist in DB');
    }
}
