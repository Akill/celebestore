<?php

use App\Models\oc_order_history;
use App\Repositories\oc_order_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_historyRepositoryTest extends TestCase
{
    use Makeoc_order_historyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_historyRepository
     */
    protected $ocOrderHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderHistoryRepo = App::make(oc_order_historyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_history()
    {
        $ocOrderHistory = $this->fakeoc_order_historyData();
        $createdoc_order_history = $this->ocOrderHistoryRepo->create($ocOrderHistory);
        $createdoc_order_history = $createdoc_order_history->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_history);
        $this->assertNotNull($createdoc_order_history['id'], 'Created oc_order_history must have id specified');
        $this->assertNotNull(oc_order_history::find($createdoc_order_history['id']), 'oc_order_history with given id must be in DB');
        $this->assertModelData($ocOrderHistory, $createdoc_order_history);
    }

    /**
     * @test read
     */
    public function testReadoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $dboc_order_history = $this->ocOrderHistoryRepo->find($ocOrderHistory->id);
        $dboc_order_history = $dboc_order_history->toArray();
        $this->assertModelData($ocOrderHistory->toArray(), $dboc_order_history);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $fakeoc_order_history = $this->fakeoc_order_historyData();
        $updatedoc_order_history = $this->ocOrderHistoryRepo->update($fakeoc_order_history, $ocOrderHistory->id);
        $this->assertModelData($fakeoc_order_history, $updatedoc_order_history->toArray());
        $dboc_order_history = $this->ocOrderHistoryRepo->find($ocOrderHistory->id);
        $this->assertModelData($fakeoc_order_history, $dboc_order_history->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_history()
    {
        $ocOrderHistory = $this->makeoc_order_history();
        $resp = $this->ocOrderHistoryRepo->delete($ocOrderHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_history::find($ocOrderHistory->id), 'oc_order_history should not exist in DB');
    }
}
