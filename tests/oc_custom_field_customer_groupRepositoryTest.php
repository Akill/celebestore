<?php

use App\Models\oc_custom_field_customer_group;
use App\Repositories\oc_custom_field_customer_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_field_customer_groupRepositoryTest extends TestCase
{
    use Makeoc_custom_field_customer_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_custom_field_customer_groupRepository
     */
    protected $ocCustomFieldCustomerGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomFieldCustomerGroupRepo = App::make(oc_custom_field_customer_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->fakeoc_custom_field_customer_groupData();
        $createdoc_custom_field_customer_group = $this->ocCustomFieldCustomerGroupRepo->create($ocCustomFieldCustomerGroup);
        $createdoc_custom_field_customer_group = $createdoc_custom_field_customer_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_custom_field_customer_group);
        $this->assertNotNull($createdoc_custom_field_customer_group['id'], 'Created oc_custom_field_customer_group must have id specified');
        $this->assertNotNull(oc_custom_field_customer_group::find($createdoc_custom_field_customer_group['id']), 'oc_custom_field_customer_group with given id must be in DB');
        $this->assertModelData($ocCustomFieldCustomerGroup, $createdoc_custom_field_customer_group);
    }

    /**
     * @test read
     */
    public function testReadoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $dboc_custom_field_customer_group = $this->ocCustomFieldCustomerGroupRepo->find($ocCustomFieldCustomerGroup->id);
        $dboc_custom_field_customer_group = $dboc_custom_field_customer_group->toArray();
        $this->assertModelData($ocCustomFieldCustomerGroup->toArray(), $dboc_custom_field_customer_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $fakeoc_custom_field_customer_group = $this->fakeoc_custom_field_customer_groupData();
        $updatedoc_custom_field_customer_group = $this->ocCustomFieldCustomerGroupRepo->update($fakeoc_custom_field_customer_group, $ocCustomFieldCustomerGroup->id);
        $this->assertModelData($fakeoc_custom_field_customer_group, $updatedoc_custom_field_customer_group->toArray());
        $dboc_custom_field_customer_group = $this->ocCustomFieldCustomerGroupRepo->find($ocCustomFieldCustomerGroup->id);
        $this->assertModelData($fakeoc_custom_field_customer_group, $dboc_custom_field_customer_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_custom_field_customer_group()
    {
        $ocCustomFieldCustomerGroup = $this->makeoc_custom_field_customer_group();
        $resp = $this->ocCustomFieldCustomerGroupRepo->delete($ocCustomFieldCustomerGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_custom_field_customer_group::find($ocCustomFieldCustomerGroup->id), 'oc_custom_field_customer_group should not exist in DB');
    }
}
