<?php

use App\Models\oc_product_to_layout;
use App\Repositories\oc_product_to_layoutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_layoutRepositoryTest extends TestCase
{
    use Makeoc_product_to_layoutTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_to_layoutRepository
     */
    protected $ocProductToLayoutRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductToLayoutRepo = App::make(oc_product_to_layoutRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_to_layout()
    {
        $ocProductToLayout = $this->fakeoc_product_to_layoutData();
        $createdoc_product_to_layout = $this->ocProductToLayoutRepo->create($ocProductToLayout);
        $createdoc_product_to_layout = $createdoc_product_to_layout->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_to_layout);
        $this->assertNotNull($createdoc_product_to_layout['id'], 'Created oc_product_to_layout must have id specified');
        $this->assertNotNull(oc_product_to_layout::find($createdoc_product_to_layout['id']), 'oc_product_to_layout with given id must be in DB');
        $this->assertModelData($ocProductToLayout, $createdoc_product_to_layout);
    }

    /**
     * @test read
     */
    public function testReadoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $dboc_product_to_layout = $this->ocProductToLayoutRepo->find($ocProductToLayout->id);
        $dboc_product_to_layout = $dboc_product_to_layout->toArray();
        $this->assertModelData($ocProductToLayout->toArray(), $dboc_product_to_layout);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $fakeoc_product_to_layout = $this->fakeoc_product_to_layoutData();
        $updatedoc_product_to_layout = $this->ocProductToLayoutRepo->update($fakeoc_product_to_layout, $ocProductToLayout->id);
        $this->assertModelData($fakeoc_product_to_layout, $updatedoc_product_to_layout->toArray());
        $dboc_product_to_layout = $this->ocProductToLayoutRepo->find($ocProductToLayout->id);
        $this->assertModelData($fakeoc_product_to_layout, $dboc_product_to_layout->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_to_layout()
    {
        $ocProductToLayout = $this->makeoc_product_to_layout();
        $resp = $this->ocProductToLayoutRepo->delete($ocProductToLayout->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_to_layout::find($ocProductToLayout->id), 'oc_product_to_layout should not exist in DB');
    }
}
