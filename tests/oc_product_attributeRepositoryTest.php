<?php

use App\Models\oc_product_attribute;
use App\Repositories\oc_product_attributeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_attributeRepositoryTest extends TestCase
{
    use Makeoc_product_attributeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_attributeRepository
     */
    protected $ocProductAttributeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductAttributeRepo = App::make(oc_product_attributeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_attribute()
    {
        $ocProductAttribute = $this->fakeoc_product_attributeData();
        $createdoc_product_attribute = $this->ocProductAttributeRepo->create($ocProductAttribute);
        $createdoc_product_attribute = $createdoc_product_attribute->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_attribute);
        $this->assertNotNull($createdoc_product_attribute['id'], 'Created oc_product_attribute must have id specified');
        $this->assertNotNull(oc_product_attribute::find($createdoc_product_attribute['id']), 'oc_product_attribute with given id must be in DB');
        $this->assertModelData($ocProductAttribute, $createdoc_product_attribute);
    }

    /**
     * @test read
     */
    public function testReadoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $dboc_product_attribute = $this->ocProductAttributeRepo->find($ocProductAttribute->id);
        $dboc_product_attribute = $dboc_product_attribute->toArray();
        $this->assertModelData($ocProductAttribute->toArray(), $dboc_product_attribute);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $fakeoc_product_attribute = $this->fakeoc_product_attributeData();
        $updatedoc_product_attribute = $this->ocProductAttributeRepo->update($fakeoc_product_attribute, $ocProductAttribute->id);
        $this->assertModelData($fakeoc_product_attribute, $updatedoc_product_attribute->toArray());
        $dboc_product_attribute = $this->ocProductAttributeRepo->find($ocProductAttribute->id);
        $this->assertModelData($fakeoc_product_attribute, $dboc_product_attribute->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_attribute()
    {
        $ocProductAttribute = $this->makeoc_product_attribute();
        $resp = $this->ocProductAttributeRepo->delete($ocProductAttribute->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_attribute::find($ocProductAttribute->id), 'oc_product_attribute should not exist in DB');
    }
}
