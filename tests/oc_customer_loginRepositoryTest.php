<?php

use App\Models\oc_customer_login;
use App\Repositories\oc_customer_loginRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_loginRepositoryTest extends TestCase
{
    use Makeoc_customer_loginTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_loginRepository
     */
    protected $ocCustomerLoginRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerLoginRepo = App::make(oc_customer_loginRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_login()
    {
        $ocCustomerLogin = $this->fakeoc_customer_loginData();
        $createdoc_customer_login = $this->ocCustomerLoginRepo->create($ocCustomerLogin);
        $createdoc_customer_login = $createdoc_customer_login->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_login);
        $this->assertNotNull($createdoc_customer_login['id'], 'Created oc_customer_login must have id specified');
        $this->assertNotNull(oc_customer_login::find($createdoc_customer_login['id']), 'oc_customer_login with given id must be in DB');
        $this->assertModelData($ocCustomerLogin, $createdoc_customer_login);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $dboc_customer_login = $this->ocCustomerLoginRepo->find($ocCustomerLogin->id);
        $dboc_customer_login = $dboc_customer_login->toArray();
        $this->assertModelData($ocCustomerLogin->toArray(), $dboc_customer_login);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $fakeoc_customer_login = $this->fakeoc_customer_loginData();
        $updatedoc_customer_login = $this->ocCustomerLoginRepo->update($fakeoc_customer_login, $ocCustomerLogin->id);
        $this->assertModelData($fakeoc_customer_login, $updatedoc_customer_login->toArray());
        $dboc_customer_login = $this->ocCustomerLoginRepo->find($ocCustomerLogin->id);
        $this->assertModelData($fakeoc_customer_login, $dboc_customer_login->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_login()
    {
        $ocCustomerLogin = $this->makeoc_customer_login();
        $resp = $this->ocCustomerLoginRepo->delete($ocCustomerLogin->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_login::find($ocCustomerLogin->id), 'oc_customer_login should not exist in DB');
    }
}
