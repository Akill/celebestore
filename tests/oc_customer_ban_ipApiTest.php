<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_ban_ipApiTest extends TestCase
{
    use Makeoc_customer_ban_ipTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->fakeoc_customer_ban_ipData();
        $this->json('POST', '/api/v1/ocCustomerBanIps', $ocCustomerBanIp);

        $this->assertApiResponse($ocCustomerBanIp);
    }

    /**
     * @test
     */
    public function testReadoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $this->json('GET', '/api/v1/ocCustomerBanIps/'.$ocCustomerBanIp->id);

        $this->assertApiResponse($ocCustomerBanIp->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $editedoc_customer_ban_ip = $this->fakeoc_customer_ban_ipData();

        $this->json('PUT', '/api/v1/ocCustomerBanIps/'.$ocCustomerBanIp->id, $editedoc_customer_ban_ip);

        $this->assertApiResponse($editedoc_customer_ban_ip);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $this->json('DELETE', '/api/v1/ocCustomerBanIps/'.$ocCustomerBanIp->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerBanIps/'.$ocCustomerBanIp->id);

        $this->assertResponseStatus(404);
    }
}
