<?php

use App\Models\oc_extension;
use App\Repositories\oc_extensionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_extensionRepositoryTest extends TestCase
{
    use Makeoc_extensionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_extensionRepository
     */
    protected $ocExtensionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocExtensionRepo = App::make(oc_extensionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_extension()
    {
        $ocExtension = $this->fakeoc_extensionData();
        $createdoc_extension = $this->ocExtensionRepo->create($ocExtension);
        $createdoc_extension = $createdoc_extension->toArray();
        $this->assertArrayHasKey('id', $createdoc_extension);
        $this->assertNotNull($createdoc_extension['id'], 'Created oc_extension must have id specified');
        $this->assertNotNull(oc_extension::find($createdoc_extension['id']), 'oc_extension with given id must be in DB');
        $this->assertModelData($ocExtension, $createdoc_extension);
    }

    /**
     * @test read
     */
    public function testReadoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $dboc_extension = $this->ocExtensionRepo->find($ocExtension->id);
        $dboc_extension = $dboc_extension->toArray();
        $this->assertModelData($ocExtension->toArray(), $dboc_extension);
    }

    /**
     * @test update
     */
    public function testUpdateoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $fakeoc_extension = $this->fakeoc_extensionData();
        $updatedoc_extension = $this->ocExtensionRepo->update($fakeoc_extension, $ocExtension->id);
        $this->assertModelData($fakeoc_extension, $updatedoc_extension->toArray());
        $dboc_extension = $this->ocExtensionRepo->find($ocExtension->id);
        $this->assertModelData($fakeoc_extension, $dboc_extension->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $resp = $this->ocExtensionRepo->delete($ocExtension->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_extension::find($ocExtension->id), 'oc_extension should not exist in DB');
    }
}
