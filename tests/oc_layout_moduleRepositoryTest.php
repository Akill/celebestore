<?php

use App\Models\oc_layout_module;
use App\Repositories\oc_layout_moduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layout_moduleRepositoryTest extends TestCase
{
    use Makeoc_layout_moduleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_layout_moduleRepository
     */
    protected $ocLayoutModuleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLayoutModuleRepo = App::make(oc_layout_moduleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_layout_module()
    {
        $ocLayoutModule = $this->fakeoc_layout_moduleData();
        $createdoc_layout_module = $this->ocLayoutModuleRepo->create($ocLayoutModule);
        $createdoc_layout_module = $createdoc_layout_module->toArray();
        $this->assertArrayHasKey('id', $createdoc_layout_module);
        $this->assertNotNull($createdoc_layout_module['id'], 'Created oc_layout_module must have id specified');
        $this->assertNotNull(oc_layout_module::find($createdoc_layout_module['id']), 'oc_layout_module with given id must be in DB');
        $this->assertModelData($ocLayoutModule, $createdoc_layout_module);
    }

    /**
     * @test read
     */
    public function testReadoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $dboc_layout_module = $this->ocLayoutModuleRepo->find($ocLayoutModule->id);
        $dboc_layout_module = $dboc_layout_module->toArray();
        $this->assertModelData($ocLayoutModule->toArray(), $dboc_layout_module);
    }

    /**
     * @test update
     */
    public function testUpdateoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $fakeoc_layout_module = $this->fakeoc_layout_moduleData();
        $updatedoc_layout_module = $this->ocLayoutModuleRepo->update($fakeoc_layout_module, $ocLayoutModule->id);
        $this->assertModelData($fakeoc_layout_module, $updatedoc_layout_module->toArray());
        $dboc_layout_module = $this->ocLayoutModuleRepo->find($ocLayoutModule->id);
        $this->assertModelData($fakeoc_layout_module, $dboc_layout_module->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $resp = $this->ocLayoutModuleRepo->delete($ocLayoutModule->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_layout_module::find($ocLayoutModule->id), 'oc_layout_module should not exist in DB');
    }
}
