<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_userApiTest extends TestCase
{
    use Makeoc_userTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_user()
    {
        $ocUser = $this->fakeoc_userData();
        $this->json('POST', '/api/v1/ocUsers', $ocUser);

        $this->assertApiResponse($ocUser);
    }

    /**
     * @test
     */
    public function testReadoc_user()
    {
        $ocUser = $this->makeoc_user();
        $this->json('GET', '/api/v1/ocUsers/'.$ocUser->id);

        $this->assertApiResponse($ocUser->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_user()
    {
        $ocUser = $this->makeoc_user();
        $editedoc_user = $this->fakeoc_userData();

        $this->json('PUT', '/api/v1/ocUsers/'.$ocUser->id, $editedoc_user);

        $this->assertApiResponse($editedoc_user);
    }

    /**
     * @test
     */
    public function testDeleteoc_user()
    {
        $ocUser = $this->makeoc_user();
        $this->json('DELETE', '/api/v1/ocUsers/'.$ocUser->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocUsers/'.$ocUser->id);

        $this->assertResponseStatus(404);
    }
}
