<?php

use App\Models\oc_return;
use App\Repositories\oc_returnRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_returnRepositoryTest extends TestCase
{
    use Makeoc_returnTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_returnRepository
     */
    protected $ocReturnRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReturnRepo = App::make(oc_returnRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_return()
    {
        $ocReturn = $this->fakeoc_returnData();
        $createdoc_return = $this->ocReturnRepo->create($ocReturn);
        $createdoc_return = $createdoc_return->toArray();
        $this->assertArrayHasKey('id', $createdoc_return);
        $this->assertNotNull($createdoc_return['id'], 'Created oc_return must have id specified');
        $this->assertNotNull(oc_return::find($createdoc_return['id']), 'oc_return with given id must be in DB');
        $this->assertModelData($ocReturn, $createdoc_return);
    }

    /**
     * @test read
     */
    public function testReadoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $dboc_return = $this->ocReturnRepo->find($ocReturn->id);
        $dboc_return = $dboc_return->toArray();
        $this->assertModelData($ocReturn->toArray(), $dboc_return);
    }

    /**
     * @test update
     */
    public function testUpdateoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $fakeoc_return = $this->fakeoc_returnData();
        $updatedoc_return = $this->ocReturnRepo->update($fakeoc_return, $ocReturn->id);
        $this->assertModelData($fakeoc_return, $updatedoc_return->toArray());
        $dboc_return = $this->ocReturnRepo->find($ocReturn->id);
        $this->assertModelData($fakeoc_return, $dboc_return->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_return()
    {
        $ocReturn = $this->makeoc_return();
        $resp = $this->ocReturnRepo->delete($ocReturn->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_return::find($ocReturn->id), 'oc_return should not exist in DB');
    }
}
