<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_categoryApiTest extends TestCase
{
    use Makeoc_categoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category()
    {
        $ocCategory = $this->fakeoc_categoryData();
        $this->json('POST', '/api/v1/ocCategories', $ocCategory);

        $this->assertApiResponse($ocCategory);
    }

    /**
     * @test
     */
    public function testReadoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $this->json('GET', '/api/v1/ocCategories/'.$ocCategory->id);

        $this->assertApiResponse($ocCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $editedoc_category = $this->fakeoc_categoryData();

        $this->json('PUT', '/api/v1/ocCategories/'.$ocCategory->id, $editedoc_category);

        $this->assertApiResponse($editedoc_category);
    }

    /**
     * @test
     */
    public function testDeleteoc_category()
    {
        $ocCategory = $this->makeoc_category();
        $this->json('DELETE', '/api/v1/ocCategories/'.$ocCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategories/'.$ocCategory->id);

        $this->assertResponseStatus(404);
    }
}
