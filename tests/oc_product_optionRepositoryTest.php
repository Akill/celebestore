<?php

use App\Models\oc_product_option;
use App\Repositories\oc_product_optionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_optionRepositoryTest extends TestCase
{
    use Makeoc_product_optionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_optionRepository
     */
    protected $ocProductOptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductOptionRepo = App::make(oc_product_optionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_option()
    {
        $ocProductOption = $this->fakeoc_product_optionData();
        $createdoc_product_option = $this->ocProductOptionRepo->create($ocProductOption);
        $createdoc_product_option = $createdoc_product_option->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_option);
        $this->assertNotNull($createdoc_product_option['id'], 'Created oc_product_option must have id specified');
        $this->assertNotNull(oc_product_option::find($createdoc_product_option['id']), 'oc_product_option with given id must be in DB');
        $this->assertModelData($ocProductOption, $createdoc_product_option);
    }

    /**
     * @test read
     */
    public function testReadoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $dboc_product_option = $this->ocProductOptionRepo->find($ocProductOption->id);
        $dboc_product_option = $dboc_product_option->toArray();
        $this->assertModelData($ocProductOption->toArray(), $dboc_product_option);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $fakeoc_product_option = $this->fakeoc_product_optionData();
        $updatedoc_product_option = $this->ocProductOptionRepo->update($fakeoc_product_option, $ocProductOption->id);
        $this->assertModelData($fakeoc_product_option, $updatedoc_product_option->toArray());
        $dboc_product_option = $this->ocProductOptionRepo->find($ocProductOption->id);
        $this->assertModelData($fakeoc_product_option, $dboc_product_option->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $resp = $this->ocProductOptionRepo->delete($ocProductOption->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_option::find($ocProductOption->id), 'oc_product_option should not exist in DB');
    }
}
