<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_group_descriptionApiTest extends TestCase
{
    use Makeoc_customer_group_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->fakeoc_customer_group_descriptionData();
        $this->json('POST', '/api/v1/ocCustomerGroupDescriptions', $ocCustomerGroupDescription);

        $this->assertApiResponse($ocCustomerGroupDescription);
    }

    /**
     * @test
     */
    public function testReadoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $this->json('GET', '/api/v1/ocCustomerGroupDescriptions/'.$ocCustomerGroupDescription->id);

        $this->assertApiResponse($ocCustomerGroupDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $editedoc_customer_group_description = $this->fakeoc_customer_group_descriptionData();

        $this->json('PUT', '/api/v1/ocCustomerGroupDescriptions/'.$ocCustomerGroupDescription->id, $editedoc_customer_group_description);

        $this->assertApiResponse($editedoc_customer_group_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $this->json('DELETE', '/api/v1/ocCustomerGroupDescriptions/'.$ocCustomerGroupDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerGroupDescriptions/'.$ocCustomerGroupDescription->id);

        $this->assertResponseStatus(404);
    }
}
