<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_onlineApiTest extends TestCase
{
    use Makeoc_customer_onlineTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_online()
    {
        $ocCustomerOnline = $this->fakeoc_customer_onlineData();
        $this->json('POST', '/api/v1/ocCustomerOnlines', $ocCustomerOnline);

        $this->assertApiResponse($ocCustomerOnline);
    }

    /**
     * @test
     */
    public function testReadoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $this->json('GET', '/api/v1/ocCustomerOnlines/'.$ocCustomerOnline->id);

        $this->assertApiResponse($ocCustomerOnline->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $editedoc_customer_online = $this->fakeoc_customer_onlineData();

        $this->json('PUT', '/api/v1/ocCustomerOnlines/'.$ocCustomerOnline->id, $editedoc_customer_online);

        $this->assertApiResponse($editedoc_customer_online);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_online()
    {
        $ocCustomerOnline = $this->makeoc_customer_online();
        $this->json('DELETE', '/api/v1/ocCustomerOnlines/'.$ocCustomerOnline->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerOnlines/'.$ocCustomerOnline->id);

        $this->assertResponseStatus(404);
    }
}
