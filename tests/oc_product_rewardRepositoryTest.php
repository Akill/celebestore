<?php

use App\Models\oc_product_reward;
use App\Repositories\oc_product_rewardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_rewardRepositoryTest extends TestCase
{
    use Makeoc_product_rewardTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_rewardRepository
     */
    protected $ocProductRewardRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductRewardRepo = App::make(oc_product_rewardRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_reward()
    {
        $ocProductReward = $this->fakeoc_product_rewardData();
        $createdoc_product_reward = $this->ocProductRewardRepo->create($ocProductReward);
        $createdoc_product_reward = $createdoc_product_reward->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_reward);
        $this->assertNotNull($createdoc_product_reward['id'], 'Created oc_product_reward must have id specified');
        $this->assertNotNull(oc_product_reward::find($createdoc_product_reward['id']), 'oc_product_reward with given id must be in DB');
        $this->assertModelData($ocProductReward, $createdoc_product_reward);
    }

    /**
     * @test read
     */
    public function testReadoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $dboc_product_reward = $this->ocProductRewardRepo->find($ocProductReward->id);
        $dboc_product_reward = $dboc_product_reward->toArray();
        $this->assertModelData($ocProductReward->toArray(), $dboc_product_reward);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $fakeoc_product_reward = $this->fakeoc_product_rewardData();
        $updatedoc_product_reward = $this->ocProductRewardRepo->update($fakeoc_product_reward, $ocProductReward->id);
        $this->assertModelData($fakeoc_product_reward, $updatedoc_product_reward->toArray());
        $dboc_product_reward = $this->ocProductRewardRepo->find($ocProductReward->id);
        $this->assertModelData($fakeoc_product_reward, $dboc_product_reward->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_reward()
    {
        $ocProductReward = $this->makeoc_product_reward();
        $resp = $this->ocProductRewardRepo->delete($ocProductReward->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_reward::find($ocProductReward->id), 'oc_product_reward should not exist in DB');
    }
}
