<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_rate_to_customer_groupApiTest extends TestCase
{
    use Makeoc_tax_rate_to_customer_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->fakeoc_tax_rate_to_customer_groupData();
        $this->json('POST', '/api/v1/ocTaxRateToCustomerGroups', $ocTaxRateToCustomerGroup);

        $this->assertApiResponse($ocTaxRateToCustomerGroup);
    }

    /**
     * @test
     */
    public function testReadoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $this->json('GET', '/api/v1/ocTaxRateToCustomerGroups/'.$ocTaxRateToCustomerGroup->id);

        $this->assertApiResponse($ocTaxRateToCustomerGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $editedoc_tax_rate_to_customer_group = $this->fakeoc_tax_rate_to_customer_groupData();

        $this->json('PUT', '/api/v1/ocTaxRateToCustomerGroups/'.$ocTaxRateToCustomerGroup->id, $editedoc_tax_rate_to_customer_group);

        $this->assertApiResponse($editedoc_tax_rate_to_customer_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_tax_rate_to_customer_group()
    {
        $ocTaxRateToCustomerGroup = $this->makeoc_tax_rate_to_customer_group();
        $this->json('DELETE', '/api/v1/ocTaxRateToCustomerGroups/'.$ocTaxRateToCustomerGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocTaxRateToCustomerGroups/'.$ocTaxRateToCustomerGroup->id);

        $this->assertResponseStatus(404);
    }
}
