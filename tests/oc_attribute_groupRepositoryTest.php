<?php

use App\Models\oc_attribute_group;
use App\Repositories\oc_attribute_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_groupRepositoryTest extends TestCase
{
    use Makeoc_attribute_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_attribute_groupRepository
     */
    protected $ocAttributeGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAttributeGroupRepo = App::make(oc_attribute_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_attribute_group()
    {
        $ocAttributeGroup = $this->fakeoc_attribute_groupData();
        $createdoc_attribute_group = $this->ocAttributeGroupRepo->create($ocAttributeGroup);
        $createdoc_attribute_group = $createdoc_attribute_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_attribute_group);
        $this->assertNotNull($createdoc_attribute_group['id'], 'Created oc_attribute_group must have id specified');
        $this->assertNotNull(oc_attribute_group::find($createdoc_attribute_group['id']), 'oc_attribute_group with given id must be in DB');
        $this->assertModelData($ocAttributeGroup, $createdoc_attribute_group);
    }

    /**
     * @test read
     */
    public function testReadoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $dboc_attribute_group = $this->ocAttributeGroupRepo->find($ocAttributeGroup->id);
        $dboc_attribute_group = $dboc_attribute_group->toArray();
        $this->assertModelData($ocAttributeGroup->toArray(), $dboc_attribute_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $fakeoc_attribute_group = $this->fakeoc_attribute_groupData();
        $updatedoc_attribute_group = $this->ocAttributeGroupRepo->update($fakeoc_attribute_group, $ocAttributeGroup->id);
        $this->assertModelData($fakeoc_attribute_group, $updatedoc_attribute_group->toArray());
        $dboc_attribute_group = $this->ocAttributeGroupRepo->find($ocAttributeGroup->id);
        $this->assertModelData($fakeoc_attribute_group, $dboc_attribute_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $resp = $this->ocAttributeGroupRepo->delete($ocAttributeGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_attribute_group::find($ocAttributeGroup->id), 'oc_attribute_group should not exist in DB');
    }
}
