<?php

use App\Models\oc_event;
use App\Repositories\oc_eventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_eventRepositoryTest extends TestCase
{
    use Makeoc_eventTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_eventRepository
     */
    protected $ocEventRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocEventRepo = App::make(oc_eventRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_event()
    {
        $ocEvent = $this->fakeoc_eventData();
        $createdoc_event = $this->ocEventRepo->create($ocEvent);
        $createdoc_event = $createdoc_event->toArray();
        $this->assertArrayHasKey('id', $createdoc_event);
        $this->assertNotNull($createdoc_event['id'], 'Created oc_event must have id specified');
        $this->assertNotNull(oc_event::find($createdoc_event['id']), 'oc_event with given id must be in DB');
        $this->assertModelData($ocEvent, $createdoc_event);
    }

    /**
     * @test read
     */
    public function testReadoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $dboc_event = $this->ocEventRepo->find($ocEvent->id);
        $dboc_event = $dboc_event->toArray();
        $this->assertModelData($ocEvent->toArray(), $dboc_event);
    }

    /**
     * @test update
     */
    public function testUpdateoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $fakeoc_event = $this->fakeoc_eventData();
        $updatedoc_event = $this->ocEventRepo->update($fakeoc_event, $ocEvent->id);
        $this->assertModelData($fakeoc_event, $updatedoc_event->toArray());
        $dboc_event = $this->ocEventRepo->find($ocEvent->id);
        $this->assertModelData($fakeoc_event, $dboc_event->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_event()
    {
        $ocEvent = $this->makeoc_event();
        $resp = $this->ocEventRepo->delete($ocEvent->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_event::find($ocEvent->id), 'oc_event should not exist in DB');
    }
}
