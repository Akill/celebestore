<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_shipping_filteredApiTest extends TestCase
{
    use Makeoc_product_shipping_filteredTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->fakeoc_product_shipping_filteredData();
        $this->json('POST', '/api/v1/ocProductShippingFiltereds', $ocProductShippingFiltered);

        $this->assertApiResponse($ocProductShippingFiltered);
    }

    /**
     * @test
     */
    public function testReadoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $this->json('GET', '/api/v1/ocProductShippingFiltereds/'.$ocProductShippingFiltered->id);

        $this->assertApiResponse($ocProductShippingFiltered->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $editedoc_product_shipping_filtered = $this->fakeoc_product_shipping_filteredData();

        $this->json('PUT', '/api/v1/ocProductShippingFiltereds/'.$ocProductShippingFiltered->id, $editedoc_product_shipping_filtered);

        $this->assertApiResponse($editedoc_product_shipping_filtered);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_shipping_filtered()
    {
        $ocProductShippingFiltered = $this->makeoc_product_shipping_filtered();
        $this->json('DELETE', '/api/v1/ocProductShippingFiltereds/'.$ocProductShippingFiltered->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductShippingFiltereds/'.$ocProductShippingFiltered->id);

        $this->assertResponseStatus(404);
    }
}
