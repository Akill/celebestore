<?php

use App\Models\oc_product_recurring;
use App\Repositories\oc_product_recurringRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_recurringRepositoryTest extends TestCase
{
    use Makeoc_product_recurringTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_recurringRepository
     */
    protected $ocProductRecurringRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductRecurringRepo = App::make(oc_product_recurringRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_recurring()
    {
        $ocProductRecurring = $this->fakeoc_product_recurringData();
        $createdoc_product_recurring = $this->ocProductRecurringRepo->create($ocProductRecurring);
        $createdoc_product_recurring = $createdoc_product_recurring->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_recurring);
        $this->assertNotNull($createdoc_product_recurring['id'], 'Created oc_product_recurring must have id specified');
        $this->assertNotNull(oc_product_recurring::find($createdoc_product_recurring['id']), 'oc_product_recurring with given id must be in DB');
        $this->assertModelData($ocProductRecurring, $createdoc_product_recurring);
    }

    /**
     * @test read
     */
    public function testReadoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $dboc_product_recurring = $this->ocProductRecurringRepo->find($ocProductRecurring->id);
        $dboc_product_recurring = $dboc_product_recurring->toArray();
        $this->assertModelData($ocProductRecurring->toArray(), $dboc_product_recurring);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $fakeoc_product_recurring = $this->fakeoc_product_recurringData();
        $updatedoc_product_recurring = $this->ocProductRecurringRepo->update($fakeoc_product_recurring, $ocProductRecurring->id);
        $this->assertModelData($fakeoc_product_recurring, $updatedoc_product_recurring->toArray());
        $dboc_product_recurring = $this->ocProductRecurringRepo->find($ocProductRecurring->id);
        $this->assertModelData($fakeoc_product_recurring, $dboc_product_recurring->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $resp = $this->ocProductRecurringRepo->delete($ocProductRecurring->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_recurring::find($ocProductRecurring->id), 'oc_product_recurring should not exist in DB');
    }
}
