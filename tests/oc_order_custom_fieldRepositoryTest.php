<?php

use App\Models\oc_order_custom_field;
use App\Repositories\oc_order_custom_fieldRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_custom_fieldRepositoryTest extends TestCase
{
    use Makeoc_order_custom_fieldTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_custom_fieldRepository
     */
    protected $ocOrderCustomFieldRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderCustomFieldRepo = App::make(oc_order_custom_fieldRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_custom_field()
    {
        $ocOrderCustomField = $this->fakeoc_order_custom_fieldData();
        $createdoc_order_custom_field = $this->ocOrderCustomFieldRepo->create($ocOrderCustomField);
        $createdoc_order_custom_field = $createdoc_order_custom_field->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_custom_field);
        $this->assertNotNull($createdoc_order_custom_field['id'], 'Created oc_order_custom_field must have id specified');
        $this->assertNotNull(oc_order_custom_field::find($createdoc_order_custom_field['id']), 'oc_order_custom_field with given id must be in DB');
        $this->assertModelData($ocOrderCustomField, $createdoc_order_custom_field);
    }

    /**
     * @test read
     */
    public function testReadoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $dboc_order_custom_field = $this->ocOrderCustomFieldRepo->find($ocOrderCustomField->id);
        $dboc_order_custom_field = $dboc_order_custom_field->toArray();
        $this->assertModelData($ocOrderCustomField->toArray(), $dboc_order_custom_field);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $fakeoc_order_custom_field = $this->fakeoc_order_custom_fieldData();
        $updatedoc_order_custom_field = $this->ocOrderCustomFieldRepo->update($fakeoc_order_custom_field, $ocOrderCustomField->id);
        $this->assertModelData($fakeoc_order_custom_field, $updatedoc_order_custom_field->toArray());
        $dboc_order_custom_field = $this->ocOrderCustomFieldRepo->find($ocOrderCustomField->id);
        $this->assertModelData($fakeoc_order_custom_field, $dboc_order_custom_field->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_custom_field()
    {
        $ocOrderCustomField = $this->makeoc_order_custom_field();
        $resp = $this->ocOrderCustomFieldRepo->delete($ocOrderCustomField->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_custom_field::find($ocOrderCustomField->id), 'oc_order_custom_field should not exist in DB');
    }
}
