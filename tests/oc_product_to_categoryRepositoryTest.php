<?php

use App\Models\oc_product_to_category;
use App\Repositories\oc_product_to_categoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_categoryRepositoryTest extends TestCase
{
    use Makeoc_product_to_categoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_to_categoryRepository
     */
    protected $ocProductToCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductToCategoryRepo = App::make(oc_product_to_categoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_to_category()
    {
        $ocProductToCategory = $this->fakeoc_product_to_categoryData();
        $createdoc_product_to_category = $this->ocProductToCategoryRepo->create($ocProductToCategory);
        $createdoc_product_to_category = $createdoc_product_to_category->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_to_category);
        $this->assertNotNull($createdoc_product_to_category['id'], 'Created oc_product_to_category must have id specified');
        $this->assertNotNull(oc_product_to_category::find($createdoc_product_to_category['id']), 'oc_product_to_category with given id must be in DB');
        $this->assertModelData($ocProductToCategory, $createdoc_product_to_category);
    }

    /**
     * @test read
     */
    public function testReadoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $dboc_product_to_category = $this->ocProductToCategoryRepo->find($ocProductToCategory->id);
        $dboc_product_to_category = $dboc_product_to_category->toArray();
        $this->assertModelData($ocProductToCategory->toArray(), $dboc_product_to_category);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $fakeoc_product_to_category = $this->fakeoc_product_to_categoryData();
        $updatedoc_product_to_category = $this->ocProductToCategoryRepo->update($fakeoc_product_to_category, $ocProductToCategory->id);
        $this->assertModelData($fakeoc_product_to_category, $updatedoc_product_to_category->toArray());
        $dboc_product_to_category = $this->ocProductToCategoryRepo->find($ocProductToCategory->id);
        $this->assertModelData($fakeoc_product_to_category, $dboc_product_to_category->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_to_category()
    {
        $ocProductToCategory = $this->makeoc_product_to_category();
        $resp = $this->ocProductToCategoryRepo->delete($ocProductToCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_to_category::find($ocProductToCategory->id), 'oc_product_to_category should not exist in DB');
    }
}
