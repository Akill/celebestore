<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_extensionApiTest extends TestCase
{
    use Makeoc_extensionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_extension()
    {
        $ocExtension = $this->fakeoc_extensionData();
        $this->json('POST', '/api/v1/ocExtensions', $ocExtension);

        $this->assertApiResponse($ocExtension);
    }

    /**
     * @test
     */
    public function testReadoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $this->json('GET', '/api/v1/ocExtensions/'.$ocExtension->id);

        $this->assertApiResponse($ocExtension->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $editedoc_extension = $this->fakeoc_extensionData();

        $this->json('PUT', '/api/v1/ocExtensions/'.$ocExtension->id, $editedoc_extension);

        $this->assertApiResponse($editedoc_extension);
    }

    /**
     * @test
     */
    public function testDeleteoc_extension()
    {
        $ocExtension = $this->makeoc_extension();
        $this->json('DELETE', '/api/v1/ocExtensions/'.$ocExtension->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocExtensions/'.$ocExtension->id);

        $this->assertResponseStatus(404);
    }
}
