<?php

use App\Models\oc_url_alias;
use App\Repositories\oc_url_aliasRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_url_aliasRepositoryTest extends TestCase
{
    use Makeoc_url_aliasTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_url_aliasRepository
     */
    protected $ocUrlAliasRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocUrlAliasRepo = App::make(oc_url_aliasRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_url_alias()
    {
        $ocUrlAlias = $this->fakeoc_url_aliasData();
        $createdoc_url_alias = $this->ocUrlAliasRepo->create($ocUrlAlias);
        $createdoc_url_alias = $createdoc_url_alias->toArray();
        $this->assertArrayHasKey('id', $createdoc_url_alias);
        $this->assertNotNull($createdoc_url_alias['id'], 'Created oc_url_alias must have id specified');
        $this->assertNotNull(oc_url_alias::find($createdoc_url_alias['id']), 'oc_url_alias with given id must be in DB');
        $this->assertModelData($ocUrlAlias, $createdoc_url_alias);
    }

    /**
     * @test read
     */
    public function testReadoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $dboc_url_alias = $this->ocUrlAliasRepo->find($ocUrlAlias->id);
        $dboc_url_alias = $dboc_url_alias->toArray();
        $this->assertModelData($ocUrlAlias->toArray(), $dboc_url_alias);
    }

    /**
     * @test update
     */
    public function testUpdateoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $fakeoc_url_alias = $this->fakeoc_url_aliasData();
        $updatedoc_url_alias = $this->ocUrlAliasRepo->update($fakeoc_url_alias, $ocUrlAlias->id);
        $this->assertModelData($fakeoc_url_alias, $updatedoc_url_alias->toArray());
        $dboc_url_alias = $this->ocUrlAliasRepo->find($ocUrlAlias->id);
        $this->assertModelData($fakeoc_url_alias, $dboc_url_alias->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $resp = $this->ocUrlAliasRepo->delete($ocUrlAlias->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_url_alias::find($ocUrlAlias->id), 'oc_url_alias should not exist in DB');
    }
}
