<?php

use App\Models\oc_option_value_description;
use App\Repositories\oc_option_value_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_value_descriptionRepositoryTest extends TestCase
{
    use Makeoc_option_value_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_option_value_descriptionRepository
     */
    protected $ocOptionValueDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOptionValueDescriptionRepo = App::make(oc_option_value_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_option_value_description()
    {
        $ocOptionValueDescription = $this->fakeoc_option_value_descriptionData();
        $createdoc_option_value_description = $this->ocOptionValueDescriptionRepo->create($ocOptionValueDescription);
        $createdoc_option_value_description = $createdoc_option_value_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_option_value_description);
        $this->assertNotNull($createdoc_option_value_description['id'], 'Created oc_option_value_description must have id specified');
        $this->assertNotNull(oc_option_value_description::find($createdoc_option_value_description['id']), 'oc_option_value_description with given id must be in DB');
        $this->assertModelData($ocOptionValueDescription, $createdoc_option_value_description);
    }

    /**
     * @test read
     */
    public function testReadoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $dboc_option_value_description = $this->ocOptionValueDescriptionRepo->find($ocOptionValueDescription->id);
        $dboc_option_value_description = $dboc_option_value_description->toArray();
        $this->assertModelData($ocOptionValueDescription->toArray(), $dboc_option_value_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $fakeoc_option_value_description = $this->fakeoc_option_value_descriptionData();
        $updatedoc_option_value_description = $this->ocOptionValueDescriptionRepo->update($fakeoc_option_value_description, $ocOptionValueDescription->id);
        $this->assertModelData($fakeoc_option_value_description, $updatedoc_option_value_description->toArray());
        $dboc_option_value_description = $this->ocOptionValueDescriptionRepo->find($ocOptionValueDescription->id);
        $this->assertModelData($fakeoc_option_value_description, $dboc_option_value_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $resp = $this->ocOptionValueDescriptionRepo->delete($ocOptionValueDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_option_value_description::find($ocOptionValueDescription->id), 'oc_option_value_description should not exist in DB');
    }
}
