<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_voucherApiTest extends TestCase
{
    use Makeoc_order_voucherTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order_voucher()
    {
        $ocOrderVoucher = $this->fakeoc_order_voucherData();
        $this->json('POST', '/api/v1/ocOrderVouchers', $ocOrderVoucher);

        $this->assertApiResponse($ocOrderVoucher);
    }

    /**
     * @test
     */
    public function testReadoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $this->json('GET', '/api/v1/ocOrderVouchers/'.$ocOrderVoucher->id);

        $this->assertApiResponse($ocOrderVoucher->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $editedoc_order_voucher = $this->fakeoc_order_voucherData();

        $this->json('PUT', '/api/v1/ocOrderVouchers/'.$ocOrderVoucher->id, $editedoc_order_voucher);

        $this->assertApiResponse($editedoc_order_voucher);
    }

    /**
     * @test
     */
    public function testDeleteoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $this->json('DELETE', '/api/v1/ocOrderVouchers/'.$ocOrderVoucher->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrderVouchers/'.$ocOrderVoucher->id);

        $this->assertResponseStatus(404);
    }
}
