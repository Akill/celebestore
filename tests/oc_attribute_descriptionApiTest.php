<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_descriptionApiTest extends TestCase
{
    use Makeoc_attribute_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_attribute_description()
    {
        $ocAttributeDescription = $this->fakeoc_attribute_descriptionData();
        $this->json('POST', '/api/v1/ocAttributeDescriptions', $ocAttributeDescription);

        $this->assertApiResponse($ocAttributeDescription);
    }

    /**
     * @test
     */
    public function testReadoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $this->json('GET', '/api/v1/ocAttributeDescriptions/'.$ocAttributeDescription->id);

        $this->assertApiResponse($ocAttributeDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $editedoc_attribute_description = $this->fakeoc_attribute_descriptionData();

        $this->json('PUT', '/api/v1/ocAttributeDescriptions/'.$ocAttributeDescription->id, $editedoc_attribute_description);

        $this->assertApiResponse($editedoc_attribute_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_attribute_description()
    {
        $ocAttributeDescription = $this->makeoc_attribute_description();
        $this->json('DELETE', '/api/v1/ocAttributeDescriptions/'.$ocAttributeDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAttributeDescriptions/'.$ocAttributeDescription->id);

        $this->assertResponseStatus(404);
    }
}
