<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_url_aliasApiTest extends TestCase
{
    use Makeoc_url_aliasTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_url_alias()
    {
        $ocUrlAlias = $this->fakeoc_url_aliasData();
        $this->json('POST', '/api/v1/ocUrlAliases', $ocUrlAlias);

        $this->assertApiResponse($ocUrlAlias);
    }

    /**
     * @test
     */
    public function testReadoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $this->json('GET', '/api/v1/ocUrlAliases/'.$ocUrlAlias->id);

        $this->assertApiResponse($ocUrlAlias->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $editedoc_url_alias = $this->fakeoc_url_aliasData();

        $this->json('PUT', '/api/v1/ocUrlAliases/'.$ocUrlAlias->id, $editedoc_url_alias);

        $this->assertApiResponse($editedoc_url_alias);
    }

    /**
     * @test
     */
    public function testDeleteoc_url_alias()
    {
        $ocUrlAlias = $this->makeoc_url_alias();
        $this->json('DELETE', '/api/v1/ocUrlAliases/'.$ocUrlAlias->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocUrlAliases/'.$ocUrlAlias->id);

        $this->assertResponseStatus(404);
    }
}
