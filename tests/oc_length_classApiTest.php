<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_length_classApiTest extends TestCase
{
    use Makeoc_length_classTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_length_class()
    {
        $ocLengthClass = $this->fakeoc_length_classData();
        $this->json('POST', '/api/v1/ocLengthClasses', $ocLengthClass);

        $this->assertApiResponse($ocLengthClass);
    }

    /**
     * @test
     */
    public function testReadoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $this->json('GET', '/api/v1/ocLengthClasses/'.$ocLengthClass->id);

        $this->assertApiResponse($ocLengthClass->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $editedoc_length_class = $this->fakeoc_length_classData();

        $this->json('PUT', '/api/v1/ocLengthClasses/'.$ocLengthClass->id, $editedoc_length_class);

        $this->assertApiResponse($editedoc_length_class);
    }

    /**
     * @test
     */
    public function testDeleteoc_length_class()
    {
        $ocLengthClass = $this->makeoc_length_class();
        $this->json('DELETE', '/api/v1/ocLengthClasses/'.$ocLengthClass->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLengthClasses/'.$ocLengthClass->id);

        $this->assertResponseStatus(404);
    }
}
