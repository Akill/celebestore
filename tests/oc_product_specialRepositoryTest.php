<?php

use App\Models\oc_product_special;
use App\Repositories\oc_product_specialRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_specialRepositoryTest extends TestCase
{
    use Makeoc_product_specialTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_specialRepository
     */
    protected $ocProductSpecialRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductSpecialRepo = App::make(oc_product_specialRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_special()
    {
        $ocProductSpecial = $this->fakeoc_product_specialData();
        $createdoc_product_special = $this->ocProductSpecialRepo->create($ocProductSpecial);
        $createdoc_product_special = $createdoc_product_special->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_special);
        $this->assertNotNull($createdoc_product_special['id'], 'Created oc_product_special must have id specified');
        $this->assertNotNull(oc_product_special::find($createdoc_product_special['id']), 'oc_product_special with given id must be in DB');
        $this->assertModelData($ocProductSpecial, $createdoc_product_special);
    }

    /**
     * @test read
     */
    public function testReadoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $dboc_product_special = $this->ocProductSpecialRepo->find($ocProductSpecial->id);
        $dboc_product_special = $dboc_product_special->toArray();
        $this->assertModelData($ocProductSpecial->toArray(), $dboc_product_special);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $fakeoc_product_special = $this->fakeoc_product_specialData();
        $updatedoc_product_special = $this->ocProductSpecialRepo->update($fakeoc_product_special, $ocProductSpecial->id);
        $this->assertModelData($fakeoc_product_special, $updatedoc_product_special->toArray());
        $dboc_product_special = $this->ocProductSpecialRepo->find($ocProductSpecial->id);
        $this->assertModelData($fakeoc_product_special, $dboc_product_special->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $resp = $this->ocProductSpecialRepo->delete($ocProductSpecial->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_special::find($ocProductSpecial->id), 'oc_product_special should not exist in DB');
    }
}
