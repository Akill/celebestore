<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_optionApiTest extends TestCase
{
    use Makeoc_product_optionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_option()
    {
        $ocProductOption = $this->fakeoc_product_optionData();
        $this->json('POST', '/api/v1/ocProductOptions', $ocProductOption);

        $this->assertApiResponse($ocProductOption);
    }

    /**
     * @test
     */
    public function testReadoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $this->json('GET', '/api/v1/ocProductOptions/'.$ocProductOption->id);

        $this->assertApiResponse($ocProductOption->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $editedoc_product_option = $this->fakeoc_product_optionData();

        $this->json('PUT', '/api/v1/ocProductOptions/'.$ocProductOption->id, $editedoc_product_option);

        $this->assertApiResponse($editedoc_product_option);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_option()
    {
        $ocProductOption = $this->makeoc_product_option();
        $this->json('DELETE', '/api/v1/ocProductOptions/'.$ocProductOption->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductOptions/'.$ocProductOption->id);

        $this->assertResponseStatus(404);
    }
}
