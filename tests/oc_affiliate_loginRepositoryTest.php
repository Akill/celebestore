<?php

use App\Models\oc_affiliate_login;
use App\Repositories\oc_affiliate_loginRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_loginRepositoryTest extends TestCase
{
    use Makeoc_affiliate_loginTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_affiliate_loginRepository
     */
    protected $ocAffiliateLoginRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAffiliateLoginRepo = App::make(oc_affiliate_loginRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->fakeoc_affiliate_loginData();
        $createdoc_affiliate_login = $this->ocAffiliateLoginRepo->create($ocAffiliateLogin);
        $createdoc_affiliate_login = $createdoc_affiliate_login->toArray();
        $this->assertArrayHasKey('id', $createdoc_affiliate_login);
        $this->assertNotNull($createdoc_affiliate_login['id'], 'Created oc_affiliate_login must have id specified');
        $this->assertNotNull(oc_affiliate_login::find($createdoc_affiliate_login['id']), 'oc_affiliate_login with given id must be in DB');
        $this->assertModelData($ocAffiliateLogin, $createdoc_affiliate_login);
    }

    /**
     * @test read
     */
    public function testReadoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $dboc_affiliate_login = $this->ocAffiliateLoginRepo->find($ocAffiliateLogin->id);
        $dboc_affiliate_login = $dboc_affiliate_login->toArray();
        $this->assertModelData($ocAffiliateLogin->toArray(), $dboc_affiliate_login);
    }

    /**
     * @test update
     */
    public function testUpdateoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $fakeoc_affiliate_login = $this->fakeoc_affiliate_loginData();
        $updatedoc_affiliate_login = $this->ocAffiliateLoginRepo->update($fakeoc_affiliate_login, $ocAffiliateLogin->id);
        $this->assertModelData($fakeoc_affiliate_login, $updatedoc_affiliate_login->toArray());
        $dboc_affiliate_login = $this->ocAffiliateLoginRepo->find($ocAffiliateLogin->id);
        $this->assertModelData($fakeoc_affiliate_login, $dboc_affiliate_login->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $resp = $this->ocAffiliateLoginRepo->delete($ocAffiliateLogin->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_affiliate_login::find($ocAffiliateLogin->id), 'oc_affiliate_login should not exist in DB');
    }
}
