<?php

use App\Models\oc_stock_status;
use App\Repositories\oc_stock_statusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_stock_statusRepositoryTest extends TestCase
{
    use Makeoc_stock_statusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_stock_statusRepository
     */
    protected $ocStockStatusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocStockStatusRepo = App::make(oc_stock_statusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_stock_status()
    {
        $ocStockStatus = $this->fakeoc_stock_statusData();
        $createdoc_stock_status = $this->ocStockStatusRepo->create($ocStockStatus);
        $createdoc_stock_status = $createdoc_stock_status->toArray();
        $this->assertArrayHasKey('id', $createdoc_stock_status);
        $this->assertNotNull($createdoc_stock_status['id'], 'Created oc_stock_status must have id specified');
        $this->assertNotNull(oc_stock_status::find($createdoc_stock_status['id']), 'oc_stock_status with given id must be in DB');
        $this->assertModelData($ocStockStatus, $createdoc_stock_status);
    }

    /**
     * @test read
     */
    public function testReadoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $dboc_stock_status = $this->ocStockStatusRepo->find($ocStockStatus->id);
        $dboc_stock_status = $dboc_stock_status->toArray();
        $this->assertModelData($ocStockStatus->toArray(), $dboc_stock_status);
    }

    /**
     * @test update
     */
    public function testUpdateoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $fakeoc_stock_status = $this->fakeoc_stock_statusData();
        $updatedoc_stock_status = $this->ocStockStatusRepo->update($fakeoc_stock_status, $ocStockStatus->id);
        $this->assertModelData($fakeoc_stock_status, $updatedoc_stock_status->toArray());
        $dboc_stock_status = $this->ocStockStatusRepo->find($ocStockStatus->id);
        $this->assertModelData($fakeoc_stock_status, $dboc_stock_status->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_stock_status()
    {
        $ocStockStatus = $this->makeoc_stock_status();
        $resp = $this->ocStockStatusRepo->delete($ocStockStatus->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_stock_status::find($ocStockStatus->id), 'oc_stock_status should not exist in DB');
    }
}
