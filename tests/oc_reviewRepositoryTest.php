<?php

use App\Models\oc_review;
use App\Repositories\oc_reviewRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_reviewRepositoryTest extends TestCase
{
    use Makeoc_reviewTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_reviewRepository
     */
    protected $ocReviewRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocReviewRepo = App::make(oc_reviewRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_review()
    {
        $ocReview = $this->fakeoc_reviewData();
        $createdoc_review = $this->ocReviewRepo->create($ocReview);
        $createdoc_review = $createdoc_review->toArray();
        $this->assertArrayHasKey('id', $createdoc_review);
        $this->assertNotNull($createdoc_review['id'], 'Created oc_review must have id specified');
        $this->assertNotNull(oc_review::find($createdoc_review['id']), 'oc_review with given id must be in DB');
        $this->assertModelData($ocReview, $createdoc_review);
    }

    /**
     * @test read
     */
    public function testReadoc_review()
    {
        $ocReview = $this->makeoc_review();
        $dboc_review = $this->ocReviewRepo->find($ocReview->id);
        $dboc_review = $dboc_review->toArray();
        $this->assertModelData($ocReview->toArray(), $dboc_review);
    }

    /**
     * @test update
     */
    public function testUpdateoc_review()
    {
        $ocReview = $this->makeoc_review();
        $fakeoc_review = $this->fakeoc_reviewData();
        $updatedoc_review = $this->ocReviewRepo->update($fakeoc_review, $ocReview->id);
        $this->assertModelData($fakeoc_review, $updatedoc_review->toArray());
        $dboc_review = $this->ocReviewRepo->find($ocReview->id);
        $this->assertModelData($fakeoc_review, $dboc_review->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_review()
    {
        $ocReview = $this->makeoc_review();
        $resp = $this->ocReviewRepo->delete($ocReview->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_review::find($ocReview->id), 'oc_review should not exist in DB');
    }
}
