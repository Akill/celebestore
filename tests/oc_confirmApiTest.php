<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_confirmApiTest extends TestCase
{
    use Makeoc_confirmTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_confirm()
    {
        $ocConfirm = $this->fakeoc_confirmData();
        $this->json('POST', '/api/v1/ocConfirms', $ocConfirm);

        $this->assertApiResponse($ocConfirm);
    }

    /**
     * @test
     */
    public function testReadoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $this->json('GET', '/api/v1/ocConfirms/'.$ocConfirm->id);

        $this->assertApiResponse($ocConfirm->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $editedoc_confirm = $this->fakeoc_confirmData();

        $this->json('PUT', '/api/v1/ocConfirms/'.$ocConfirm->id, $editedoc_confirm);

        $this->assertApiResponse($editedoc_confirm);
    }

    /**
     * @test
     */
    public function testDeleteoc_confirm()
    {
        $ocConfirm = $this->makeoc_confirm();
        $this->json('DELETE', '/api/v1/ocConfirms/'.$ocConfirm->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocConfirms/'.$ocConfirm->id);

        $this->assertResponseStatus(404);
    }
}
