<?php

use App\Models\oc_order_product;
use App\Repositories\oc_order_productRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_productRepositoryTest extends TestCase
{
    use Makeoc_order_productTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_productRepository
     */
    protected $ocOrderProductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderProductRepo = App::make(oc_order_productRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_product()
    {
        $ocOrderProduct = $this->fakeoc_order_productData();
        $createdoc_order_product = $this->ocOrderProductRepo->create($ocOrderProduct);
        $createdoc_order_product = $createdoc_order_product->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_product);
        $this->assertNotNull($createdoc_order_product['id'], 'Created oc_order_product must have id specified');
        $this->assertNotNull(oc_order_product::find($createdoc_order_product['id']), 'oc_order_product with given id must be in DB');
        $this->assertModelData($ocOrderProduct, $createdoc_order_product);
    }

    /**
     * @test read
     */
    public function testReadoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $dboc_order_product = $this->ocOrderProductRepo->find($ocOrderProduct->id);
        $dboc_order_product = $dboc_order_product->toArray();
        $this->assertModelData($ocOrderProduct->toArray(), $dboc_order_product);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $fakeoc_order_product = $this->fakeoc_order_productData();
        $updatedoc_order_product = $this->ocOrderProductRepo->update($fakeoc_order_product, $ocOrderProduct->id);
        $this->assertModelData($fakeoc_order_product, $updatedoc_order_product->toArray());
        $dboc_order_product = $this->ocOrderProductRepo->find($ocOrderProduct->id);
        $this->assertModelData($fakeoc_order_product, $dboc_order_product->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_product()
    {
        $ocOrderProduct = $this->makeoc_order_product();
        $resp = $this->ocOrderProductRepo->delete($ocOrderProduct->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_product::find($ocOrderProduct->id), 'oc_order_product should not exist in DB');
    }
}
