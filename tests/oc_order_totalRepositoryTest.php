<?php

use App\Models\oc_order_total;
use App\Repositories\oc_order_totalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_totalRepositoryTest extends TestCase
{
    use Makeoc_order_totalTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_totalRepository
     */
    protected $ocOrderTotalRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderTotalRepo = App::make(oc_order_totalRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_total()
    {
        $ocOrderTotal = $this->fakeoc_order_totalData();
        $createdoc_order_total = $this->ocOrderTotalRepo->create($ocOrderTotal);
        $createdoc_order_total = $createdoc_order_total->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_total);
        $this->assertNotNull($createdoc_order_total['id'], 'Created oc_order_total must have id specified');
        $this->assertNotNull(oc_order_total::find($createdoc_order_total['id']), 'oc_order_total with given id must be in DB');
        $this->assertModelData($ocOrderTotal, $createdoc_order_total);
    }

    /**
     * @test read
     */
    public function testReadoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $dboc_order_total = $this->ocOrderTotalRepo->find($ocOrderTotal->id);
        $dboc_order_total = $dboc_order_total->toArray();
        $this->assertModelData($ocOrderTotal->toArray(), $dboc_order_total);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $fakeoc_order_total = $this->fakeoc_order_totalData();
        $updatedoc_order_total = $this->ocOrderTotalRepo->update($fakeoc_order_total, $ocOrderTotal->id);
        $this->assertModelData($fakeoc_order_total, $updatedoc_order_total->toArray());
        $dboc_order_total = $this->ocOrderTotalRepo->find($ocOrderTotal->id);
        $this->assertModelData($fakeoc_order_total, $dboc_order_total->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_total()
    {
        $ocOrderTotal = $this->makeoc_order_total();
        $resp = $this->ocOrderTotalRepo->delete($ocOrderTotal->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_total::find($ocOrderTotal->id), 'oc_order_total should not exist in DB');
    }
}
