<?php

use App\Models\oc_tax_rule;
use App\Repositories\oc_tax_ruleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_ruleRepositoryTest extends TestCase
{
    use Makeoc_tax_ruleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_tax_ruleRepository
     */
    protected $ocTaxRuleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocTaxRuleRepo = App::make(oc_tax_ruleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_tax_rule()
    {
        $ocTaxRule = $this->fakeoc_tax_ruleData();
        $createdoc_tax_rule = $this->ocTaxRuleRepo->create($ocTaxRule);
        $createdoc_tax_rule = $createdoc_tax_rule->toArray();
        $this->assertArrayHasKey('id', $createdoc_tax_rule);
        $this->assertNotNull($createdoc_tax_rule['id'], 'Created oc_tax_rule must have id specified');
        $this->assertNotNull(oc_tax_rule::find($createdoc_tax_rule['id']), 'oc_tax_rule with given id must be in DB');
        $this->assertModelData($ocTaxRule, $createdoc_tax_rule);
    }

    /**
     * @test read
     */
    public function testReadoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $dboc_tax_rule = $this->ocTaxRuleRepo->find($ocTaxRule->id);
        $dboc_tax_rule = $dboc_tax_rule->toArray();
        $this->assertModelData($ocTaxRule->toArray(), $dboc_tax_rule);
    }

    /**
     * @test update
     */
    public function testUpdateoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $fakeoc_tax_rule = $this->fakeoc_tax_ruleData();
        $updatedoc_tax_rule = $this->ocTaxRuleRepo->update($fakeoc_tax_rule, $ocTaxRule->id);
        $this->assertModelData($fakeoc_tax_rule, $updatedoc_tax_rule->toArray());
        $dboc_tax_rule = $this->ocTaxRuleRepo->find($ocTaxRule->id);
        $this->assertModelData($fakeoc_tax_rule, $dboc_tax_rule->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_tax_rule()
    {
        $ocTaxRule = $this->makeoc_tax_rule();
        $resp = $this->ocTaxRuleRepo->delete($ocTaxRule->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_tax_rule::find($ocTaxRule->id), 'oc_tax_rule should not exist in DB');
    }
}
