<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_descriptionApiTest extends TestCase
{
    use Makeoc_filter_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_filter_description()
    {
        $ocFilterDescription = $this->fakeoc_filter_descriptionData();
        $this->json('POST', '/api/v1/ocFilterDescriptions', $ocFilterDescription);

        $this->assertApiResponse($ocFilterDescription);
    }

    /**
     * @test
     */
    public function testReadoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $this->json('GET', '/api/v1/ocFilterDescriptions/'.$ocFilterDescription->id);

        $this->assertApiResponse($ocFilterDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $editedoc_filter_description = $this->fakeoc_filter_descriptionData();

        $this->json('PUT', '/api/v1/ocFilterDescriptions/'.$ocFilterDescription->id, $editedoc_filter_description);

        $this->assertApiResponse($editedoc_filter_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $this->json('DELETE', '/api/v1/ocFilterDescriptions/'.$ocFilterDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocFilterDescriptions/'.$ocFilterDescription->id);

        $this->assertResponseStatus(404);
    }
}
