<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_weight_classApiTest extends TestCase
{
    use Makeoc_weight_classTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_weight_class()
    {
        $ocWeightClass = $this->fakeoc_weight_classData();
        $this->json('POST', '/api/v1/ocWeightClasses', $ocWeightClass);

        $this->assertApiResponse($ocWeightClass);
    }

    /**
     * @test
     */
    public function testReadoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $this->json('GET', '/api/v1/ocWeightClasses/'.$ocWeightClass->id);

        $this->assertApiResponse($ocWeightClass->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $editedoc_weight_class = $this->fakeoc_weight_classData();

        $this->json('PUT', '/api/v1/ocWeightClasses/'.$ocWeightClass->id, $editedoc_weight_class);

        $this->assertApiResponse($editedoc_weight_class);
    }

    /**
     * @test
     */
    public function testDeleteoc_weight_class()
    {
        $ocWeightClass = $this->makeoc_weight_class();
        $this->json('DELETE', '/api/v1/ocWeightClasses/'.$ocWeightClass->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocWeightClasses/'.$ocWeightClass->id);

        $this->assertResponseStatus(404);
    }
}
