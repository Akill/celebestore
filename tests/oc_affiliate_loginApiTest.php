<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_loginApiTest extends TestCase
{
    use Makeoc_affiliate_loginTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->fakeoc_affiliate_loginData();
        $this->json('POST', '/api/v1/ocAffiliateLogins', $ocAffiliateLogin);

        $this->assertApiResponse($ocAffiliateLogin);
    }

    /**
     * @test
     */
    public function testReadoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $this->json('GET', '/api/v1/ocAffiliateLogins/'.$ocAffiliateLogin->id);

        $this->assertApiResponse($ocAffiliateLogin->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $editedoc_affiliate_login = $this->fakeoc_affiliate_loginData();

        $this->json('PUT', '/api/v1/ocAffiliateLogins/'.$ocAffiliateLogin->id, $editedoc_affiliate_login);

        $this->assertApiResponse($editedoc_affiliate_login);
    }

    /**
     * @test
     */
    public function testDeleteoc_affiliate_login()
    {
        $ocAffiliateLogin = $this->makeoc_affiliate_login();
        $this->json('DELETE', '/api/v1/ocAffiliateLogins/'.$ocAffiliateLogin->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAffiliateLogins/'.$ocAffiliateLogin->id);

        $this->assertResponseStatus(404);
    }
}
