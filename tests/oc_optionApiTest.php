<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_optionApiTest extends TestCase
{
    use Makeoc_optionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_option()
    {
        $ocOption = $this->fakeoc_optionData();
        $this->json('POST', '/api/v1/ocOptions', $ocOption);

        $this->assertApiResponse($ocOption);
    }

    /**
     * @test
     */
    public function testReadoc_option()
    {
        $ocOption = $this->makeoc_option();
        $this->json('GET', '/api/v1/ocOptions/'.$ocOption->id);

        $this->assertApiResponse($ocOption->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_option()
    {
        $ocOption = $this->makeoc_option();
        $editedoc_option = $this->fakeoc_optionData();

        $this->json('PUT', '/api/v1/ocOptions/'.$ocOption->id, $editedoc_option);

        $this->assertApiResponse($editedoc_option);
    }

    /**
     * @test
     */
    public function testDeleteoc_option()
    {
        $ocOption = $this->makeoc_option();
        $this->json('DELETE', '/api/v1/ocOptions/'.$ocOption->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOptions/'.$ocOption->id);

        $this->assertResponseStatus(404);
    }
}
