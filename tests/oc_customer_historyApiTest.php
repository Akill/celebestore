<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_historyApiTest extends TestCase
{
    use Makeoc_customer_historyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_history()
    {
        $ocCustomerHistory = $this->fakeoc_customer_historyData();
        $this->json('POST', '/api/v1/ocCustomerHistories', $ocCustomerHistory);

        $this->assertApiResponse($ocCustomerHistory);
    }

    /**
     * @test
     */
    public function testReadoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $this->json('GET', '/api/v1/ocCustomerHistories/'.$ocCustomerHistory->id);

        $this->assertApiResponse($ocCustomerHistory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $editedoc_customer_history = $this->fakeoc_customer_historyData();

        $this->json('PUT', '/api/v1/ocCustomerHistories/'.$ocCustomerHistory->id, $editedoc_customer_history);

        $this->assertApiResponse($editedoc_customer_history);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_history()
    {
        $ocCustomerHistory = $this->makeoc_customer_history();
        $this->json('DELETE', '/api/v1/ocCustomerHistories/'.$ocCustomerHistory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerHistories/'.$ocCustomerHistory->id);

        $this->assertResponseStatus(404);
    }
}
