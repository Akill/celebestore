<?php

use App\Models\oc_coupon_product;
use App\Repositories\oc_coupon_productRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_productRepositoryTest extends TestCase
{
    use Makeoc_coupon_productTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_coupon_productRepository
     */
    protected $ocCouponProductRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCouponProductRepo = App::make(oc_coupon_productRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_coupon_product()
    {
        $ocCouponProduct = $this->fakeoc_coupon_productData();
        $createdoc_coupon_product = $this->ocCouponProductRepo->create($ocCouponProduct);
        $createdoc_coupon_product = $createdoc_coupon_product->toArray();
        $this->assertArrayHasKey('id', $createdoc_coupon_product);
        $this->assertNotNull($createdoc_coupon_product['id'], 'Created oc_coupon_product must have id specified');
        $this->assertNotNull(oc_coupon_product::find($createdoc_coupon_product['id']), 'oc_coupon_product with given id must be in DB');
        $this->assertModelData($ocCouponProduct, $createdoc_coupon_product);
    }

    /**
     * @test read
     */
    public function testReadoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $dboc_coupon_product = $this->ocCouponProductRepo->find($ocCouponProduct->id);
        $dboc_coupon_product = $dboc_coupon_product->toArray();
        $this->assertModelData($ocCouponProduct->toArray(), $dboc_coupon_product);
    }

    /**
     * @test update
     */
    public function testUpdateoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $fakeoc_coupon_product = $this->fakeoc_coupon_productData();
        $updatedoc_coupon_product = $this->ocCouponProductRepo->update($fakeoc_coupon_product, $ocCouponProduct->id);
        $this->assertModelData($fakeoc_coupon_product, $updatedoc_coupon_product->toArray());
        $dboc_coupon_product = $this->ocCouponProductRepo->find($ocCouponProduct->id);
        $this->assertModelData($fakeoc_coupon_product, $dboc_coupon_product->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_coupon_product()
    {
        $ocCouponProduct = $this->makeoc_coupon_product();
        $resp = $this->ocCouponProductRepo->delete($ocCouponProduct->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_coupon_product::find($ocCouponProduct->id), 'oc_coupon_product should not exist in DB');
    }
}
