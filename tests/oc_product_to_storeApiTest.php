<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_storeApiTest extends TestCase
{
    use Makeoc_product_to_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_to_store()
    {
        $ocProductToStore = $this->fakeoc_product_to_storeData();
        $this->json('POST', '/api/v1/ocProductToStores', $ocProductToStore);

        $this->assertApiResponse($ocProductToStore);
    }

    /**
     * @test
     */
    public function testReadoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $this->json('GET', '/api/v1/ocProductToStores/'.$ocProductToStore->id);

        $this->assertApiResponse($ocProductToStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $editedoc_product_to_store = $this->fakeoc_product_to_storeData();

        $this->json('PUT', '/api/v1/ocProductToStores/'.$ocProductToStore->id, $editedoc_product_to_store);

        $this->assertApiResponse($editedoc_product_to_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_to_store()
    {
        $ocProductToStore = $this->makeoc_product_to_store();
        $this->json('DELETE', '/api/v1/ocProductToStores/'.$ocProductToStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductToStores/'.$ocProductToStore->id);

        $this->assertResponseStatus(404);
    }
}
