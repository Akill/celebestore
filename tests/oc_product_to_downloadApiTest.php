<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_to_downloadApiTest extends TestCase
{
    use Makeoc_product_to_downloadTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_to_download()
    {
        $ocProductToDownload = $this->fakeoc_product_to_downloadData();
        $this->json('POST', '/api/v1/ocProductToDownloads', $ocProductToDownload);

        $this->assertApiResponse($ocProductToDownload);
    }

    /**
     * @test
     */
    public function testReadoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $this->json('GET', '/api/v1/ocProductToDownloads/'.$ocProductToDownload->id);

        $this->assertApiResponse($ocProductToDownload->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $editedoc_product_to_download = $this->fakeoc_product_to_downloadData();

        $this->json('PUT', '/api/v1/ocProductToDownloads/'.$ocProductToDownload->id, $editedoc_product_to_download);

        $this->assertApiResponse($editedoc_product_to_download);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_to_download()
    {
        $ocProductToDownload = $this->makeoc_product_to_download();
        $this->json('DELETE', '/api/v1/ocProductToDownloads/'.$ocProductToDownload->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductToDownloads/'.$ocProductToDownload->id);

        $this->assertResponseStatus(404);
    }
}
