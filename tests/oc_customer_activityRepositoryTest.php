<?php

use App\Models\oc_customer_activity;
use App\Repositories\oc_customer_activityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_activityRepositoryTest extends TestCase
{
    use Makeoc_customer_activityTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_activityRepository
     */
    protected $ocCustomerActivityRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerActivityRepo = App::make(oc_customer_activityRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_activity()
    {
        $ocCustomerActivity = $this->fakeoc_customer_activityData();
        $createdoc_customer_activity = $this->ocCustomerActivityRepo->create($ocCustomerActivity);
        $createdoc_customer_activity = $createdoc_customer_activity->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_activity);
        $this->assertNotNull($createdoc_customer_activity['id'], 'Created oc_customer_activity must have id specified');
        $this->assertNotNull(oc_customer_activity::find($createdoc_customer_activity['id']), 'oc_customer_activity with given id must be in DB');
        $this->assertModelData($ocCustomerActivity, $createdoc_customer_activity);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $dboc_customer_activity = $this->ocCustomerActivityRepo->find($ocCustomerActivity->id);
        $dboc_customer_activity = $dboc_customer_activity->toArray();
        $this->assertModelData($ocCustomerActivity->toArray(), $dboc_customer_activity);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $fakeoc_customer_activity = $this->fakeoc_customer_activityData();
        $updatedoc_customer_activity = $this->ocCustomerActivityRepo->update($fakeoc_customer_activity, $ocCustomerActivity->id);
        $this->assertModelData($fakeoc_customer_activity, $updatedoc_customer_activity->toArray());
        $dboc_customer_activity = $this->ocCustomerActivityRepo->find($ocCustomerActivity->id);
        $this->assertModelData($fakeoc_customer_activity, $dboc_customer_activity->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_activity()
    {
        $ocCustomerActivity = $this->makeoc_customer_activity();
        $resp = $this->ocCustomerActivityRepo->delete($ocCustomerActivity->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_activity::find($ocCustomerActivity->id), 'oc_customer_activity should not exist in DB');
    }
}
