<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_weight_class_descriptionApiTest extends TestCase
{
    use Makeoc_weight_class_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->fakeoc_weight_class_descriptionData();
        $this->json('POST', '/api/v1/ocWeightClassDescriptions', $ocWeightClassDescription);

        $this->assertApiResponse($ocWeightClassDescription);
    }

    /**
     * @test
     */
    public function testReadoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $this->json('GET', '/api/v1/ocWeightClassDescriptions/'.$ocWeightClassDescription->id);

        $this->assertApiResponse($ocWeightClassDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $editedoc_weight_class_description = $this->fakeoc_weight_class_descriptionData();

        $this->json('PUT', '/api/v1/ocWeightClassDescriptions/'.$ocWeightClassDescription->id, $editedoc_weight_class_description);

        $this->assertApiResponse($editedoc_weight_class_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $this->json('DELETE', '/api/v1/ocWeightClassDescriptions/'.$ocWeightClassDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocWeightClassDescriptions/'.$ocWeightClassDescription->id);

        $this->assertResponseStatus(404);
    }
}
