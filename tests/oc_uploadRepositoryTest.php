<?php

use App\Models\oc_upload;
use App\Repositories\oc_uploadRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_uploadRepositoryTest extends TestCase
{
    use Makeoc_uploadTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_uploadRepository
     */
    protected $ocUploadRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocUploadRepo = App::make(oc_uploadRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_upload()
    {
        $ocUpload = $this->fakeoc_uploadData();
        $createdoc_upload = $this->ocUploadRepo->create($ocUpload);
        $createdoc_upload = $createdoc_upload->toArray();
        $this->assertArrayHasKey('id', $createdoc_upload);
        $this->assertNotNull($createdoc_upload['id'], 'Created oc_upload must have id specified');
        $this->assertNotNull(oc_upload::find($createdoc_upload['id']), 'oc_upload with given id must be in DB');
        $this->assertModelData($ocUpload, $createdoc_upload);
    }

    /**
     * @test read
     */
    public function testReadoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $dboc_upload = $this->ocUploadRepo->find($ocUpload->id);
        $dboc_upload = $dboc_upload->toArray();
        $this->assertModelData($ocUpload->toArray(), $dboc_upload);
    }

    /**
     * @test update
     */
    public function testUpdateoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $fakeoc_upload = $this->fakeoc_uploadData();
        $updatedoc_upload = $this->ocUploadRepo->update($fakeoc_upload, $ocUpload->id);
        $this->assertModelData($fakeoc_upload, $updatedoc_upload->toArray());
        $dboc_upload = $this->ocUploadRepo->find($ocUpload->id);
        $this->assertModelData($fakeoc_upload, $dboc_upload->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_upload()
    {
        $ocUpload = $this->makeoc_upload();
        $resp = $this->ocUploadRepo->delete($ocUpload->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_upload::find($ocUpload->id), 'oc_upload should not exist in DB');
    }
}
