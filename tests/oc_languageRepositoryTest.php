<?php

use App\Models\oc_language;
use App\Repositories\oc_languageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_languageRepositoryTest extends TestCase
{
    use Makeoc_languageTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_languageRepository
     */
    protected $ocLanguageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLanguageRepo = App::make(oc_languageRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_language()
    {
        $ocLanguage = $this->fakeoc_languageData();
        $createdoc_language = $this->ocLanguageRepo->create($ocLanguage);
        $createdoc_language = $createdoc_language->toArray();
        $this->assertArrayHasKey('id', $createdoc_language);
        $this->assertNotNull($createdoc_language['id'], 'Created oc_language must have id specified');
        $this->assertNotNull(oc_language::find($createdoc_language['id']), 'oc_language with given id must be in DB');
        $this->assertModelData($ocLanguage, $createdoc_language);
    }

    /**
     * @test read
     */
    public function testReadoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $dboc_language = $this->ocLanguageRepo->find($ocLanguage->id);
        $dboc_language = $dboc_language->toArray();
        $this->assertModelData($ocLanguage->toArray(), $dboc_language);
    }

    /**
     * @test update
     */
    public function testUpdateoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $fakeoc_language = $this->fakeoc_languageData();
        $updatedoc_language = $this->ocLanguageRepo->update($fakeoc_language, $ocLanguage->id);
        $this->assertModelData($fakeoc_language, $updatedoc_language->toArray());
        $dboc_language = $this->ocLanguageRepo->find($ocLanguage->id);
        $this->assertModelData($fakeoc_language, $dboc_language->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_language()
    {
        $ocLanguage = $this->makeoc_language();
        $resp = $this->ocLanguageRepo->delete($ocLanguage->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_language::find($ocLanguage->id), 'oc_language should not exist in DB');
    }
}
