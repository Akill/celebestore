<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_descriptionApiTest extends TestCase
{
    use Makeoc_category_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category_description()
    {
        $ocCategoryDescription = $this->fakeoc_category_descriptionData();
        $this->json('POST', '/api/v1/ocCategoryDescriptions', $ocCategoryDescription);

        $this->assertApiResponse($ocCategoryDescription);
    }

    /**
     * @test
     */
    public function testReadoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $this->json('GET', '/api/v1/ocCategoryDescriptions/'.$ocCategoryDescription->id);

        $this->assertApiResponse($ocCategoryDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $editedoc_category_description = $this->fakeoc_category_descriptionData();

        $this->json('PUT', '/api/v1/ocCategoryDescriptions/'.$ocCategoryDescription->id, $editedoc_category_description);

        $this->assertApiResponse($editedoc_category_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $this->json('DELETE', '/api/v1/ocCategoryDescriptions/'.$ocCategoryDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategoryDescriptions/'.$ocCategoryDescription->id);

        $this->assertResponseStatus(404);
    }
}
