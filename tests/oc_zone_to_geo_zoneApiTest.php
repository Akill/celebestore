<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_zone_to_geo_zoneApiTest extends TestCase
{
    use Makeoc_zone_to_geo_zoneTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->fakeoc_zone_to_geo_zoneData();
        $this->json('POST', '/api/v1/ocZoneToGeoZones', $ocZoneToGeoZone);

        $this->assertApiResponse($ocZoneToGeoZone);
    }

    /**
     * @test
     */
    public function testReadoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $this->json('GET', '/api/v1/ocZoneToGeoZones/'.$ocZoneToGeoZone->id);

        $this->assertApiResponse($ocZoneToGeoZone->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $editedoc_zone_to_geo_zone = $this->fakeoc_zone_to_geo_zoneData();

        $this->json('PUT', '/api/v1/ocZoneToGeoZones/'.$ocZoneToGeoZone->id, $editedoc_zone_to_geo_zone);

        $this->assertApiResponse($editedoc_zone_to_geo_zone);
    }

    /**
     * @test
     */
    public function testDeleteoc_zone_to_geo_zone()
    {
        $ocZoneToGeoZone = $this->makeoc_zone_to_geo_zone();
        $this->json('DELETE', '/api/v1/ocZoneToGeoZones/'.$ocZoneToGeoZone->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocZoneToGeoZones/'.$ocZoneToGeoZone->id);

        $this->assertResponseStatus(404);
    }
}
