<?php

use App\Models\oc_customer_group_description;
use App\Repositories\oc_customer_group_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_group_descriptionRepositoryTest extends TestCase
{
    use Makeoc_customer_group_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_group_descriptionRepository
     */
    protected $ocCustomerGroupDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerGroupDescriptionRepo = App::make(oc_customer_group_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->fakeoc_customer_group_descriptionData();
        $createdoc_customer_group_description = $this->ocCustomerGroupDescriptionRepo->create($ocCustomerGroupDescription);
        $createdoc_customer_group_description = $createdoc_customer_group_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_group_description);
        $this->assertNotNull($createdoc_customer_group_description['id'], 'Created oc_customer_group_description must have id specified');
        $this->assertNotNull(oc_customer_group_description::find($createdoc_customer_group_description['id']), 'oc_customer_group_description with given id must be in DB');
        $this->assertModelData($ocCustomerGroupDescription, $createdoc_customer_group_description);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $dboc_customer_group_description = $this->ocCustomerGroupDescriptionRepo->find($ocCustomerGroupDescription->id);
        $dboc_customer_group_description = $dboc_customer_group_description->toArray();
        $this->assertModelData($ocCustomerGroupDescription->toArray(), $dboc_customer_group_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $fakeoc_customer_group_description = $this->fakeoc_customer_group_descriptionData();
        $updatedoc_customer_group_description = $this->ocCustomerGroupDescriptionRepo->update($fakeoc_customer_group_description, $ocCustomerGroupDescription->id);
        $this->assertModelData($fakeoc_customer_group_description, $updatedoc_customer_group_description->toArray());
        $dboc_customer_group_description = $this->ocCustomerGroupDescriptionRepo->find($ocCustomerGroupDescription->id);
        $this->assertModelData($fakeoc_customer_group_description, $dboc_customer_group_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_group_description()
    {
        $ocCustomerGroupDescription = $this->makeoc_customer_group_description();
        $resp = $this->ocCustomerGroupDescriptionRepo->delete($ocCustomerGroupDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_group_description::find($ocCustomerGroupDescription->id), 'oc_customer_group_description should not exist in DB');
    }
}
