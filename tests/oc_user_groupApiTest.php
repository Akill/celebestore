<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_user_groupApiTest extends TestCase
{
    use Makeoc_user_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_user_group()
    {
        $ocUserGroup = $this->fakeoc_user_groupData();
        $this->json('POST', '/api/v1/ocUserGroups', $ocUserGroup);

        $this->assertApiResponse($ocUserGroup);
    }

    /**
     * @test
     */
    public function testReadoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $this->json('GET', '/api/v1/ocUserGroups/'.$ocUserGroup->id);

        $this->assertApiResponse($ocUserGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $editedoc_user_group = $this->fakeoc_user_groupData();

        $this->json('PUT', '/api/v1/ocUserGroups/'.$ocUserGroup->id, $editedoc_user_group);

        $this->assertApiResponse($editedoc_user_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_user_group()
    {
        $ocUserGroup = $this->makeoc_user_group();
        $this->json('DELETE', '/api/v1/ocUserGroups/'.$ocUserGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocUserGroups/'.$ocUserGroup->id);

        $this->assertResponseStatus(404);
    }
}
