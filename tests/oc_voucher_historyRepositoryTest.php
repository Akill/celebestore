<?php

use App\Models\oc_voucher_history;
use App\Repositories\oc_voucher_historyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_historyRepositoryTest extends TestCase
{
    use Makeoc_voucher_historyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_voucher_historyRepository
     */
    protected $ocVoucherHistoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocVoucherHistoryRepo = App::make(oc_voucher_historyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_voucher_history()
    {
        $ocVoucherHistory = $this->fakeoc_voucher_historyData();
        $createdoc_voucher_history = $this->ocVoucherHistoryRepo->create($ocVoucherHistory);
        $createdoc_voucher_history = $createdoc_voucher_history->toArray();
        $this->assertArrayHasKey('id', $createdoc_voucher_history);
        $this->assertNotNull($createdoc_voucher_history['id'], 'Created oc_voucher_history must have id specified');
        $this->assertNotNull(oc_voucher_history::find($createdoc_voucher_history['id']), 'oc_voucher_history with given id must be in DB');
        $this->assertModelData($ocVoucherHistory, $createdoc_voucher_history);
    }

    /**
     * @test read
     */
    public function testReadoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $dboc_voucher_history = $this->ocVoucherHistoryRepo->find($ocVoucherHistory->id);
        $dboc_voucher_history = $dboc_voucher_history->toArray();
        $this->assertModelData($ocVoucherHistory->toArray(), $dboc_voucher_history);
    }

    /**
     * @test update
     */
    public function testUpdateoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $fakeoc_voucher_history = $this->fakeoc_voucher_historyData();
        $updatedoc_voucher_history = $this->ocVoucherHistoryRepo->update($fakeoc_voucher_history, $ocVoucherHistory->id);
        $this->assertModelData($fakeoc_voucher_history, $updatedoc_voucher_history->toArray());
        $dboc_voucher_history = $this->ocVoucherHistoryRepo->find($ocVoucherHistory->id);
        $this->assertModelData($fakeoc_voucher_history, $dboc_voucher_history->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_voucher_history()
    {
        $ocVoucherHistory = $this->makeoc_voucher_history();
        $resp = $this->ocVoucherHistoryRepo->delete($ocVoucherHistory->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_voucher_history::find($ocVoucherHistory->id), 'oc_voucher_history should not exist in DB');
    }
}
