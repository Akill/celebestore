<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_to_layoutApiTest extends TestCase
{
    use Makeoc_category_to_layoutTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->fakeoc_category_to_layoutData();
        $this->json('POST', '/api/v1/ocCategoryToLayouts', $ocCategoryToLayout);

        $this->assertApiResponse($ocCategoryToLayout);
    }

    /**
     * @test
     */
    public function testReadoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $this->json('GET', '/api/v1/ocCategoryToLayouts/'.$ocCategoryToLayout->id);

        $this->assertApiResponse($ocCategoryToLayout->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $editedoc_category_to_layout = $this->fakeoc_category_to_layoutData();

        $this->json('PUT', '/api/v1/ocCategoryToLayouts/'.$ocCategoryToLayout->id, $editedoc_category_to_layout);

        $this->assertApiResponse($editedoc_category_to_layout);
    }

    /**
     * @test
     */
    public function testDeleteoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $this->json('DELETE', '/api/v1/ocCategoryToLayouts/'.$ocCategoryToLayout->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategoryToLayouts/'.$ocCategoryToLayout->id);

        $this->assertResponseStatus(404);
    }
}
