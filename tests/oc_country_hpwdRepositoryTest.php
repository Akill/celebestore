<?php

use App\Models\oc_country_hpwd;
use App\Repositories\oc_country_hpwdRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_country_hpwdRepositoryTest extends TestCase
{
    use Makeoc_country_hpwdTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_country_hpwdRepository
     */
    protected $ocCountryHpwdRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCountryHpwdRepo = App::make(oc_country_hpwdRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_country_hpwd()
    {
        $ocCountryHpwd = $this->fakeoc_country_hpwdData();
        $createdoc_country_hpwd = $this->ocCountryHpwdRepo->create($ocCountryHpwd);
        $createdoc_country_hpwd = $createdoc_country_hpwd->toArray();
        $this->assertArrayHasKey('id', $createdoc_country_hpwd);
        $this->assertNotNull($createdoc_country_hpwd['id'], 'Created oc_country_hpwd must have id specified');
        $this->assertNotNull(oc_country_hpwd::find($createdoc_country_hpwd['id']), 'oc_country_hpwd with given id must be in DB');
        $this->assertModelData($ocCountryHpwd, $createdoc_country_hpwd);
    }

    /**
     * @test read
     */
    public function testReadoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $dboc_country_hpwd = $this->ocCountryHpwdRepo->find($ocCountryHpwd->id);
        $dboc_country_hpwd = $dboc_country_hpwd->toArray();
        $this->assertModelData($ocCountryHpwd->toArray(), $dboc_country_hpwd);
    }

    /**
     * @test update
     */
    public function testUpdateoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $fakeoc_country_hpwd = $this->fakeoc_country_hpwdData();
        $updatedoc_country_hpwd = $this->ocCountryHpwdRepo->update($fakeoc_country_hpwd, $ocCountryHpwd->id);
        $this->assertModelData($fakeoc_country_hpwd, $updatedoc_country_hpwd->toArray());
        $dboc_country_hpwd = $this->ocCountryHpwdRepo->find($ocCountryHpwd->id);
        $this->assertModelData($fakeoc_country_hpwd, $dboc_country_hpwd->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_country_hpwd()
    {
        $ocCountryHpwd = $this->makeoc_country_hpwd();
        $resp = $this->ocCountryHpwdRepo->delete($ocCountryHpwd->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_country_hpwd::find($ocCountryHpwd->id), 'oc_country_hpwd should not exist in DB');
    }
}
