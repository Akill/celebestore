<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_actionApiTest extends TestCase
{
    use Makeoc_return_actionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_return_action()
    {
        $ocReturnAction = $this->fakeoc_return_actionData();
        $this->json('POST', '/api/v1/ocReturnActions', $ocReturnAction);

        $this->assertApiResponse($ocReturnAction);
    }

    /**
     * @test
     */
    public function testReadoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $this->json('GET', '/api/v1/ocReturnActions/'.$ocReturnAction->id);

        $this->assertApiResponse($ocReturnAction->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $editedoc_return_action = $this->fakeoc_return_actionData();

        $this->json('PUT', '/api/v1/ocReturnActions/'.$ocReturnAction->id, $editedoc_return_action);

        $this->assertApiResponse($editedoc_return_action);
    }

    /**
     * @test
     */
    public function testDeleteoc_return_action()
    {
        $ocReturnAction = $this->makeoc_return_action();
        $this->json('DELETE', '/api/v1/ocReturnActions/'.$ocReturnAction->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReturnActions/'.$ocReturnAction->id);

        $this->assertResponseStatus(404);
    }
}
