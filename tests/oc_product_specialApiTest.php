<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_specialApiTest extends TestCase
{
    use Makeoc_product_specialTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_special()
    {
        $ocProductSpecial = $this->fakeoc_product_specialData();
        $this->json('POST', '/api/v1/ocProductSpecials', $ocProductSpecial);

        $this->assertApiResponse($ocProductSpecial);
    }

    /**
     * @test
     */
    public function testReadoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $this->json('GET', '/api/v1/ocProductSpecials/'.$ocProductSpecial->id);

        $this->assertApiResponse($ocProductSpecial->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $editedoc_product_special = $this->fakeoc_product_specialData();

        $this->json('PUT', '/api/v1/ocProductSpecials/'.$ocProductSpecial->id, $editedoc_product_special);

        $this->assertApiResponse($editedoc_product_special);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_special()
    {
        $ocProductSpecial = $this->makeoc_product_special();
        $this->json('DELETE', '/api/v1/ocProductSpecials/'.$ocProductSpecial->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductSpecials/'.$ocProductSpecial->id);

        $this->assertResponseStatus(404);
    }
}
