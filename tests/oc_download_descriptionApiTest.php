<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_download_descriptionApiTest extends TestCase
{
    use Makeoc_download_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_download_description()
    {
        $ocDownloadDescription = $this->fakeoc_download_descriptionData();
        $this->json('POST', '/api/v1/ocDownloadDescriptions', $ocDownloadDescription);

        $this->assertApiResponse($ocDownloadDescription);
    }

    /**
     * @test
     */
    public function testReadoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $this->json('GET', '/api/v1/ocDownloadDescriptions/'.$ocDownloadDescription->id);

        $this->assertApiResponse($ocDownloadDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $editedoc_download_description = $this->fakeoc_download_descriptionData();

        $this->json('PUT', '/api/v1/ocDownloadDescriptions/'.$ocDownloadDescription->id, $editedoc_download_description);

        $this->assertApiResponse($editedoc_download_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_download_description()
    {
        $ocDownloadDescription = $this->makeoc_download_description();
        $this->json('DELETE', '/api/v1/ocDownloadDescriptions/'.$ocDownloadDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocDownloadDescriptions/'.$ocDownloadDescription->id);

        $this->assertResponseStatus(404);
    }
}
