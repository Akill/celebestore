<?php

use App\Models\oc_manufacturer;
use App\Repositories\oc_manufacturerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_manufacturerRepositoryTest extends TestCase
{
    use Makeoc_manufacturerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_manufacturerRepository
     */
    protected $ocManufacturerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocManufacturerRepo = App::make(oc_manufacturerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_manufacturer()
    {
        $ocManufacturer = $this->fakeoc_manufacturerData();
        $createdoc_manufacturer = $this->ocManufacturerRepo->create($ocManufacturer);
        $createdoc_manufacturer = $createdoc_manufacturer->toArray();
        $this->assertArrayHasKey('id', $createdoc_manufacturer);
        $this->assertNotNull($createdoc_manufacturer['id'], 'Created oc_manufacturer must have id specified');
        $this->assertNotNull(oc_manufacturer::find($createdoc_manufacturer['id']), 'oc_manufacturer with given id must be in DB');
        $this->assertModelData($ocManufacturer, $createdoc_manufacturer);
    }

    /**
     * @test read
     */
    public function testReadoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $dboc_manufacturer = $this->ocManufacturerRepo->find($ocManufacturer->id);
        $dboc_manufacturer = $dboc_manufacturer->toArray();
        $this->assertModelData($ocManufacturer->toArray(), $dboc_manufacturer);
    }

    /**
     * @test update
     */
    public function testUpdateoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $fakeoc_manufacturer = $this->fakeoc_manufacturerData();
        $updatedoc_manufacturer = $this->ocManufacturerRepo->update($fakeoc_manufacturer, $ocManufacturer->id);
        $this->assertModelData($fakeoc_manufacturer, $updatedoc_manufacturer->toArray());
        $dboc_manufacturer = $this->ocManufacturerRepo->find($ocManufacturer->id);
        $this->assertModelData($fakeoc_manufacturer, $dboc_manufacturer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_manufacturer()
    {
        $ocManufacturer = $this->makeoc_manufacturer();
        $resp = $this->ocManufacturerRepo->delete($ocManufacturer->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_manufacturer::find($ocManufacturer->id), 'oc_manufacturer should not exist in DB');
    }
}
