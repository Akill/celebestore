<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_rewardApiTest extends TestCase
{
    use Makeoc_customer_rewardTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_reward()
    {
        $ocCustomerReward = $this->fakeoc_customer_rewardData();
        $this->json('POST', '/api/v1/ocCustomerRewards', $ocCustomerReward);

        $this->assertApiResponse($ocCustomerReward);
    }

    /**
     * @test
     */
    public function testReadoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $this->json('GET', '/api/v1/ocCustomerRewards/'.$ocCustomerReward->id);

        $this->assertApiResponse($ocCustomerReward->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $editedoc_customer_reward = $this->fakeoc_customer_rewardData();

        $this->json('PUT', '/api/v1/ocCustomerRewards/'.$ocCustomerReward->id, $editedoc_customer_reward);

        $this->assertApiResponse($editedoc_customer_reward);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_reward()
    {
        $ocCustomerReward = $this->makeoc_customer_reward();
        $this->json('DELETE', '/api/v1/ocCustomerRewards/'.$ocCustomerReward->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerRewards/'.$ocCustomerReward->id);

        $this->assertResponseStatus(404);
    }
}
