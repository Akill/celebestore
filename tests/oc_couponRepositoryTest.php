<?php

use App\Models\oc_coupon;
use App\Repositories\oc_couponRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_couponRepositoryTest extends TestCase
{
    use Makeoc_couponTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_couponRepository
     */
    protected $ocCouponRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCouponRepo = App::make(oc_couponRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_coupon()
    {
        $ocCoupon = $this->fakeoc_couponData();
        $createdoc_coupon = $this->ocCouponRepo->create($ocCoupon);
        $createdoc_coupon = $createdoc_coupon->toArray();
        $this->assertArrayHasKey('id', $createdoc_coupon);
        $this->assertNotNull($createdoc_coupon['id'], 'Created oc_coupon must have id specified');
        $this->assertNotNull(oc_coupon::find($createdoc_coupon['id']), 'oc_coupon with given id must be in DB');
        $this->assertModelData($ocCoupon, $createdoc_coupon);
    }

    /**
     * @test read
     */
    public function testReadoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $dboc_coupon = $this->ocCouponRepo->find($ocCoupon->id);
        $dboc_coupon = $dboc_coupon->toArray();
        $this->assertModelData($ocCoupon->toArray(), $dboc_coupon);
    }

    /**
     * @test update
     */
    public function testUpdateoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $fakeoc_coupon = $this->fakeoc_couponData();
        $updatedoc_coupon = $this->ocCouponRepo->update($fakeoc_coupon, $ocCoupon->id);
        $this->assertModelData($fakeoc_coupon, $updatedoc_coupon->toArray());
        $dboc_coupon = $this->ocCouponRepo->find($ocCoupon->id);
        $this->assertModelData($fakeoc_coupon, $dboc_coupon->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_coupon()
    {
        $ocCoupon = $this->makeoc_coupon();
        $resp = $this->ocCouponRepo->delete($ocCoupon->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_coupon::find($ocCoupon->id), 'oc_coupon should not exist in DB');
    }
}
