<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_descriptionApiTest extends TestCase
{
    use Makeoc_product_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_description()
    {
        $ocProductDescription = $this->fakeoc_product_descriptionData();
        $this->json('POST', '/api/v1/ocProductDescriptions', $ocProductDescription);

        $this->assertApiResponse($ocProductDescription);
    }

    /**
     * @test
     */
    public function testReadoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $this->json('GET', '/api/v1/ocProductDescriptions/'.$ocProductDescription->id);

        $this->assertApiResponse($ocProductDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $editedoc_product_description = $this->fakeoc_product_descriptionData();

        $this->json('PUT', '/api/v1/ocProductDescriptions/'.$ocProductDescription->id, $editedoc_product_description);

        $this->assertApiResponse($editedoc_product_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_description()
    {
        $ocProductDescription = $this->makeoc_product_description();
        $this->json('DELETE', '/api/v1/ocProductDescriptions/'.$ocProductDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductDescriptions/'.$ocProductDescription->id);

        $this->assertResponseStatus(404);
    }
}
