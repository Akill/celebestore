<?php

use App\Models\oc_information_to_store;
use App\Repositories\oc_information_to_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_to_storeRepositoryTest extends TestCase
{
    use Makeoc_information_to_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_information_to_storeRepository
     */
    protected $ocInformationToStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocInformationToStoreRepo = App::make(oc_information_to_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_information_to_store()
    {
        $ocInformationToStore = $this->fakeoc_information_to_storeData();
        $createdoc_information_to_store = $this->ocInformationToStoreRepo->create($ocInformationToStore);
        $createdoc_information_to_store = $createdoc_information_to_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_information_to_store);
        $this->assertNotNull($createdoc_information_to_store['id'], 'Created oc_information_to_store must have id specified');
        $this->assertNotNull(oc_information_to_store::find($createdoc_information_to_store['id']), 'oc_information_to_store with given id must be in DB');
        $this->assertModelData($ocInformationToStore, $createdoc_information_to_store);
    }

    /**
     * @test read
     */
    public function testReadoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $dboc_information_to_store = $this->ocInformationToStoreRepo->find($ocInformationToStore->id);
        $dboc_information_to_store = $dboc_information_to_store->toArray();
        $this->assertModelData($ocInformationToStore->toArray(), $dboc_information_to_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $fakeoc_information_to_store = $this->fakeoc_information_to_storeData();
        $updatedoc_information_to_store = $this->ocInformationToStoreRepo->update($fakeoc_information_to_store, $ocInformationToStore->id);
        $this->assertModelData($fakeoc_information_to_store, $updatedoc_information_to_store->toArray());
        $dboc_information_to_store = $this->ocInformationToStoreRepo->find($ocInformationToStore->id);
        $this->assertModelData($fakeoc_information_to_store, $dboc_information_to_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_information_to_store()
    {
        $ocInformationToStore = $this->makeoc_information_to_store();
        $resp = $this->ocInformationToStoreRepo->delete($ocInformationToStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_information_to_store::find($ocInformationToStore->id), 'oc_information_to_store should not exist in DB');
    }
}
