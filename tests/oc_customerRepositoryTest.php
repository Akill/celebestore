<?php

use App\Models\oc_customer;
use App\Repositories\oc_customerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customerRepositoryTest extends TestCase
{
    use Makeoc_customerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customerRepository
     */
    protected $ocCustomerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerRepo = App::make(oc_customerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer()
    {
        $ocCustomer = $this->fakeoc_customerData();
        $createdoc_customer = $this->ocCustomerRepo->create($ocCustomer);
        $createdoc_customer = $createdoc_customer->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer);
        $this->assertNotNull($createdoc_customer['id'], 'Created oc_customer must have id specified');
        $this->assertNotNull(oc_customer::find($createdoc_customer['id']), 'oc_customer with given id must be in DB');
        $this->assertModelData($ocCustomer, $createdoc_customer);
    }

    /**
     * @test read
     */
    public function testReadoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $dboc_customer = $this->ocCustomerRepo->find($ocCustomer->id);
        $dboc_customer = $dboc_customer->toArray();
        $this->assertModelData($ocCustomer->toArray(), $dboc_customer);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $fakeoc_customer = $this->fakeoc_customerData();
        $updatedoc_customer = $this->ocCustomerRepo->update($fakeoc_customer, $ocCustomer->id);
        $this->assertModelData($fakeoc_customer, $updatedoc_customer->toArray());
        $dboc_customer = $this->ocCustomerRepo->find($ocCustomer->id);
        $this->assertModelData($fakeoc_customer, $dboc_customer->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer()
    {
        $ocCustomer = $this->makeoc_customer();
        $resp = $this->ocCustomerRepo->delete($ocCustomer->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer::find($ocCustomer->id), 'oc_customer should not exist in DB');
    }
}
