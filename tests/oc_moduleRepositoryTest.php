<?php

use App\Models\oc_module;
use App\Repositories\oc_moduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_moduleRepositoryTest extends TestCase
{
    use Makeoc_moduleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_moduleRepository
     */
    protected $ocModuleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocModuleRepo = App::make(oc_moduleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_module()
    {
        $ocModule = $this->fakeoc_moduleData();
        $createdoc_module = $this->ocModuleRepo->create($ocModule);
        $createdoc_module = $createdoc_module->toArray();
        $this->assertArrayHasKey('id', $createdoc_module);
        $this->assertNotNull($createdoc_module['id'], 'Created oc_module must have id specified');
        $this->assertNotNull(oc_module::find($createdoc_module['id']), 'oc_module with given id must be in DB');
        $this->assertModelData($ocModule, $createdoc_module);
    }

    /**
     * @test read
     */
    public function testReadoc_module()
    {
        $ocModule = $this->makeoc_module();
        $dboc_module = $this->ocModuleRepo->find($ocModule->id);
        $dboc_module = $dboc_module->toArray();
        $this->assertModelData($ocModule->toArray(), $dboc_module);
    }

    /**
     * @test update
     */
    public function testUpdateoc_module()
    {
        $ocModule = $this->makeoc_module();
        $fakeoc_module = $this->fakeoc_moduleData();
        $updatedoc_module = $this->ocModuleRepo->update($fakeoc_module, $ocModule->id);
        $this->assertModelData($fakeoc_module, $updatedoc_module->toArray());
        $dboc_module = $this->ocModuleRepo->find($ocModule->id);
        $this->assertModelData($fakeoc_module, $dboc_module->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_module()
    {
        $ocModule = $this->makeoc_module();
        $resp = $this->ocModuleRepo->delete($ocModule->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_module::find($ocModule->id), 'oc_module should not exist in DB');
    }
}
