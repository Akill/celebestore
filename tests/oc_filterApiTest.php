<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filterApiTest extends TestCase
{
    use Makeoc_filterTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_filter()
    {
        $ocFilter = $this->fakeoc_filterData();
        $this->json('POST', '/api/v1/ocFilters', $ocFilter);

        $this->assertApiResponse($ocFilter);
    }

    /**
     * @test
     */
    public function testReadoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $this->json('GET', '/api/v1/ocFilters/'.$ocFilter->id);

        $this->assertApiResponse($ocFilter->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $editedoc_filter = $this->fakeoc_filterData();

        $this->json('PUT', '/api/v1/ocFilters/'.$ocFilter->id, $editedoc_filter);

        $this->assertApiResponse($editedoc_filter);
    }

    /**
     * @test
     */
    public function testDeleteoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $this->json('DELETE', '/api/v1/ocFilters/'.$ocFilter->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocFilters/'.$ocFilter->id);

        $this->assertResponseStatus(404);
    }
}
