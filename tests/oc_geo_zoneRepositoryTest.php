<?php

use App\Models\oc_geo_zone;
use App\Repositories\oc_geo_zoneRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_geo_zoneRepositoryTest extends TestCase
{
    use Makeoc_geo_zoneTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_geo_zoneRepository
     */
    protected $ocGeoZoneRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocGeoZoneRepo = App::make(oc_geo_zoneRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_geo_zone()
    {
        $ocGeoZone = $this->fakeoc_geo_zoneData();
        $createdoc_geo_zone = $this->ocGeoZoneRepo->create($ocGeoZone);
        $createdoc_geo_zone = $createdoc_geo_zone->toArray();
        $this->assertArrayHasKey('id', $createdoc_geo_zone);
        $this->assertNotNull($createdoc_geo_zone['id'], 'Created oc_geo_zone must have id specified');
        $this->assertNotNull(oc_geo_zone::find($createdoc_geo_zone['id']), 'oc_geo_zone with given id must be in DB');
        $this->assertModelData($ocGeoZone, $createdoc_geo_zone);
    }

    /**
     * @test read
     */
    public function testReadoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $dboc_geo_zone = $this->ocGeoZoneRepo->find($ocGeoZone->id);
        $dboc_geo_zone = $dboc_geo_zone->toArray();
        $this->assertModelData($ocGeoZone->toArray(), $dboc_geo_zone);
    }

    /**
     * @test update
     */
    public function testUpdateoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $fakeoc_geo_zone = $this->fakeoc_geo_zoneData();
        $updatedoc_geo_zone = $this->ocGeoZoneRepo->update($fakeoc_geo_zone, $ocGeoZone->id);
        $this->assertModelData($fakeoc_geo_zone, $updatedoc_geo_zone->toArray());
        $dboc_geo_zone = $this->ocGeoZoneRepo->find($ocGeoZone->id);
        $this->assertModelData($fakeoc_geo_zone, $dboc_geo_zone->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_geo_zone()
    {
        $ocGeoZone = $this->makeoc_geo_zone();
        $resp = $this->ocGeoZoneRepo->delete($ocGeoZone->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_geo_zone::find($ocGeoZone->id), 'oc_geo_zone should not exist in DB');
    }
}
