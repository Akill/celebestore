<?php

use App\Models\oc_customer_ban_ip;
use App\Repositories\oc_customer_ban_ipRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_ban_ipRepositoryTest extends TestCase
{
    use Makeoc_customer_ban_ipTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_customer_ban_ipRepository
     */
    protected $ocCustomerBanIpRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomerBanIpRepo = App::make(oc_customer_ban_ipRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->fakeoc_customer_ban_ipData();
        $createdoc_customer_ban_ip = $this->ocCustomerBanIpRepo->create($ocCustomerBanIp);
        $createdoc_customer_ban_ip = $createdoc_customer_ban_ip->toArray();
        $this->assertArrayHasKey('id', $createdoc_customer_ban_ip);
        $this->assertNotNull($createdoc_customer_ban_ip['id'], 'Created oc_customer_ban_ip must have id specified');
        $this->assertNotNull(oc_customer_ban_ip::find($createdoc_customer_ban_ip['id']), 'oc_customer_ban_ip with given id must be in DB');
        $this->assertModelData($ocCustomerBanIp, $createdoc_customer_ban_ip);
    }

    /**
     * @test read
     */
    public function testReadoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $dboc_customer_ban_ip = $this->ocCustomerBanIpRepo->find($ocCustomerBanIp->id);
        $dboc_customer_ban_ip = $dboc_customer_ban_ip->toArray();
        $this->assertModelData($ocCustomerBanIp->toArray(), $dboc_customer_ban_ip);
    }

    /**
     * @test update
     */
    public function testUpdateoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $fakeoc_customer_ban_ip = $this->fakeoc_customer_ban_ipData();
        $updatedoc_customer_ban_ip = $this->ocCustomerBanIpRepo->update($fakeoc_customer_ban_ip, $ocCustomerBanIp->id);
        $this->assertModelData($fakeoc_customer_ban_ip, $updatedoc_customer_ban_ip->toArray());
        $dboc_customer_ban_ip = $this->ocCustomerBanIpRepo->find($ocCustomerBanIp->id);
        $this->assertModelData($fakeoc_customer_ban_ip, $dboc_customer_ban_ip->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_customer_ban_ip()
    {
        $ocCustomerBanIp = $this->makeoc_customer_ban_ip();
        $resp = $this->ocCustomerBanIpRepo->delete($ocCustomerBanIp->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_customer_ban_ip::find($ocCustomerBanIp->id), 'oc_customer_ban_ip should not exist in DB');
    }
}
