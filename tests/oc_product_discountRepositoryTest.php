<?php

use App\Models\oc_product_discount;
use App\Repositories\oc_product_discountRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_discountRepositoryTest extends TestCase
{
    use Makeoc_product_discountTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_discountRepository
     */
    protected $ocProductDiscountRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductDiscountRepo = App::make(oc_product_discountRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_discount()
    {
        $ocProductDiscount = $this->fakeoc_product_discountData();
        $createdoc_product_discount = $this->ocProductDiscountRepo->create($ocProductDiscount);
        $createdoc_product_discount = $createdoc_product_discount->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_discount);
        $this->assertNotNull($createdoc_product_discount['id'], 'Created oc_product_discount must have id specified');
        $this->assertNotNull(oc_product_discount::find($createdoc_product_discount['id']), 'oc_product_discount with given id must be in DB');
        $this->assertModelData($ocProductDiscount, $createdoc_product_discount);
    }

    /**
     * @test read
     */
    public function testReadoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $dboc_product_discount = $this->ocProductDiscountRepo->find($ocProductDiscount->id);
        $dboc_product_discount = $dboc_product_discount->toArray();
        $this->assertModelData($ocProductDiscount->toArray(), $dboc_product_discount);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $fakeoc_product_discount = $this->fakeoc_product_discountData();
        $updatedoc_product_discount = $this->ocProductDiscountRepo->update($fakeoc_product_discount, $ocProductDiscount->id);
        $this->assertModelData($fakeoc_product_discount, $updatedoc_product_discount->toArray());
        $dboc_product_discount = $this->ocProductDiscountRepo->find($ocProductDiscount->id);
        $this->assertModelData($fakeoc_product_discount, $dboc_product_discount->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_discount()
    {
        $ocProductDiscount = $this->makeoc_product_discount();
        $resp = $this->ocProductDiscountRepo->delete($ocProductDiscount->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_discount::find($ocProductDiscount->id), 'oc_product_discount should not exist in DB');
    }
}
