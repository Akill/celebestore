<?php

use App\Models\oc_category_description;
use App\Repositories\oc_category_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_descriptionRepositoryTest extends TestCase
{
    use Makeoc_category_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_category_descriptionRepository
     */
    protected $ocCategoryDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryDescriptionRepo = App::make(oc_category_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category_description()
    {
        $ocCategoryDescription = $this->fakeoc_category_descriptionData();
        $createdoc_category_description = $this->ocCategoryDescriptionRepo->create($ocCategoryDescription);
        $createdoc_category_description = $createdoc_category_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_category_description);
        $this->assertNotNull($createdoc_category_description['id'], 'Created oc_category_description must have id specified');
        $this->assertNotNull(oc_category_description::find($createdoc_category_description['id']), 'oc_category_description with given id must be in DB');
        $this->assertModelData($ocCategoryDescription, $createdoc_category_description);
    }

    /**
     * @test read
     */
    public function testReadoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $dboc_category_description = $this->ocCategoryDescriptionRepo->find($ocCategoryDescription->id);
        $dboc_category_description = $dboc_category_description->toArray();
        $this->assertModelData($ocCategoryDescription->toArray(), $dboc_category_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $fakeoc_category_description = $this->fakeoc_category_descriptionData();
        $updatedoc_category_description = $this->ocCategoryDescriptionRepo->update($fakeoc_category_description, $ocCategoryDescription->id);
        $this->assertModelData($fakeoc_category_description, $updatedoc_category_description->toArray());
        $dboc_category_description = $this->ocCategoryDescriptionRepo->find($ocCategoryDescription->id);
        $this->assertModelData($fakeoc_category_description, $dboc_category_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category_description()
    {
        $ocCategoryDescription = $this->makeoc_category_description();
        $resp = $this->ocCategoryDescriptionRepo->delete($ocCategoryDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category_description::find($ocCategoryDescription->id), 'oc_category_description should not exist in DB');
    }
}
