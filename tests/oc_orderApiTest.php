<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_orderApiTest extends TestCase
{
    use Makeoc_orderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_order()
    {
        $ocOrder = $this->fakeoc_orderData();
        $this->json('POST', '/api/v1/ocOrders', $ocOrder);

        $this->assertApiResponse($ocOrder);
    }

    /**
     * @test
     */
    public function testReadoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $this->json('GET', '/api/v1/ocOrders/'.$ocOrder->id);

        $this->assertApiResponse($ocOrder->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $editedoc_order = $this->fakeoc_orderData();

        $this->json('PUT', '/api/v1/ocOrders/'.$ocOrder->id, $editedoc_order);

        $this->assertApiResponse($editedoc_order);
    }

    /**
     * @test
     */
    public function testDeleteoc_order()
    {
        $ocOrder = $this->makeoc_order();
        $this->json('DELETE', '/api/v1/ocOrders/'.$ocOrder->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOrders/'.$ocOrder->id);

        $this->assertResponseStatus(404);
    }
}
