<?php

use App\Models\oc_category_to_layout;
use App\Repositories\oc_category_to_layoutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_to_layoutRepositoryTest extends TestCase
{
    use Makeoc_category_to_layoutTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_category_to_layoutRepository
     */
    protected $ocCategoryToLayoutRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryToLayoutRepo = App::make(oc_category_to_layoutRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->fakeoc_category_to_layoutData();
        $createdoc_category_to_layout = $this->ocCategoryToLayoutRepo->create($ocCategoryToLayout);
        $createdoc_category_to_layout = $createdoc_category_to_layout->toArray();
        $this->assertArrayHasKey('id', $createdoc_category_to_layout);
        $this->assertNotNull($createdoc_category_to_layout['id'], 'Created oc_category_to_layout must have id specified');
        $this->assertNotNull(oc_category_to_layout::find($createdoc_category_to_layout['id']), 'oc_category_to_layout with given id must be in DB');
        $this->assertModelData($ocCategoryToLayout, $createdoc_category_to_layout);
    }

    /**
     * @test read
     */
    public function testReadoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $dboc_category_to_layout = $this->ocCategoryToLayoutRepo->find($ocCategoryToLayout->id);
        $dboc_category_to_layout = $dboc_category_to_layout->toArray();
        $this->assertModelData($ocCategoryToLayout->toArray(), $dboc_category_to_layout);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $fakeoc_category_to_layout = $this->fakeoc_category_to_layoutData();
        $updatedoc_category_to_layout = $this->ocCategoryToLayoutRepo->update($fakeoc_category_to_layout, $ocCategoryToLayout->id);
        $this->assertModelData($fakeoc_category_to_layout, $updatedoc_category_to_layout->toArray());
        $dboc_category_to_layout = $this->ocCategoryToLayoutRepo->find($ocCategoryToLayout->id);
        $this->assertModelData($fakeoc_category_to_layout, $dboc_category_to_layout->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category_to_layout()
    {
        $ocCategoryToLayout = $this->makeoc_category_to_layout();
        $resp = $this->ocCategoryToLayoutRepo->delete($ocCategoryToLayout->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category_to_layout::find($ocCategoryToLayout->id), 'oc_category_to_layout should not exist in DB');
    }
}
