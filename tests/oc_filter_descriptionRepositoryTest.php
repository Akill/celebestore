<?php

use App\Models\oc_filter_description;
use App\Repositories\oc_filter_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_descriptionRepositoryTest extends TestCase
{
    use Makeoc_filter_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_filter_descriptionRepository
     */
    protected $ocFilterDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocFilterDescriptionRepo = App::make(oc_filter_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_filter_description()
    {
        $ocFilterDescription = $this->fakeoc_filter_descriptionData();
        $createdoc_filter_description = $this->ocFilterDescriptionRepo->create($ocFilterDescription);
        $createdoc_filter_description = $createdoc_filter_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_filter_description);
        $this->assertNotNull($createdoc_filter_description['id'], 'Created oc_filter_description must have id specified');
        $this->assertNotNull(oc_filter_description::find($createdoc_filter_description['id']), 'oc_filter_description with given id must be in DB');
        $this->assertModelData($ocFilterDescription, $createdoc_filter_description);
    }

    /**
     * @test read
     */
    public function testReadoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $dboc_filter_description = $this->ocFilterDescriptionRepo->find($ocFilterDescription->id);
        $dboc_filter_description = $dboc_filter_description->toArray();
        $this->assertModelData($ocFilterDescription->toArray(), $dboc_filter_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $fakeoc_filter_description = $this->fakeoc_filter_descriptionData();
        $updatedoc_filter_description = $this->ocFilterDescriptionRepo->update($fakeoc_filter_description, $ocFilterDescription->id);
        $this->assertModelData($fakeoc_filter_description, $updatedoc_filter_description->toArray());
        $dboc_filter_description = $this->ocFilterDescriptionRepo->find($ocFilterDescription->id);
        $this->assertModelData($fakeoc_filter_description, $dboc_filter_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_filter_description()
    {
        $ocFilterDescription = $this->makeoc_filter_description();
        $resp = $this->ocFilterDescriptionRepo->delete($ocFilterDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_filter_description::find($ocFilterDescription->id), 'oc_filter_description should not exist in DB');
    }
}
