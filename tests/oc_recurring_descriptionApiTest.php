<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_recurring_descriptionApiTest extends TestCase
{
    use Makeoc_recurring_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_recurring_description()
    {
        $ocRecurringDescription = $this->fakeoc_recurring_descriptionData();
        $this->json('POST', '/api/v1/ocRecurringDescriptions', $ocRecurringDescription);

        $this->assertApiResponse($ocRecurringDescription);
    }

    /**
     * @test
     */
    public function testReadoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $this->json('GET', '/api/v1/ocRecurringDescriptions/'.$ocRecurringDescription->id);

        $this->assertApiResponse($ocRecurringDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $editedoc_recurring_description = $this->fakeoc_recurring_descriptionData();

        $this->json('PUT', '/api/v1/ocRecurringDescriptions/'.$ocRecurringDescription->id, $editedoc_recurring_description);

        $this->assertApiResponse($editedoc_recurring_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_recurring_description()
    {
        $ocRecurringDescription = $this->makeoc_recurring_description();
        $this->json('DELETE', '/api/v1/ocRecurringDescriptions/'.$ocRecurringDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocRecurringDescriptions/'.$ocRecurringDescription->id);

        $this->assertResponseStatus(404);
    }
}
