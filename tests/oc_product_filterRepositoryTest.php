<?php

use App\Models\oc_product_filter;
use App\Repositories\oc_product_filterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_filterRepositoryTest extends TestCase
{
    use Makeoc_product_filterTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_product_filterRepository
     */
    protected $ocProductFilterRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocProductFilterRepo = App::make(oc_product_filterRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_product_filter()
    {
        $ocProductFilter = $this->fakeoc_product_filterData();
        $createdoc_product_filter = $this->ocProductFilterRepo->create($ocProductFilter);
        $createdoc_product_filter = $createdoc_product_filter->toArray();
        $this->assertArrayHasKey('id', $createdoc_product_filter);
        $this->assertNotNull($createdoc_product_filter['id'], 'Created oc_product_filter must have id specified');
        $this->assertNotNull(oc_product_filter::find($createdoc_product_filter['id']), 'oc_product_filter with given id must be in DB');
        $this->assertModelData($ocProductFilter, $createdoc_product_filter);
    }

    /**
     * @test read
     */
    public function testReadoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $dboc_product_filter = $this->ocProductFilterRepo->find($ocProductFilter->id);
        $dboc_product_filter = $dboc_product_filter->toArray();
        $this->assertModelData($ocProductFilter->toArray(), $dboc_product_filter);
    }

    /**
     * @test update
     */
    public function testUpdateoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $fakeoc_product_filter = $this->fakeoc_product_filterData();
        $updatedoc_product_filter = $this->ocProductFilterRepo->update($fakeoc_product_filter, $ocProductFilter->id);
        $this->assertModelData($fakeoc_product_filter, $updatedoc_product_filter->toArray());
        $dboc_product_filter = $this->ocProductFilterRepo->find($ocProductFilter->id);
        $this->assertModelData($fakeoc_product_filter, $dboc_product_filter->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_product_filter()
    {
        $ocProductFilter = $this->makeoc_product_filter();
        $resp = $this->ocProductFilterRepo->delete($ocProductFilter->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_product_filter::find($ocProductFilter->id), 'oc_product_filter should not exist in DB');
    }
}
