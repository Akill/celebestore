<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_countryApiTest extends TestCase
{
    use Makeoc_countryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_country()
    {
        $ocCountry = $this->fakeoc_countryData();
        $this->json('POST', '/api/v1/ocCountries', $ocCountry);

        $this->assertApiResponse($ocCountry);
    }

    /**
     * @test
     */
    public function testReadoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $this->json('GET', '/api/v1/ocCountries/'.$ocCountry->id);

        $this->assertApiResponse($ocCountry->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $editedoc_country = $this->fakeoc_countryData();

        $this->json('PUT', '/api/v1/ocCountries/'.$ocCountry->id, $editedoc_country);

        $this->assertApiResponse($editedoc_country);
    }

    /**
     * @test
     */
    public function testDeleteoc_country()
    {
        $ocCountry = $this->makeoc_country();
        $this->json('DELETE', '/api/v1/ocCountries/'.$ocCountry->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCountries/'.$ocCountry->id);

        $this->assertResponseStatus(404);
    }
}
