<?php

use App\Models\oc_filter;
use App\Repositories\oc_filterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filterRepositoryTest extends TestCase
{
    use Makeoc_filterTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_filterRepository
     */
    protected $ocFilterRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocFilterRepo = App::make(oc_filterRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_filter()
    {
        $ocFilter = $this->fakeoc_filterData();
        $createdoc_filter = $this->ocFilterRepo->create($ocFilter);
        $createdoc_filter = $createdoc_filter->toArray();
        $this->assertArrayHasKey('id', $createdoc_filter);
        $this->assertNotNull($createdoc_filter['id'], 'Created oc_filter must have id specified');
        $this->assertNotNull(oc_filter::find($createdoc_filter['id']), 'oc_filter with given id must be in DB');
        $this->assertModelData($ocFilter, $createdoc_filter);
    }

    /**
     * @test read
     */
    public function testReadoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $dboc_filter = $this->ocFilterRepo->find($ocFilter->id);
        $dboc_filter = $dboc_filter->toArray();
        $this->assertModelData($ocFilter->toArray(), $dboc_filter);
    }

    /**
     * @test update
     */
    public function testUpdateoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $fakeoc_filter = $this->fakeoc_filterData();
        $updatedoc_filter = $this->ocFilterRepo->update($fakeoc_filter, $ocFilter->id);
        $this->assertModelData($fakeoc_filter, $updatedoc_filter->toArray());
        $dboc_filter = $this->ocFilterRepo->find($ocFilter->id);
        $this->assertModelData($fakeoc_filter, $dboc_filter->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_filter()
    {
        $ocFilter = $this->makeoc_filter();
        $resp = $this->ocFilterRepo->delete($ocFilter->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_filter::find($ocFilter->id), 'oc_filter should not exist in DB');
    }
}
