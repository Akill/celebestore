<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_confirm_to_storeApiTest extends TestCase
{
    use Makeoc_confirm_to_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->fakeoc_confirm_to_storeData();
        $this->json('POST', '/api/v1/ocConfirmToStores', $ocConfirmToStore);

        $this->assertApiResponse($ocConfirmToStore);
    }

    /**
     * @test
     */
    public function testReadoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $this->json('GET', '/api/v1/ocConfirmToStores/'.$ocConfirmToStore->id);

        $this->assertApiResponse($ocConfirmToStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $editedoc_confirm_to_store = $this->fakeoc_confirm_to_storeData();

        $this->json('PUT', '/api/v1/ocConfirmToStores/'.$ocConfirmToStore->id, $editedoc_confirm_to_store);

        $this->assertApiResponse($editedoc_confirm_to_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_confirm_to_store()
    {
        $ocConfirmToStore = $this->makeoc_confirm_to_store();
        $this->json('DELETE', '/api/v1/ocConfirmToStores/'.$ocConfirmToStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocConfirmToStores/'.$ocConfirmToStore->id);

        $this->assertResponseStatus(404);
    }
}
