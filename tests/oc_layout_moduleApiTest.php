<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layout_moduleApiTest extends TestCase
{
    use Makeoc_layout_moduleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_layout_module()
    {
        $ocLayoutModule = $this->fakeoc_layout_moduleData();
        $this->json('POST', '/api/v1/ocLayoutModules', $ocLayoutModule);

        $this->assertApiResponse($ocLayoutModule);
    }

    /**
     * @test
     */
    public function testReadoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $this->json('GET', '/api/v1/ocLayoutModules/'.$ocLayoutModule->id);

        $this->assertApiResponse($ocLayoutModule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $editedoc_layout_module = $this->fakeoc_layout_moduleData();

        $this->json('PUT', '/api/v1/ocLayoutModules/'.$ocLayoutModule->id, $editedoc_layout_module);

        $this->assertApiResponse($editedoc_layout_module);
    }

    /**
     * @test
     */
    public function testDeleteoc_layout_module()
    {
        $ocLayoutModule = $this->makeoc_layout_module();
        $this->json('DELETE', '/api/v1/ocLayoutModules/'.$ocLayoutModule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocLayoutModules/'.$ocLayoutModule->id);

        $this->assertResponseStatus(404);
    }
}
