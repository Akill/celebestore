<?php

use App\Models\oc_attribute_group_description;
use App\Repositories\oc_attribute_group_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_group_descriptionRepositoryTest extends TestCase
{
    use Makeoc_attribute_group_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_attribute_group_descriptionRepository
     */
    protected $ocAttributeGroupDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAttributeGroupDescriptionRepo = App::make(oc_attribute_group_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->fakeoc_attribute_group_descriptionData();
        $createdoc_attribute_group_description = $this->ocAttributeGroupDescriptionRepo->create($ocAttributeGroupDescription);
        $createdoc_attribute_group_description = $createdoc_attribute_group_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_attribute_group_description);
        $this->assertNotNull($createdoc_attribute_group_description['id'], 'Created oc_attribute_group_description must have id specified');
        $this->assertNotNull(oc_attribute_group_description::find($createdoc_attribute_group_description['id']), 'oc_attribute_group_description with given id must be in DB');
        $this->assertModelData($ocAttributeGroupDescription, $createdoc_attribute_group_description);
    }

    /**
     * @test read
     */
    public function testReadoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $dboc_attribute_group_description = $this->ocAttributeGroupDescriptionRepo->find($ocAttributeGroupDescription->id);
        $dboc_attribute_group_description = $dboc_attribute_group_description->toArray();
        $this->assertModelData($ocAttributeGroupDescription->toArray(), $dboc_attribute_group_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $fakeoc_attribute_group_description = $this->fakeoc_attribute_group_descriptionData();
        $updatedoc_attribute_group_description = $this->ocAttributeGroupDescriptionRepo->update($fakeoc_attribute_group_description, $ocAttributeGroupDescription->id);
        $this->assertModelData($fakeoc_attribute_group_description, $updatedoc_attribute_group_description->toArray());
        $dboc_attribute_group_description = $this->ocAttributeGroupDescriptionRepo->find($ocAttributeGroupDescription->id);
        $this->assertModelData($fakeoc_attribute_group_description, $dboc_attribute_group_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_attribute_group_description()
    {
        $ocAttributeGroupDescription = $this->makeoc_attribute_group_description();
        $resp = $this->ocAttributeGroupDescriptionRepo->delete($ocAttributeGroupDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_attribute_group_description::find($ocAttributeGroupDescription->id), 'oc_attribute_group_description should not exist in DB');
    }
}
