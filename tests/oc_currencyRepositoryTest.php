<?php

use App\Models\oc_currency;
use App\Repositories\oc_currencyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_currencyRepositoryTest extends TestCase
{
    use Makeoc_currencyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_currencyRepository
     */
    protected $ocCurrencyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCurrencyRepo = App::make(oc_currencyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_currency()
    {
        $ocCurrency = $this->fakeoc_currencyData();
        $createdoc_currency = $this->ocCurrencyRepo->create($ocCurrency);
        $createdoc_currency = $createdoc_currency->toArray();
        $this->assertArrayHasKey('id', $createdoc_currency);
        $this->assertNotNull($createdoc_currency['id'], 'Created oc_currency must have id specified');
        $this->assertNotNull(oc_currency::find($createdoc_currency['id']), 'oc_currency with given id must be in DB');
        $this->assertModelData($ocCurrency, $createdoc_currency);
    }

    /**
     * @test read
     */
    public function testReadoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $dboc_currency = $this->ocCurrencyRepo->find($ocCurrency->id);
        $dboc_currency = $dboc_currency->toArray();
        $this->assertModelData($ocCurrency->toArray(), $dboc_currency);
    }

    /**
     * @test update
     */
    public function testUpdateoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $fakeoc_currency = $this->fakeoc_currencyData();
        $updatedoc_currency = $this->ocCurrencyRepo->update($fakeoc_currency, $ocCurrency->id);
        $this->assertModelData($fakeoc_currency, $updatedoc_currency->toArray());
        $dboc_currency = $this->ocCurrencyRepo->find($ocCurrency->id);
        $this->assertModelData($fakeoc_currency, $dboc_currency->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_currency()
    {
        $ocCurrency = $this->makeoc_currency();
        $resp = $this->ocCurrencyRepo->delete($ocCurrency->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_currency::find($ocCurrency->id), 'oc_currency should not exist in DB');
    }
}
