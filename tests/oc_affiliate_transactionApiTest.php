<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_transactionApiTest extends TestCase
{
    use Makeoc_affiliate_transactionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->fakeoc_affiliate_transactionData();
        $this->json('POST', '/api/v1/ocAffiliateTransactions', $ocAffiliateTransaction);

        $this->assertApiResponse($ocAffiliateTransaction);
    }

    /**
     * @test
     */
    public function testReadoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $this->json('GET', '/api/v1/ocAffiliateTransactions/'.$ocAffiliateTransaction->id);

        $this->assertApiResponse($ocAffiliateTransaction->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $editedoc_affiliate_transaction = $this->fakeoc_affiliate_transactionData();

        $this->json('PUT', '/api/v1/ocAffiliateTransactions/'.$ocAffiliateTransaction->id, $editedoc_affiliate_transaction);

        $this->assertApiResponse($editedoc_affiliate_transaction);
    }

    /**
     * @test
     */
    public function testDeleteoc_affiliate_transaction()
    {
        $ocAffiliateTransaction = $this->makeoc_affiliate_transaction();
        $this->json('DELETE', '/api/v1/ocAffiliateTransactions/'.$ocAffiliateTransaction->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAffiliateTransactions/'.$ocAffiliateTransaction->id);

        $this->assertResponseStatus(404);
    }
}
