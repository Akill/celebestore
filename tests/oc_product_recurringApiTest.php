<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_recurringApiTest extends TestCase
{
    use Makeoc_product_recurringTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_recurring()
    {
        $ocProductRecurring = $this->fakeoc_product_recurringData();
        $this->json('POST', '/api/v1/ocProductRecurrings', $ocProductRecurring);

        $this->assertApiResponse($ocProductRecurring);
    }

    /**
     * @test
     */
    public function testReadoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $this->json('GET', '/api/v1/ocProductRecurrings/'.$ocProductRecurring->id);

        $this->assertApiResponse($ocProductRecurring->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $editedoc_product_recurring = $this->fakeoc_product_recurringData();

        $this->json('PUT', '/api/v1/ocProductRecurrings/'.$ocProductRecurring->id, $editedoc_product_recurring);

        $this->assertApiResponse($editedoc_product_recurring);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_recurring()
    {
        $ocProductRecurring = $this->makeoc_product_recurring();
        $this->json('DELETE', '/api/v1/ocProductRecurrings/'.$ocProductRecurring->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductRecurrings/'.$ocProductRecurring->id);

        $this->assertResponseStatus(404);
    }
}
