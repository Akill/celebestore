<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_valueApiTest extends TestCase
{
    use Makeoc_option_valueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_option_value()
    {
        $ocOptionValue = $this->fakeoc_option_valueData();
        $this->json('POST', '/api/v1/ocOptionValues', $ocOptionValue);

        $this->assertApiResponse($ocOptionValue);
    }

    /**
     * @test
     */
    public function testReadoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $this->json('GET', '/api/v1/ocOptionValues/'.$ocOptionValue->id);

        $this->assertApiResponse($ocOptionValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $editedoc_option_value = $this->fakeoc_option_valueData();

        $this->json('PUT', '/api/v1/ocOptionValues/'.$ocOptionValue->id, $editedoc_option_value);

        $this->assertApiResponse($editedoc_option_value);
    }

    /**
     * @test
     */
    public function testDeleteoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $this->json('DELETE', '/api/v1/ocOptionValues/'.$ocOptionValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOptionValues/'.$ocOptionValue->id);

        $this->assertResponseStatus(404);
    }
}
