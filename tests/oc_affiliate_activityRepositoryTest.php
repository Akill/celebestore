<?php

use App\Models\oc_affiliate_activity;
use App\Repositories\oc_affiliate_activityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_affiliate_activityRepositoryTest extends TestCase
{
    use Makeoc_affiliate_activityTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_affiliate_activityRepository
     */
    protected $ocAffiliateActivityRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocAffiliateActivityRepo = App::make(oc_affiliate_activityRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->fakeoc_affiliate_activityData();
        $createdoc_affiliate_activity = $this->ocAffiliateActivityRepo->create($ocAffiliateActivity);
        $createdoc_affiliate_activity = $createdoc_affiliate_activity->toArray();
        $this->assertArrayHasKey('id', $createdoc_affiliate_activity);
        $this->assertNotNull($createdoc_affiliate_activity['id'], 'Created oc_affiliate_activity must have id specified');
        $this->assertNotNull(oc_affiliate_activity::find($createdoc_affiliate_activity['id']), 'oc_affiliate_activity with given id must be in DB');
        $this->assertModelData($ocAffiliateActivity, $createdoc_affiliate_activity);
    }

    /**
     * @test read
     */
    public function testReadoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $dboc_affiliate_activity = $this->ocAffiliateActivityRepo->find($ocAffiliateActivity->id);
        $dboc_affiliate_activity = $dboc_affiliate_activity->toArray();
        $this->assertModelData($ocAffiliateActivity->toArray(), $dboc_affiliate_activity);
    }

    /**
     * @test update
     */
    public function testUpdateoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $fakeoc_affiliate_activity = $this->fakeoc_affiliate_activityData();
        $updatedoc_affiliate_activity = $this->ocAffiliateActivityRepo->update($fakeoc_affiliate_activity, $ocAffiliateActivity->id);
        $this->assertModelData($fakeoc_affiliate_activity, $updatedoc_affiliate_activity->toArray());
        $dboc_affiliate_activity = $this->ocAffiliateActivityRepo->find($ocAffiliateActivity->id);
        $this->assertModelData($fakeoc_affiliate_activity, $dboc_affiliate_activity->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_affiliate_activity()
    {
        $ocAffiliateActivity = $this->makeoc_affiliate_activity();
        $resp = $this->ocAffiliateActivityRepo->delete($ocAffiliateActivity->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_affiliate_activity::find($ocAffiliateActivity->id), 'oc_affiliate_activity should not exist in DB');
    }
}
