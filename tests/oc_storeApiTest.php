<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_storeApiTest extends TestCase
{
    use Makeoc_storeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_store()
    {
        $ocStore = $this->fakeoc_storeData();
        $this->json('POST', '/api/v1/ocStores', $ocStore);

        $this->assertApiResponse($ocStore);
    }

    /**
     * @test
     */
    public function testReadoc_store()
    {
        $ocStore = $this->makeoc_store();
        $this->json('GET', '/api/v1/ocStores/'.$ocStore->id);

        $this->assertApiResponse($ocStore->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_store()
    {
        $ocStore = $this->makeoc_store();
        $editedoc_store = $this->fakeoc_storeData();

        $this->json('PUT', '/api/v1/ocStores/'.$ocStore->id, $editedoc_store);

        $this->assertApiResponse($editedoc_store);
    }

    /**
     * @test
     */
    public function testDeleteoc_store()
    {
        $ocStore = $this->makeoc_store();
        $this->json('DELETE', '/api/v1/ocStores/'.$ocStore->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocStores/'.$ocStore->id);

        $this->assertResponseStatus(404);
    }
}
