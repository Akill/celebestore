<?php

use App\Models\oc_voucher_theme;
use App\Repositories\oc_voucher_themeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_themeRepositoryTest extends TestCase
{
    use Makeoc_voucher_themeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_voucher_themeRepository
     */
    protected $ocVoucherThemeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocVoucherThemeRepo = App::make(oc_voucher_themeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_voucher_theme()
    {
        $ocVoucherTheme = $this->fakeoc_voucher_themeData();
        $createdoc_voucher_theme = $this->ocVoucherThemeRepo->create($ocVoucherTheme);
        $createdoc_voucher_theme = $createdoc_voucher_theme->toArray();
        $this->assertArrayHasKey('id', $createdoc_voucher_theme);
        $this->assertNotNull($createdoc_voucher_theme['id'], 'Created oc_voucher_theme must have id specified');
        $this->assertNotNull(oc_voucher_theme::find($createdoc_voucher_theme['id']), 'oc_voucher_theme with given id must be in DB');
        $this->assertModelData($ocVoucherTheme, $createdoc_voucher_theme);
    }

    /**
     * @test read
     */
    public function testReadoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $dboc_voucher_theme = $this->ocVoucherThemeRepo->find($ocVoucherTheme->id);
        $dboc_voucher_theme = $dboc_voucher_theme->toArray();
        $this->assertModelData($ocVoucherTheme->toArray(), $dboc_voucher_theme);
    }

    /**
     * @test update
     */
    public function testUpdateoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $fakeoc_voucher_theme = $this->fakeoc_voucher_themeData();
        $updatedoc_voucher_theme = $this->ocVoucherThemeRepo->update($fakeoc_voucher_theme, $ocVoucherTheme->id);
        $this->assertModelData($fakeoc_voucher_theme, $updatedoc_voucher_theme->toArray());
        $dboc_voucher_theme = $this->ocVoucherThemeRepo->find($ocVoucherTheme->id);
        $this->assertModelData($fakeoc_voucher_theme, $dboc_voucher_theme->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $resp = $this->ocVoucherThemeRepo->delete($ocVoucherTheme->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_voucher_theme::find($ocVoucherTheme->id), 'oc_voucher_theme should not exist in DB');
    }
}
