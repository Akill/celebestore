<?php

use App\Models\oc_category_filter;
use App\Repositories\oc_category_filterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_filterRepositoryTest extends TestCase
{
    use Makeoc_category_filterTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_category_filterRepository
     */
    protected $ocCategoryFilterRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryFilterRepo = App::make(oc_category_filterRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category_filter()
    {
        $ocCategoryFilter = $this->fakeoc_category_filterData();
        $createdoc_category_filter = $this->ocCategoryFilterRepo->create($ocCategoryFilter);
        $createdoc_category_filter = $createdoc_category_filter->toArray();
        $this->assertArrayHasKey('id', $createdoc_category_filter);
        $this->assertNotNull($createdoc_category_filter['id'], 'Created oc_category_filter must have id specified');
        $this->assertNotNull(oc_category_filter::find($createdoc_category_filter['id']), 'oc_category_filter with given id must be in DB');
        $this->assertModelData($ocCategoryFilter, $createdoc_category_filter);
    }

    /**
     * @test read
     */
    public function testReadoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $dboc_category_filter = $this->ocCategoryFilterRepo->find($ocCategoryFilter->id);
        $dboc_category_filter = $dboc_category_filter->toArray();
        $this->assertModelData($ocCategoryFilter->toArray(), $dboc_category_filter);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $fakeoc_category_filter = $this->fakeoc_category_filterData();
        $updatedoc_category_filter = $this->ocCategoryFilterRepo->update($fakeoc_category_filter, $ocCategoryFilter->id);
        $this->assertModelData($fakeoc_category_filter, $updatedoc_category_filter->toArray());
        $dboc_category_filter = $this->ocCategoryFilterRepo->find($ocCategoryFilter->id);
        $this->assertModelData($fakeoc_category_filter, $dboc_category_filter->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category_filter()
    {
        $ocCategoryFilter = $this->makeoc_category_filter();
        $resp = $this->ocCategoryFilterRepo->delete($ocCategoryFilter->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category_filter::find($ocCategoryFilter->id), 'oc_category_filter should not exist in DB');
    }
}
