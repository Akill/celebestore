<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_bannerApiTest extends TestCase
{
    use Makeoc_bannerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_banner()
    {
        $ocBanner = $this->fakeoc_bannerData();
        $this->json('POST', '/api/v1/ocBanners', $ocBanner);

        $this->assertApiResponse($ocBanner);
    }

    /**
     * @test
     */
    public function testReadoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $this->json('GET', '/api/v1/ocBanners/'.$ocBanner->id);

        $this->assertApiResponse($ocBanner->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $editedoc_banner = $this->fakeoc_bannerData();

        $this->json('PUT', '/api/v1/ocBanners/'.$ocBanner->id, $editedoc_banner);

        $this->assertApiResponse($editedoc_banner);
    }

    /**
     * @test
     */
    public function testDeleteoc_banner()
    {
        $ocBanner = $this->makeoc_banner();
        $this->json('DELETE', '/api/v1/ocBanners/'.$ocBanner->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocBanners/'.$ocBanner->id);

        $this->assertResponseStatus(404);
    }
}
