<?php

use Faker\Factory as Faker;
use App\Models\oc_filter_description;
use App\Repositories\oc_filter_descriptionRepository;

trait Makeoc_filter_descriptionTrait
{
    /**
     * Create fake instance of oc_filter_description and save it in database
     *
     * @param array $ocFilterDescriptionFields
     * @return oc_filter_description
     */
    public function makeoc_filter_description($ocFilterDescriptionFields = [])
    {
        /** @var oc_filter_descriptionRepository $ocFilterDescriptionRepo */
        $ocFilterDescriptionRepo = App::make(oc_filter_descriptionRepository::class);
        $theme = $this->fakeoc_filter_descriptionData($ocFilterDescriptionFields);
        return $ocFilterDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_filter_description
     *
     * @param array $ocFilterDescriptionFields
     * @return oc_filter_description
     */
    public function fakeoc_filter_description($ocFilterDescriptionFields = [])
    {
        return new oc_filter_description($this->fakeoc_filter_descriptionData($ocFilterDescriptionFields));
    }

    /**
     * Get fake data of oc_filter_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_filter_descriptionData($ocFilterDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'filter_group_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocFilterDescriptionFields);
    }
}
