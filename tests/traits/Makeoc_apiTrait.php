<?php

use Faker\Factory as Faker;
use App\Models\oc_api;
use App\Repositories\oc_apiRepository;

trait Makeoc_apiTrait
{
    /**
     * Create fake instance of oc_api and save it in database
     *
     * @param array $ocApiFields
     * @return oc_api
     */
    public function makeoc_api($ocApiFields = [])
    {
        /** @var oc_apiRepository $ocApiRepo */
        $ocApiRepo = App::make(oc_apiRepository::class);
        $theme = $this->fakeoc_apiData($ocApiFields);
        return $ocApiRepo->create($theme);
    }

    /**
     * Get fake instance of oc_api
     *
     * @param array $ocApiFields
     * @return oc_api
     */
    public function fakeoc_api($ocApiFields = [])
    {
        return new oc_api($this->fakeoc_apiData($ocApiFields));
    }

    /**
     * Get fake data of oc_api
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_apiData($ocApiFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'username' => $fake->word,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'password' => $fake->text,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocApiFields);
    }
}
