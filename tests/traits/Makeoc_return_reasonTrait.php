<?php

use Faker\Factory as Faker;
use App\Models\oc_return_reason;
use App\Repositories\oc_return_reasonRepository;

trait Makeoc_return_reasonTrait
{
    /**
     * Create fake instance of oc_return_reason and save it in database
     *
     * @param array $ocReturnReasonFields
     * @return oc_return_reason
     */
    public function makeoc_return_reason($ocReturnReasonFields = [])
    {
        /** @var oc_return_reasonRepository $ocReturnReasonRepo */
        $ocReturnReasonRepo = App::make(oc_return_reasonRepository::class);
        $theme = $this->fakeoc_return_reasonData($ocReturnReasonFields);
        return $ocReturnReasonRepo->create($theme);
    }

    /**
     * Get fake instance of oc_return_reason
     *
     * @param array $ocReturnReasonFields
     * @return oc_return_reason
     */
    public function fakeoc_return_reason($ocReturnReasonFields = [])
    {
        return new oc_return_reason($this->fakeoc_return_reasonData($ocReturnReasonFields));
    }

    /**
     * Get fake data of oc_return_reason
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_return_reasonData($ocReturnReasonFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocReturnReasonFields);
    }
}
