<?php

use Faker\Factory as Faker;
use App\Models\oc_setting;
use App\Repositories\oc_settingRepository;

trait Makeoc_settingTrait
{
    /**
     * Create fake instance of oc_setting and save it in database
     *
     * @param array $ocSettingFields
     * @return oc_setting
     */
    public function makeoc_setting($ocSettingFields = [])
    {
        /** @var oc_settingRepository $ocSettingRepo */
        $ocSettingRepo = App::make(oc_settingRepository::class);
        $theme = $this->fakeoc_settingData($ocSettingFields);
        return $ocSettingRepo->create($theme);
    }

    /**
     * Get fake instance of oc_setting
     *
     * @param array $ocSettingFields
     * @return oc_setting
     */
    public function fakeoc_setting($ocSettingFields = [])
    {
        return new oc_setting($this->fakeoc_settingData($ocSettingFields));
    }

    /**
     * Get fake data of oc_setting
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_settingData($ocSettingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'key' => $fake->word,
            'value' => $fake->text,
            'serialized' => $fake->word
        ], $ocSettingFields);
    }
}
