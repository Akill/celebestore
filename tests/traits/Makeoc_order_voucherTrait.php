<?php

use Faker\Factory as Faker;
use App\Models\oc_order_voucher;
use App\Repositories\oc_order_voucherRepository;

trait Makeoc_order_voucherTrait
{
    /**
     * Create fake instance of oc_order_voucher and save it in database
     *
     * @param array $ocOrderVoucherFields
     * @return oc_order_voucher
     */
    public function makeoc_order_voucher($ocOrderVoucherFields = [])
    {
        /** @var oc_order_voucherRepository $ocOrderVoucherRepo */
        $ocOrderVoucherRepo = App::make(oc_order_voucherRepository::class);
        $theme = $this->fakeoc_order_voucherData($ocOrderVoucherFields);
        return $ocOrderVoucherRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_voucher
     *
     * @param array $ocOrderVoucherFields
     * @return oc_order_voucher
     */
    public function fakeoc_order_voucher($ocOrderVoucherFields = [])
    {
        return new oc_order_voucher($this->fakeoc_order_voucherData($ocOrderVoucherFields));
    }

    /**
     * Get fake data of oc_order_voucher
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_voucherData($ocOrderVoucherFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'voucher_id' => $fake->randomDigitNotNull,
            'description' => $fake->word,
            'code' => $fake->word,
            'from_name' => $fake->word,
            'from_email' => $fake->word,
            'to_name' => $fake->word,
            'to_email' => $fake->word,
            'voucher_theme_id' => $fake->randomDigitNotNull,
            'message' => $fake->text,
            'amount' => $fake->word
        ], $ocOrderVoucherFields);
    }
}
