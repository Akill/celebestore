<?php

use Faker\Factory as Faker;
use App\Models\oc_product_related;
use App\Repositories\oc_product_relatedRepository;

trait Makeoc_product_relatedTrait
{
    /**
     * Create fake instance of oc_product_related and save it in database
     *
     * @param array $ocProductRelatedFields
     * @return oc_product_related
     */
    public function makeoc_product_related($ocProductRelatedFields = [])
    {
        /** @var oc_product_relatedRepository $ocProductRelatedRepo */
        $ocProductRelatedRepo = App::make(oc_product_relatedRepository::class);
        $theme = $this->fakeoc_product_relatedData($ocProductRelatedFields);
        return $ocProductRelatedRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_related
     *
     * @param array $ocProductRelatedFields
     * @return oc_product_related
     */
    public function fakeoc_product_related($ocProductRelatedFields = [])
    {
        return new oc_product_related($this->fakeoc_product_relatedData($ocProductRelatedFields));
    }

    /**
     * Get fake data of oc_product_related
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_relatedData($ocProductRelatedFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'related_id' => $fake->randomDigitNotNull
        ], $ocProductRelatedFields);
    }
}
