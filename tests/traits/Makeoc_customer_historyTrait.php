<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_history;
use App\Repositories\oc_customer_historyRepository;

trait Makeoc_customer_historyTrait
{
    /**
     * Create fake instance of oc_customer_history and save it in database
     *
     * @param array $ocCustomerHistoryFields
     * @return oc_customer_history
     */
    public function makeoc_customer_history($ocCustomerHistoryFields = [])
    {
        /** @var oc_customer_historyRepository $ocCustomerHistoryRepo */
        $ocCustomerHistoryRepo = App::make(oc_customer_historyRepository::class);
        $theme = $this->fakeoc_customer_historyData($ocCustomerHistoryFields);
        return $ocCustomerHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_history
     *
     * @param array $ocCustomerHistoryFields
     * @return oc_customer_history
     */
    public function fakeoc_customer_history($ocCustomerHistoryFields = [])
    {
        return new oc_customer_history($this->fakeoc_customer_historyData($ocCustomerHistoryFields));
    }

    /**
     * Get fake data of oc_customer_history
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_historyData($ocCustomerHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'comment' => $fake->text,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerHistoryFields);
    }
}
