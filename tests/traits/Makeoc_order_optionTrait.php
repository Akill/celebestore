<?php

use Faker\Factory as Faker;
use App\Models\oc_order_option;
use App\Repositories\oc_order_optionRepository;

trait Makeoc_order_optionTrait
{
    /**
     * Create fake instance of oc_order_option and save it in database
     *
     * @param array $ocOrderOptionFields
     * @return oc_order_option
     */
    public function makeoc_order_option($ocOrderOptionFields = [])
    {
        /** @var oc_order_optionRepository $ocOrderOptionRepo */
        $ocOrderOptionRepo = App::make(oc_order_optionRepository::class);
        $theme = $this->fakeoc_order_optionData($ocOrderOptionFields);
        return $ocOrderOptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_option
     *
     * @param array $ocOrderOptionFields
     * @return oc_order_option
     */
    public function fakeoc_order_option($ocOrderOptionFields = [])
    {
        return new oc_order_option($this->fakeoc_order_optionData($ocOrderOptionFields));
    }

    /**
     * Get fake data of oc_order_option
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_optionData($ocOrderOptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'order_product_id' => $fake->randomDigitNotNull,
            'product_option_id' => $fake->randomDigitNotNull,
            'product_option_value_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'value' => $fake->text,
            'type' => $fake->word
        ], $ocOrderOptionFields);
    }
}
