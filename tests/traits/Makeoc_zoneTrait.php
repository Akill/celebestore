<?php

use Faker\Factory as Faker;
use App\Models\oc_zone;
use App\Repositories\oc_zoneRepository;

trait Makeoc_zoneTrait
{
    /**
     * Create fake instance of oc_zone and save it in database
     *
     * @param array $ocZoneFields
     * @return oc_zone
     */
    public function makeoc_zone($ocZoneFields = [])
    {
        /** @var oc_zoneRepository $ocZoneRepo */
        $ocZoneRepo = App::make(oc_zoneRepository::class);
        $theme = $this->fakeoc_zoneData($ocZoneFields);
        return $ocZoneRepo->create($theme);
    }

    /**
     * Get fake instance of oc_zone
     *
     * @param array $ocZoneFields
     * @return oc_zone
     */
    public function fakeoc_zone($ocZoneFields = [])
    {
        return new oc_zone($this->fakeoc_zoneData($ocZoneFields));
    }

    /**
     * Get fake data of oc_zone
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_zoneData($ocZoneFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'country_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'code' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocZoneFields);
    }
}
