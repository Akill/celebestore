<?php

use Faker\Factory as Faker;
use App\Models\oc_return_history;
use App\Repositories\oc_return_historyRepository;

trait Makeoc_return_historyTrait
{
    /**
     * Create fake instance of oc_return_history and save it in database
     *
     * @param array $ocReturnHistoryFields
     * @return oc_return_history
     */
    public function makeoc_return_history($ocReturnHistoryFields = [])
    {
        /** @var oc_return_historyRepository $ocReturnHistoryRepo */
        $ocReturnHistoryRepo = App::make(oc_return_historyRepository::class);
        $theme = $this->fakeoc_return_historyData($ocReturnHistoryFields);
        return $ocReturnHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_return_history
     *
     * @param array $ocReturnHistoryFields
     * @return oc_return_history
     */
    public function fakeoc_return_history($ocReturnHistoryFields = [])
    {
        return new oc_return_history($this->fakeoc_return_historyData($ocReturnHistoryFields));
    }

    /**
     * Get fake data of oc_return_history
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_return_historyData($ocReturnHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'return_id' => $fake->randomDigitNotNull,
            'return_status_id' => $fake->randomDigitNotNull,
            'notify' => $fake->word,
            'comment' => $fake->text,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocReturnHistoryFields);
    }
}
