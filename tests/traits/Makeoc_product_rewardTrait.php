<?php

use Faker\Factory as Faker;
use App\Models\oc_product_reward;
use App\Repositories\oc_product_rewardRepository;

trait Makeoc_product_rewardTrait
{
    /**
     * Create fake instance of oc_product_reward and save it in database
     *
     * @param array $ocProductRewardFields
     * @return oc_product_reward
     */
    public function makeoc_product_reward($ocProductRewardFields = [])
    {
        /** @var oc_product_rewardRepository $ocProductRewardRepo */
        $ocProductRewardRepo = App::make(oc_product_rewardRepository::class);
        $theme = $this->fakeoc_product_rewardData($ocProductRewardFields);
        return $ocProductRewardRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_reward
     *
     * @param array $ocProductRewardFields
     * @return oc_product_reward
     */
    public function fakeoc_product_reward($ocProductRewardFields = [])
    {
        return new oc_product_reward($this->fakeoc_product_rewardData($ocProductRewardFields));
    }

    /**
     * Get fake data of oc_product_reward
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_rewardData($ocProductRewardFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'customer_group_id' => $fake->randomDigitNotNull,
            'points' => $fake->randomDigitNotNull
        ], $ocProductRewardFields);
    }
}
