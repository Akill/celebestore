<?php

use Faker\Factory as Faker;
use App\Models\oc_extension;
use App\Repositories\oc_extensionRepository;

trait Makeoc_extensionTrait
{
    /**
     * Create fake instance of oc_extension and save it in database
     *
     * @param array $ocExtensionFields
     * @return oc_extension
     */
    public function makeoc_extension($ocExtensionFields = [])
    {
        /** @var oc_extensionRepository $ocExtensionRepo */
        $ocExtensionRepo = App::make(oc_extensionRepository::class);
        $theme = $this->fakeoc_extensionData($ocExtensionFields);
        return $ocExtensionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_extension
     *
     * @param array $ocExtensionFields
     * @return oc_extension
     */
    public function fakeoc_extension($ocExtensionFields = [])
    {
        return new oc_extension($this->fakeoc_extensionData($ocExtensionFields));
    }

    /**
     * Get fake data of oc_extension
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_extensionData($ocExtensionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'code' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocExtensionFields);
    }
}
