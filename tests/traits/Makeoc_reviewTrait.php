<?php

use Faker\Factory as Faker;
use App\Models\oc_review;
use App\Repositories\oc_reviewRepository;

trait Makeoc_reviewTrait
{
    /**
     * Create fake instance of oc_review and save it in database
     *
     * @param array $ocReviewFields
     * @return oc_review
     */
    public function makeoc_review($ocReviewFields = [])
    {
        /** @var oc_reviewRepository $ocReviewRepo */
        $ocReviewRepo = App::make(oc_reviewRepository::class);
        $theme = $this->fakeoc_reviewData($ocReviewFields);
        return $ocReviewRepo->create($theme);
    }

    /**
     * Get fake instance of oc_review
     *
     * @param array $ocReviewFields
     * @return oc_review
     */
    public function fakeoc_review($ocReviewFields = [])
    {
        return new oc_review($this->fakeoc_reviewData($ocReviewFields));
    }

    /**
     * Get fake data of oc_review
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_reviewData($ocReviewFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'customer_id' => $fake->randomDigitNotNull,
            'author' => $fake->word,
            'text' => $fake->text,
            'rating' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocReviewFields);
    }
}
