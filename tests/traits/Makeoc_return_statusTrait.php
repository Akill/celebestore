<?php

use Faker\Factory as Faker;
use App\Models\oc_return_status;
use App\Repositories\oc_return_statusRepository;

trait Makeoc_return_statusTrait
{
    /**
     * Create fake instance of oc_return_status and save it in database
     *
     * @param array $ocReturnStatusFields
     * @return oc_return_status
     */
    public function makeoc_return_status($ocReturnStatusFields = [])
    {
        /** @var oc_return_statusRepository $ocReturnStatusRepo */
        $ocReturnStatusRepo = App::make(oc_return_statusRepository::class);
        $theme = $this->fakeoc_return_statusData($ocReturnStatusFields);
        return $ocReturnStatusRepo->create($theme);
    }

    /**
     * Get fake instance of oc_return_status
     *
     * @param array $ocReturnStatusFields
     * @return oc_return_status
     */
    public function fakeoc_return_status($ocReturnStatusFields = [])
    {
        return new oc_return_status($this->fakeoc_return_statusData($ocReturnStatusFields));
    }

    /**
     * Get fake data of oc_return_status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_return_statusData($ocReturnStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocReturnStatusFields);
    }
}
