<?php

use Faker\Factory as Faker;
use App\Models\oc_banner;
use App\Repositories\oc_bannerRepository;

trait Makeoc_bannerTrait
{
    /**
     * Create fake instance of oc_banner and save it in database
     *
     * @param array $ocBannerFields
     * @return oc_banner
     */
    public function makeoc_banner($ocBannerFields = [])
    {
        /** @var oc_bannerRepository $ocBannerRepo */
        $ocBannerRepo = App::make(oc_bannerRepository::class);
        $theme = $this->fakeoc_bannerData($ocBannerFields);
        return $ocBannerRepo->create($theme);
    }

    /**
     * Get fake instance of oc_banner
     *
     * @param array $ocBannerFields
     * @return oc_banner
     */
    public function fakeoc_banner($ocBannerFields = [])
    {
        return new oc_banner($this->fakeoc_bannerData($ocBannerFields));
    }

    /**
     * Get fake data of oc_banner
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_bannerData($ocBannerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocBannerFields);
    }
}
