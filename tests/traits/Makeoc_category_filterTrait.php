<?php

use Faker\Factory as Faker;
use App\Models\oc_category_filter;
use App\Repositories\oc_category_filterRepository;

trait Makeoc_category_filterTrait
{
    /**
     * Create fake instance of oc_category_filter and save it in database
     *
     * @param array $ocCategoryFilterFields
     * @return oc_category_filter
     */
    public function makeoc_category_filter($ocCategoryFilterFields = [])
    {
        /** @var oc_category_filterRepository $ocCategoryFilterRepo */
        $ocCategoryFilterRepo = App::make(oc_category_filterRepository::class);
        $theme = $this->fakeoc_category_filterData($ocCategoryFilterFields);
        return $ocCategoryFilterRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category_filter
     *
     * @param array $ocCategoryFilterFields
     * @return oc_category_filter
     */
    public function fakeoc_category_filter($ocCategoryFilterFields = [])
    {
        return new oc_category_filter($this->fakeoc_category_filterData($ocCategoryFilterFields));
    }

    /**
     * Get fake data of oc_category_filter
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_category_filterData($ocCategoryFilterFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'filter_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryFilterFields);
    }
}
