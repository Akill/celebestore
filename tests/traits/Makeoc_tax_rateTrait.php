<?php

use Faker\Factory as Faker;
use App\Models\oc_tax_rate;
use App\Repositories\oc_tax_rateRepository;

trait Makeoc_tax_rateTrait
{
    /**
     * Create fake instance of oc_tax_rate and save it in database
     *
     * @param array $ocTaxRateFields
     * @return oc_tax_rate
     */
    public function makeoc_tax_rate($ocTaxRateFields = [])
    {
        /** @var oc_tax_rateRepository $ocTaxRateRepo */
        $ocTaxRateRepo = App::make(oc_tax_rateRepository::class);
        $theme = $this->fakeoc_tax_rateData($ocTaxRateFields);
        return $ocTaxRateRepo->create($theme);
    }

    /**
     * Get fake instance of oc_tax_rate
     *
     * @param array $ocTaxRateFields
     * @return oc_tax_rate
     */
    public function fakeoc_tax_rate($ocTaxRateFields = [])
    {
        return new oc_tax_rate($this->fakeoc_tax_rateData($ocTaxRateFields));
    }

    /**
     * Get fake data of oc_tax_rate
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_tax_rateData($ocTaxRateFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'geo_zone_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'rate' => $fake->word,
            'type' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocTaxRateFields);
    }
}
