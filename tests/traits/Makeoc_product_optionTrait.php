<?php

use Faker\Factory as Faker;
use App\Models\oc_product_option;
use App\Repositories\oc_product_optionRepository;

trait Makeoc_product_optionTrait
{
    /**
     * Create fake instance of oc_product_option and save it in database
     *
     * @param array $ocProductOptionFields
     * @return oc_product_option
     */
    public function makeoc_product_option($ocProductOptionFields = [])
    {
        /** @var oc_product_optionRepository $ocProductOptionRepo */
        $ocProductOptionRepo = App::make(oc_product_optionRepository::class);
        $theme = $this->fakeoc_product_optionData($ocProductOptionFields);
        return $ocProductOptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_option
     *
     * @param array $ocProductOptionFields
     * @return oc_product_option
     */
    public function fakeoc_product_option($ocProductOptionFields = [])
    {
        return new oc_product_option($this->fakeoc_product_optionData($ocProductOptionFields));
    }

    /**
     * Get fake data of oc_product_option
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_optionData($ocProductOptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'option_id' => $fake->randomDigitNotNull,
            'value' => $fake->text,
            'required' => $fake->word
        ], $ocProductOptionFields);
    }
}
