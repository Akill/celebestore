<?php

use Faker\Factory as Faker;
use App\Models\oc_download;
use App\Repositories\oc_downloadRepository;

trait Makeoc_downloadTrait
{
    /**
     * Create fake instance of oc_download and save it in database
     *
     * @param array $ocDownloadFields
     * @return oc_download
     */
    public function makeoc_download($ocDownloadFields = [])
    {
        /** @var oc_downloadRepository $ocDownloadRepo */
        $ocDownloadRepo = App::make(oc_downloadRepository::class);
        $theme = $this->fakeoc_downloadData($ocDownloadFields);
        return $ocDownloadRepo->create($theme);
    }

    /**
     * Get fake instance of oc_download
     *
     * @param array $ocDownloadFields
     * @return oc_download
     */
    public function fakeoc_download($ocDownloadFields = [])
    {
        return new oc_download($this->fakeoc_downloadData($ocDownloadFields));
    }

    /**
     * Get fake data of oc_download
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_downloadData($ocDownloadFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'filename' => $fake->word,
            'mask' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocDownloadFields);
    }
}
