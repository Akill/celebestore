<?php

use Faker\Factory as Faker;
use App\Models\oc_filter_group_description;
use App\Repositories\oc_filter_group_descriptionRepository;

trait Makeoc_filter_group_descriptionTrait
{
    /**
     * Create fake instance of oc_filter_group_description and save it in database
     *
     * @param array $ocFilterGroupDescriptionFields
     * @return oc_filter_group_description
     */
    public function makeoc_filter_group_description($ocFilterGroupDescriptionFields = [])
    {
        /** @var oc_filter_group_descriptionRepository $ocFilterGroupDescriptionRepo */
        $ocFilterGroupDescriptionRepo = App::make(oc_filter_group_descriptionRepository::class);
        $theme = $this->fakeoc_filter_group_descriptionData($ocFilterGroupDescriptionFields);
        return $ocFilterGroupDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_filter_group_description
     *
     * @param array $ocFilterGroupDescriptionFields
     * @return oc_filter_group_description
     */
    public function fakeoc_filter_group_description($ocFilterGroupDescriptionFields = [])
    {
        return new oc_filter_group_description($this->fakeoc_filter_group_descriptionData($ocFilterGroupDescriptionFields));
    }

    /**
     * Get fake data of oc_filter_group_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_filter_group_descriptionData($ocFilterGroupDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocFilterGroupDescriptionFields);
    }
}
