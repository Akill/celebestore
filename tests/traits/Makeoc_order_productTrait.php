<?php

use Faker\Factory as Faker;
use App\Models\oc_order_product;
use App\Repositories\oc_order_productRepository;

trait Makeoc_order_productTrait
{
    /**
     * Create fake instance of oc_order_product and save it in database
     *
     * @param array $ocOrderProductFields
     * @return oc_order_product
     */
    public function makeoc_order_product($ocOrderProductFields = [])
    {
        /** @var oc_order_productRepository $ocOrderProductRepo */
        $ocOrderProductRepo = App::make(oc_order_productRepository::class);
        $theme = $this->fakeoc_order_productData($ocOrderProductFields);
        return $ocOrderProductRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_product
     *
     * @param array $ocOrderProductFields
     * @return oc_order_product
     */
    public function fakeoc_order_product($ocOrderProductFields = [])
    {
        return new oc_order_product($this->fakeoc_order_productData($ocOrderProductFields));
    }

    /**
     * Get fake data of oc_order_product
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_productData($ocOrderProductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'model' => $fake->word,
            'quantity' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'total' => $fake->word,
            'tax' => $fake->word,
            'reward' => $fake->randomDigitNotNull
        ], $ocOrderProductFields);
    }
}
