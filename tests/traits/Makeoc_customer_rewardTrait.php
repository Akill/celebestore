<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_reward;
use App\Repositories\oc_customer_rewardRepository;

trait Makeoc_customer_rewardTrait
{
    /**
     * Create fake instance of oc_customer_reward and save it in database
     *
     * @param array $ocCustomerRewardFields
     * @return oc_customer_reward
     */
    public function makeoc_customer_reward($ocCustomerRewardFields = [])
    {
        /** @var oc_customer_rewardRepository $ocCustomerRewardRepo */
        $ocCustomerRewardRepo = App::make(oc_customer_rewardRepository::class);
        $theme = $this->fakeoc_customer_rewardData($ocCustomerRewardFields);
        return $ocCustomerRewardRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_reward
     *
     * @param array $ocCustomerRewardFields
     * @return oc_customer_reward
     */
    public function fakeoc_customer_reward($ocCustomerRewardFields = [])
    {
        return new oc_customer_reward($this->fakeoc_customer_rewardData($ocCustomerRewardFields));
    }

    /**
     * Get fake data of oc_customer_reward
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_rewardData($ocCustomerRewardFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'order_id' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'points' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerRewardFields);
    }
}
