<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_online;
use App\Repositories\oc_customer_onlineRepository;

trait Makeoc_customer_onlineTrait
{
    /**
     * Create fake instance of oc_customer_online and save it in database
     *
     * @param array $ocCustomerOnlineFields
     * @return oc_customer_online
     */
    public function makeoc_customer_online($ocCustomerOnlineFields = [])
    {
        /** @var oc_customer_onlineRepository $ocCustomerOnlineRepo */
        $ocCustomerOnlineRepo = App::make(oc_customer_onlineRepository::class);
        $theme = $this->fakeoc_customer_onlineData($ocCustomerOnlineFields);
        return $ocCustomerOnlineRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_online
     *
     * @param array $ocCustomerOnlineFields
     * @return oc_customer_online
     */
    public function fakeoc_customer_online($ocCustomerOnlineFields = [])
    {
        return new oc_customer_online($this->fakeoc_customer_onlineData($ocCustomerOnlineFields));
    }

    /**
     * Get fake data of oc_customer_online
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_onlineData($ocCustomerOnlineFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'url' => $fake->text,
            'referer' => $fake->text,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerOnlineFields);
    }
}
