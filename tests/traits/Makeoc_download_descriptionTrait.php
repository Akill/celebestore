<?php

use Faker\Factory as Faker;
use App\Models\oc_download_description;
use App\Repositories\oc_download_descriptionRepository;

trait Makeoc_download_descriptionTrait
{
    /**
     * Create fake instance of oc_download_description and save it in database
     *
     * @param array $ocDownloadDescriptionFields
     * @return oc_download_description
     */
    public function makeoc_download_description($ocDownloadDescriptionFields = [])
    {
        /** @var oc_download_descriptionRepository $ocDownloadDescriptionRepo */
        $ocDownloadDescriptionRepo = App::make(oc_download_descriptionRepository::class);
        $theme = $this->fakeoc_download_descriptionData($ocDownloadDescriptionFields);
        return $ocDownloadDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_download_description
     *
     * @param array $ocDownloadDescriptionFields
     * @return oc_download_description
     */
    public function fakeoc_download_description($ocDownloadDescriptionFields = [])
    {
        return new oc_download_description($this->fakeoc_download_descriptionData($ocDownloadDescriptionFields));
    }

    /**
     * Get fake data of oc_download_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_download_descriptionData($ocDownloadDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocDownloadDescriptionFields);
    }
}
