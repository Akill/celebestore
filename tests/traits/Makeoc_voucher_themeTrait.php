<?php

use Faker\Factory as Faker;
use App\Models\oc_voucher_theme;
use App\Repositories\oc_voucher_themeRepository;

trait Makeoc_voucher_themeTrait
{
    /**
     * Create fake instance of oc_voucher_theme and save it in database
     *
     * @param array $ocVoucherThemeFields
     * @return oc_voucher_theme
     */
    public function makeoc_voucher_theme($ocVoucherThemeFields = [])
    {
        /** @var oc_voucher_themeRepository $ocVoucherThemeRepo */
        $ocVoucherThemeRepo = App::make(oc_voucher_themeRepository::class);
        $theme = $this->fakeoc_voucher_themeData($ocVoucherThemeFields);
        return $ocVoucherThemeRepo->create($theme);
    }

    /**
     * Get fake instance of oc_voucher_theme
     *
     * @param array $ocVoucherThemeFields
     * @return oc_voucher_theme
     */
    public function fakeoc_voucher_theme($ocVoucherThemeFields = [])
    {
        return new oc_voucher_theme($this->fakeoc_voucher_themeData($ocVoucherThemeFields));
    }

    /**
     * Get fake data of oc_voucher_theme
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_voucher_themeData($ocVoucherThemeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'image' => $fake->word
        ], $ocVoucherThemeFields);
    }
}
