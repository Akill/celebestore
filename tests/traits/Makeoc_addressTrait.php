<?php

use Faker\Factory as Faker;
use App\Models\oc_address;
use App\Repositories\oc_addressRepository;

trait Makeoc_addressTrait
{
    /**
     * Create fake instance of oc_address and save it in database
     *
     * @param array $ocAddressFields
     * @return oc_address
     */
    public function makeoc_address($ocAddressFields = [])
    {
        /** @var oc_addressRepository $ocAddressRepo */
        $ocAddressRepo = App::make(oc_addressRepository::class);
        $theme = $this->fakeoc_addressData($ocAddressFields);
        return $ocAddressRepo->create($theme);
    }

    /**
     * Get fake instance of oc_address
     *
     * @param array $ocAddressFields
     * @return oc_address
     */
    public function fakeoc_address($ocAddressFields = [])
    {
        return new oc_address($this->fakeoc_addressData($ocAddressFields));
    }

    /**
     * Get fake data of oc_address
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_addressData($ocAddressFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'sub_district_id' => $fake->randomDigitNotNull,
            'customer_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'company' => $fake->word,
            'address_1' => $fake->word,
            'address_2' => $fake->word,
            'city' => $fake->word,
            'postcode' => $fake->word,
            'country_id' => $fake->randomDigitNotNull,
            'zone_id' => $fake->randomDigitNotNull,
            'custom_field' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAddressFields);
    }
}
