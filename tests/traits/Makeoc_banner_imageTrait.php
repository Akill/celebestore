<?php

use Faker\Factory as Faker;
use App\Models\oc_banner_image;
use App\Repositories\oc_banner_imageRepository;

trait Makeoc_banner_imageTrait
{
    /**
     * Create fake instance of oc_banner_image and save it in database
     *
     * @param array $ocBannerImageFields
     * @return oc_banner_image
     */
    public function makeoc_banner_image($ocBannerImageFields = [])
    {
        /** @var oc_banner_imageRepository $ocBannerImageRepo */
        $ocBannerImageRepo = App::make(oc_banner_imageRepository::class);
        $theme = $this->fakeoc_banner_imageData($ocBannerImageFields);
        return $ocBannerImageRepo->create($theme);
    }

    /**
     * Get fake instance of oc_banner_image
     *
     * @param array $ocBannerImageFields
     * @return oc_banner_image
     */
    public function fakeoc_banner_image($ocBannerImageFields = [])
    {
        return new oc_banner_image($this->fakeoc_banner_imageData($ocBannerImageFields));
    }

    /**
     * Get fake data of oc_banner_image
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_banner_imageData($ocBannerImageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'banner_id' => $fake->randomDigitNotNull,
            'link' => $fake->word,
            'image' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocBannerImageFields);
    }
}
