<?php

use Faker\Factory as Faker;
use App\Models\oc_custom_field;
use App\Repositories\oc_custom_fieldRepository;

trait Makeoc_custom_fieldTrait
{
    /**
     * Create fake instance of oc_custom_field and save it in database
     *
     * @param array $ocCustomFieldFields
     * @return oc_custom_field
     */
    public function makeoc_custom_field($ocCustomFieldFields = [])
    {
        /** @var oc_custom_fieldRepository $ocCustomFieldRepo */
        $ocCustomFieldRepo = App::make(oc_custom_fieldRepository::class);
        $theme = $this->fakeoc_custom_fieldData($ocCustomFieldFields);
        return $ocCustomFieldRepo->create($theme);
    }

    /**
     * Get fake instance of oc_custom_field
     *
     * @param array $ocCustomFieldFields
     * @return oc_custom_field
     */
    public function fakeoc_custom_field($ocCustomFieldFields = [])
    {
        return new oc_custom_field($this->fakeoc_custom_fieldData($ocCustomFieldFields));
    }

    /**
     * Get fake data of oc_custom_field
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_custom_fieldData($ocCustomFieldFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'value' => $fake->text,
            'location' => $fake->word,
            'status' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomFieldFields);
    }
}
