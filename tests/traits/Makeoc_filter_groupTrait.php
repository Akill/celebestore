<?php

use Faker\Factory as Faker;
use App\Models\oc_filter_group;
use App\Repositories\oc_filter_groupRepository;

trait Makeoc_filter_groupTrait
{
    /**
     * Create fake instance of oc_filter_group and save it in database
     *
     * @param array $ocFilterGroupFields
     * @return oc_filter_group
     */
    public function makeoc_filter_group($ocFilterGroupFields = [])
    {
        /** @var oc_filter_groupRepository $ocFilterGroupRepo */
        $ocFilterGroupRepo = App::make(oc_filter_groupRepository::class);
        $theme = $this->fakeoc_filter_groupData($ocFilterGroupFields);
        return $ocFilterGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_filter_group
     *
     * @param array $ocFilterGroupFields
     * @return oc_filter_group
     */
    public function fakeoc_filter_group($ocFilterGroupFields = [])
    {
        return new oc_filter_group($this->fakeoc_filter_groupData($ocFilterGroupFields));
    }

    /**
     * Get fake data of oc_filter_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_filter_groupData($ocFilterGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'sort_order' => $fake->randomDigitNotNull
        ], $ocFilterGroupFields);
    }
}
