<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_group;
use App\Repositories\oc_customer_groupRepository;

trait Makeoc_customer_groupTrait
{
    /**
     * Create fake instance of oc_customer_group and save it in database
     *
     * @param array $ocCustomerGroupFields
     * @return oc_customer_group
     */
    public function makeoc_customer_group($ocCustomerGroupFields = [])
    {
        /** @var oc_customer_groupRepository $ocCustomerGroupRepo */
        $ocCustomerGroupRepo = App::make(oc_customer_groupRepository::class);
        $theme = $this->fakeoc_customer_groupData($ocCustomerGroupFields);
        return $ocCustomerGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_group
     *
     * @param array $ocCustomerGroupFields
     * @return oc_customer_group
     */
    public function fakeoc_customer_group($ocCustomerGroupFields = [])
    {
        return new oc_customer_group($this->fakeoc_customer_groupData($ocCustomerGroupFields));
    }

    /**
     * Get fake data of oc_customer_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_groupData($ocCustomerGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'approval' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerGroupFields);
    }
}
