<?php

use Faker\Factory as Faker;
use App\Models\oc_product_special;
use App\Repositories\oc_product_specialRepository;

trait Makeoc_product_specialTrait
{
    /**
     * Create fake instance of oc_product_special and save it in database
     *
     * @param array $ocProductSpecialFields
     * @return oc_product_special
     */
    public function makeoc_product_special($ocProductSpecialFields = [])
    {
        /** @var oc_product_specialRepository $ocProductSpecialRepo */
        $ocProductSpecialRepo = App::make(oc_product_specialRepository::class);
        $theme = $this->fakeoc_product_specialData($ocProductSpecialFields);
        return $ocProductSpecialRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_special
     *
     * @param array $ocProductSpecialFields
     * @return oc_product_special
     */
    public function fakeoc_product_special($ocProductSpecialFields = [])
    {
        return new oc_product_special($this->fakeoc_product_specialData($ocProductSpecialFields));
    }

    /**
     * Get fake data of oc_product_special
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_specialData($ocProductSpecialFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'customer_group_id' => $fake->randomDigitNotNull,
            'priority' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'date_start' => $fake->word,
            'date_end' => $fake->word
        ], $ocProductSpecialFields);
    }
}
