<?php

use Faker\Factory as Faker;
use App\Models\oc_product_to_download;
use App\Repositories\oc_product_to_downloadRepository;

trait Makeoc_product_to_downloadTrait
{
    /**
     * Create fake instance of oc_product_to_download and save it in database
     *
     * @param array $ocProductToDownloadFields
     * @return oc_product_to_download
     */
    public function makeoc_product_to_download($ocProductToDownloadFields = [])
    {
        /** @var oc_product_to_downloadRepository $ocProductToDownloadRepo */
        $ocProductToDownloadRepo = App::make(oc_product_to_downloadRepository::class);
        $theme = $this->fakeoc_product_to_downloadData($ocProductToDownloadFields);
        return $ocProductToDownloadRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_to_download
     *
     * @param array $ocProductToDownloadFields
     * @return oc_product_to_download
     */
    public function fakeoc_product_to_download($ocProductToDownloadFields = [])
    {
        return new oc_product_to_download($this->fakeoc_product_to_downloadData($ocProductToDownloadFields));
    }

    /**
     * Get fake data of oc_product_to_download
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_to_downloadData($ocProductToDownloadFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'download_id' => $fake->randomDigitNotNull
        ], $ocProductToDownloadFields);
    }
}
