<?php

use Faker\Factory as Faker;
use App\Models\oc_product_shipping_filtered;
use App\Repositories\oc_product_shipping_filteredRepository;

trait Makeoc_product_shipping_filteredTrait
{
    /**
     * Create fake instance of oc_product_shipping_filtered and save it in database
     *
     * @param array $ocProductShippingFilteredFields
     * @return oc_product_shipping_filtered
     */
    public function makeoc_product_shipping_filtered($ocProductShippingFilteredFields = [])
    {
        /** @var oc_product_shipping_filteredRepository $ocProductShippingFilteredRepo */
        $ocProductShippingFilteredRepo = App::make(oc_product_shipping_filteredRepository::class);
        $theme = $this->fakeoc_product_shipping_filteredData($ocProductShippingFilteredFields);
        return $ocProductShippingFilteredRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_shipping_filtered
     *
     * @param array $ocProductShippingFilteredFields
     * @return oc_product_shipping_filtered
     */
    public function fakeoc_product_shipping_filtered($ocProductShippingFilteredFields = [])
    {
        return new oc_product_shipping_filtered($this->fakeoc_product_shipping_filteredData($ocProductShippingFilteredFields));
    }

    /**
     * Get fake data of oc_product_shipping_filtered
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_shipping_filteredData($ocProductShippingFilteredFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'shipping_code' => $fake->word
        ], $ocProductShippingFilteredFields);
    }
}
