<?php

use Faker\Factory as Faker;
use App\Models\oc_order_custom_field;
use App\Repositories\oc_order_custom_fieldRepository;

trait Makeoc_order_custom_fieldTrait
{
    /**
     * Create fake instance of oc_order_custom_field and save it in database
     *
     * @param array $ocOrderCustomFieldFields
     * @return oc_order_custom_field
     */
    public function makeoc_order_custom_field($ocOrderCustomFieldFields = [])
    {
        /** @var oc_order_custom_fieldRepository $ocOrderCustomFieldRepo */
        $ocOrderCustomFieldRepo = App::make(oc_order_custom_fieldRepository::class);
        $theme = $this->fakeoc_order_custom_fieldData($ocOrderCustomFieldFields);
        return $ocOrderCustomFieldRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_custom_field
     *
     * @param array $ocOrderCustomFieldFields
     * @return oc_order_custom_field
     */
    public function fakeoc_order_custom_field($ocOrderCustomFieldFields = [])
    {
        return new oc_order_custom_field($this->fakeoc_order_custom_fieldData($ocOrderCustomFieldFields));
    }

    /**
     * Get fake data of oc_order_custom_field
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_custom_fieldData($ocOrderCustomFieldFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'custom_field_id' => $fake->randomDigitNotNull,
            'custom_field_value_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'value' => $fake->text,
            'type' => $fake->word,
            'location' => $fake->word
        ], $ocOrderCustomFieldFields);
    }
}
