<?php

use Faker\Factory as Faker;
use App\Models\oc_layout_route;
use App\Repositories\oc_layout_routeRepository;

trait Makeoc_layout_routeTrait
{
    /**
     * Create fake instance of oc_layout_route and save it in database
     *
     * @param array $ocLayoutRouteFields
     * @return oc_layout_route
     */
    public function makeoc_layout_route($ocLayoutRouteFields = [])
    {
        /** @var oc_layout_routeRepository $ocLayoutRouteRepo */
        $ocLayoutRouteRepo = App::make(oc_layout_routeRepository::class);
        $theme = $this->fakeoc_layout_routeData($ocLayoutRouteFields);
        return $ocLayoutRouteRepo->create($theme);
    }

    /**
     * Get fake instance of oc_layout_route
     *
     * @param array $ocLayoutRouteFields
     * @return oc_layout_route
     */
    public function fakeoc_layout_route($ocLayoutRouteFields = [])
    {
        return new oc_layout_route($this->fakeoc_layout_routeData($ocLayoutRouteFields));
    }

    /**
     * Get fake data of oc_layout_route
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_layout_routeData($ocLayoutRouteFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'layout_id' => $fake->randomDigitNotNull,
            'store_id' => $fake->randomDigitNotNull,
            'route' => $fake->word
        ], $ocLayoutRouteFields);
    }
}
