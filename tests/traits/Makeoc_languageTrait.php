<?php

use Faker\Factory as Faker;
use App\Models\oc_language;
use App\Repositories\oc_languageRepository;

trait Makeoc_languageTrait
{
    /**
     * Create fake instance of oc_language and save it in database
     *
     * @param array $ocLanguageFields
     * @return oc_language
     */
    public function makeoc_language($ocLanguageFields = [])
    {
        /** @var oc_languageRepository $ocLanguageRepo */
        $ocLanguageRepo = App::make(oc_languageRepository::class);
        $theme = $this->fakeoc_languageData($ocLanguageFields);
        return $ocLanguageRepo->create($theme);
    }

    /**
     * Get fake instance of oc_language
     *
     * @param array $ocLanguageFields
     * @return oc_language
     */
    public function fakeoc_language($ocLanguageFields = [])
    {
        return new oc_language($this->fakeoc_languageData($ocLanguageFields));
    }

    /**
     * Get fake data of oc_language
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_languageData($ocLanguageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'locale' => $fake->word,
            'image' => $fake->word,
            'directory' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull,
            'status' => $fake->word
        ], $ocLanguageFields);
    }
}
