<?php

use Faker\Factory as Faker;
use App\Models\oc_product_to_category;
use App\Repositories\oc_product_to_categoryRepository;

trait Makeoc_product_to_categoryTrait
{
    /**
     * Create fake instance of oc_product_to_category and save it in database
     *
     * @param array $ocProductToCategoryFields
     * @return oc_product_to_category
     */
    public function makeoc_product_to_category($ocProductToCategoryFields = [])
    {
        /** @var oc_product_to_categoryRepository $ocProductToCategoryRepo */
        $ocProductToCategoryRepo = App::make(oc_product_to_categoryRepository::class);
        $theme = $this->fakeoc_product_to_categoryData($ocProductToCategoryFields);
        return $ocProductToCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_to_category
     *
     * @param array $ocProductToCategoryFields
     * @return oc_product_to_category
     */
    public function fakeoc_product_to_category($ocProductToCategoryFields = [])
    {
        return new oc_product_to_category($this->fakeoc_product_to_categoryData($ocProductToCategoryFields));
    }

    /**
     * Get fake data of oc_product_to_category
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_to_categoryData($ocProductToCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'category_id' => $fake->randomDigitNotNull
        ], $ocProductToCategoryFields);
    }
}
