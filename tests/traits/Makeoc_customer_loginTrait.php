<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_login;
use App\Repositories\oc_customer_loginRepository;

trait Makeoc_customer_loginTrait
{
    /**
     * Create fake instance of oc_customer_login and save it in database
     *
     * @param array $ocCustomerLoginFields
     * @return oc_customer_login
     */
    public function makeoc_customer_login($ocCustomerLoginFields = [])
    {
        /** @var oc_customer_loginRepository $ocCustomerLoginRepo */
        $ocCustomerLoginRepo = App::make(oc_customer_loginRepository::class);
        $theme = $this->fakeoc_customer_loginData($ocCustomerLoginFields);
        return $ocCustomerLoginRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_login
     *
     * @param array $ocCustomerLoginFields
     * @return oc_customer_login
     */
    public function fakeoc_customer_login($ocCustomerLoginFields = [])
    {
        return new oc_customer_login($this->fakeoc_customer_loginData($ocCustomerLoginFields));
    }

    /**
     * Get fake data of oc_customer_login
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_loginData($ocCustomerLoginFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'email' => $fake->word,
            'ip' => $fake->word,
            'total' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerLoginFields);
    }
}
