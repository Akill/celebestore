<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_group_description;
use App\Repositories\oc_customer_group_descriptionRepository;

trait Makeoc_customer_group_descriptionTrait
{
    /**
     * Create fake instance of oc_customer_group_description and save it in database
     *
     * @param array $ocCustomerGroupDescriptionFields
     * @return oc_customer_group_description
     */
    public function makeoc_customer_group_description($ocCustomerGroupDescriptionFields = [])
    {
        /** @var oc_customer_group_descriptionRepository $ocCustomerGroupDescriptionRepo */
        $ocCustomerGroupDescriptionRepo = App::make(oc_customer_group_descriptionRepository::class);
        $theme = $this->fakeoc_customer_group_descriptionData($ocCustomerGroupDescriptionFields);
        return $ocCustomerGroupDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_group_description
     *
     * @param array $ocCustomerGroupDescriptionFields
     * @return oc_customer_group_description
     */
    public function fakeoc_customer_group_description($ocCustomerGroupDescriptionFields = [])
    {
        return new oc_customer_group_description($this->fakeoc_customer_group_descriptionData($ocCustomerGroupDescriptionFields));
    }

    /**
     * Get fake data of oc_customer_group_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_group_descriptionData($ocCustomerGroupDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerGroupDescriptionFields);
    }
}
