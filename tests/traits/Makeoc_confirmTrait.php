<?php

use Faker\Factory as Faker;
use App\Models\oc_confirm;
use App\Repositories\oc_confirmRepository;

trait Makeoc_confirmTrait
{
    /**
     * Create fake instance of oc_confirm and save it in database
     *
     * @param array $ocConfirmFields
     * @return oc_confirm
     */
    public function makeoc_confirm($ocConfirmFields = [])
    {
        /** @var oc_confirmRepository $ocConfirmRepo */
        $ocConfirmRepo = App::make(oc_confirmRepository::class);
        $theme = $this->fakeoc_confirmData($ocConfirmFields);
        return $ocConfirmRepo->create($theme);
    }

    /**
     * Get fake instance of oc_confirm
     *
     * @param array $ocConfirmFields
     * @return oc_confirm
     */
    public function fakeoc_confirm($ocConfirmFields = [])
    {
        return new oc_confirm($this->fakeoc_confirmData($ocConfirmFields));
    }

    /**
     * Get fake data of oc_confirm
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_confirmData($ocConfirmFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'email' => $fake->word,
            'order_id' => $fake->word,
            'payment_date' => $fake->word,
            'total_amount' => $fake->randomDigitNotNull,
            'destination_bank' => $fake->word,
            'resi' => $fake->word,
            'payment_method' => $fake->word,
            'sender_name' => $fake->word,
            'bank_origin' => $fake->word,
            'code' => $fake->word,
            'no_receipt' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocConfirmFields);
    }
}
