<?php

use Faker\Factory as Faker;
use App\Models\oc_custom_field_customer_group;
use App\Repositories\oc_custom_field_customer_groupRepository;

trait Makeoc_custom_field_customer_groupTrait
{
    /**
     * Create fake instance of oc_custom_field_customer_group and save it in database
     *
     * @param array $ocCustomFieldCustomerGroupFields
     * @return oc_custom_field_customer_group
     */
    public function makeoc_custom_field_customer_group($ocCustomFieldCustomerGroupFields = [])
    {
        /** @var oc_custom_field_customer_groupRepository $ocCustomFieldCustomerGroupRepo */
        $ocCustomFieldCustomerGroupRepo = App::make(oc_custom_field_customer_groupRepository::class);
        $theme = $this->fakeoc_custom_field_customer_groupData($ocCustomFieldCustomerGroupFields);
        return $ocCustomFieldCustomerGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_custom_field_customer_group
     *
     * @param array $ocCustomFieldCustomerGroupFields
     * @return oc_custom_field_customer_group
     */
    public function fakeoc_custom_field_customer_group($ocCustomFieldCustomerGroupFields = [])
    {
        return new oc_custom_field_customer_group($this->fakeoc_custom_field_customer_groupData($ocCustomFieldCustomerGroupFields));
    }

    /**
     * Get fake data of oc_custom_field_customer_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_custom_field_customer_groupData($ocCustomFieldCustomerGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_group_id' => $fake->randomDigitNotNull,
            'required' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomFieldCustomerGroupFields);
    }
}
