<?php

use Faker\Factory as Faker;
use App\Models\oc_affiliate;
use App\Repositories\oc_affiliateRepository;

trait Makeoc_affiliateTrait
{
    /**
     * Create fake instance of oc_affiliate and save it in database
     *
     * @param array $ocAffiliateFields
     * @return oc_affiliate
     */
    public function makeoc_affiliate($ocAffiliateFields = [])
    {
        /** @var oc_affiliateRepository $ocAffiliateRepo */
        $ocAffiliateRepo = App::make(oc_affiliateRepository::class);
        $theme = $this->fakeoc_affiliateData($ocAffiliateFields);
        return $ocAffiliateRepo->create($theme);
    }

    /**
     * Get fake instance of oc_affiliate
     *
     * @param array $ocAffiliateFields
     * @return oc_affiliate
     */
    public function fakeoc_affiliate($ocAffiliateFields = [])
    {
        return new oc_affiliate($this->fakeoc_affiliateData($ocAffiliateFields));
    }

    /**
     * Get fake data of oc_affiliate
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_affiliateData($ocAffiliateFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'sub_district_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'email' => $fake->word,
            'telephone' => $fake->word,
            'fax' => $fake->word,
            'password' => $fake->word,
            'salt' => $fake->word,
            'company' => $fake->word,
            'website' => $fake->word,
            'address_1' => $fake->word,
            'address_2' => $fake->word,
            'city' => $fake->word,
            'postcode' => $fake->word,
            'country_id' => $fake->randomDigitNotNull,
            'zone_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'commission' => $fake->word,
            'tax' => $fake->word,
            'payment' => $fake->word,
            'cheque' => $fake->word,
            'paypal' => $fake->word,
            'bank_name' => $fake->word,
            'bank_branch_number' => $fake->word,
            'bank_swift_code' => $fake->word,
            'bank_account_name' => $fake->word,
            'bank_account_number' => $fake->word,
            'ip' => $fake->word,
            'status' => $fake->word,
            'approved' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAffiliateFields);
    }
}
