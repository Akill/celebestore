<?php

use Faker\Factory as Faker;
use App\Models\oc_voucher_history;
use App\Repositories\oc_voucher_historyRepository;

trait Makeoc_voucher_historyTrait
{
    /**
     * Create fake instance of oc_voucher_history and save it in database
     *
     * @param array $ocVoucherHistoryFields
     * @return oc_voucher_history
     */
    public function makeoc_voucher_history($ocVoucherHistoryFields = [])
    {
        /** @var oc_voucher_historyRepository $ocVoucherHistoryRepo */
        $ocVoucherHistoryRepo = App::make(oc_voucher_historyRepository::class);
        $theme = $this->fakeoc_voucher_historyData($ocVoucherHistoryFields);
        return $ocVoucherHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_voucher_history
     *
     * @param array $ocVoucherHistoryFields
     * @return oc_voucher_history
     */
    public function fakeoc_voucher_history($ocVoucherHistoryFields = [])
    {
        return new oc_voucher_history($this->fakeoc_voucher_historyData($ocVoucherHistoryFields));
    }

    /**
     * Get fake data of oc_voucher_history
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_voucher_historyData($ocVoucherHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'voucher_id' => $fake->randomDigitNotNull,
            'order_id' => $fake->randomDigitNotNull,
            'amount' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocVoucherHistoryFields);
    }
}
