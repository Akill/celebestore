<?php

use Faker\Factory as Faker;
use App\Models\oc_product_image;
use App\Repositories\oc_product_imageRepository;

trait Makeoc_product_imageTrait
{
    /**
     * Create fake instance of oc_product_image and save it in database
     *
     * @param array $ocProductImageFields
     * @return oc_product_image
     */
    public function makeoc_product_image($ocProductImageFields = [])
    {
        /** @var oc_product_imageRepository $ocProductImageRepo */
        $ocProductImageRepo = App::make(oc_product_imageRepository::class);
        $theme = $this->fakeoc_product_imageData($ocProductImageFields);
        return $ocProductImageRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_image
     *
     * @param array $ocProductImageFields
     * @return oc_product_image
     */
    public function fakeoc_product_image($ocProductImageFields = [])
    {
        return new oc_product_image($this->fakeoc_product_imageData($ocProductImageFields));
    }

    /**
     * Get fake data of oc_product_image
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_imageData($ocProductImageFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'image' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocProductImageFields);
    }
}
