<?php

use Faker\Factory as Faker;
use App\Models\oc_currency;
use App\Repositories\oc_currencyRepository;

trait Makeoc_currencyTrait
{
    /**
     * Create fake instance of oc_currency and save it in database
     *
     * @param array $ocCurrencyFields
     * @return oc_currency
     */
    public function makeoc_currency($ocCurrencyFields = [])
    {
        /** @var oc_currencyRepository $ocCurrencyRepo */
        $ocCurrencyRepo = App::make(oc_currencyRepository::class);
        $theme = $this->fakeoc_currencyData($ocCurrencyFields);
        return $ocCurrencyRepo->create($theme);
    }

    /**
     * Get fake instance of oc_currency
     *
     * @param array $ocCurrencyFields
     * @return oc_currency
     */
    public function fakeoc_currency($ocCurrencyFields = [])
    {
        return new oc_currency($this->fakeoc_currencyData($ocCurrencyFields));
    }

    /**
     * Get fake data of oc_currency
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_currencyData($ocCurrencyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'code' => $fake->word,
            'symbol_left' => $fake->word,
            'symbol_right' => $fake->word,
            'decimal_place' => $fake->word,
            'value' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCurrencyFields);
    }
}
