<?php

use Faker\Factory as Faker;
use App\Models\oc_product_recurring;
use App\Repositories\oc_product_recurringRepository;

trait Makeoc_product_recurringTrait
{
    /**
     * Create fake instance of oc_product_recurring and save it in database
     *
     * @param array $ocProductRecurringFields
     * @return oc_product_recurring
     */
    public function makeoc_product_recurring($ocProductRecurringFields = [])
    {
        /** @var oc_product_recurringRepository $ocProductRecurringRepo */
        $ocProductRecurringRepo = App::make(oc_product_recurringRepository::class);
        $theme = $this->fakeoc_product_recurringData($ocProductRecurringFields);
        return $ocProductRecurringRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_recurring
     *
     * @param array $ocProductRecurringFields
     * @return oc_product_recurring
     */
    public function fakeoc_product_recurring($ocProductRecurringFields = [])
    {
        return new oc_product_recurring($this->fakeoc_product_recurringData($ocProductRecurringFields));
    }

    /**
     * Get fake data of oc_product_recurring
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_recurringData($ocProductRecurringFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'recurring_id' => $fake->randomDigitNotNull,
            'customer_group_id' => $fake->randomDigitNotNull
        ], $ocProductRecurringFields);
    }
}
