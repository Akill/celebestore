<?php

use Faker\Factory as Faker;
use App\Models\oc_product_to_layout;
use App\Repositories\oc_product_to_layoutRepository;

trait Makeoc_product_to_layoutTrait
{
    /**
     * Create fake instance of oc_product_to_layout and save it in database
     *
     * @param array $ocProductToLayoutFields
     * @return oc_product_to_layout
     */
    public function makeoc_product_to_layout($ocProductToLayoutFields = [])
    {
        /** @var oc_product_to_layoutRepository $ocProductToLayoutRepo */
        $ocProductToLayoutRepo = App::make(oc_product_to_layoutRepository::class);
        $theme = $this->fakeoc_product_to_layoutData($ocProductToLayoutFields);
        return $ocProductToLayoutRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_to_layout
     *
     * @param array $ocProductToLayoutFields
     * @return oc_product_to_layout
     */
    public function fakeoc_product_to_layout($ocProductToLayoutFields = [])
    {
        return new oc_product_to_layout($this->fakeoc_product_to_layoutData($ocProductToLayoutFields));
    }

    /**
     * Get fake data of oc_product_to_layout
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_to_layoutData($ocProductToLayoutFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'layout_id' => $fake->randomDigitNotNull
        ], $ocProductToLayoutFields);
    }
}
