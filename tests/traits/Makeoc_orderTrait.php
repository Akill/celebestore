<?php

use Faker\Factory as Faker;
use App\Models\oc_order;
use App\Repositories\oc_orderRepository;

trait Makeoc_orderTrait
{
    /**
     * Create fake instance of oc_order and save it in database
     *
     * @param array $ocOrderFields
     * @return oc_order
     */
    public function makeoc_order($ocOrderFields = [])
    {
        /** @var oc_orderRepository $ocOrderRepo */
        $ocOrderRepo = App::make(oc_orderRepository::class);
        $theme = $this->fakeoc_orderData($ocOrderFields);
        return $ocOrderRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order
     *
     * @param array $ocOrderFields
     * @return oc_order
     */
    public function fakeoc_order($ocOrderFields = [])
    {
        return new oc_order($this->fakeoc_orderData($ocOrderFields));
    }

    /**
     * Get fake data of oc_order
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_orderData($ocOrderFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'invoice_no' => $fake->randomDigitNotNull,
            'invoice_prefix' => $fake->word,
            'store_id' => $fake->randomDigitNotNull,
            'store_name' => $fake->word,
            'store_url' => $fake->word,
            'customer_id' => $fake->randomDigitNotNull,
            'customer_group_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'email' => $fake->word,
            'telephone' => $fake->word,
            'fax' => $fake->word,
            'custom_field' => $fake->text,
            'payment_firstname' => $fake->word,
            'payment_lastname' => $fake->word,
            'payment_company' => $fake->word,
            'payment_address_1' => $fake->word,
            'payment_address_2' => $fake->word,
            'payment_city' => $fake->word,
            'payment_postcode' => $fake->word,
            'payment_country' => $fake->word,
            'payment_country_id' => $fake->randomDigitNotNull,
            'payment_zone' => $fake->word,
            'payment_zone_id' => $fake->randomDigitNotNull,
            'payment_sub_district_id' => $fake->randomDigitNotNull,
            'payment_address_format' => $fake->text,
            'payment_custom_field' => $fake->text,
            'payment_method' => $fake->word,
            'payment_code' => $fake->word,
            'shipping_firstname' => $fake->word,
            'shipping_lastname' => $fake->word,
            'shipping_company' => $fake->word,
            'shipping_address_1' => $fake->word,
            'shipping_address_2' => $fake->word,
            'shipping_city' => $fake->word,
            'shipping_postcode' => $fake->word,
            'shipping_country' => $fake->word,
            'shipping_country_id' => $fake->randomDigitNotNull,
            'shipping_zone' => $fake->word,
            'shipping_zone_id' => $fake->randomDigitNotNull,
            'shipping_sub_district_id' => $fake->randomDigitNotNull,
            'shipping_address_format' => $fake->text,
            'shipping_custom_field' => $fake->text,
            'shipping_method' => $fake->word,
            'shipping_code' => $fake->word,
            'comment' => $fake->text,
            'total' => $fake->word,
            'order_status_id' => $fake->randomDigitNotNull,
            'affiliate_id' => $fake->randomDigitNotNull,
            'commission' => $fake->word,
            'marketing_id' => $fake->randomDigitNotNull,
            'tracking' => $fake->word,
            'language_id' => $fake->randomDigitNotNull,
            'currency_id' => $fake->randomDigitNotNull,
            'currency_code' => $fake->word,
            'currency_value' => $fake->word,
            'ip' => $fake->word,
            'forwarded_ip' => $fake->word,
            'user_agent' => $fake->word,
            'accept_language' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocOrderFields);
    }
}
