<?php

use Faker\Factory as Faker;
use App\Models\oc_category;
use App\Repositories\oc_categoryRepository;

trait Makeoc_categoryTrait
{
    /**
     * Create fake instance of oc_category and save it in database
     *
     * @param array $ocCategoryFields
     * @return oc_category
     */
    public function makeoc_category($ocCategoryFields = [])
    {
        /** @var oc_categoryRepository $ocCategoryRepo */
        $ocCategoryRepo = App::make(oc_categoryRepository::class);
        $theme = $this->fakeoc_categoryData($ocCategoryFields);
        return $ocCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category
     *
     * @param array $ocCategoryFields
     * @return oc_category
     */
    public function fakeoc_category($ocCategoryFields = [])
    {
        return new oc_category($this->fakeoc_categoryData($ocCategoryFields));
    }

    /**
     * Get fake data of oc_category
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_categoryData($ocCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'image' => $fake->word,
            'parent_id' => $fake->randomDigitNotNull,
            'top' => $fake->word,
            'column' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryFields);
    }
}
