<?php

use Faker\Factory as Faker;
use App\Models\oc_geo_zone;
use App\Repositories\oc_geo_zoneRepository;

trait Makeoc_geo_zoneTrait
{
    /**
     * Create fake instance of oc_geo_zone and save it in database
     *
     * @param array $ocGeoZoneFields
     * @return oc_geo_zone
     */
    public function makeoc_geo_zone($ocGeoZoneFields = [])
    {
        /** @var oc_geo_zoneRepository $ocGeoZoneRepo */
        $ocGeoZoneRepo = App::make(oc_geo_zoneRepository::class);
        $theme = $this->fakeoc_geo_zoneData($ocGeoZoneFields);
        return $ocGeoZoneRepo->create($theme);
    }

    /**
     * Get fake instance of oc_geo_zone
     *
     * @param array $ocGeoZoneFields
     * @return oc_geo_zone
     */
    public function fakeoc_geo_zone($ocGeoZoneFields = [])
    {
        return new oc_geo_zone($this->fakeoc_geo_zoneData($ocGeoZoneFields));
    }

    /**
     * Get fake data of oc_geo_zone
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_geo_zoneData($ocGeoZoneFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocGeoZoneFields);
    }
}
