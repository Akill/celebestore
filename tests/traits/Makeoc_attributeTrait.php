<?php

use Faker\Factory as Faker;
use App\Models\oc_attribute;
use App\Repositories\oc_attributeRepository;

trait Makeoc_attributeTrait
{
    /**
     * Create fake instance of oc_attribute and save it in database
     *
     * @param array $ocAttributeFields
     * @return oc_attribute
     */
    public function makeoc_attribute($ocAttributeFields = [])
    {
        /** @var oc_attributeRepository $ocAttributeRepo */
        $ocAttributeRepo = App::make(oc_attributeRepository::class);
        $theme = $this->fakeoc_attributeData($ocAttributeFields);
        return $ocAttributeRepo->create($theme);
    }

    /**
     * Get fake instance of oc_attribute
     *
     * @param array $ocAttributeFields
     * @return oc_attribute
     */
    public function fakeoc_attribute($ocAttributeFields = [])
    {
        return new oc_attribute($this->fakeoc_attributeData($ocAttributeFields));
    }

    /**
     * Get fake data of oc_attribute
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_attributeData($ocAttributeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'attribute_group_id' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAttributeFields);
    }
}
