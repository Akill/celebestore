<?php

use Faker\Factory as Faker;
use App\Models\oc_modification;
use App\Repositories\oc_modificationRepository;

trait Makeoc_modificationTrait
{
    /**
     * Create fake instance of oc_modification and save it in database
     *
     * @param array $ocModificationFields
     * @return oc_modification
     */
    public function makeoc_modification($ocModificationFields = [])
    {
        /** @var oc_modificationRepository $ocModificationRepo */
        $ocModificationRepo = App::make(oc_modificationRepository::class);
        $theme = $this->fakeoc_modificationData($ocModificationFields);
        return $ocModificationRepo->create($theme);
    }

    /**
     * Get fake instance of oc_modification
     *
     * @param array $ocModificationFields
     * @return oc_modification
     */
    public function fakeoc_modification($ocModificationFields = [])
    {
        return new oc_modification($this->fakeoc_modificationData($ocModificationFields));
    }

    /**
     * Get fake data of oc_modification
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_modificationData($ocModificationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'author' => $fake->word,
            'version' => $fake->word,
            'link' => $fake->word,
            'xml' => $fake->text,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocModificationFields);
    }
}
