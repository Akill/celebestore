<?php

use Faker\Factory as Faker;
use App\Models\oc_order_recurring;
use App\Repositories\oc_order_recurringRepository;

trait Makeoc_order_recurringTrait
{
    /**
     * Create fake instance of oc_order_recurring and save it in database
     *
     * @param array $ocOrderRecurringFields
     * @return oc_order_recurring
     */
    public function makeoc_order_recurring($ocOrderRecurringFields = [])
    {
        /** @var oc_order_recurringRepository $ocOrderRecurringRepo */
        $ocOrderRecurringRepo = App::make(oc_order_recurringRepository::class);
        $theme = $this->fakeoc_order_recurringData($ocOrderRecurringFields);
        return $ocOrderRecurringRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_recurring
     *
     * @param array $ocOrderRecurringFields
     * @return oc_order_recurring
     */
    public function fakeoc_order_recurring($ocOrderRecurringFields = [])
    {
        return new oc_order_recurring($this->fakeoc_order_recurringData($ocOrderRecurringFields));
    }

    /**
     * Get fake data of oc_order_recurring
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_recurringData($ocOrderRecurringFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'reference' => $fake->word,
            'product_id' => $fake->randomDigitNotNull,
            'product_name' => $fake->word,
            'product_quantity' => $fake->randomDigitNotNull,
            'recurring_id' => $fake->randomDigitNotNull,
            'recurring_name' => $fake->word,
            'recurring_description' => $fake->word,
            'recurring_frequency' => $fake->word,
            'recurring_cycle' => $fake->word,
            'recurring_duration' => $fake->word,
            'recurring_price' => $fake->word,
            'trial' => $fake->word,
            'trial_frequency' => $fake->word,
            'trial_cycle' => $fake->word,
            'trial_duration' => $fake->word,
            'trial_price' => $fake->word,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocOrderRecurringFields);
    }
}
