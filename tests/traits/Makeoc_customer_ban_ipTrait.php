<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_ban_ip;
use App\Repositories\oc_customer_ban_ipRepository;

trait Makeoc_customer_ban_ipTrait
{
    /**
     * Create fake instance of oc_customer_ban_ip and save it in database
     *
     * @param array $ocCustomerBanIpFields
     * @return oc_customer_ban_ip
     */
    public function makeoc_customer_ban_ip($ocCustomerBanIpFields = [])
    {
        /** @var oc_customer_ban_ipRepository $ocCustomerBanIpRepo */
        $ocCustomerBanIpRepo = App::make(oc_customer_ban_ipRepository::class);
        $theme = $this->fakeoc_customer_ban_ipData($ocCustomerBanIpFields);
        return $ocCustomerBanIpRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_ban_ip
     *
     * @param array $ocCustomerBanIpFields
     * @return oc_customer_ban_ip
     */
    public function fakeoc_customer_ban_ip($ocCustomerBanIpFields = [])
    {
        return new oc_customer_ban_ip($this->fakeoc_customer_ban_ipData($ocCustomerBanIpFields));
    }

    /**
     * Get fake data of oc_customer_ban_ip
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_ban_ipData($ocCustomerBanIpFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ip' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerBanIpFields);
    }
}
