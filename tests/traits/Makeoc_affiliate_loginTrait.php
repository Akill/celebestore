<?php

use Faker\Factory as Faker;
use App\Models\oc_affiliate_login;
use App\Repositories\oc_affiliate_loginRepository;

trait Makeoc_affiliate_loginTrait
{
    /**
     * Create fake instance of oc_affiliate_login and save it in database
     *
     * @param array $ocAffiliateLoginFields
     * @return oc_affiliate_login
     */
    public function makeoc_affiliate_login($ocAffiliateLoginFields = [])
    {
        /** @var oc_affiliate_loginRepository $ocAffiliateLoginRepo */
        $ocAffiliateLoginRepo = App::make(oc_affiliate_loginRepository::class);
        $theme = $this->fakeoc_affiliate_loginData($ocAffiliateLoginFields);
        return $ocAffiliateLoginRepo->create($theme);
    }

    /**
     * Get fake instance of oc_affiliate_login
     *
     * @param array $ocAffiliateLoginFields
     * @return oc_affiliate_login
     */
    public function fakeoc_affiliate_login($ocAffiliateLoginFields = [])
    {
        return new oc_affiliate_login($this->fakeoc_affiliate_loginData($ocAffiliateLoginFields));
    }

    /**
     * Get fake data of oc_affiliate_login
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_affiliate_loginData($ocAffiliateLoginFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'email' => $fake->word,
            'ip' => $fake->word,
            'total' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAffiliateLoginFields);
    }
}
