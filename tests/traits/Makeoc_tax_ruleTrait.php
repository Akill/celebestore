<?php

use Faker\Factory as Faker;
use App\Models\oc_tax_rule;
use App\Repositories\oc_tax_ruleRepository;

trait Makeoc_tax_ruleTrait
{
    /**
     * Create fake instance of oc_tax_rule and save it in database
     *
     * @param array $ocTaxRuleFields
     * @return oc_tax_rule
     */
    public function makeoc_tax_rule($ocTaxRuleFields = [])
    {
        /** @var oc_tax_ruleRepository $ocTaxRuleRepo */
        $ocTaxRuleRepo = App::make(oc_tax_ruleRepository::class);
        $theme = $this->fakeoc_tax_ruleData($ocTaxRuleFields);
        return $ocTaxRuleRepo->create($theme);
    }

    /**
     * Get fake instance of oc_tax_rule
     *
     * @param array $ocTaxRuleFields
     * @return oc_tax_rule
     */
    public function fakeoc_tax_rule($ocTaxRuleFields = [])
    {
        return new oc_tax_rule($this->fakeoc_tax_ruleData($ocTaxRuleFields));
    }

    /**
     * Get fake data of oc_tax_rule
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_tax_ruleData($ocTaxRuleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'tax_class_id' => $fake->randomDigitNotNull,
            'tax_rate_id' => $fake->randomDigitNotNull,
            'based' => $fake->word,
            'priority' => $fake->randomDigitNotNull
        ], $ocTaxRuleFields);
    }
}
