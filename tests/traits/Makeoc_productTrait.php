<?php

use Faker\Factory as Faker;
use App\Models\oc_product;
use App\Repositories\oc_productRepository;

trait Makeoc_productTrait
{
    /**
     * Create fake instance of oc_product and save it in database
     *
     * @param array $ocProductFields
     * @return oc_product
     */
    public function makeoc_product($ocProductFields = [])
    {
        /** @var oc_productRepository $ocProductRepo */
        $ocProductRepo = App::make(oc_productRepository::class);
        $theme = $this->fakeoc_productData($ocProductFields);
        return $ocProductRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product
     *
     * @param array $ocProductFields
     * @return oc_product
     */
    public function fakeoc_product($ocProductFields = [])
    {
        return new oc_product($this->fakeoc_productData($ocProductFields));
    }

    /**
     * Get fake data of oc_product
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_productData($ocProductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'model' => $fake->word,
            'sku' => $fake->word,
            'upc' => $fake->word,
            'ean' => $fake->word,
            'jan' => $fake->word,
            'isbn' => $fake->word,
            'mpn' => $fake->word,
            'location' => $fake->word,
            'quantity' => $fake->randomDigitNotNull,
            'stock_status_id' => $fake->randomDigitNotNull,
            'image' => $fake->word,
            'manufacturer_id' => $fake->randomDigitNotNull,
            'shipping' => $fake->word,
            'price' => $fake->word,
            'points' => $fake->randomDigitNotNull,
            'tax_class_id' => $fake->randomDigitNotNull,
            'date_available' => $fake->word,
            'weight' => $fake->word,
            'weight_class_id' => $fake->randomDigitNotNull,
            'length' => $fake->word,
            'width' => $fake->word,
            'height' => $fake->word,
            'length_class_id' => $fake->randomDigitNotNull,
            'subtract' => $fake->word,
            'minimum' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'viewed' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->word
        ], $ocProductFields);
    }
}
