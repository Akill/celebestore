<?php

use Faker\Factory as Faker;
use App\Models\oc_voucher_theme_description;
use App\Repositories\oc_voucher_theme_descriptionRepository;

trait Makeoc_voucher_theme_descriptionTrait
{
    /**
     * Create fake instance of oc_voucher_theme_description and save it in database
     *
     * @param array $ocVoucherThemeDescriptionFields
     * @return oc_voucher_theme_description
     */
    public function makeoc_voucher_theme_description($ocVoucherThemeDescriptionFields = [])
    {
        /** @var oc_voucher_theme_descriptionRepository $ocVoucherThemeDescriptionRepo */
        $ocVoucherThemeDescriptionRepo = App::make(oc_voucher_theme_descriptionRepository::class);
        $theme = $this->fakeoc_voucher_theme_descriptionData($ocVoucherThemeDescriptionFields);
        return $ocVoucherThemeDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_voucher_theme_description
     *
     * @param array $ocVoucherThemeDescriptionFields
     * @return oc_voucher_theme_description
     */
    public function fakeoc_voucher_theme_description($ocVoucherThemeDescriptionFields = [])
    {
        return new oc_voucher_theme_description($this->fakeoc_voucher_theme_descriptionData($ocVoucherThemeDescriptionFields));
    }

    /**
     * Get fake data of oc_voucher_theme_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_voucher_theme_descriptionData($ocVoucherThemeDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocVoucherThemeDescriptionFields);
    }
}
