<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_ip;
use App\Repositories\oc_customer_ipRepository;

trait Makeoc_customer_ipTrait
{
    /**
     * Create fake instance of oc_customer_ip and save it in database
     *
     * @param array $ocCustomerIpFields
     * @return oc_customer_ip
     */
    public function makeoc_customer_ip($ocCustomerIpFields = [])
    {
        /** @var oc_customer_ipRepository $ocCustomerIpRepo */
        $ocCustomerIpRepo = App::make(oc_customer_ipRepository::class);
        $theme = $this->fakeoc_customer_ipData($ocCustomerIpFields);
        return $ocCustomerIpRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_ip
     *
     * @param array $ocCustomerIpFields
     * @return oc_customer_ip
     */
    public function fakeoc_customer_ip($ocCustomerIpFields = [])
    {
        return new oc_customer_ip($this->fakeoc_customer_ipData($ocCustomerIpFields));
    }

    /**
     * Get fake data of oc_customer_ip
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_ipData($ocCustomerIpFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'ip' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerIpFields);
    }
}
