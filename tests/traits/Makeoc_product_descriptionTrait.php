<?php

use Faker\Factory as Faker;
use App\Models\oc_product_description;
use App\Repositories\oc_product_descriptionRepository;

trait Makeoc_product_descriptionTrait
{
    /**
     * Create fake instance of oc_product_description and save it in database
     *
     * @param array $ocProductDescriptionFields
     * @return oc_product_description
     */
    public function makeoc_product_description($ocProductDescriptionFields = [])
    {
        /** @var oc_product_descriptionRepository $ocProductDescriptionRepo */
        $ocProductDescriptionRepo = App::make(oc_product_descriptionRepository::class);
        $theme = $this->fakeoc_product_descriptionData($ocProductDescriptionFields);
        return $ocProductDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_description
     *
     * @param array $ocProductDescriptionFields
     * @return oc_product_description
     */
    public function fakeoc_product_description($ocProductDescriptionFields = [])
    {
        return new oc_product_description($this->fakeoc_product_descriptionData($ocProductDescriptionFields));
    }

    /**
     * Get fake data of oc_product_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_descriptionData($ocProductDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->text,
            'tag' => $fake->text,
            'meta_title' => $fake->word,
            'meta_description' => $fake->word,
            'meta_keyword' => $fake->word
        ], $ocProductDescriptionFields);
    }
}
