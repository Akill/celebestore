<?php

use Faker\Factory as Faker;
use App\Models\oc_country;
use App\Repositories\oc_countryRepository;

trait Makeoc_countryTrait
{
    /**
     * Create fake instance of oc_country and save it in database
     *
     * @param array $ocCountryFields
     * @return oc_country
     */
    public function makeoc_country($ocCountryFields = [])
    {
        /** @var oc_countryRepository $ocCountryRepo */
        $ocCountryRepo = App::make(oc_countryRepository::class);
        $theme = $this->fakeoc_countryData($ocCountryFields);
        return $ocCountryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_country
     *
     * @param array $ocCountryFields
     * @return oc_country
     */
    public function fakeoc_country($ocCountryFields = [])
    {
        return new oc_country($this->fakeoc_countryData($ocCountryFields));
    }

    /**
     * Get fake data of oc_country
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_countryData($ocCountryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'status' => $fake->word,
            'address_format' => $fake->word,
            'iso_code_2' => $fake->word,
            'iso_code_3' => $fake->word,
            'postcode_required' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCountryFields);
    }
}
