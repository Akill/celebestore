<?php

use Faker\Factory as Faker;
use App\Models\oc_order_total;
use App\Repositories\oc_order_totalRepository;

trait Makeoc_order_totalTrait
{
    /**
     * Create fake instance of oc_order_total and save it in database
     *
     * @param array $ocOrderTotalFields
     * @return oc_order_total
     */
    public function makeoc_order_total($ocOrderTotalFields = [])
    {
        /** @var oc_order_totalRepository $ocOrderTotalRepo */
        $ocOrderTotalRepo = App::make(oc_order_totalRepository::class);
        $theme = $this->fakeoc_order_totalData($ocOrderTotalFields);
        return $ocOrderTotalRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_total
     *
     * @param array $ocOrderTotalFields
     * @return oc_order_total
     */
    public function fakeoc_order_total($ocOrderTotalFields = [])
    {
        return new oc_order_total($this->fakeoc_order_totalData($ocOrderTotalFields));
    }

    /**
     * Get fake data of oc_order_total
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_totalData($ocOrderTotalFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'title' => $fake->word,
            'value' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocOrderTotalFields);
    }
}
