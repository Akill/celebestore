<?php

use Faker\Factory as Faker;
use App\Models\oc_user_group;
use App\Repositories\oc_user_groupRepository;

trait Makeoc_user_groupTrait
{
    /**
     * Create fake instance of oc_user_group and save it in database
     *
     * @param array $ocUserGroupFields
     * @return oc_user_group
     */
    public function makeoc_user_group($ocUserGroupFields = [])
    {
        /** @var oc_user_groupRepository $ocUserGroupRepo */
        $ocUserGroupRepo = App::make(oc_user_groupRepository::class);
        $theme = $this->fakeoc_user_groupData($ocUserGroupFields);
        return $ocUserGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_user_group
     *
     * @param array $ocUserGroupFields
     * @return oc_user_group
     */
    public function fakeoc_user_group($ocUserGroupFields = [])
    {
        return new oc_user_group($this->fakeoc_user_groupData($ocUserGroupFields));
    }

    /**
     * Get fake data of oc_user_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_user_groupData($ocUserGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'permission' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocUserGroupFields);
    }
}
