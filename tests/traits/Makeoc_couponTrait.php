<?php

use Faker\Factory as Faker;
use App\Models\oc_coupon;
use App\Repositories\oc_couponRepository;

trait Makeoc_couponTrait
{
    /**
     * Create fake instance of oc_coupon and save it in database
     *
     * @param array $ocCouponFields
     * @return oc_coupon
     */
    public function makeoc_coupon($ocCouponFields = [])
    {
        /** @var oc_couponRepository $ocCouponRepo */
        $ocCouponRepo = App::make(oc_couponRepository::class);
        $theme = $this->fakeoc_couponData($ocCouponFields);
        return $ocCouponRepo->create($theme);
    }

    /**
     * Get fake instance of oc_coupon
     *
     * @param array $ocCouponFields
     * @return oc_coupon
     */
    public function fakeoc_coupon($ocCouponFields = [])
    {
        return new oc_coupon($this->fakeoc_couponData($ocCouponFields));
    }

    /**
     * Get fake data of oc_coupon
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_couponData($ocCouponFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'type' => $fake->word,
            'discount' => $fake->word,
            'logged' => $fake->word,
            'shipping' => $fake->word,
            'total' => $fake->word,
            'date_start' => $fake->word,
            'date_end' => $fake->word,
            'uses_total' => $fake->randomDigitNotNull,
            'uses_customer' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocCouponFields);
    }
}
