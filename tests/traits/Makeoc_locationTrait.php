<?php

use Faker\Factory as Faker;
use App\Models\oc_location;
use App\Repositories\oc_locationRepository;

trait Makeoc_locationTrait
{
    /**
     * Create fake instance of oc_location and save it in database
     *
     * @param array $ocLocationFields
     * @return oc_location
     */
    public function makeoc_location($ocLocationFields = [])
    {
        /** @var oc_locationRepository $ocLocationRepo */
        $ocLocationRepo = App::make(oc_locationRepository::class);
        $theme = $this->fakeoc_locationData($ocLocationFields);
        return $ocLocationRepo->create($theme);
    }

    /**
     * Get fake instance of oc_location
     *
     * @param array $ocLocationFields
     * @return oc_location
     */
    public function fakeoc_location($ocLocationFields = [])
    {
        return new oc_location($this->fakeoc_locationData($ocLocationFields));
    }

    /**
     * Get fake data of oc_location
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_locationData($ocLocationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'address' => $fake->text,
            'telephone' => $fake->word,
            'fax' => $fake->word,
            'geocode' => $fake->word,
            'image' => $fake->word,
            'open' => $fake->text,
            'comment' => $fake->text
        ], $ocLocationFields);
    }
}
