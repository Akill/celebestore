<?php

use Faker\Factory as Faker;
use App\Models\oc_option_value;
use App\Repositories\oc_option_valueRepository;

trait Makeoc_option_valueTrait
{
    /**
     * Create fake instance of oc_option_value and save it in database
     *
     * @param array $ocOptionValueFields
     * @return oc_option_value
     */
    public function makeoc_option_value($ocOptionValueFields = [])
    {
        /** @var oc_option_valueRepository $ocOptionValueRepo */
        $ocOptionValueRepo = App::make(oc_option_valueRepository::class);
        $theme = $this->fakeoc_option_valueData($ocOptionValueFields);
        return $ocOptionValueRepo->create($theme);
    }

    /**
     * Get fake instance of oc_option_value
     *
     * @param array $ocOptionValueFields
     * @return oc_option_value
     */
    public function fakeoc_option_value($ocOptionValueFields = [])
    {
        return new oc_option_value($this->fakeoc_option_valueData($ocOptionValueFields));
    }

    /**
     * Get fake data of oc_option_value
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_option_valueData($ocOptionValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'option_id' => $fake->randomDigitNotNull,
            'image' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocOptionValueFields);
    }
}
