<?php

use Faker\Factory as Faker;
use App\Models\oc_category_to_layout;
use App\Repositories\oc_category_to_layoutRepository;

trait Makeoc_category_to_layoutTrait
{
    /**
     * Create fake instance of oc_category_to_layout and save it in database
     *
     * @param array $ocCategoryToLayoutFields
     * @return oc_category_to_layout
     */
    public function makeoc_category_to_layout($ocCategoryToLayoutFields = [])
    {
        /** @var oc_category_to_layoutRepository $ocCategoryToLayoutRepo */
        $ocCategoryToLayoutRepo = App::make(oc_category_to_layoutRepository::class);
        $theme = $this->fakeoc_category_to_layoutData($ocCategoryToLayoutFields);
        return $ocCategoryToLayoutRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category_to_layout
     *
     * @param array $ocCategoryToLayoutFields
     * @return oc_category_to_layout
     */
    public function fakeoc_category_to_layout($ocCategoryToLayoutFields = [])
    {
        return new oc_category_to_layout($this->fakeoc_category_to_layoutData($ocCategoryToLayoutFields));
    }

    /**
     * Get fake data of oc_category_to_layout
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_category_to_layoutData($ocCategoryToLayoutFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'layout_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryToLayoutFields);
    }
}
