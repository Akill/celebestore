<?php

use Faker\Factory as Faker;
use App\Models\oc_weight_class;
use App\Repositories\oc_weight_classRepository;

trait Makeoc_weight_classTrait
{
    /**
     * Create fake instance of oc_weight_class and save it in database
     *
     * @param array $ocWeightClassFields
     * @return oc_weight_class
     */
    public function makeoc_weight_class($ocWeightClassFields = [])
    {
        /** @var oc_weight_classRepository $ocWeightClassRepo */
        $ocWeightClassRepo = App::make(oc_weight_classRepository::class);
        $theme = $this->fakeoc_weight_classData($ocWeightClassFields);
        return $ocWeightClassRepo->create($theme);
    }

    /**
     * Get fake instance of oc_weight_class
     *
     * @param array $ocWeightClassFields
     * @return oc_weight_class
     */
    public function fakeoc_weight_class($ocWeightClassFields = [])
    {
        return new oc_weight_class($this->fakeoc_weight_classData($ocWeightClassFields));
    }

    /**
     * Get fake data of oc_weight_class
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_weight_classData($ocWeightClassFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word
        ], $ocWeightClassFields);
    }
}
