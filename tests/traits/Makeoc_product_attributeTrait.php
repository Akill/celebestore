<?php

use Faker\Factory as Faker;
use App\Models\oc_product_attribute;
use App\Repositories\oc_product_attributeRepository;

trait Makeoc_product_attributeTrait
{
    /**
     * Create fake instance of oc_product_attribute and save it in database
     *
     * @param array $ocProductAttributeFields
     * @return oc_product_attribute
     */
    public function makeoc_product_attribute($ocProductAttributeFields = [])
    {
        /** @var oc_product_attributeRepository $ocProductAttributeRepo */
        $ocProductAttributeRepo = App::make(oc_product_attributeRepository::class);
        $theme = $this->fakeoc_product_attributeData($ocProductAttributeFields);
        return $ocProductAttributeRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_attribute
     *
     * @param array $ocProductAttributeFields
     * @return oc_product_attribute
     */
    public function fakeoc_product_attribute($ocProductAttributeFields = [])
    {
        return new oc_product_attribute($this->fakeoc_product_attributeData($ocProductAttributeFields));
    }

    /**
     * Get fake data of oc_product_attribute
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_attributeData($ocProductAttributeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'attribute_id' => $fake->randomDigitNotNull,
            'language_id' => $fake->randomDigitNotNull,
            'text' => $fake->text
        ], $ocProductAttributeFields);
    }
}
