<?php

use Faker\Factory as Faker;
use App\Models\oc_manufacturer;
use App\Repositories\oc_manufacturerRepository;

trait Makeoc_manufacturerTrait
{
    /**
     * Create fake instance of oc_manufacturer and save it in database
     *
     * @param array $ocManufacturerFields
     * @return oc_manufacturer
     */
    public function makeoc_manufacturer($ocManufacturerFields = [])
    {
        /** @var oc_manufacturerRepository $ocManufacturerRepo */
        $ocManufacturerRepo = App::make(oc_manufacturerRepository::class);
        $theme = $this->fakeoc_manufacturerData($ocManufacturerFields);
        return $ocManufacturerRepo->create($theme);
    }

    /**
     * Get fake instance of oc_manufacturer
     *
     * @param array $ocManufacturerFields
     * @return oc_manufacturer
     */
    public function fakeoc_manufacturer($ocManufacturerFields = [])
    {
        return new oc_manufacturer($this->fakeoc_manufacturerData($ocManufacturerFields));
    }

    /**
     * Get fake data of oc_manufacturer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_manufacturerData($ocManufacturerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'image' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocManufacturerFields);
    }
}
