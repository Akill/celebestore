<?php

use Faker\Factory as Faker;
use App\Models\oc_information;
use App\Repositories\oc_informationRepository;

trait Makeoc_informationTrait
{
    /**
     * Create fake instance of oc_information and save it in database
     *
     * @param array $ocInformationFields
     * @return oc_information
     */
    public function makeoc_information($ocInformationFields = [])
    {
        /** @var oc_informationRepository $ocInformationRepo */
        $ocInformationRepo = App::make(oc_informationRepository::class);
        $theme = $this->fakeoc_informationData($ocInformationFields);
        return $ocInformationRepo->create($theme);
    }

    /**
     * Get fake instance of oc_information
     *
     * @param array $ocInformationFields
     * @return oc_information
     */
    public function fakeoc_information($ocInformationFields = [])
    {
        return new oc_information($this->fakeoc_informationData($ocInformationFields));
    }

    /**
     * Get fake data of oc_information
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_informationData($ocInformationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bottom' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'status' => $fake->word
        ], $ocInformationFields);
    }
}
