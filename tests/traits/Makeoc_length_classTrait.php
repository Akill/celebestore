<?php

use Faker\Factory as Faker;
use App\Models\oc_length_class;
use App\Repositories\oc_length_classRepository;

trait Makeoc_length_classTrait
{
    /**
     * Create fake instance of oc_length_class and save it in database
     *
     * @param array $ocLengthClassFields
     * @return oc_length_class
     */
    public function makeoc_length_class($ocLengthClassFields = [])
    {
        /** @var oc_length_classRepository $ocLengthClassRepo */
        $ocLengthClassRepo = App::make(oc_length_classRepository::class);
        $theme = $this->fakeoc_length_classData($ocLengthClassFields);
        return $ocLengthClassRepo->create($theme);
    }

    /**
     * Get fake instance of oc_length_class
     *
     * @param array $ocLengthClassFields
     * @return oc_length_class
     */
    public function fakeoc_length_class($ocLengthClassFields = [])
    {
        return new oc_length_class($this->fakeoc_length_classData($ocLengthClassFields));
    }

    /**
     * Get fake data of oc_length_class
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_length_classData($ocLengthClassFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word
        ], $ocLengthClassFields);
    }
}
