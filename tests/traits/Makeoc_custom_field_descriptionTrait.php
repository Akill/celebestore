<?php

use Faker\Factory as Faker;
use App\Models\oc_custom_field_description;
use App\Repositories\oc_custom_field_descriptionRepository;

trait Makeoc_custom_field_descriptionTrait
{
    /**
     * Create fake instance of oc_custom_field_description and save it in database
     *
     * @param array $ocCustomFieldDescriptionFields
     * @return oc_custom_field_description
     */
    public function makeoc_custom_field_description($ocCustomFieldDescriptionFields = [])
    {
        /** @var oc_custom_field_descriptionRepository $ocCustomFieldDescriptionRepo */
        $ocCustomFieldDescriptionRepo = App::make(oc_custom_field_descriptionRepository::class);
        $theme = $this->fakeoc_custom_field_descriptionData($ocCustomFieldDescriptionFields);
        return $ocCustomFieldDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_custom_field_description
     *
     * @param array $ocCustomFieldDescriptionFields
     * @return oc_custom_field_description
     */
    public function fakeoc_custom_field_description($ocCustomFieldDescriptionFields = [])
    {
        return new oc_custom_field_description($this->fakeoc_custom_field_descriptionData($ocCustomFieldDescriptionFields));
    }

    /**
     * Get fake data of oc_custom_field_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_custom_field_descriptionData($ocCustomFieldDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomFieldDescriptionFields);
    }
}
