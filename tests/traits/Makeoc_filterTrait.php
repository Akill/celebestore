<?php

use Faker\Factory as Faker;
use App\Models\oc_filter;
use App\Repositories\oc_filterRepository;

trait Makeoc_filterTrait
{
    /**
     * Create fake instance of oc_filter and save it in database
     *
     * @param array $ocFilterFields
     * @return oc_filter
     */
    public function makeoc_filter($ocFilterFields = [])
    {
        /** @var oc_filterRepository $ocFilterRepo */
        $ocFilterRepo = App::make(oc_filterRepository::class);
        $theme = $this->fakeoc_filterData($ocFilterFields);
        return $ocFilterRepo->create($theme);
    }

    /**
     * Get fake instance of oc_filter
     *
     * @param array $ocFilterFields
     * @return oc_filter
     */
    public function fakeoc_filter($ocFilterFields = [])
    {
        return new oc_filter($this->fakeoc_filterData($ocFilterFields));
    }

    /**
     * Get fake data of oc_filter
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_filterData($ocFilterFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'filter_group_id' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocFilterFields);
    }
}
