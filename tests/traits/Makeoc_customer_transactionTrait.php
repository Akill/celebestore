<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_transaction;
use App\Repositories\oc_customer_transactionRepository;

trait Makeoc_customer_transactionTrait
{
    /**
     * Create fake instance of oc_customer_transaction and save it in database
     *
     * @param array $ocCustomerTransactionFields
     * @return oc_customer_transaction
     */
    public function makeoc_customer_transaction($ocCustomerTransactionFields = [])
    {
        /** @var oc_customer_transactionRepository $ocCustomerTransactionRepo */
        $ocCustomerTransactionRepo = App::make(oc_customer_transactionRepository::class);
        $theme = $this->fakeoc_customer_transactionData($ocCustomerTransactionFields);
        return $ocCustomerTransactionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_transaction
     *
     * @param array $ocCustomerTransactionFields
     * @return oc_customer_transaction
     */
    public function fakeoc_customer_transaction($ocCustomerTransactionFields = [])
    {
        return new oc_customer_transaction($this->fakeoc_customer_transactionData($ocCustomerTransactionFields));
    }

    /**
     * Get fake data of oc_customer_transaction
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_transactionData($ocCustomerTransactionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'order_id' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'amount' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerTransactionFields);
    }
}
