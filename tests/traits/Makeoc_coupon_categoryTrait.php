<?php

use Faker\Factory as Faker;
use App\Models\oc_coupon_category;
use App\Repositories\oc_coupon_categoryRepository;

trait Makeoc_coupon_categoryTrait
{
    /**
     * Create fake instance of oc_coupon_category and save it in database
     *
     * @param array $ocCouponCategoryFields
     * @return oc_coupon_category
     */
    public function makeoc_coupon_category($ocCouponCategoryFields = [])
    {
        /** @var oc_coupon_categoryRepository $ocCouponCategoryRepo */
        $ocCouponCategoryRepo = App::make(oc_coupon_categoryRepository::class);
        $theme = $this->fakeoc_coupon_categoryData($ocCouponCategoryFields);
        return $ocCouponCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_coupon_category
     *
     * @param array $ocCouponCategoryFields
     * @return oc_coupon_category
     */
    public function fakeoc_coupon_category($ocCouponCategoryFields = [])
    {
        return new oc_coupon_category($this->fakeoc_coupon_categoryData($ocCouponCategoryFields));
    }

    /**
     * Get fake data of oc_coupon_category
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_coupon_categoryData($ocCouponCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'category_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCouponCategoryFields);
    }
}
