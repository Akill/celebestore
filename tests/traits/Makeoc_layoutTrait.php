<?php

use Faker\Factory as Faker;
use App\Models\oc_layout;
use App\Repositories\oc_layoutRepository;

trait Makeoc_layoutTrait
{
    /**
     * Create fake instance of oc_layout and save it in database
     *
     * @param array $ocLayoutFields
     * @return oc_layout
     */
    public function makeoc_layout($ocLayoutFields = [])
    {
        /** @var oc_layoutRepository $ocLayoutRepo */
        $ocLayoutRepo = App::make(oc_layoutRepository::class);
        $theme = $this->fakeoc_layoutData($ocLayoutFields);
        return $ocLayoutRepo->create($theme);
    }

    /**
     * Get fake instance of oc_layout
     *
     * @param array $ocLayoutFields
     * @return oc_layout
     */
    public function fakeoc_layout($ocLayoutFields = [])
    {
        return new oc_layout($this->fakeoc_layoutData($ocLayoutFields));
    }

    /**
     * Get fake data of oc_layout
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_layoutData($ocLayoutFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word
        ], $ocLayoutFields);
    }
}
