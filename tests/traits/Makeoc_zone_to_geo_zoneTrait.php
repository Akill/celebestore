<?php

use Faker\Factory as Faker;
use App\Models\oc_zone_to_geo_zone;
use App\Repositories\oc_zone_to_geo_zoneRepository;

trait Makeoc_zone_to_geo_zoneTrait
{
    /**
     * Create fake instance of oc_zone_to_geo_zone and save it in database
     *
     * @param array $ocZoneToGeoZoneFields
     * @return oc_zone_to_geo_zone
     */
    public function makeoc_zone_to_geo_zone($ocZoneToGeoZoneFields = [])
    {
        /** @var oc_zone_to_geo_zoneRepository $ocZoneToGeoZoneRepo */
        $ocZoneToGeoZoneRepo = App::make(oc_zone_to_geo_zoneRepository::class);
        $theme = $this->fakeoc_zone_to_geo_zoneData($ocZoneToGeoZoneFields);
        return $ocZoneToGeoZoneRepo->create($theme);
    }

    /**
     * Get fake instance of oc_zone_to_geo_zone
     *
     * @param array $ocZoneToGeoZoneFields
     * @return oc_zone_to_geo_zone
     */
    public function fakeoc_zone_to_geo_zone($ocZoneToGeoZoneFields = [])
    {
        return new oc_zone_to_geo_zone($this->fakeoc_zone_to_geo_zoneData($ocZoneToGeoZoneFields));
    }

    /**
     * Get fake data of oc_zone_to_geo_zone
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_zone_to_geo_zoneData($ocZoneToGeoZoneFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'country_id' => $fake->randomDigitNotNull,
            'zone_id' => $fake->randomDigitNotNull,
            'geo_zone_id' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocZoneToGeoZoneFields);
    }
}
