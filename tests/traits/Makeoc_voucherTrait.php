<?php

use Faker\Factory as Faker;
use App\Models\oc_voucher;
use App\Repositories\oc_voucherRepository;

trait Makeoc_voucherTrait
{
    /**
     * Create fake instance of oc_voucher and save it in database
     *
     * @param array $ocVoucherFields
     * @return oc_voucher
     */
    public function makeoc_voucher($ocVoucherFields = [])
    {
        /** @var oc_voucherRepository $ocVoucherRepo */
        $ocVoucherRepo = App::make(oc_voucherRepository::class);
        $theme = $this->fakeoc_voucherData($ocVoucherFields);
        return $ocVoucherRepo->create($theme);
    }

    /**
     * Get fake instance of oc_voucher
     *
     * @param array $ocVoucherFields
     * @return oc_voucher
     */
    public function fakeoc_voucher($ocVoucherFields = [])
    {
        return new oc_voucher($this->fakeoc_voucherData($ocVoucherFields));
    }

    /**
     * Get fake data of oc_voucher
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_voucherData($ocVoucherFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'from_name' => $fake->word,
            'from_email' => $fake->word,
            'to_name' => $fake->word,
            'to_email' => $fake->word,
            'voucher_theme_id' => $fake->randomDigitNotNull,
            'message' => $fake->text,
            'amount' => $fake->word,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocVoucherFields);
    }
}
