<?php

use Faker\Factory as Faker;
use App\Models\oc_length_class_description;
use App\Repositories\oc_length_class_descriptionRepository;

trait Makeoc_length_class_descriptionTrait
{
    /**
     * Create fake instance of oc_length_class_description and save it in database
     *
     * @param array $ocLengthClassDescriptionFields
     * @return oc_length_class_description
     */
    public function makeoc_length_class_description($ocLengthClassDescriptionFields = [])
    {
        /** @var oc_length_class_descriptionRepository $ocLengthClassDescriptionRepo */
        $ocLengthClassDescriptionRepo = App::make(oc_length_class_descriptionRepository::class);
        $theme = $this->fakeoc_length_class_descriptionData($ocLengthClassDescriptionFields);
        return $ocLengthClassDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_length_class_description
     *
     * @param array $ocLengthClassDescriptionFields
     * @return oc_length_class_description
     */
    public function fakeoc_length_class_description($ocLengthClassDescriptionFields = [])
    {
        return new oc_length_class_description($this->fakeoc_length_class_descriptionData($ocLengthClassDescriptionFields));
    }

    /**
     * Get fake data of oc_length_class_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_length_class_descriptionData($ocLengthClassDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'unit' => $fake->word
        ], $ocLengthClassDescriptionFields);
    }
}
