<?php

use Faker\Factory as Faker;
use App\Models\oc_custom_field_value;
use App\Repositories\oc_custom_field_valueRepository;

trait Makeoc_custom_field_valueTrait
{
    /**
     * Create fake instance of oc_custom_field_value and save it in database
     *
     * @param array $ocCustomFieldValueFields
     * @return oc_custom_field_value
     */
    public function makeoc_custom_field_value($ocCustomFieldValueFields = [])
    {
        /** @var oc_custom_field_valueRepository $ocCustomFieldValueRepo */
        $ocCustomFieldValueRepo = App::make(oc_custom_field_valueRepository::class);
        $theme = $this->fakeoc_custom_field_valueData($ocCustomFieldValueFields);
        return $ocCustomFieldValueRepo->create($theme);
    }

    /**
     * Get fake instance of oc_custom_field_value
     *
     * @param array $ocCustomFieldValueFields
     * @return oc_custom_field_value
     */
    public function fakeoc_custom_field_value($ocCustomFieldValueFields = [])
    {
        return new oc_custom_field_value($this->fakeoc_custom_field_valueData($ocCustomFieldValueFields));
    }

    /**
     * Get fake data of oc_custom_field_value
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_custom_field_valueData($ocCustomFieldValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'custom_field_id' => $fake->randomDigitNotNull,
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomFieldValueFields);
    }
}
