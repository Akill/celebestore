<?php

use Faker\Factory as Faker;
use App\Models\oc_option_value_description;
use App\Repositories\oc_option_value_descriptionRepository;

trait Makeoc_option_value_descriptionTrait
{
    /**
     * Create fake instance of oc_option_value_description and save it in database
     *
     * @param array $ocOptionValueDescriptionFields
     * @return oc_option_value_description
     */
    public function makeoc_option_value_description($ocOptionValueDescriptionFields = [])
    {
        /** @var oc_option_value_descriptionRepository $ocOptionValueDescriptionRepo */
        $ocOptionValueDescriptionRepo = App::make(oc_option_value_descriptionRepository::class);
        $theme = $this->fakeoc_option_value_descriptionData($ocOptionValueDescriptionFields);
        return $ocOptionValueDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_option_value_description
     *
     * @param array $ocOptionValueDescriptionFields
     * @return oc_option_value_description
     */
    public function fakeoc_option_value_description($ocOptionValueDescriptionFields = [])
    {
        return new oc_option_value_description($this->fakeoc_option_value_descriptionData($ocOptionValueDescriptionFields));
    }

    /**
     * Get fake data of oc_option_value_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_option_value_descriptionData($ocOptionValueDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'option_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocOptionValueDescriptionFields);
    }
}
