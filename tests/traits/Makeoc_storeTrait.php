<?php

use Faker\Factory as Faker;
use App\Models\oc_store;
use App\Repositories\oc_storeRepository;

trait Makeoc_storeTrait
{
    /**
     * Create fake instance of oc_store and save it in database
     *
     * @param array $ocStoreFields
     * @return oc_store
     */
    public function makeoc_store($ocStoreFields = [])
    {
        /** @var oc_storeRepository $ocStoreRepo */
        $ocStoreRepo = App::make(oc_storeRepository::class);
        $theme = $this->fakeoc_storeData($ocStoreFields);
        return $ocStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_store
     *
     * @param array $ocStoreFields
     * @return oc_store
     */
    public function fakeoc_store($ocStoreFields = [])
    {
        return new oc_store($this->fakeoc_storeData($ocStoreFields));
    }

    /**
     * Get fake data of oc_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_storeData($ocStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'url' => $fake->word,
            'ssl' => $fake->word
        ], $ocStoreFields);
    }
}
