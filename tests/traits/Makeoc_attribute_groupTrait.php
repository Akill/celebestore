<?php

use Faker\Factory as Faker;
use App\Models\oc_attribute_group;
use App\Repositories\oc_attribute_groupRepository;

trait Makeoc_attribute_groupTrait
{
    /**
     * Create fake instance of oc_attribute_group and save it in database
     *
     * @param array $ocAttributeGroupFields
     * @return oc_attribute_group
     */
    public function makeoc_attribute_group($ocAttributeGroupFields = [])
    {
        /** @var oc_attribute_groupRepository $ocAttributeGroupRepo */
        $ocAttributeGroupRepo = App::make(oc_attribute_groupRepository::class);
        $theme = $this->fakeoc_attribute_groupData($ocAttributeGroupFields);
        return $ocAttributeGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_attribute_group
     *
     * @param array $ocAttributeGroupFields
     * @return oc_attribute_group
     */
    public function fakeoc_attribute_group($ocAttributeGroupFields = [])
    {
        return new oc_attribute_group($this->fakeoc_attribute_groupData($ocAttributeGroupFields));
    }

    /**
     * Get fake data of oc_attribute_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_attribute_groupData($ocAttributeGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'sort_order' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAttributeGroupFields);
    }
}
