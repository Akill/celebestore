<?php

use Faker\Factory as Faker;
use App\Models\oc_custom_field_value_description;
use App\Repositories\oc_custom_field_value_descriptionRepository;

trait Makeoc_custom_field_value_descriptionTrait
{
    /**
     * Create fake instance of oc_custom_field_value_description and save it in database
     *
     * @param array $ocCustomFieldValueDescriptionFields
     * @return oc_custom_field_value_description
     */
    public function makeoc_custom_field_value_description($ocCustomFieldValueDescriptionFields = [])
    {
        /** @var oc_custom_field_value_descriptionRepository $ocCustomFieldValueDescriptionRepo */
        $ocCustomFieldValueDescriptionRepo = App::make(oc_custom_field_value_descriptionRepository::class);
        $theme = $this->fakeoc_custom_field_value_descriptionData($ocCustomFieldValueDescriptionFields);
        return $ocCustomFieldValueDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_custom_field_value_description
     *
     * @param array $ocCustomFieldValueDescriptionFields
     * @return oc_custom_field_value_description
     */
    public function fakeoc_custom_field_value_description($ocCustomFieldValueDescriptionFields = [])
    {
        return new oc_custom_field_value_description($this->fakeoc_custom_field_value_descriptionData($ocCustomFieldValueDescriptionFields));
    }

    /**
     * Get fake data of oc_custom_field_value_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_custom_field_value_descriptionData($ocCustomFieldValueDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'custom_field_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomFieldValueDescriptionFields);
    }
}
