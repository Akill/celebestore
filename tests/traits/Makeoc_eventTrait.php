<?php

use Faker\Factory as Faker;
use App\Models\oc_event;
use App\Repositories\oc_eventRepository;

trait Makeoc_eventTrait
{
    /**
     * Create fake instance of oc_event and save it in database
     *
     * @param array $ocEventFields
     * @return oc_event
     */
    public function makeoc_event($ocEventFields = [])
    {
        /** @var oc_eventRepository $ocEventRepo */
        $ocEventRepo = App::make(oc_eventRepository::class);
        $theme = $this->fakeoc_eventData($ocEventFields);
        return $ocEventRepo->create($theme);
    }

    /**
     * Get fake instance of oc_event
     *
     * @param array $ocEventFields
     * @return oc_event
     */
    public function fakeoc_event($ocEventFields = [])
    {
        return new oc_event($this->fakeoc_eventData($ocEventFields));
    }

    /**
     * Get fake data of oc_event
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_eventData($ocEventFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'code' => $fake->word,
            'trigger' => $fake->text,
            'action' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocEventFields);
    }
}
