<?php

use Faker\Factory as Faker;
use App\Models\oc_upload;
use App\Repositories\oc_uploadRepository;

trait Makeoc_uploadTrait
{
    /**
     * Create fake instance of oc_upload and save it in database
     *
     * @param array $ocUploadFields
     * @return oc_upload
     */
    public function makeoc_upload($ocUploadFields = [])
    {
        /** @var oc_uploadRepository $ocUploadRepo */
        $ocUploadRepo = App::make(oc_uploadRepository::class);
        $theme = $this->fakeoc_uploadData($ocUploadFields);
        return $ocUploadRepo->create($theme);
    }

    /**
     * Get fake instance of oc_upload
     *
     * @param array $ocUploadFields
     * @return oc_upload
     */
    public function fakeoc_upload($ocUploadFields = [])
    {
        return new oc_upload($this->fakeoc_uploadData($ocUploadFields));
    }

    /**
     * Get fake data of oc_upload
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_uploadData($ocUploadFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'filename' => $fake->word,
            'code' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocUploadFields);
    }
}
