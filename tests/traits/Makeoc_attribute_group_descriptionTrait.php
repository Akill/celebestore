<?php

use Faker\Factory as Faker;
use App\Models\oc_attribute_group_description;
use App\Repositories\oc_attribute_group_descriptionRepository;

trait Makeoc_attribute_group_descriptionTrait
{
    /**
     * Create fake instance of oc_attribute_group_description and save it in database
     *
     * @param array $ocAttributeGroupDescriptionFields
     * @return oc_attribute_group_description
     */
    public function makeoc_attribute_group_description($ocAttributeGroupDescriptionFields = [])
    {
        /** @var oc_attribute_group_descriptionRepository $ocAttributeGroupDescriptionRepo */
        $ocAttributeGroupDescriptionRepo = App::make(oc_attribute_group_descriptionRepository::class);
        $theme = $this->fakeoc_attribute_group_descriptionData($ocAttributeGroupDescriptionFields);
        return $ocAttributeGroupDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_attribute_group_description
     *
     * @param array $ocAttributeGroupDescriptionFields
     * @return oc_attribute_group_description
     */
    public function fakeoc_attribute_group_description($ocAttributeGroupDescriptionFields = [])
    {
        return new oc_attribute_group_description($this->fakeoc_attribute_group_descriptionData($ocAttributeGroupDescriptionFields));
    }

    /**
     * Get fake data of oc_attribute_group_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_attribute_group_descriptionData($ocAttributeGroupDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAttributeGroupDescriptionFields);
    }
}
