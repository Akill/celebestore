<?php

use Faker\Factory as Faker;
use App\Models\oc_option;
use App\Repositories\oc_optionRepository;

trait Makeoc_optionTrait
{
    /**
     * Create fake instance of oc_option and save it in database
     *
     * @param array $ocOptionFields
     * @return oc_option
     */
    public function makeoc_option($ocOptionFields = [])
    {
        /** @var oc_optionRepository $ocOptionRepo */
        $ocOptionRepo = App::make(oc_optionRepository::class);
        $theme = $this->fakeoc_optionData($ocOptionFields);
        return $ocOptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_option
     *
     * @param array $ocOptionFields
     * @return oc_option
     */
    public function fakeoc_option($ocOptionFields = [])
    {
        return new oc_option($this->fakeoc_optionData($ocOptionFields));
    }

    /**
     * Get fake data of oc_option
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_optionData($ocOptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocOptionFields);
    }
}
