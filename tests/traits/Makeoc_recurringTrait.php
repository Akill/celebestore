<?php

use Faker\Factory as Faker;
use App\Models\oc_recurring;
use App\Repositories\oc_recurringRepository;

trait Makeoc_recurringTrait
{
    /**
     * Create fake instance of oc_recurring and save it in database
     *
     * @param array $ocRecurringFields
     * @return oc_recurring
     */
    public function makeoc_recurring($ocRecurringFields = [])
    {
        /** @var oc_recurringRepository $ocRecurringRepo */
        $ocRecurringRepo = App::make(oc_recurringRepository::class);
        $theme = $this->fakeoc_recurringData($ocRecurringFields);
        return $ocRecurringRepo->create($theme);
    }

    /**
     * Get fake instance of oc_recurring
     *
     * @param array $ocRecurringFields
     * @return oc_recurring
     */
    public function fakeoc_recurring($ocRecurringFields = [])
    {
        return new oc_recurring($this->fakeoc_recurringData($ocRecurringFields));
    }

    /**
     * Get fake data of oc_recurring
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_recurringData($ocRecurringFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'price' => $fake->word,
            'frequency' => $fake->word,
            'duration' => $fake->randomDigitNotNull,
            'cycle' => $fake->randomDigitNotNull,
            'trial_status' => $fake->word,
            'trial_price' => $fake->word,
            'trial_frequency' => $fake->word,
            'trial_duration' => $fake->randomDigitNotNull,
            'trial_cycle' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocRecurringFields);
    }
}
