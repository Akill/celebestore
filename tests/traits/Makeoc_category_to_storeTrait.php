<?php

use Faker\Factory as Faker;
use App\Models\oc_category_to_store;
use App\Repositories\oc_category_to_storeRepository;

trait Makeoc_category_to_storeTrait
{
    /**
     * Create fake instance of oc_category_to_store and save it in database
     *
     * @param array $ocCategoryToStoreFields
     * @return oc_category_to_store
     */
    public function makeoc_category_to_store($ocCategoryToStoreFields = [])
    {
        /** @var oc_category_to_storeRepository $ocCategoryToStoreRepo */
        $ocCategoryToStoreRepo = App::make(oc_category_to_storeRepository::class);
        $theme = $this->fakeoc_category_to_storeData($ocCategoryToStoreFields);
        return $ocCategoryToStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category_to_store
     *
     * @param array $ocCategoryToStoreFields
     * @return oc_category_to_store
     */
    public function fakeoc_category_to_store($ocCategoryToStoreFields = [])
    {
        return new oc_category_to_store($this->fakeoc_category_to_storeData($ocCategoryToStoreFields));
    }

    /**
     * Get fake data of oc_category_to_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_category_to_storeData($ocCategoryToStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryToStoreFields);
    }
}
