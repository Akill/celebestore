<?php

use Faker\Factory as Faker;
use App\Models\oc_user;
use App\Repositories\oc_userRepository;

trait Makeoc_userTrait
{
    /**
     * Create fake instance of oc_user and save it in database
     *
     * @param array $ocUserFields
     * @return oc_user
     */
    public function makeoc_user($ocUserFields = [])
    {
        /** @var oc_userRepository $ocUserRepo */
        $ocUserRepo = App::make(oc_userRepository::class);
        $theme = $this->fakeoc_userData($ocUserFields);
        return $ocUserRepo->create($theme);
    }

    /**
     * Get fake instance of oc_user
     *
     * @param array $ocUserFields
     * @return oc_user
     */
    public function fakeoc_user($ocUserFields = [])
    {
        return new oc_user($this->fakeoc_userData($ocUserFields));
    }

    /**
     * Get fake data of oc_user
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_userData($ocUserFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_group_id' => $fake->randomDigitNotNull,
            'username' => $fake->word,
            'password' => $fake->word,
            'salt' => $fake->word,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'email' => $fake->word,
            'image' => $fake->word,
            'code' => $fake->word,
            'ip' => $fake->word,
            'status' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->word
        ], $ocUserFields);
    }
}
