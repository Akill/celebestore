<?php

use Faker\Factory as Faker;
use App\Models\oc_attribute_description;
use App\Repositories\oc_attribute_descriptionRepository;

trait Makeoc_attribute_descriptionTrait
{
    /**
     * Create fake instance of oc_attribute_description and save it in database
     *
     * @param array $ocAttributeDescriptionFields
     * @return oc_attribute_description
     */
    public function makeoc_attribute_description($ocAttributeDescriptionFields = [])
    {
        /** @var oc_attribute_descriptionRepository $ocAttributeDescriptionRepo */
        $ocAttributeDescriptionRepo = App::make(oc_attribute_descriptionRepository::class);
        $theme = $this->fakeoc_attribute_descriptionData($ocAttributeDescriptionFields);
        return $ocAttributeDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_attribute_description
     *
     * @param array $ocAttributeDescriptionFields
     * @return oc_attribute_description
     */
    public function fakeoc_attribute_description($ocAttributeDescriptionFields = [])
    {
        return new oc_attribute_description($this->fakeoc_attribute_descriptionData($ocAttributeDescriptionFields));
    }

    /**
     * Get fake data of oc_attribute_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_attribute_descriptionData($ocAttributeDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAttributeDescriptionFields);
    }
}
