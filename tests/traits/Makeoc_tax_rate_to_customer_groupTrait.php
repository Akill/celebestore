<?php

use Faker\Factory as Faker;
use App\Models\oc_tax_rate_to_customer_group;
use App\Repositories\oc_tax_rate_to_customer_groupRepository;

trait Makeoc_tax_rate_to_customer_groupTrait
{
    /**
     * Create fake instance of oc_tax_rate_to_customer_group and save it in database
     *
     * @param array $ocTaxRateToCustomerGroupFields
     * @return oc_tax_rate_to_customer_group
     */
    public function makeoc_tax_rate_to_customer_group($ocTaxRateToCustomerGroupFields = [])
    {
        /** @var oc_tax_rate_to_customer_groupRepository $ocTaxRateToCustomerGroupRepo */
        $ocTaxRateToCustomerGroupRepo = App::make(oc_tax_rate_to_customer_groupRepository::class);
        $theme = $this->fakeoc_tax_rate_to_customer_groupData($ocTaxRateToCustomerGroupFields);
        return $ocTaxRateToCustomerGroupRepo->create($theme);
    }

    /**
     * Get fake instance of oc_tax_rate_to_customer_group
     *
     * @param array $ocTaxRateToCustomerGroupFields
     * @return oc_tax_rate_to_customer_group
     */
    public function fakeoc_tax_rate_to_customer_group($ocTaxRateToCustomerGroupFields = [])
    {
        return new oc_tax_rate_to_customer_group($this->fakeoc_tax_rate_to_customer_groupData($ocTaxRateToCustomerGroupFields));
    }

    /**
     * Get fake data of oc_tax_rate_to_customer_group
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_tax_rate_to_customer_groupData($ocTaxRateToCustomerGroupFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_group_id' => $fake->randomDigitNotNull
        ], $ocTaxRateToCustomerGroupFields);
    }
}
