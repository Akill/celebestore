<?php

use Faker\Factory as Faker;
use App\Models\oc_return;
use App\Repositories\oc_returnRepository;

trait Makeoc_returnTrait
{
    /**
     * Create fake instance of oc_return and save it in database
     *
     * @param array $ocReturnFields
     * @return oc_return
     */
    public function makeoc_return($ocReturnFields = [])
    {
        /** @var oc_returnRepository $ocReturnRepo */
        $ocReturnRepo = App::make(oc_returnRepository::class);
        $theme = $this->fakeoc_returnData($ocReturnFields);
        return $ocReturnRepo->create($theme);
    }

    /**
     * Get fake instance of oc_return
     *
     * @param array $ocReturnFields
     * @return oc_return
     */
    public function fakeoc_return($ocReturnFields = [])
    {
        return new oc_return($this->fakeoc_returnData($ocReturnFields));
    }

    /**
     * Get fake data of oc_return
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_returnData($ocReturnFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'customer_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'email' => $fake->word,
            'telephone' => $fake->word,
            'product' => $fake->word,
            'model' => $fake->word,
            'quantity' => $fake->randomDigitNotNull,
            'opened' => $fake->word,
            'return_reason_id' => $fake->randomDigitNotNull,
            'return_action_id' => $fake->randomDigitNotNull,
            'return_status_id' => $fake->randomDigitNotNull,
            'comment' => $fake->text,
            'date_ordered' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocReturnFields);
    }
}
