<?php

use Faker\Factory as Faker;
use App\Models\oc_information_to_layout;
use App\Repositories\oc_information_to_layoutRepository;

trait Makeoc_information_to_layoutTrait
{
    /**
     * Create fake instance of oc_information_to_layout and save it in database
     *
     * @param array $ocInformationToLayoutFields
     * @return oc_information_to_layout
     */
    public function makeoc_information_to_layout($ocInformationToLayoutFields = [])
    {
        /** @var oc_information_to_layoutRepository $ocInformationToLayoutRepo */
        $ocInformationToLayoutRepo = App::make(oc_information_to_layoutRepository::class);
        $theme = $this->fakeoc_information_to_layoutData($ocInformationToLayoutFields);
        return $ocInformationToLayoutRepo->create($theme);
    }

    /**
     * Get fake instance of oc_information_to_layout
     *
     * @param array $ocInformationToLayoutFields
     * @return oc_information_to_layout
     */
    public function fakeoc_information_to_layout($ocInformationToLayoutFields = [])
    {
        return new oc_information_to_layout($this->fakeoc_information_to_layoutData($ocInformationToLayoutFields));
    }

    /**
     * Get fake data of oc_information_to_layout
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_information_to_layoutData($ocInformationToLayoutFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'layout_id' => $fake->randomDigitNotNull
        ], $ocInformationToLayoutFields);
    }
}
