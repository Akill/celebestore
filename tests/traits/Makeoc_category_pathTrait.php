<?php

use Faker\Factory as Faker;
use App\Models\oc_category_path;
use App\Repositories\oc_category_pathRepository;

trait Makeoc_category_pathTrait
{
    /**
     * Create fake instance of oc_category_path and save it in database
     *
     * @param array $ocCategoryPathFields
     * @return oc_category_path
     */
    public function makeoc_category_path($ocCategoryPathFields = [])
    {
        /** @var oc_category_pathRepository $ocCategoryPathRepo */
        $ocCategoryPathRepo = App::make(oc_category_pathRepository::class);
        $theme = $this->fakeoc_category_pathData($ocCategoryPathFields);
        return $ocCategoryPathRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category_path
     *
     * @param array $ocCategoryPathFields
     * @return oc_category_path
     */
    public function fakeoc_category_path($ocCategoryPathFields = [])
    {
        return new oc_category_path($this->fakeoc_category_pathData($ocCategoryPathFields));
    }

    /**
     * Get fake data of oc_category_path
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_category_pathData($ocCategoryPathFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'path_id' => $fake->randomDigitNotNull,
            'level' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryPathFields);
    }
}
