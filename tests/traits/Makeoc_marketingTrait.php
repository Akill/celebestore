<?php

use Faker\Factory as Faker;
use App\Models\oc_marketing;
use App\Repositories\oc_marketingRepository;

trait Makeoc_marketingTrait
{
    /**
     * Create fake instance of oc_marketing and save it in database
     *
     * @param array $ocMarketingFields
     * @return oc_marketing
     */
    public function makeoc_marketing($ocMarketingFields = [])
    {
        /** @var oc_marketingRepository $ocMarketingRepo */
        $ocMarketingRepo = App::make(oc_marketingRepository::class);
        $theme = $this->fakeoc_marketingData($ocMarketingFields);
        return $ocMarketingRepo->create($theme);
    }

    /**
     * Get fake instance of oc_marketing
     *
     * @param array $ocMarketingFields
     * @return oc_marketing
     */
    public function fakeoc_marketing($ocMarketingFields = [])
    {
        return new oc_marketing($this->fakeoc_marketingData($ocMarketingFields));
    }

    /**
     * Get fake data of oc_marketing
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_marketingData($ocMarketingFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'code' => $fake->word,
            'clicks' => $fake->randomDigitNotNull,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocMarketingFields);
    }
}
