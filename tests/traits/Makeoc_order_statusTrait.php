<?php

use Faker\Factory as Faker;
use App\Models\oc_order_status;
use App\Repositories\oc_order_statusRepository;

trait Makeoc_order_statusTrait
{
    /**
     * Create fake instance of oc_order_status and save it in database
     *
     * @param array $ocOrderStatusFields
     * @return oc_order_status
     */
    public function makeoc_order_status($ocOrderStatusFields = [])
    {
        /** @var oc_order_statusRepository $ocOrderStatusRepo */
        $ocOrderStatusRepo = App::make(oc_order_statusRepository::class);
        $theme = $this->fakeoc_order_statusData($ocOrderStatusFields);
        return $ocOrderStatusRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_status
     *
     * @param array $ocOrderStatusFields
     * @return oc_order_status
     */
    public function fakeoc_order_status($ocOrderStatusFields = [])
    {
        return new oc_order_status($this->fakeoc_order_statusData($ocOrderStatusFields));
    }

    /**
     * Get fake data of oc_order_status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_statusData($ocOrderStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocOrderStatusFields);
    }
}
