<?php

use Faker\Factory as Faker;
use App\Models\oc_order_recurring_transaction;
use App\Repositories\oc_order_recurring_transactionRepository;

trait Makeoc_order_recurring_transactionTrait
{
    /**
     * Create fake instance of oc_order_recurring_transaction and save it in database
     *
     * @param array $ocOrderRecurringTransactionFields
     * @return oc_order_recurring_transaction
     */
    public function makeoc_order_recurring_transaction($ocOrderRecurringTransactionFields = [])
    {
        /** @var oc_order_recurring_transactionRepository $ocOrderRecurringTransactionRepo */
        $ocOrderRecurringTransactionRepo = App::make(oc_order_recurring_transactionRepository::class);
        $theme = $this->fakeoc_order_recurring_transactionData($ocOrderRecurringTransactionFields);
        return $ocOrderRecurringTransactionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_recurring_transaction
     *
     * @param array $ocOrderRecurringTransactionFields
     * @return oc_order_recurring_transaction
     */
    public function fakeoc_order_recurring_transaction($ocOrderRecurringTransactionFields = [])
    {
        return new oc_order_recurring_transaction($this->fakeoc_order_recurring_transactionData($ocOrderRecurringTransactionFields));
    }

    /**
     * Get fake data of oc_order_recurring_transaction
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_recurring_transactionData($ocOrderRecurringTransactionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_recurring_id' => $fake->randomDigitNotNull,
            'reference' => $fake->word,
            'type' => $fake->word,
            'amount' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocOrderRecurringTransactionFields);
    }
}
