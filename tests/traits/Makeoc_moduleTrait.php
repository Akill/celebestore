<?php

use Faker\Factory as Faker;
use App\Models\oc_module;
use App\Repositories\oc_moduleRepository;

trait Makeoc_moduleTrait
{
    /**
     * Create fake instance of oc_module and save it in database
     *
     * @param array $ocModuleFields
     * @return oc_module
     */
    public function makeoc_module($ocModuleFields = [])
    {
        /** @var oc_moduleRepository $ocModuleRepo */
        $ocModuleRepo = App::make(oc_moduleRepository::class);
        $theme = $this->fakeoc_moduleData($ocModuleFields);
        return $ocModuleRepo->create($theme);
    }

    /**
     * Get fake instance of oc_module
     *
     * @param array $ocModuleFields
     * @return oc_module
     */
    public function fakeoc_module($ocModuleFields = [])
    {
        return new oc_module($this->fakeoc_moduleData($ocModuleFields));
    }

    /**
     * Get fake data of oc_module
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_moduleData($ocModuleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'code' => $fake->word,
            'setting' => $fake->text
        ], $ocModuleFields);
    }
}
