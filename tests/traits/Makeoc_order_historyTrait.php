<?php

use Faker\Factory as Faker;
use App\Models\oc_order_history;
use App\Repositories\oc_order_historyRepository;

trait Makeoc_order_historyTrait
{
    /**
     * Create fake instance of oc_order_history and save it in database
     *
     * @param array $ocOrderHistoryFields
     * @return oc_order_history
     */
    public function makeoc_order_history($ocOrderHistoryFields = [])
    {
        /** @var oc_order_historyRepository $ocOrderHistoryRepo */
        $ocOrderHistoryRepo = App::make(oc_order_historyRepository::class);
        $theme = $this->fakeoc_order_historyData($ocOrderHistoryFields);
        return $ocOrderHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_order_history
     *
     * @param array $ocOrderHistoryFields
     * @return oc_order_history
     */
    public function fakeoc_order_history($ocOrderHistoryFields = [])
    {
        return new oc_order_history($this->fakeoc_order_historyData($ocOrderHistoryFields));
    }

    /**
     * Get fake data of oc_order_history
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_order_historyData($ocOrderHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'order_id' => $fake->randomDigitNotNull,
            'order_status_id' => $fake->randomDigitNotNull,
            'notify' => $fake->word,
            'comment' => $fake->text,
            'date_added' => $fake->date('Y-m-d H:i:s')
        ], $ocOrderHistoryFields);
    }
}
