<?php

use Faker\Factory as Faker;
use App\Models\oc_affiliate_activity;
use App\Repositories\oc_affiliate_activityRepository;

trait Makeoc_affiliate_activityTrait
{
    /**
     * Create fake instance of oc_affiliate_activity and save it in database
     *
     * @param array $ocAffiliateActivityFields
     * @return oc_affiliate_activity
     */
    public function makeoc_affiliate_activity($ocAffiliateActivityFields = [])
    {
        /** @var oc_affiliate_activityRepository $ocAffiliateActivityRepo */
        $ocAffiliateActivityRepo = App::make(oc_affiliate_activityRepository::class);
        $theme = $this->fakeoc_affiliate_activityData($ocAffiliateActivityFields);
        return $ocAffiliateActivityRepo->create($theme);
    }

    /**
     * Get fake instance of oc_affiliate_activity
     *
     * @param array $ocAffiliateActivityFields
     * @return oc_affiliate_activity
     */
    public function fakeoc_affiliate_activity($ocAffiliateActivityFields = [])
    {
        return new oc_affiliate_activity($this->fakeoc_affiliate_activityData($ocAffiliateActivityFields));
    }

    /**
     * Get fake data of oc_affiliate_activity
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_affiliate_activityData($ocAffiliateActivityFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'affiliate_id' => $fake->randomDigitNotNull,
            'key' => $fake->word,
            'data' => $fake->text,
            'ip' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAffiliateActivityFields);
    }
}
