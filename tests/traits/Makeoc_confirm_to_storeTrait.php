<?php

use Faker\Factory as Faker;
use App\Models\oc_confirm_to_store;
use App\Repositories\oc_confirm_to_storeRepository;

trait Makeoc_confirm_to_storeTrait
{
    /**
     * Create fake instance of oc_confirm_to_store and save it in database
     *
     * @param array $ocConfirmToStoreFields
     * @return oc_confirm_to_store
     */
    public function makeoc_confirm_to_store($ocConfirmToStoreFields = [])
    {
        /** @var oc_confirm_to_storeRepository $ocConfirmToStoreRepo */
        $ocConfirmToStoreRepo = App::make(oc_confirm_to_storeRepository::class);
        $theme = $this->fakeoc_confirm_to_storeData($ocConfirmToStoreFields);
        return $ocConfirmToStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_confirm_to_store
     *
     * @param array $ocConfirmToStoreFields
     * @return oc_confirm_to_store
     */
    public function fakeoc_confirm_to_store($ocConfirmToStoreFields = [])
    {
        return new oc_confirm_to_store($this->fakeoc_confirm_to_storeData($ocConfirmToStoreFields));
    }

    /**
     * Get fake data of oc_confirm_to_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_confirm_to_storeData($ocConfirmToStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocConfirmToStoreFields);
    }
}
