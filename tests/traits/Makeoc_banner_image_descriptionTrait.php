<?php

use Faker\Factory as Faker;
use App\Models\oc_banner_image_description;
use App\Repositories\oc_banner_image_descriptionRepository;

trait Makeoc_banner_image_descriptionTrait
{
    /**
     * Create fake instance of oc_banner_image_description and save it in database
     *
     * @param array $ocBannerImageDescriptionFields
     * @return oc_banner_image_description
     */
    public function makeoc_banner_image_description($ocBannerImageDescriptionFields = [])
    {
        /** @var oc_banner_image_descriptionRepository $ocBannerImageDescriptionRepo */
        $ocBannerImageDescriptionRepo = App::make(oc_banner_image_descriptionRepository::class);
        $theme = $this->fakeoc_banner_image_descriptionData($ocBannerImageDescriptionFields);
        return $ocBannerImageDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_banner_image_description
     *
     * @param array $ocBannerImageDescriptionFields
     * @return oc_banner_image_description
     */
    public function fakeoc_banner_image_description($ocBannerImageDescriptionFields = [])
    {
        return new oc_banner_image_description($this->fakeoc_banner_image_descriptionData($ocBannerImageDescriptionFields));
    }

    /**
     * Get fake data of oc_banner_image_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_banner_image_descriptionData($ocBannerImageDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'banner_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocBannerImageDescriptionFields);
    }
}
