<?php

use Faker\Factory as Faker;
use App\Models\oc_country_hpwd;
use App\Repositories\oc_country_hpwdRepository;

trait Makeoc_country_hpwdTrait
{
    /**
     * Create fake instance of oc_country_hpwd and save it in database
     *
     * @param array $ocCountryHpwdFields
     * @return oc_country_hpwd
     */
    public function makeoc_country_hpwd($ocCountryHpwdFields = [])
    {
        /** @var oc_country_hpwdRepository $ocCountryHpwdRepo */
        $ocCountryHpwdRepo = App::make(oc_country_hpwdRepository::class);
        $theme = $this->fakeoc_country_hpwdData($ocCountryHpwdFields);
        return $ocCountryHpwdRepo->create($theme);
    }

    /**
     * Get fake instance of oc_country_hpwd
     *
     * @param array $ocCountryHpwdFields
     * @return oc_country_hpwd
     */
    public function fakeoc_country_hpwd($ocCountryHpwdFields = [])
    {
        return new oc_country_hpwd($this->fakeoc_country_hpwdData($ocCountryHpwdFields));
    }

    /**
     * Get fake data of oc_country_hpwd
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_country_hpwdData($ocCountryHpwdFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'iso_code_2' => $fake->word,
            'iso_code_3' => $fake->word,
            'address_format' => $fake->text,
            'postcode_required' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCountryHpwdFields);
    }
}
