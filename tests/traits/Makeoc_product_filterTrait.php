<?php

use Faker\Factory as Faker;
use App\Models\oc_product_filter;
use App\Repositories\oc_product_filterRepository;

trait Makeoc_product_filterTrait
{
    /**
     * Create fake instance of oc_product_filter and save it in database
     *
     * @param array $ocProductFilterFields
     * @return oc_product_filter
     */
    public function makeoc_product_filter($ocProductFilterFields = [])
    {
        /** @var oc_product_filterRepository $ocProductFilterRepo */
        $ocProductFilterRepo = App::make(oc_product_filterRepository::class);
        $theme = $this->fakeoc_product_filterData($ocProductFilterFields);
        return $ocProductFilterRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_filter
     *
     * @param array $ocProductFilterFields
     * @return oc_product_filter
     */
    public function fakeoc_product_filter($ocProductFilterFields = [])
    {
        return new oc_product_filter($this->fakeoc_product_filterData($ocProductFilterFields));
    }

    /**
     * Get fake data of oc_product_filter
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_filterData($ocProductFilterFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'filter_id' => $fake->randomDigitNotNull
        ], $ocProductFilterFields);
    }
}
