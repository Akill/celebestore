<?php

use Faker\Factory as Faker;
use App\Models\oc_affiliate_transaction;
use App\Repositories\oc_affiliate_transactionRepository;

trait Makeoc_affiliate_transactionTrait
{
    /**
     * Create fake instance of oc_affiliate_transaction and save it in database
     *
     * @param array $ocAffiliateTransactionFields
     * @return oc_affiliate_transaction
     */
    public function makeoc_affiliate_transaction($ocAffiliateTransactionFields = [])
    {
        /** @var oc_affiliate_transactionRepository $ocAffiliateTransactionRepo */
        $ocAffiliateTransactionRepo = App::make(oc_affiliate_transactionRepository::class);
        $theme = $this->fakeoc_affiliate_transactionData($ocAffiliateTransactionFields);
        return $ocAffiliateTransactionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_affiliate_transaction
     *
     * @param array $ocAffiliateTransactionFields
     * @return oc_affiliate_transaction
     */
    public function fakeoc_affiliate_transaction($ocAffiliateTransactionFields = [])
    {
        return new oc_affiliate_transaction($this->fakeoc_affiliate_transactionData($ocAffiliateTransactionFields));
    }

    /**
     * Get fake data of oc_affiliate_transaction
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_affiliate_transactionData($ocAffiliateTransactionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'affiliate_id' => $fake->randomDigitNotNull,
            'order_id' => $fake->randomDigitNotNull,
            'description' => $fake->text,
            'amount' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocAffiliateTransactionFields);
    }
}
