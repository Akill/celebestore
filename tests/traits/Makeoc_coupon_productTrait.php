<?php

use Faker\Factory as Faker;
use App\Models\oc_coupon_product;
use App\Repositories\oc_coupon_productRepository;

trait Makeoc_coupon_productTrait
{
    /**
     * Create fake instance of oc_coupon_product and save it in database
     *
     * @param array $ocCouponProductFields
     * @return oc_coupon_product
     */
    public function makeoc_coupon_product($ocCouponProductFields = [])
    {
        /** @var oc_coupon_productRepository $ocCouponProductRepo */
        $ocCouponProductRepo = App::make(oc_coupon_productRepository::class);
        $theme = $this->fakeoc_coupon_productData($ocCouponProductFields);
        return $ocCouponProductRepo->create($theme);
    }

    /**
     * Get fake instance of oc_coupon_product
     *
     * @param array $ocCouponProductFields
     * @return oc_coupon_product
     */
    public function fakeoc_coupon_product($ocCouponProductFields = [])
    {
        return new oc_coupon_product($this->fakeoc_coupon_productData($ocCouponProductFields));
    }

    /**
     * Get fake data of oc_coupon_product
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_coupon_productData($ocCouponProductFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'coupon_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCouponProductFields);
    }
}
