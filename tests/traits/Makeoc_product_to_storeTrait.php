<?php

use Faker\Factory as Faker;
use App\Models\oc_product_to_store;
use App\Repositories\oc_product_to_storeRepository;

trait Makeoc_product_to_storeTrait
{
    /**
     * Create fake instance of oc_product_to_store and save it in database
     *
     * @param array $ocProductToStoreFields
     * @return oc_product_to_store
     */
    public function makeoc_product_to_store($ocProductToStoreFields = [])
    {
        /** @var oc_product_to_storeRepository $ocProductToStoreRepo */
        $ocProductToStoreRepo = App::make(oc_product_to_storeRepository::class);
        $theme = $this->fakeoc_product_to_storeData($ocProductToStoreFields);
        return $ocProductToStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_to_store
     *
     * @param array $ocProductToStoreFields
     * @return oc_product_to_store
     */
    public function fakeoc_product_to_store($ocProductToStoreFields = [])
    {
        return new oc_product_to_store($this->fakeoc_product_to_storeData($ocProductToStoreFields));
    }

    /**
     * Get fake data of oc_product_to_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_to_storeData($ocProductToStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull
        ], $ocProductToStoreFields);
    }
}
