<?php

use Faker\Factory as Faker;
use App\Models\oc_customer;
use App\Repositories\oc_customerRepository;

trait Makeoc_customerTrait
{
    /**
     * Create fake instance of oc_customer and save it in database
     *
     * @param array $ocCustomerFields
     * @return oc_customer
     */
    public function makeoc_customer($ocCustomerFields = [])
    {
        /** @var oc_customerRepository $ocCustomerRepo */
        $ocCustomerRepo = App::make(oc_customerRepository::class);
        $theme = $this->fakeoc_customerData($ocCustomerFields);
        return $ocCustomerRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer
     *
     * @param array $ocCustomerFields
     * @return oc_customer
     */
    public function fakeoc_customer($ocCustomerFields = [])
    {
        return new oc_customer($this->fakeoc_customerData($ocCustomerFields));
    }

    /**
     * Get fake data of oc_customer
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customerData($ocCustomerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_group_id' => $fake->randomDigitNotNull,
            'store_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'email' => $fake->word,
            'telephone' => $fake->word,
            'fax' => $fake->word,
            'password' => $fake->word,
            'salt' => $fake->word,
            'cart' => $fake->text,
            'wishlist' => $fake->text,
            'newsletter' => $fake->word,
            'address_id' => $fake->randomDigitNotNull,
            'custom_field' => $fake->text,
            'ip' => $fake->word,
            'status' => $fake->word,
            'approved' => $fake->word,
            'safe' => $fake->word,
            'token' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerFields);
    }
}
