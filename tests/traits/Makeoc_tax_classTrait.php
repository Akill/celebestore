<?php

use Faker\Factory as Faker;
use App\Models\oc_tax_class;
use App\Repositories\oc_tax_classRepository;

trait Makeoc_tax_classTrait
{
    /**
     * Create fake instance of oc_tax_class and save it in database
     *
     * @param array $ocTaxClassFields
     * @return oc_tax_class
     */
    public function makeoc_tax_class($ocTaxClassFields = [])
    {
        /** @var oc_tax_classRepository $ocTaxClassRepo */
        $ocTaxClassRepo = App::make(oc_tax_classRepository::class);
        $theme = $this->fakeoc_tax_classData($ocTaxClassFields);
        return $ocTaxClassRepo->create($theme);
    }

    /**
     * Get fake instance of oc_tax_class
     *
     * @param array $ocTaxClassFields
     * @return oc_tax_class
     */
    public function fakeoc_tax_class($ocTaxClassFields = [])
    {
        return new oc_tax_class($this->fakeoc_tax_classData($ocTaxClassFields));
    }

    /**
     * Get fake data of oc_tax_class
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_tax_classData($ocTaxClassFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'date_modified' => $fake->date('Y-m-d H:i:s')
        ], $ocTaxClassFields);
    }
}
