<?php

use Faker\Factory as Faker;
use App\Models\oc_layout_module;
use App\Repositories\oc_layout_moduleRepository;

trait Makeoc_layout_moduleTrait
{
    /**
     * Create fake instance of oc_layout_module and save it in database
     *
     * @param array $ocLayoutModuleFields
     * @return oc_layout_module
     */
    public function makeoc_layout_module($ocLayoutModuleFields = [])
    {
        /** @var oc_layout_moduleRepository $ocLayoutModuleRepo */
        $ocLayoutModuleRepo = App::make(oc_layout_moduleRepository::class);
        $theme = $this->fakeoc_layout_moduleData($ocLayoutModuleFields);
        return $ocLayoutModuleRepo->create($theme);
    }

    /**
     * Get fake instance of oc_layout_module
     *
     * @param array $ocLayoutModuleFields
     * @return oc_layout_module
     */
    public function fakeoc_layout_module($ocLayoutModuleFields = [])
    {
        return new oc_layout_module($this->fakeoc_layout_moduleData($ocLayoutModuleFields));
    }

    /**
     * Get fake data of oc_layout_module
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_layout_moduleData($ocLayoutModuleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'layout_id' => $fake->randomDigitNotNull,
            'code' => $fake->word,
            'position' => $fake->word,
            'sort_order' => $fake->randomDigitNotNull
        ], $ocLayoutModuleFields);
    }
}
