<?php

use Faker\Factory as Faker;
use App\Models\oc_manufacturer_to_store;
use App\Repositories\oc_manufacturer_to_storeRepository;

trait Makeoc_manufacturer_to_storeTrait
{
    /**
     * Create fake instance of oc_manufacturer_to_store and save it in database
     *
     * @param array $ocManufacturerToStoreFields
     * @return oc_manufacturer_to_store
     */
    public function makeoc_manufacturer_to_store($ocManufacturerToStoreFields = [])
    {
        /** @var oc_manufacturer_to_storeRepository $ocManufacturerToStoreRepo */
        $ocManufacturerToStoreRepo = App::make(oc_manufacturer_to_storeRepository::class);
        $theme = $this->fakeoc_manufacturer_to_storeData($ocManufacturerToStoreFields);
        return $ocManufacturerToStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_manufacturer_to_store
     *
     * @param array $ocManufacturerToStoreFields
     * @return oc_manufacturer_to_store
     */
    public function fakeoc_manufacturer_to_store($ocManufacturerToStoreFields = [])
    {
        return new oc_manufacturer_to_store($this->fakeoc_manufacturer_to_storeData($ocManufacturerToStoreFields));
    }

    /**
     * Get fake data of oc_manufacturer_to_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_manufacturer_to_storeData($ocManufacturerToStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull
        ], $ocManufacturerToStoreFields);
    }
}
