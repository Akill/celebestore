<?php

use Faker\Factory as Faker;
use App\Models\oc_url_alias;
use App\Repositories\oc_url_aliasRepository;

trait Makeoc_url_aliasTrait
{
    /**
     * Create fake instance of oc_url_alias and save it in database
     *
     * @param array $ocUrlAliasFields
     * @return oc_url_alias
     */
    public function makeoc_url_alias($ocUrlAliasFields = [])
    {
        /** @var oc_url_aliasRepository $ocUrlAliasRepo */
        $ocUrlAliasRepo = App::make(oc_url_aliasRepository::class);
        $theme = $this->fakeoc_url_aliasData($ocUrlAliasFields);
        return $ocUrlAliasRepo->create($theme);
    }

    /**
     * Get fake instance of oc_url_alias
     *
     * @param array $ocUrlAliasFields
     * @return oc_url_alias
     */
    public function fakeoc_url_alias($ocUrlAliasFields = [])
    {
        return new oc_url_alias($this->fakeoc_url_aliasData($ocUrlAliasFields));
    }

    /**
     * Get fake data of oc_url_alias
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_url_aliasData($ocUrlAliasFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'query' => $fake->word,
            'keyword' => $fake->word
        ], $ocUrlAliasFields);
    }
}
