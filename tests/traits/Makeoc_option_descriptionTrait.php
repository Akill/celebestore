<?php

use Faker\Factory as Faker;
use App\Models\oc_option_description;
use App\Repositories\oc_option_descriptionRepository;

trait Makeoc_option_descriptionTrait
{
    /**
     * Create fake instance of oc_option_description and save it in database
     *
     * @param array $ocOptionDescriptionFields
     * @return oc_option_description
     */
    public function makeoc_option_description($ocOptionDescriptionFields = [])
    {
        /** @var oc_option_descriptionRepository $ocOptionDescriptionRepo */
        $ocOptionDescriptionRepo = App::make(oc_option_descriptionRepository::class);
        $theme = $this->fakeoc_option_descriptionData($ocOptionDescriptionFields);
        return $ocOptionDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_option_description
     *
     * @param array $ocOptionDescriptionFields
     * @return oc_option_description
     */
    public function fakeoc_option_description($ocOptionDescriptionFields = [])
    {
        return new oc_option_description($this->fakeoc_option_descriptionData($ocOptionDescriptionFields));
    }

    /**
     * Get fake data of oc_option_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_option_descriptionData($ocOptionDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocOptionDescriptionFields);
    }
}
