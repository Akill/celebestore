<?php

use Faker\Factory as Faker;
use App\Models\oc_weight_class_description;
use App\Repositories\oc_weight_class_descriptionRepository;

trait Makeoc_weight_class_descriptionTrait
{
    /**
     * Create fake instance of oc_weight_class_description and save it in database
     *
     * @param array $ocWeightClassDescriptionFields
     * @return oc_weight_class_description
     */
    public function makeoc_weight_class_description($ocWeightClassDescriptionFields = [])
    {
        /** @var oc_weight_class_descriptionRepository $ocWeightClassDescriptionRepo */
        $ocWeightClassDescriptionRepo = App::make(oc_weight_class_descriptionRepository::class);
        $theme = $this->fakeoc_weight_class_descriptionData($ocWeightClassDescriptionFields);
        return $ocWeightClassDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_weight_class_description
     *
     * @param array $ocWeightClassDescriptionFields
     * @return oc_weight_class_description
     */
    public function fakeoc_weight_class_description($ocWeightClassDescriptionFields = [])
    {
        return new oc_weight_class_description($this->fakeoc_weight_class_descriptionData($ocWeightClassDescriptionFields));
    }

    /**
     * Get fake data of oc_weight_class_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_weight_class_descriptionData($ocWeightClassDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'unit' => $fake->word
        ], $ocWeightClassDescriptionFields);
    }
}
