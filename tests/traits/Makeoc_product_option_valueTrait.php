<?php

use Faker\Factory as Faker;
use App\Models\oc_product_option_value;
use App\Repositories\oc_product_option_valueRepository;

trait Makeoc_product_option_valueTrait
{
    /**
     * Create fake instance of oc_product_option_value and save it in database
     *
     * @param array $ocProductOptionValueFields
     * @return oc_product_option_value
     */
    public function makeoc_product_option_value($ocProductOptionValueFields = [])
    {
        /** @var oc_product_option_valueRepository $ocProductOptionValueRepo */
        $ocProductOptionValueRepo = App::make(oc_product_option_valueRepository::class);
        $theme = $this->fakeoc_product_option_valueData($ocProductOptionValueFields);
        return $ocProductOptionValueRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_option_value
     *
     * @param array $ocProductOptionValueFields
     * @return oc_product_option_value
     */
    public function fakeoc_product_option_value($ocProductOptionValueFields = [])
    {
        return new oc_product_option_value($this->fakeoc_product_option_valueData($ocProductOptionValueFields));
    }

    /**
     * Get fake data of oc_product_option_value
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_option_valueData($ocProductOptionValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_option_id' => $fake->randomDigitNotNull,
            'product_id' => $fake->randomDigitNotNull,
            'option_id' => $fake->randomDigitNotNull,
            'option_value_id' => $fake->randomDigitNotNull,
            'quantity' => $fake->randomDigitNotNull,
            'subtract' => $fake->word,
            'price' => $fake->word,
            'price_prefix' => $fake->word,
            'points' => $fake->randomDigitNotNull,
            'points_prefix' => $fake->word,
            'weight' => $fake->word,
            'weight_prefix' => $fake->word
        ], $ocProductOptionValueFields);
    }
}
