<?php

use Faker\Factory as Faker;
use App\Models\oc_recurring_description;
use App\Repositories\oc_recurring_descriptionRepository;

trait Makeoc_recurring_descriptionTrait
{
    /**
     * Create fake instance of oc_recurring_description and save it in database
     *
     * @param array $ocRecurringDescriptionFields
     * @return oc_recurring_description
     */
    public function makeoc_recurring_description($ocRecurringDescriptionFields = [])
    {
        /** @var oc_recurring_descriptionRepository $ocRecurringDescriptionRepo */
        $ocRecurringDescriptionRepo = App::make(oc_recurring_descriptionRepository::class);
        $theme = $this->fakeoc_recurring_descriptionData($ocRecurringDescriptionFields);
        return $ocRecurringDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_recurring_description
     *
     * @param array $ocRecurringDescriptionFields
     * @return oc_recurring_description
     */
    public function fakeoc_recurring_description($ocRecurringDescriptionFields = [])
    {
        return new oc_recurring_description($this->fakeoc_recurring_descriptionData($ocRecurringDescriptionFields));
    }

    /**
     * Get fake data of oc_recurring_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_recurring_descriptionData($ocRecurringDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocRecurringDescriptionFields);
    }
}
