<?php

use Faker\Factory as Faker;
use App\Models\oc_coupon_history;
use App\Repositories\oc_coupon_historyRepository;

trait Makeoc_coupon_historyTrait
{
    /**
     * Create fake instance of oc_coupon_history and save it in database
     *
     * @param array $ocCouponHistoryFields
     * @return oc_coupon_history
     */
    public function makeoc_coupon_history($ocCouponHistoryFields = [])
    {
        /** @var oc_coupon_historyRepository $ocCouponHistoryRepo */
        $ocCouponHistoryRepo = App::make(oc_coupon_historyRepository::class);
        $theme = $this->fakeoc_coupon_historyData($ocCouponHistoryFields);
        return $ocCouponHistoryRepo->create($theme);
    }

    /**
     * Get fake instance of oc_coupon_history
     *
     * @param array $ocCouponHistoryFields
     * @return oc_coupon_history
     */
    public function fakeoc_coupon_history($ocCouponHistoryFields = [])
    {
        return new oc_coupon_history($this->fakeoc_coupon_historyData($ocCouponHistoryFields));
    }

    /**
     * Get fake data of oc_coupon_history
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_coupon_historyData($ocCouponHistoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'coupon_id' => $fake->randomDigitNotNull,
            'order_id' => $fake->randomDigitNotNull,
            'customer_id' => $fake->randomDigitNotNull,
            'amount' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCouponHistoryFields);
    }
}
