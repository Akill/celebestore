<?php

use Faker\Factory as Faker;
use App\Models\oc_information_description;
use App\Repositories\oc_information_descriptionRepository;

trait Makeoc_information_descriptionTrait
{
    /**
     * Create fake instance of oc_information_description and save it in database
     *
     * @param array $ocInformationDescriptionFields
     * @return oc_information_description
     */
    public function makeoc_information_description($ocInformationDescriptionFields = [])
    {
        /** @var oc_information_descriptionRepository $ocInformationDescriptionRepo */
        $ocInformationDescriptionRepo = App::make(oc_information_descriptionRepository::class);
        $theme = $this->fakeoc_information_descriptionData($ocInformationDescriptionFields);
        return $ocInformationDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_information_description
     *
     * @param array $ocInformationDescriptionFields
     * @return oc_information_description
     */
    public function fakeoc_information_description($ocInformationDescriptionFields = [])
    {
        return new oc_information_description($this->fakeoc_information_descriptionData($ocInformationDescriptionFields));
    }

    /**
     * Get fake data of oc_information_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_information_descriptionData($ocInformationDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'title' => $fake->word,
            'description' => $fake->text,
            'meta_title' => $fake->word,
            'meta_description' => $fake->word,
            'meta_keyword' => $fake->word
        ], $ocInformationDescriptionFields);
    }
}
