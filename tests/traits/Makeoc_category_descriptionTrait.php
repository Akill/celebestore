<?php

use Faker\Factory as Faker;
use App\Models\oc_category_description;
use App\Repositories\oc_category_descriptionRepository;

trait Makeoc_category_descriptionTrait
{
    /**
     * Create fake instance of oc_category_description and save it in database
     *
     * @param array $ocCategoryDescriptionFields
     * @return oc_category_description
     */
    public function makeoc_category_description($ocCategoryDescriptionFields = [])
    {
        /** @var oc_category_descriptionRepository $ocCategoryDescriptionRepo */
        $ocCategoryDescriptionRepo = App::make(oc_category_descriptionRepository::class);
        $theme = $this->fakeoc_category_descriptionData($ocCategoryDescriptionFields);
        return $ocCategoryDescriptionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_category_description
     *
     * @param array $ocCategoryDescriptionFields
     * @return oc_category_description
     */
    public function fakeoc_category_description($ocCategoryDescriptionFields = [])
    {
        return new oc_category_description($this->fakeoc_category_descriptionData($ocCategoryDescriptionFields));
    }

    /**
     * Get fake data of oc_category_description
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_category_descriptionData($ocCategoryDescriptionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'description' => $fake->text,
            'meta_title' => $fake->word,
            'meta_description' => $fake->word,
            'meta_keyword' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCategoryDescriptionFields);
    }
}
