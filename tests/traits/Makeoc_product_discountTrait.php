<?php

use Faker\Factory as Faker;
use App\Models\oc_product_discount;
use App\Repositories\oc_product_discountRepository;

trait Makeoc_product_discountTrait
{
    /**
     * Create fake instance of oc_product_discount and save it in database
     *
     * @param array $ocProductDiscountFields
     * @return oc_product_discount
     */
    public function makeoc_product_discount($ocProductDiscountFields = [])
    {
        /** @var oc_product_discountRepository $ocProductDiscountRepo */
        $ocProductDiscountRepo = App::make(oc_product_discountRepository::class);
        $theme = $this->fakeoc_product_discountData($ocProductDiscountFields);
        return $ocProductDiscountRepo->create($theme);
    }

    /**
     * Get fake instance of oc_product_discount
     *
     * @param array $ocProductDiscountFields
     * @return oc_product_discount
     */
    public function fakeoc_product_discount($ocProductDiscountFields = [])
    {
        return new oc_product_discount($this->fakeoc_product_discountData($ocProductDiscountFields));
    }

    /**
     * Get fake data of oc_product_discount
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_product_discountData($ocProductDiscountFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'product_id' => $fake->randomDigitNotNull,
            'customer_group_id' => $fake->randomDigitNotNull,
            'quantity' => $fake->randomDigitNotNull,
            'priority' => $fake->randomDigitNotNull,
            'price' => $fake->word,
            'date_start' => $fake->word,
            'date_end' => $fake->word
        ], $ocProductDiscountFields);
    }
}
