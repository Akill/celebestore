<?php

use Faker\Factory as Faker;
use App\Models\oc_information_to_store;
use App\Repositories\oc_information_to_storeRepository;

trait Makeoc_information_to_storeTrait
{
    /**
     * Create fake instance of oc_information_to_store and save it in database
     *
     * @param array $ocInformationToStoreFields
     * @return oc_information_to_store
     */
    public function makeoc_information_to_store($ocInformationToStoreFields = [])
    {
        /** @var oc_information_to_storeRepository $ocInformationToStoreRepo */
        $ocInformationToStoreRepo = App::make(oc_information_to_storeRepository::class);
        $theme = $this->fakeoc_information_to_storeData($ocInformationToStoreFields);
        return $ocInformationToStoreRepo->create($theme);
    }

    /**
     * Get fake instance of oc_information_to_store
     *
     * @param array $ocInformationToStoreFields
     * @return oc_information_to_store
     */
    public function fakeoc_information_to_store($ocInformationToStoreFields = [])
    {
        return new oc_information_to_store($this->fakeoc_information_to_storeData($ocInformationToStoreFields));
    }

    /**
     * Get fake data of oc_information_to_store
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_information_to_storeData($ocInformationToStoreFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'store_id' => $fake->randomDigitNotNull
        ], $ocInformationToStoreFields);
    }
}
