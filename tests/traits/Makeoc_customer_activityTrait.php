<?php

use Faker\Factory as Faker;
use App\Models\oc_customer_activity;
use App\Repositories\oc_customer_activityRepository;

trait Makeoc_customer_activityTrait
{
    /**
     * Create fake instance of oc_customer_activity and save it in database
     *
     * @param array $ocCustomerActivityFields
     * @return oc_customer_activity
     */
    public function makeoc_customer_activity($ocCustomerActivityFields = [])
    {
        /** @var oc_customer_activityRepository $ocCustomerActivityRepo */
        $ocCustomerActivityRepo = App::make(oc_customer_activityRepository::class);
        $theme = $this->fakeoc_customer_activityData($ocCustomerActivityFields);
        return $ocCustomerActivityRepo->create($theme);
    }

    /**
     * Get fake instance of oc_customer_activity
     *
     * @param array $ocCustomerActivityFields
     * @return oc_customer_activity
     */
    public function fakeoc_customer_activity($ocCustomerActivityFields = [])
    {
        return new oc_customer_activity($this->fakeoc_customer_activityData($ocCustomerActivityFields));
    }

    /**
     * Get fake data of oc_customer_activity
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_customer_activityData($ocCustomerActivityFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'customer_id' => $fake->randomDigitNotNull,
            'key' => $fake->word,
            'data' => $fake->text,
            'ip' => $fake->word,
            'date_added' => $fake->date('Y-m-d H:i:s'),
            'created_at' => $fake->word,
            'updated_at' => $fake->word,
            'deleted_at' => $fake->word
        ], $ocCustomerActivityFields);
    }
}
