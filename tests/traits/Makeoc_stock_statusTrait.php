<?php

use Faker\Factory as Faker;
use App\Models\oc_stock_status;
use App\Repositories\oc_stock_statusRepository;

trait Makeoc_stock_statusTrait
{
    /**
     * Create fake instance of oc_stock_status and save it in database
     *
     * @param array $ocStockStatusFields
     * @return oc_stock_status
     */
    public function makeoc_stock_status($ocStockStatusFields = [])
    {
        /** @var oc_stock_statusRepository $ocStockStatusRepo */
        $ocStockStatusRepo = App::make(oc_stock_statusRepository::class);
        $theme = $this->fakeoc_stock_statusData($ocStockStatusFields);
        return $ocStockStatusRepo->create($theme);
    }

    /**
     * Get fake instance of oc_stock_status
     *
     * @param array $ocStockStatusFields
     * @return oc_stock_status
     */
    public function fakeoc_stock_status($ocStockStatusFields = [])
    {
        return new oc_stock_status($this->fakeoc_stock_statusData($ocStockStatusFields));
    }

    /**
     * Get fake data of oc_stock_status
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_stock_statusData($ocStockStatusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocStockStatusFields);
    }
}
