<?php

use Faker\Factory as Faker;
use App\Models\oc_return_action;
use App\Repositories\oc_return_actionRepository;

trait Makeoc_return_actionTrait
{
    /**
     * Create fake instance of oc_return_action and save it in database
     *
     * @param array $ocReturnActionFields
     * @return oc_return_action
     */
    public function makeoc_return_action($ocReturnActionFields = [])
    {
        /** @var oc_return_actionRepository $ocReturnActionRepo */
        $ocReturnActionRepo = App::make(oc_return_actionRepository::class);
        $theme = $this->fakeoc_return_actionData($ocReturnActionFields);
        return $ocReturnActionRepo->create($theme);
    }

    /**
     * Get fake instance of oc_return_action
     *
     * @param array $ocReturnActionFields
     * @return oc_return_action
     */
    public function fakeoc_return_action($ocReturnActionFields = [])
    {
        return new oc_return_action($this->fakeoc_return_actionData($ocReturnActionFields));
    }

    /**
     * Get fake data of oc_return_action
     *
     * @param array $postFields
     * @return array
     */
    public function fakeoc_return_actionData($ocReturnActionFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'language_id' => $fake->randomDigitNotNull,
            'name' => $fake->word
        ], $ocReturnActionFields);
    }
}
