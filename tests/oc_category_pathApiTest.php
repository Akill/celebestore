<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_pathApiTest extends TestCase
{
    use Makeoc_category_pathTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_category_path()
    {
        $ocCategoryPath = $this->fakeoc_category_pathData();
        $this->json('POST', '/api/v1/ocCategoryPaths', $ocCategoryPath);

        $this->assertApiResponse($ocCategoryPath);
    }

    /**
     * @test
     */
    public function testReadoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $this->json('GET', '/api/v1/ocCategoryPaths/'.$ocCategoryPath->id);

        $this->assertApiResponse($ocCategoryPath->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $editedoc_category_path = $this->fakeoc_category_pathData();

        $this->json('PUT', '/api/v1/ocCategoryPaths/'.$ocCategoryPath->id, $editedoc_category_path);

        $this->assertApiResponse($editedoc_category_path);
    }

    /**
     * @test
     */
    public function testDeleteoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $this->json('DELETE', '/api/v1/ocCategoryPaths/'.$ocCategoryPath->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCategoryPaths/'.$ocCategoryPath->id);

        $this->assertResponseStatus(404);
    }
}
