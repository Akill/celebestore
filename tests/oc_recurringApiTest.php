<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_recurringApiTest extends TestCase
{
    use Makeoc_recurringTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_recurring()
    {
        $ocRecurring = $this->fakeoc_recurringData();
        $this->json('POST', '/api/v1/ocRecurrings', $ocRecurring);

        $this->assertApiResponse($ocRecurring);
    }

    /**
     * @test
     */
    public function testReadoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $this->json('GET', '/api/v1/ocRecurrings/'.$ocRecurring->id);

        $this->assertApiResponse($ocRecurring->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $editedoc_recurring = $this->fakeoc_recurringData();

        $this->json('PUT', '/api/v1/ocRecurrings/'.$ocRecurring->id, $editedoc_recurring);

        $this->assertApiResponse($editedoc_recurring);
    }

    /**
     * @test
     */
    public function testDeleteoc_recurring()
    {
        $ocRecurring = $this->makeoc_recurring();
        $this->json('DELETE', '/api/v1/ocRecurrings/'.$ocRecurring->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocRecurrings/'.$ocRecurring->id);

        $this->assertResponseStatus(404);
    }
}
