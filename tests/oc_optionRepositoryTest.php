<?php

use App\Models\oc_option;
use App\Repositories\oc_optionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_optionRepositoryTest extends TestCase
{
    use Makeoc_optionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_optionRepository
     */
    protected $ocOptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOptionRepo = App::make(oc_optionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_option()
    {
        $ocOption = $this->fakeoc_optionData();
        $createdoc_option = $this->ocOptionRepo->create($ocOption);
        $createdoc_option = $createdoc_option->toArray();
        $this->assertArrayHasKey('id', $createdoc_option);
        $this->assertNotNull($createdoc_option['id'], 'Created oc_option must have id specified');
        $this->assertNotNull(oc_option::find($createdoc_option['id']), 'oc_option with given id must be in DB');
        $this->assertModelData($ocOption, $createdoc_option);
    }

    /**
     * @test read
     */
    public function testReadoc_option()
    {
        $ocOption = $this->makeoc_option();
        $dboc_option = $this->ocOptionRepo->find($ocOption->id);
        $dboc_option = $dboc_option->toArray();
        $this->assertModelData($ocOption->toArray(), $dboc_option);
    }

    /**
     * @test update
     */
    public function testUpdateoc_option()
    {
        $ocOption = $this->makeoc_option();
        $fakeoc_option = $this->fakeoc_optionData();
        $updatedoc_option = $this->ocOptionRepo->update($fakeoc_option, $ocOption->id);
        $this->assertModelData($fakeoc_option, $updatedoc_option->toArray());
        $dboc_option = $this->ocOptionRepo->find($ocOption->id);
        $this->assertModelData($fakeoc_option, $dboc_option->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_option()
    {
        $ocOption = $this->makeoc_option();
        $resp = $this->ocOptionRepo->delete($ocOption->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_option::find($ocOption->id), 'oc_option should not exist in DB');
    }
}
