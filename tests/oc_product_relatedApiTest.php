<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_product_relatedApiTest extends TestCase
{
    use Makeoc_product_relatedTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_product_related()
    {
        $ocProductRelated = $this->fakeoc_product_relatedData();
        $this->json('POST', '/api/v1/ocProductRelateds', $ocProductRelated);

        $this->assertApiResponse($ocProductRelated);
    }

    /**
     * @test
     */
    public function testReadoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $this->json('GET', '/api/v1/ocProductRelateds/'.$ocProductRelated->id);

        $this->assertApiResponse($ocProductRelated->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $editedoc_product_related = $this->fakeoc_product_relatedData();

        $this->json('PUT', '/api/v1/ocProductRelateds/'.$ocProductRelated->id, $editedoc_product_related);

        $this->assertApiResponse($editedoc_product_related);
    }

    /**
     * @test
     */
    public function testDeleteoc_product_related()
    {
        $ocProductRelated = $this->makeoc_product_related();
        $this->json('DELETE', '/api/v1/ocProductRelateds/'.$ocProductRelated->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocProductRelateds/'.$ocProductRelated->id);

        $this->assertResponseStatus(404);
    }
}
