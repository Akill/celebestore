<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_attribute_groupApiTest extends TestCase
{
    use Makeoc_attribute_groupTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_attribute_group()
    {
        $ocAttributeGroup = $this->fakeoc_attribute_groupData();
        $this->json('POST', '/api/v1/ocAttributeGroups', $ocAttributeGroup);

        $this->assertApiResponse($ocAttributeGroup);
    }

    /**
     * @test
     */
    public function testReadoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $this->json('GET', '/api/v1/ocAttributeGroups/'.$ocAttributeGroup->id);

        $this->assertApiResponse($ocAttributeGroup->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $editedoc_attribute_group = $this->fakeoc_attribute_groupData();

        $this->json('PUT', '/api/v1/ocAttributeGroups/'.$ocAttributeGroup->id, $editedoc_attribute_group);

        $this->assertApiResponse($editedoc_attribute_group);
    }

    /**
     * @test
     */
    public function testDeleteoc_attribute_group()
    {
        $ocAttributeGroup = $this->makeoc_attribute_group();
        $this->json('DELETE', '/api/v1/ocAttributeGroups/'.$ocAttributeGroup->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocAttributeGroups/'.$ocAttributeGroup->id);

        $this->assertResponseStatus(404);
    }
}
