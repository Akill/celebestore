<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_information_descriptionApiTest extends TestCase
{
    use Makeoc_information_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_information_description()
    {
        $ocInformationDescription = $this->fakeoc_information_descriptionData();
        $this->json('POST', '/api/v1/ocInformationDescriptions', $ocInformationDescription);

        $this->assertApiResponse($ocInformationDescription);
    }

    /**
     * @test
     */
    public function testReadoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $this->json('GET', '/api/v1/ocInformationDescriptions/'.$ocInformationDescription->id);

        $this->assertApiResponse($ocInformationDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $editedoc_information_description = $this->fakeoc_information_descriptionData();

        $this->json('PUT', '/api/v1/ocInformationDescriptions/'.$ocInformationDescription->id, $editedoc_information_description);

        $this->assertApiResponse($editedoc_information_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_information_description()
    {
        $ocInformationDescription = $this->makeoc_information_description();
        $this->json('DELETE', '/api/v1/ocInformationDescriptions/'.$ocInformationDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocInformationDescriptions/'.$ocInformationDescription->id);

        $this->assertResponseStatus(404);
    }
}
