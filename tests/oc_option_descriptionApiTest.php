<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_descriptionApiTest extends TestCase
{
    use Makeoc_option_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_option_description()
    {
        $ocOptionDescription = $this->fakeoc_option_descriptionData();
        $this->json('POST', '/api/v1/ocOptionDescriptions', $ocOptionDescription);

        $this->assertApiResponse($ocOptionDescription);
    }

    /**
     * @test
     */
    public function testReadoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $this->json('GET', '/api/v1/ocOptionDescriptions/'.$ocOptionDescription->id);

        $this->assertApiResponse($ocOptionDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $editedoc_option_description = $this->fakeoc_option_descriptionData();

        $this->json('PUT', '/api/v1/ocOptionDescriptions/'.$ocOptionDescription->id, $editedoc_option_description);

        $this->assertApiResponse($editedoc_option_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_option_description()
    {
        $ocOptionDescription = $this->makeoc_option_description();
        $this->json('DELETE', '/api/v1/ocOptionDescriptions/'.$ocOptionDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOptionDescriptions/'.$ocOptionDescription->id);

        $this->assertResponseStatus(404);
    }
}
