<?php

use App\Models\oc_modification;
use App\Repositories\oc_modificationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_modificationRepositoryTest extends TestCase
{
    use Makeoc_modificationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_modificationRepository
     */
    protected $ocModificationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocModificationRepo = App::make(oc_modificationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_modification()
    {
        $ocModification = $this->fakeoc_modificationData();
        $createdoc_modification = $this->ocModificationRepo->create($ocModification);
        $createdoc_modification = $createdoc_modification->toArray();
        $this->assertArrayHasKey('id', $createdoc_modification);
        $this->assertNotNull($createdoc_modification['id'], 'Created oc_modification must have id specified');
        $this->assertNotNull(oc_modification::find($createdoc_modification['id']), 'oc_modification with given id must be in DB');
        $this->assertModelData($ocModification, $createdoc_modification);
    }

    /**
     * @test read
     */
    public function testReadoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $dboc_modification = $this->ocModificationRepo->find($ocModification->id);
        $dboc_modification = $dboc_modification->toArray();
        $this->assertModelData($ocModification->toArray(), $dboc_modification);
    }

    /**
     * @test update
     */
    public function testUpdateoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $fakeoc_modification = $this->fakeoc_modificationData();
        $updatedoc_modification = $this->ocModificationRepo->update($fakeoc_modification, $ocModification->id);
        $this->assertModelData($fakeoc_modification, $updatedoc_modification->toArray());
        $dboc_modification = $this->ocModificationRepo->find($ocModification->id);
        $this->assertModelData($fakeoc_modification, $dboc_modification->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_modification()
    {
        $ocModification = $this->makeoc_modification();
        $resp = $this->ocModificationRepo->delete($ocModification->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_modification::find($ocModification->id), 'oc_modification should not exist in DB');
    }
}
