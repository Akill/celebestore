<?php

use App\Models\oc_setting;
use App\Repositories\oc_settingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_settingRepositoryTest extends TestCase
{
    use Makeoc_settingTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_settingRepository
     */
    protected $ocSettingRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocSettingRepo = App::make(oc_settingRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_setting()
    {
        $ocSetting = $this->fakeoc_settingData();
        $createdoc_setting = $this->ocSettingRepo->create($ocSetting);
        $createdoc_setting = $createdoc_setting->toArray();
        $this->assertArrayHasKey('id', $createdoc_setting);
        $this->assertNotNull($createdoc_setting['id'], 'Created oc_setting must have id specified');
        $this->assertNotNull(oc_setting::find($createdoc_setting['id']), 'oc_setting with given id must be in DB');
        $this->assertModelData($ocSetting, $createdoc_setting);
    }

    /**
     * @test read
     */
    public function testReadoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $dboc_setting = $this->ocSettingRepo->find($ocSetting->id);
        $dboc_setting = $dboc_setting->toArray();
        $this->assertModelData($ocSetting->toArray(), $dboc_setting);
    }

    /**
     * @test update
     */
    public function testUpdateoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $fakeoc_setting = $this->fakeoc_settingData();
        $updatedoc_setting = $this->ocSettingRepo->update($fakeoc_setting, $ocSetting->id);
        $this->assertModelData($fakeoc_setting, $updatedoc_setting->toArray());
        $dboc_setting = $this->ocSettingRepo->find($ocSetting->id);
        $this->assertModelData($fakeoc_setting, $dboc_setting->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_setting()
    {
        $ocSetting = $this->makeoc_setting();
        $resp = $this->ocSettingRepo->delete($ocSetting->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_setting::find($ocSetting->id), 'oc_setting should not exist in DB');
    }
}
