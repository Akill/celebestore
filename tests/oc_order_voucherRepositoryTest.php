<?php

use App\Models\oc_order_voucher;
use App\Repositories\oc_order_voucherRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_order_voucherRepositoryTest extends TestCase
{
    use Makeoc_order_voucherTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_order_voucherRepository
     */
    protected $ocOrderVoucherRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOrderVoucherRepo = App::make(oc_order_voucherRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_order_voucher()
    {
        $ocOrderVoucher = $this->fakeoc_order_voucherData();
        $createdoc_order_voucher = $this->ocOrderVoucherRepo->create($ocOrderVoucher);
        $createdoc_order_voucher = $createdoc_order_voucher->toArray();
        $this->assertArrayHasKey('id', $createdoc_order_voucher);
        $this->assertNotNull($createdoc_order_voucher['id'], 'Created oc_order_voucher must have id specified');
        $this->assertNotNull(oc_order_voucher::find($createdoc_order_voucher['id']), 'oc_order_voucher with given id must be in DB');
        $this->assertModelData($ocOrderVoucher, $createdoc_order_voucher);
    }

    /**
     * @test read
     */
    public function testReadoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $dboc_order_voucher = $this->ocOrderVoucherRepo->find($ocOrderVoucher->id);
        $dboc_order_voucher = $dboc_order_voucher->toArray();
        $this->assertModelData($ocOrderVoucher->toArray(), $dboc_order_voucher);
    }

    /**
     * @test update
     */
    public function testUpdateoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $fakeoc_order_voucher = $this->fakeoc_order_voucherData();
        $updatedoc_order_voucher = $this->ocOrderVoucherRepo->update($fakeoc_order_voucher, $ocOrderVoucher->id);
        $this->assertModelData($fakeoc_order_voucher, $updatedoc_order_voucher->toArray());
        $dboc_order_voucher = $this->ocOrderVoucherRepo->find($ocOrderVoucher->id);
        $this->assertModelData($fakeoc_order_voucher, $dboc_order_voucher->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_order_voucher()
    {
        $ocOrderVoucher = $this->makeoc_order_voucher();
        $resp = $this->ocOrderVoucherRepo->delete($ocOrderVoucher->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_order_voucher::find($ocOrderVoucher->id), 'oc_order_voucher should not exist in DB');
    }
}
