<?php

use App\Models\oc_weight_class_description;
use App\Repositories\oc_weight_class_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_weight_class_descriptionRepositoryTest extends TestCase
{
    use Makeoc_weight_class_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_weight_class_descriptionRepository
     */
    protected $ocWeightClassDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocWeightClassDescriptionRepo = App::make(oc_weight_class_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->fakeoc_weight_class_descriptionData();
        $createdoc_weight_class_description = $this->ocWeightClassDescriptionRepo->create($ocWeightClassDescription);
        $createdoc_weight_class_description = $createdoc_weight_class_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_weight_class_description);
        $this->assertNotNull($createdoc_weight_class_description['id'], 'Created oc_weight_class_description must have id specified');
        $this->assertNotNull(oc_weight_class_description::find($createdoc_weight_class_description['id']), 'oc_weight_class_description with given id must be in DB');
        $this->assertModelData($ocWeightClassDescription, $createdoc_weight_class_description);
    }

    /**
     * @test read
     */
    public function testReadoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $dboc_weight_class_description = $this->ocWeightClassDescriptionRepo->find($ocWeightClassDescription->id);
        $dboc_weight_class_description = $dboc_weight_class_description->toArray();
        $this->assertModelData($ocWeightClassDescription->toArray(), $dboc_weight_class_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $fakeoc_weight_class_description = $this->fakeoc_weight_class_descriptionData();
        $updatedoc_weight_class_description = $this->ocWeightClassDescriptionRepo->update($fakeoc_weight_class_description, $ocWeightClassDescription->id);
        $this->assertModelData($fakeoc_weight_class_description, $updatedoc_weight_class_description->toArray());
        $dboc_weight_class_description = $this->ocWeightClassDescriptionRepo->find($ocWeightClassDescription->id);
        $this->assertModelData($fakeoc_weight_class_description, $dboc_weight_class_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_weight_class_description()
    {
        $ocWeightClassDescription = $this->makeoc_weight_class_description();
        $resp = $this->ocWeightClassDescriptionRepo->delete($ocWeightClassDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_weight_class_description::find($ocWeightClassDescription->id), 'oc_weight_class_description should not exist in DB');
    }
}
