<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_value_descriptionApiTest extends TestCase
{
    use Makeoc_option_value_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_option_value_description()
    {
        $ocOptionValueDescription = $this->fakeoc_option_value_descriptionData();
        $this->json('POST', '/api/v1/ocOptionValueDescriptions', $ocOptionValueDescription);

        $this->assertApiResponse($ocOptionValueDescription);
    }

    /**
     * @test
     */
    public function testReadoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $this->json('GET', '/api/v1/ocOptionValueDescriptions/'.$ocOptionValueDescription->id);

        $this->assertApiResponse($ocOptionValueDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $editedoc_option_value_description = $this->fakeoc_option_value_descriptionData();

        $this->json('PUT', '/api/v1/ocOptionValueDescriptions/'.$ocOptionValueDescription->id, $editedoc_option_value_description);

        $this->assertApiResponse($editedoc_option_value_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_option_value_description()
    {
        $ocOptionValueDescription = $this->makeoc_option_value_description();
        $this->json('DELETE', '/api/v1/ocOptionValueDescriptions/'.$ocOptionValueDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocOptionValueDescriptions/'.$ocOptionValueDescription->id);

        $this->assertResponseStatus(404);
    }
}
