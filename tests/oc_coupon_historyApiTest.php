<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_coupon_historyApiTest extends TestCase
{
    use Makeoc_coupon_historyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_coupon_history()
    {
        $ocCouponHistory = $this->fakeoc_coupon_historyData();
        $this->json('POST', '/api/v1/ocCouponHistories', $ocCouponHistory);

        $this->assertApiResponse($ocCouponHistory);
    }

    /**
     * @test
     */
    public function testReadoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $this->json('GET', '/api/v1/ocCouponHistories/'.$ocCouponHistory->id);

        $this->assertApiResponse($ocCouponHistory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $editedoc_coupon_history = $this->fakeoc_coupon_historyData();

        $this->json('PUT', '/api/v1/ocCouponHistories/'.$ocCouponHistory->id, $editedoc_coupon_history);

        $this->assertApiResponse($editedoc_coupon_history);
    }

    /**
     * @test
     */
    public function testDeleteoc_coupon_history()
    {
        $ocCouponHistory = $this->makeoc_coupon_history();
        $this->json('DELETE', '/api/v1/ocCouponHistories/'.$ocCouponHistory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCouponHistories/'.$ocCouponHistory->id);

        $this->assertResponseStatus(404);
    }
}
