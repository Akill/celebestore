<?php

use App\Models\oc_layout;
use App\Repositories\oc_layoutRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_layoutRepositoryTest extends TestCase
{
    use Makeoc_layoutTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_layoutRepository
     */
    protected $ocLayoutRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocLayoutRepo = App::make(oc_layoutRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_layout()
    {
        $ocLayout = $this->fakeoc_layoutData();
        $createdoc_layout = $this->ocLayoutRepo->create($ocLayout);
        $createdoc_layout = $createdoc_layout->toArray();
        $this->assertArrayHasKey('id', $createdoc_layout);
        $this->assertNotNull($createdoc_layout['id'], 'Created oc_layout must have id specified');
        $this->assertNotNull(oc_layout::find($createdoc_layout['id']), 'oc_layout with given id must be in DB');
        $this->assertModelData($ocLayout, $createdoc_layout);
    }

    /**
     * @test read
     */
    public function testReadoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $dboc_layout = $this->ocLayoutRepo->find($ocLayout->id);
        $dboc_layout = $dboc_layout->toArray();
        $this->assertModelData($ocLayout->toArray(), $dboc_layout);
    }

    /**
     * @test update
     */
    public function testUpdateoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $fakeoc_layout = $this->fakeoc_layoutData();
        $updatedoc_layout = $this->ocLayoutRepo->update($fakeoc_layout, $ocLayout->id);
        $this->assertModelData($fakeoc_layout, $updatedoc_layout->toArray());
        $dboc_layout = $this->ocLayoutRepo->find($ocLayout->id);
        $this->assertModelData($fakeoc_layout, $dboc_layout->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_layout()
    {
        $ocLayout = $this->makeoc_layout();
        $resp = $this->ocLayoutRepo->delete($ocLayout->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_layout::find($ocLayout->id), 'oc_layout should not exist in DB');
    }
}
