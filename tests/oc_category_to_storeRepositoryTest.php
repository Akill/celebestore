<?php

use App\Models\oc_category_to_store;
use App\Repositories\oc_category_to_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_to_storeRepositoryTest extends TestCase
{
    use Makeoc_category_to_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_category_to_storeRepository
     */
    protected $ocCategoryToStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryToStoreRepo = App::make(oc_category_to_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category_to_store()
    {
        $ocCategoryToStore = $this->fakeoc_category_to_storeData();
        $createdoc_category_to_store = $this->ocCategoryToStoreRepo->create($ocCategoryToStore);
        $createdoc_category_to_store = $createdoc_category_to_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_category_to_store);
        $this->assertNotNull($createdoc_category_to_store['id'], 'Created oc_category_to_store must have id specified');
        $this->assertNotNull(oc_category_to_store::find($createdoc_category_to_store['id']), 'oc_category_to_store with given id must be in DB');
        $this->assertModelData($ocCategoryToStore, $createdoc_category_to_store);
    }

    /**
     * @test read
     */
    public function testReadoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $dboc_category_to_store = $this->ocCategoryToStoreRepo->find($ocCategoryToStore->id);
        $dboc_category_to_store = $dboc_category_to_store->toArray();
        $this->assertModelData($ocCategoryToStore->toArray(), $dboc_category_to_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $fakeoc_category_to_store = $this->fakeoc_category_to_storeData();
        $updatedoc_category_to_store = $this->ocCategoryToStoreRepo->update($fakeoc_category_to_store, $ocCategoryToStore->id);
        $this->assertModelData($fakeoc_category_to_store, $updatedoc_category_to_store->toArray());
        $dboc_category_to_store = $this->ocCategoryToStoreRepo->find($ocCategoryToStore->id);
        $this->assertModelData($fakeoc_category_to_store, $dboc_category_to_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category_to_store()
    {
        $ocCategoryToStore = $this->makeoc_category_to_store();
        $resp = $this->ocCategoryToStoreRepo->delete($ocCategoryToStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category_to_store::find($ocCategoryToStore->id), 'oc_category_to_store should not exist in DB');
    }
}
