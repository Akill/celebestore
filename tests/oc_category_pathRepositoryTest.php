<?php

use App\Models\oc_category_path;
use App\Repositories\oc_category_pathRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_category_pathRepositoryTest extends TestCase
{
    use Makeoc_category_pathTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_category_pathRepository
     */
    protected $ocCategoryPathRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCategoryPathRepo = App::make(oc_category_pathRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_category_path()
    {
        $ocCategoryPath = $this->fakeoc_category_pathData();
        $createdoc_category_path = $this->ocCategoryPathRepo->create($ocCategoryPath);
        $createdoc_category_path = $createdoc_category_path->toArray();
        $this->assertArrayHasKey('id', $createdoc_category_path);
        $this->assertNotNull($createdoc_category_path['id'], 'Created oc_category_path must have id specified');
        $this->assertNotNull(oc_category_path::find($createdoc_category_path['id']), 'oc_category_path with given id must be in DB');
        $this->assertModelData($ocCategoryPath, $createdoc_category_path);
    }

    /**
     * @test read
     */
    public function testReadoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $dboc_category_path = $this->ocCategoryPathRepo->find($ocCategoryPath->id);
        $dboc_category_path = $dboc_category_path->toArray();
        $this->assertModelData($ocCategoryPath->toArray(), $dboc_category_path);
    }

    /**
     * @test update
     */
    public function testUpdateoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $fakeoc_category_path = $this->fakeoc_category_pathData();
        $updatedoc_category_path = $this->ocCategoryPathRepo->update($fakeoc_category_path, $ocCategoryPath->id);
        $this->assertModelData($fakeoc_category_path, $updatedoc_category_path->toArray());
        $dboc_category_path = $this->ocCategoryPathRepo->find($ocCategoryPath->id);
        $this->assertModelData($fakeoc_category_path, $dboc_category_path->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_category_path()
    {
        $ocCategoryPath = $this->makeoc_category_path();
        $resp = $this->ocCategoryPathRepo->delete($ocCategoryPath->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_category_path::find($ocCategoryPath->id), 'oc_category_path should not exist in DB');
    }
}
