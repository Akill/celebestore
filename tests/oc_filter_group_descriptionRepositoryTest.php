<?php

use App\Models\oc_filter_group_description;
use App\Repositories\oc_filter_group_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_group_descriptionRepositoryTest extends TestCase
{
    use Makeoc_filter_group_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_filter_group_descriptionRepository
     */
    protected $ocFilterGroupDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocFilterGroupDescriptionRepo = App::make(oc_filter_group_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->fakeoc_filter_group_descriptionData();
        $createdoc_filter_group_description = $this->ocFilterGroupDescriptionRepo->create($ocFilterGroupDescription);
        $createdoc_filter_group_description = $createdoc_filter_group_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_filter_group_description);
        $this->assertNotNull($createdoc_filter_group_description['id'], 'Created oc_filter_group_description must have id specified');
        $this->assertNotNull(oc_filter_group_description::find($createdoc_filter_group_description['id']), 'oc_filter_group_description with given id must be in DB');
        $this->assertModelData($ocFilterGroupDescription, $createdoc_filter_group_description);
    }

    /**
     * @test read
     */
    public function testReadoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $dboc_filter_group_description = $this->ocFilterGroupDescriptionRepo->find($ocFilterGroupDescription->id);
        $dboc_filter_group_description = $dboc_filter_group_description->toArray();
        $this->assertModelData($ocFilterGroupDescription->toArray(), $dboc_filter_group_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $fakeoc_filter_group_description = $this->fakeoc_filter_group_descriptionData();
        $updatedoc_filter_group_description = $this->ocFilterGroupDescriptionRepo->update($fakeoc_filter_group_description, $ocFilterGroupDescription->id);
        $this->assertModelData($fakeoc_filter_group_description, $updatedoc_filter_group_description->toArray());
        $dboc_filter_group_description = $this->ocFilterGroupDescriptionRepo->find($ocFilterGroupDescription->id);
        $this->assertModelData($fakeoc_filter_group_description, $dboc_filter_group_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_filter_group_description()
    {
        $ocFilterGroupDescription = $this->makeoc_filter_group_description();
        $resp = $this->ocFilterGroupDescriptionRepo->delete($ocFilterGroupDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_filter_group_description::find($ocFilterGroupDescription->id), 'oc_filter_group_description should not exist in DB');
    }
}
