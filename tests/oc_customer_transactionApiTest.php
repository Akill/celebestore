<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_customer_transactionApiTest extends TestCase
{
    use Makeoc_customer_transactionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->fakeoc_customer_transactionData();
        $this->json('POST', '/api/v1/ocCustomerTransactions', $ocCustomerTransaction);

        $this->assertApiResponse($ocCustomerTransaction);
    }

    /**
     * @test
     */
    public function testReadoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $this->json('GET', '/api/v1/ocCustomerTransactions/'.$ocCustomerTransaction->id);

        $this->assertApiResponse($ocCustomerTransaction->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $editedoc_customer_transaction = $this->fakeoc_customer_transactionData();

        $this->json('PUT', '/api/v1/ocCustomerTransactions/'.$ocCustomerTransaction->id, $editedoc_customer_transaction);

        $this->assertApiResponse($editedoc_customer_transaction);
    }

    /**
     * @test
     */
    public function testDeleteoc_customer_transaction()
    {
        $ocCustomerTransaction = $this->makeoc_customer_transaction();
        $this->json('DELETE', '/api/v1/ocCustomerTransactions/'.$ocCustomerTransaction->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocCustomerTransactions/'.$ocCustomerTransaction->id);

        $this->assertResponseStatus(404);
    }
}
