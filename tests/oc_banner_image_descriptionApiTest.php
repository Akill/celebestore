<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_banner_image_descriptionApiTest extends TestCase
{
    use Makeoc_banner_image_descriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->fakeoc_banner_image_descriptionData();
        $this->json('POST', '/api/v1/ocBannerImageDescriptions', $ocBannerImageDescription);

        $this->assertApiResponse($ocBannerImageDescription);
    }

    /**
     * @test
     */
    public function testReadoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $this->json('GET', '/api/v1/ocBannerImageDescriptions/'.$ocBannerImageDescription->id);

        $this->assertApiResponse($ocBannerImageDescription->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $editedoc_banner_image_description = $this->fakeoc_banner_image_descriptionData();

        $this->json('PUT', '/api/v1/ocBannerImageDescriptions/'.$ocBannerImageDescription->id, $editedoc_banner_image_description);

        $this->assertApiResponse($editedoc_banner_image_description);
    }

    /**
     * @test
     */
    public function testDeleteoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $this->json('DELETE', '/api/v1/ocBannerImageDescriptions/'.$ocBannerImageDescription->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocBannerImageDescriptions/'.$ocBannerImageDescription->id);

        $this->assertResponseStatus(404);
    }
}
