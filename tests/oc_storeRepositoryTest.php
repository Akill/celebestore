<?php

use App\Models\oc_store;
use App\Repositories\oc_storeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_storeRepositoryTest extends TestCase
{
    use Makeoc_storeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_storeRepository
     */
    protected $ocStoreRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocStoreRepo = App::make(oc_storeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_store()
    {
        $ocStore = $this->fakeoc_storeData();
        $createdoc_store = $this->ocStoreRepo->create($ocStore);
        $createdoc_store = $createdoc_store->toArray();
        $this->assertArrayHasKey('id', $createdoc_store);
        $this->assertNotNull($createdoc_store['id'], 'Created oc_store must have id specified');
        $this->assertNotNull(oc_store::find($createdoc_store['id']), 'oc_store with given id must be in DB');
        $this->assertModelData($ocStore, $createdoc_store);
    }

    /**
     * @test read
     */
    public function testReadoc_store()
    {
        $ocStore = $this->makeoc_store();
        $dboc_store = $this->ocStoreRepo->find($ocStore->id);
        $dboc_store = $dboc_store->toArray();
        $this->assertModelData($ocStore->toArray(), $dboc_store);
    }

    /**
     * @test update
     */
    public function testUpdateoc_store()
    {
        $ocStore = $this->makeoc_store();
        $fakeoc_store = $this->fakeoc_storeData();
        $updatedoc_store = $this->ocStoreRepo->update($fakeoc_store, $ocStore->id);
        $this->assertModelData($fakeoc_store, $updatedoc_store->toArray());
        $dboc_store = $this->ocStoreRepo->find($ocStore->id);
        $this->assertModelData($fakeoc_store, $dboc_store->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_store()
    {
        $ocStore = $this->makeoc_store();
        $resp = $this->ocStoreRepo->delete($ocStore->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_store::find($ocStore->id), 'oc_store should not exist in DB');
    }
}
