<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucher_themeApiTest extends TestCase
{
    use Makeoc_voucher_themeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_voucher_theme()
    {
        $ocVoucherTheme = $this->fakeoc_voucher_themeData();
        $this->json('POST', '/api/v1/ocVoucherThemes', $ocVoucherTheme);

        $this->assertApiResponse($ocVoucherTheme);
    }

    /**
     * @test
     */
    public function testReadoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $this->json('GET', '/api/v1/ocVoucherThemes/'.$ocVoucherTheme->id);

        $this->assertApiResponse($ocVoucherTheme->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $editedoc_voucher_theme = $this->fakeoc_voucher_themeData();

        $this->json('PUT', '/api/v1/ocVoucherThemes/'.$ocVoucherTheme->id, $editedoc_voucher_theme);

        $this->assertApiResponse($editedoc_voucher_theme);
    }

    /**
     * @test
     */
    public function testDeleteoc_voucher_theme()
    {
        $ocVoucherTheme = $this->makeoc_voucher_theme();
        $this->json('DELETE', '/api/v1/ocVoucherThemes/'.$ocVoucherTheme->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocVoucherThemes/'.$ocVoucherTheme->id);

        $this->assertResponseStatus(404);
    }
}
