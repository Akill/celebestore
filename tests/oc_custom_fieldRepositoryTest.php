<?php

use App\Models\oc_custom_field;
use App\Repositories\oc_custom_fieldRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_custom_fieldRepositoryTest extends TestCase
{
    use Makeoc_custom_fieldTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_custom_fieldRepository
     */
    protected $ocCustomFieldRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocCustomFieldRepo = App::make(oc_custom_fieldRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_custom_field()
    {
        $ocCustomField = $this->fakeoc_custom_fieldData();
        $createdoc_custom_field = $this->ocCustomFieldRepo->create($ocCustomField);
        $createdoc_custom_field = $createdoc_custom_field->toArray();
        $this->assertArrayHasKey('id', $createdoc_custom_field);
        $this->assertNotNull($createdoc_custom_field['id'], 'Created oc_custom_field must have id specified');
        $this->assertNotNull(oc_custom_field::find($createdoc_custom_field['id']), 'oc_custom_field with given id must be in DB');
        $this->assertModelData($ocCustomField, $createdoc_custom_field);
    }

    /**
     * @test read
     */
    public function testReadoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $dboc_custom_field = $this->ocCustomFieldRepo->find($ocCustomField->id);
        $dboc_custom_field = $dboc_custom_field->toArray();
        $this->assertModelData($ocCustomField->toArray(), $dboc_custom_field);
    }

    /**
     * @test update
     */
    public function testUpdateoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $fakeoc_custom_field = $this->fakeoc_custom_fieldData();
        $updatedoc_custom_field = $this->ocCustomFieldRepo->update($fakeoc_custom_field, $ocCustomField->id);
        $this->assertModelData($fakeoc_custom_field, $updatedoc_custom_field->toArray());
        $dboc_custom_field = $this->ocCustomFieldRepo->find($ocCustomField->id);
        $this->assertModelData($fakeoc_custom_field, $dboc_custom_field->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_custom_field()
    {
        $ocCustomField = $this->makeoc_custom_field();
        $resp = $this->ocCustomFieldRepo->delete($ocCustomField->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_custom_field::find($ocCustomField->id), 'oc_custom_field should not exist in DB');
    }
}
