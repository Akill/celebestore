<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_tax_rateApiTest extends TestCase
{
    use Makeoc_tax_rateTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_tax_rate()
    {
        $ocTaxRate = $this->fakeoc_tax_rateData();
        $this->json('POST', '/api/v1/ocTaxRates', $ocTaxRate);

        $this->assertApiResponse($ocTaxRate);
    }

    /**
     * @test
     */
    public function testReadoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $this->json('GET', '/api/v1/ocTaxRates/'.$ocTaxRate->id);

        $this->assertApiResponse($ocTaxRate->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $editedoc_tax_rate = $this->fakeoc_tax_rateData();

        $this->json('PUT', '/api/v1/ocTaxRates/'.$ocTaxRate->id, $editedoc_tax_rate);

        $this->assertApiResponse($editedoc_tax_rate);
    }

    /**
     * @test
     */
    public function testDeleteoc_tax_rate()
    {
        $ocTaxRate = $this->makeoc_tax_rate();
        $this->json('DELETE', '/api/v1/ocTaxRates/'.$ocTaxRate->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocTaxRates/'.$ocTaxRate->id);

        $this->assertResponseStatus(404);
    }
}
