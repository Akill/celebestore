<?php

use App\Models\oc_option_value;
use App\Repositories\oc_option_valueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_option_valueRepositoryTest extends TestCase
{
    use Makeoc_option_valueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_option_valueRepository
     */
    protected $ocOptionValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocOptionValueRepo = App::make(oc_option_valueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_option_value()
    {
        $ocOptionValue = $this->fakeoc_option_valueData();
        $createdoc_option_value = $this->ocOptionValueRepo->create($ocOptionValue);
        $createdoc_option_value = $createdoc_option_value->toArray();
        $this->assertArrayHasKey('id', $createdoc_option_value);
        $this->assertNotNull($createdoc_option_value['id'], 'Created oc_option_value must have id specified');
        $this->assertNotNull(oc_option_value::find($createdoc_option_value['id']), 'oc_option_value with given id must be in DB');
        $this->assertModelData($ocOptionValue, $createdoc_option_value);
    }

    /**
     * @test read
     */
    public function testReadoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $dboc_option_value = $this->ocOptionValueRepo->find($ocOptionValue->id);
        $dboc_option_value = $dboc_option_value->toArray();
        $this->assertModelData($ocOptionValue->toArray(), $dboc_option_value);
    }

    /**
     * @test update
     */
    public function testUpdateoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $fakeoc_option_value = $this->fakeoc_option_valueData();
        $updatedoc_option_value = $this->ocOptionValueRepo->update($fakeoc_option_value, $ocOptionValue->id);
        $this->assertModelData($fakeoc_option_value, $updatedoc_option_value->toArray());
        $dboc_option_value = $this->ocOptionValueRepo->find($ocOptionValue->id);
        $this->assertModelData($fakeoc_option_value, $dboc_option_value->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_option_value()
    {
        $ocOptionValue = $this->makeoc_option_value();
        $resp = $this->ocOptionValueRepo->delete($ocOptionValue->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_option_value::find($ocOptionValue->id), 'oc_option_value should not exist in DB');
    }
}
