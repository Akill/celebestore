<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_return_statusApiTest extends TestCase
{
    use Makeoc_return_statusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateoc_return_status()
    {
        $ocReturnStatus = $this->fakeoc_return_statusData();
        $this->json('POST', '/api/v1/ocReturnStatuses', $ocReturnStatus);

        $this->assertApiResponse($ocReturnStatus);
    }

    /**
     * @test
     */
    public function testReadoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $this->json('GET', '/api/v1/ocReturnStatuses/'.$ocReturnStatus->id);

        $this->assertApiResponse($ocReturnStatus->toArray());
    }

    /**
     * @test
     */
    public function testUpdateoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $editedoc_return_status = $this->fakeoc_return_statusData();

        $this->json('PUT', '/api/v1/ocReturnStatuses/'.$ocReturnStatus->id, $editedoc_return_status);

        $this->assertApiResponse($editedoc_return_status);
    }

    /**
     * @test
     */
    public function testDeleteoc_return_status()
    {
        $ocReturnStatus = $this->makeoc_return_status();
        $this->json('DELETE', '/api/v1/ocReturnStatuses/'.$ocReturnStatus->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ocReturnStatuses/'.$ocReturnStatus->id);

        $this->assertResponseStatus(404);
    }
}
