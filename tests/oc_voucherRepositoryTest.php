<?php

use App\Models\oc_voucher;
use App\Repositories\oc_voucherRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_voucherRepositoryTest extends TestCase
{
    use Makeoc_voucherTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_voucherRepository
     */
    protected $ocVoucherRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocVoucherRepo = App::make(oc_voucherRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_voucher()
    {
        $ocVoucher = $this->fakeoc_voucherData();
        $createdoc_voucher = $this->ocVoucherRepo->create($ocVoucher);
        $createdoc_voucher = $createdoc_voucher->toArray();
        $this->assertArrayHasKey('id', $createdoc_voucher);
        $this->assertNotNull($createdoc_voucher['id'], 'Created oc_voucher must have id specified');
        $this->assertNotNull(oc_voucher::find($createdoc_voucher['id']), 'oc_voucher with given id must be in DB');
        $this->assertModelData($ocVoucher, $createdoc_voucher);
    }

    /**
     * @test read
     */
    public function testReadoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $dboc_voucher = $this->ocVoucherRepo->find($ocVoucher->id);
        $dboc_voucher = $dboc_voucher->toArray();
        $this->assertModelData($ocVoucher->toArray(), $dboc_voucher);
    }

    /**
     * @test update
     */
    public function testUpdateoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $fakeoc_voucher = $this->fakeoc_voucherData();
        $updatedoc_voucher = $this->ocVoucherRepo->update($fakeoc_voucher, $ocVoucher->id);
        $this->assertModelData($fakeoc_voucher, $updatedoc_voucher->toArray());
        $dboc_voucher = $this->ocVoucherRepo->find($ocVoucher->id);
        $this->assertModelData($fakeoc_voucher, $dboc_voucher->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_voucher()
    {
        $ocVoucher = $this->makeoc_voucher();
        $resp = $this->ocVoucherRepo->delete($ocVoucher->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_voucher::find($ocVoucher->id), 'oc_voucher should not exist in DB');
    }
}
