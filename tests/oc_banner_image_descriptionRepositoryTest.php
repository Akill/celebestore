<?php

use App\Models\oc_banner_image_description;
use App\Repositories\oc_banner_image_descriptionRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_banner_image_descriptionRepositoryTest extends TestCase
{
    use Makeoc_banner_image_descriptionTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_banner_image_descriptionRepository
     */
    protected $ocBannerImageDescriptionRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocBannerImageDescriptionRepo = App::make(oc_banner_image_descriptionRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->fakeoc_banner_image_descriptionData();
        $createdoc_banner_image_description = $this->ocBannerImageDescriptionRepo->create($ocBannerImageDescription);
        $createdoc_banner_image_description = $createdoc_banner_image_description->toArray();
        $this->assertArrayHasKey('id', $createdoc_banner_image_description);
        $this->assertNotNull($createdoc_banner_image_description['id'], 'Created oc_banner_image_description must have id specified');
        $this->assertNotNull(oc_banner_image_description::find($createdoc_banner_image_description['id']), 'oc_banner_image_description with given id must be in DB');
        $this->assertModelData($ocBannerImageDescription, $createdoc_banner_image_description);
    }

    /**
     * @test read
     */
    public function testReadoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $dboc_banner_image_description = $this->ocBannerImageDescriptionRepo->find($ocBannerImageDescription->id);
        $dboc_banner_image_description = $dboc_banner_image_description->toArray();
        $this->assertModelData($ocBannerImageDescription->toArray(), $dboc_banner_image_description);
    }

    /**
     * @test update
     */
    public function testUpdateoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $fakeoc_banner_image_description = $this->fakeoc_banner_image_descriptionData();
        $updatedoc_banner_image_description = $this->ocBannerImageDescriptionRepo->update($fakeoc_banner_image_description, $ocBannerImageDescription->id);
        $this->assertModelData($fakeoc_banner_image_description, $updatedoc_banner_image_description->toArray());
        $dboc_banner_image_description = $this->ocBannerImageDescriptionRepo->find($ocBannerImageDescription->id);
        $this->assertModelData($fakeoc_banner_image_description, $dboc_banner_image_description->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_banner_image_description()
    {
        $ocBannerImageDescription = $this->makeoc_banner_image_description();
        $resp = $this->ocBannerImageDescriptionRepo->delete($ocBannerImageDescription->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_banner_image_description::find($ocBannerImageDescription->id), 'oc_banner_image_description should not exist in DB');
    }
}
