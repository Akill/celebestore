<?php

use App\Models\oc_filter_group;
use App\Repositories\oc_filter_groupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class oc_filter_groupRepositoryTest extends TestCase
{
    use Makeoc_filter_groupTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var oc_filter_groupRepository
     */
    protected $ocFilterGroupRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ocFilterGroupRepo = App::make(oc_filter_groupRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateoc_filter_group()
    {
        $ocFilterGroup = $this->fakeoc_filter_groupData();
        $createdoc_filter_group = $this->ocFilterGroupRepo->create($ocFilterGroup);
        $createdoc_filter_group = $createdoc_filter_group->toArray();
        $this->assertArrayHasKey('id', $createdoc_filter_group);
        $this->assertNotNull($createdoc_filter_group['id'], 'Created oc_filter_group must have id specified');
        $this->assertNotNull(oc_filter_group::find($createdoc_filter_group['id']), 'oc_filter_group with given id must be in DB');
        $this->assertModelData($ocFilterGroup, $createdoc_filter_group);
    }

    /**
     * @test read
     */
    public function testReadoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $dboc_filter_group = $this->ocFilterGroupRepo->find($ocFilterGroup->id);
        $dboc_filter_group = $dboc_filter_group->toArray();
        $this->assertModelData($ocFilterGroup->toArray(), $dboc_filter_group);
    }

    /**
     * @test update
     */
    public function testUpdateoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $fakeoc_filter_group = $this->fakeoc_filter_groupData();
        $updatedoc_filter_group = $this->ocFilterGroupRepo->update($fakeoc_filter_group, $ocFilterGroup->id);
        $this->assertModelData($fakeoc_filter_group, $updatedoc_filter_group->toArray());
        $dboc_filter_group = $this->ocFilterGroupRepo->find($ocFilterGroup->id);
        $this->assertModelData($fakeoc_filter_group, $dboc_filter_group->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteoc_filter_group()
    {
        $ocFilterGroup = $this->makeoc_filter_group();
        $resp = $this->ocFilterGroupRepo->delete($ocFilterGroup->id);
        $this->assertTrue($resp);
        $this->assertNull(oc_filter_group::find($ocFilterGroup->id), 'oc_filter_group should not exist in DB');
    }
}
