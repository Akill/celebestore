-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 04 Sep 2017 pada 22.54
-- Versi Server: 10.0.31-MariaDB-0ubuntu0.16.04.2
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_address`
--

CREATE TABLE `oc_address` (
  `id` int(11) NOT NULL,
  `sub_district_id` int(4) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_address`
--

INSERT INTO `oc_address` (`id`, `sub_district_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 3996, 3, 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 24, 284, '', '0000-00-00', '0000-00-00', NULL),
(2, 3588, 2, 'amin', 'udin', '', 'jl tinumbu', '', 'Bontoala', '90031', 28, 254, '', '0000-00-00', '0000-00-00', NULL),
(4, 0, 4, 'zuhry', 'culli', 'da', 'jl. tinumbu no. 48 f makassar', '', 'Awangpone', '904321', 28, 87, 'a:0:{}', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_affiliate`
--

CREATE TABLE `oc_affiliate` (
  `id` int(11) NOT NULL,
  `sub_district_id` int(4) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_affiliate_activity`
--

CREATE TABLE `oc_affiliate_activity` (
  `id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_affiliate_login`
--

CREATE TABLE `oc_affiliate_login` (
  `id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_affiliate_transaction`
--

CREATE TABLE `oc_affiliate_transaction` (
  `id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_api`
--

CREATE TABLE `oc_api` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `password` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_api`
--

INSERT INTO `oc_api` (`id`, `username`, `firstname`, `lastname`, `password`, `status`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '', '', '4DktLRugd46jmBTkBrZ70knmQvvDGUIlK4diLoCknxZdP0nlSPLhtWpAsb7VZekJAfELPpJnV5HdUrEHeUJLjwamd7iK11bGh3pZtMfjGyFdsbEEUfNcFKdRNCDvxfL6zS2A7wZydmTXvQcoO3lNHGsRUgGqElTXj7RDkfqFKQqHvC5BYVDtNsbxwzxVbbl57MrFIgLozRlNheSgEDOSHSvIO68eL8YIW6d20PovnDiJZCYPPHAEza20qkEU1v5G', 1, '2017-01-30 16:43:09', '2017-02-02 04:49:47', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_attribute`
--

INSERT INTO `oc_attribute` (`id`, `attribute_group_id`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, '0000-00-00', '0000-00-00', NULL),
(2, 6, 5, '0000-00-00', '0000-00-00', NULL),
(3, 6, 3, '0000-00-00', '0000-00-00', NULL),
(4, 3, 1, '0000-00-00', '0000-00-00', NULL),
(5, 3, 2, '0000-00-00', '0000-00-00', NULL),
(6, 3, 3, '0000-00-00', '0000-00-00', NULL),
(7, 3, 4, '0000-00-00', '0000-00-00', NULL),
(8, 3, 5, '0000-00-00', '0000-00-00', NULL),
(9, 3, 6, '0000-00-00', '0000-00-00', NULL),
(10, 3, 7, '0000-00-00', '0000-00-00', NULL),
(11, 3, 8, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`id`, `attribute_id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 'Description', '0000-00-00', '0000-00-00', NULL),
(2, 2, 2, 'No. of Cores', '0000-00-00', '0000-00-00', NULL),
(4, 4, 2, 'test 1', '0000-00-00', '0000-00-00', NULL),
(5, 5, 2, 'test 2', '0000-00-00', '0000-00-00', NULL),
(6, 6, 2, 'test 3', '0000-00-00', '0000-00-00', NULL),
(7, 7, 2, 'test 4', '0000-00-00', '0000-00-00', NULL),
(8, 8, 2, 'test 5', '0000-00-00', '0000-00-00', NULL),
(9, 9, 2, 'test 6', '0000-00-00', '0000-00-00', NULL),
(10, 10, 2, 'test 7', '0000-00-00', '0000-00-00', NULL),
(11, 11, 2, 'test 8', '0000-00-00', '0000-00-00', NULL),
(3, 3, 2, 'Clockspeed', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`id`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 2, '0000-00-00', '0000-00-00', NULL),
(4, 1, '0000-00-00', '0000-00-00', NULL),
(5, 3, '0000-00-00', '0000-00-00', NULL),
(6, 4, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 2, 'Memory', '0000-00-00', '0000-00-00', NULL),
(4, 2, 'Technical', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Motherboard', '0000-00-00', '0000-00-00', NULL),
(6, 2, 'Processor', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner`
--

CREATE TABLE `oc_banner` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner`
--

INSERT INTO `oc_banner` (`id`, `name`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 'HP Products', 1, '0000-00-00', '0000-00-00', NULL),
(7, 'Home Page Slideshow', 1, '0000-00-00', '0000-00-00', NULL),
(8, 'Manufacturers', 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`id`, `banner_id`, `link`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(79, 7, 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/demo/banners/iPhone6.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(87, 6, 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(106, 8, '', 'catalog/demo/manufacturer/dell.png', 0, '0000-00-00', '0000-00-00', NULL),
(105, 8, '', 'catalog/demo/manufacturer/harley.png', 0, '0000-00-00', '0000-00-00', NULL),
(104, 8, '', 'catalog/demo/manufacturer/canon.png', 0, '0000-00-00', '0000-00-00', NULL),
(103, 8, '', 'catalog/demo/manufacturer/burgerking.png', 0, '0000-00-00', '0000-00-00', NULL),
(102, 8, '', 'catalog/demo/manufacturer/cocacola.png', 0, '0000-00-00', '0000-00-00', NULL),
(101, 8, '', 'catalog/demo/manufacturer/sony.png', 0, '0000-00-00', '0000-00-00', NULL),
(80, 7, '', 'catalog/demo/banners/MacBookAir.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(100, 8, '', 'catalog/bkm.png', 0, '0000-00-00', '0000-00-00', NULL),
(99, 8, '', 'catalog/jk.png', 0, '0000-00-00', '0000-00-00', NULL),
(107, 8, '', 'catalog/demo/manufacturer/disney.png', 0, '0000-00-00', '0000-00-00', NULL),
(108, 8, '', 'catalog/demo/manufacturer/starbucks.png', 0, '0000-00-00', '0000-00-00', NULL),
(109, 8, '', 'catalog/demo/manufacturer/nintendo.png', 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_banner_image_description`
--

CREATE TABLE `oc_banner_image_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_banner_image_description`
--

INSERT INTO `oc_banner_image_description` (`id`, `language_id`, `banner_id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(109, 2, 8, 'Nintendo', '0000-00-00', '0000-00-00', NULL),
(108, 2, 8, 'Starbucks', '0000-00-00', '0000-00-00', NULL),
(79, 2, 7, 'iPhone 6', '0000-00-00', '0000-00-00', NULL),
(87, 2, 6, 'HP Banner', '0000-00-00', '0000-00-00', NULL),
(107, 2, 8, 'Disney', '0000-00-00', '0000-00-00', NULL),
(106, 2, 8, 'Dell', '0000-00-00', '0000-00-00', NULL),
(105, 2, 8, 'Harley Davidson', '0000-00-00', '0000-00-00', NULL),
(104, 2, 8, 'Canon', '0000-00-00', '0000-00-00', NULL),
(80, 2, 7, 'MacBookAir', '0000-00-00', '0000-00-00', NULL),
(103, 2, 8, 'Burger King', '0000-00-00', '0000-00-00', NULL),
(102, 2, 8, 'Coca Cola', '0000-00-00', '0000-00-00', NULL),
(101, 2, 8, 'Sony', '0000-00-00', '0000-00-00', NULL),
(100, 2, 8, 'RedBull', '0000-00-00', '0000-00-00', NULL),
(99, 2, 8, 'NFL', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category`
--

CREATE TABLE `oc_category` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category`
--

INSERT INTO `oc_category` (`id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(25, '', 0, 0, 1, 3, 1, '2009-01-31 01:04:25', '2017-02-04 09:25:03', '0000-00-00', '0000-00-00', NULL),
(27, '', 20, 0, 0, 2, 1, '2009-01-31 01:55:34', '2010-08-22 06:32:15', '0000-00-00', '0000-00-00', NULL),
(20, 'catalog/demo/compaq_presario.jpg', 0, 1, 1, 1, 1, '2009-01-05 21:49:43', '2017-01-30 19:26:08', '0000-00-00', '0000-00-00', NULL),
(24, '', 0, 1, 1, 5, 1, '2009-01-20 02:36:26', '2011-05-30 12:15:18', '0000-00-00', '0000-00-00', NULL),
(18, 'catalog/demo/hp_2.jpg', 0, 0, 0, 2, 1, '2009-01-05 21:49:15', '2017-02-04 09:25:56', '0000-00-00', '0000-00-00', NULL),
(17, '', 0, 1, 1, 4, 1, '2009-01-03 21:08:57', '2011-05-30 12:15:11', '0000-00-00', '0000-00-00', NULL),
(28, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:12', '2010-08-22 06:32:46', '0000-00-00', '0000-00-00', NULL),
(26, '', 20, 0, 0, 1, 1, '2009-01-31 01:55:14', '2010-08-22 06:31:45', '0000-00-00', '0000-00-00', NULL),
(29, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:37', '2010-08-22 06:32:39', '0000-00-00', '0000-00-00', NULL),
(30, '', 25, 0, 0, 1, 1, '2009-02-02 13:11:59', '2010-08-22 06:33:00', '0000-00-00', '0000-00-00', NULL),
(31, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:24', '2010-08-22 06:33:06', '0000-00-00', '0000-00-00', NULL),
(32, '', 25, 0, 0, 1, 1, '2009-02-03 14:17:34', '2010-08-22 06:33:12', '0000-00-00', '0000-00-00', NULL),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2011-05-30 12:15:25', '0000-00-00', '0000-00-00', NULL),
(34, 'catalog/demo/ipod_touch_4.jpg', 0, 1, 4, 7, 1, '2009-02-03 14:18:11', '2011-05-30 12:15:31', '0000-00-00', '0000-00-00', NULL),
(35, '', 28, 0, 0, 0, 1, '2010-09-17 10:06:48', '2010-09-18 14:02:42', '0000-00-00', '0000-00-00', NULL),
(36, '', 28, 0, 0, 0, 1, '2010-09-17 10:07:13', '2010-09-18 14:02:55', '0000-00-00', '0000-00-00', NULL),
(37, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:39', '2011-04-22 01:55:08', '0000-00-00', '0000-00-00', NULL),
(38, '', 34, 0, 0, 0, 1, '2010-09-18 14:03:51', '2010-09-18 14:03:51', '0000-00-00', '0000-00-00', NULL),
(39, '', 34, 0, 0, 0, 1, '2010-09-18 14:04:17', '2011-04-22 01:55:20', '0000-00-00', '0000-00-00', NULL),
(40, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:36', '2010-09-18 14:05:36', '0000-00-00', '0000-00-00', NULL),
(41, '', 34, 0, 0, 0, 1, '2010-09-18 14:05:49', '2011-04-22 01:55:30', '0000-00-00', '0000-00-00', NULL),
(42, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:34', '2010-11-07 20:31:04', '0000-00-00', '0000-00-00', NULL),
(43, '', 34, 0, 0, 0, 1, '2010-09-18 14:06:49', '2011-04-22 01:55:40', '0000-00-00', '0000-00-00', NULL),
(44, '', 34, 0, 0, 0, 1, '2010-09-21 15:39:21', '2010-11-07 20:30:55', '0000-00-00', '0000-00-00', NULL),
(45, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:16', '2011-04-26 08:52:11', '0000-00-00', '0000-00-00', NULL),
(46, '', 18, 0, 0, 0, 1, '2010-09-24 18:29:31', '2011-04-26 08:52:23', '0000-00-00', '0000-00-00', NULL),
(47, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:16', '2010-11-07 11:13:16', '0000-00-00', '0000-00-00', NULL),
(48, '', 34, 0, 0, 0, 1, '2010-11-07 11:13:33', '2010-11-07 11:13:33', '0000-00-00', '0000-00-00', NULL),
(49, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:04', '2010-11-07 11:14:04', '0000-00-00', '0000-00-00', NULL),
(50, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:23', '2011-04-22 01:16:01', '0000-00-00', '0000-00-00', NULL),
(51, '', 34, 0, 0, 0, 1, '2010-11-07 11:14:38', '2011-04-22 01:16:13', '0000-00-00', '0000-00-00', NULL),
(52, '', 34, 0, 0, 0, 1, '2010-11-07 11:16:09', '2011-04-22 01:54:57', '0000-00-00', '0000-00-00', NULL),
(53, '', 34, 0, 0, 0, 1, '2010-11-07 11:28:53', '2011-04-22 01:14:36', '0000-00-00', '0000-00-00', NULL),
(54, '', 34, 0, 0, 0, 1, '2010-11-07 11:29:16', '2011-04-22 01:16:50', '0000-00-00', '0000-00-00', NULL),
(55, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:32', '2010-11-08 10:31:32', '0000-00-00', '0000-00-00', NULL),
(56, '', 34, 0, 0, 0, 1, '2010-11-08 10:31:50', '2011-04-22 01:16:37', '0000-00-00', '0000-00-00', NULL),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2011-05-30 12:15:05', '0000-00-00', '0000-00-00', NULL),
(58, '', 52, 0, 0, 0, 1, '2011-05-08 13:44:16', '2011-05-08 13:44:16', '0000-00-00', '0000-00-00', NULL),
(59, '', 0, 0, 1, 0, 1, '2017-03-17 11:35:49', '2017-03-17 11:35:49', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `id` int(11) NOT NULL,
  `oc_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category_description`
--

INSERT INTO `oc_category_description` (`id`, `oc_category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(59, 0, 2, 'makanan', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'makanan', '', '', '0000-00-00', '0000-00-00', NULL),
(28, 0, 2, 'Monitors', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(33, 0, 2, 'Cameras', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(32, 0, 2, 'Web Cameras', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(31, 0, 2, 'Scanners', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(30, 0, 2, 'Printers', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(29, 0, 2, 'Mice and Trackballs', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(27, 0, 2, 'Mac', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(26, 0, 2, 'PC', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(17, 0, 2, 'Software', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(25, 0, 2, 'Components', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'Components', '', '', '0000-00-00', '0000-00-00', NULL),
(24, 0, 2, 'Phones &amp; PDAs', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(20, 0, 2, 'Oleh-oleh', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'oleh-oleh', 'Example of category description', '', '0000-00-00', '0000-00-00', NULL),
(45, 0, 2, 'Windows', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(46, 0, 2, 'Macs', '', '', '', '', '0000-00-00', '0000-00-00', NULL),
(57, 0, 2, 'Tablets', '', '', '', '', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category_path`
--

INSERT INTO `oc_category_path` (`id`, `path_id`, `level`, `created_at`, `updated_at`, `deleted_at`) VALUES
(25, 25, 0, '0000-00-00', '0000-00-00', NULL),
(28, 25, 0, '0000-00-00', '0000-00-00', NULL),
(28, 28, 1, '0000-00-00', '0000-00-00', NULL),
(35, 25, 0, '0000-00-00', '0000-00-00', NULL),
(35, 28, 1, '0000-00-00', '0000-00-00', NULL),
(35, 35, 2, '0000-00-00', '0000-00-00', NULL),
(36, 25, 0, '0000-00-00', '0000-00-00', NULL),
(36, 28, 1, '0000-00-00', '0000-00-00', NULL),
(36, 36, 2, '0000-00-00', '0000-00-00', NULL),
(29, 25, 0, '0000-00-00', '0000-00-00', NULL),
(29, 29, 1, '0000-00-00', '0000-00-00', NULL),
(30, 25, 0, '0000-00-00', '0000-00-00', NULL),
(30, 30, 1, '0000-00-00', '0000-00-00', NULL),
(31, 25, 0, '0000-00-00', '0000-00-00', NULL),
(31, 31, 1, '0000-00-00', '0000-00-00', NULL),
(32, 25, 0, '0000-00-00', '0000-00-00', NULL),
(32, 32, 1, '0000-00-00', '0000-00-00', NULL),
(20, 20, 0, '0000-00-00', '0000-00-00', NULL),
(27, 20, 0, '0000-00-00', '0000-00-00', NULL),
(27, 27, 1, '0000-00-00', '0000-00-00', NULL),
(26, 20, 0, '0000-00-00', '0000-00-00', NULL),
(26, 26, 1, '0000-00-00', '0000-00-00', NULL),
(24, 24, 0, '0000-00-00', '0000-00-00', NULL),
(18, 18, 0, '0000-00-00', '0000-00-00', NULL),
(45, 18, 0, '0000-00-00', '0000-00-00', NULL),
(45, 45, 1, '0000-00-00', '0000-00-00', NULL),
(46, 18, 0, '0000-00-00', '0000-00-00', NULL),
(46, 46, 1, '0000-00-00', '0000-00-00', NULL),
(17, 17, 0, '0000-00-00', '0000-00-00', NULL),
(33, 33, 0, '0000-00-00', '0000-00-00', NULL),
(34, 34, 0, '0000-00-00', '0000-00-00', NULL),
(37, 34, 0, '0000-00-00', '0000-00-00', NULL),
(37, 37, 1, '0000-00-00', '0000-00-00', NULL),
(38, 34, 0, '0000-00-00', '0000-00-00', NULL),
(38, 38, 1, '0000-00-00', '0000-00-00', NULL),
(39, 34, 0, '0000-00-00', '0000-00-00', NULL),
(39, 39, 1, '0000-00-00', '0000-00-00', NULL),
(40, 34, 0, '0000-00-00', '0000-00-00', NULL),
(40, 40, 1, '0000-00-00', '0000-00-00', NULL),
(41, 34, 0, '0000-00-00', '0000-00-00', NULL),
(41, 41, 1, '0000-00-00', '0000-00-00', NULL),
(42, 34, 0, '0000-00-00', '0000-00-00', NULL),
(42, 42, 1, '0000-00-00', '0000-00-00', NULL),
(43, 34, 0, '0000-00-00', '0000-00-00', NULL),
(43, 43, 1, '0000-00-00', '0000-00-00', NULL),
(44, 34, 0, '0000-00-00', '0000-00-00', NULL),
(44, 44, 1, '0000-00-00', '0000-00-00', NULL),
(47, 34, 0, '0000-00-00', '0000-00-00', NULL),
(47, 47, 1, '0000-00-00', '0000-00-00', NULL),
(48, 34, 0, '0000-00-00', '0000-00-00', NULL),
(48, 48, 1, '0000-00-00', '0000-00-00', NULL),
(49, 34, 0, '0000-00-00', '0000-00-00', NULL),
(49, 49, 1, '0000-00-00', '0000-00-00', NULL),
(50, 34, 0, '0000-00-00', '0000-00-00', NULL),
(50, 50, 1, '0000-00-00', '0000-00-00', NULL),
(51, 34, 0, '0000-00-00', '0000-00-00', NULL),
(51, 51, 1, '0000-00-00', '0000-00-00', NULL),
(52, 34, 0, '0000-00-00', '0000-00-00', NULL),
(52, 52, 1, '0000-00-00', '0000-00-00', NULL),
(58, 34, 0, '0000-00-00', '0000-00-00', NULL),
(58, 52, 1, '0000-00-00', '0000-00-00', NULL),
(58, 58, 2, '0000-00-00', '0000-00-00', NULL),
(53, 34, 0, '0000-00-00', '0000-00-00', NULL),
(53, 53, 1, '0000-00-00', '0000-00-00', NULL),
(54, 34, 0, '0000-00-00', '0000-00-00', NULL),
(54, 54, 1, '0000-00-00', '0000-00-00', NULL),
(55, 34, 0, '0000-00-00', '0000-00-00', NULL),
(55, 55, 1, '0000-00-00', '0000-00-00', NULL),
(56, 34, 0, '0000-00-00', '0000-00-00', NULL),
(56, 56, 1, '0000-00-00', '0000-00-00', NULL),
(57, 57, 0, '0000-00-00', '0000-00-00', NULL),
(59, 59, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`id`, `store_id`, `layout_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(20, 0, 0, '0000-00-00', '0000-00-00', NULL),
(25, 0, 0, '0000-00-00', '0000-00-00', NULL),
(18, 0, 0, '0000-00-00', '0000-00-00', NULL),
(59, 0, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`id`, `store_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(17, 0, '0000-00-00', '0000-00-00', NULL),
(18, 0, '0000-00-00', '0000-00-00', NULL),
(20, 0, '0000-00-00', '0000-00-00', NULL),
(24, 0, '0000-00-00', '0000-00-00', NULL),
(25, 0, '0000-00-00', '0000-00-00', NULL),
(26, 0, '0000-00-00', '0000-00-00', NULL),
(27, 0, '0000-00-00', '0000-00-00', NULL),
(28, 0, '0000-00-00', '0000-00-00', NULL),
(29, 0, '0000-00-00', '0000-00-00', NULL),
(30, 0, '0000-00-00', '0000-00-00', NULL),
(31, 0, '0000-00-00', '0000-00-00', NULL),
(32, 0, '0000-00-00', '0000-00-00', NULL),
(33, 0, '0000-00-00', '0000-00-00', NULL),
(34, 0, '0000-00-00', '0000-00-00', NULL),
(35, 0, '0000-00-00', '0000-00-00', NULL),
(36, 0, '0000-00-00', '0000-00-00', NULL),
(37, 0, '0000-00-00', '0000-00-00', NULL),
(38, 0, '0000-00-00', '0000-00-00', NULL),
(39, 0, '0000-00-00', '0000-00-00', NULL),
(40, 0, '0000-00-00', '0000-00-00', NULL),
(41, 0, '0000-00-00', '0000-00-00', NULL),
(42, 0, '0000-00-00', '0000-00-00', NULL),
(43, 0, '0000-00-00', '0000-00-00', NULL),
(44, 0, '0000-00-00', '0000-00-00', NULL),
(45, 0, '0000-00-00', '0000-00-00', NULL),
(46, 0, '0000-00-00', '0000-00-00', NULL),
(47, 0, '0000-00-00', '0000-00-00', NULL),
(48, 0, '0000-00-00', '0000-00-00', NULL),
(49, 0, '0000-00-00', '0000-00-00', NULL),
(50, 0, '0000-00-00', '0000-00-00', NULL),
(51, 0, '0000-00-00', '0000-00-00', NULL),
(52, 0, '0000-00-00', '0000-00-00', NULL),
(53, 0, '0000-00-00', '0000-00-00', NULL),
(54, 0, '0000-00-00', '0000-00-00', NULL),
(55, 0, '0000-00-00', '0000-00-00', NULL),
(56, 0, '0000-00-00', '0000-00-00', NULL),
(57, 0, '0000-00-00', '0000-00-00', NULL),
(58, 0, '0000-00-00', '0000-00-00', NULL),
(59, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_confirm`
--

CREATE TABLE `oc_confirm` (
  `id` int(11) NOT NULL,
  `email` varchar(60) NOT NULL,
  `order_id` varchar(15) NOT NULL,
  `payment_date` date NOT NULL,
  `total_amount` int(11) NOT NULL,
  `destination_bank` varchar(20) NOT NULL,
  `resi` varchar(30) NOT NULL,
  `payment_method` varchar(20) NOT NULL,
  `sender_name` varchar(80) NOT NULL,
  `bank_origin` varchar(50) NOT NULL,
  `code` varchar(255) NOT NULL,
  `no_receipt` varchar(50) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `oc_confirm`
--

INSERT INTO `oc_confirm` (`id`, `email`, `order_id`, `payment_date`, `total_amount`, `destination_bank`, `resi`, `payment_method`, `sender_name`, `bank_origin`, `code`, `no_receipt`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'cullicuplis13@gmail.com', '23', '2017-04-03', 45000, 'BNI a.n. zuhry 73372', '', 'ATM', 'dadasd', 'dasdasd', '0ebc7f2be38e7eb1ee60c60a0b90e17798d7c921', '', '2017-04-22 04:34:32', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_confirm_to_store`
--

CREATE TABLE `oc_confirm_to_store` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_confirm_to_store`
--

INSERT INTO `oc_confirm_to_store` (`id`, `store_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, '0000-00-00', '0000-00-00', NULL),
(2, 0, '0000-00-00', '0000-00-00', NULL),
(3, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_country`
--

CREATE TABLE `oc_country` (
  `id` int(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  `code` varchar(10) NOT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `address_format` varchar(200) NOT NULL,
  `iso_code_2` varchar(5) NOT NULL,
  `iso_code_3` varchar(5) NOT NULL,
  `postcode_required` enum('1','0') NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `oc_country`
--

INSERT INTO `oc_country` (`id`, `name`, `code`, `status`, `address_format`, `iso_code_2`, `iso_code_3`, `postcode_required`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bali', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(2, 'Bangka Belitung', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(3, 'Banten', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(4, 'Bengkulu', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(5, 'DI Yogyakarta', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(6, 'DKI Jakarta', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(7, 'Gorontalo', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(8, 'Jambi', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(9, 'Jawa Barat', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(10, 'Jawa Tengah', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(11, 'Jawa Timur', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(12, 'Kalimantan Barat', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(13, 'Kalimantan Selatan', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(14, 'Kalimantan Tengah', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(15, 'Kalimantan Timur', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(16, 'Kalimantan Utara', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(17, 'Kepulauan Riau', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(18, 'Lampung', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(19, 'Maluku', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(20, 'Maluku Utara', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(21, 'Nanggroe Aceh Darussalam (NAD)', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(22, 'Nusa Tenggara Barat (NTB)', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(23, 'Nusa Tenggara Timur (NTT)', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(24, 'Papua', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(25, 'Papua Barat', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(26, 'Riau', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(27, 'Sulawesi Barat', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(28, 'Sulawesi Selatan', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(29, 'Sulawesi Tengah', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(30, 'Sulawesi Tenggara', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(31, 'Sulawesi Utara', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(32, 'Sumatera Barat', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(33, 'Sumatera Selatan', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL),
(34, 'Sumatera Utara', '', '1', '', '', '', '1', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_country_hpwd`
--

CREATE TABLE `oc_country_hpwd` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_country_hpwd`
--

INSERT INTO `oc_country_hpwd` (`id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(2, 'Albania', 'AL', 'ALB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(5, 'Andorra', 'AD', 'AND', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(6, 'Angola', 'AO', 'AGO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(13, 'Australia', 'AU', 'AUS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(14, 'Austria', 'AT', 'AUT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1, '0000-00-00', '0000-00-00', NULL),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(38, 'Canada', 'CA', 'CAN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(42, 'Chad', 'TD', 'TCD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(43, 'Chile', 'CL', 'CHL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(44, 'China', 'CN', 'CHN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(47, 'Colombia', 'CO', 'COL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(48, 'Comoros', 'KM', 'COM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(49, 'Congo', 'CG', 'COG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(67, 'Estonia', 'EE', 'EST', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(72, 'Finland', 'FI', 'FIN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1, '0000-00-00', '0000-00-00', NULL),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1, '0000-00-00', '0000-00-00', NULL),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(84, 'Greece', 'GR', 'GRC', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(88, 'Guam', 'GU', 'GUM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(95, 'Honduras', 'HN', 'HND', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(99, 'India', 'IN', 'IND', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(104, 'Israel', 'IL', 'ISR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(105, 'Italy', 'IT', 'ITA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(107, 'Japan', 'JP', 'JPN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(113, 'Korea, Republic of', 'KR', 'KOR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(125, 'Macau', 'MO', 'MAC', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(131, 'Mali', 'ML', 'MLI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(132, 'Malta', 'MT', 'MLT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(155, 'Niger', 'NE', 'NER', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(157, 'Niue', 'NU', 'NIU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(160, 'Norway', 'NO', 'NOR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(161, 'Oman', 'OM', 'OMN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(163, 'Palau', 'PW', 'PLW', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(164, 'Panama', 'PA', 'PAN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(167, 'Peru', 'PE', 'PER', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(170, 'Poland', 'PL', 'POL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(174, 'Reunion', 'RE', 'REU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(175, 'Romania', 'RO', 'ROM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1, '0000-00-00', '0000-00-00', NULL),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(195, 'Spain', 'ES', 'ESP', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1, '0000-00-00', '0000-00-00', NULL),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(209, 'Thailand', 'TH', 'THA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(210, 'Togo', 'TG', 'TGO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(212, 'Tonga', 'TO', 'TON', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1, '0000-00-00', '0000-00-00', NULL),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1, '0000-00-00', '0000-00-00', NULL),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1, '0000-00-00', '0000-00-00', NULL),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(10) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_coupon`
--

INSERT INTO `oc_coupon` (`id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `created_at`, `updated_at`, `deleted_at`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '0000-00-00', '0000-00-00', NULL, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '0000-00-00', '0000-00-00', NULL, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '0000-00-00', '0000-00-00', NULL, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_currency`
--

CREATE TABLE `oc_currency` (
  `id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` float(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_currency`
--

INSERT INTO `oc_currency` (`id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'Rupiah', 'IDR', 'Rp. ', '', '0', 1.00000000, 1, '2017-04-22 03:16:03', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer`
--

CREATE TABLE `oc_customer` (
  `id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer`
--

INSERT INTO `oc_customer` (`id`, `customer_group_id`, `store_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `approved`, `safe`, `token`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 1, 0, 'amin', 'udin', 'aminudin@gmail.com', '085397226021', 'da', 'b26ae0c98ed18ea88a64e97da7f676105446d0ff', '82599a7a5', 'a:1:{s:40:"YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjUyO30=";i:2;}', '', 0, 2, '', '::1', 1, 1, 0, '', '2017-02-02 03:35:22', '0000-00-00', '0000-00-00', '0000-00-00'),
(3, 1, 0, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', '236652de315835b65fd8bcca6e88093d01834b8c', '972577cda', 'a:1:{s:40:"YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjUyO30=";i:1;}', 'a:0:{}', 0, 3, '', '::1', 1, 1, 0, '', '2017-02-02 04:05:59', '0000-00-00', '0000-00-00', '0000-00-00'),
(4, 1, 0, 'zuhry', 'culli', 'cullicuplis13@gmail.com', '085397226021', 'da', '6d2cb30412afa63a8453fec9394b02945f923f38', 'b7e8fb4a1', 'a:0:{}', '', 0, 4, 'a:0:{}', '::1', 1, 1, 0, '', '2017-04-12 18:39:27', '0000-00-00', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer_activity`
--

INSERT INTO `oc_customer_activity` (`id`, `customer_id`, `key`, `data`, `ip`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'register', 'a:2:{s:11:"customer_id";i:1;s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-30 16:51:36', '0000-00-00', '0000-00-00', NULL),
(2, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-30 17:38:18', '0000-00-00', '0000-00-00', NULL),
(3, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-30 18:33:33', '0000-00-00', '0000-00-00', NULL),
(4, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:6;}', '::1', '2017-01-30 19:05:35', '0000-00-00', '0000-00-00', NULL),
(5, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-31 08:45:35', '0000-00-00', '0000-00-00', NULL),
(6, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:9;}', '::1', '2017-01-31 08:46:07', '0000-00-00', '0000-00-00', NULL),
(7, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-31 11:20:12', '0000-00-00', '0000-00-00', NULL),
(8, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-31 14:37:02', '0000-00-00', '0000-00-00', NULL),
(9, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:11;}', '::1', '2017-01-31 14:52:53', '0000-00-00', '0000-00-00', NULL),
(10, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:12;}', '::1', '2017-01-31 14:59:21', '0000-00-00', '0000-00-00', NULL),
(11, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:13;}', '::1', '2017-01-31 15:10:43', '0000-00-00', '0000-00-00', NULL),
(12, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-01-31 15:22:25', '0000-00-00', '0000-00-00', NULL),
(13, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:14;}', '::1', '2017-01-31 16:12:56', '0000-00-00', '0000-00-00', NULL),
(14, 1, 'login', 'a:2:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-02-01 03:07:47', '0000-00-00', '0000-00-00', NULL),
(15, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:15;}', '::1', '2017-02-01 03:08:21', '0000-00-00', '0000-00-00', NULL),
(16, 1, 'order_account', 'a:3:{s:11:"customer_id";s:1:"1";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:16;}', '::1', '2017-02-01 03:35:57', '0000-00-00', '0000-00-00', NULL),
(17, 2, 'register', 'a:2:{s:11:"customer_id";i:2;s:4:"name";s:9:"amin udin";}', '::1', '2017-02-02 03:35:24', '0000-00-00', '0000-00-00', NULL),
(18, 2, 'order_account', 'a:3:{s:11:"customer_id";s:1:"2";s:4:"name";s:9:"amin udin";s:8:"order_id";i:17;}', '::1', '2017-02-02 03:36:08', '0000-00-00', '0000-00-00', NULL),
(19, 3, 'register', 'a:2:{s:11:"customer_id";i:3;s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-02 04:06:00', '0000-00-00', '0000-00-00', NULL),
(20, 3, 'address_edit', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-02 04:10:03', '0000-00-00', '0000-00-00', NULL),
(21, 3, 'order_account', 'a:3:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";s:8:"order_id";i:18;}', '::1', '2017-02-02 04:10:50', '0000-00-00', '0000-00-00', NULL),
(22, 3, 'order_account', 'a:3:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";s:8:"order_id";i:19;}', '::1', '2017-02-02 04:11:29', '0000-00-00', '0000-00-00', NULL),
(23, 3, 'login', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-02 17:42:32', '0000-00-00', '0000-00-00', NULL),
(24, 3, 'order_account', 'a:3:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";s:8:"order_id";i:20;}', '::1', '2017-02-02 17:42:59', '0000-00-00', '0000-00-00', NULL),
(25, 3, 'login', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-04 17:07:14', '0000-00-00', '0000-00-00', NULL),
(26, 3, 'login', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-04 23:39:37', '0000-00-00', '0000-00-00', NULL),
(27, 3, 'login', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-02-07 12:03:45', '0000-00-00', '0000-00-00', NULL),
(28, 3, 'order_account', 'a:3:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";s:8:"order_id";i:22;}', '::1', '2017-02-07 12:04:47', '0000-00-00', '0000-00-00', NULL),
(29, 3, 'forgotten', 'a:2:{s:11:"customer_id";s:1:"3";s:4:"name";s:12:"Makmur galih";}', '::1', '2017-04-10 21:42:58', '0000-00-00', '0000-00-00', NULL),
(30, 4, 'register', 'a:2:{s:11:"customer_id";i:4;s:4:"name";s:11:"zuhry culli";}', '::1', '2017-04-12 18:39:29', '0000-00-00', '0000-00-00', NULL),
(31, 4, 'login', 'a:2:{s:11:"customer_id";s:1:"4";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-04-22 03:15:57', '0000-00-00', '0000-00-00', NULL),
(32, 4, 'login', 'a:2:{s:11:"customer_id";s:1:"4";s:4:"name";s:11:"zuhry culli";}', '::1', '2017-04-22 04:32:39', '0000-00-00', '0000-00-00', NULL),
(33, 4, 'order_account', 'a:3:{s:11:"customer_id";s:1:"4";s:4:"name";s:11:"zuhry culli";s:8:"order_id";i:23;}', '::1', '2017-04-22 04:34:13', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_ban_ip`
--

CREATE TABLE `oc_customer_ban_ip` (
  `id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`id`, `approval`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`id`, `customer_group_id`, `language_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 2, 'Default', 'test', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`id`, `customer_id`, `ip`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, '::1', '2017-02-02 03:35:24', '0000-00-00', '0000-00-00', NULL),
(3, 3, '::1', '2017-02-02 04:06:00', '0000-00-00', '0000-00-00', NULL),
(4, 4, '::1', '2017-04-22 03:15:57', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`id`, `email`, `ip`, `total`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '::1', 1, '2017-02-01 06:13:52', '2017-02-01 06:13:52', '0000-00-00', '0000-00-00', NULL),
(3, 'zuhry', '::1', 5, '2017-03-17 17:45:07', '2017-04-22 03:03:30', '0000-00-00', '0000-00-00', NULL),
(4, 'zuhryculli@gmail.com', '::1', 3, '2017-04-22 03:03:46', '2017-04-22 03:14:49', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `location` varchar(7) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_download`
--

CREATE TABLE `oc_download` (
  `id` int(11) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_event`
--

CREATE TABLE `oc_event` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_extension`
--

CREATE TABLE `oc_extension` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_extension`
--

INSERT INTO `oc_extension` (`id`, `type`, `code`, `created_at`, `updated_at`, `deleted_at`) VALUES
(23, 'payment', 'cod', '0000-00-00', '0000-00-00', NULL),
(22, 'total', 'shipping', '0000-00-00', '0000-00-00', NULL),
(57, 'total', 'sub_total', '0000-00-00', '0000-00-00', NULL),
(58, 'total', 'tax', '0000-00-00', '0000-00-00', NULL),
(59, 'total', 'total', '0000-00-00', '0000-00-00', NULL),
(410, 'module', 'banner', '0000-00-00', '0000-00-00', NULL),
(426, 'module', 'carousel', '0000-00-00', '0000-00-00', NULL),
(390, 'total', 'credit', '0000-00-00', '0000-00-00', NULL),
(447, 'module', 'anylist', '0000-00-00', '0000-00-00', NULL),
(349, 'total', 'handling', '0000-00-00', '0000-00-00', NULL),
(350, 'total', 'low_order_fee', '0000-00-00', '0000-00-00', NULL),
(389, 'total', 'coupon', '0000-00-00', '0000-00-00', NULL),
(413, 'module', 'category', '0000-00-00', '0000-00-00', NULL),
(408, 'module', 'account', '0000-00-00', '0000-00-00', NULL),
(393, 'total', 'reward', '0000-00-00', '0000-00-00', NULL),
(398, 'total', 'voucher', '0000-00-00', '0000-00-00', NULL),
(407, 'payment', 'free_checkout', '0000-00-00', '0000-00-00', NULL),
(427, 'module', 'featured', '0000-00-00', '0000-00-00', NULL),
(419, 'module', 'slideshow', '0000-00-00', '0000-00-00', NULL),
(446, 'module', 'special', '0000-00-00', '0000-00-00', NULL),
(429, 'shipping', 'jneoke', '0000-00-00', '0000-00-00', NULL),
(430, 'shipping', 'jnereg', '0000-00-00', '0000-00-00', NULL),
(431, 'shipping', 'jneyes', '0000-00-00', '0000-00-00', NULL),
(432, 'shipping', 'kontak_shipping', '0000-00-00', '0000-00-00', NULL),
(435, 'payment', 'bank_transfer', '0000-00-00', '0000-00-00', NULL),
(436, 'shipping', 'tikireg', '0000-00-00', '0000-00-00', NULL),
(437, 'shipping', 'tikisds', '0000-00-00', '0000-00-00', NULL),
(438, 'shipping', 'tikions', '0000-00-00', '0000-00-00', NULL),
(439, 'shipping', 'tikieco', '0000-00-00', '0000-00-00', NULL),
(440, 'shipping', 'tikihds', '0000-00-00', '0000-00-00', NULL),
(443, 'module', 'confirm_setting', '0000-00-00', '0000-00-00', NULL),
(444, 'module', 'tiki_setting', '0000-00-00', '0000-00-00', NULL),
(445, 'module', 'latest', '0000-00-00', '0000-00-00', NULL),
(448, 'module', 'chatnox', '0000-00-00', '0000-00-00', NULL),
(449, 'shipping', 'flat', '0000-00-00', '0000-00-00', NULL),
(450, 'shipping', 'jne_trucking', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_filter`
--

CREATE TABLE `oc_filter` (
  `id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_modified` datetime NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`id`, `name`, `description`, `date_modified`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2010-02-26 22:33:24', '2009-01-06 23:26:25', '0000-00-00', '0000-00-00', NULL),
(4, 'UK Shipping', 'UK Shipping Zones', '2010-12-15 15:18:13', '2009-06-23 01:14:53', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_information`
--

CREATE TABLE `oc_information` (
  `id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_information`
--

INSERT INTO `oc_information` (`id`, `bottom`, `sort_order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 1, 3, 1, '0000-00-00', '0000-00-00', NULL),
(4, 1, 1, 1, '0000-00-00', '0000-00-00', NULL),
(5, 1, 4, 1, '0000-00-00', '0000-00-00', NULL),
(6, 1, 2, 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_information_description`
--

INSERT INTO `oc_information_description` (`id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 2, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', '', '', '', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', '', '', '', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', '', '', '', '0000-00-00', '0000-00-00', NULL),
(6, 2, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', '', '', '', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`id`, `store_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 0, '0000-00-00', '0000-00-00', NULL),
(4, 0, '0000-00-00', '0000-00-00', NULL),
(5, 0, '0000-00-00', '0000-00-00', NULL),
(6, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_language`
--

CREATE TABLE `oc_language` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_language`
--

INSERT INTO `oc_language` (`id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Indonesia', 'id', 'id_ID', 'id.png', 'indonesia', 1, 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_layout`
--

CREATE TABLE `oc_layout` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_layout`
--

INSERT INTO `oc_layout` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Home', '0000-00-00', '0000-00-00', NULL),
(2, 'Product', '0000-00-00', '0000-00-00', NULL),
(3, 'Category', '0000-00-00', '0000-00-00', NULL),
(4, 'Default', '0000-00-00', '0000-00-00', NULL),
(5, 'Manufacturer', '0000-00-00', '0000-00-00', NULL),
(6, 'Account', '0000-00-00', '0000-00-00', NULL),
(7, 'Checkout', '0000-00-00', '0000-00-00', NULL),
(8, 'Contact', '0000-00-00', '0000-00-00', NULL),
(9, 'Sitemap', '0000-00-00', '0000-00-00', NULL),
(10, 'Affiliate', '0000-00-00', '0000-00-00', NULL),
(11, 'Information', '0000-00-00', '0000-00-00', NULL),
(12, 'Compare', '0000-00-00', '0000-00-00', NULL),
(13, 'Search', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`id`, `layout_id`, `code`, `position`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 4, '0', 'content_top', 0, '0000-00-00', '0000-00-00', NULL),
(3, 4, '0', 'content_top', 1, '0000-00-00', '0000-00-00', NULL),
(143, 5, 'account', 'column_left', 2, '0000-00-00', '0000-00-00', NULL),
(69, 10, 'affiliate', 'column_right', 1, '0000-00-00', '0000-00-00', NULL),
(68, 6, 'account', 'column_right', 1, '0000-00-00', '0000-00-00', NULL),
(204, 2, 'slideshow.27', 'content_top', 0, '0000-00-00', '0000-00-00', NULL),
(205, 2, 'featured.28', 'content_bottom', 1, '0000-00-00', '0000-00-00', NULL),
(261, 1, 'chatnox', 'content_bottom', 0, '0000-00-00', '0000-00-00', NULL),
(260, 1, 'carousel.29', 'content_top', 3, '0000-00-00', '0000-00-00', NULL),
(259, 1, 'slideshow.27', 'content_top', 0, '0000-00-00', '0000-00-00', NULL),
(126, 3, 'banner.30', 'column_left', 2, '0000-00-00', '0000-00-00', NULL),
(125, 3, 'category', 'column_left', 0, '0000-00-00', '0000-00-00', NULL),
(258, 1, 'featured.28', 'content_bottom', 2, '0000-00-00', '0000-00-00', NULL),
(257, 1, 'latest.32', 'content_top', 2, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`id`, `layout_id`, `store_id`, `route`, `created_at`, `updated_at`, `deleted_at`) VALUES
(38, 6, 0, 'account/%', '0000-00-00', '0000-00-00', NULL),
(17, 10, 0, 'affiliate/%', '0000-00-00', '0000-00-00', NULL),
(73, 3, 0, 'product/category', '0000-00-00', '0000-00-00', NULL),
(97, 1, 0, 'common/home', '0000-00-00', '0000-00-00', NULL),
(86, 2, 0, 'product/product', '0000-00-00', '0000-00-00', NULL),
(23, 7, 0, 'checkout/%', '0000-00-00', '0000-00-00', NULL),
(31, 8, 0, 'information/contact', '0000-00-00', '0000-00-00', NULL),
(32, 9, 0, 'information/sitemap', '0000-00-00', '0000-00-00', NULL),
(34, 4, 0, '', '0000-00-00', '0000-00-00', NULL),
(77, 5, 0, 'product/manufacturer', '0000-00-00', '0000-00-00', NULL),
(52, 12, 0, 'product/compare', '0000-00-00', '0000-00-00', NULL),
(53, 13, 0, 'product/search', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_length_class`
--

INSERT INTO `oc_length_class` (`id`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1.00000000', '0000-00-00', '0000-00-00', NULL),
(2, '10.00000000', '0000-00-00', '0000-00-00', NULL),
(3, '0.39370000', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`id`, `language_id`, `title`, `unit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Centimeter', 'cm', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Millimeter', 'mm', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Inch', 'in', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_location`
--

CREATE TABLE `oc_location` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`id`, `name`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(7, 'Hewlett-Packard', 'catalog/demo/hp_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(8, 'Apple', 'catalog/demo/apple_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(9, 'Canon', 'catalog/demo/canon_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL),
(10, 'Sony', 'catalog/demo/sony_logo.jpg', 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`id`, `store_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 0, '0000-00-00', '0000-00-00', NULL),
(6, 0, '0000-00-00', '0000-00-00', NULL),
(7, 0, '0000-00-00', '0000-00-00', NULL),
(8, 0, '0000-00-00', '0000-00-00', NULL),
(9, 0, '0000-00-00', '0000-00-00', NULL),
(10, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_modification`
--

CREATE TABLE `oc_modification` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_modification`
--

INSERT INTO `oc_modification` (`id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Base Shipping BackEnd', 'BaseShippingBackEnd', 'HP Web Design 2.0.3.1', '<b>1.0</b>', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[Base Shipping BackEnd]]></name>\n   <code>BaseShippingBackEnd</code>\n    <version><![CDATA[<b>1.0</b>]]></version>\n      <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design 2.0.3.1]]></author>\n\n<file path="admin/language/english/sale/order.php,admin/language/english/marketing/affiliate.php,admin/language/english/sale/customer.php,admin/language/english/setting/setting.php">\n  <operation>\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="13"><![CDATA[	\n			$_[\'text_country\']          = \'Province\';\n			$_[\'text_zone\']             = \'City/Regency\';\n			$_[\'text_city\']             = \'SubDistrict\';\n			$_[\'entry_country\']          = \'Province\';\n			$_[\'entry_city\']             = \'SubDistrict\';\n			$_[\'entry_zone\']             = \'City/Regency\';\n			$_[\'error_country\']          = \'Please select a Province\';	\n			$_[\'error_zone\']             = \'Please select a City/Regency\';\n			$_[\'error_city\']         = \'Please select a SubDistrict\';\n			]]></add>\n		</operation>\n</file>	\n    \n<file path="admin/language/indonesia/sale/order.php,admin/language/indonesia/marketing/affiliate.php,admin/language/indonesia/sale/customer.php,admin/language/english/setting/setting.php">\n  <operation>\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="1"><![CDATA[	\n			$_[\'text_country\']          = \'Propinsi\';\n			$_[\'text_zone\']             = \'Kota/Kabupaten\';\n			$_[\'text_city\']             = \'Kecamatan\';		\n			$_[\'entry_country\']          = \'Propinsi\';		\n			$_[\'entry_zone\']             = \'Kota/Kabupaten\';\n			$_[\'entry_city\']        = \'Kecamatan\';\n			$_[\'error_country\']          = \'Silahkan pilih Propinsi\';\n			$_[\'error_zone\']             = \'Silahkan pilih Kota/Kabupaten\';\n			$_[\'error_city\']         = \'Silahkan pilih Kecamatan\';\n			]]></add>\n		</operation>\n</file>	\n    \n  <file path="admin/controller/sale/order.php">\n    <operation>\n      <search trim="true"><![CDATA[\n	public function shipping() {\n      ]]></search>\n			<add position="before"><![CDATA[ \npublic function subdistrict() {\n        if(isset($this->request->get[\'city_id\'])) {\n            $subdistrict_data = $this->cache->get(\'hpwd.subdistrict.\'.(int)$this->request->get[\'city_id\']);\n        }\n         \n         if(!$subdistrict_data) {\n            if(isset($this->request->get[\'city_id\'])) {\n            $url="http://pro.rajaongkir.com/api/subdistrict?city=".$this->request->get[\'city_id\'];\n        $subdistrict_data = $this->requestData($url);\n            $this->cache->set(\'hpwd.subdistrict.\'.(int)$this->request->get[\'city_id\'], $subdistrict_data);     \n        } \n     }   \n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput($subdistrict_data);\n    }\n    \n    public function city() {\n        if(isset($this->request->get[\'province_id\'])) {\n            $city_data = $this->cache->get(\'hpwd.city.\'.(int)$this->request->get[\'province_id\']);\n        }\n        \n        $city_data = $this->cache->get(\'hpwd.city.\'.(int)$this->request->get[\'province_id\']);\n        if(!$city_data) {\n            if(isset($this->request->get[\'province_id\'])) {\n            $url="http://pro.rajaongkir.com/api/city?province=".$this->request->get[\'province_id\'];\n        $city_data = $this->requestData($url);\n            $this->cache->set(\'hpwd.city.\'.(int)$this->request->get[\'province_id\'], $city_data);     \n        } \n    }    \n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput($city_data);\n    }\n\npublic function getCityName($city_id,$province_id) {\n    $curl = curl_init();\n\n    curl_setopt_array($curl, array(\n      CURLOPT_URL => "http://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,\n      CURLOPT_RETURNTRANSFER => true,\n      CURLOPT_ENCODING => "",\n      CURLOPT_MAXREDIRS => 10,\n      CURLOPT_TIMEOUT => 30,\n      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n      CURLOPT_CUSTOMREQUEST => "GET",\n      CURLOPT_HTTPHEADER => array(\n        "key: 503a8f186660898c990c11e562435849"\n      ),\n    ));\n\n    $response = curl_exec($curl);\n    $err = curl_error($curl);\n\n    curl_close($curl);\n\n    if ($err) {\n      return false;\n    } else {\n      $json = json_decode($response);\n\n        if(!empty($json->rajaongkir->results->city_name)){\n        return $json->rajaongkir->results->city_name;\n                } \n            }  \n    }\n\n    public function province() {\n		$json = array();\n        \n        $json[\'error\'] = \'\';\n        $json[\'sucess\'] = array();\n        \n        $province_data = $this->cache->get(\'hpwd.province\');\n        if(!$province_data) {\n            if(isset($this->request->get[\'province_id\'])) {\n            $province_id = $this->request->get[\'province_id\'];\n            $url="http://pro.rajaongkir.com/api/province?id=".$province_id;\n        $province_data = $this->requestData($url);    \n        $this->cache->set(\'hpwd.province\', $province_data);     \n        } \n     }\n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput($province_data);\n	}\n    \n   public function requestData($url) {\n        \n        $curl = curl_init();\n\n        curl_setopt_array($curl, array(\n          CURLOPT_URL => $url,\n          CURLOPT_RETURNTRANSFER => true,\n          CURLOPT_ENCODING => "",\n          CURLOPT_MAXREDIRS => 10,\n          CURLOPT_TIMEOUT => 30,\n          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n          CURLOPT_CUSTOMREQUEST => "GET",\n          CURLOPT_HTTPHEADER => array(\n            "key:  503a8f186660898c990c11e562435849"\n          ),\n        ));\n\n        $response = curl_exec($curl);\n        $err = curl_error($curl);\n\n        curl_close($curl);\n          if ($err) {\n          return $err;\n        } else {\n          return $response;\n        }          \n    } ]]></add>\n		</operation>\n<operation>\n      <search trim="true"><![CDATA[\n    $data[\'payment_zone\'] = $order_info[\'payment_zone\'];\n      ]]></search>\n			<add position="replace"><![CDATA[$data[\'payment_zone\'] = $this->getCityName($order_info[\'payment_zone_id\'],$order_info[\'payment_country_id\']);\n				]]></add>\n		</operation>    \n<operation>\n      <search trim="true"><![CDATA[\n    $data[\'shipping_zone\'] = $order_info[\'shipping_zone\'];\n      ]]></search>\n			<add position="replace"><![CDATA[$data[\'shipping_zone\'] = $this->getCityName($order_info[\'shipping_zone_id\'],$order_info[\'shipping_country_id\']);\n				]]></add>\n		</operation>\n      <operation>\n      <search trim="true"><![CDATA[\n        \'zone\'      => $order_info[\'payment_zone\'],\n      ]]></search>\n			<add position="replace"><![CDATA[\n        \'zone\'      => $this->getCityName($order_info[\'payment_zone_id\'],$order_info[\'payment_country_id\']),\n				]]></add>\n		</operation> \n        <operation>\n      <search trim="true"><![CDATA[\n        \'zone\'      => $order_info[\'shipping_zone\'],\n      ]]></search>\n			<add position="replace"><![CDATA[\n        \'zone\'      => $this->getCityName($order_info[\'shipping_zone_id\'],$order_info[\'shipping_country_id\']),\n				]]></add>\n		</operation> \n </file>  \n<file path="admin/view/template/setting/setting.tpl">\n     <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=setting/setting/country&token=<?php echo $token; ?>&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=sale/order/city&token=<?php echo $token; ?>&province_id=\' + this.value,\n            ]]></add>\n</operation>\n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\nif(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $config_zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n            $(\'select[name=\\\'config_zone_id\\\']\').html(html);\n            });	\n    }\n            ]]></add>\n</operation> \n   </file>\n     \n<file path="admin/view/template/sale/order_info.tpl">\n        <operation>\n      <search trim="true"><![CDATA[\n              <?php if ($payment_zone_code) { ?>\n      ]]></search>\n			<add position="replace"><![CDATA[ \n              <?php if (0) { ?>\n				]]></add>\n		</operation>\n    <operation>\n      <search trim="true"><![CDATA[\n              <?php if ($shipping_zone_code) { ?>\n      ]]></search>\n			<add position="replace"><![CDATA[ \n              <?php if (0) { ?>\n				]]></add>\n		</operation>\n    </file>\n    \n<file path="admin/controller/sale/customer.php">\n    <operation>\n      <search trim="true"><![CDATA[\n		if ($country_info && $country_info[\'postcode_required\'] && (utf8_strlen($value[\'postcode\']) < 2 || utf8_strlen($value[\'postcode\']) > 10)) {\n      ]]></search>\n			<add position="replace"><![CDATA[ \n	if (utf8_strlen($value[\'postcode\']) < 3) {\n				]]></add>\n		</operation>\n		\n    <operation>\n      <search trim="true"><![CDATA[  \n	public function country() {\n      ]]></search>\n			<add position="before"><![CDATA[\n		public function zone() {\n		$json = array();\n		\n		$this->load->model(\'localisation/zone\');\n\n    	$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\n		\n		if ($zone_info) {\n			$this->load->model(\'localisation/kecamatan\');\n\n			$json = array(\n				\'zone_id\'        => $zone_info[\'zone_id\'],\n				\'name\'              => $zone_info[\'name\'],\n				\'kecamatan\'         => $this->model_localisation_kecamatan->getKecamatansByKabupatenId($this->request->get[\'zone_id\']),\n				\'status\'            => $zone_info[\'status\']		\n			);\n		}\n		$this->response->addHeader(\'Content-Type: application/json\');		\n		$this->response->setOutput(json_encode($json));\n	}	\n			]]></add>\n		</operation>\n   </file>\n<file path="admin/view/template/marketing/affiliate_form.tpl">\n     <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=marketing/affiliate/country&token=<?php echo $token; ?>&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=sale/order/city&token=<?php echo $token; ?>&province_id=\' + this.value,\n            ]]></add>\n</operation>\n <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[		\n$(\'input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'<?php if ($error_city) { ?> <div class="text-danger"><?php echo $error_city; ?></div><?php } ?></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\nvar city = \'<?php echo $city; ?>\';\n	$.ajax({\n		url: \'index.php?route=sale/order/subdistrict&token=<?php echo $token; ?>&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\n	if(json[\'rajaongkir\'][\'results\']) {		\n     $.map(json, function(item) {\n                    html = \'<option value=""><?php echo $text_select; ?></option>\';\n                    $.each(item[\'results\'], function( key, val ) {\n                    if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                    html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                    } else { \n                    html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                      }\n                    });\n\n                $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n\n                var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n                $(\'input[name=\\\'city\\\']\').val(the_value);\n             });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'city\\\']\').val(the_value);\n});			]]></add>\n		</operation>\n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[\n 	if(json[\'rajaongkir\'][\'results\']) {		\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                 if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');       \n            });	}\n            ]]></add>\n</operation>\n<operation error="skip">\n      <search trim="true"><![CDATA[ \n	html += \'    <div class="col-sm-10"><input type="text" name="address[\' + address_row + \'][address_2]" value="" placeholder="<?php echo $entry_address_2; ?>" id="input-address-2\' + address_row + \'" class="form-control" /></div>\';\n        ]]></search>\n			<add position="replace" offset="6"><![CDATA[\n	html += \'  </div>\';\n            ]]></add>\n		</operation>\n    \n    <operation error="skip">\n      <search trim="true"><![CDATA[ \n	html += \'    <div class="col-sm-10"><input type="text" name="address[\' + address_row + \'][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city\' + address_row + \'" class="form-control" /></div>\';\n        ]]></search>\n			<add position="replace"><![CDATA[\n	html += \'    <div class="col-sm-10"><input type="hidden" name="address[\' + address_row + \'][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city\' + address_row + \'" class="form-control" /></div>\';\n            ]]></add>\n		</operation>\n\n    <operation error="skip">\n      <search trim="true"><![CDATA[ \n	html += \'    <label class="col-sm-2 control-label" for="input-zone\' + address_row + \'"><?php echo $entry_zone; ?></label>\';\n        ]]></search>\n			<add position="after" offset="2"><![CDATA[\n    html += \'  <div class="form-group required">\';\n	html += \'    <label class="col-sm-2 control-label" for="input-city\' + address_row + \'"><?php echo $entry_city; ?></label>\';\n	html += \'    <div class="col-sm-10"><select name="address[\' + address_row + \'][city]" id="input-city\' + address_row + \'" class="form-control"></select></div>\';\n	html += \'  </div>\';\n            ]]></add>\n		</operation>\n    \n     <operation error="skip">\n      <search trim="true"><![CDATA[ \n	html += \'    <div class="col-sm-10"><select name="address[\' + address_row + \'][zone_id]" id="input-zone\' + address_row + \'" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>\';\n        ]]></search>\n			<add position="replace"><![CDATA[\n	html += \'    <div class="col-sm-10"><select name="address[\' + address_row + \'][zone_id]" onchange="zone(this, \\\'\' + address_row + \'\\\', \\\'\\\');"  id="input-zone\' + address_row + \'" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>\';\n            ]]></add>\n		</operation>\n</file>\n\n     \n<file path="admin/view/template/sale/customer_form.tpl">\n    <operation>\n      <search trim="true" index="1"><![CDATA[ \n                    <?php $address_row++; ?>\n        ]]></search>\n			<add position="before"><![CDATA[\n<script type="text/javascript"><!--\n$(\'input[name=\\\'address[<?php echo $address_row; ?>][city]\\\']\').parent().parent().hide(); \n    \nhtml = \'<div class="form-group required">\';\nhtml += \'<label class="col-sm-2 control-label" for="input-city<?php echo $address_row; ?>"><?php echo $entry_city; ?></label>\';\nhtml += \'<div class="col-sm-10"><input type="hidden" name="address[<?php echo $address_row; ?>][city]" value="<?php echo $address[\'city\']; ?>" />\';\nhtml += \'<select name="address[<?php echo $address_row; ?>][sub_district_id]" id="input-city<?php echo $address_row; ?>" class="form-control">\';\nhtml += "<?php if (isset($error_address[$address_row][\'city\'])) { ?>";\nhtml += "<div class=\'text-danger\'><?php echo $error_address[$address_row][\'city\']; ?></div><?php } ?></div>";\nhtml += \'</select></div>\';\n\n$(\'select[name=\\\'address[<?php echo $address_row; ?>][zone_id]\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'address[<?php echo $address_row; ?>][zone_id]\\\']\').attr("onchange", "zone(this, \'<?php echo $address_row; ?>\', \\\'<?php echo $address[\'city\']; ?>\\\');");\n        //--></script>\n       ]]></add>\n		</operation>\n   \n     <operation>\n      <search trim="true"><![CDATA[ \n	html += \'    <div class="col-sm-10"><select name="address[\' + address_row + \'][zone_id]" id="input-zone\' + address_row + \'" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>\';\n        ]]></search>\n			<add position="replace"><![CDATA[\n	html += \'    <div class="col-sm-10"><select name="address[\' + address_row + \'][zone_id]" onchange="zone(this, \\\'\' + address_row + \'\\\', \\\'\\\');"  id="input-zone\' + address_row + \'" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>\';\n            ]]></add>\n		</operation>\n    <operation>\n      <search trim="true"><![CDATA[ \n	// Custom Fields\n        ]]></search>\n			<add position="before"><![CDATA[\n    	html += \'  <div class="form-group required">\';\n	html += \'    <label class="col-sm-2 control-label" for="input-sub-district-id\' + address_row + \'"><?php echo $entry_city; ?></label>\';\n	html += \'    <div class="col-sm-10"><input type="hidden" name="address[\' + address_row + \'][city]" value="" placeholder="<?php echo $entry_city; ?>" id="input-city\' + address_row + \'" class="form-control" /><select name="address[\' + address_row + \'][sub_district_id]" id="input-zone\' + address_row + \'" class="form-control"><option value=""><?php echo $text_none; ?></option></select></div>\';\n	html += \'  </div>\';\n    \n            ]]></add>\n		</operation>\n\n     <operation>\n      <search trim="true"><![CDATA[ \n	html += \'    <label class="col-sm-2 control-label" for="input-city\' + address_row + \'"><?php echo $entry_city; ?></label>\';\n        ]]></search>\n				<add position="replace" offset="2"><![CDATA[\n         html += \'<span id="city-replace"></span>\';\n            ]]></add>\n		</operation>\n<operation error="skip">\n      <search trim="true"><![CDATA[ \n         html += \'<span id="city-replace"></span>\';\n        ]]></search>\n			<add position="replace" offset="-1"><![CDATA[\n         html += \'</div>\';\n            ]]></add>\n		</operation>\n    <operation>\n      <search trim="true"><![CDATA[ \nfunction country(element, index, zone_id) {\n        ]]></search>\n			<add position="before"><![CDATA[\n  function zone(element, index, city) {\n	$.ajax({\n		url: \'index.php?route=sale/order/subdistrict&token=<?php echo $token; ?>&city_id=\' + element.value,        \n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\n		if(json[\'rajaongkir\'][\'results\']) {				\n        $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == city) {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n			$(\'select[name=\\\'address[\' + index + \'][sub_district_id]\\\']\').html(html);\n            var the_value = $(\'select[name=\\\'address[\' + index + \'][sub_district_id]\\\'] option:selected\').text();\n            $(\'input[name=\\\'address[\' + index + \'][city]\\\']\').val(the_value);\n            }); }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n\n$(\'select[name=\\\'address[\' + index + \'][sub_district_id]\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'address[\' + index + \'][sub_district_id]\\\'] option:selected\').text();\n    $(\'input[name=\\\'address[\' + index + \'][city]\\\']\').val(the_value);\n});\n} ]]></add>\n		</operation>\n<operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=sale/customer/country&token=<?php echo $token; ?>&country_id=\' + element.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=sale/order/city&token=<?php echo $token; ?>&province_id=\' + element.value,\n            ]]></add>\n</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[\n        if(json[\'rajaongkir\'][\'results\']) {		\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == zone_id ) {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n			$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').html(html);\n			$(\'select[name=\\\'address[\' + index + \'][zone_id]\\\']\').trigger(\'change\');\n            });		}	\n            ]]></add>\n</operation>\n    </file>\n\n<file path="admin/view/template/sale/order_form.tpl">\n<operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=sale/order/country&token=<?php echo $token; ?>&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=sale/order/city&token=<?php echo $token; ?>&province_id=\' + this.value,\n            ]]></add>\n</operation> \n    <operation>\n      <search trim="true"><![CDATA[ \n        $(\'#tab-payment select[name=\\\'country_id\\\']\').trigger(\'change\');\n        ]]></search>\n			<add position="after"><![CDATA[\n		$(\'#tab-payment input[name=\\\'city\\\']\').parent().parent().hide();   \nhtml = \'<div class="form-group required"><input type="hidden" name="city" value="<?php echo $payment_city; ?>" />\';\nhtml += \'<label class="col-sm-2 control-label" for="input-payment-sub-district-id"><?php echo $entry_city; ?></label>\';\nhtml += \'<div class="col-sm-10"><select name="sub_district_id" id="input-payment-sub-district-id" class="form-control"></div>\';\nhtml += \'</select></div>\';\n\n$(\'#tab-payment select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'#tab-payment select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=sale/order/subdistrict&token=<?php echo $token; ?>&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#tab-payment select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#tab-payment .fa-spin\').remove();\n		},\n		success: function(json) {\n	if(json[\'rajaongkir\'][\'results\']) {		\n $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $payment_city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            $(\'#tab-payment select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#tab-payment select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#tab-payment input[name=\\\'city\\\']\').val(the_value);\n            }); }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'#tab-payment select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#tab-payment select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#tab-payment input[name=\\\'city\\\']\').val(the_value);\n}); ]]></add>\n		</operation>\n    <operation>\n      <search trim="true"><![CDATA[ \n        $(\'#tab-shipping select[name=\\\'country_id\\\']\').trigger(\'change\');\n        ]]></search>\n			<add position="after"><![CDATA[\n		$(\'#tab-shipping input[name=\\\'city\\\']\').parent().parent().hide();   \nhtml = \'<div class="form-group required"><input type="hidden" name="city" value="<?php echo $shipping_city; ?>" />\';\nhtml += \'<label class="col-sm-2 control-label" for="input-shipping-sub-district-id"><?php echo $entry_city; ?></label>\';\nhtml += \'<div class="col-sm-10"><select name="sub_district_id" id="input-shipping-sub-district-id" class="form-control"></div>\';\nhtml += \'</select></div>\';\n\n$(\'#tab-shipping select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'#tab-shipping select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=sale/order/subdistrict&token=<?php echo $token; ?>&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#tab-shipping select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#tab-shipping .fa-spin\').remove();\n		},\n		success: function(json) {\n	if(json[\'rajaongkir\'][\'results\']) {		\n $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $shipping_city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            $(\'#tab-shipping select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#tab-shipping select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#tab-shipping input[name=\\\'city\\\']\').val(the_value);\n            }); }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'#tab-shipping select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#tab-shipping select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#tab-shipping input[name=\\\'city\\\']\').val(the_value);\n}); ]]></add>\n		</operation>\n    <operation>\n			<search trim="true" index="0"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\n    	if(json[\'rajaongkir\'][\'results\']) {		\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $payment_zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');      \n            });	}\n            ]]></add>\n</operation>\n    <operation>\n			<search trim="true" index="0"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[\n    	if(json[\'rajaongkir\'][\'results\']) {		\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $shipping_zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'#tab-shipping select[name=\\\'zone_id\\\']\').html(html);\n            $(\'#tab-shipping select[name=\\\'zone_id\\\']\').trigger(\'change\');      \n            });		}	\n            ]]></add>\n</operation>\n </file>\n<file path="admin/model/sale/customer.php">\n    <operation>\n			<search trim="true"><![CDATA[\n			foreach ($data[\'address\'] as $address) {\n			]]></search>\n			<add position="after" offset="1"><![CDATA[	\n    $the_address_id = $this->db->getLastId();\n		$this->db->query("UPDATE " . DB_PREFIX . "address SET sub_district_id = \'" . (int)$address[\'sub_district_id\'] . "\' WHERE address_id = \'" . (int)$the_address_id . "\'");\n			]]></add>\n		</operation> \n</file>\n</modification>', 1, '2017-01-30 16:43:50', '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_modification` (`id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Base Shipping FrontEnd', 'BaseShippingFrontEnd', 'HP Web Design 2.0.3.1', '<b>1.0</b>', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[Base Shipping FrontEnd]]></name>\n   <code>BaseShippingFrontEnd</code>\n    <version><![CDATA[<b>1.0</b>]]></version>\n      <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design 2.0.3.1]]></author>\n\n <file path="catalog/language/english/checkout/shipping.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="13"><![CDATA[	\n			$_[\'entry_country\']          = \'Province\';\n			$_[\'entry_zone\']             = \'City/Regency\';\n			$_[\'entry_postcode\']         = \'SubDistrict\';\n		    $_[\'error_country\']          = \'Please select a Province\';\n			$_[\'error_zone\']             = \'Please select a City/Regency\';\n			$_[\'error_city\']             = \'Please select a SubDistrict\';\n			$_[\'text_kg\']        			= \'Kg\';\n			$_[\'text_day\']        			= \'Day(s) \';\n			$_[\'text_city\']        			= \'Post Code\';\n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/indonesia/checkout/shipping.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="13"><![CDATA[	\n			$_[\'entry_country\']          = \'Propinsi\';\n			$_[\'entry_zone\']             = \'Kota/Kab.\';\n			$_[\'entry_postcode\']              = \'Kecamatan\';\n			$_[\'error_country\']          = \'Silahkan pilih Propinsi\';\n			$_[\'error_zone\']             = \'Silahkan pilih Kota/Kab.\';\n			$_[\'error_city\']             = \'Silahkan pilih Kecamatan\';\n			$_[\'text_kg\']        			= \'Kg\';\n			$_[\'text_day\']        			= \'Hari\';\n            $_[\'text_packing_kayu\']          = \'Gunakan packing kayu\';\n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/english/checkout/checkout.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="13"><![CDATA[	\n			$_[\'entry_country\']          = \'Province\';\n			$_[\'entry_city\']          = \'SubDistrict\';\n			$_[\'entry_zone\']             = \'City/Regency\';\n			$_[\'error_country\']          = \'Please select a Province\';\n			$_[\'error_zone\']             = \'Please select a City/Regency\';\n			$_[\'error_city\']        = \'Please select a SubDistrict\';\n			$_[\'text_day\']          = \'Day(s)\';			\n			$_[\'text_kg\']          = \'Kg\';			\n			$_[\'error_city\']         = \'Please select a SubDistrict\';	\n            $_[\'text_packing_kayu\']          = \'Use wooden packing\'; \n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/indonesia/checkout/checkout.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n		<add position="after" offset="13"><![CDATA[	\n			$_[\'entry_country\']          = \'Propinsi\';\n			$_[\'entry_zone\']             = \'Kota/Kab.\';\n			$_[\'entry_city\']        = \'Kecamatan\';\n			$_[\'error_country\']          = \'Silahkan pilih Propinsi\';\n			$_[\'error_zone\']             = \'Silahkan pilih Kota/Kab.\';\n			$_[\'error_city\']         = \'Silahkan pilih kecamatan\';\n			$_[\'text_day\']          = \'Hari\';			\n			$_[\'text_kg\']          = \'Kg\';\n			$_[\'error_city\']          = \'Silahkan pilih Kecamatan\';\n            $_[\'text_packing_kayu\']          = \'Gunakan packing kayu\'; \n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/english/affiliate/edit.php,catalog/language/english/affiliate/register.php,catalog/language/english/account/register.php,catalog/language/english/account/address.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n			<add position="after" offset="13"><![CDATA[		\n			$_[\'entry_city\']          = \'SubDistrict\';\n			$_[\'entry_country\']          = \'Province\';\n			$_[\'entry_zone\']             = \'City/Regency\';	\n			$_[\'error_country\']          = \'Please select a Province\';\n			$_[\'error_zone\']             = \'Please select a City/Regency\';\n			$_[\'error_city\']             = \'Please select a SubDistrict\';\n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/indonesia/affiliate/edit.php,catalog/language/english/affiliate/register.php,catalog/language/english/account/register.php,catalog/language/english/account/address.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Error]]></search>\n			<add position="after" offset="13"><![CDATA[		\n			$_[\'entry_country\']          = \'Propinsi\';\n			$_[\'entry_zone\']             = \'Kota/Kab.\';\n			$_[\'entry_city\']        = \'Kecamatan\';\n			$_[\'error_city\']          = \'Silakan pilih Kecamatan\';\n			$_[\'error_country\']          = \'Silahkan pilih Propinsi\';\n			$_[\'error_zone\']             = \'Silahkan pilih Kota/Kab.\';\n			]]></add>\n		</operation>\n</file>	\n  <file path="system/library/cart.php">\n         <operation>\n			<search trim="true"><![CDATA[\n	public function getProducts() {\n            ]]></search>\n			<add position="before"><![CDATA[		\n  protected function getCache($key) {\n        $cache = new Cache(\'file\');\n        return $cache->get($key);\n    }\n     protected function setCache($key,$value) {\n        $cache = new Cache(\'file\');\n        return $cache->set($key,$value);\n    } \npublic function getCosts($origin_id, $dest_id, $weight, $type, $courrier) {\n$costs_data = array();\n      if($origin_id && $dest_id && $weight && $type && $courrier) {\n            $costs_data = $this->getCache(\'hpwd.\'.$courrier.\'.\'.$origin_id.\'.\'.$dest_id.\'.\'.$weight);\n        }\nif(!$costs_data) {\n  $curl = curl_init();\ncurl_setopt_array($curl, array(\n  CURLOPT_URL => "http://pro.rajaongkir.com/api/cost",\n  CURLOPT_RETURNTRANSFER => true,\n  CURLOPT_ENCODING => "",\n  CURLOPT_MAXREDIRS => 10,\n  CURLOPT_TIMEOUT => 30,\n  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n  CURLOPT_CUSTOMREQUEST => "POST",\n  CURLOPT_POSTFIELDS => "origin=".$origin_id."&originType=city&destination=".$dest_id."&destinationType=subdistrict&weight=".$weight."&courier=".strtolower($courrier)."",  CURLOPT_HTTPHEADER => array(\n    "content-type: application/x-www-form-urlencoded",\n    "key: 503a8f186660898c990c11e562435849"\n  ),\n));\n\n$response = curl_exec($curl);\n$err = curl_error($curl);\n\ncurl_close($curl);\n\nif ($err) {\n  return false;\n} else {\n $costs_data = $response;\n    $this->setCache(\'hpwd.\'.$courrier.\'.\'.$origin_id.\'.\'.$dest_id.\'.\'.$weight, $costs_data);     \n    } // end of else\n}    \n  $costs = array();      \n  $json = json_decode($costs_data);\n$ar_type=array($type);   \nif($courrier == "JNE") {\n    if($type == "REG")  { \n    array_push($ar_type,"CTC");    \n    } else {\n     array_push($ar_type,"CTC".$type); \n    } \n } else if($courrier == "POS") {\n    if($type == "KILAT")  { \n    array_push($ar_type,"Surat Kilat Khusus");    \n        }  else if($type == "EXPRESS") {\n    array_push($ar_type,"Express Next Day");    \n        }\n}\n   foreach($json->rajaongkir->results[0]->costs as $cost => $val) {     \n    if(in_array($val->service,$ar_type)) {\n                    $costs = array(\n                    \'cost\' => $val->cost[0]->value,\n                    \'etd\' => $val->cost[0]->etd\n                    );\n                }\n            }\n\n    if(!empty($costs)) {\n        return $costs;\n    } else {\n     return array(\'cost\' =>\'\', \'etd\' => \'\');\n    }\n}     \npublic function getCityName($city_id,$province_id) {\n    $curl = curl_init();\n    curl_setopt_array($curl, array(\n      CURLOPT_URL => "http://pro.rajaongkir.com/api/city?id=".$city_id."&province=".$province_id,\n      CURLOPT_RETURNTRANSFER => true,\n      CURLOPT_ENCODING => "",\n      CURLOPT_MAXREDIRS => 10,\n      CURLOPT_TIMEOUT => 30,\n      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n      CURLOPT_CUSTOMREQUEST => "GET",\n      CURLOPT_HTTPHEADER => array(\n        "key: 503a8f186660898c990c11e562435849"\n      ),\n    ));\n    $response = curl_exec($curl);\n    $err = curl_error($curl);\n    curl_close($curl);\n    if ($err) {\n      return false;\n    } else {\n      $json = json_decode($response);\n\n        if(!empty($json->rajaongkir->results->city_name)){\n        return $json->rajaongkir->results->city_name;\n                } \n            }  \n    }\n			]]></add>\n		</operation>\n    </file>\n     <file path="catalog/controller/checkout/cart.php">\n  <operation>\n			<search trim="true"><![CDATA[\n	public function remove() {\n			]]></search>\n			<add position="before"><![CDATA[		\n\n     public function subdistrict() {\n        $subdistrict_data = array();\n        if(isset($this->request->get[\'city_id\'])) {\n            $subdistrict_data = $this->cache->get(\'hpwd.subdistrict.\'.(int)$this->request->get[\'city_id\']);\n        }\n         \n         if(!$subdistrict_data) {\n            if(isset($this->request->get[\'city_id\'])) {\n            $url="http://pro.rajaongkir.com/api/subdistrict?city=".$this->request->get[\'city_id\'];\n                 \n        $subdistrict_data = $this->requestData($url);\n        if(!empty($subdistrict_data)) {\n            $this->cache->set(\'hpwd.subdistrict.\'.(int)$this->request->get[\'city_id\'], $subdistrict_data);    \n                }\n            }\n        } \n         \n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput($subdistrict_data);\n    }\n    \n   public function city() {\n        $json = array();\n        $city_data = array();\n        if(isset($this->request->get[\'province_id\'])) {\n            $city_data = $this->cache->get(\'hpwd.city.\'.(int)$this->request->get[\'province_id\']);\n        }\n        if(!$city_data) {\n            if(isset($this->request->get[\'province_id\'])) {\n        $city_data = $this->requestData("http://pro.rajaongkir.com/api/city?province=".$this->request->get[\'province_id\']);\n        if(!empty($city_data)) {\n            $this->cache->set(\'hpwd.city.\'.(int)$this->request->get[\'province_id\'], $city_data);     \n                }\n            } \n        } \n    $this->load->model(\'localisation/country\');\n    $country_info = $this->model_localisation_country->getCountry($this->request->get[\'province_id\']);\n        $json = array(\n            \'provinces\' => json_decode($city_data),    \n            \'postcode_required\' => !empty($country_info) ? $country_info[\'postcode_required\'] : \'0\'  \n        );\n\n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput(json_encode($json));\n    }\n    \n    public function province() {\n		$json = array();\n        \n        $json[\'error\'] = \'\';\n        $json[\'sucess\'] = array();\n        \n        $province_data = $this->cache->get(\'hpwd.province\');\n\n        if(!$province_data) {\n            if(isset($this->request->get[\'province_id\'])) {\n            $province_id = $this->request->get[\'province_id\'];\n            $url="http://pro.rajaongkir.com/api/province?id=".$province_id;\n                \n        $province_data = $this->requestData($url);   \n        if(!empty($province_data)) {\n        $this->cache->set(\'hpwd.province\', $province_data);    \n                    }\n                } \n        }    \n	    $this->response->addHeader(\'Content-Type: application/json\');\n		$this->response->setOutput($province_data);\n	}\n    \n   public function requestData($url) {\n        \n        $curl = curl_init();\n\n        curl_setopt_array($curl, array(\n          CURLOPT_URL => $url,\n          CURLOPT_RETURNTRANSFER => true,\n          CURLOPT_ENCODING => "",\n          CURLOPT_MAXREDIRS => 10,\n          CURLOPT_TIMEOUT => 50,\n          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,\n          CURLOPT_CUSTOMREQUEST => "GET",\n          CURLOPT_HTTPHEADER => array(\n            "key:  503a8f186660898c990c11e562435849"\n          ),\n        ));\n\n        $response = curl_exec($curl);\n        $err = curl_error($curl);\n\n        curl_close($curl);\n          if ($err) {\n          return false;\n        } else {\n          return $response;\n        }          \n    }\n   ]]></add>\n		</operation>			\n </file>	 \n<file path="catalog/controller/checkout/register.php">\n  <operation>\n			<search trim="true"><![CDATA[\n		if (isset($this->session->data[\'shipping_address\'][\'postcode\'])) {\n			]]></search>\n			<add position="before"><![CDATA[		\n	if (isset($this->session->data[\'shipping_address\'][\'city\'])) {\n			$data[\'city\'] = $this->session->data[\'shipping_address\'][\'city\'];\n		} else {\n			$data[\'city\'] = \'\';\n		}\n			]]></add>\n    </operation>\n </file>	 \n<file path="catalog/controller/checkout/guest.php">\n     <operation>\n			<search trim="true"><![CDATA[\n        if (isset($this->session->data[\'payment_address\'][\'zone_id\'])) {\n            ]]></search>\n			<add position="before"><![CDATA[\n        if (isset($this->session->data[\'payment_address\'][\'sub_district_id\'])) {\n			$data[\'sub_district_id\'] = $this->session->data[\'payment_address\'][\'sub_district_id\'];\n		} elseif (isset($this->session->data[\'shipping_address\'][\'sub_district_id\'])) {\n			$data[\'sub_district_id\'] = $this->session->data[\'shipping_address\'][\'sub_district_id\'];\n		} else {\n			$data[\'sub_district_id\'] = \'\';\n		}\n            ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n			if (!isset($this->request->post[\'zone_id\']) || $this->request->post[\'zone_id\'] == \'\') {\n            ]]></search>\n			<add position="before"><![CDATA[\n       if (!isset($this->request->post[\'sub_district_id\']) || $this->request->post[\'sub_district_id\'] == \'\') {\n				$json[\'error\'][\'city\'] = $this->language->get(\'error_city\');\n			}\n            ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n$this->session->data[\'payment_address\'][\'zone_id\'] = $this->request->post[\'zone_id\'];            \n            ]]></search>\n			<add position="after"><![CDATA[\n$this->session->data[\'payment_address\'][\'sub_district_id\'] = $this->request->post[\'sub_district_id\'];               \n            ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n$this->session->data[\'shipping_address\'][\'zone_id\'] = $this->request->post[\'zone_id\'];            \n            ]]></search>\n			<add position="after"><![CDATA[\n$this->session->data[\'shipping_address\'][\'sub_district_id\'] = $this->request->post[\'sub_district_id\'];               \n            ]]></add>\n		</operation>\n	 </file>\n<file path="catalog/controller/checkout/guest_shipping.php">\n     <operation>\n			<search trim="true"><![CDATA[\n		if (isset($this->session->data[\'shipping_address\'][\'zone_id\'])) {\n            ]]></search>\n			<add position="before"><![CDATA[\n       	if (isset($this->session->data[\'shipping_address\'][\'sub_district_id\'])) {\n			$data[\'sub_district_id\'] = $this->session->data[\'shipping_address\'][\'sub_district_id\'];\n		} else {\n			$data[\'sub_district_id\'] = \'\';\n		}\n            ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n			if (!isset($this->request->post[\'zone_id\']) || $this->request->post[\'zone_id\'] == \'\') {\n            ]]></search>\n			<add position="before"><![CDATA[\n       if (!isset($this->request->post[\'sub_district_id\']) || $this->request->post[\'sub_district_id\'] == \'\') {\n				$json[\'error\'][\'city\'] = $this->language->get(\'error_city\');\n			}\n            ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n$this->session->data[\'shipping_address\'][\'zone_id\'] = $this->request->post[\'zone_id\'];            \n            ]]></search>\n			<add position="after"><![CDATA[\n$this->session->data[\'shipping_address\'][\'sub_district_id\'] = $this->request->post[\'sub_district_id\'];               \n            ]]></add>\n		</operation>\n	 </file>\n	<file path="catalog/controller/checkout/payment_address.php">\n  <operation>\n			<search trim="true"><![CDATA[\n		if (isset($this->session->data[\'payment_address\'][\'zone_id\'])) {\n			]]></search>\n			<add position="before"><![CDATA[		\n	if (isset($this->session->data[\'payment_address\'][\'city\'])) {\n			$data[\'city\'] = $this->session->data[\'payment_address\'][\'city\'];\n		} else {\n			$data[\'city\'] = \'\';\n		}\n			]]></add>\n    </operation>\n </file>	  		 	\n <file path="catalog/controller/checkout/shipping_address.php">\n  <operation>\n			<search trim="true"><![CDATA[\n		if (isset($this->session->data[\'shipping_address\'][\'postcode\'])) {\n			]]></search>\n			<add position="before"><![CDATA[		\n	if (isset($this->session->data[\'shipping_address\'][\'city\'])) {\n			$data[\'city\'] = $this->session->data[\'shipping_address\'][\'city\'];\n		} else {\n			$data[\'city\'] = \'\';\n		}\n			]]></add>\n    </operation>\n </file>	\n\n    \n  <file path="catalog/controller/account/account.php">\n		<operation>\n			<search trim="true"><![CDATA[\n	public function country() {\n			]]></search>\n			<add position="before" offset="1"><![CDATA[		\npublic function zone() {\n		$json = array();\n		\n		$this->load->model(\'localisation/zone\');\n\n    	$zone_info = $this->model_localisation_zone->getZone($this->request->get[\'zone_id\']);\n		\n		if ($zone_info) {\n			$this->load->model(\'localisation/kecamatan\');\n\n			$json = array(\n				\'zone_id\'        => $zone_info[\'zone_id\'],\n				\'name\'              => $zone_info[\'name\'],\n				\'kecamatan\'         => $this->model_localisation_kecamatan->getKecamatansByKabupatenId($this->request->get[\'zone_id\']),\n				\'status\'            => $zone_info[\'status\']		\n			);\n		}\n		$this->response->addHeader(\'Content-Type: application/json\');		\n		$this->response->setOutput(json_encode($json));\n	}	\n			]]></add>\n		</operation>			\n </file>	 \n \n<file path="catalog/controller/account/address.php">\n  <operation>\n			<search trim="true"><![CDATA[\n				\'zone\'      => $result[\'zone\'],\n			]]></search>\n			<add position="replace"><![CDATA[		\n				\'zone\'      => $this->cart->getCityName($result[\'zone_id\'],$result[\'country_id\']),\n			]]></add>\n		</operation>	\n </file>	\n    \n    <file path="catalog/controller/account/order.php">\n  <operation>\n			<search trim="true"><![CDATA[\n				\'zone\'      => $order_info[\'payment_zone\'],\n			]]></search>\n			<add position="replace"><![CDATA[		\n				\'zone\'      => $this->cart->getCityName($order_info[\'payment_zone_id\'],$order_info[\'payment_country_id\']),\n			]]></add>\n		</operation>\n        <operation>\n			<search trim="true"><![CDATA[\n				\'zone\'      => $order_info[\'shipping_zone\'],\n			]]></search>\n			<add position="replace"><![CDATA[		\n				\'zone\'      => $this->cart->getCityName($order_info[\'shipping_zone_id\'],$order_info[\'shipping_country_id\']),\n			]]></add>\n		</operation>\n </file>	 \n\n<file path="catalog/controller/api/shipping.php">\n    <operation>\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $this->request->post[\'country_id\'],\n            ]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $this->request->post[\'sub_district_id\'],]]></add>\n		</operation>\n    </file>    \n<file path="catalog/controller/checkout/shipping.php">\n	 <operation>\n			<search trim="true"><![CDATA[\n			$zone_info = $this->model_localisation_zone->getZone($this->request->post[\'zone_id\']);\n			]]></search>\n			<add position="before"><![CDATA[\n		$json[\'text_day\'] = $this->language->get(\'text_day\');\n			$json[\'text_kg\'] = $this->language->get(\'text_kg\');\n			]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n			$sort_order = array();\n            ]]></search>\n			<add position="after"><![CDATA[\n   if(isset($this->session->data[\'shipping_address\'][\'postcode\'])) {\n            $this->session->data[\'shipping_address\'][\'postcode\']=\'\';\n            }\n			]]></add>\n		</operation>\n     <operation>\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $this->request->post[\'country_id\'],\n            ]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $this->request->post[\'sub_district_id\'],]]></add>\n		</operation>\n     <operation>\n			<search trim="true"><![CDATA[\n			$json[\'error\'][\'postcode\'] = $this->language->get(\'error_postcode\');\n			]]></search>\n			<add position="replace"><![CDATA[\n            ]]></add>\n		</operation>\n	 </file>\n	\n	\n<file path="catalog/view/theme/*/template/checkout/register.tpl">\n      <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/checkout/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n 	 <operation>\n			<search trim="true"><![CDATA[\n$(\'#collapse-payment-address select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[		\n$(\'#collapse-payment-address input[name=\\\'city\\\']\').parent().hide();\n\nhtml = \'<div class="form-group required"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml += \'<label class="control-label" for="input-payment-city"><?php echo $entry_city; ?></label>\';\nhtml += \'<select name="sub_district_id" id="input-payment-sub-district" class="form-control">\';\nhtml += \'</select></div>\';\n\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').parent().after(html);\n\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#collapse-payment-address .fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n		  $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#collapse-payment-address input[name=\\\'city\\\']\').val(the_value);\n            });\n         }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#collapse-payment-address input[name=\\\'city\\\']\').val(the_value);\n});\n   ]]></add>\n		</operation>\n  <operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\n      if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {            \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');      \n            });		\n        }\n            ]]></add>\n		</operation>  \n</file>\n<file path="catalog/view/theme/*/template/checkout/payment_address.tpl">\n          <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/checkout/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="12"><![CDATA[	\n    if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {\n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n               if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n            });		\n         }\n            ]]></add>\n		</operation> \n<operation>\n    <search trim="true"><![CDATA[\n    $(\'#collapse-payment-address select[name=\\\'country_id\\\']\').on(\'change\', function() {        \n            ]]></search>\n			<add position="before"><![CDATA[		\n$(\'#collapse-payment-address input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'</div></div>\';\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#collapse-payment-address .fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n			$.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });          \n            $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#collapse-payment-address input[name=\\\'sub_district_id\\\']\').val(the_value);\n            });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n$(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#collapse-payment-address input[name=\\\'city\\\']\').val(the_value);\n});\n			]]></add>\n		</operation>\n   </file>\n<file path="catalog/view/theme/*/template/checkout/guest.tpl">\n        <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/checkout/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n       <operation>\n			<search trim="true"><![CDATA[\n$(\'#collapse-payment-address select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[		\n$(\'#collapse-payment-address input[name=\\\'city\\\']\').parent().hide();\n\nhtml = \'<div class="form-group required"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml += \'<label class="control-label" for="input-payment-sub-district"><?php echo $entry_city; ?></label>\';\nhtml += \'<select name="sub_district_id" id="input-payment-sub-district" class="form-control">\';\nhtml += \'</select></div>\';\n\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').parent().after(html);\n\n$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#collapse-payment-address .fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n		  $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#collapse-payment-address input[name=\\\'city\\\']\').val(the_value);\n            });\n        }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'#collapse-payment-address select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#collapse-payment-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#collapse-payment-address input[name=\\\'city\\\']\').val(the_value);\n}); ]]></add>\n		</operation>\n    <operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[\n if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {            \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n               if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').html(html);\n            $(\'#collapse-payment-address select[name=\\\'zone_id\\\']\').trigger(\'change\');      \n            });	\n    }\n            ]]></add>\n		</operation>     \n    </file>\n  <file path="catalog/view/theme/*/template/checkout/guest_shipping.tpl">\n <operation>\n			<search trim="true"><![CDATA[\n$(\'#collapse-shipping-address select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'#collapse-shipping-address input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'</div></div>\';\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#collapse-shipping-address .fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n			$.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });          \n            $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#collapse-shipping-address input[name=\\\'sub_district_id\\\']\').val(the_value);\n            });\n        }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n$(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#collapse-shipping-address input[name=\\\'city\\\']\').val(the_value);\n});            ]]></add>\n		</operation>   	\n <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/checkout/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n      <operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[\nif(json[\'provinces\'][\'rajaongkir\'][\'results\']) {            \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n               if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').html(html);\n            $(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').trigger(\'change\');      \n            });		\n        }\n            ]]></add>\n		</operation>  \n   </file>\n   \n <file path="catalog/view/theme/*/template/affiliate/register.tpl">\n        <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=affiliate/register/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	\n       <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'<?php if ($error_city) { ?> <div class="text-danger"><?php echo $error_city; ?></div><?php } ?></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            \n            $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n	       \n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'input[name=\\\'city\\\']\').val(the_value);\n            \n            }); \n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'city\\\']\').val(the_value);\n});\n]]></add>\n		</operation>      \n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\n          if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {  \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n              if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\');          \n            });			\n         }\n            ]]></add>\n		</operation>     \n   </file>\n    \n<file path="catalog/model/account/customer.php">\n    <operation>\n			<search trim="true"><![CDATA[\n		      $address_id = $this->db->getLastId();\n			]]></search>\n			<add position="after"><![CDATA[	\n		$this->db->query("UPDATE " . DB_PREFIX . "address SET sub_district_id = \'" . (int)$data[\'sub_district_id\'] . "\' WHERE address_id = \'" . (int)$address_id . "\'");\n			]]></add>\n		</operation> \n</file>\n    \n    <file path="catalog/model/checkout/order.php">\n    <operation>\n			<search trim="true"><![CDATA[\n				\'payment_zone\'            => $order_query->row[\'payment_zone\'],\n			]]></search>\n			<add position="replace"><![CDATA[	\n				\'payment_zone\'            => $this->cart->getCityName($order_query->row[\'payment_zone_id\'],$order_query->row[\'payment_country_id\']),\n			]]></add>\n		</operation> \n        <operation>\n			<search trim="true"><![CDATA[\n				\'shipping_zone\'           => $order_query->row[\'shipping_zone\'],\n			]]></search>\n			<add position="replace"><![CDATA[	\n				\'shipping_zone\'            => $this->cart->getCityName($order_query->row[\'shipping_zone_id\'],$order_query->row[\'shipping_country_id\']),\n			]]></add>\n		</operation> \n</file>\n    \n<file path="catalog/model/account/address.php">\n    <operation>\n			<search trim="true"><![CDATA[\n		      $address_id = $this->db->getLastId();\n			]]></search>\n			<add position="after"><![CDATA[	\n		$this->db->query("UPDATE " . DB_PREFIX . "address SET sub_district_id = \'" . (int)$data[\'sub_district_id\'] . "\' WHERE address_id = \'" . (int)$address_id . "\'");\n			]]></add>\n		</operation>\n    <operation error="skip">\n			<search trim="true"><![CDATA[\n	public function editAddress($address_id, $data) {\n            ]]></search>\n			<add position="after" offset="1"><![CDATA[	\n$this->db->query("UPDATE " . DB_PREFIX . "address SET sub_district_id = \'" . (int)$data[\'sub_district_id\'] . "\' WHERE address_id  = \'" . (int)$address_id . "\'");\n   ]]></add>\n		</operation> \n    <operation>\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $address_query->row[\'country_id\'],\n			]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $address_query->row[\'sub_district_id\'],]]></add>\n		</operation> \n    <operation>\n			<search trim="true"><![CDATA[\n				\'zone_id\'        => $result[\'zone_id\'],\n			]]></search>\n			<add position="replace" offset="1"><![CDATA[\'zone_id\'        => $result[\'zone_id\'],\n                \'zone\'        => $this->cart->getCityName($result[\'zone_id\'],$result[\'country_id\']),]]></add>\n		</operation> \n    <operation>\n			<search trim="true"><![CDATA[\n            \'zone_id\'        => $address_query->row[\'zone_id\'],\n            ]]></search>\n			<add position="replace" offset="1"><![CDATA[\'zone_id\'      => $address_query->row[\'zone_id\'],\n            \'zone\'        => $this->cart->getCityName($address_query->row[\'zone_id\'],$address_query->row[\'country_id\']),]]></add>\n		</operation> \n</file>\n <file path="catalog/view/theme/*/template/account/address_form.tpl">\n        <operation>\n			<search trim="true"><![CDATA[\n    url: \'index.php?route=account/account/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	\n       <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'<?php if ($error_city) { ?> <div class="text-danger"><?php echo $error_city; ?></div><?php } ?></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            \n            $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n	       \n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'input[name=\\\'city\\\']\').val(the_value);\n            \n            });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'city\\\']\').val(the_value);\n});\n]]></add>\n		</operation> \n     \n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\nif(json[\'provinces\'][\'rajaongkir\'][\'results\']) {            \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n             \n            });	\n     }\n            ]]></add>\n		</operation> \n    </file>\n\n<file path="catalog/view/theme/*/template/account/register.tpl">\n        <operation>\n			<search trim="true"><![CDATA[\n    url: \'index.php?route=account/account/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	\n       <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'<?php if ($error_city) { ?> <div class="text-danger"><?php echo $error_city; ?></div><?php } ?></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\n    if(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            \n            $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n	       \n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'input[name=\\\'city\\\']\').val(the_value);\n            \n            });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'city\\\']\').val(the_value);\n});\n]]></add>\n		</operation> \n     \n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\n        if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {            \n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n               if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n                $(\'select[name=\\\'zone_id\\\']\').html(html);\n                $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n            });	\n        }\n            ]]></add>\n		</operation>     \n   </file>\n    \n    <file path="catalog/view/theme/*/template/checkout/shipping.tpl">  \n         <operation>\n			<search trim="true"><![CDATA[\n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n			]]></search>\n			<add position="replace"><![CDATA[	\n			]]></add>\n		</operation> \n        <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/shipping/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n\n        <operation>\n			<search trim="true"><![CDATA[\n			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="12"><![CDATA[	\n          if(json[\'provinces\'][\'rajaongkir\'][\'results\']) {\n             $.map(json[\'provinces\'], function(item) {\n			 html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n                $(\'select[name=\\\'zone_id\\\']\').html(html);\n                $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n                });			\n            }\n            ]]></add>\n		</operation>  \n         \n   <operation>\n			<search trim="true"><![CDATA[\n			html += json[\'shipping_method\'][i][\'quote\'][j][\'title\'] + \' - \' + json[\'shipping_method\'][i][\'quote\'][j][\'text\'] + \'</label></div>\';\n			]]></search>\n			<add position="replace"><![CDATA[		\n                var str = json[\'shipping_method\'][i][\'quote\'][j][\'code\'];\n                var shipping_code=str.substring(0,3);\n                var image = str.split(\'.\');\n   \n			if ((shipping_code == "pos") || (shipping_code == "jne") || (shipping_code == "tik")) {\n				html += \'<img src="catalog/view/theme/default/image/shipping/\' + image[0] + \'.png" /><br />\' + json[\'shipping_method\'][i][\'quote\'][j][\'title\'] + \' - \' + json[\'shipping_method\'][i][\'quote\'][j][\'text\'] + \' ( \' + json[\'shipping_method\'][i][\'quote\'][j][\'weight\']  + \'\' + json[\'text_kg\']  + \', \' + json[\'shipping_method\'][i][\'quote\'][j][\'etd\']  + \'\' + json[\'text_day\'] + \') </label></div>\';		\n			} else {\n				html += json[\'shipping_method\'][i][\'quote\'][j][\'title\'] + \' - \' + json[\'shipping_method\'][i][\'quote\'][j][\'text\'] + \'</label></div>\';\n			}				\n			]]></add>\n		</operation>\n	\n       <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'input[name=\\\'postcode\\\']\').parent().parent().hide();\nhtml = \'<div class="form-group required"><input type="hidden" name="postcode" value="<?php echo $postcode; ?>" />\';\nhtml += \'<label class="col-sm-2 control-label" for="input-sub-district"><?php echo $entry_postcode; ?></label>\';\nhtml += \'<div class="col-sm-10"><select name="sub_district_id" id="input-sub-district" class="form-control"></select></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {    \n    if(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n			html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $postcode; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            \n            $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n\n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'input[name=\\\'postcode\\\']\').val(the_value);\n\n            });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'postcode\\\']\').val(the_value);\n});\n   ]]></add>\n		</operation> \n         \n         <operation>\n			<search trim="true"><![CDATA[\n		data: \'country_id=\' + $(\'select[name=\\\'country_id\\\']\').val() + \'&zone_id=\' + $(\'select[name=\\\'zone_id\\\']\').val() + \'&postcode=\' + encodeURIComponent($(\'input[name=\\\'postcode\\\']\').val()),\n			]]></search>\n			<add position="replace"><![CDATA[		\n		data: \'sub_district_id=\' + $(\'select[name=\\\'sub_district_id\\\']\').val() + \'&country_id=\' + $(\'select[name=\\\'country_id\\\']\').val() + \'&zone_id=\' + $(\'select[name=\\\'zone_id\\\']\').val() + \'&postcode=\' + encodeURIComponent($(\'select[name=\\\'postcode\\\']\').val()),\n			]]></add>\n</operation>		\n	 </file>\n    \n<file path="catalog/view/theme/*/template/checkout/shipping_address.tpl">\n          <operation error="skip">\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=checkout/checkout/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	<operation error="skip">\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="12"><![CDATA[	\nif(json[\'provinces\'][\'rajaongkir\'][\'results\']) {\n             $.map(json[\'provinces\'], function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n            });\n   }\n            ]]></add>\n		</operation> \n<operation error="skip">\n    <search trim="true"><![CDATA[\n$(\'#collapse-shipping-address select[name=\\\'country_id\\\']\').on(\'change\', function() {\n            ]]></search>\n			<add position="before"><![CDATA[		\n$(\'#collapse-shipping-address input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'</div></div>\';\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'#collapse-shipping-address select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'#collapse-shipping-address .fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n			$.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });          \n            $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\']\').html(html);\n            var the_value = $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'#collapse-shipping-address input[name=\\\'sub_district_id\\\']\').val(the_value);\n                });\n            }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n$(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'#collapse-shipping-address select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'#collapse-shipping-address input[name=\\\'city\\\']\').val(the_value);\n});\n			]]></add>\n		</operation>\n   </file>\n <file path="catalog/controller/checkout/shipping_method.php">\n  <operation>\n			<search trim="true"><![CDATA[\n		$data[\'text_shipping_method\'] = $this->language->get(\'text_shipping_method\');\n			]]></search>\n			<add position="before"><![CDATA[		\n		$data[\'text_kg\'] = $this->language->get(\'text_kg\');\n		$data[\'text_day\'] = $this->language->get(\'text_day\');\n			]]></add>\n		</operation>\n </file>\n <file path="catalog/view/theme/*/template/checkout/shipping_method.tpl">\n <operation>\n 				<search trim="true"><![CDATA[\n 				    <?php echo $quote[\'title\']; ?> - <?php echo $quote[\'text\']; ?></label>\n 				]]></search>\n 				<add position="replace"><![CDATA[	\n 				<?php  \n            $gambar=explode(".",$quote[\'code\']);	\n                if(file_exists(\'catalog/view/theme/default/image/shipping/\'.$gambar[0].\'.png\')) {  ?>\n						\n		<?php echo "<img src=\'catalog/view/theme/default/image/shipping/".$gambar[0].".png\' /><br />"; ?>\n     	<?php echo $quote[\'title\']; ?> - <?php echo $quote[\'text\']; ?> (<?php echo $quote[\'weight\']." ".$quote[\'text_kg\']; ?>, <?php echo $quote[\'etd\']." ".$quote[\'text_day\']; ?>) </label>\n     <?php } else { ?>			    \n    <?php echo $quote[\'title\']; ?> - <?php echo $quote[\'text\']; ?></label>\n    <?php } ?>\n 				]]></add>\n		</operation>\n	   </file>\n <file path="catalog/view/theme/*/template/common/header.tpl">\n  <operation>\n    <search trim="true"><![CDATA[\n                </head>\n            ]]></search>\n			<add position="before"><![CDATA[	\n            \n            <style type="text/css">\n            .radio label img {\n            border: 1px solid #BBB;\n            box-shadow: 1px 1px 1px #888;\n            border-radius: 3px;\n            }\n            </style>\n            \n   			]]></add>\n		</operation>\n</file>	\n<file path="catalog/controller/api/payment.php">\n    <operation error="skip">\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $this->request->post[\'country_id\'],\n            ]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $this->request->post[\'sub_district_id\'],]]></add>\n		</operation>\n     <operation error="skip">\n			<search trim="true"><![CDATA[\'zone_id\',]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\',]]></add>\n		</operation>\n    </file>   \n    \n<file path="catalog/controller/api/shipping.php">\n    <operation error="skip">\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $this->request->post[\'country_id\'],\n            ]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $this->request->post[\'sub_district_id\'],]]></add>\n		</operation>\n    </file>   \n</modification>', 1, '2017-01-30 16:43:53', '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_modification` (`id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 'Base Shipping FrontEnd - Addition', 'BaseShippingFrontEndAddition', 'HalalPro Web Design 2.0.3.1', '<b>1.0</b>', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[Base Shipping FrontEnd - Addition]]></name>\n   <code>BaseShippingFrontEndAddition</code>\n    <version><![CDATA[<b>1.0</b>]]></version>\n      <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HalalPro Web Design 2.0.3.1]]></author>\n\n       \n<file path="catalog/model/checkout/order.php">\n    <operation>\n			<search trim="true"><![CDATA[\n        \'payment_zone\'            => $order_query->row[\'payment_zone\'],\n            ]]></search>\n			<add position="replace"><![CDATA[\'payment_zone\'            => $this->cart->getCityName($order_query->row[\'payment_zone_id\'],$order_query->row[\'payment_country_id\']),]]></add>\n		</operation> \n    <operation>\n			<search trim="true"><![CDATA[\n        \'shipping_zone\'           => $order_query->row[\'shipping_zone\'],\n            ]]></search>\n			<add position="replace"><![CDATA[\'shipping_zone\'            => $this->cart->getCityName($order_query->row[\'shipping_zone_id\'],$order_query->row[\'shipping_country_id\']),]]></add>\n		</operation> \n    </file>\n<file path="catalog/view/theme/*/template/affiliate/edit.tpl">\n        <operation>\n			<search trim="true"><![CDATA[\n		url: \'index.php?route=affiliate/edit/country&country_id=\' + this.value,\n			]]></search>\n			<add position="replace"><![CDATA[	\n		url: \'index.php?route=checkout/cart/city&province_id=\' + this.value,\n            ]]></add>\n		</operation> \n	\n       <operation>\n			<search trim="true"><![CDATA[\n$(\'select[name=\\\'country_id\\\']\').on(\'change\', function() {\n			]]></search>\n			<add position="before"><![CDATA[	\n$(\'input[name=\\\'city\\\']\').parent().parent().hide();\nhtml =\'<div class="form-group required">\';\nhtml +=\'<label class="col-sm-2 control-label" for="input-city"><?php echo $entry_city; ?></label>\';\nhtml +=\'<div class="col-sm-10"><input type="hidden" name="city" value="<?php echo $city; ?>" />\';\nhtml +=\'<select name="sub_district_id" class="form-control"><option value=""><?php echo $text_select; ?> </option></select>\';\nhtml += \'<?php if ($error_city) { ?> <div class="text-danger"><?php echo $error_city; ?></div><?php } ?></div></div>\';\n\n$(\'select[name=\\\'zone_id\\\']\').parent().parent().after(html);\n\n$(\'select[name=\\\'zone_id\\\']\').on(\'change\', function() {\n	$.ajax({\n		url: \'index.php?route=checkout/cart/subdistrict&city_id=\' + this.value,\n		dataType: \'json\',\n		beforeSend: function() {\n			$(\'select[name=\\\'zone_id\\\']\').after(\' <i class="fa fa-circle-o-notch fa-spin"></i>\');\n		},\n		complete: function() {\n			$(\'.fa-spin\').remove();\n		},\n		success: function(json) {\nif(json[\'rajaongkir\'][\'results\']) {\n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'subdistrict_name\'] == \'<?php echo $city; ?>\') {\n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\' selected=\'selected\'>" + val[\'subdistrict_name\'] + "</option>";                \n                } else { \n                html+="<option value=\'" + val[\'subdistrict_id\'] + "\'>" + val[\'subdistrict_name\'] + "</option>";\n                  }\n                });\n            \n            $(\'select[name=\\\'sub_district_id\\\']\').html(html);\n	       \n            var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n            $(\'input[name=\\\'city\\\']\').val(the_value);\n            \n            });\n          }\n		},\n		error: function(xhr, ajaxOptions, thrownError) {\n			alert(thrownError + "\\r\\n" + xhr.statusText + "\\r\\n" + xhr.responseText);\n		}\n	});\n});\n\n$(\'select[name=\\\'sub_district_id\\\']\').on(\'change\', function() {\n    var the_value = $(\'select[name=\\\'sub_district_id\\\'] option:selected\').text();\n    $(\'input[name=\\\'city\\\']\').val(the_value);\n});\n]]></add>\n		</operation> \n     \n<operation>\n			<search trim="true"><![CDATA[\n     			if (json[\'zone\'] && json[\'zone\'] != \'\') {\n			]]></search>\n			<add position="replace" offset="14"><![CDATA[	\nif(json[\'rajaongkir\'][\'results\']) {            \n             $.map(json, function(item) {\n                html = \'<option value=""><?php echo $text_select; ?></option>\';\n                $.each(item[\'results\'], function( key, val ) {\n                if(val[\'type\'] == "Kabupaten")\n                  var kab = \'Kab.\';  \n                else \n                    kab = val[\'type\'];\n\n                if(val[\'city_id\'] == \'<?php echo $zone_id; ?>\') {\n                    html+="<option value=\'" + val[\'city_id\'] + "\' selected=\'selected\'>" +  kab + \' \' + val[\'city_name\'] + "</option>";\n                } else {\n                    html+="<option value=\'" + val[\'city_id\'] + "\'>" + kab + \' \' + val[\'city_name\'] + "</option>";\n                 }\n                });\n            \n            $(\'select[name=\\\'zone_id\\\']\').html(html);\n            $(\'select[name=\\\'zone_id\\\']\').trigger(\'change\'); \n             \n            });	\n     }\n            ]]></add>\n		</operation> \n    </file>  \n<file path="catalog/model/affiliate/affiliate.php">\n    <operation>\n			<search trim="true"><![CDATA[\n		$affiliate_id = $this->db->getLastId();\n			]]></search>\n			<add position="after"><![CDATA[	\n		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET sub_district_id = \'" . (int)$data[\'sub_district_id\'] . "\' WHERE affiliate_id = \'" . (int)$affiliate_id . "\'");\n			]]></add>\n		</operation> \n    <operation>\n			<search trim="true" index="0"><![CDATA[\n		$affiliate_id = $this->affiliate->getId();\n			]]></search>\n			<add position="after"><![CDATA[	\n		$this->db->query("UPDATE " . DB_PREFIX . "affiliate SET sub_district_id = \'" . (int)$data[\'sub_district_id\'] . "\' WHERE affiliate_id = \'" . (int)$affiliate_id . "\'");\n			]]></add>\n		</operation> \n</file>\n    \n<file path="catalog/language/english/affiliate/register.php">\n  <operation>\n			<search trim="true"><![CDATA[\n// Error\n			]]></search>\n			<add position="after"><![CDATA[		\n			$_[\'entry_city\']          = \'Sub District\';\n			$_[\'entry_country\']          = \'Province\';\n			$_[\'entry_zone\']             = \'City/Regency\';\n			\n			$_[\'help_city\']        = \'Please type your Sub District\';\n			\n			\n			$_[\'error_country\']          = \'Please select a Province\';\n			$_[\'error_zone\']             = \'Please select a City/Regency\';\n			$_[\'error_city\']        = \'Please select a Sub District\';\n			]]></add>\n		</operation>\n</file>	\n<file path="catalog/language/indonesia/affiliate/register.php">\n  <operation>\n			<search trim="true"><![CDATA[\n// Error\n			]]></search>\n			<add position="after"><![CDATA[		\n			$_[\'entry_country\']          = \'Propinsi\';\n			\n			$_[\'entry_zone\']             = \'Kota/Kabupaten\';\n			$_[\'entry_city\']        = \'Kecamatan\';\n			$_[\'help_postcode\']        = \'ketikkan nama kecamatan\';\n			\n			$_[\'error_city\']          = \'Mohon masukkan kode pos\';\n			$_[\'error_country\']          = \'Silahkan pilih Propinsi\';\n			$_[\'error_zone\']             = \'Silahkan pilih Kota/Kabupaten\';\n			$_[\'error_postcode\']        = \'Silahkan masukkan kecamatan\';\n			]]></add>\n		</operation>\n</file>	\n  \n<file path="catalog/controller/api/shipping.php">\n    <operation>\n			<search trim="true"><![CDATA[\n				\'country_id\'     => $this->request->post[\'country_id\'],\n            ]]></search>\n			<add position="after"><![CDATA[\'sub_district_id\'     => $this->request->post[\'sub_district_id\'],]]></add>\n		</operation>\n     <operation>\n			<search trim="true"><![CDATA[\n				\'zone\'           => $zone,\n            ]]></search>\n			<add position="replace"><![CDATA[\'zone\'           => $this->cart->getCityName($this->request->post[\'zone_id\'],$this->request->post[\'country_id\']),]]></add>\n		</operation>\n    </file>   \n   \n<file path="catalog/controller/checkout/confirm.php">\n  <operation error="skip">\n    <search trim="true"><![CDATA[\n                $this->load->model(\'total/\' . $result[\'code\']);\n            ]]></search>\n			<add position="after"><![CDATA[\n                 \n                if($result[\'code\'] == "sub_total") {\n                    if(isset($this->session->data[\'packing_kayu\']) && $this->session->data[\'packing_kayu\']) {\n                    $this->load->model(\'total/jne_pk\');\n                    $this->{\'model_total_jne_pk\'}->getTotal($order_data[\'totals\'], $total, $taxes);\n                        }\n                    }\n            ]]></add>\n		</operation>\n</file>	\n  <file path="catalog/controller/checkout/shipping_method.php">\n        <operation error="skip">\n			<search trim="true"><![CDATA[\n		$data[\'text_shipping_method\'] = $this->language->get(\'text_shipping_method\');\n			]]></search>\n			<add position="before"><![CDATA[\n$data[\'text_kg\'] = $this->language->get(\'text_kg\');                                            $data[\'text_day\'] = $this->language->get(\'text_day\');\n			]]></add>\n		</operation>\n  <operation error="skip">\n    <search trim="true"><![CDATA[\n              $shipping = explode(\'.\', $this->request->post[\'shipping_method\']);\n            ]]></search>\n			<add position="after"><![CDATA[                      \n            if(substr($shipping[0],0,3) == "jne") {\n                if(isset($this->request->post[\'packing_kayu\'])) {\n                    $this->session->data[\'packing_kayu\'] = 1;\n                } else {\n                    $this->session->data[\'packing_kayu\'] = 0;\n                }\n            }\n            ]]></add>\n		</operation>\n           <operation error="skip">\n			<search trim="true"><![CDATA[\n$data[\'text_loading\'] = $this->language->get(\'text_loading\');			\n    ]]></search>\n			<add position="after"><![CDATA[\n$data[\'text_packing_kayu\'] = $this->language->get(\'text_packing_kayu\');\n$data[\'use_wooden_package\'] = $this->config->get(\'jne_wooden_package\');\n   ]]></add>\n		</operation>\n</file>	\n    <file path="catalog/view/theme/*/template/checkout/shipping_method.tpl">\n  <operation error="skip">\n    <search trim="true"><![CDATA[\n<p><strong><?php echo $text_comments; ?></strong></p> \n           ]]></search>\n			<add position="before" offset="1"><![CDATA[ \n            <?php if($use_wooden_package) { ?> \n           <input type="checkbox" name="packing_kayu" value="1" /> <?php echo $text_packing_kayu; ?>\n            <?php } ?>\n            ]]></add>\n		</operation>\n</file>	\n<file path="catalog/view/theme/*/template/checkout/checkout.tpl">\n  <operation error="skip">\n    <search trim="true"><![CDATA[\n        data: $(\'#collapse-shipping-method input[type=\\\'radio\\\']:checked, #collapse-shipping-method textarea\'),\n           ]]></search>\n			<add position="replace"><![CDATA[data: $(\'#collapse-shipping-method input[type=\\\'checkbox\\\']:checked, #collapse-shipping-method input[type=\\\'radio\\\']:checked, #collapse-shipping-method textarea\'),\n            ]]></add>\n		</operation>\n</file>	  \n</modification>    ', 1, '2017-01-30 16:43:56', '0000-00-00', '0000-00-00', NULL),
(4, 'Local copy OCMOD by iSenseLabs', 'isensealabs_quickfix_ocmod', 'iSenseLabs', '1.3', 'http://isenselabs.com', '<modification>\r\n    <name>Local copy OCMOD by iSenseLabs</name>\r\n	<version>1.3</version>\r\n	<link>http://isenselabs.com</link>\r\n	<author>iSenseLabs</author>\r\n	<code>isensealabs_quickfix_ocmod</code>\r\n\r\n	<file path="admin/controller/extension/installer.php">\r\n		<operation error="skip">\r\n			<search ><![CDATA[\'url\'  => str_replace(\'&amp;\', \'&\', $this->url->link(\'extension/installer/ftp\', \'token=\' . $this->session->data[\'token\'],]]></search>\r\n			<add position="replace"><![CDATA[\'url\'  => str_replace(\'&amp;\', \'&\', $this->url->link(\'extension/installer/localcopy\', \'token=\' . $this->session->data[\'token\'],]]></add>\r\n		</operation>\r\n\r\n		<operation>\r\n			<search><![CDATA[public function unzip() {]]></search>\r\n			<add position="before"><![CDATA[			\r\n	public function localcopy() {\r\n		$this->load->language(\'extension/installer\');\r\n\r\n		$json = array();\r\n\r\n		if (!$this->user->hasPermission(\'modify\', \'extension/installer\')) {\r\n			$json[\'error\'] = $this->language->get(\'error_permission\');\r\n		}\r\n\r\n		if (VERSION == \'2.0.0.0\') {\r\n		    $directory = DIR_DOWNLOAD  . str_replace(array(\'../\', \'..\\\\\', \'..\'), \'\', $this->request->post[\'path\']) . \'/upload/\';\r\n		} else {\r\n		    $directory = DIR_UPLOAD  . str_replace(array(\'../\', \'..\\\\\', \'..\'), \'\', $this->request->post[\'path\']) . \'/upload/\';\r\n		}\r\n\r\n		if (!is_dir($directory)) {\r\n			$json[\'error\'] = $this->language->get(\'error_directory\');\r\n		}\r\n\r\n		if (!$json) {\r\n			// Get a list of files ready to upload\r\n			$files = array();\r\n\r\n			$path = array($directory . \'*\');\r\n\r\n			while (count($path) != 0) {\r\n				$next = array_shift($path);\r\n\r\n				foreach (glob($next) as $file) {\r\n					if (is_dir($file)) {\r\n						$path[] = $file . \'/*\';\r\n					}\r\n\r\n					$files[] = $file;\r\n				}\r\n			}\r\n\r\n			$root = dirname(DIR_APPLICATION).\'/\';\r\n\r\n			foreach ($files as $file) {\r\n				// Upload everything in the upload directory\r\n				$destination = substr($file, strlen($directory));\r\n\r\n				// Update from newer OpenCart versions:\r\n				if (substr($destination, 0, 5) == \'admin\') {\r\n					$destination = DIR_APPLICATION . substr($destination, 5);\r\n				} else if (substr($destination, 0, 7) == \'catalog\') {\r\n					$destination = DIR_CATALOG . substr($destination, 7);\r\n				} else if (substr($destination, 0, 5) == \'image\') {\r\n					$destination = DIR_IMAGE . substr($destination, 5);\r\n				} else if (substr($destination, 0, 6) == \'system\') {\r\n					$destination = DIR_SYSTEM . substr($destination, 6);\r\n				} else {\r\n					$destination = $root.$destination;\r\n				}\r\n\r\n				if (is_dir($file)) {\r\n					if (!file_exists($destination)) {\r\n						if (!mkdir($destination)) {\r\n							$json[\'error\'] = sprintf($this->language->get(\'error_ftp_directory\'), $destination);\r\n						}\r\n					}\r\n				}\r\n\r\n				if (is_file($file)) {\r\n					if (!copy($file, $destination)) {\r\n						$json[\'error\'] = sprintf($this->language->get(\'error_ftp_file\'), $file);\r\n					}\r\n				}\r\n			}\r\n		}\r\n\r\n		$this->response->addHeader(\'Content-Type: application/json\');\r\n		$this->response->setOutput(json_encode($json));\r\n	}]]></add>\r\n		</operation>\r\n	</file>	\r\n</modification>\r\n', 1, '2017-01-30 16:45:47', '0000-00-00', '0000-00-00', NULL),
(5, 'Admin Core Menu', 'AdminCoreMenuHPWD', 'HP Web Design', '1.1', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[Admin Core Menu]]></name>\n   <code>AdminCoreMenuHPWD</code>\n    <version>1.1</version>\n    <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design]]></author>\n\n	<file path="admin/view/template/common/menu.tpl">\n		<operation>\n			<search trim="true"><![CDATA[\n           <ul id="menu">\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n			<li id="extension"> <a class="parent"><i class="fa fa-puzzle-piece fa-fw"></i><span>HP Web Design</span></a>\n				<ul class="halalmenu">\n\n				</ul>\n			</li>			\n			]]></add>\n		</operation>\n	</file>\n</modification>\n', 1, '2017-01-30 16:46:08', '0000-00-00', '0000-00-00', NULL),
(6, 'JNE Shipping BackEnd Menu', 'JNEShippingMenuBackEnd', 'HP Web Design', '<b>1.0</b>', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[JNE Shipping BackEnd Menu]]></name>\n   <code>JNEShippingMenuBackEnd</code>\n    <version><![CDATA[<b>1.0</b>]]></version>\n      <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design]]></author>\n\n<file path="admin/language/english/common/menu.php">\n  <operation>\n			<search trim="true"><![CDATA[// Text]]></search>\n		<add position="after" offset="1"><![CDATA[	\n			$_[\'text_country\']          = \'Province\';\n			$_[\'text_zone\']             = \'City/Regency\';\n			$_[\'text_city\']        = \'Sub District\';\n			$_[\'text_jne_shipping\']         = \'JNE Shipping\';\n			]]></add>\n		</operation>\n</file>\n\n<file path="admin/language/indonesia/common/menu.php">\n  <operation>\n			<search trim="true"><![CDATA[// Text]]></search>\n			<add position="after" offset="1"><![CDATA[\n			$_[\'text_country\']          = \'Propinsi\';\n			$_[\'text_zone\']             = \'Kota/Kabupaten\';\n			$_[\'text_city\']        = \'Kecamatan\';\n			$_[\'text_jne_shipping\']         = \'Pengiriman JNE\';\n			]]></add>\n		</operation>\n</file>\n\n<file path="admin/controller/common/menu.php">\n <operation>\n            <search><![CDATA[$this->load->language(\'common/menu\');]]></search>\n            <add position="after"><![CDATA[\n            	$data[\'text_jne_shipping\'] = $this->language->get(\'text_jne_shipping\');\n			if(isset($this->session->data[\'token\'])) {\n		$data[\'jne_setting\'] = $this->url->link(\'module/jne_setting\', \'token=\' . $this->session->data[\'token\'], true);   \n		}\n            ]]></add>\n        </operation>\n </file>\n\n	<file path="admin/view/template/common/menu.tpl">\n		<operation>\n			<search trim="true"><![CDATA[\n			<ul class="halalmenu">\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n					<li id="jne"><a href="<?php echo $jne_setting; ?>"><?php echo $text_jne_shipping; ?></a></li>		\n			]]></add>\n		</operation>\n	</file>\n    \n <file path="catalog/controller/account/shippingcost.php">\n		<operation>\n			<search trim="true"><![CDATA[$data[\'heading_title\'] = $this->language->get(\'heading_title\');]]></search>\n			<add position="after"><![CDATA[\n    	$data[\'entry_jne_shipping_method\'] = $this->language->get(\'entry_jne_shipping_method\');\n\n        $data[\'text_jneyes\'] = $this->language->get(\'text_jneyes\');\n    	$data[\'text_jneoke\'] = $this->language->get(\'text_jneoke\');\n    	$data[\'text_jnereg\'] = $this->language->get(\'text_jnereg\');\n    	$data[\'text_jnetrucking\'] = $this->language->get(\'text_jnetrucking\');\n			]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/language/english/account/shippingcost.php">\n	<operation>\n				<search trim="true"><![CDATA[\n// Text\n            ]]></search>\n		<add position="after"><![CDATA[\n            $_[\'entry_jne_shipping_method\']               = \'JNE Shipping Method\';  \n\n            $_[\'text_jneyes\']               = \'JNE YES\';  \n            $_[\'text_jneoke\']               = \'JNE OKE\';  \n            $_[\'text_jnereg\']               = \'JNE Regular\';\n            $_[\'text_jnetrucking\']               = \'JNE Trucking\';\n]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/language/indonesia/account/shippingcost.php">\n	<operation>\n				<search trim="true"><![CDATA[\n// Text\n            ]]></search>\n		<add position="after"><![CDATA[\n$_[\'entry_jne_shipping_method\']               = \'Metode Pengiriman JNE\';  \n$_[\'text_jneyes\']               = \'JNE YES\';  \n$_[\'text_jneoke\']               = \'JNE OKE\';  \n$_[\'text_jnereg\']               = \'JNE Reguler\';  \n$_[\'text_jnetrucking\']               = \'JNE Trucking\';  \n]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/view/theme/*/template/account/shippingcost.tpl">\n		<operation error="skip">\n			<search trim="true"><![CDATA[\n                <h3><?php echo $text_form_shippingcost; ?></h3>\n                ]]></search>\n			<add position="after"><![CDATA[\n <div class="form-group required">\n            <label class="col-sm-2 control-label"><?php echo $entry_jne_shipping_method; ?></label>\n            <div class="col-sm-10">\n\n    <span class="radio">\n    <label><input type="radio" name="shipping_method" value="jne_trucking"/> <?php echo $text_jnetrucking; ?>  </label>  </span>\n           \n    <span class="radio">\n    <label><input type="radio" name="shipping_method" value="jne_yes"/> <?php echo $text_jneyes; ?>  </label>  </span>\n                \n    	<span class="radio">\n	<label> <input type="radio" name="shipping_method" value="jne_reg"  checked="checked" /> <?php echo $text_jnereg; ?>   </label> </span> \n	<span class="radio">\n		 <label> <input type="radio" name="shipping_method" value="jne_oke" /> <?php echo $text_jneoke; ?>   </label>\n		 </span>		           \n		</div>\n		</div>\n            ]]></add>\n		</operation>\n	</file>\n </modification>', 1, '2017-01-30 16:49:17', '0000-00-00', '0000-00-00', NULL),
(7, '<font color="#0000"><b>Payment Method</font>', 'Payment MethodHPWD', '<font color="#CC0000"><b>HalalPro Web Design</font>', '<b>1.1</b>', 'http://www.halalprowebdesign.com', '<modification>\r\n   <name><![CDATA[<font color="#0000"><b>Payment Method</font>]]></name>\r\n   <code>Payment MethodHPWD</code>\r\n    <version><![CDATA[<b>1.1</b>]]></version>\n    <link>http://www.halalprowebdesign.com</link>\r\n    <author><![CDATA[<font color="#CC0000"><b>HalalPro Web Design</font>]]></author>\n    \n  <file path="catalog/view/theme/*/template/checkout/payment_method.tpl">\n  <operation>\n			<search trim="true"><![CDATA[\n    <?php echo $payment_method[\'title\']; ?>\n			]]></search>\n			<add position="replace"><![CDATA[	\n     <?php echo $payment_method[\'title\']; ?>  \n      <?php   if( (substr($payment_method[\'code\'],0,5) == "bank_") && (substr($payment_method[\'code\'],0,8) != "bank_tra")) { ?>\n      <br />\n   <img src="<?php echo \'catalog/view/theme/default/image/\'.$payment_method[\'code\'].\'_logo.jpg\'; ?>" />\n   	<?php } ?>\n   			]]></add>\n		</operation>\n</file>	\n</modification>', 1, '2017-01-31 09:15:29', '0000-00-00', '0000-00-00', NULL),
(8, 'TIKI Shipping BackEnd Menu', 'TIKIShippingMenuBackEnd', 'HP Web Design', '1.0', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[TIKI Shipping BackEnd Menu]]></name>\n   <code>TIKIShippingMenuBackEnd</code>\n    <version>1.0</version>\n      <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design]]></author>\n\n<file path="admin/language/english/common/menu.php">\n  <operation>\n			<search trim="true"><![CDATA[// Text]]></search>\n		<add position="after" offset="1"><![CDATA[	\n			$_[\'text_country\']               = \'Province\';\n			$_[\'text_zone\']                  = \'City/Regency\';\n			$_[\'text_city\']                  = \'Sub District\';\n			$_[\'text_tiki_shipping\']         = \'TIKI Shipping\';\n			]]></add>\n		</operation>\n</file>\n\n<file path="admin/language/indonesia/common/menu.php">\n  <operation>\n			<search trim="true"><![CDATA[// Text]]></search>\n			<add position="after" offset="1"><![CDATA[\n			$_[\'text_country\']          = \'Propinsi\';\n			$_[\'text_zone\']             = \'Kota/Kabupaten\';\n			$_[\'text_city\']        = \'Kecamatan\';\n			$_[\'text_tiki_shipping\']         = \'Pengiriman TIKI\';\n			]]></add>\n		</operation>\n</file>\n\n<file path="admin/controller/common/menu.php">\n <operation>\n            <search><![CDATA[\n		$data[\'home\'] = $this->url->link(\'common/dashboard\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\n            ]]></search>\n            <add position="before" offset="1"><![CDATA[\n            	$data[\'text_tiki_shipping\'] = $this->language->get(\'text_tiki_shipping\');\n			if(isset($this->session->data[\'token\'])) {\n		$data[\'tiki_setting\'] = $this->url->link(\'module/tiki_setting\', \'token=\' . $this->session->data[\'token\'], \'SSL\');   \n		}\n            ]]></add>\n        </operation>\n </file>\n\n	<file path="admin/view/template/common/menu.tpl">\n		<operation>\n			<search trim="true"><![CDATA[\n			<ul class="halalmenu">\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n					<li id="jne"><a href="<?php echo $tiki_setting; ?>"><?php echo $text_tiki_shipping; ?></a></li>		\n			]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/controller/custom/shippingcost.php">\n		<operation>\n			<search trim="true"><![CDATA[\n    	$data[\'heading_title\'] = $this->language->get(\'heading_title\');\n			]]></search>\n			<add position="after"><![CDATA[\n    	$data[\'entry_tiki_shipping_method\'] = $this->language->get(\'entry_tiki_shipping_method\');\n\n        $data[\'text_tikieco\'] = $this->language->get(\'text_tikieco\');\n    	$data[\'text_tikireg\'] = $this->language->get(\'text_tikireg\');\n    	$data[\'text_tikihds\'] = $this->language->get(\'text_tikihds\');\n    	$data[\'text_tikisds\'] = $this->language->get(\'text_tikisds\');\n    	$data[\'text_tikions\'] = $this->language->get(\'text_tikions\');\n			]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/language/english/custom/shippingcost.php">\n	<operation>\n			<search trim="true"><![CDATA[\n// Text\n            ]]></search>\n		<add position="after"><![CDATA[\n            $_[\'entry_tiki_shipping_method\']               = \'TIKI Shipping Method\';  \n            $_[\'text_tikions\']               = \'TIKI ONS\';  \n            $_[\'text_tikieco\']               = \'TIKI Eco\';  \n            $_[\'text_tikihds\']               = \'TIKI HDS\';\n            $_[\'text_tikisds\']               = \'TIKI SDS\';\n            $_[\'text_tikireg\']               = \'TIKI Regular\';\n            ]]></add>\n		</operation>\n	</file>\n    \n    <file path="catalog/language/indonesia/custom/shippingcost.php">\n	<operation>\n			<search trim="true"><![CDATA[\n// Text\n            ]]></search>\n		<add position="after"><![CDATA[\n            $_[\'entry_tiki_shipping_method\']               = \'Metode Pengiriman TIKI\';  \n            $_[\'text_tikions\']               = \'TIKI ONS\';  \n            $_[\'text_tikieco\']               = \'TIKI ECO\';  \n            $_[\'text_tikihds\']               = \'TIKI HDS\';\n            $_[\'text_tikisds\']               = \'TIKI SDS\';\n            $_[\'text_tikireg\']               = \'TIKI Regular\'; \n            ]]></add>\n		</operation>\n	</file>\n    \n     <file path="catalog/view/theme/*/template/account/shippingcost.tpl">\n		<operation error="skip">\n			<search trim="true"><![CDATA[\n                <h3><?php echo $text_form_shippingcost; ?></h3>\n                ]]></search>\n			<add position="after"><![CDATA[\n <div class="form-group required">\n            <label class="col-sm-2 control-label"><?php echo $entry_tiki_shipping_method; ?></label>\n            <div class="col-sm-10">\n\n    <span class="radio">\n    <label><input type="radio" name="shipping_method" value="tiki_eco"/> <?php echo $text_tikieco; ?>  </label>  </span>\n                \n    	<span class="radio">\n	<label> <input type="radio" name="shipping_method" value="tiki_reg"  checked="checked" /> <?php echo $text_tikireg; ?>   </label> </span> \n	<span class="radio">\n		 <label> <input type="radio" name="shipping_method" value="tiki_sds" /> <?php echo $text_tikisds; ?>   </label>\n    </span>	\n    <span class="radio">\n		 <label> <input type="radio" name="shipping_method" value="tiki_hds" /> <?php echo $text_tikihds; ?>   </label>\n    </span>\n        <span class="radio">\n		 <label> <input type="radio" name="shipping_method" value="tiki_ons" /> <?php echo $text_tikions; ?>   </label>\n    </span>	\n		</div>\n</div>\n            ]]></add>\n		</operation>\n	</file>\n </modification>', 1, '2017-01-31 14:34:28', '0000-00-00', '0000-00-00', NULL),
(9, 'Payment Confirmation', 'PaymentConfirmationHPWD', 'HP Web Design', '1.1', 'http://www.halalprowebdesign.com', '<modification>\n   <name><![CDATA[Payment Confirmation]]></name>\n   <code>PaymentConfirmationHPWD</code>\n    <version><![CDATA[1.1]]></version>\n    <link>http://www.halalprowebdesign.com</link>\n    <author><![CDATA[HP Web Design]]></author>\n\n	 <!-- language override-->\n  \n    <file path="admin/language/english/common/menu.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[\n   		<?php\n			]]></search>\n			<add position="after"><![CDATA[		\n		 $_[\'text_confirm_bayar\']   = \'Payment Confirmation\';\n	 	 $_[\'text_confirm_setting\']   = \'Setting\';\n	 	 $_[\'text_confirm_list\']   = \'Confirmation List\';\n			]]></add>\n		</operation>\n</file>\n\n   <file path="admin/language/indonesia/common/menu.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[\n   		<?php\n			]]></search>\n			<add position="after" offset="1"><![CDATA[		\n	 $_[\'text_confirm_bayar\']   = \'Konfirmasi Pembayaran\';\n	 $_[\'text_confirm_setting\']   = \'Seting\';\n	 $_[\'text_confirm_list\']   = \'Daftar Konfirmasi\';\n			]]></add>\n		</operation>\n </file>\n\n<!-- menu override-->\n	<file path="admin/view/template/common/menu.tpl">\n		<operation error="skip">\n			<search trim="true"><![CDATA[\n        <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n				  <li><a href="<?php echo $confirm_list; ?>"><?php echo $text_confirm_bayar; ?></a></li>			\n			]]></add>\n		</operation>\n </file>\n \n<!-- menu.php override-->\n	<file path="admin/view/template/common/menu.tpl">\n		<operation error="skip">\n			<search trim="true"><![CDATA[\n			<ul class="halalmenu">\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n<li> <a class="parent"><?php echo $text_confirm_bayar; ?></a>\n					<ul><li><a href="<?php echo $confirm_setting; ?>"><?php echo $text_confirm_setting; ?></a></li>\n				  <li><a href="<?php echo $confirm_list; ?>"><?php echo $text_confirm_list; ?></a></li>			\n					</ul>\n			</li>\n			]]></add>\n		</operation>\n	</file>\n\n<file path="admin/controller/common/menu.php">\n		<operation error="skip">\n			<search trim="true"><![CDATA[\n		$data[\'text_openbay_order_import\'] = $this->language->get(\'text_openbay_order_import\');\n			]]></search>\n			<add position="after" offset="1"><![CDATA[\n		  $data[\'text_confirm_bayar\'] = $this->language->get(\'text_confirm_bayar\');\n			$data[\'text_confirm_setting\'] = $this->language->get(\'text_confirm_setting\');\n			$data[\'text_confirm_list\'] = $this->language->get(\'text_confirm_list\');\n\n			$data[\'confirm_list\'] = $this->url->link(\'sale/confirm\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\n			$data[\'confirm_setting\'] = $this->url->link(\'module/confirm_setting\', \'token=\' . $this->session->data[\'token\'], \'SSL\');\n			\n			]]></add>\n		</operation>\n </file>\n <!-- add front end menu-->\n   <file path="catalog/language/indonesia/common/header.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Text]]></search>\n			<add position="after"><![CDATA[	\n	 $_[\'text_confirm\']   = \'Konfirmasi Pembayaran\';\n			]]></add>\n		</operation>\n </file>\n    <file path="catalog/language/english/common/header.php">\n  <operation error="skip">\n				<search trim="true"><![CDATA[\n   		// Text\n			]]></search>\n			<add position="after"><![CDATA[	\n	 $_[\'text_confirm\']   = \'Payment Confirmation\';\n			]]></add>\n		</operation>\n </file>\n <!-- add front end menu-->\n   <file path="catalog/language/indonesia/common/header.php,catalog/language/indonesia/account/account.php,catalog/language/indonesia/module/account.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[// Text]]></search>\n			<add position="after"><![CDATA[	\n	 $_[\'text_confirm_payment\']   = \'Konfirmasi Pembayaran\';\n			]]></add>\n		</operation>\n </file>\n    <file path="catalog/language/english/common/header.php,catalog/language/english/account/account.php,catalog/language/english/module/account.php">\n  <operation error="skip">\n				<search trim="true"><![CDATA[// Text]]></search>\n			<add position="after"><![CDATA[	\n	 $_[\'text_confirm_payment\']   = \'Payment Confirmation\';\n			]]></add>\n		</operation>\n </file>  \n    <file path="catalog/controller/common/header.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[$this->load->language(\'common/header\');]]></search>\n			<add position="after"><![CDATA[	\n    	$data[\'confirm\'] = $this->url->link(\'account/confirm\', \'\', \'SSL\');\n    	$data[\'text_confirm\'] = $this->language->get(\'text_confirm\');\n    	$data[\'confirm_status\'] = $this->config->get(\'confirm_status\');\n			]]></add>\n		</operation>\n </file>\n \n<file path="catalog/view/theme/*/template/common/header.tpl">\n  <operation error="skip">\n			<search trim="true"><![CDATA[\n        <li><a href="<?php echo $contact; ?>"><i class="fa fa-phone"></i></a> <span class="hidden-xs hidden-sm hidden-md"><?php echo $telephone; ?></span></li>\n			]]></search>\n			<add position="before"><![CDATA[\n		<?php if($confirm_status) { ?>	\n 		<li><a href="<?php echo $confirm; ?>" title="<?php echo $text_confirm; ?>"><i class="fa fa-credit-card"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_confirm; ?></span></a></li>\n		<?php } ?>			\n			]]></add>\n		</operation>\n		\n		  <operation error="skip">\n			<search trim="true"><![CDATA[\n				</head>\n			]]></search>\n			<add position="before"><![CDATA[\n			<style type="text/css">\n				#bank_transfer { text-transform: uppercase;}\n			</style>\n			]]></add>\n		</operation>	\n </file>\n\n<file path="catalog/language/english/account/login.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[<?php]]></search>\n			<add position="after"><![CDATA[\n	  	 $_[\'text_login_confimation\']               = \'Login Confirmation\';\n	  	 $_[\'text_login_confirmation_intrusction\']   = \'Please login first before confirming your payment. <br/> Once you have logged in, you can confirm the payment with filling the available form.\';\n			]]></add>\n		</operation>\n </file>\n    <file path="catalog/language/indonesia/account/login.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[<?php]]></search>\n			<add position="after"><![CDATA[\n	  	 $_[\'text_login_confimation\']               = \'Login Konfirmasi\';\n	  	 $_[\'text_login_confirmation_intrusction\']   = \'Silakan login terlebih dahulu sebelum melakukan confirm pembayaran. <br /> Setelah login Anda dapat mengconfirm pembayaran Anda dengan form yang tersedia.\';\n			]]></add>\n		</operation>\n </file>\n<file path="catalog/view/theme/default/template/account/login.tpl">\n  <operation error="skip">\n			<search trim="true" index="0"><![CDATA[\n<div class="col-sm-6">\n            ]]></search>\n			<add position="before"><![CDATA[\n	    <div class="col-sm-12" style="margin-bottom: 50px;">\n        <div class="well"><h2><?php echo $text_login_confimation; ?></h2>\n                <p><?php echo $text_login_confirmation_intrusction; ?></p>\n            </div></div>\n			]]></add>\n		</operation>\n    <operation error="skip">\n			<search trim="true"><![CDATA[<?php echo $footer; ?>]]></search>\n			<add position="before"><![CDATA[\n<script type="text/javascript"><!--\n$(\'.breadcrumb\').next().css("margin-top","80px");\n//--></script>\n			]]></add>\n		</operation>\n </file>\n<file path="catalog/view/theme/kingstorepro/template/account/login.tpl">\n  <operation error="skip">\n			<search trim="true" index="0"><![CDATA[\n<div class="row">\n            ]]></search>\n			<add position="before"><![CDATA[\n	    <div class="col-sm-12" style="margin-bottom: 50px;">\n        <div class="well"><h2><?php echo $text_login_confimation; ?></h2>\n                <p><?php echo $text_login_confirmation_intrusction; ?></p>\n            </div></div>\n			]]></add>\n		</operation>\n    <operation error="skip">\n			<search trim="true"><![CDATA[<?php echo $footer; ?>]]></search>\n			<add position="before"><![CDATA[\n<script type="text/javascript"><!--\n$(\'.breadcrumb\').next().css("margin-top","80px");\n//--></script>\n			]]></add>\n		</operation>\n </file>\n <file path="catalog/controller/account/login.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[\n   			$this->load->language(\'account/login\');	\n			]]></search>\n			<add position="after"><![CDATA[	\n            $data[\'text_login_confimation\'] = $this->language->get(\'text_login_confimation\');\n            $data[\'text_login_confirmation_intrusction\'] = $this->language->get(\'text_login_confirmation_intrusction\');\n            ]]></add>\n		</operation>\n </file> \n    <file path="catalog/controller/tool/upload.php">\n  <operation error="skip">\n			<search trim="true"><![CDATA[\n	public function index() {\n            ]]></search>\n			<add position="before"><![CDATA[	\npublic function download() {\n		$this->load->model(\'tool/upload\');\n\n		if (isset($this->request->get[\'code\'])) {\n			$code = $this->request->get[\'code\'];\n		} else {\n			$code = 0;\n		}\n\n		$upload_info = $this->model_tool_upload->getUploadByCode($code);\n\n		if ($upload_info) {\n			$file = DIR_UPLOAD . $upload_info[\'filename\'];\n			$mask = basename($upload_info[\'name\']);\n\n			if (!headers_sent()) {\n				if (is_file($file)) {\n					header(\'Content-Type: application/octet-stream\');\n					header(\'Content-Description: File Transfer\');\n					header(\'Content-Disposition: attachment; filename="\' . ($mask ? $mask : basename($file)) . \'"\');\n					header(\'Content-Transfer-Encoding: binary\');\n					header(\'Expires: 0\');\n					header(\'Cache-Control: must-revalidate, post-check=0, pre-check=0\');\n					header(\'Pragma: public\');\n					header(\'Content-Length: \' . filesize($file));\n\n					readfile($file, \'rb\');\n					exit;\n				} else {\n					exit(\'Error: Could not find file \' . $file . \'!\');\n				}\n			} else {\n				exit(\'Error: Headers already sent out!\');\n			}\n		} else {\n			$this->load->language(\'error/not_found\');\n\n			$this->document->setTitle($this->language->get(\'heading_title\'));\n\n			$data[\'heading_title\'] = $this->language->get(\'heading_title\');\n\n			$data[\'text_not_found\'] = $this->language->get(\'text_not_found\');\n\n			$data[\'breadcrumbs\'] = array();\n\n			$data[\'breadcrumbs\'][] = array(\n				\'text\' => $this->language->get(\'text_home\'),\n				\'href\' => $this->url->link(\'common/dashboard\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n\n			$data[\'breadcrumbs\'][] = array(\n				\'text\' => $this->language->get(\'heading_title\'),\n				\'href\' => $this->url->link(\'error/not_found\', \'token=\' . $this->session->data[\'token\'], \'SSL\')\n			);\n\n			$data[\'header\'] = $this->load->controller(\'common/header\');\n			$data[\'column_left\'] = $this->load->controller(\'common/column_left\');\n			$data[\'footer\'] = $this->load->controller(\'common/footer\');\n\n			$this->response->setOutput($this->load->view(\'error/not_found.tpl\', $data));\n		}\n	}\n            ]]></add>\n		</operation>\n </file>\n     \n<file path="catalog/controller/module/account.php">\n  <operation error="skip">\n      <search trim="true"><![CDATA[$data[\'heading_title\'] = $this->language->get(\'heading_title\');]]></search>\n			<add position="after"><![CDATA[\n            $data[\'text_confirm_payment\'] = $this->language->get(\'text_confirm_payment\');\n            $data[\'confirm_payment\'] = $this->url->link(\'account/confirm\', \'\', true);\n            ]]></add>\n		</operation>\n </file>\n<file path="catalog/controller/account/account.php">\n  <operation error="skip">\n      <search trim="true"><![CDATA[$data[\'heading_title\'] = $this->language->get(\'heading_title\');]]></search>\n			<add position="after"><![CDATA[\n            $data[\'text_confirm_payment\'] = $this->language->get(\'text_confirm_payment\');\n            $data[\'confirm_payment\'] = $this->url->link(\'account/confirm\', \'\', true);\n            ]]></add>\n		</operation>\n </file>\n    \n<file path="catalog/view/theme/*/template/account/account.tpl">\n  <operation error="skip">\n      <search trim="true"><![CDATA[<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>]]></search>\n      <add position="after"><![CDATA[<li><i class="fa fa-credit-card"></i> <a href="<?php echo $confirm_payment; ?>"><?php echo $text_confirm_payment; ?></a></li>]]></add>\n		</operation>\n </file>\n\n<file path="catalog/view/theme/*/template/module/account.tpl">\n  <operation error="skip">\n      <search trim="true"><![CDATA[<div class="list-group">]]></search>\n      <add position="after"><![CDATA[<a class="list-group-item" href="<?php echo $confirm_payment; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_confirm_payment; ?></a>]]></add>\n		</operation>\n </file>\n    \n<file path="catalog/view/theme/supersaver/template/module/account.tpl">\n  <operation error="skip">\n      <search trim="true"><![CDATA[<ul class="list-box">]]></search>\n      <add position="after"><![CDATA[<li><a href="<?php echo $confirm_payment; ?>"><i class="fa fa-credit-card"></i> <?php echo $text_confirm_payment; ?></a></li>]]></add>\n		</operation>\n </file>   \n<file path="catalog/view/theme/*/template/common/_live_search.tpl">\n  <operation error="skip">\n			<search trim="true"><![CDATA[<script>]]></search>\n			<add position="after"><![CDATA[// hpwd]]></add>\n		</operation>\n    </file>\n</modification>', 1, '2017-01-31 14:50:36', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_module`
--

CREATE TABLE `oc_module` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_module`
--

INSERT INTO `oc_module` (`id`, `name`, `code`, `setting`, `created_at`, `updated_at`, `deleted_at`) VALUES
(30, 'Category', 'banner', 'a:5:{s:4:"name";s:17:"Banner - Category";s:9:"banner_id";s:1:"6";s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(29, 'Home Page', 'carousel', 'a:5:{s:4:"name";s:20:"Carousel - Home Page";s:9:"banner_id";s:1:"8";s:5:"width";s:3:"130";s:6:"height";s:3:"100";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(28, 'Featured - Home Page', 'featured', 'a:6:{s:4:"name";s:20:"Featured - Home Page";s:7:"product";a:1:{i:0;s:2:"52";}s:5:"limit";s:1:"6";s:5:"width";s:3:"200";s:6:"height";s:3:"200";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(27, 'Slideshow - Home Page', 'slideshow', 'a:5:{s:4:"name";s:21:"Slideshow - Home Page";s:9:"banner_id";s:1:"7";s:5:"width";s:4:"1000";s:6:"height";s:3:"500";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(31, 'Oleh-oleh', 'featured', 'a:6:{s:4:"name";s:9:"Oleh-oleh";s:7:"product";a:1:{i:0;s:2:"42";}s:5:"limit";s:1:"5";s:5:"width";s:3:"200";s:6:"height";s:3:"200";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(32, 'Latest', 'latest', 'a:5:{s:4:"name";s:6:"Latest";s:5:"limit";s:1:"5";s:5:"width";s:3:"200";s:6:"height";s:3:"200";s:6:"status";s:1:"1";}', '0000-00-00', '0000-00-00', NULL),
(34, 'Featured', 'anylist', 'a:21:{s:4:"name";s:8:"Featured";s:14:"anylist_stores";a:1:{i:0;s:1:"0";}s:5:"title";a:1:{i:2;s:0:"";}s:4:"code";s:0:"";s:9:"titlelink";s:0:"";s:8:"category";s:0:"";s:10:"categories";a:2:{i:0;s:2:"33";i:1;s:2:"29";}s:7:"product";s:0:"";s:8:"products";a:2:{i:0;s:2:"52";i:1;s:2:"51";}s:6:"latest";s:1:"4";s:8:"specials";s:1:"4";s:9:"sortfield";s:0:"";s:5:"width";s:2:"80";s:6:"height";s:2:"80";s:5:"limit";s:1:"8";s:17:"limitmanufacturer";a:6:{i:0;s:1:"8";i:1;s:1:"9";i:2;s:1:"7";i:3;s:1:"5";i:4;s:1:"6";i:5;s:2:"10";}s:13:"limitcategory";a:38:{i:0;s:2:"43";i:1;s:2:"44";i:2;s:2:"45";i:3;s:2:"46";i:4;s:2:"47";i:5;s:2:"48";i:6;s:2:"49";i:7;s:2:"50";i:8;s:2:"35";i:9;s:2:"51";i:10;s:2:"36";i:11;s:2:"52";i:12;s:2:"37";i:13;s:2:"53";i:14;s:2:"38";i:15;s:2:"54";i:16;s:2:"39";i:17;s:2:"55";i:18;s:2:"40";i:19;s:2:"56";i:20;s:2:"41";i:21;s:2:"42";i:22;s:2:"58";i:23;s:2:"28";i:24;s:2:"29";i:25;s:2:"30";i:26;s:2:"31";i:27;s:2:"32";i:28;s:2:"20";i:29;s:2:"26";i:30;s:2:"27";i:31;s:2:"18";i:32;s:2:"57";i:33;s:2:"25";i:34;s:2:"17";i:35;s:2:"24";i:36;s:2:"33";i:37;s:2:"34";}s:10:"date_start";s:0:"";s:8:"date_end";s:0:"";s:6:"status";s:1:"1";s:9:"module_id";s:2:"34";}', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_option`
--

CREATE TABLE `oc_option` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_option`
--

INSERT INTO `oc_option` (`id`, `type`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'radio', 1, '0000-00-00', '0000-00-00', NULL),
(2, 'checkbox', 2, '0000-00-00', '0000-00-00', NULL),
(4, 'text', 3, '0000-00-00', '0000-00-00', NULL),
(5, 'select', 4, '0000-00-00', '0000-00-00', NULL),
(6, 'textarea', 5, '0000-00-00', '0000-00-00', NULL),
(7, 'file', 6, '0000-00-00', '0000-00-00', NULL),
(8, 'date', 7, '0000-00-00', '0000-00-00', NULL),
(9, 'time', 8, '0000-00-00', '0000-00-00', NULL),
(10, 'datetime', 9, '0000-00-00', '0000-00-00', NULL),
(11, 'select', 10, '0000-00-00', '0000-00-00', NULL),
(12, 'date', 11, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_option_description`
--

INSERT INTO `oc_option_description` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Radio', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Checkbox', '0000-00-00', '0000-00-00', NULL),
(4, 2, 'Text', '0000-00-00', '0000-00-00', NULL),
(6, 2, 'Textarea', '0000-00-00', '0000-00-00', NULL),
(8, 2, 'Date', '0000-00-00', '0000-00-00', NULL),
(7, 2, 'File', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Select', '0000-00-00', '0000-00-00', NULL),
(9, 2, 'Time', '0000-00-00', '0000-00-00', NULL),
(10, 2, 'Date &amp; Time', '0000-00-00', '0000-00-00', NULL),
(12, 2, 'Delivery Date', '0000-00-00', '0000-00-00', NULL),
(11, 2, 'Size', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_option_value`
--

INSERT INTO `oc_option_value` (`id`, `option_id`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(43, 1, '', 3, '0000-00-00', '0000-00-00', NULL),
(32, 1, '', 1, '0000-00-00', '0000-00-00', NULL),
(45, 2, '', 4, '0000-00-00', '0000-00-00', NULL),
(44, 2, '', 3, '0000-00-00', '0000-00-00', NULL),
(42, 5, '', 4, '0000-00-00', '0000-00-00', NULL),
(41, 5, '', 3, '0000-00-00', '0000-00-00', NULL),
(39, 5, '', 1, '0000-00-00', '0000-00-00', NULL),
(40, 5, '', 2, '0000-00-00', '0000-00-00', NULL),
(31, 1, '', 2, '0000-00-00', '0000-00-00', NULL),
(23, 2, '', 1, '0000-00-00', '0000-00-00', NULL),
(24, 2, '', 2, '0000-00-00', '0000-00-00', NULL),
(46, 11, '', 1, '0000-00-00', '0000-00-00', NULL),
(47, 11, '', 2, '0000-00-00', '0000-00-00', NULL),
(48, 11, '', 3, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`id`, `language_id`, `option_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(43, 2, 1, 'Large', '0000-00-00', '0000-00-00', NULL),
(32, 2, 1, 'Small', '0000-00-00', '0000-00-00', NULL),
(45, 2, 2, 'Checkbox 4', '0000-00-00', '0000-00-00', NULL),
(44, 2, 2, 'Checkbox 3', '0000-00-00', '0000-00-00', NULL),
(31, 2, 1, 'Medium', '0000-00-00', '0000-00-00', NULL),
(42, 2, 5, 'Yellow', '0000-00-00', '0000-00-00', NULL),
(41, 2, 5, 'Green', '0000-00-00', '0000-00-00', NULL),
(39, 2, 5, 'Red', '0000-00-00', '0000-00-00', NULL),
(40, 2, 5, 'Blue', '0000-00-00', '0000-00-00', NULL),
(23, 2, 2, 'Checkbox 1', '0000-00-00', '0000-00-00', NULL),
(24, 2, 2, 'Checkbox 2', '0000-00-00', '0000-00-00', NULL),
(48, 2, 11, 'Large', '0000-00-00', '0000-00-00', NULL),
(47, 2, 11, 'Medium', '0000-00-00', '0000-00-00', NULL),
(46, 2, 11, 'Small', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order`
--

CREATE TABLE `oc_order` (
  `id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(40) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_sub_district_id` int(4) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_sub_district_id` int(4) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_order`
--

INSERT INTO `oc_order` (`id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_sub_district_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_sub_district_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(23, 0, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 4, 1, 'zuhry', 'culli', 'cullicuplis13@gmail.com', '085397226021', 'da', 'a:0:{}', 'zuhry', 'culli', 'da', 'jl. tinumbu no. 48 f makassar', '', 'Awangpone', '904321', 'Sulawesi Selatan', 28, 'Bone', 87, 0, '', 'a:0:{}', 'Transfer Bank', 'bank_transfer', 'zuhry', 'culli', 'da', 'jl. tinumbu no. 48 f makassar', '', 'Awangpone', '904321', 'Sulawesi Selatan', 28, 'Bone', 87, 0, '', 'a:0:{}', 'Ongkos Kirim Tarif Tunggal', 'flat.flat', '', '45000.0000', 5, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36', 'en-US,en;q=0.8', '2017-04-22 04:34:07', '2017-04-22 04:34:11', '0000-00-00', '0000-00-00', NULL),
(22, 3, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 3, 1, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', 'b:0;', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Pengiriman dengan JNE OKE', 'jneoke.jneoke', '', '360000.0000', 7, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-07 12:04:35', '2017-02-07 12:04:46', '0000-00-00', '0000-00-00', NULL),
(21, 0, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 3, 1, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', 'b:0;', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Pengiriman dengan JNE YES', 'jneyes.jneyes', '', '15000.0000', 0, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-04 17:10:53', '2017-02-04 17:10:53', '0000-00-00', '0000-00-00', NULL),
(19, 0, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 3, 1, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', 'b:0;', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Pengiriman dengan TIKI Regular Service', 'tikireg.tikireg', '', '92500.0000', 7, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-02 04:11:23', '2017-02-02 04:11:28', '0000-00-00', '0000-00-00', NULL),
(20, 0, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 3, 1, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', 'b:0;', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Pengiriman dengan JNE OKE', 'jneoke.jneoke', '', '185000.0000', 7, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-02 17:42:48', '2017-02-02 17:42:58', '0000-00-00', '0000-00-00', NULL),
(18, 2, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 3, 1, 'Makmur', 'galih', 'zuhryculli@gmail.com', '084242432', '', 'b:0;', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'Makmur', 'galih', '', 'makassar', '', 'Jita', '90432', 'Papua', 24, 'Mimika', 284, 0, '', 'b:0;', 'Pengiriman dengan JNE OKE', 'jneoke.jneoke', '', '180000.0000', 7, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-02 04:10:38', '2017-02-02 04:10:49', '0000-00-00', '0000-00-00', NULL),
(17, 1, 'INV-2013-00', 0, 'Tokoandi', 'http://localhost/andi/', 2, 1, 'amin', 'udin', 'aminudin@gmail.com', '085397226021', 'da', 'b:0;', 'amin', 'udin', '', 'jl tinumbu', '', 'Bontoala', '90031', 'Sulawesi Selatan', 28, 'Makassar', 254, 0, '', 'b:0;', 'Transfer Bank', 'bank_transfer', 'amin', 'udin', '', 'jl tinumbu', '', 'Bontoala', '90031', 'Sulawesi Selatan', 28, 'Makassar', 254, 0, '', 'b:0;', 'Pengiriman dengan JNE Reguler', 'jnereg.jnereg', '', '26000.0000', 5, 0, '0.0000', 0, '', 2, 4, 'IDR', '1.00000000', '::1', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 'en-US,en;q=0.8', '2017-02-02 03:36:02', '2017-02-02 03:43:08', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_custom_field`
--

CREATE TABLE `oc_order_custom_field` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `custom_field_value_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `location` varchar(16) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_order_history`
--

INSERT INTO `oc_order_history` (`id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 0, '', '2017-01-30 19:05:34', '0000-00-00', '0000-00-00', NULL),
(2, 9, 15, 0, '', '2017-01-31 08:46:06', '0000-00-00', '0000-00-00', NULL),
(3, 11, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-01-31 14:52:52', '0000-00-00', '0000-00-00', NULL),
(4, 12, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-01-31 14:59:20', '0000-00-00', '0000-00-00', NULL),
(5, 13, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-01-31 15:10:42', '0000-00-00', '0000-00-00', NULL),
(6, 13, 5, 0, 'Konfirmasi Pembayaran', '2017-01-31 15:11:26', '0000-00-00', '0000-00-00', NULL),
(7, 14, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-01-31 16:12:55', '0000-00-00', '0000-00-00', NULL),
(8, 14, 5, 0, 'Konfirmasi Pembayaran', '2017-01-31 16:13:40', '0000-00-00', '0000-00-00', NULL),
(9, 15, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-01 03:08:20', '0000-00-00', '0000-00-00', NULL),
(10, 15, 5, 0, 'Konfirmasi Pembayaran', '2017-02-01 03:08:48', '0000-00-00', '0000-00-00', NULL),
(11, 16, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-01 03:35:56', '0000-00-00', '0000-00-00', NULL),
(12, 17, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-02 03:36:07', '0000-00-00', '0000-00-00', NULL),
(13, 17, 5, 0, 'Konfirmasi Pembayaran', '2017-02-02 03:36:41', '0000-00-00', '0000-00-00', NULL),
(14, 17, 5, 0, 'Baik kami sudah menerima transferan anda, trima kasih telah mempercayakan kami di tokoandi, ditunggu orderan selanjtnya yah', '2017-02-02 03:43:08', '0000-00-00', '0000-00-00', NULL),
(15, 18, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-02 04:10:49', '0000-00-00', '0000-00-00', NULL),
(16, 19, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-02 04:11:28', '0000-00-00', '0000-00-00', NULL),
(17, 20, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-02 17:42:58', '0000-00-00', '0000-00-00', NULL),
(18, 22, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-02-07 12:04:46', '0000-00-00', '0000-00-00', NULL),
(19, 23, 7, 1, 'Instruksi Transfer Bank\n\nBNI\r\n312312312\n\nPesanan anda tidak akan kami kirim sampai kami menerima pembayaran.', '2017-04-22 04:34:11', '0000-00-00', '0000-00-00', NULL),
(20, 23, 5, 0, 'Konfirmasi Pembayaran', '2017-04-22 04:34:33', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_order_product`
--

INSERT INTO `oc_order_product` (`id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 41, 'iMac', 'Product 14', 2, '100.0000', '200.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(2, 2, 41, 'iMac', 'Product 14', 2, '100.0000', '200.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(3, 3, 41, 'iMac', 'Product 14', 2, '100.0000', '200.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(4, 4, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(5, 4, 41, 'iMac', 'Product 14', 2, '100.0000', '200.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(6, 5, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(7, 6, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(8, 7, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(9, 8, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(10, 9, 50, 'Mi Phone', 'Mi Phone', 1, '100000.0000', '100000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(11, 10, 41, 'iMac', 'Product 14', 2, '100.0000', '200.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(12, 11, 41, 'iMac', 'Product 14', 3, '100.0000', '300.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(13, 12, 41, 'iMac', 'Product 14', 1, '100.0000', '100.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(14, 13, 41, 'iMac', 'Product 14', 1, '100.0000', '100.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(15, 14, 41, 'iMac', 'Product 14', 1, '100.0000', '100.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(16, 15, 41, 'iMac', 'Product 14', 1, '100.0000', '100.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(17, 16, 41, 'iMac', 'Product 14', 1, '100.0000', '100.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(18, 17, 51, 'Otak-otak', 'Makanan', 1, '20000.0000', '20000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(19, 18, 52, 'Kacang telur sembunyi', 'Makanan', 1, '15000.0000', '15000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(20, 19, 51, 'Otak-otak', 'Makanan', 1, '20000.0000', '20000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(21, 20, 51, 'Otak-otak', 'Makanan', 1, '20000.0000', '20000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(22, 21, 52, 'Kacang telur sembunyi', 'Makanan', 1, '15000.0000', '15000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(23, 22, 52, 'Kacang telur sembunyi', 'Makanan', 2, '15000.0000', '30000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL),
(24, 23, 52, 'Kacang telur sembunyi', 'Makanan', 2, '15000.0000', '30000.0000', '0.0000', 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_order_status`
--

INSERT INTO `oc_order_status` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 'Processing', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Shipped', '0000-00-00', '0000-00-00', NULL),
(7, 2, 'Canceled', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Complete', '0000-00-00', '0000-00-00', NULL),
(8, 2, 'Denied', '0000-00-00', '0000-00-00', NULL),
(9, 2, 'Canceled Reversal', '0000-00-00', '0000-00-00', NULL),
(10, 2, 'Failed', '0000-00-00', '0000-00-00', NULL),
(11, 2, 'Refunded', '0000-00-00', '0000-00-00', NULL),
(12, 2, 'Reversed', '0000-00-00', '0000-00-00', NULL),
(13, 2, 'Chargeback', '0000-00-00', '0000-00-00', NULL),
(1, 2, 'Pending', '0000-00-00', '0000-00-00', NULL),
(16, 2, 'Voided', '0000-00-00', '0000-00-00', NULL),
(15, 2, 'Processed', '0000-00-00', '0000-00-00', NULL),
(14, 2, 'Expired', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_order_total`
--

INSERT INTO `oc_order_total` (`id`, `order_id`, `code`, `title`, `value`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '200.0000', 1, '0000-00-00', '0000-00-00', NULL),
(2, 1, 'shipping', 'Pengiriman dengan JNE Trucking', '0.0000', 3, '0000-00-00', '0000-00-00', NULL),
(3, 1, 'total', 'Jumlah', '200.0000', 9, '0000-00-00', '0000-00-00', NULL),
(4, 2, 'sub_total', 'Sub-Total', '200.0000', 1, '0000-00-00', '0000-00-00', NULL),
(5, 2, 'shipping', 'Pengiriman dengan JNE Reguler', '330000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(6, 2, 'total', 'Jumlah', '330200.0000', 9, '0000-00-00', '0000-00-00', NULL),
(7, 3, 'sub_total', 'Sub-Total', '200.0000', 1, '0000-00-00', '0000-00-00', NULL),
(8, 3, 'shipping', 'Pengiriman dengan JNE OKE', '290000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(9, 3, 'total', 'Jumlah', '290200.0000', 9, '0000-00-00', '0000-00-00', NULL),
(10, 4, 'sub_total', 'Sub-Total', '100200.0000', 1, '0000-00-00', '0000-00-00', NULL),
(11, 4, 'shipping', 'Pengiriman dengan JNE OKE', '580000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(12, 4, 'total', 'Jumlah', '680200.0000', 9, '0000-00-00', '0000-00-00', NULL),
(13, 5, 'sub_total', 'Sub-Total', '100000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(14, 5, 'shipping', 'Pengiriman dengan JNE OKE', '29000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(15, 5, 'total', 'Jumlah', '129000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(16, 6, 'sub_total', 'Asuransi JNE', '7000.0000', 0, '0000-00-00', '0000-00-00', NULL),
(17, 6, 'sub_total', 'Sub-Total', '100000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(18, 6, 'shipping', 'Pengiriman dengan JNE OKE', '29000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(19, 6, 'jne_pk', 'JNE Packing Kayu', '58000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(20, 6, 'total', 'Jumlah', '194000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(21, 7, 'sub_total', 'Asuransi JNE', '7000.0000', 0, '0000-00-00', '0000-00-00', NULL),
(22, 7, 'sub_total', 'Sub-Total', '100000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(23, 7, 'total', 'Jumlah', '107000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(24, 8, 'sub_total', 'Asuransi JNE', '7000.0000', 0, '0000-00-00', '0000-00-00', NULL),
(25, 8, 'sub_total', 'Sub-Total', '100000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(26, 8, 'total', 'Jumlah', '107000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(27, 9, 'sub_total', 'Asuransi JNE', '7000.0000', 0, '0000-00-00', '0000-00-00', NULL),
(28, 9, 'sub_total', 'Sub-Total', '100000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(29, 9, 'total', 'Jumlah', '107000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(30, 10, 'sub_total', 'Asuransi JNE', '5004.0000', 0, '0000-00-00', '0000-00-00', NULL),
(31, 10, 'sub_total', 'Sub-Total', '200.0000', 1, '0000-00-00', '0000-00-00', NULL),
(32, 10, 'shipping', 'Pengiriman dengan JNE Reguler', '330000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(33, 10, 'jne_pk', 'JNE Packing Kayu', '660000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(34, 10, 'total', 'Jumlah', '995204.0000', 9, '0000-00-00', '0000-00-00', NULL),
(35, 11, 'sub_total', 'Asuransi JNE', '5006.0000', 0, '0000-00-00', '0000-00-00', NULL),
(36, 11, 'sub_total', 'Sub-Total', '300.0000', 1, '0000-00-00', '0000-00-00', NULL),
(37, 11, 'shipping', 'Pengiriman dengan JNE OKE', '435000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(38, 11, 'jne_pk', 'JNE Packing Kayu', '870000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(39, 11, 'total', 'Jumlah', '1310306.0000', 9, '0000-00-00', '0000-00-00', NULL),
(40, 12, 'sub_total', 'Asuransi JNE', '5002.0000', 0, '0000-00-00', '0000-00-00', NULL),
(41, 12, 'sub_total', 'Sub-Total', '100.0000', 1, '0000-00-00', '0000-00-00', NULL),
(42, 12, 'shipping', 'Pengiriman dengan JNE OKE', '145000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(43, 12, 'jne_pk', 'JNE Packing Kayu', '290000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(44, 12, 'total', 'Jumlah', '440102.0000', 9, '0000-00-00', '0000-00-00', NULL),
(45, 13, 'sub_total', 'Asuransi JNE', '5002.0000', 0, '0000-00-00', '0000-00-00', NULL),
(46, 13, 'sub_total', 'Sub-Total', '100.0000', 1, '0000-00-00', '0000-00-00', NULL),
(47, 13, 'shipping', 'Pengiriman dengan JNE OKE', '145000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(48, 13, 'jne_pk', 'JNE Packing Kayu', '290000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(49, 13, 'total', 'Jumlah', '440102.0000', 9, '0000-00-00', '0000-00-00', NULL),
(50, 14, 'sub_total', 'Asuransi JNE', '5002.0000', 0, '0000-00-00', '0000-00-00', NULL),
(51, 14, 'sub_total', 'Sub-Total', '100.0000', 1, '0000-00-00', '0000-00-00', NULL),
(52, 14, 'shipping', 'Pengiriman dengan JNE OKE', '145000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(53, 14, 'jne_pk', 'JNE Packing Kayu', '290000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(54, 14, 'total', 'Jumlah', '440102.0000', 9, '0000-00-00', '0000-00-00', NULL),
(55, 15, 'sub_total', 'Asuransi JNE', '5002.0000', 0, '0000-00-00', '0000-00-00', NULL),
(56, 15, 'sub_total', 'Sub-Total', '100.0000', 1, '0000-00-00', '0000-00-00', NULL),
(57, 15, 'shipping', 'Pengiriman dengan JNE OKE', '145000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(58, 15, 'jne_pk', 'JNE Packing Kayu', '290000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(59, 15, 'total', 'Jumlah', '440102.0000', 9, '0000-00-00', '0000-00-00', NULL),
(60, 16, 'sub_total', 'Asuransi JNE', '5002.0000', 0, '0000-00-00', '0000-00-00', NULL),
(61, 16, 'sub_total', 'Sub-Total', '100.0000', 1, '0000-00-00', '0000-00-00', NULL),
(62, 16, 'shipping', 'Pengiriman dengan JNE OKE', '145000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(63, 16, 'jne_pk', 'JNE Packing Kayu', '290000.0000', 8, '0000-00-00', '0000-00-00', NULL),
(64, 16, 'total', 'Jumlah', '440102.0000', 9, '0000-00-00', '0000-00-00', NULL),
(65, 17, 'sub_total', 'Sub-Total', '20000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(66, 17, 'shipping', 'Pengiriman dengan JNE Reguler', '6000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(67, 17, 'total', 'Jumlah', '26000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(68, 18, 'sub_total', 'Sub-Total', '15000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(69, 18, 'shipping', 'Pengiriman dengan JNE OKE', '165000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(70, 18, 'total', 'Jumlah', '180000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(71, 19, 'sub_total', 'Sub-Total', '20000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(72, 19, 'shipping', 'Pengiriman dengan TIKI Regular Service', '72500.0000', 3, '0000-00-00', '0000-00-00', NULL),
(73, 19, 'total', 'Jumlah', '92500.0000', 9, '0000-00-00', '0000-00-00', NULL),
(74, 20, 'sub_total', 'Sub-Total', '20000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(75, 20, 'shipping', 'Pengiriman dengan JNE OKE', '165000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(76, 20, 'total', 'Jumlah', '185000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(77, 21, 'sub_total', 'Sub-Total', '15000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(78, 21, 'shipping', 'Pengiriman dengan JNE YES', '0.0000', 3, '0000-00-00', '0000-00-00', NULL),
(79, 21, 'total', 'Jumlah', '15000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(80, 22, 'sub_total', 'Sub-Total', '30000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(81, 22, 'shipping', 'Pengiriman dengan JNE OKE', '330000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(82, 22, 'total', 'Jumlah', '360000.0000', 9, '0000-00-00', '0000-00-00', NULL),
(83, 23, 'sub_total', 'Sub-Total', '30000.0000', 1, '0000-00-00', '0000-00-00', NULL),
(84, 23, 'shipping', 'Ongkos Kirim Tarif Tunggal', '15000.0000', 3, '0000-00-00', '0000-00-00', NULL),
(85, 23, 'total', 'Jumlah', '45000.0000', 9, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product`
--

CREATE TABLE `oc_product` (
  `id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product`
--

INSERT INTO `oc_product` (`id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 'Makanan', '1', '1', '1', '1', '1', '1', 'Makassar', 19, 7, 'Screenshot from 2017-08-05 21-56-09.png', 5, 1, '20000.0000', 0, 9, '2017-09-04', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 9, '2017-09-04 00:00:00', '2017-09-04 00:00:00', '0000-00-00', '2017-09-03', NULL),
(52, 'Makanan', '1', '1', '1', '1', '1', '1', 'Makassar', 30, 7, 'Screenshot from 2017-07-25 14-04-59.png', 5, 1, '15000.0000', 0, 9, '2017-09-04', '1.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 8, '2017-09-04 00:00:00', '2017-09-04 00:00:00', '0000-00-00', '2017-09-03', NULL),
(53, 'ksjadhkj', 'kjhkj', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'kj', 1, 7, 'Screenshot from 2017-08-22 16-06-53.png', 5, 1, '6.0000', 1, 9, '2017-09-04', '5.00000000', 1, '5.00000000', '4.00000000', '5.00000000', 1, 1, 6, 5, 1, 4, '2017-09-04 00:00:00', '2017-09-04 00:00:00', '2017-08-28', '2017-09-03', NULL),
(54, 'asdasdasda', 'asdasd', 'asdasd', 'asdas', 'asdasd', 'asdasdasd', 'asdasdas', 'asdasdasd', 12, 7, '1250719_016815c2-61af-4a0e-9327-12d6cb337fa4.jpg', 5, 1, '122222222.0000', 14, 9, '2017-09-04', '144.00000000', 1, '111.00000000', '1122.00000000', '123123.00000000', 1, 1, 123, 123, 1, 123, '2017-09-04 00:00:00', '2017-09-04 00:00:00', '2017-09-03', '2017-09-03', NULL),
(55, 'asdasdasda', 'kjhkj', 'kjh', 'kjh', 'kjh', 'kjh', 'kjh', 'Makassar', 38, 7, 'OPEN-ORDER.jpg', 5, 1, '2712312312.0000', 4, 9, '2017-09-04', '1.00000000', 1, '5.00000000', '21.00000000', '35.00000000', 1, 1, 15, 16, 1, 17, '2017-09-04 00:00:00', '2017-09-04 00:00:00', '2017-09-03', '2017-09-03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`id`, `product_id`, `attribute_id`, `language_id`, `text`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 52, 3, 2, 'aksjhdaksjdhkajsd', '2017-08-28', '2017-08-28', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_description`
--

INSERT INTO `oc_product_description` (`id`, `product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 51, 2, 'Otak-otak', 'Otak-otak dibuat dari ikan yang diambil dagingnya, kemudian dihaluskan dan dibumbui. Selanjutnya, daging ikan tersebut dimasukkan lagi ke dalam kulit ikan yang kemudian direbus atau dipanggang dalam balutan daun pisang. Makanan ini dapat dimakan tersendiri atau dengan saus asam pedas. Kadang-kadang dijadikan lauk untuk makan nasi.', 'makassar, makanan, tradisional', 'Otak-otak', 'cemilan', 'cemilan', '0000-00-00', '2017-08-29', NULL),
(52, 52, 2, 'Kacang telur sembunyi', 'Kacang telur sembunyi\nBahan\n1 kg tepung terigu\n500 gram gula pasir\n500 gram kacang tanah\n4 butir telur ayam\nMentega secukupnya', 'cemilan, makanan ringan, kacang, makassar, tradisional, khas makassar', 'Kacang telur sembunyi', 'kacang', 'kacang', '0000-00-00', '2017-08-29', NULL),
(53, 53, 2, 'qwerty', 'skjdbvjdshb', 'uhbudvb', 'ububhu', 'ubu', 'ubu', '2017-09-03', '2017-09-03', '2017-09-03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_discount`
--

INSERT INTO `oc_product_discount` (`id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`, `created_at`, `updated_at`, `deleted_at`) VALUES
(441, 52, 2, 2, 2, '2.0000', '2017-08-29', '2017-08-29', '2017-08-28', '2017-08-29', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_image`
--

INSERT INTO `oc_product_image` (`id`, `product_id`, `image`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2367, 52, 'bug.png', 12, '2017-09-03', '2017-09-03', NULL),
(2366, 51, 'Screenshot from 2017-08-22 15-52-59.png', 1, '2017-09-03', '2017-09-03', NULL),
(2365, 51, 'timeline.png', 17, '2017-09-03', '2017-09-03', NULL),
(2364, 51, 'buy.png', 12, '2017-09-03', '2017-09-03', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_shipping_filtered`
--

CREATE TABLE `oc_product_shipping_filtered` (
  `filtered_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `shipping_code` varchar(30) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`id`, `product_id`, `category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(52, 0, 17, '0000-00-00', '0000-00-00', NULL),
(51, 0, 20, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 0, 1, '0000-00-00', '0000-00-00', NULL),
(52, 0, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(51, 0, '0000-00-00', '0000-00-00', NULL),
(52, 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_return`
--

CREATE TABLE `oc_return` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_return_action`
--

INSERT INTO `oc_return_action` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Refunded', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Credit Issued', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Replacement Sent', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Dead On Arrival', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Received Wrong Item', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Order Error', '0000-00-00', '0000-00-00', NULL),
(4, 2, 'Faulty, please supply details', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Other, please supply details', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_return_status`
--

INSERT INTO `oc_return_status` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Pending', '0000-00-00', '0000-00-00', NULL),
(3, 2, 'Complete', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Awaiting Products', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_review`
--

CREATE TABLE `oc_review` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_setting`
--

CREATE TABLE `oc_setting` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_setting`
--

INSERT INTO `oc_setting` (`id`, `store_id`, `code`, `key`, `value`, `serialized`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 0, 'shipping', 'shipping_sort_order', '3', 0, '0000-00-00', '0000-00-00', NULL),
(2, 0, 'sub_total', 'sub_total_sort_order', '1', 0, '0000-00-00', '0000-00-00', NULL),
(3, 0, 'sub_total', 'sub_total_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(682, 0, 'tax', 'tax_sort_order', '5', 0, '0000-00-00', '0000-00-00', NULL),
(5, 0, 'total', 'total_sort_order', '9', 0, '0000-00-00', '0000-00-00', NULL),
(6, 0, 'total', 'total_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(681, 0, 'tax', 'tax_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(8, 0, 'free_checkout', 'free_checkout_sort_order', '1', 0, '0000-00-00', '0000-00-00', NULL),
(698, 0, 'cod', 'cod_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(697, 0, 'cod', 'cod_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(14, 0, 'shipping', 'shipping_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(15, 0, 'shipping', 'shipping_estimator', '1', 0, '0000-00-00', '0000-00-00', NULL),
(27, 0, 'coupon', 'coupon_sort_order', '4', 0, '0000-00-00', '0000-00-00', NULL),
(28, 0, 'coupon', 'coupon_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(823, 0, 'bank_transfer', 'bank_transfer_order_status_id', '7', 0, '0000-00-00', '0000-00-00', NULL),
(822, 0, 'bank_transfer', 'bank_transfer_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(704, 0, 'jneoke', 'jneoke_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(696, 0, 'cod', 'cod_order_status_id', '15', 0, '0000-00-00', '0000-00-00', NULL),
(42, 0, 'credit', 'credit_sort_order', '7', 0, '0000-00-00', '0000-00-00', NULL),
(43, 0, 'credit', 'credit_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(53, 0, 'reward', 'reward_sort_order', '2', 0, '0000-00-00', '0000-00-00', NULL),
(54, 0, 'reward', 'reward_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(2627, 0, 'category', 'category_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(158, 0, 'account', 'account_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(159, 0, 'affiliate', 'affiliate_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(821, 0, 'bank_transfer', 'bank_transfer_bank2', 'BNI\r\n312312312', 0, '0000-00-00', '0000-00-00', NULL),
(2626, 0, 'confirm', 'confirm_bank_accounts', 'a:1:{i:1;a:3:{s:4:"name";s:5:"zuhry";s:10:"acc_number";s:12:"733721399312";s:9:"bank_name";s:3:"BNI";}}', 1, '0000-00-00', '0000-00-00', NULL),
(4367, 0, 'config', 'config_google_captcha_status', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4366, 0, 'config', 'config_google_captcha_secret', '', 0, '0000-00-00', '0000-00-00', NULL),
(4365, 0, 'config', 'config_google_captcha_public', '', 0, '0000-00-00', '0000-00-00', NULL),
(4364, 0, 'config', 'config_google_analytics_status', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4363, 0, 'config', 'config_google_analytics', '', 0, '0000-00-00', '0000-00-00', NULL),
(4362, 0, 'config', 'config_error_filename', 'error.log', 0, '0000-00-00', '0000-00-00', NULL),
(4361, 0, 'config', 'config_error_log', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4360, 0, 'config', 'config_error_display', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4359, 0, 'config', 'config_compression', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4348, 0, 'config', 'config_mail_alert', '', 0, '0000-00-00', '0000-00-00', NULL),
(4349, 0, 'config', 'config_secure', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4350, 0, 'config', 'config_shared', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4351, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0, '0000-00-00', '0000-00-00', NULL),
(4356, 0, 'config', 'config_maintenance', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4357, 0, 'config', 'config_password', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4358, 0, 'config', 'config_encryption', 'e4bd0a7928deb04e3a39f4abfdb9f724', 0, '0000-00-00', '0000-00-00', NULL),
(4346, 0, 'config', 'config_mail_smtp_port', '25', 0, '0000-00-00', '0000-00-00', NULL),
(4347, 0, 'config', 'config_mail_smtp_timeout', '5', 0, '0000-00-00', '0000-00-00', NULL),
(1058, 0, 'tikieco', 'tikieco_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(1057, 0, 'tikieco', 'tikieco_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(4353, 0, 'config', 'config_file_max_size', '300000', 0, '0000-00-00', '0000-00-00', NULL),
(4354, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0, '0000-00-00', '0000-00-00', NULL),
(4355, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0, '0000-00-00', '0000-00-00', NULL),
(4352, 0, 'config', 'config_seo_url', '0', 0, '0000-00-00', '0000-00-00', NULL),
(94, 0, 'voucher', 'voucher_sort_order', '8', 0, '0000-00-00', '0000-00-00', NULL),
(95, 0, 'voucher', 'voucher_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(103, 0, 'free_checkout', 'free_checkout_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(104, 0, 'free_checkout', 'free_checkout_order_status_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4345, 0, 'config', 'config_mail_smtp_password', '', 0, '0000-00-00', '0000-00-00', NULL),
(4344, 0, 'config', 'config_mail_smtp_username', '', 0, '0000-00-00', '0000-00-00', NULL),
(4343, 0, 'config', 'config_mail_smtp_hostname', '', 0, '0000-00-00', '0000-00-00', NULL),
(4342, 0, 'config', 'config_mail_parameter', '', 0, '0000-00-00', '0000-00-00', NULL),
(4341, 0, 'config', 'config_mail_protocol', 'mail', 0, '0000-00-00', '0000-00-00', NULL),
(4340, 0, 'config', 'config_ftp_status', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4339, 0, 'config', 'config_ftp_root', '', 0, '0000-00-00', '0000-00-00', NULL),
(4338, 0, 'config', 'config_ftp_password', '', 0, '0000-00-00', '0000-00-00', NULL),
(4337, 0, 'config', 'config_ftp_username', '', 0, '0000-00-00', '0000-00-00', NULL),
(4336, 0, 'config', 'config_ftp_port', '21', 0, '0000-00-00', '0000-00-00', NULL),
(4335, 0, 'config', 'config_ftp_hostname', 'localhost', 0, '0000-00-00', '0000-00-00', NULL),
(4334, 0, 'config', 'config_image_location_height', '50', 0, '0000-00-00', '0000-00-00', NULL),
(4333, 0, 'config', 'config_image_location_width', '268', 0, '0000-00-00', '0000-00-00', NULL),
(4332, 0, 'config', 'config_image_cart_height', '47', 0, '0000-00-00', '0000-00-00', NULL),
(4331, 0, 'config', 'config_image_cart_width', '47', 0, '0000-00-00', '0000-00-00', NULL),
(4330, 0, 'config', 'config_image_wishlist_height', '47', 0, '0000-00-00', '0000-00-00', NULL),
(4329, 0, 'config', 'config_image_wishlist_width', '47', 0, '0000-00-00', '0000-00-00', NULL),
(4328, 0, 'config', 'config_image_compare_height', '90', 0, '0000-00-00', '0000-00-00', NULL),
(4327, 0, 'config', 'config_image_compare_width', '90', 0, '0000-00-00', '0000-00-00', NULL),
(4326, 0, 'config', 'config_image_related_height', '300', 0, '0000-00-00', '0000-00-00', NULL),
(4325, 0, 'config', 'config_image_related_width', '300', 0, '0000-00-00', '0000-00-00', NULL),
(4324, 0, 'config', 'config_image_additional_height', '140', 0, '0000-00-00', '0000-00-00', NULL),
(4323, 0, 'config', 'config_image_additional_width', '140', 0, '0000-00-00', '0000-00-00', NULL),
(4322, 0, 'config', 'config_image_product_height', '228', 0, '0000-00-00', '0000-00-00', NULL),
(4321, 0, 'config', 'config_image_product_width', '228', 0, '0000-00-00', '0000-00-00', NULL),
(4320, 0, 'config', 'config_image_popup_height', '700', 0, '0000-00-00', '0000-00-00', NULL),
(4319, 0, 'config', 'config_image_popup_width', '700', 0, '0000-00-00', '0000-00-00', NULL),
(4318, 0, 'config', 'config_image_thumb_height', '500', 0, '0000-00-00', '0000-00-00', NULL),
(4317, 0, 'config', 'config_image_thumb_width', '500', 0, '0000-00-00', '0000-00-00', NULL),
(4316, 0, 'config', 'config_image_category_height', '400', 0, '0000-00-00', '0000-00-00', NULL),
(4315, 0, 'config', 'config_image_category_width', '900', 0, '0000-00-00', '0000-00-00', NULL),
(4314, 0, 'config', 'config_icon', 'catalog/cart.png', 0, '0000-00-00', '0000-00-00', NULL),
(4313, 0, 'config', 'config_logo', 'catalog/ds.png', 0, '0000-00-00', '0000-00-00', NULL),
(4312, 0, 'config', 'config_return_status_id', '2', 0, '0000-00-00', '0000-00-00', NULL),
(4311, 0, 'config', 'config_return_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4310, 0, 'config', 'config_affiliate_mail', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4309, 0, 'config', 'config_affiliate_id', '4', 0, '0000-00-00', '0000-00-00', NULL),
(4308, 0, 'config', 'config_affiliate_commission', '5', 0, '0000-00-00', '0000-00-00', NULL),
(4307, 0, 'config', 'config_affiliate_auto', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4306, 0, 'config', 'config_affiliate_approval', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4305, 0, 'config', 'config_stock_checkout', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4304, 0, 'config', 'config_stock_warning', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4303, 0, 'config', 'config_stock_display', '0', 0, '0000-00-00', '0000-00-00', NULL),
(699, 0, 'cod', 'cod_sort_order', '5', 0, '0000-00-00', '0000-00-00', NULL),
(694, 0, 'jne', 'jne_city_id', '254', 0, '0000-00-00', '0000-00-00', NULL),
(691, 0, 'jne', 'jne_handling_fee_mode', 'perweight', 0, '0000-00-00', '0000-00-00', NULL),
(692, 0, 'jne', 'jne_wooden_package', '0', 0, '0000-00-00', '0000-00-00', NULL),
(695, 0, 'cod', 'cod_total', '15000', 0, '0000-00-00', '0000-00-00', NULL),
(4301, 0, 'config', 'config_complete_status', 'a:1:{i:0;s:1:"5";}', 1, '0000-00-00', '0000-00-00', NULL),
(4302, 0, 'config', 'config_order_mail', '0', 0, '0000-00-00', '0000-00-00', NULL),
(703, 0, 'jneoke', 'jneoke_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(702, 0, 'jneoke', 'jneoke_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(701, 0, 'jneoke', 'jneoke_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(700, 0, 'jneoke', 'jneoke_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(300, 0, 'jnereg', 'jnereg_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(301, 0, 'jnereg', 'jnereg_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(302, 0, 'jnereg', 'jnereg_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(303, 0, 'jnereg', 'jnereg_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(304, 0, 'jnereg', 'jnereg_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(305, 0, 'jneyes', 'jneyes_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(306, 0, 'jneyes', 'jneyes_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(307, 0, 'jneyes', 'jneyes_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(308, 0, 'jneyes', 'jneyes_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(309, 0, 'jneyes', 'jneyes_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(678, 0, 'kontak_shipping', 'kontak_shipping_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(677, 0, 'kontak_shipping', 'kontak_shipping_status', '0', 0, '0000-00-00', '0000-00-00', NULL),
(676, 0, 'kontak_shipping', 'kontak_shipping_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(4300, 0, 'config', 'config_processing_status', 'a:1:{i:0;s:1:"2";}', 1, '0000-00-00', '0000-00-00', NULL),
(4299, 0, 'config', 'config_order_status_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4298, 0, 'config', 'config_checkout_id', '5', 0, '0000-00-00', '0000-00-00', NULL),
(4297, 0, 'config', 'config_checkout_guest', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4296, 0, 'config', 'config_cart_weight', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4295, 0, 'config', 'config_api_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4294, 0, 'config', 'config_invoice_prefix', 'INV-2013-00', 0, '0000-00-00', '0000-00-00', NULL),
(4293, 0, 'config', 'config_account_mail', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4292, 0, 'config', 'config_account_id', '3', 0, '0000-00-00', '0000-00-00', NULL),
(4291, 0, 'config', 'config_login_attempts', '5', 0, '0000-00-00', '0000-00-00', NULL),
(4290, 0, 'config', 'config_customer_price', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4289, 0, 'config', 'config_customer_group_display', 'a:1:{i:0;s:1:"1";}', 1, '0000-00-00', '0000-00-00', NULL),
(4288, 0, 'config', 'config_customer_group_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4287, 0, 'config', 'config_customer_online', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4286, 0, 'config', 'config_tax_customer', 'shipping', 0, '0000-00-00', '0000-00-00', NULL),
(4285, 0, 'config', 'config_tax_default', 'shipping', 0, '0000-00-00', '0000-00-00', NULL),
(4284, 0, 'config', 'config_tax', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4283, 0, 'config', 'config_voucher_max', '1000', 0, '0000-00-00', '0000-00-00', NULL),
(4282, 0, 'config', 'config_voucher_min', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4281, 0, 'config', 'config_review_mail', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4280, 0, 'config', 'config_review_guest', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4279, 0, 'config', 'config_review_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4278, 0, 'config', 'config_limit_admin', '20', 0, '0000-00-00', '0000-00-00', NULL),
(4277, 0, 'config', 'config_product_description_length', '100', 0, '0000-00-00', '0000-00-00', NULL),
(4275, 0, 'config', 'config_product_count', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4276, 0, 'config', 'config_product_limit', '15', 0, '0000-00-00', '0000-00-00', NULL),
(4274, 0, 'config', 'config_weight_class_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4273, 0, 'config', 'config_length_class_id', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4272, 0, 'config', 'config_currency_auto', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4271, 0, 'config', 'config_currency', 'IDR', 0, '0000-00-00', '0000-00-00', NULL),
(4270, 0, 'config', 'config_admin_language', 'id', 0, '0000-00-00', '0000-00-00', NULL),
(4269, 0, 'config', 'config_language', 'id', 0, '0000-00-00', '0000-00-00', NULL),
(4268, 0, 'config', 'config_zone_id', '254', 0, '0000-00-00', '0000-00-00', NULL),
(4267, 0, 'config', 'config_country_id', '28', 0, '0000-00-00', '0000-00-00', NULL),
(4266, 0, 'config', 'config_layout_id', '2', 0, '0000-00-00', '0000-00-00', NULL),
(4265, 0, 'config', 'config_template', 'default', 0, '0000-00-00', '0000-00-00', NULL),
(4264, 0, 'config', 'config_meta_keyword', '', 0, '0000-00-00', '0000-00-00', NULL),
(693, 0, 'jne', 'jne_province_id', '28', 0, '0000-00-00', '0000-00-00', NULL),
(690, 0, 'jne', 'jne_handling_fee', '', 0, '0000-00-00', '0000-00-00', NULL),
(4263, 0, 'config', 'config_meta_description', 'My Store', 0, '0000-00-00', '0000-00-00', NULL),
(4262, 0, 'config', 'config_meta_title', 'Your Store', 0, '0000-00-00', '0000-00-00', NULL),
(4261, 0, 'config', 'config_comment', '', 0, '0000-00-00', '0000-00-00', NULL),
(824, 0, 'bank_transfer', 'bank_transfer_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(825, 0, 'bank_transfer', 'bank_transfer_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(826, 0, 'bank_transfer', 'bank_transfer_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1059, 0, 'tikieco', 'tikieco_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1060, 0, 'tikieco', 'tikieco_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1061, 0, 'tikieco', 'tikieco_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1062, 0, 'tikihds', 'tikihds_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(1063, 0, 'tikihds', 'tikihds_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(1064, 0, 'tikihds', 'tikihds_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1065, 0, 'tikihds', 'tikihds_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1066, 0, 'tikihds', 'tikihds_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1067, 0, 'tikions', 'tikions_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(1068, 0, 'tikions', 'tikions_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(1069, 0, 'tikions', 'tikions_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1070, 0, 'tikions', 'tikions_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1071, 0, 'tikions', 'tikions_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1072, 0, 'tikireg', 'tikireg_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(1073, 0, 'tikireg', 'tikireg_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(1074, 0, 'tikireg', 'tikireg_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1075, 0, 'tikireg', 'tikireg_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1076, 0, 'tikireg', 'tikireg_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1077, 0, 'tikisds', 'tikisds_total', '', 0, '0000-00-00', '0000-00-00', NULL),
(1078, 0, 'tikisds', 'tikisds_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(1079, 0, 'tikisds', 'tikisds_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1080, 0, 'tikisds', 'tikisds_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(1081, 0, 'tikisds', 'tikisds_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL),
(1088, 0, 'tiki', 'tiki_province_id', '28', 0, '0000-00-00', '0000-00-00', NULL),
(1087, 0, 'tiki', 'tiki_handling_fee_mode', 'perweight', 0, '0000-00-00', '0000-00-00', NULL),
(1086, 0, 'tiki', 'tiki_handling_fee', '', 0, '0000-00-00', '0000-00-00', NULL),
(1089, 0, 'tiki', 'tiki_city_id', '254', 0, '0000-00-00', '0000-00-00', NULL),
(2625, 0, 'confirm', 'confirm_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(2624, 0, 'confirm', 'confirm_return_stock_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(2622, 0, 'confirm', 'confirm_order_status', '5', 0, '0000-00-00', '0000-00-00', NULL),
(2623, 0, 'confirm', 'confirm_substract_stock', '0', 0, '0000-00-00', '0000-00-00', NULL),
(2620, 0, 'confirm', 'confirm_login_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(2621, 0, 'confirm', 'confirm_transfer_receipt', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4260, 0, 'config', 'config_open', '', 0, '0000-00-00', '0000-00-00', NULL),
(4259, 0, 'config', 'config_image', 'catalog/jk.png', 0, '0000-00-00', '0000-00-00', NULL),
(4258, 0, 'config', 'config_fax', '', 0, '0000-00-00', '0000-00-00', NULL),
(4257, 0, 'config', 'config_telephone', '085397226021', 0, '0000-00-00', '0000-00-00', NULL),
(4256, 0, 'config', 'config_email', 'zuhryculli@gmail.com', 0, '0000-00-00', '0000-00-00', NULL),
(4255, 0, 'config', 'config_geocode', '', 0, '0000-00-00', '0000-00-00', NULL),
(4254, 0, 'config', 'config_address', 'Perumahan Anging Mamiri', 0, '0000-00-00', '0000-00-00', NULL),
(4253, 0, 'config', 'config_owner', 'Zuhry', 0, '0000-00-00', '0000-00-00', NULL),
(4252, 0, 'config', 'config_name', 'Tokoandi', 0, '0000-00-00', '0000-00-00', NULL),
(4372, 0, 'chatnox', 'chatnox_code', '&lt;!-- ChatNox -  Paste this ChatNox code before closing body tag --&gt;\r\n&lt;script type=\'text/javascript\'&gt;\r\nvar _chatnox = _chatnox || [];_chatnox.setAccount = \'5793436716761088\';\r\nvar cnox = document.createElement(\'script\');cnox.type = \'text/javascript\';cnox.async = true;\r\ncnox.src = (\'https:\' == document.location.protocol ? \'https://app.chatnox.com\' : \'http://app.chatnox.com\') + \'/site/chat.js\';\r\nvar s = document.getElementsByTagName(\'script\')[0];s.parentNode.insertBefore(cnox, s);\r\n&lt;/script&gt;&lt;!-- ChatNox code ends here --&gt;', 0, '0000-00-00', '0000-00-00', NULL),
(4373, 0, 'chatnox', 'chatnox_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4382, 0, 'flat', 'flat_status', '1', 0, '0000-00-00', '0000-00-00', NULL),
(4381, 0, 'flat', 'flat_geo_zone_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4380, 0, 'flat', 'flat_tax_class_id', '0', 0, '0000-00-00', '0000-00-00', NULL),
(4379, 0, 'flat', 'flat_cost', '15000', 0, '0000-00-00', '0000-00-00', NULL),
(4383, 0, 'flat', 'flat_sort_order', '', 0, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, 2, 'In Stock', '0000-00-00', '0000-00-00', '0000-00-00'),
(8, 2, 'Pre-Order', '0000-00-00', '0000-00-00', '0000-00-00'),
(5, 2, 'Out Of Stock', '0000-00-00', '0000-00-00', '0000-00-00'),
(6, 2, '2-3 Days', '0000-00-00', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_store`
--

CREATE TABLE `oc_store` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`id`, `title`, `description`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50', '0000-00-00', '0000-00-00', NULL),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29', '0000-00-00', '0000-00-00', NULL),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(86, 1, '0000-00-00', '0000-00-00', NULL),
(87, 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`, `created_at`, `updated_at`, `deleted_at`) VALUES
(121, 10, 86, 'payment', 1, '0000-00-00', '0000-00-00', NULL),
(120, 10, 87, 'store', 0, '0000-00-00', '0000-00-00', NULL),
(128, 9, 86, 'shipping', 1, '0000-00-00', '0000-00-00', NULL),
(127, 9, 87, 'shipping', 2, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_upload`
--

CREATE TABLE `oc_upload` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_upload`
--

INSERT INTO `oc_upload` (`id`, `name`, `filename`, `code`, `date_added`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'bkm.png', 'bkm.png.49191b23e949968a3282ae83352248f4', '6e4244175922fb82bfd9407bda5fee86309d9cbb', '2017-01-31 15:11:23', '0000-00-00', '0000-00-00', NULL),
(2, '15349584102102460988623823963043195565548482n.jpg', '15349584102102460988623823963043195565548482n.jpg.c0b3d50464d9244fbdf8e7f44bbecb42', '53bfa46db2531b8187b109869e446189bb9d88db', '2017-01-31 15:21:47', '0000-00-00', '0000-00-00', NULL),
(3, '15349584102102460988623823963043195565548482n.jpg', '15349584102102460988623823963043195565548482n.jpg.7fdfc88bc398b21bd5fef75f0c85ea37', '1656df4d50e7ad235bb4b55053f60f0e72078382', '2017-01-31 16:13:24', '0000-00-00', '0000-00-00', NULL),
(4, 'IMG20161204220449.jpg', 'IMG20161204220449.jpg.0ec3343e20b72288554ac45a5cc0c1ed', 'c895bf8c13c11c9a8c0e33000b322d5ab6fb348a', '2017-01-31 16:13:38', '0000-00-00', '0000-00-00', NULL),
(5, 'bkm.png', 'bkm.png.7e4a2402a441c53fe304deb5acbbb320', 'bc1a994a4cf7df962fb80c7c104d32d80beefb8b', '2017-02-01 03:08:46', '0000-00-00', '0000-00-00', NULL),
(6, 'otak-otak.jpg', 'otak-otak.jpg.24ede1ac59cd9c2993ef4a14b3cd9cef', '19649864a0b1afe6fb7dcda586e411114cdb0b2c', '2017-02-02 03:36:40', '0000-00-00', '0000-00-00', NULL),
(7, 'kacang sembunyi.png', 'kacang sembunyi.png.ea2d0639f54e9bd7a5c0fc796a6941ba', '1af93a7128bea864c3f02c070be7d52fb6857516', '2017-04-22 04:33:24', '0000-00-00', '0000-00-00', NULL),
(8, 'kacang sembunyi.png', 'kacang sembunyi.png.d287d1afea5222d37aa34216d220dc23', '0ebc7f2be38e7eb1ee60c60a0b90e17798d7c921', '2017-04-22 04:34:31', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_url_alias`
--

CREATE TABLE `oc_url_alias` (
  `id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_url_alias`
--

INSERT INTO `oc_url_alias` (`id`, `query`, `keyword`, `created_at`, `updated_at`, `deleted_at`) VALUES
(845, 'category_id=20', 'desktops', '0000-00-00', '0000-00-00', NULL),
(834, 'category_id=26', 'pc', '0000-00-00', '0000-00-00', NULL),
(835, 'category_id=27', 'mac', '0000-00-00', '0000-00-00', NULL),
(730, 'manufacturer_id=8', 'apple', '0000-00-00', '0000-00-00', NULL),
(772, 'information_id=4', 'about_us', '0000-00-00', '0000-00-00', NULL),
(789, 'category_id=34', 'mp3-players', '0000-00-00', '0000-00-00', NULL),
(781, 'category_id=36', 'test2', '0000-00-00', '0000-00-00', NULL),
(849, 'category_id=18', 'laptop-notebook', '0000-00-00', '0000-00-00', NULL),
(775, 'category_id=46', 'macs', '0000-00-00', '0000-00-00', NULL),
(776, 'category_id=45', 'windows', '0000-00-00', '0000-00-00', NULL),
(848, 'category_id=25', 'component', '0000-00-00', '0000-00-00', NULL),
(778, 'category_id=29', 'mouse', '0000-00-00', '0000-00-00', NULL),
(779, 'category_id=28', 'monitor', '0000-00-00', '0000-00-00', NULL),
(780, 'category_id=35', 'test1', '0000-00-00', '0000-00-00', NULL),
(782, 'category_id=30', 'printer', '0000-00-00', '0000-00-00', NULL),
(783, 'category_id=31', 'scanner', '0000-00-00', '0000-00-00', NULL),
(784, 'category_id=32', 'web-camera', '0000-00-00', '0000-00-00', NULL),
(785, 'category_id=57', 'tablet', '0000-00-00', '0000-00-00', NULL),
(786, 'category_id=17', 'software', '0000-00-00', '0000-00-00', NULL),
(787, 'category_id=24', 'smartphone', '0000-00-00', '0000-00-00', NULL),
(788, 'category_id=33', 'camera', '0000-00-00', '0000-00-00', NULL),
(790, 'category_id=43', 'test11', '0000-00-00', '0000-00-00', NULL),
(791, 'category_id=44', 'test12', '0000-00-00', '0000-00-00', NULL),
(792, 'category_id=47', 'test15', '0000-00-00', '0000-00-00', NULL),
(793, 'category_id=48', 'test16', '0000-00-00', '0000-00-00', NULL),
(794, 'category_id=49', 'test17', '0000-00-00', '0000-00-00', NULL),
(795, 'category_id=50', 'test18', '0000-00-00', '0000-00-00', NULL),
(796, 'category_id=51', 'test19', '0000-00-00', '0000-00-00', NULL),
(797, 'category_id=52', 'test20', '0000-00-00', '0000-00-00', NULL),
(798, 'category_id=58', 'test25', '0000-00-00', '0000-00-00', NULL),
(799, 'category_id=53', 'test21', '0000-00-00', '0000-00-00', NULL),
(800, 'category_id=54', 'test22', '0000-00-00', '0000-00-00', NULL),
(801, 'category_id=55', 'test23', '0000-00-00', '0000-00-00', NULL),
(802, 'category_id=56', 'test24', '0000-00-00', '0000-00-00', NULL),
(803, 'category_id=38', 'test4', '0000-00-00', '0000-00-00', NULL),
(804, 'category_id=37', 'test5', '0000-00-00', '0000-00-00', NULL),
(805, 'category_id=39', 'test6', '0000-00-00', '0000-00-00', NULL),
(806, 'category_id=40', 'test7', '0000-00-00', '0000-00-00', NULL),
(807, 'category_id=41', 'test8', '0000-00-00', '0000-00-00', NULL),
(808, 'category_id=42', 'test9', '0000-00-00', '0000-00-00', NULL),
(850, 'category_id=59', '', '0000-00-00', '0000-00-00', NULL),
(828, 'manufacturer_id=9', 'canon', '0000-00-00', '0000-00-00', NULL),
(829, 'manufacturer_id=5', 'htc', '0000-00-00', '0000-00-00', NULL),
(830, 'manufacturer_id=7', 'hewlett-packard', '0000-00-00', '0000-00-00', NULL),
(831, 'manufacturer_id=6', 'palm', '0000-00-00', '0000-00-00', NULL),
(832, 'manufacturer_id=10', 'sony', '0000-00-00', '0000-00-00', NULL),
(841, 'information_id=6', 'delivery', '0000-00-00', '0000-00-00', NULL),
(842, 'information_id=3', 'privacy', '0000-00-00', '0000-00-00', NULL),
(843, 'information_id=5', 'terms', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_user`
--

CREATE TABLE `oc_user` (
  `id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_user`
--

INSERT INTO `oc_user` (`id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`, `deleted_at`) VALUES
(1, 1, 'admin', '$2y$10$yiyuwUNOw4TKo1PRlT8cnusRPQ6vSfTbAsqSgkJ9SrXvyXxn/PWuO', '7194b9243', 'John', 'Doe', 'zuhryculli@gmail.com', '', '', '::1', 1, '2017-01-30 16:43:09', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_user_group`
--

INSERT INTO `oc_user_group` (`id`, `name`, `permission`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Administrator', 'a:2:{s:6:"access";a:205:{i:0;s:17:"catalog/attribute";i:1;s:23:"catalog/attribute_group";i:2;s:16:"catalog/category";i:3;s:16:"catalog/download";i:4;s:14:"catalog/filter";i:5;s:19:"catalog/information";i:6;s:20:"catalog/manufacturer";i:7;s:14:"catalog/option";i:8;s:15:"catalog/product";i:9;s:17:"catalog/recurring";i:10;s:14:"catalog/review";i:11;s:18:"common/column_left";i:12;s:18:"common/filemanager";i:13;s:11:"common/menu";i:14;s:14:"common/profile";i:15;s:12:"common/stats";i:16;s:13:"design/banner";i:17;s:13:"design/layout";i:18;s:14:"extension/feed";i:19;s:15:"extension/fraud";i:20;s:19:"extension/installer";i:21;s:22:"extension/modification";i:22;s:16:"extension/module";i:23;s:17:"extension/openbay";i:24;s:17:"extension/payment";i:25;s:18:"extension/shipping";i:26;s:15:"extension/total";i:27;s:16:"feed/google_base";i:28;s:19:"feed/google_sitemap";i:29;s:15:"feed/openbaypro";i:30;s:18:"fraud/fraudlabspro";i:31;s:13:"fraud/maxmind";i:32;s:20:"localisation/country";i:33;s:21:"localisation/currency";i:34;s:21:"localisation/geo_zone";i:35;s:21:"localisation/language";i:36;s:25:"localisation/length_class";i:37;s:21:"localisation/location";i:38;s:25:"localisation/order_status";i:39;s:26:"localisation/return_action";i:40;s:26:"localisation/return_reason";i:41;s:26:"localisation/return_status";i:42;s:25:"localisation/stock_status";i:43;s:22:"localisation/tax_class";i:44;s:21:"localisation/tax_rate";i:45;s:25:"localisation/weight_class";i:46;s:17:"localisation/zone";i:47;s:19:"marketing/affiliate";i:48;s:17:"marketing/contact";i:49;s:16:"marketing/coupon";i:50;s:19:"marketing/marketing";i:51;s:14:"module/account";i:52;s:16:"module/affiliate";i:53;s:20:"module/amazon_button";i:54;s:19:"module/amazon_login";i:55;s:17:"module/amazon_pay";i:56;s:13:"module/banner";i:57;s:17:"module/bestseller";i:58;s:15:"module/carousel";i:59;s:15:"module/category";i:60;s:22:"module/confirm_setting";i:61;s:19:"module/ebay_listing";i:62;s:15:"module/featured";i:63;s:13:"module/filter";i:64;s:22:"module/google_hangouts";i:65;s:11:"module/html";i:66;s:18:"module/information";i:67;s:18:"module/jne_setting";i:68;s:13:"module/latest";i:69;s:16:"module/pp_button";i:70;s:15:"module/pp_login";i:71;s:16:"module/slideshow";i:72;s:14:"module/special";i:73;s:12:"module/store";i:74;s:19:"module/tiki_setting";i:75;s:14:"openbay/amazon";i:76;s:22:"openbay/amazon_listing";i:77;s:22:"openbay/amazon_product";i:78;s:16:"openbay/amazonus";i:79;s:24:"openbay/amazonus_listing";i:80;s:24:"openbay/amazonus_product";i:81;s:12:"openbay/ebay";i:82;s:20:"openbay/ebay_profile";i:83;s:21:"openbay/ebay_template";i:84;s:12:"openbay/etsy";i:85;s:20:"openbay/etsy_product";i:86;s:21:"openbay/etsy_shipping";i:87;s:17:"openbay/etsy_shop";i:88;s:23:"payment/amazon_checkout";i:89;s:24:"payment/amazon_login_pay";i:90;s:24:"payment/authorizenet_aim";i:91;s:24:"payment/authorizenet_sim";i:92;s:16:"payment/bank_bni";i:93;s:21:"payment/bank_transfer";i:94;s:22:"payment/bluepay_hosted";i:95;s:24:"payment/bluepay_redirect";i:96;s:14:"payment/cheque";i:97;s:11:"payment/cod";i:98;s:17:"payment/firstdata";i:99;s:24:"payment/firstdata_remote";i:100;s:21:"payment/free_checkout";i:101;s:14:"payment/g2apay";i:102;s:17:"payment/globalpay";i:103;s:24:"payment/globalpay_remote";i:104;s:22:"payment/klarna_account";i:105;s:22:"payment/klarna_invoice";i:106;s:14:"payment/liqpay";i:107;s:14:"payment/nochex";i:108;s:15:"payment/paymate";i:109;s:16:"payment/paypoint";i:110;s:13:"payment/payza";i:111;s:26:"payment/perpetual_payments";i:112;s:18:"payment/pp_express";i:113;s:18:"payment/pp_payflow";i:114;s:25:"payment/pp_payflow_iframe";i:115;s:14:"payment/pp_pro";i:116;s:21:"payment/pp_pro_iframe";i:117;s:19:"payment/pp_standard";i:118;s:14:"payment/realex";i:119;s:21:"payment/realex_remote";i:120;s:22:"payment/sagepay_direct";i:121;s:22:"payment/sagepay_server";i:122;s:18:"payment/sagepay_us";i:123;s:24:"payment/securetrading_pp";i:124;s:24:"payment/securetrading_ws";i:125;s:14:"payment/skrill";i:126;s:19:"payment/twocheckout";i:127;s:28:"payment/web_payment_software";i:128;s:16:"payment/worldpay";i:129;s:16:"report/affiliate";i:130;s:25:"report/affiliate_activity";i:131;s:22:"report/affiliate_login";i:132;s:24:"report/customer_activity";i:133;s:22:"report/customer_credit";i:134;s:21:"report/customer_login";i:135;s:22:"report/customer_online";i:136;s:21:"report/customer_order";i:137;s:22:"report/customer_reward";i:138;s:16:"report/marketing";i:139;s:24:"report/product_purchased";i:140;s:21:"report/product_viewed";i:141;s:18:"report/sale_coupon";i:142;s:17:"report/sale_order";i:143;s:18:"report/sale_return";i:144;s:20:"report/sale_shipping";i:145;s:15:"report/sale_tax";i:146;s:12:"sale/confirm";i:147;s:17:"sale/custom_field";i:148;s:13:"sale/customer";i:149;s:20:"sale/customer_ban_ip";i:150;s:19:"sale/customer_group";i:151;s:10:"sale/order";i:152;s:14:"sale/recurring";i:153;s:11:"sale/return";i:154;s:12:"sale/voucher";i:155;s:18:"sale/voucher_theme";i:156;s:15:"setting/setting";i:157;s:13:"setting/store";i:158;s:16:"shipping/auspost";i:159;s:17:"shipping/citylink";i:160;s:14:"shipping/fedex";i:161;s:13:"shipping/flat";i:162;s:13:"shipping/free";i:163;s:13:"shipping/item";i:164;s:21:"shipping/jne_trucking";i:165;s:15:"shipping/jneoke";i:166;s:15:"shipping/jnereg";i:167;s:15:"shipping/jneyes";i:168;s:24:"shipping/kontak_shipping";i:169;s:23:"shipping/parcelforce_48";i:170;s:15:"shipping/pickup";i:171;s:19:"shipping/royal_mail";i:172;s:16:"shipping/tikieco";i:173;s:16:"shipping/tikihds";i:174;s:16:"shipping/tikions";i:175;s:16:"shipping/tikireg";i:176;s:16:"shipping/tikisds";i:177;s:12:"shipping/ups";i:178;s:13:"shipping/usps";i:179;s:15:"shipping/weight";i:180;s:11:"tool/backup";i:181;s:14:"tool/error_log";i:182;s:11:"tool/upload";i:183;s:12:"total/coupon";i:184;s:12:"total/credit";i:185;s:14:"total/handling";i:186;s:19:"total/jne_assurance";i:187;s:12:"total/jne_pk";i:188;s:16:"total/klarna_fee";i:189;s:19:"total/low_order_fee";i:190;s:12:"total/reward";i:191;s:14:"total/shipping";i:192;s:15:"total/sub_total";i:193;s:9:"total/tax";i:194;s:11:"total/total";i:195;s:13:"total/voucher";i:196;s:8:"user/api";i:197;s:9:"user/user";i:198;s:20:"user/user_permission";i:199;s:13:"module/latest";i:200;s:14:"module/special";i:201;s:14:"module/anylist";i:202;s:14:"module/chatnox";i:203;s:13:"shipping/flat";i:204;s:21:"shipping/jne_trucking";}s:6:"modify";a:205:{i:0;s:17:"catalog/attribute";i:1;s:23:"catalog/attribute_group";i:2;s:16:"catalog/category";i:3;s:16:"catalog/download";i:4;s:14:"catalog/filter";i:5;s:19:"catalog/information";i:6;s:20:"catalog/manufacturer";i:7;s:14:"catalog/option";i:8;s:15:"catalog/product";i:9;s:17:"catalog/recurring";i:10;s:14:"catalog/review";i:11;s:18:"common/column_left";i:12;s:18:"common/filemanager";i:13;s:11:"common/menu";i:14;s:14:"common/profile";i:15;s:12:"common/stats";i:16;s:13:"design/banner";i:17;s:13:"design/layout";i:18;s:14:"extension/feed";i:19;s:15:"extension/fraud";i:20;s:19:"extension/installer";i:21;s:22:"extension/modification";i:22;s:16:"extension/module";i:23;s:17:"extension/openbay";i:24;s:17:"extension/payment";i:25;s:18:"extension/shipping";i:26;s:15:"extension/total";i:27;s:16:"feed/google_base";i:28;s:19:"feed/google_sitemap";i:29;s:15:"feed/openbaypro";i:30;s:18:"fraud/fraudlabspro";i:31;s:13:"fraud/maxmind";i:32;s:20:"localisation/country";i:33;s:21:"localisation/currency";i:34;s:21:"localisation/geo_zone";i:35;s:21:"localisation/language";i:36;s:25:"localisation/length_class";i:37;s:21:"localisation/location";i:38;s:25:"localisation/order_status";i:39;s:26:"localisation/return_action";i:40;s:26:"localisation/return_reason";i:41;s:26:"localisation/return_status";i:42;s:25:"localisation/stock_status";i:43;s:22:"localisation/tax_class";i:44;s:21:"localisation/tax_rate";i:45;s:25:"localisation/weight_class";i:46;s:17:"localisation/zone";i:47;s:19:"marketing/affiliate";i:48;s:17:"marketing/contact";i:49;s:16:"marketing/coupon";i:50;s:19:"marketing/marketing";i:51;s:14:"module/account";i:52;s:16:"module/affiliate";i:53;s:20:"module/amazon_button";i:54;s:19:"module/amazon_login";i:55;s:17:"module/amazon_pay";i:56;s:13:"module/banner";i:57;s:17:"module/bestseller";i:58;s:15:"module/carousel";i:59;s:15:"module/category";i:60;s:22:"module/confirm_setting";i:61;s:19:"module/ebay_listing";i:62;s:15:"module/featured";i:63;s:13:"module/filter";i:64;s:22:"module/google_hangouts";i:65;s:11:"module/html";i:66;s:18:"module/information";i:67;s:18:"module/jne_setting";i:68;s:13:"module/latest";i:69;s:16:"module/pp_button";i:70;s:15:"module/pp_login";i:71;s:16:"module/slideshow";i:72;s:14:"module/special";i:73;s:12:"module/store";i:74;s:19:"module/tiki_setting";i:75;s:14:"openbay/amazon";i:76;s:22:"openbay/amazon_listing";i:77;s:22:"openbay/amazon_product";i:78;s:16:"openbay/amazonus";i:79;s:24:"openbay/amazonus_listing";i:80;s:24:"openbay/amazonus_product";i:81;s:12:"openbay/ebay";i:82;s:20:"openbay/ebay_profile";i:83;s:21:"openbay/ebay_template";i:84;s:12:"openbay/etsy";i:85;s:20:"openbay/etsy_product";i:86;s:21:"openbay/etsy_shipping";i:87;s:17:"openbay/etsy_shop";i:88;s:23:"payment/amazon_checkout";i:89;s:24:"payment/amazon_login_pay";i:90;s:24:"payment/authorizenet_aim";i:91;s:24:"payment/authorizenet_sim";i:92;s:16:"payment/bank_bni";i:93;s:21:"payment/bank_transfer";i:94;s:22:"payment/bluepay_hosted";i:95;s:24:"payment/bluepay_redirect";i:96;s:14:"payment/cheque";i:97;s:11:"payment/cod";i:98;s:17:"payment/firstdata";i:99;s:24:"payment/firstdata_remote";i:100;s:21:"payment/free_checkout";i:101;s:14:"payment/g2apay";i:102;s:17:"payment/globalpay";i:103;s:24:"payment/globalpay_remote";i:104;s:22:"payment/klarna_account";i:105;s:22:"payment/klarna_invoice";i:106;s:14:"payment/liqpay";i:107;s:14:"payment/nochex";i:108;s:15:"payment/paymate";i:109;s:16:"payment/paypoint";i:110;s:13:"payment/payza";i:111;s:26:"payment/perpetual_payments";i:112;s:18:"payment/pp_express";i:113;s:18:"payment/pp_payflow";i:114;s:25:"payment/pp_payflow_iframe";i:115;s:14:"payment/pp_pro";i:116;s:21:"payment/pp_pro_iframe";i:117;s:19:"payment/pp_standard";i:118;s:14:"payment/realex";i:119;s:21:"payment/realex_remote";i:120;s:22:"payment/sagepay_direct";i:121;s:22:"payment/sagepay_server";i:122;s:18:"payment/sagepay_us";i:123;s:24:"payment/securetrading_pp";i:124;s:24:"payment/securetrading_ws";i:125;s:14:"payment/skrill";i:126;s:19:"payment/twocheckout";i:127;s:28:"payment/web_payment_software";i:128;s:16:"payment/worldpay";i:129;s:16:"report/affiliate";i:130;s:25:"report/affiliate_activity";i:131;s:22:"report/affiliate_login";i:132;s:24:"report/customer_activity";i:133;s:22:"report/customer_credit";i:134;s:21:"report/customer_login";i:135;s:22:"report/customer_online";i:136;s:21:"report/customer_order";i:137;s:22:"report/customer_reward";i:138;s:16:"report/marketing";i:139;s:24:"report/product_purchased";i:140;s:21:"report/product_viewed";i:141;s:18:"report/sale_coupon";i:142;s:17:"report/sale_order";i:143;s:18:"report/sale_return";i:144;s:20:"report/sale_shipping";i:145;s:15:"report/sale_tax";i:146;s:12:"sale/confirm";i:147;s:17:"sale/custom_field";i:148;s:13:"sale/customer";i:149;s:20:"sale/customer_ban_ip";i:150;s:19:"sale/customer_group";i:151;s:10:"sale/order";i:152;s:14:"sale/recurring";i:153;s:11:"sale/return";i:154;s:12:"sale/voucher";i:155;s:18:"sale/voucher_theme";i:156;s:15:"setting/setting";i:157;s:13:"setting/store";i:158;s:16:"shipping/auspost";i:159;s:17:"shipping/citylink";i:160;s:14:"shipping/fedex";i:161;s:13:"shipping/flat";i:162;s:13:"shipping/free";i:163;s:13:"shipping/item";i:164;s:21:"shipping/jne_trucking";i:165;s:15:"shipping/jneoke";i:166;s:15:"shipping/jnereg";i:167;s:15:"shipping/jneyes";i:168;s:24:"shipping/kontak_shipping";i:169;s:23:"shipping/parcelforce_48";i:170;s:15:"shipping/pickup";i:171;s:19:"shipping/royal_mail";i:172;s:16:"shipping/tikieco";i:173;s:16:"shipping/tikihds";i:174;s:16:"shipping/tikions";i:175;s:16:"shipping/tikireg";i:176;s:16:"shipping/tikisds";i:177;s:12:"shipping/ups";i:178;s:13:"shipping/usps";i:179;s:15:"shipping/weight";i:180;s:11:"tool/backup";i:181;s:14:"tool/error_log";i:182;s:11:"tool/upload";i:183;s:12:"total/coupon";i:184;s:12:"total/credit";i:185;s:14:"total/handling";i:186;s:19:"total/jne_assurance";i:187;s:12:"total/jne_pk";i:188;s:16:"total/klarna_fee";i:189;s:19:"total/low_order_fee";i:190;s:12:"total/reward";i:191;s:14:"total/shipping";i:192;s:15:"total/sub_total";i:193;s:9:"total/tax";i:194;s:11:"total/total";i:195;s:13:"total/voucher";i:196;s:8:"user/api";i:197;s:9:"user/user";i:198;s:20:"user/user_permission";i:199;s:13:"module/latest";i:200;s:14:"module/special";i:201;s:14:"module/anylist";i:202;s:14:"module/chatnox";i:203;s:13:"shipping/flat";i:204;s:21:"shipping/jne_trucking";}}', '0000-00-00', '0000-00-00', NULL),
(10, 'Demonstration', '', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`id`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg', '0000-00-00', '0000-00-00', NULL),
(7, 'catalog/demo/gift-voucher-birthday.jpg', '0000-00-00', '0000-00-00', NULL),
(6, 'catalog/demo/apple_logo.jpg', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`id`, `language_id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 2, 'Christmas', '0000-00-00', '0000-00-00', NULL),
(7, 2, 'Birthday', '0000-00-00', '0000-00-00', NULL),
(8, 2, 'General', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`id`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1.00000000', '0000-00-00', '0000-00-00', NULL),
(2, '1000.00000000', '0000-00-00', '0000-00-00', NULL),
(5, '2.20460000', '0000-00-00', '0000-00-00', NULL),
(6, '35.27400000', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`id`, `language_id`, `title`, `unit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Kilogram', 'kg', '0000-00-00', '0000-00-00', NULL),
(2, 2, 'Gram', 'g', '0000-00-00', '0000-00-00', NULL),
(5, 2, 'Pound ', 'lb', '0000-00-00', '0000-00-00', NULL),
(6, 2, 'Ounce', 'oz', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_zone`
--

CREATE TABLE `oc_zone` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_zone`
--

INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1, '0000-00-00', '0000-00-00', NULL),
(2, 1, 'Badghis', 'BDG', 1, '0000-00-00', '0000-00-00', NULL),
(3, 1, 'Baghlan', 'BGL', 1, '0000-00-00', '0000-00-00', NULL),
(4, 1, 'Balkh', 'BAL', 1, '0000-00-00', '0000-00-00', NULL),
(5, 1, 'Bamian', 'BAM', 1, '0000-00-00', '0000-00-00', NULL),
(6, 1, 'Farah', 'FRA', 1, '0000-00-00', '0000-00-00', NULL),
(7, 1, 'Faryab', 'FYB', 1, '0000-00-00', '0000-00-00', NULL),
(8, 1, 'Ghazni', 'GHA', 1, '0000-00-00', '0000-00-00', NULL),
(9, 1, 'Ghowr', 'GHO', 1, '0000-00-00', '0000-00-00', NULL),
(10, 1, 'Helmand', 'HEL', 1, '0000-00-00', '0000-00-00', NULL),
(11, 1, 'Herat', 'HER', 1, '0000-00-00', '0000-00-00', NULL),
(12, 1, 'Jowzjan', 'JOW', 1, '0000-00-00', '0000-00-00', NULL),
(13, 1, 'Kabul', 'KAB', 1, '0000-00-00', '0000-00-00', NULL),
(14, 1, 'Kandahar', 'KAN', 1, '0000-00-00', '0000-00-00', NULL),
(15, 1, 'Kapisa', 'KAP', 1, '0000-00-00', '0000-00-00', NULL),
(16, 1, 'Khost', 'KHO', 1, '0000-00-00', '0000-00-00', NULL),
(17, 1, 'Konar', 'KNR', 1, '0000-00-00', '0000-00-00', NULL),
(18, 1, 'Kondoz', 'KDZ', 1, '0000-00-00', '0000-00-00', NULL),
(19, 1, 'Laghman', 'LAG', 1, '0000-00-00', '0000-00-00', NULL),
(20, 1, 'Lowgar', 'LOW', 1, '0000-00-00', '0000-00-00', NULL),
(21, 1, 'Nangrahar', 'NAN', 1, '0000-00-00', '0000-00-00', NULL),
(22, 1, 'Nimruz', 'NIM', 1, '0000-00-00', '0000-00-00', NULL),
(23, 1, 'Nurestan', 'NUR', 1, '0000-00-00', '0000-00-00', NULL),
(24, 1, 'Oruzgan', 'ORU', 1, '0000-00-00', '0000-00-00', NULL),
(25, 1, 'Paktia', 'PIA', 1, '0000-00-00', '0000-00-00', NULL),
(26, 1, 'Paktika', 'PKA', 1, '0000-00-00', '0000-00-00', NULL),
(27, 1, 'Parwan', 'PAR', 1, '0000-00-00', '0000-00-00', NULL),
(28, 1, 'Samangan', 'SAM', 1, '0000-00-00', '0000-00-00', NULL),
(29, 1, 'Sar-e Pol', 'SAR', 1, '0000-00-00', '0000-00-00', NULL),
(30, 1, 'Takhar', 'TAK', 1, '0000-00-00', '0000-00-00', NULL),
(31, 1, 'Wardak', 'WAR', 1, '0000-00-00', '0000-00-00', NULL),
(32, 1, 'Zabol', 'ZAB', 1, '0000-00-00', '0000-00-00', NULL),
(33, 2, 'Berat', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(34, 2, 'Bulqize', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(35, 2, 'Delvine', 'DL', 1, '0000-00-00', '0000-00-00', NULL),
(36, 2, 'Devoll', 'DV', 1, '0000-00-00', '0000-00-00', NULL),
(37, 2, 'Diber', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(38, 2, 'Durres', 'DR', 1, '0000-00-00', '0000-00-00', NULL),
(39, 2, 'Elbasan', 'EL', 1, '0000-00-00', '0000-00-00', NULL),
(40, 2, 'Kolonje', 'ER', 1, '0000-00-00', '0000-00-00', NULL),
(41, 2, 'Fier', 'FR', 1, '0000-00-00', '0000-00-00', NULL),
(42, 2, 'Gjirokaster', 'GJ', 1, '0000-00-00', '0000-00-00', NULL),
(43, 2, 'Gramsh', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(44, 2, 'Has', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(45, 2, 'Kavaje', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(46, 2, 'Kurbin', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(47, 2, 'Kucove', 'KC', 1, '0000-00-00', '0000-00-00', NULL),
(48, 2, 'Korce', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(49, 2, 'Kruje', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(50, 2, 'Kukes', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(51, 2, 'Librazhd', 'LB', 1, '0000-00-00', '0000-00-00', NULL),
(52, 2, 'Lezhe', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(53, 2, 'Lushnje', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(54, 2, 'Malesi e Madhe', 'MM', 1, '0000-00-00', '0000-00-00', NULL),
(55, 2, 'Mallakaster', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(56, 2, 'Mat', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(57, 2, 'Mirdite', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(58, 2, 'Peqin', 'PQ', 1, '0000-00-00', '0000-00-00', NULL),
(59, 2, 'Permet', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(60, 2, 'Pogradec', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(61, 2, 'Puke', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(62, 2, 'Shkoder', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(63, 2, 'Skrapar', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(64, 2, 'Sarande', 'SR', 1, '0000-00-00', '0000-00-00', NULL),
(65, 2, 'Tepelene', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(66, 2, 'Tropoje', 'TP', 1, '0000-00-00', '0000-00-00', NULL),
(67, 2, 'Tirane', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(68, 2, 'Vlore', 'VL', 1, '0000-00-00', '0000-00-00', NULL),
(69, 3, 'Adrar', 'ADR', 1, '0000-00-00', '0000-00-00', NULL),
(70, 3, 'Ain Defla', 'ADE', 1, '0000-00-00', '0000-00-00', NULL),
(71, 3, 'Ain Temouchent', 'ATE', 1, '0000-00-00', '0000-00-00', NULL),
(72, 3, 'Alger', 'ALG', 1, '0000-00-00', '0000-00-00', NULL),
(73, 3, 'Annaba', 'ANN', 1, '0000-00-00', '0000-00-00', NULL),
(74, 3, 'Batna', 'BAT', 1, '0000-00-00', '0000-00-00', NULL),
(75, 3, 'Bechar', 'BEC', 1, '0000-00-00', '0000-00-00', NULL),
(76, 3, 'Bejaia', 'BEJ', 1, '0000-00-00', '0000-00-00', NULL),
(77, 3, 'Biskra', 'BIS', 1, '0000-00-00', '0000-00-00', NULL),
(78, 3, 'Blida', 'BLI', 1, '0000-00-00', '0000-00-00', NULL),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1, '0000-00-00', '0000-00-00', NULL),
(80, 3, 'Bouira', 'BOA', 1, '0000-00-00', '0000-00-00', NULL),
(81, 3, 'Boumerdes', 'BMD', 1, '0000-00-00', '0000-00-00', NULL),
(82, 3, 'Chlef', 'CHL', 1, '0000-00-00', '0000-00-00', NULL),
(83, 3, 'Constantine', 'CON', 1, '0000-00-00', '0000-00-00', NULL),
(84, 3, 'Djelfa', 'DJE', 1, '0000-00-00', '0000-00-00', NULL),
(85, 3, 'El Bayadh', 'EBA', 1, '0000-00-00', '0000-00-00', NULL),
(86, 3, 'El Oued', 'EOU', 1, '0000-00-00', '0000-00-00', NULL),
(87, 3, 'El Tarf', 'ETA', 1, '0000-00-00', '0000-00-00', NULL),
(88, 3, 'Ghardaia', 'GHA', 1, '0000-00-00', '0000-00-00', NULL),
(89, 3, 'Guelma', 'GUE', 1, '0000-00-00', '0000-00-00', NULL),
(90, 3, 'Illizi', 'ILL', 1, '0000-00-00', '0000-00-00', NULL),
(91, 3, 'Jijel', 'JIJ', 1, '0000-00-00', '0000-00-00', NULL),
(92, 3, 'Khenchela', 'KHE', 1, '0000-00-00', '0000-00-00', NULL),
(93, 3, 'Laghouat', 'LAG', 1, '0000-00-00', '0000-00-00', NULL),
(94, 3, 'Muaskar', 'MUA', 1, '0000-00-00', '0000-00-00', NULL),
(95, 3, 'Medea', 'MED', 1, '0000-00-00', '0000-00-00', NULL),
(96, 3, 'Mila', 'MIL', 1, '0000-00-00', '0000-00-00', NULL),
(97, 3, 'Mostaganem', 'MOS', 1, '0000-00-00', '0000-00-00', NULL),
(98, 3, 'M\'Sila', 'MSI', 1, '0000-00-00', '0000-00-00', NULL),
(99, 3, 'Naama', 'NAA', 1, '0000-00-00', '0000-00-00', NULL),
(100, 3, 'Oran', 'ORA', 1, '0000-00-00', '0000-00-00', NULL),
(101, 3, 'Ouargla', 'OUA', 1, '0000-00-00', '0000-00-00', NULL),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1, '0000-00-00', '0000-00-00', NULL),
(103, 3, 'Relizane', 'REL', 1, '0000-00-00', '0000-00-00', NULL),
(104, 3, 'Saida', 'SAI', 1, '0000-00-00', '0000-00-00', NULL),
(105, 3, 'Setif', 'SET', 1, '0000-00-00', '0000-00-00', NULL),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1, '0000-00-00', '0000-00-00', NULL),
(107, 3, 'Skikda', 'SKI', 1, '0000-00-00', '0000-00-00', NULL),
(108, 3, 'Souk Ahras', 'SAH', 1, '0000-00-00', '0000-00-00', NULL),
(109, 3, 'Tamanghasset', 'TAM', 1, '0000-00-00', '0000-00-00', NULL),
(110, 3, 'Tebessa', 'TEB', 1, '0000-00-00', '0000-00-00', NULL),
(111, 3, 'Tiaret', 'TIA', 1, '0000-00-00', '0000-00-00', NULL),
(112, 3, 'Tindouf', 'TIN', 1, '0000-00-00', '0000-00-00', NULL),
(113, 3, 'Tipaza', 'TIP', 1, '0000-00-00', '0000-00-00', NULL),
(114, 3, 'Tissemsilt', 'TIS', 1, '0000-00-00', '0000-00-00', NULL),
(115, 3, 'Tizi Ouzou', 'TOU', 1, '0000-00-00', '0000-00-00', NULL),
(116, 3, 'Tlemcen', 'TLE', 1, '0000-00-00', '0000-00-00', NULL),
(117, 4, 'Eastern', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(118, 4, 'Manu\'a', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(119, 4, 'Rose Island', 'R', 1, '0000-00-00', '0000-00-00', NULL),
(120, 4, 'Swains Island', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(121, 4, 'Western', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(122, 5, 'Andorra la Vella', 'ALV', 1, '0000-00-00', '0000-00-00', NULL),
(123, 5, 'Canillo', 'CAN', 1, '0000-00-00', '0000-00-00', NULL),
(124, 5, 'Encamp', 'ENC', 1, '0000-00-00', '0000-00-00', NULL),
(125, 5, 'Escaldes-Engordany', 'ESE', 1, '0000-00-00', '0000-00-00', NULL),
(126, 5, 'La Massana', 'LMA', 1, '0000-00-00', '0000-00-00', NULL),
(127, 5, 'Ordino', 'ORD', 1, '0000-00-00', '0000-00-00', NULL),
(128, 5, 'Sant Julia de Loria', 'SJL', 1, '0000-00-00', '0000-00-00', NULL),
(129, 6, 'Bengo', 'BGO', 1, '0000-00-00', '0000-00-00', NULL),
(130, 6, 'Benguela', 'BGU', 1, '0000-00-00', '0000-00-00', NULL),
(131, 6, 'Bie', 'BIE', 1, '0000-00-00', '0000-00-00', NULL),
(132, 6, 'Cabinda', 'CAB', 1, '0000-00-00', '0000-00-00', NULL),
(133, 6, 'Cuando-Cubango', 'CCU', 1, '0000-00-00', '0000-00-00', NULL),
(134, 6, 'Cuanza Norte', 'CNO', 1, '0000-00-00', '0000-00-00', NULL),
(135, 6, 'Cuanza Sul', 'CUS', 1, '0000-00-00', '0000-00-00', NULL),
(136, 6, 'Cunene', 'CNN', 1, '0000-00-00', '0000-00-00', NULL),
(137, 6, 'Huambo', 'HUA', 1, '0000-00-00', '0000-00-00', NULL),
(138, 6, 'Huila', 'HUI', 1, '0000-00-00', '0000-00-00', NULL),
(139, 6, 'Luanda', 'LUA', 1, '0000-00-00', '0000-00-00', NULL),
(140, 6, 'Lunda Norte', 'LNO', 1, '0000-00-00', '0000-00-00', NULL),
(141, 6, 'Lunda Sul', 'LSU', 1, '0000-00-00', '0000-00-00', NULL),
(142, 6, 'Malange', 'MAL', 1, '0000-00-00', '0000-00-00', NULL),
(143, 6, 'Moxico', 'MOX', 1, '0000-00-00', '0000-00-00', NULL),
(144, 6, 'Namibe', 'NAM', 1, '0000-00-00', '0000-00-00', NULL),
(145, 6, 'Uige', 'UIG', 1, '0000-00-00', '0000-00-00', NULL),
(146, 6, 'Zaire', 'ZAI', 1, '0000-00-00', '0000-00-00', NULL),
(147, 9, 'Saint George', 'ASG', 1, '0000-00-00', '0000-00-00', NULL),
(148, 9, 'Saint John', 'ASJ', 1, '0000-00-00', '0000-00-00', NULL),
(149, 9, 'Saint Mary', 'ASM', 1, '0000-00-00', '0000-00-00', NULL),
(150, 9, 'Saint Paul', 'ASL', 1, '0000-00-00', '0000-00-00', NULL),
(151, 9, 'Saint Peter', 'ASR', 1, '0000-00-00', '0000-00-00', NULL),
(152, 9, 'Saint Philip', 'ASH', 1, '0000-00-00', '0000-00-00', NULL),
(153, 9, 'Barbuda', 'BAR', 1, '0000-00-00', '0000-00-00', NULL),
(154, 9, 'Redonda', 'RED', 1, '0000-00-00', '0000-00-00', NULL),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(156, 10, 'Buenos Aires', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(157, 10, 'Catamarca', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(158, 10, 'Chaco', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(159, 10, 'Chubut', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(160, 10, 'Cordoba', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(161, 10, 'Corrientes', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(162, 10, 'Distrito Federal', 'DF', 1, '0000-00-00', '0000-00-00', NULL),
(163, 10, 'Entre Rios', 'ER', 1, '0000-00-00', '0000-00-00', NULL),
(164, 10, 'Formosa', 'FO', 1, '0000-00-00', '0000-00-00', NULL),
(165, 10, 'Jujuy', 'JU', 1, '0000-00-00', '0000-00-00', NULL),
(166, 10, 'La Pampa', 'LP', 1, '0000-00-00', '0000-00-00', NULL),
(167, 10, 'La Rioja', 'LR', 1, '0000-00-00', '0000-00-00', NULL),
(168, 10, 'Mendoza', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(169, 10, 'Misiones', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(170, 10, 'Neuquen', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(171, 10, 'Rio Negro', 'RN', 1, '0000-00-00', '0000-00-00', NULL),
(172, 10, 'Salta', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(173, 10, 'San Juan', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(174, 10, 'San Luis', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(175, 10, 'Santa Cruz', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(176, 10, 'Santa Fe', 'SF', 1, '0000-00-00', '0000-00-00', NULL),
(177, 10, 'Santiago del Estero', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(178, 10, 'Tierra del Fuego', 'TF', 1, '0000-00-00', '0000-00-00', NULL),
(179, 10, 'Tucuman', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(180, 11, 'Aragatsotn', 'AGT', 1, '0000-00-00', '0000-00-00', NULL),
(181, 11, 'Ararat', 'ARR', 1, '0000-00-00', '0000-00-00', NULL),
(182, 11, 'Armavir', 'ARM', 1, '0000-00-00', '0000-00-00', NULL),
(183, 11, 'Geghark\'unik\'', 'GEG', 1, '0000-00-00', '0000-00-00', NULL),
(184, 11, 'Kotayk\'', 'KOT', 1, '0000-00-00', '0000-00-00', NULL),
(185, 11, 'Lorri', 'LOR', 1, '0000-00-00', '0000-00-00', NULL),
(186, 11, 'Shirak', 'SHI', 1, '0000-00-00', '0000-00-00', NULL),
(187, 11, 'Syunik\'', 'SYU', 1, '0000-00-00', '0000-00-00', NULL),
(188, 11, 'Tavush', 'TAV', 1, '0000-00-00', '0000-00-00', NULL),
(189, 11, 'Vayots\' Dzor', 'VAY', 1, '0000-00-00', '0000-00-00', NULL),
(190, 11, 'Yerevan', 'YER', 1, '0000-00-00', '0000-00-00', NULL),
(191, 13, 'Australian Capital Territory', 'ACT', 1, '0000-00-00', '0000-00-00', NULL),
(192, 13, 'New South Wales', 'NSW', 1, '0000-00-00', '0000-00-00', NULL),
(193, 13, 'Northern Territory', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(194, 13, 'Queensland', 'QLD', 1, '0000-00-00', '0000-00-00', NULL),
(195, 13, 'South Australia', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(196, 13, 'Tasmania', 'TAS', 1, '0000-00-00', '0000-00-00', NULL),
(197, 13, 'Victoria', 'VIC', 1, '0000-00-00', '0000-00-00', NULL),
(198, 13, 'Western Australia', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(199, 14, 'Burgenland', 'BUR', 1, '0000-00-00', '0000-00-00', NULL),
(200, 14, 'Kärnten', 'KAR', 1, '0000-00-00', '0000-00-00', NULL),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1, '0000-00-00', '0000-00-00', NULL),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1, '0000-00-00', '0000-00-00', NULL),
(203, 14, 'Salzburg', 'SAL', 1, '0000-00-00', '0000-00-00', NULL),
(204, 14, 'Steiermark', 'STE', 1, '0000-00-00', '0000-00-00', NULL),
(205, 14, 'Tirol', 'TIR', 1, '0000-00-00', '0000-00-00', NULL),
(206, 14, 'Vorarlberg', 'VOR', 1, '0000-00-00', '0000-00-00', NULL),
(207, 14, 'Wien', 'WIE', 1, '0000-00-00', '0000-00-00', NULL),
(208, 15, 'Ali Bayramli', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(209, 15, 'Abseron', 'ABS', 1, '0000-00-00', '0000-00-00', NULL),
(210, 15, 'AgcabAdi', 'AGC', 1, '0000-00-00', '0000-00-00', NULL),
(211, 15, 'Agdam', 'AGM', 1, '0000-00-00', '0000-00-00', NULL),
(212, 15, 'Agdas', 'AGS', 1, '0000-00-00', '0000-00-00', NULL),
(213, 15, 'Agstafa', 'AGA', 1, '0000-00-00', '0000-00-00', NULL),
(214, 15, 'Agsu', 'AGU', 1, '0000-00-00', '0000-00-00', NULL),
(215, 15, 'Astara', 'AST', 1, '0000-00-00', '0000-00-00', NULL),
(216, 15, 'Baki', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(217, 15, 'BabAk', 'BAB', 1, '0000-00-00', '0000-00-00', NULL),
(218, 15, 'BalakAn', 'BAL', 1, '0000-00-00', '0000-00-00', NULL),
(219, 15, 'BArdA', 'BAR', 1, '0000-00-00', '0000-00-00', NULL),
(220, 15, 'Beylaqan', 'BEY', 1, '0000-00-00', '0000-00-00', NULL),
(221, 15, 'Bilasuvar', 'BIL', 1, '0000-00-00', '0000-00-00', NULL),
(222, 15, 'Cabrayil', 'CAB', 1, '0000-00-00', '0000-00-00', NULL),
(223, 15, 'Calilabab', 'CAL', 1, '0000-00-00', '0000-00-00', NULL),
(224, 15, 'Culfa', 'CUL', 1, '0000-00-00', '0000-00-00', NULL),
(225, 15, 'Daskasan', 'DAS', 1, '0000-00-00', '0000-00-00', NULL),
(226, 15, 'Davaci', 'DAV', 1, '0000-00-00', '0000-00-00', NULL),
(227, 15, 'Fuzuli', 'FUZ', 1, '0000-00-00', '0000-00-00', NULL),
(228, 15, 'Ganca', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(229, 15, 'Gadabay', 'GAD', 1, '0000-00-00', '0000-00-00', NULL),
(230, 15, 'Goranboy', 'GOR', 1, '0000-00-00', '0000-00-00', NULL),
(231, 15, 'Goycay', 'GOY', 1, '0000-00-00', '0000-00-00', NULL),
(232, 15, 'Haciqabul', 'HAC', 1, '0000-00-00', '0000-00-00', NULL),
(233, 15, 'Imisli', 'IMI', 1, '0000-00-00', '0000-00-00', NULL),
(234, 15, 'Ismayilli', 'ISM', 1, '0000-00-00', '0000-00-00', NULL),
(235, 15, 'Kalbacar', 'KAL', 1, '0000-00-00', '0000-00-00', NULL),
(236, 15, 'Kurdamir', 'KUR', 1, '0000-00-00', '0000-00-00', NULL),
(237, 15, 'Lankaran', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(238, 15, 'Lacin', 'LAC', 1, '0000-00-00', '0000-00-00', NULL),
(239, 15, 'Lankaran', 'LAN', 1, '0000-00-00', '0000-00-00', NULL),
(240, 15, 'Lerik', 'LER', 1, '0000-00-00', '0000-00-00', NULL),
(241, 15, 'Masalli', 'MAS', 1, '0000-00-00', '0000-00-00', NULL),
(242, 15, 'Mingacevir', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(243, 15, 'Naftalan', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(244, 15, 'Neftcala', 'NEF', 1, '0000-00-00', '0000-00-00', NULL),
(245, 15, 'Oguz', 'OGU', 1, '0000-00-00', '0000-00-00', NULL),
(246, 15, 'Ordubad', 'ORD', 1, '0000-00-00', '0000-00-00', NULL),
(247, 15, 'Qabala', 'QAB', 1, '0000-00-00', '0000-00-00', NULL),
(248, 15, 'Qax', 'QAX', 1, '0000-00-00', '0000-00-00', NULL),
(249, 15, 'Qazax', 'QAZ', 1, '0000-00-00', '0000-00-00', NULL),
(250, 15, 'Qobustan', 'QOB', 1, '0000-00-00', '0000-00-00', NULL),
(251, 15, 'Quba', 'QBA', 1, '0000-00-00', '0000-00-00', NULL),
(252, 15, 'Qubadli', 'QBI', 1, '0000-00-00', '0000-00-00', NULL),
(253, 15, 'Qusar', 'QUS', 1, '0000-00-00', '0000-00-00', NULL),
(254, 15, 'Saki', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(255, 15, 'Saatli', 'SAT', 1, '0000-00-00', '0000-00-00', NULL),
(256, 15, 'Sabirabad', 'SAB', 1, '0000-00-00', '0000-00-00', NULL),
(257, 15, 'Sadarak', 'SAD', 1, '0000-00-00', '0000-00-00', NULL),
(258, 15, 'Sahbuz', 'SAH', 1, '0000-00-00', '0000-00-00', NULL),
(259, 15, 'Saki', 'SAK', 1, '0000-00-00', '0000-00-00', NULL),
(260, 15, 'Salyan', 'SAL', 1, '0000-00-00', '0000-00-00', NULL),
(261, 15, 'Sumqayit', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(262, 15, 'Samaxi', 'SMI', 1, '0000-00-00', '0000-00-00', NULL),
(263, 15, 'Samkir', 'SKR', 1, '0000-00-00', '0000-00-00', NULL),
(264, 15, 'Samux', 'SMX', 1, '0000-00-00', '0000-00-00', NULL),
(265, 15, 'Sarur', 'SAR', 1, '0000-00-00', '0000-00-00', NULL),
(266, 15, 'Siyazan', 'SIY', 1, '0000-00-00', '0000-00-00', NULL),
(267, 15, 'Susa', 'SS', 1, '0000-00-00', '0000-00-00', NULL),
(268, 15, 'Susa', 'SUS', 1, '0000-00-00', '0000-00-00', NULL),
(269, 15, 'Tartar', 'TAR', 1, '0000-00-00', '0000-00-00', NULL),
(270, 15, 'Tovuz', 'TOV', 1, '0000-00-00', '0000-00-00', NULL),
(271, 15, 'Ucar', 'UCA', 1, '0000-00-00', '0000-00-00', NULL),
(272, 15, 'Xankandi', 'XA', 1, '0000-00-00', '0000-00-00', NULL),
(273, 15, 'Xacmaz', 'XAC', 1, '0000-00-00', '0000-00-00', NULL),
(274, 15, 'Xanlar', 'XAN', 1, '0000-00-00', '0000-00-00', NULL),
(275, 15, 'Xizi', 'XIZ', 1, '0000-00-00', '0000-00-00', NULL),
(276, 15, 'Xocali', 'XCI', 1, '0000-00-00', '0000-00-00', NULL),
(277, 15, 'Xocavand', 'XVD', 1, '0000-00-00', '0000-00-00', NULL),
(278, 15, 'Yardimli', 'YAR', 1, '0000-00-00', '0000-00-00', NULL),
(279, 15, 'Yevlax', 'YEV', 1, '0000-00-00', '0000-00-00', NULL),
(280, 15, 'Zangilan', 'ZAN', 1, '0000-00-00', '0000-00-00', NULL),
(281, 15, 'Zaqatala', 'ZAQ', 1, '0000-00-00', '0000-00-00', NULL),
(282, 15, 'Zardab', 'ZAR', 1, '0000-00-00', '0000-00-00', NULL),
(283, 15, 'Naxcivan', 'NX', 1, '0000-00-00', '0000-00-00', NULL),
(284, 16, 'Acklins', 'ACK', 1, '0000-00-00', '0000-00-00', NULL),
(285, 16, 'Berry Islands', 'BER', 1, '0000-00-00', '0000-00-00', NULL),
(286, 16, 'Bimini', 'BIM', 1, '0000-00-00', '0000-00-00', NULL),
(287, 16, 'Black Point', 'BLK', 1, '0000-00-00', '0000-00-00', NULL),
(288, 16, 'Cat Island', 'CAT', 1, '0000-00-00', '0000-00-00', NULL),
(289, 16, 'Central Abaco', 'CAB', 1, '0000-00-00', '0000-00-00', NULL),
(290, 16, 'Central Andros', 'CAN', 1, '0000-00-00', '0000-00-00', NULL),
(291, 16, 'Central Eleuthera', 'CEL', 1, '0000-00-00', '0000-00-00', NULL),
(292, 16, 'City of Freeport', 'FRE', 1, '0000-00-00', '0000-00-00', NULL),
(293, 16, 'Crooked Island', 'CRO', 1, '0000-00-00', '0000-00-00', NULL),
(294, 16, 'East Grand Bahama', 'EGB', 1, '0000-00-00', '0000-00-00', NULL),
(295, 16, 'Exuma', 'EXU', 1, '0000-00-00', '0000-00-00', NULL),
(296, 16, 'Grand Cay', 'GRD', 1, '0000-00-00', '0000-00-00', NULL),
(297, 16, 'Harbour Island', 'HAR', 1, '0000-00-00', '0000-00-00', NULL),
(298, 16, 'Hope Town', 'HOP', 1, '0000-00-00', '0000-00-00', NULL),
(299, 16, 'Inagua', 'INA', 1, '0000-00-00', '0000-00-00', NULL),
(300, 16, 'Long Island', 'LNG', 1, '0000-00-00', '0000-00-00', NULL),
(301, 16, 'Mangrove Cay', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(302, 16, 'Mayaguana', 'MAY', 1, '0000-00-00', '0000-00-00', NULL),
(303, 16, 'Moore\'s Island', 'MOO', 1, '0000-00-00', '0000-00-00', NULL),
(304, 16, 'North Abaco', 'NAB', 1, '0000-00-00', '0000-00-00', NULL),
(305, 16, 'North Andros', 'NAN', 1, '0000-00-00', '0000-00-00', NULL),
(306, 16, 'North Eleuthera', 'NEL', 1, '0000-00-00', '0000-00-00', NULL),
(307, 16, 'Ragged Island', 'RAG', 1, '0000-00-00', '0000-00-00', NULL),
(308, 16, 'Rum Cay', 'RUM', 1, '0000-00-00', '0000-00-00', NULL),
(309, 16, 'San Salvador', 'SAL', 1, '0000-00-00', '0000-00-00', NULL),
(310, 16, 'South Abaco', 'SAB', 1, '0000-00-00', '0000-00-00', NULL),
(311, 16, 'South Andros', 'SAN', 1, '0000-00-00', '0000-00-00', NULL),
(312, 16, 'South Eleuthera', 'SEL', 1, '0000-00-00', '0000-00-00', NULL),
(313, 16, 'Spanish Wells', 'SWE', 1, '0000-00-00', '0000-00-00', NULL),
(314, 16, 'West Grand Bahama', 'WGB', 1, '0000-00-00', '0000-00-00', NULL),
(315, 17, 'Capital', 'CAP', 1, '0000-00-00', '0000-00-00', NULL),
(316, 17, 'Central', 'CEN', 1, '0000-00-00', '0000-00-00', NULL),
(317, 17, 'Muharraq', 'MUH', 1, '0000-00-00', '0000-00-00', NULL),
(318, 17, 'Northern', 'NOR', 1, '0000-00-00', '0000-00-00', NULL),
(319, 17, 'Southern', 'SOU', 1, '0000-00-00', '0000-00-00', NULL),
(320, 18, 'Barisal', 'BAR', 1, '0000-00-00', '0000-00-00', NULL),
(321, 18, 'Chittagong', 'CHI', 1, '0000-00-00', '0000-00-00', NULL),
(322, 18, 'Dhaka', 'DHA', 1, '0000-00-00', '0000-00-00', NULL),
(323, 18, 'Khulna', 'KHU', 1, '0000-00-00', '0000-00-00', NULL),
(324, 18, 'Rajshahi', 'RAJ', 1, '0000-00-00', '0000-00-00', NULL),
(325, 18, 'Sylhet', 'SYL', 1, '0000-00-00', '0000-00-00', NULL),
(326, 19, 'Christ Church', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(327, 19, 'Saint Andrew', 'AND', 1, '0000-00-00', '0000-00-00', NULL),
(328, 19, 'Saint George', 'GEO', 1, '0000-00-00', '0000-00-00', NULL),
(329, 19, 'Saint James', 'JAM', 1, '0000-00-00', '0000-00-00', NULL),
(330, 19, 'Saint John', 'JOH', 1, '0000-00-00', '0000-00-00', NULL),
(331, 19, 'Saint Joseph', 'JOS', 1, '0000-00-00', '0000-00-00', NULL),
(332, 19, 'Saint Lucy', 'LUC', 1, '0000-00-00', '0000-00-00', NULL),
(333, 19, 'Saint Michael', 'MIC', 1, '0000-00-00', '0000-00-00', NULL),
(334, 19, 'Saint Peter', 'PET', 1, '0000-00-00', '0000-00-00', NULL),
(335, 19, 'Saint Philip', 'PHI', 1, '0000-00-00', '0000-00-00', NULL),
(336, 19, 'Saint Thomas', 'THO', 1, '0000-00-00', '0000-00-00', NULL),
(337, 20, 'Brestskaya (Brest)', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(339, 20, 'Horad Minsk', 'HM', 1, '0000-00-00', '0000-00-00', NULL),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1, '0000-00-00', '0000-00-00', NULL),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(342, 20, 'Minskaya', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(344, 21, 'Antwerpen', 'VAN', 1, '0000-00-00', '0000-00-00', NULL),
(345, 21, 'Brabant Wallon', 'WBR', 1, '0000-00-00', '0000-00-00', NULL),
(346, 21, 'Hainaut', 'WHT', 1, '0000-00-00', '0000-00-00', NULL),
(347, 21, 'Liège', 'WLG', 1, '0000-00-00', '0000-00-00', NULL),
(348, 21, 'Limburg', 'VLI', 1, '0000-00-00', '0000-00-00', NULL),
(349, 21, 'Luxembourg', 'WLX', 1, '0000-00-00', '0000-00-00', NULL),
(350, 21, 'Namur', 'WNA', 1, '0000-00-00', '0000-00-00', NULL),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1, '0000-00-00', '0000-00-00', NULL),
(352, 21, 'Vlaams Brabant', 'VBR', 1, '0000-00-00', '0000-00-00', NULL),
(353, 21, 'West-Vlaanderen', 'VWV', 1, '0000-00-00', '0000-00-00', NULL),
(354, 22, 'Belize', 'BZ', 1, '0000-00-00', '0000-00-00', NULL),
(355, 22, 'Cayo', 'CY', 1, '0000-00-00', '0000-00-00', NULL),
(356, 22, 'Corozal', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(357, 22, 'Orange Walk', 'OW', 1, '0000-00-00', '0000-00-00', NULL),
(358, 22, 'Stann Creek', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(359, 22, 'Toledo', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(360, 23, 'Alibori', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(361, 23, 'Atakora', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(362, 23, 'Atlantique', 'AQ', 1, '0000-00-00', '0000-00-00', NULL),
(363, 23, 'Borgou', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(364, 23, 'Collines', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(365, 23, 'Donga', 'DO', 1, '0000-00-00', '0000-00-00', NULL),
(366, 23, 'Kouffo', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(367, 23, 'Littoral', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(368, 23, 'Mono', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(369, 23, 'Oueme', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(370, 23, 'Plateau', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(371, 23, 'Zou', 'ZO', 1, '0000-00-00', '0000-00-00', NULL),
(372, 24, 'Devonshire', 'DS', 1, '0000-00-00', '0000-00-00', NULL),
(373, 24, 'Hamilton City', 'HC', 1, '0000-00-00', '0000-00-00', NULL),
(374, 24, 'Hamilton', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(375, 24, 'Paget', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(376, 24, 'Pembroke', 'PB', 1, '0000-00-00', '0000-00-00', NULL),
(377, 24, 'Saint George City', 'GC', 1, '0000-00-00', '0000-00-00', NULL),
(378, 24, 'Saint George\'s', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(379, 24, 'Sandys', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(380, 24, 'Smith\'s', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(381, 24, 'Southampton', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(382, 24, 'Warwick', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(383, 25, 'Bumthang', 'BUM', 1, '0000-00-00', '0000-00-00', NULL),
(384, 25, 'Chukha', 'CHU', 1, '0000-00-00', '0000-00-00', NULL),
(385, 25, 'Dagana', 'DAG', 1, '0000-00-00', '0000-00-00', NULL),
(386, 25, 'Gasa', 'GAS', 1, '0000-00-00', '0000-00-00', NULL),
(387, 25, 'Haa', 'HAA', 1, '0000-00-00', '0000-00-00', NULL),
(388, 25, 'Lhuntse', 'LHU', 1, '0000-00-00', '0000-00-00', NULL),
(389, 25, 'Mongar', 'MON', 1, '0000-00-00', '0000-00-00', NULL),
(390, 25, 'Paro', 'PAR', 1, '0000-00-00', '0000-00-00', NULL),
(391, 25, 'Pemagatshel', 'PEM', 1, '0000-00-00', '0000-00-00', NULL),
(392, 25, 'Punakha', 'PUN', 1, '0000-00-00', '0000-00-00', NULL),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1, '0000-00-00', '0000-00-00', NULL),
(394, 25, 'Samtse', 'SAT', 1, '0000-00-00', '0000-00-00', NULL),
(395, 25, 'Sarpang', 'SAR', 1, '0000-00-00', '0000-00-00', NULL),
(396, 25, 'Thimphu', 'THI', 1, '0000-00-00', '0000-00-00', NULL),
(397, 25, 'Trashigang', 'TRG', 1, '0000-00-00', '0000-00-00', NULL),
(398, 25, 'Trashiyangste', 'TRY', 1, '0000-00-00', '0000-00-00', NULL),
(399, 25, 'Trongsa', 'TRO', 1, '0000-00-00', '0000-00-00', NULL),
(400, 25, 'Tsirang', 'TSI', 1, '0000-00-00', '0000-00-00', NULL),
(401, 25, 'Wangdue Phodrang', 'WPH', 1, '0000-00-00', '0000-00-00', NULL),
(402, 25, 'Zhemgang', 'ZHE', 1, '0000-00-00', '0000-00-00', NULL),
(403, 26, 'Beni', 'BEN', 1, '0000-00-00', '0000-00-00', NULL),
(404, 26, 'Chuquisaca', 'CHU', 1, '0000-00-00', '0000-00-00', NULL),
(405, 26, 'Cochabamba', 'COC', 1, '0000-00-00', '0000-00-00', NULL),
(406, 26, 'La Paz', 'LPZ', 1, '0000-00-00', '0000-00-00', NULL),
(407, 26, 'Oruro', 'ORU', 1, '0000-00-00', '0000-00-00', NULL),
(408, 26, 'Pando', 'PAN', 1, '0000-00-00', '0000-00-00', NULL),
(409, 26, 'Potosi', 'POT', 1, '0000-00-00', '0000-00-00', NULL),
(410, 26, 'Santa Cruz', 'SCZ', 1, '0000-00-00', '0000-00-00', NULL),
(411, 26, 'Tarija', 'TAR', 1, '0000-00-00', '0000-00-00', NULL),
(412, 27, 'Brcko district', 'BRO', 1, '0000-00-00', '0000-00-00', NULL),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1, '0000-00-00', '0000-00-00', NULL),
(414, 27, 'Posavski Kanton', 'FPO', 1, '0000-00-00', '0000-00-00', NULL),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1, '0000-00-00', '0000-00-00', NULL),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1, '0000-00-00', '0000-00-00', NULL),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1, '0000-00-00', '0000-00-00', NULL),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1, '0000-00-00', '0000-00-00', NULL),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1, '0000-00-00', '0000-00-00', NULL),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1, '0000-00-00', '0000-00-00', NULL),
(421, 27, 'Kanton Sarajevo', 'FSA', 1, '0000-00-00', '0000-00-00', NULL),
(422, 27, 'Zapadnobosanska', 'FZA', 1, '0000-00-00', '0000-00-00', NULL),
(423, 27, 'Banja Luka', 'SBL', 1, '0000-00-00', '0000-00-00', NULL),
(424, 27, 'Doboj', 'SDO', 1, '0000-00-00', '0000-00-00', NULL),
(425, 27, 'Bijeljina', 'SBI', 1, '0000-00-00', '0000-00-00', NULL),
(426, 27, 'Vlasenica', 'SVL', 1, '0000-00-00', '0000-00-00', NULL),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1, '0000-00-00', '0000-00-00', NULL),
(428, 27, 'Foca', 'SFO', 1, '0000-00-00', '0000-00-00', NULL),
(429, 27, 'Trebinje', 'STR', 1, '0000-00-00', '0000-00-00', NULL),
(430, 28, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(431, 28, 'Ghanzi', 'GH', 1, '0000-00-00', '0000-00-00', NULL),
(432, 28, 'Kgalagadi', 'KD', 1, '0000-00-00', '0000-00-00', NULL),
(433, 28, 'Kgatleng', 'KT', 1, '0000-00-00', '0000-00-00', NULL),
(434, 28, 'Kweneng', 'KW', 1, '0000-00-00', '0000-00-00', NULL),
(435, 28, 'Ngamiland', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(436, 28, 'North East', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(437, 28, 'North West', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(438, 28, 'South East', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(439, 28, 'Southern', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(440, 30, 'Acre', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(441, 30, 'Alagoas', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(442, 30, 'Amapá', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(443, 30, 'Amazonas', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(444, 30, 'Bahia', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(445, 30, 'Ceará', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(446, 30, 'Distrito Federal', 'DF', 1, '0000-00-00', '0000-00-00', NULL),
(447, 30, 'Espírito Santo', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(448, 30, 'Goiás', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(449, 30, 'Maranhão', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(450, 30, 'Mato Grosso', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(451, 30, 'Mato Grosso do Sul', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(452, 30, 'Minas Gerais', 'MG', 1, '0000-00-00', '0000-00-00', NULL),
(453, 30, 'Pará', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(454, 30, 'Paraíba', 'PB', 1, '0000-00-00', '0000-00-00', NULL),
(455, 30, 'Paraná', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(456, 30, 'Pernambuco', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(457, 30, 'Piauí', 'PI', 1, '0000-00-00', '0000-00-00', NULL),
(458, 30, 'Rio de Janeiro', 'RJ', 1, '0000-00-00', '0000-00-00', NULL),
(459, 30, 'Rio Grande do Norte', 'RN', 1, '0000-00-00', '0000-00-00', NULL),
(460, 30, 'Rio Grande do Sul', 'RS', 1, '0000-00-00', '0000-00-00', NULL),
(461, 30, 'Rondônia', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(462, 30, 'Roraima', 'RR', 1, '0000-00-00', '0000-00-00', NULL),
(463, 30, 'Santa Catarina', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(464, 30, 'São Paulo', 'SP', 1, '0000-00-00', '0000-00-00', NULL),
(465, 30, 'Sergipe', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(466, 30, 'Tocantins', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(467, 31, 'Peros Banhos', 'PB', 1, '0000-00-00', '0000-00-00', NULL),
(468, 31, 'Salomon Islands', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(469, 31, 'Nelsons Island', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(470, 31, 'Three Brothers', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(471, 31, 'Eagle Islands', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(472, 31, 'Danger Island', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(473, 31, 'Egmont Islands', 'EG', 1, '0000-00-00', '0000-00-00', NULL),
(474, 31, 'Diego Garcia', 'DG', 1, '0000-00-00', '0000-00-00', NULL),
(475, 32, 'Belait', 'BEL', 1, '0000-00-00', '0000-00-00', NULL),
(476, 32, 'Brunei and Muara', 'BRM', 1, '0000-00-00', '0000-00-00', NULL),
(477, 32, 'Temburong', 'TEM', 1, '0000-00-00', '0000-00-00', NULL),
(478, 32, 'Tutong', 'TUT', 1, '0000-00-00', '0000-00-00', NULL),
(479, 33, 'Blagoevgrad', '', 1, '0000-00-00', '0000-00-00', NULL),
(480, 33, 'Burgas', '', 1, '0000-00-00', '0000-00-00', NULL),
(481, 33, 'Dobrich', '', 1, '0000-00-00', '0000-00-00', NULL),
(482, 33, 'Gabrovo', '', 1, '0000-00-00', '0000-00-00', NULL),
(483, 33, 'Haskovo', '', 1, '0000-00-00', '0000-00-00', NULL),
(484, 33, 'Kardjali', '', 1, '0000-00-00', '0000-00-00', NULL),
(485, 33, 'Kyustendil', '', 1, '0000-00-00', '0000-00-00', NULL),
(486, 33, 'Lovech', '', 1, '0000-00-00', '0000-00-00', NULL),
(487, 33, 'Montana', '', 1, '0000-00-00', '0000-00-00', NULL),
(488, 33, 'Pazardjik', '', 1, '0000-00-00', '0000-00-00', NULL),
(489, 33, 'Pernik', '', 1, '0000-00-00', '0000-00-00', NULL),
(490, 33, 'Pleven', '', 1, '0000-00-00', '0000-00-00', NULL),
(491, 33, 'Plovdiv', '', 1, '0000-00-00', '0000-00-00', NULL),
(492, 33, 'Razgrad', '', 1, '0000-00-00', '0000-00-00', NULL),
(493, 33, 'Shumen', '', 1, '0000-00-00', '0000-00-00', NULL),
(494, 33, 'Silistra', '', 1, '0000-00-00', '0000-00-00', NULL),
(495, 33, 'Sliven', '', 1, '0000-00-00', '0000-00-00', NULL),
(496, 33, 'Smolyan', '', 1, '0000-00-00', '0000-00-00', NULL),
(497, 33, 'Sofia', '', 1, '0000-00-00', '0000-00-00', NULL),
(498, 33, 'Sofia - town', '', 1, '0000-00-00', '0000-00-00', NULL),
(499, 33, 'Stara Zagora', '', 1, '0000-00-00', '0000-00-00', NULL),
(500, 33, 'Targovishte', '', 1, '0000-00-00', '0000-00-00', NULL),
(501, 33, 'Varna', '', 1, '0000-00-00', '0000-00-00', NULL),
(502, 33, 'Veliko Tarnovo', '', 1, '0000-00-00', '0000-00-00', NULL),
(503, 33, 'Vidin', '', 1, '0000-00-00', '0000-00-00', NULL),
(504, 33, 'Vratza', '', 1, '0000-00-00', '0000-00-00', NULL),
(505, 33, 'Yambol', '', 1, '0000-00-00', '0000-00-00', NULL),
(506, 34, 'Bale', 'BAL', 1, '0000-00-00', '0000-00-00', NULL),
(507, 34, 'Bam', 'BAM', 1, '0000-00-00', '0000-00-00', NULL),
(508, 34, 'Banwa', 'BAN', 1, '0000-00-00', '0000-00-00', NULL),
(509, 34, 'Bazega', 'BAZ', 1, '0000-00-00', '0000-00-00', NULL),
(510, 34, 'Bougouriba', 'BOR', 1, '0000-00-00', '0000-00-00', NULL),
(511, 34, 'Boulgou', 'BLG', 1, '0000-00-00', '0000-00-00', NULL),
(512, 34, 'Boulkiemde', 'BOK', 1, '0000-00-00', '0000-00-00', NULL),
(513, 34, 'Comoe', 'COM', 1, '0000-00-00', '0000-00-00', NULL),
(514, 34, 'Ganzourgou', 'GAN', 1, '0000-00-00', '0000-00-00', NULL),
(515, 34, 'Gnagna', 'GNA', 1, '0000-00-00', '0000-00-00', NULL),
(516, 34, 'Gourma', 'GOU', 1, '0000-00-00', '0000-00-00', NULL),
(517, 34, 'Houet', 'HOU', 1, '0000-00-00', '0000-00-00', NULL),
(518, 34, 'Ioba', 'IOA', 1, '0000-00-00', '0000-00-00', NULL),
(519, 34, 'Kadiogo', 'KAD', 1, '0000-00-00', '0000-00-00', NULL),
(520, 34, 'Kenedougou', 'KEN', 1, '0000-00-00', '0000-00-00', NULL),
(521, 34, 'Komondjari', 'KOD', 1, '0000-00-00', '0000-00-00', NULL),
(522, 34, 'Kompienga', 'KOP', 1, '0000-00-00', '0000-00-00', NULL),
(523, 34, 'Kossi', 'KOS', 1, '0000-00-00', '0000-00-00', NULL),
(524, 34, 'Koulpelogo', 'KOL', 1, '0000-00-00', '0000-00-00', NULL),
(525, 34, 'Kouritenga', 'KOT', 1, '0000-00-00', '0000-00-00', NULL),
(526, 34, 'Kourweogo', 'KOW', 1, '0000-00-00', '0000-00-00', NULL),
(527, 34, 'Leraba', 'LER', 1, '0000-00-00', '0000-00-00', NULL),
(528, 34, 'Loroum', 'LOR', 1, '0000-00-00', '0000-00-00', NULL),
(529, 34, 'Mouhoun', 'MOU', 1, '0000-00-00', '0000-00-00', NULL),
(530, 34, 'Nahouri', 'NAH', 1, '0000-00-00', '0000-00-00', NULL),
(531, 34, 'Namentenga', 'NAM', 1, '0000-00-00', '0000-00-00', NULL),
(532, 34, 'Nayala', 'NAY', 1, '0000-00-00', '0000-00-00', NULL),
(533, 34, 'Noumbiel', 'NOU', 1, '0000-00-00', '0000-00-00', NULL),
(534, 34, 'Oubritenga', 'OUB', 1, '0000-00-00', '0000-00-00', NULL),
(535, 34, 'Oudalan', 'OUD', 1, '0000-00-00', '0000-00-00', NULL),
(536, 34, 'Passore', 'PAS', 1, '0000-00-00', '0000-00-00', NULL),
(537, 34, 'Poni', 'PON', 1, '0000-00-00', '0000-00-00', NULL),
(538, 34, 'Sanguie', 'SAG', 1, '0000-00-00', '0000-00-00', NULL),
(539, 34, 'Sanmatenga', 'SAM', 1, '0000-00-00', '0000-00-00', NULL),
(540, 34, 'Seno', 'SEN', 1, '0000-00-00', '0000-00-00', NULL),
(541, 34, 'Sissili', 'SIS', 1, '0000-00-00', '0000-00-00', NULL),
(542, 34, 'Soum', 'SOM', 1, '0000-00-00', '0000-00-00', NULL),
(543, 34, 'Sourou', 'SOR', 1, '0000-00-00', '0000-00-00', NULL),
(544, 34, 'Tapoa', 'TAP', 1, '0000-00-00', '0000-00-00', NULL),
(545, 34, 'Tuy', 'TUY', 1, '0000-00-00', '0000-00-00', NULL),
(546, 34, 'Yagha', 'YAG', 1, '0000-00-00', '0000-00-00', NULL),
(547, 34, 'Yatenga', 'YAT', 1, '0000-00-00', '0000-00-00', NULL),
(548, 34, 'Ziro', 'ZIR', 1, '0000-00-00', '0000-00-00', NULL),
(549, 34, 'Zondoma', 'ZOD', 1, '0000-00-00', '0000-00-00', NULL),
(550, 34, 'Zoundweogo', 'ZOW', 1, '0000-00-00', '0000-00-00', NULL),
(551, 35, 'Bubanza', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(552, 35, 'Bujumbura', 'BJ', 1, '0000-00-00', '0000-00-00', NULL),
(553, 35, 'Bururi', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(554, 35, 'Cankuzo', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(555, 35, 'Cibitoke', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(556, 35, 'Gitega', 'GI', 1, '0000-00-00', '0000-00-00', NULL),
(557, 35, 'Karuzi', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(558, 35, 'Kayanza', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(559, 35, 'Kirundo', 'KI', 1, '0000-00-00', '0000-00-00', NULL),
(560, 35, 'Makamba', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(561, 35, 'Muramvya', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(562, 35, 'Muyinga', 'MY', 1, '0000-00-00', '0000-00-00', NULL),
(563, 35, 'Mwaro', 'MW', 1, '0000-00-00', '0000-00-00', NULL),
(564, 35, 'Ngozi', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(565, 35, 'Rutana', 'RT', 1, '0000-00-00', '0000-00-00', NULL),
(566, 35, 'Ruyigi', 'RY', 1, '0000-00-00', '0000-00-00', NULL),
(567, 36, 'Phnom Penh', 'PP', 1, '0000-00-00', '0000-00-00', NULL),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1, '0000-00-00', '0000-00-00', NULL),
(569, 36, 'Pailin', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(570, 36, 'Keb', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(571, 36, 'Banteay Meanchey', 'BM', 1, '0000-00-00', '0000-00-00', NULL),
(572, 36, 'Battambang', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(573, 36, 'Kampong Cham', 'KM', 1, '0000-00-00', '0000-00-00', NULL),
(574, 36, 'Kampong Chhnang', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(575, 36, 'Kampong Speu', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(576, 36, 'Kampong Som', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(577, 36, 'Kampong Thom', 'KT', 1, '0000-00-00', '0000-00-00', NULL),
(578, 36, 'Kampot', 'KP', 1, '0000-00-00', '0000-00-00', NULL),
(579, 36, 'Kandal', 'KL', 1, '0000-00-00', '0000-00-00', NULL),
(580, 36, 'Kaoh Kong', 'KK', 1, '0000-00-00', '0000-00-00', NULL),
(581, 36, 'Kratie', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(582, 36, 'Mondul Kiri', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(583, 36, 'Oddar Meancheay', 'OM', 1, '0000-00-00', '0000-00-00', NULL),
(584, 36, 'Pursat', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(585, 36, 'Preah Vihear', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(586, 36, 'Prey Veng', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(587, 36, 'Ratanak Kiri', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(588, 36, 'Siemreap', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(589, 36, 'Stung Treng', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(590, 36, 'Svay Rieng', 'SR', 1, '0000-00-00', '0000-00-00', NULL),
(591, 36, 'Takeo', 'TK', 1, '0000-00-00', '0000-00-00', NULL),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1, '0000-00-00', '0000-00-00', NULL),
(593, 37, 'Centre', 'CEN', 1, '0000-00-00', '0000-00-00', NULL),
(594, 37, 'East (Est)', 'EST', 1, '0000-00-00', '0000-00-00', NULL),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1, '0000-00-00', '0000-00-00', NULL),
(596, 37, 'Littoral', 'LIT', 1, '0000-00-00', '0000-00-00', NULL),
(597, 37, 'North (Nord)', 'NOR', 1, '0000-00-00', '0000-00-00', NULL),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1, '0000-00-00', '0000-00-00', NULL),
(599, 37, 'West (Ouest)', 'OUE', 1, '0000-00-00', '0000-00-00', NULL),
(600, 37, 'South (Sud)', 'SUD', 1, '0000-00-00', '0000-00-00', NULL),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1, '0000-00-00', '0000-00-00', NULL),
(602, 38, 'Alberta', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(603, 38, 'British Columbia', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(604, 38, 'Manitoba', 'MB', 1, '0000-00-00', '0000-00-00', NULL),
(605, 38, 'New Brunswick', 'NB', 1, '0000-00-00', '0000-00-00', NULL),
(606, 38, 'Newfoundland and Labrador', 'NL', 1, '0000-00-00', '0000-00-00', NULL),
(607, 38, 'Northwest Territories', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(608, 38, 'Nova Scotia', 'NS', 1, '0000-00-00', '0000-00-00', NULL),
(609, 38, 'Nunavut', 'NU', 1, '0000-00-00', '0000-00-00', NULL),
(610, 38, 'Ontario', 'ON', 1, '0000-00-00', '0000-00-00', NULL),
(611, 38, 'Prince Edward Island', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(612, 38, 'Qu&eacute;bec', 'QC', 1, '0000-00-00', '0000-00-00', NULL),
(613, 38, 'Saskatchewan', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(614, 38, 'Yukon Territory', 'YT', 1, '0000-00-00', '0000-00-00', NULL),
(615, 39, 'Boa Vista', 'BV', 1, '0000-00-00', '0000-00-00', NULL),
(616, 39, 'Brava', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1, '0000-00-00', '0000-00-00', NULL),
(618, 39, 'Maio', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(619, 39, 'Mosteiros', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(620, 39, 'Paul', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(621, 39, 'Porto Novo', 'PN', 1, '0000-00-00', '0000-00-00', NULL),
(622, 39, 'Praia', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(623, 39, 'Ribeira Grande', 'RG', 1, '0000-00-00', '0000-00-00', NULL),
(624, 39, 'Sal', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(625, 39, 'Santa Catarina', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(626, 39, 'Santa Cruz', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(627, 39, 'Sao Domingos', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(628, 39, 'Sao Filipe', 'SF', 1, '0000-00-00', '0000-00-00', NULL),
(629, 39, 'Sao Nicolau', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(630, 39, 'Sao Vicente', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(631, 39, 'Tarrafal', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(632, 40, 'Creek', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(633, 40, 'Eastern', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(634, 40, 'Midland', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(635, 40, 'South Town', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(636, 40, 'Spot Bay', 'SP', 1, '0000-00-00', '0000-00-00', NULL),
(637, 40, 'Stake Bay', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(638, 40, 'West End', 'WD', 1, '0000-00-00', '0000-00-00', NULL),
(639, 40, 'Western', 'WN', 1, '0000-00-00', '0000-00-00', NULL),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1, '0000-00-00', '0000-00-00', NULL),
(641, 41, 'Basse-Kotto', 'BKO', 1, '0000-00-00', '0000-00-00', NULL),
(642, 41, 'Haute-Kotto', 'HKO', 1, '0000-00-00', '0000-00-00', NULL),
(643, 41, 'Haut-Mbomou', 'HMB', 1, '0000-00-00', '0000-00-00', NULL),
(644, 41, 'Kemo', 'KEM', 1, '0000-00-00', '0000-00-00', NULL),
(645, 41, 'Lobaye', 'LOB', 1, '0000-00-00', '0000-00-00', NULL),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1, '0000-00-00', '0000-00-00', NULL),
(647, 41, 'Mbomou', 'MBO', 1, '0000-00-00', '0000-00-00', NULL),
(648, 41, 'Nana-Mambere', 'NMM', 1, '0000-00-00', '0000-00-00', NULL),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1, '0000-00-00', '0000-00-00', NULL),
(650, 41, 'Ouaka', 'OUK', 1, '0000-00-00', '0000-00-00', NULL),
(651, 41, 'Ouham', 'OUH', 1, '0000-00-00', '0000-00-00', NULL),
(652, 41, 'Ouham-Pende', 'OPE', 1, '0000-00-00', '0000-00-00', NULL),
(653, 41, 'Vakaga', 'VAK', 1, '0000-00-00', '0000-00-00', NULL),
(654, 41, 'Nana-Grebizi', 'NGR', 1, '0000-00-00', '0000-00-00', NULL),
(655, 41, 'Sangha-Mbaere', 'SMB', 1, '0000-00-00', '0000-00-00', NULL),
(656, 41, 'Bangui', 'BAN', 1, '0000-00-00', '0000-00-00', NULL),
(657, 42, 'Batha', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(658, 42, 'Biltine', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(660, 42, 'Chari-Baguirmi', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(661, 42, 'Guera', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(662, 42, 'Kanem', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(663, 42, 'Lac', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(664, 42, 'Logone Occidental', 'LC', 1, '0000-00-00', '0000-00-00', NULL),
(665, 42, 'Logone Oriental', 'LR', 1, '0000-00-00', '0000-00-00', NULL),
(666, 42, 'Mayo-Kebbi', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(667, 42, 'Moyen-Chari', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(668, 42, 'Ouaddai', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(669, 42, 'Salamat', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(670, 42, 'Tandjile', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(672, 43, 'Antofagasta', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(673, 43, 'Araucania', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(674, 43, 'Atacama', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(675, 43, 'Bio-Bio', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(676, 43, 'Coquimbo', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(678, 43, 'Los Lagos', 'LL', 1, '0000-00-00', '0000-00-00', NULL),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(680, 43, 'Maule', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(681, 43, 'Region Metropolitana', 'RM', 1, '0000-00-00', '0000-00-00', NULL),
(682, 43, 'Tarapaca', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(683, 43, 'Valparaiso', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(684, 44, 'Anhui', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(685, 44, 'Beijing', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(686, 44, 'Chongqing', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(687, 44, 'Fujian', 'FU', 1, '0000-00-00', '0000-00-00', NULL),
(688, 44, 'Gansu', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(689, 44, 'Guangdong', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(690, 44, 'Guangxi', 'GX', 1, '0000-00-00', '0000-00-00', NULL),
(691, 44, 'Guizhou', 'GZ', 1, '0000-00-00', '0000-00-00', NULL),
(692, 44, 'Hainan', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(693, 44, 'Hebei', 'HB', 1, '0000-00-00', '0000-00-00', NULL),
(694, 44, 'Heilongjiang', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(695, 44, 'Henan', 'HE', 1, '0000-00-00', '0000-00-00', NULL),
(696, 44, 'Hong Kong', 'HK', 1, '0000-00-00', '0000-00-00', NULL),
(697, 44, 'Hubei', 'HU', 1, '0000-00-00', '0000-00-00', NULL),
(698, 44, 'Hunan', 'HN', 1, '0000-00-00', '0000-00-00', NULL),
(699, 44, 'Inner Mongolia', 'IM', 1, '0000-00-00', '0000-00-00', NULL),
(700, 44, 'Jiangsu', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(701, 44, 'Jiangxi', 'JX', 1, '0000-00-00', '0000-00-00', NULL),
(702, 44, 'Jilin', 'JL', 1, '0000-00-00', '0000-00-00', NULL),
(703, 44, 'Liaoning', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(704, 44, 'Macau', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(705, 44, 'Ningxia', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(706, 44, 'Shaanxi', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(707, 44, 'Shandong', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(708, 44, 'Shanghai', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(709, 44, 'Shanxi', 'SX', 1, '0000-00-00', '0000-00-00', NULL),
(710, 44, 'Sichuan', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(711, 44, 'Tianjin', 'TI', 1, '0000-00-00', '0000-00-00', NULL),
(712, 44, 'Xinjiang', 'XI', 1, '0000-00-00', '0000-00-00', NULL),
(713, 44, 'Yunnan', 'YU', 1, '0000-00-00', '0000-00-00', NULL),
(714, 44, 'Zhejiang', 'ZH', 1, '0000-00-00', '0000-00-00', NULL),
(715, 46, 'Direction Island', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(716, 46, 'Home Island', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(717, 46, 'Horsburgh Island', 'O', 1, '0000-00-00', '0000-00-00', NULL),
(718, 46, 'South Island', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(719, 46, 'West Island', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(720, 47, 'Amazonas', 'AMZ', 1, '0000-00-00', '0000-00-00', NULL),
(721, 47, 'Antioquia', 'ANT', 1, '0000-00-00', '0000-00-00', NULL),
(722, 47, 'Arauca', 'ARA', 1, '0000-00-00', '0000-00-00', NULL),
(723, 47, 'Atlantico', 'ATL', 1, '0000-00-00', '0000-00-00', NULL),
(724, 47, 'Bogota D.C.', 'BDC', 1, '0000-00-00', '0000-00-00', NULL),
(725, 47, 'Bolivar', 'BOL', 1, '0000-00-00', '0000-00-00', NULL),
(726, 47, 'Boyaca', 'BOY', 1, '0000-00-00', '0000-00-00', NULL),
(727, 47, 'Caldas', 'CAL', 1, '0000-00-00', '0000-00-00', NULL),
(728, 47, 'Caqueta', 'CAQ', 1, '0000-00-00', '0000-00-00', NULL),
(729, 47, 'Casanare', 'CAS', 1, '0000-00-00', '0000-00-00', NULL),
(730, 47, 'Cauca', 'CAU', 1, '0000-00-00', '0000-00-00', NULL),
(731, 47, 'Cesar', 'CES', 1, '0000-00-00', '0000-00-00', NULL),
(732, 47, 'Choco', 'CHO', 1, '0000-00-00', '0000-00-00', NULL),
(733, 47, 'Cordoba', 'COR', 1, '0000-00-00', '0000-00-00', NULL),
(734, 47, 'Cundinamarca', 'CAM', 1, '0000-00-00', '0000-00-00', NULL),
(735, 47, 'Guainia', 'GNA', 1, '0000-00-00', '0000-00-00', NULL),
(736, 47, 'Guajira', 'GJR', 1, '0000-00-00', '0000-00-00', NULL),
(737, 47, 'Guaviare', 'GVR', 1, '0000-00-00', '0000-00-00', NULL),
(738, 47, 'Huila', 'HUI', 1, '0000-00-00', '0000-00-00', NULL),
(739, 47, 'Magdalena', 'MAG', 1, '0000-00-00', '0000-00-00', NULL),
(740, 47, 'Meta', 'MET', 1, '0000-00-00', '0000-00-00', NULL),
(741, 47, 'Narino', 'NAR', 1, '0000-00-00', '0000-00-00', NULL),
(742, 47, 'Norte de Santander', 'NDS', 1, '0000-00-00', '0000-00-00', NULL),
(743, 47, 'Putumayo', 'PUT', 1, '0000-00-00', '0000-00-00', NULL),
(744, 47, 'Quindio', 'QUI', 1, '0000-00-00', '0000-00-00', NULL),
(745, 47, 'Risaralda', 'RIS', 1, '0000-00-00', '0000-00-00', NULL),
(746, 47, 'San Andres y Providencia', 'SAP', 1, '0000-00-00', '0000-00-00', NULL),
(747, 47, 'Santander', 'SAN', 1, '0000-00-00', '0000-00-00', NULL),
(748, 47, 'Sucre', 'SUC', 1, '0000-00-00', '0000-00-00', NULL),
(749, 47, 'Tolima', 'TOL', 1, '0000-00-00', '0000-00-00', NULL),
(750, 47, 'Valle del Cauca', 'VDC', 1, '0000-00-00', '0000-00-00', NULL),
(751, 47, 'Vaupes', 'VAU', 1, '0000-00-00', '0000-00-00', NULL),
(752, 47, 'Vichada', 'VIC', 1, '0000-00-00', '0000-00-00', NULL),
(753, 48, 'Grande Comore', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(754, 48, 'Anjouan', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(755, 48, 'Moheli', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(756, 49, 'Bouenza', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(757, 49, 'Brazzaville', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(758, 49, 'Cuvette', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(759, 49, 'Cuvette-Ouest', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(760, 49, 'Kouilou', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(761, 49, 'Lekoumou', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(762, 49, 'Likouala', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(763, 49, 'Niari', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(764, 49, 'Plateaux', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(765, 49, 'Pool', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(766, 49, 'Sangha', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(767, 50, 'Pukapuka', 'PU', 1, '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(768, 50, 'Rakahanga', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(769, 50, 'Manihiki', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(770, 50, 'Penrhyn', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(771, 50, 'Nassau Island', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(772, 50, 'Surwarrow', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(773, 50, 'Palmerston', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(774, 50, 'Aitutaki', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(775, 50, 'Manuae', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(776, 50, 'Takutea', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(777, 50, 'Mitiaro', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(778, 50, 'Atiu', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(779, 50, 'Mauke', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(780, 50, 'Rarotonga', 'RR', 1, '0000-00-00', '0000-00-00', NULL),
(781, 50, 'Mangaia', 'MG', 1, '0000-00-00', '0000-00-00', NULL),
(782, 51, 'Alajuela', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(783, 51, 'Cartago', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(784, 51, 'Guanacaste', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(785, 51, 'Heredia', 'HE', 1, '0000-00-00', '0000-00-00', NULL),
(786, 51, 'Limon', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(787, 51, 'Puntarenas', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(788, 51, 'San Jose', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(789, 52, 'Abengourou', 'ABE', 1, '0000-00-00', '0000-00-00', NULL),
(790, 52, 'Abidjan', 'ABI', 1, '0000-00-00', '0000-00-00', NULL),
(791, 52, 'Aboisso', 'ABO', 1, '0000-00-00', '0000-00-00', NULL),
(792, 52, 'Adiake', 'ADI', 1, '0000-00-00', '0000-00-00', NULL),
(793, 52, 'Adzope', 'ADZ', 1, '0000-00-00', '0000-00-00', NULL),
(794, 52, 'Agboville', 'AGB', 1, '0000-00-00', '0000-00-00', NULL),
(795, 52, 'Agnibilekrou', 'AGN', 1, '0000-00-00', '0000-00-00', NULL),
(796, 52, 'Alepe', 'ALE', 1, '0000-00-00', '0000-00-00', NULL),
(797, 52, 'Bocanda', 'BOC', 1, '0000-00-00', '0000-00-00', NULL),
(798, 52, 'Bangolo', 'BAN', 1, '0000-00-00', '0000-00-00', NULL),
(799, 52, 'Beoumi', 'BEO', 1, '0000-00-00', '0000-00-00', NULL),
(800, 52, 'Biankouma', 'BIA', 1, '0000-00-00', '0000-00-00', NULL),
(801, 52, 'Bondoukou', 'BDK', 1, '0000-00-00', '0000-00-00', NULL),
(802, 52, 'Bongouanou', 'BGN', 1, '0000-00-00', '0000-00-00', NULL),
(803, 52, 'Bouafle', 'BFL', 1, '0000-00-00', '0000-00-00', NULL),
(804, 52, 'Bouake', 'BKE', 1, '0000-00-00', '0000-00-00', NULL),
(805, 52, 'Bouna', 'BNA', 1, '0000-00-00', '0000-00-00', NULL),
(806, 52, 'Boundiali', 'BDL', 1, '0000-00-00', '0000-00-00', NULL),
(807, 52, 'Dabakala', 'DKL', 1, '0000-00-00', '0000-00-00', NULL),
(808, 52, 'Dabou', 'DBU', 1, '0000-00-00', '0000-00-00', NULL),
(809, 52, 'Daloa', 'DAL', 1, '0000-00-00', '0000-00-00', NULL),
(810, 52, 'Danane', 'DAN', 1, '0000-00-00', '0000-00-00', NULL),
(811, 52, 'Daoukro', 'DAO', 1, '0000-00-00', '0000-00-00', NULL),
(812, 52, 'Dimbokro', 'DIM', 1, '0000-00-00', '0000-00-00', NULL),
(813, 52, 'Divo', 'DIV', 1, '0000-00-00', '0000-00-00', NULL),
(814, 52, 'Duekoue', 'DUE', 1, '0000-00-00', '0000-00-00', NULL),
(815, 52, 'Ferkessedougou', 'FER', 1, '0000-00-00', '0000-00-00', NULL),
(816, 52, 'Gagnoa', 'GAG', 1, '0000-00-00', '0000-00-00', NULL),
(817, 52, 'Grand-Bassam', 'GBA', 1, '0000-00-00', '0000-00-00', NULL),
(818, 52, 'Grand-Lahou', 'GLA', 1, '0000-00-00', '0000-00-00', NULL),
(819, 52, 'Guiglo', 'GUI', 1, '0000-00-00', '0000-00-00', NULL),
(820, 52, 'Issia', 'ISS', 1, '0000-00-00', '0000-00-00', NULL),
(821, 52, 'Jacqueville', 'JAC', 1, '0000-00-00', '0000-00-00', NULL),
(822, 52, 'Katiola', 'KAT', 1, '0000-00-00', '0000-00-00', NULL),
(823, 52, 'Korhogo', 'KOR', 1, '0000-00-00', '0000-00-00', NULL),
(824, 52, 'Lakota', 'LAK', 1, '0000-00-00', '0000-00-00', NULL),
(825, 52, 'Man', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(826, 52, 'Mankono', 'MKN', 1, '0000-00-00', '0000-00-00', NULL),
(827, 52, 'Mbahiakro', 'MBA', 1, '0000-00-00', '0000-00-00', NULL),
(828, 52, 'Odienne', 'ODI', 1, '0000-00-00', '0000-00-00', NULL),
(829, 52, 'Oume', 'OUM', 1, '0000-00-00', '0000-00-00', NULL),
(830, 52, 'Sakassou', 'SAK', 1, '0000-00-00', '0000-00-00', NULL),
(831, 52, 'San-Pedro', 'SPE', 1, '0000-00-00', '0000-00-00', NULL),
(832, 52, 'Sassandra', 'SAS', 1, '0000-00-00', '0000-00-00', NULL),
(833, 52, 'Seguela', 'SEG', 1, '0000-00-00', '0000-00-00', NULL),
(834, 52, 'Sinfra', 'SIN', 1, '0000-00-00', '0000-00-00', NULL),
(835, 52, 'Soubre', 'SOU', 1, '0000-00-00', '0000-00-00', NULL),
(836, 52, 'Tabou', 'TAB', 1, '0000-00-00', '0000-00-00', NULL),
(837, 52, 'Tanda', 'TAN', 1, '0000-00-00', '0000-00-00', NULL),
(838, 52, 'Tiebissou', 'TIE', 1, '0000-00-00', '0000-00-00', NULL),
(839, 52, 'Tingrela', 'TIN', 1, '0000-00-00', '0000-00-00', NULL),
(840, 52, 'Tiassale', 'TIA', 1, '0000-00-00', '0000-00-00', NULL),
(841, 52, 'Touba', 'TBA', 1, '0000-00-00', '0000-00-00', NULL),
(842, 52, 'Toulepleu', 'TLP', 1, '0000-00-00', '0000-00-00', NULL),
(843, 52, 'Toumodi', 'TMD', 1, '0000-00-00', '0000-00-00', NULL),
(844, 52, 'Vavoua', 'VAV', 1, '0000-00-00', '0000-00-00', NULL),
(845, 52, 'Yamoussoukro', 'YAM', 1, '0000-00-00', '0000-00-00', NULL),
(846, 52, 'Zuenoula', 'ZUE', 1, '0000-00-00', '0000-00-00', NULL),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(848, 53, 'Grad Zagreb', 'GZ', 1, '0000-00-00', '0000-00-00', NULL),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1, '0000-00-00', '0000-00-00', NULL),
(850, 53, 'Istarska', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(851, 53, 'Karlovačka', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(852, 53, 'Koprivničko-križevačka', 'KK', 1, '0000-00-00', '0000-00-00', NULL),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1, '0000-00-00', '0000-00-00', NULL),
(854, 53, 'Ličko-senjska', 'LS', 1, '0000-00-00', '0000-00-00', NULL),
(855, 53, 'Međimurska', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(856, 53, 'Osječko-baranjska', 'OB', 1, '0000-00-00', '0000-00-00', NULL),
(857, 53, 'Požeško-slavonska', 'PS', 1, '0000-00-00', '0000-00-00', NULL),
(858, 53, 'Primorsko-goranska', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(859, 53, 'Šibensko-kninska', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(860, 53, 'Sisačko-moslavačka', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(861, 53, 'Brodsko-posavska', 'BP', 1, '0000-00-00', '0000-00-00', NULL),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(863, 53, 'Varaždinska', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(864, 53, 'Virovitičko-podravska', 'VP', 1, '0000-00-00', '0000-00-00', NULL),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(866, 53, 'Zadarska', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(867, 53, 'Zagrebačka', 'ZG', 1, '0000-00-00', '0000-00-00', NULL),
(868, 54, 'Camaguey', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(869, 54, 'Ciego de Avila', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(870, 54, 'Cienfuegos', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(871, 54, 'Ciudad de La Habana', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(872, 54, 'Granma', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(873, 54, 'Guantanamo', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(874, 54, 'Holguin', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(875, 54, 'Isla de la Juventud', 'IJ', 1, '0000-00-00', '0000-00-00', NULL),
(876, 54, 'La Habana', 'LH', 1, '0000-00-00', '0000-00-00', NULL),
(877, 54, 'Las Tunas', 'LT', 1, '0000-00-00', '0000-00-00', NULL),
(878, 54, 'Matanzas', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(879, 54, 'Pinar del Rio', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(880, 54, 'Sancti Spiritus', 'SS', 1, '0000-00-00', '0000-00-00', NULL),
(881, 54, 'Santiago de Cuba', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(882, 54, 'Villa Clara', 'VC', 1, '0000-00-00', '0000-00-00', NULL),
(883, 55, 'Famagusta', 'F', 1, '0000-00-00', '0000-00-00', NULL),
(884, 55, 'Kyrenia', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(885, 55, 'Larnaca', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(886, 55, 'Limassol', 'I', 1, '0000-00-00', '0000-00-00', NULL),
(887, 55, 'Nicosia', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(888, 55, 'Paphos', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(889, 56, 'Ústecký', 'U', 1, '0000-00-00', '0000-00-00', NULL),
(890, 56, 'Jihočeský', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(891, 56, 'Jihomoravský', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(892, 56, 'Karlovarský', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(893, 56, 'Královehradecký', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(894, 56, 'Liberecký', 'L', 1, '0000-00-00', '0000-00-00', NULL),
(895, 56, 'Moravskoslezský', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(896, 56, 'Olomoucký', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(897, 56, 'Pardubický', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(898, 56, 'Plzeňský', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(899, 56, 'Praha', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(900, 56, 'Středočeský', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(901, 56, 'Vysočina', 'J', 1, '0000-00-00', '0000-00-00', NULL),
(902, 56, 'Zlínský', 'Z', 1, '0000-00-00', '0000-00-00', NULL),
(903, 57, 'Arhus', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(904, 57, 'Bornholm', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(905, 57, 'Copenhagen', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(906, 57, 'Faroe Islands', 'FO', 1, '0000-00-00', '0000-00-00', NULL),
(907, 57, 'Frederiksborg', 'FR', 1, '0000-00-00', '0000-00-00', NULL),
(908, 57, 'Fyn', 'FY', 1, '0000-00-00', '0000-00-00', NULL),
(909, 57, 'Kobenhavn', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(910, 57, 'Nordjylland', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(911, 57, 'Ribe', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(912, 57, 'Ringkobing', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(913, 57, 'Roskilde', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(914, 57, 'Sonderjylland', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(915, 57, 'Storstrom', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(916, 57, 'Vejle', 'VK', 1, '0000-00-00', '0000-00-00', NULL),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1, '0000-00-00', '0000-00-00', NULL),
(918, 57, 'Viborg', 'VB', 1, '0000-00-00', '0000-00-00', NULL),
(919, 58, '\'Ali Sabih', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(920, 58, 'Dikhil', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(921, 58, 'Djibouti', 'J', 1, '0000-00-00', '0000-00-00', NULL),
(922, 58, 'Obock', 'O', 1, '0000-00-00', '0000-00-00', NULL),
(923, 58, 'Tadjoura', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(924, 59, 'Saint Andrew Parish', 'AND', 1, '0000-00-00', '0000-00-00', NULL),
(925, 59, 'Saint David Parish', 'DAV', 1, '0000-00-00', '0000-00-00', NULL),
(926, 59, 'Saint George Parish', 'GEO', 1, '0000-00-00', '0000-00-00', NULL),
(927, 59, 'Saint John Parish', 'JOH', 1, '0000-00-00', '0000-00-00', NULL),
(928, 59, 'Saint Joseph Parish', 'JOS', 1, '0000-00-00', '0000-00-00', NULL),
(929, 59, 'Saint Luke Parish', 'LUK', 1, '0000-00-00', '0000-00-00', NULL),
(930, 59, 'Saint Mark Parish', 'MAR', 1, '0000-00-00', '0000-00-00', NULL),
(931, 59, 'Saint Patrick Parish', 'PAT', 1, '0000-00-00', '0000-00-00', NULL),
(932, 59, 'Saint Paul Parish', 'PAU', 1, '0000-00-00', '0000-00-00', NULL),
(933, 59, 'Saint Peter Parish', 'PET', 1, '0000-00-00', '0000-00-00', NULL),
(934, 60, 'Distrito Nacional', 'DN', 1, '0000-00-00', '0000-00-00', NULL),
(935, 60, 'Azua', 'AZ', 1, '0000-00-00', '0000-00-00', NULL),
(936, 60, 'Baoruco', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(937, 60, 'Barahona', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(938, 60, 'Dajabon', 'DJ', 1, '0000-00-00', '0000-00-00', NULL),
(939, 60, 'Duarte', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(940, 60, 'Elias Pina', 'EL', 1, '0000-00-00', '0000-00-00', NULL),
(941, 60, 'El Seybo', 'SY', 1, '0000-00-00', '0000-00-00', NULL),
(942, 60, 'Espaillat', 'ET', 1, '0000-00-00', '0000-00-00', NULL),
(943, 60, 'Hato Mayor', 'HM', 1, '0000-00-00', '0000-00-00', NULL),
(944, 60, 'Independencia', 'IN', 1, '0000-00-00', '0000-00-00', NULL),
(945, 60, 'La Altagracia', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(946, 60, 'La Romana', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(947, 60, 'La Vega', 'VE', 1, '0000-00-00', '0000-00-00', NULL),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(949, 60, 'Monsenor Nouel', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(950, 60, 'Monte Cristi', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(951, 60, 'Monte Plata', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(952, 60, 'Pedernales', 'PD', 1, '0000-00-00', '0000-00-00', NULL),
(953, 60, 'Peravia (Bani)', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(954, 60, 'Puerto Plata', 'PP', 1, '0000-00-00', '0000-00-00', NULL),
(955, 60, 'Salcedo', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(956, 60, 'Samana', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(957, 60, 'Sanchez Ramirez', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(958, 60, 'San Cristobal', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(959, 60, 'San Jose de Ocoa', 'JO', 1, '0000-00-00', '0000-00-00', NULL),
(960, 60, 'San Juan', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(961, 60, 'San Pedro de Macoris', 'PM', 1, '0000-00-00', '0000-00-00', NULL),
(962, 60, 'Santiago', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(963, 60, 'Santiago Rodriguez', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(964, 60, 'Santo Domingo', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(965, 60, 'Valverde', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(966, 61, 'Aileu', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(967, 61, 'Ainaro', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(968, 61, 'Baucau', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(969, 61, 'Bobonaro', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(970, 61, 'Cova Lima', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(971, 61, 'Dili', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(972, 61, 'Ermera', 'ER', 1, '0000-00-00', '0000-00-00', NULL),
(973, 61, 'Lautem', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(974, 61, 'Liquica', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(975, 61, 'Manatuto', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(976, 61, 'Manufahi', 'MF', 1, '0000-00-00', '0000-00-00', NULL),
(977, 61, 'Oecussi', 'OE', 1, '0000-00-00', '0000-00-00', NULL),
(978, 61, 'Viqueque', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(979, 62, 'Azuay', 'AZU', 1, '0000-00-00', '0000-00-00', NULL),
(980, 62, 'Bolivar', 'BOL', 1, '0000-00-00', '0000-00-00', NULL),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1, '0000-00-00', '0000-00-00', NULL),
(982, 62, 'Carchi', 'CAR', 1, '0000-00-00', '0000-00-00', NULL),
(983, 62, 'Chimborazo', 'CHI', 1, '0000-00-00', '0000-00-00', NULL),
(984, 62, 'Cotopaxi', 'COT', 1, '0000-00-00', '0000-00-00', NULL),
(985, 62, 'El Oro', 'EOR', 1, '0000-00-00', '0000-00-00', NULL),
(986, 62, 'Esmeraldas', 'ESM', 1, '0000-00-00', '0000-00-00', NULL),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1, '0000-00-00', '0000-00-00', NULL),
(988, 62, 'Guayas', 'GUA', 1, '0000-00-00', '0000-00-00', NULL),
(989, 62, 'Imbabura', 'IMB', 1, '0000-00-00', '0000-00-00', NULL),
(990, 62, 'Loja', 'LOJ', 1, '0000-00-00', '0000-00-00', NULL),
(991, 62, 'Los Rios', 'LRO', 1, '0000-00-00', '0000-00-00', NULL),
(992, 62, 'Manab&iacute;', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(993, 62, 'Morona Santiago', 'MSA', 1, '0000-00-00', '0000-00-00', NULL),
(994, 62, 'Napo', 'NAP', 1, '0000-00-00', '0000-00-00', NULL),
(995, 62, 'Orellana', 'ORE', 1, '0000-00-00', '0000-00-00', NULL),
(996, 62, 'Pastaza', 'PAS', 1, '0000-00-00', '0000-00-00', NULL),
(997, 62, 'Pichincha', 'PIC', 1, '0000-00-00', '0000-00-00', NULL),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1, '0000-00-00', '0000-00-00', NULL),
(999, 62, 'Tungurahua', 'TUN', 1, '0000-00-00', '0000-00-00', NULL),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1, '0000-00-00', '0000-00-00', NULL),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1, '0000-00-00', '0000-00-00', NULL),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1, '0000-00-00', '0000-00-00', NULL),
(1003, 63, 'Al Buhayrah', 'BHY', 1, '0000-00-00', '0000-00-00', NULL),
(1004, 63, 'Al Fayyum', 'FYM', 1, '0000-00-00', '0000-00-00', NULL),
(1005, 63, 'Al Gharbiyah', 'GBY', 1, '0000-00-00', '0000-00-00', NULL),
(1006, 63, 'Al Iskandariyah', 'IDR', 1, '0000-00-00', '0000-00-00', NULL),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1, '0000-00-00', '0000-00-00', NULL),
(1008, 63, 'Al Jizah', 'JZH', 1, '0000-00-00', '0000-00-00', NULL),
(1009, 63, 'Al Minufiyah', 'MFY', 1, '0000-00-00', '0000-00-00', NULL),
(1010, 63, 'Al Minya', 'MNY', 1, '0000-00-00', '0000-00-00', NULL),
(1011, 63, 'Al Qahirah', 'QHR', 1, '0000-00-00', '0000-00-00', NULL),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1, '0000-00-00', '0000-00-00', NULL),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1, '0000-00-00', '0000-00-00', NULL),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1, '0000-00-00', '0000-00-00', NULL),
(1015, 63, 'As Suways', 'SWY', 1, '0000-00-00', '0000-00-00', NULL),
(1016, 63, 'Aswan', 'ASW', 1, '0000-00-00', '0000-00-00', NULL),
(1017, 63, 'Asyut', 'ASY', 1, '0000-00-00', '0000-00-00', NULL),
(1018, 63, 'Bani Suwayf', 'BSW', 1, '0000-00-00', '0000-00-00', NULL),
(1019, 63, 'Bur Sa\'id', 'BSD', 1, '0000-00-00', '0000-00-00', NULL),
(1020, 63, 'Dumyat', 'DMY', 1, '0000-00-00', '0000-00-00', NULL),
(1021, 63, 'Janub Sina\'', 'JNS', 1, '0000-00-00', '0000-00-00', NULL),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1, '0000-00-00', '0000-00-00', NULL),
(1023, 63, 'Matruh', 'MAT', 1, '0000-00-00', '0000-00-00', NULL),
(1024, 63, 'Qina', 'QIN', 1, '0000-00-00', '0000-00-00', NULL),
(1025, 63, 'Shamal Sina\'', 'SHS', 1, '0000-00-00', '0000-00-00', NULL),
(1026, 63, 'Suhaj', 'SUH', 1, '0000-00-00', '0000-00-00', NULL),
(1027, 64, 'Ahuachapan', 'AH', 1, '0000-00-00', '0000-00-00', NULL),
(1028, 64, 'Cabanas', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(1029, 64, 'Chalatenango', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1030, 64, 'Cuscatlan', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(1031, 64, 'La Libertad', 'LB', 1, '0000-00-00', '0000-00-00', NULL),
(1032, 64, 'La Paz', 'PZ', 1, '0000-00-00', '0000-00-00', NULL),
(1033, 64, 'La Union', 'UN', 1, '0000-00-00', '0000-00-00', NULL),
(1034, 64, 'Morazan', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(1035, 64, 'San Miguel', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(1036, 64, 'San Salvador', 'SS', 1, '0000-00-00', '0000-00-00', NULL),
(1037, 64, 'San Vicente', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(1038, 64, 'Santa Ana', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(1039, 64, 'Sonsonate', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(1040, 64, 'Usulutan', 'US', 1, '0000-00-00', '0000-00-00', NULL),
(1041, 65, 'Provincia Annobon', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(1044, 65, 'Provincia Centro Sur', 'CS', 1, '0000-00-00', '0000-00-00', NULL),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(1046, 65, 'Provincia Litoral', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1, '0000-00-00', '0000-00-00', NULL),
(1048, 66, 'Central (Maekel)', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1049, 66, 'Anseba (Keren)', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1, '0000-00-00', '0000-00-00', NULL),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(1052, 66, 'Southern (Debub)', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1, '0000-00-00', '0000-00-00', NULL),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1, '0000-00-00', '0000-00-00', NULL),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1, '0000-00-00', '0000-00-00', NULL),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1, '0000-00-00', '0000-00-00', NULL),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1, '0000-00-00', '0000-00-00', NULL),
(1069, 68, 'Afar', 'AF', 1, '0000-00-00', '0000-00-00', NULL),
(1070, 68, 'Amhara', 'AH', 1, '0000-00-00', '0000-00-00', NULL),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1, '0000-00-00', '0000-00-00', NULL),
(1072, 68, 'Gambela', 'GB', 1, '0000-00-00', '0000-00-00', NULL),
(1073, 68, 'Hariai', 'HR', 1, '0000-00-00', '0000-00-00', NULL),
(1074, 68, 'Oromia', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(1075, 68, 'Somali', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(1077, 68, 'Tigray', 'TG', 1, '0000-00-00', '0000-00-00', NULL),
(1078, 68, 'Addis Ababa', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(1079, 68, 'Dire Dawa', 'DD', 1, '0000-00-00', '0000-00-00', NULL),
(1080, 71, 'Central Division', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(1081, 71, 'Northern Division', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(1082, 71, 'Eastern Division', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(1083, 71, 'Western Division', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(1084, 71, 'Rotuma', 'R', 1, '0000-00-00', '0000-00-00', NULL),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1, '0000-00-00', '0000-00-00', NULL),
(1089, 72, 'Lapin lääni', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(1090, 72, 'Oulun lääni', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(1114, 74, 'Ain', '01', 1, '0000-00-00', '0000-00-00', NULL),
(1115, 74, 'Aisne', '02', 1, '0000-00-00', '0000-00-00', NULL),
(1116, 74, 'Allier', '03', 1, '0000-00-00', '0000-00-00', NULL),
(1117, 74, 'Alpes de Haute Provence', '04', 1, '0000-00-00', '0000-00-00', NULL),
(1118, 74, 'Hautes-Alpes', '05', 1, '0000-00-00', '0000-00-00', NULL),
(1119, 74, 'Alpes Maritimes', '06', 1, '0000-00-00', '0000-00-00', NULL),
(1120, 74, 'Ard&egrave;che', '07', 1, '0000-00-00', '0000-00-00', NULL),
(1121, 74, 'Ardennes', '08', 1, '0000-00-00', '0000-00-00', NULL),
(1122, 74, 'Ari&egrave;ge', '09', 1, '0000-00-00', '0000-00-00', NULL),
(1123, 74, 'Aube', '10', 1, '0000-00-00', '0000-00-00', NULL),
(1124, 74, 'Aude', '11', 1, '0000-00-00', '0000-00-00', NULL),
(1125, 74, 'Aveyron', '12', 1, '0000-00-00', '0000-00-00', NULL),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1, '0000-00-00', '0000-00-00', NULL),
(1127, 74, 'Calvados', '14', 1, '0000-00-00', '0000-00-00', NULL),
(1128, 74, 'Cantal', '15', 1, '0000-00-00', '0000-00-00', NULL),
(1129, 74, 'Charente', '16', 1, '0000-00-00', '0000-00-00', NULL),
(1130, 74, 'Charente Maritime', '17', 1, '0000-00-00', '0000-00-00', NULL),
(1131, 74, 'Cher', '18', 1, '0000-00-00', '0000-00-00', NULL),
(1132, 74, 'Corr&egrave;ze', '19', 1, '0000-00-00', '0000-00-00', NULL),
(1133, 74, 'Corse du Sud', '2A', 1, '0000-00-00', '0000-00-00', NULL),
(1134, 74, 'Haute Corse', '2B', 1, '0000-00-00', '0000-00-00', NULL),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1, '0000-00-00', '0000-00-00', NULL),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1, '0000-00-00', '0000-00-00', NULL),
(1137, 74, 'Creuse', '23', 1, '0000-00-00', '0000-00-00', NULL),
(1138, 74, 'Dordogne', '24', 1, '0000-00-00', '0000-00-00', NULL),
(1139, 74, 'Doubs', '25', 1, '0000-00-00', '0000-00-00', NULL),
(1140, 74, 'Dr&ocirc;me', '26', 1, '0000-00-00', '0000-00-00', NULL),
(1141, 74, 'Eure', '27', 1, '0000-00-00', '0000-00-00', NULL),
(1142, 74, 'Eure et Loir', '28', 1, '0000-00-00', '0000-00-00', NULL),
(1143, 74, 'Finist&egrave;re', '29', 1, '0000-00-00', '0000-00-00', NULL),
(1144, 74, 'Gard', '30', 1, '0000-00-00', '0000-00-00', NULL),
(1145, 74, 'Haute Garonne', '31', 1, '0000-00-00', '0000-00-00', NULL),
(1146, 74, 'Gers', '32', 1, '0000-00-00', '0000-00-00', NULL),
(1147, 74, 'Gironde', '33', 1, '0000-00-00', '0000-00-00', NULL),
(1148, 74, 'H&eacute;rault', '34', 1, '0000-00-00', '0000-00-00', NULL),
(1149, 74, 'Ille et Vilaine', '35', 1, '0000-00-00', '0000-00-00', NULL),
(1150, 74, 'Indre', '36', 1, '0000-00-00', '0000-00-00', NULL),
(1151, 74, 'Indre et Loire', '37', 1, '0000-00-00', '0000-00-00', NULL),
(1152, 74, 'Is&eacute;re', '38', 1, '0000-00-00', '0000-00-00', NULL),
(1153, 74, 'Jura', '39', 1, '0000-00-00', '0000-00-00', NULL),
(1154, 74, 'Landes', '40', 1, '0000-00-00', '0000-00-00', NULL),
(1155, 74, 'Loir et Cher', '41', 1, '0000-00-00', '0000-00-00', NULL),
(1156, 74, 'Loire', '42', 1, '0000-00-00', '0000-00-00', NULL),
(1157, 74, 'Haute Loire', '43', 1, '0000-00-00', '0000-00-00', NULL),
(1158, 74, 'Loire Atlantique', '44', 1, '0000-00-00', '0000-00-00', NULL),
(1159, 74, 'Loiret', '45', 1, '0000-00-00', '0000-00-00', NULL),
(1160, 74, 'Lot', '46', 1, '0000-00-00', '0000-00-00', NULL),
(1161, 74, 'Lot et Garonne', '47', 1, '0000-00-00', '0000-00-00', NULL),
(1162, 74, 'Loz&egrave;re', '48', 1, '0000-00-00', '0000-00-00', NULL),
(1163, 74, 'Maine et Loire', '49', 1, '0000-00-00', '0000-00-00', NULL),
(1164, 74, 'Manche', '50', 1, '0000-00-00', '0000-00-00', NULL),
(1165, 74, 'Marne', '51', 1, '0000-00-00', '0000-00-00', NULL),
(1166, 74, 'Haute Marne', '52', 1, '0000-00-00', '0000-00-00', NULL),
(1167, 74, 'Mayenne', '53', 1, '0000-00-00', '0000-00-00', NULL),
(1168, 74, 'Meurthe et Moselle', '54', 1, '0000-00-00', '0000-00-00', NULL),
(1169, 74, 'Meuse', '55', 1, '0000-00-00', '0000-00-00', NULL),
(1170, 74, 'Morbihan', '56', 1, '0000-00-00', '0000-00-00', NULL),
(1171, 74, 'Moselle', '57', 1, '0000-00-00', '0000-00-00', NULL),
(1172, 74, 'Ni&egrave;vre', '58', 1, '0000-00-00', '0000-00-00', NULL),
(1173, 74, 'Nord', '59', 1, '0000-00-00', '0000-00-00', NULL),
(1174, 74, 'Oise', '60', 1, '0000-00-00', '0000-00-00', NULL),
(1175, 74, 'Orne', '61', 1, '0000-00-00', '0000-00-00', NULL),
(1176, 74, 'Pas de Calais', '62', 1, '0000-00-00', '0000-00-00', NULL),
(1177, 74, 'Puy de D&ocirc;me', '63', 1, '0000-00-00', '0000-00-00', NULL),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1, '0000-00-00', '0000-00-00', NULL),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1, '0000-00-00', '0000-00-00', NULL),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1, '0000-00-00', '0000-00-00', NULL),
(1181, 74, 'Bas Rhin', '67', 1, '0000-00-00', '0000-00-00', NULL),
(1182, 74, 'Haut Rhin', '68', 1, '0000-00-00', '0000-00-00', NULL),
(1183, 74, 'Rh&ocirc;ne', '69', 1, '0000-00-00', '0000-00-00', NULL),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1, '0000-00-00', '0000-00-00', NULL),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1, '0000-00-00', '0000-00-00', NULL),
(1186, 74, 'Sarthe', '72', 1, '0000-00-00', '0000-00-00', NULL),
(1187, 74, 'Savoie', '73', 1, '0000-00-00', '0000-00-00', NULL),
(1188, 74, 'Haute Savoie', '74', 1, '0000-00-00', '0000-00-00', NULL),
(1189, 74, 'Paris', '75', 1, '0000-00-00', '0000-00-00', NULL),
(1190, 74, 'Seine Maritime', '76', 1, '0000-00-00', '0000-00-00', NULL),
(1191, 74, 'Seine et Marne', '77', 1, '0000-00-00', '0000-00-00', NULL),
(1192, 74, 'Yvelines', '78', 1, '0000-00-00', '0000-00-00', NULL),
(1193, 74, 'Deux S&egrave;vres', '79', 1, '0000-00-00', '0000-00-00', NULL),
(1194, 74, 'Somme', '80', 1, '0000-00-00', '0000-00-00', NULL),
(1195, 74, 'Tarn', '81', 1, '0000-00-00', '0000-00-00', NULL),
(1196, 74, 'Tarn et Garonne', '82', 1, '0000-00-00', '0000-00-00', NULL),
(1197, 74, 'Var', '83', 1, '0000-00-00', '0000-00-00', NULL),
(1198, 74, 'Vaucluse', '84', 1, '0000-00-00', '0000-00-00', NULL),
(1199, 74, 'Vend&eacute;e', '85', 1, '0000-00-00', '0000-00-00', NULL),
(1200, 74, 'Vienne', '86', 1, '0000-00-00', '0000-00-00', NULL),
(1201, 74, 'Haute Vienne', '87', 1, '0000-00-00', '0000-00-00', NULL),
(1202, 74, 'Vosges', '88', 1, '0000-00-00', '0000-00-00', NULL),
(1203, 74, 'Yonne', '89', 1, '0000-00-00', '0000-00-00', NULL),
(1204, 74, 'Territoire de Belfort', '90', 1, '0000-00-00', '0000-00-00', NULL),
(1205, 74, 'Essonne', '91', 1, '0000-00-00', '0000-00-00', NULL),
(1206, 74, 'Hauts de Seine', '92', 1, '0000-00-00', '0000-00-00', NULL),
(1207, 74, 'Seine St-Denis', '93', 1, '0000-00-00', '0000-00-00', NULL),
(1208, 74, 'Val de Marne', '94', 1, '0000-00-00', '0000-00-00', NULL),
(1209, 74, 'Val d\'Oise', '95', 1, '0000-00-00', '0000-00-00', NULL),
(1210, 76, 'Archipel des Marquises', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(1211, 76, 'Archipel des Tuamotu', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(1212, 76, 'Archipel des Tubuai', 'I', 1, '0000-00-00', '0000-00-00', NULL),
(1213, 76, 'Iles du Vent', 'V', 1, '0000-00-00', '0000-00-00', NULL),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(1215, 77, 'Iles Crozet', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(1216, 77, 'Iles Kerguelen', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(1217, 77, 'Ile Amsterdam', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(1218, 77, 'Ile Saint-Paul', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(1219, 77, 'Adelie Land', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(1220, 78, 'Estuaire', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(1221, 78, 'Haut-Ogooue', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(1222, 78, 'Moyen-Ogooue', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(1223, 78, 'Ngounie', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(1224, 78, 'Nyanga', 'NY', 1, '0000-00-00', '0000-00-00', NULL),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1, '0000-00-00', '0000-00-00', NULL),
(1226, 78, 'Ogooue-Lolo', 'OL', 1, '0000-00-00', '0000-00-00', NULL),
(1227, 78, 'Ogooue-Maritime', 'OM', 1, '0000-00-00', '0000-00-00', NULL),
(1228, 78, 'Woleu-Ntem', 'WN', 1, '0000-00-00', '0000-00-00', NULL),
(1229, 79, 'Banjul', 'BJ', 1, '0000-00-00', '0000-00-00', NULL),
(1230, 79, 'Basse', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(1231, 79, 'Brikama', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(1232, 79, 'Janjangbure', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1233, 79, 'Kanifeng', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1234, 79, 'Kerewan', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(1235, 79, 'Kuntaur', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(1236, 79, 'Mansakonko', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1237, 79, 'Lower River', 'LR', 1, '0000-00-00', '0000-00-00', NULL),
(1238, 79, 'Central River', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(1239, 79, 'North Bank', 'NB', 1, '0000-00-00', '0000-00-00', NULL),
(1240, 79, 'Upper River', 'UR', 1, '0000-00-00', '0000-00-00', NULL),
(1241, 79, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(1242, 80, 'Abkhazia', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(1243, 80, 'Ajaria', 'AJ', 1, '0000-00-00', '0000-00-00', NULL),
(1244, 80, 'Tbilisi', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(1245, 80, 'Guria', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(1246, 80, 'Imereti', 'IM', 1, '0000-00-00', '0000-00-00', NULL),
(1247, 80, 'Kakheti', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1248, 80, 'Kvemo Kartli', 'KK', 1, '0000-00-00', '0000-00-00', NULL),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1, '0000-00-00', '0000-00-00', NULL),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1, '0000-00-00', '0000-00-00', NULL),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1, '0000-00-00', '0000-00-00', NULL),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(1253, 80, 'Shida Kartli', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1, '0000-00-00', '0000-00-00', NULL),
(1255, 81, 'Bayern', 'BAY', 1, '0000-00-00', '0000-00-00', NULL),
(1256, 81, 'Berlin', 'BER', 1, '0000-00-00', '0000-00-00', NULL),
(1257, 81, 'Brandenburg', 'BRG', 1, '0000-00-00', '0000-00-00', NULL),
(1258, 81, 'Bremen', 'BRE', 1, '0000-00-00', '0000-00-00', NULL),
(1259, 81, 'Hamburg', 'HAM', 1, '0000-00-00', '0000-00-00', NULL),
(1260, 81, 'Hessen', 'HES', 1, '0000-00-00', '0000-00-00', NULL),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1, '0000-00-00', '0000-00-00', NULL),
(1262, 81, 'Niedersachsen', 'NDS', 1, '0000-00-00', '0000-00-00', NULL),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1, '0000-00-00', '0000-00-00', NULL),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1, '0000-00-00', '0000-00-00', NULL),
(1265, 81, 'Saarland', 'SAR', 1, '0000-00-00', '0000-00-00', NULL),
(1266, 81, 'Sachsen', 'SAS', 1, '0000-00-00', '0000-00-00', NULL),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1, '0000-00-00', '0000-00-00', NULL),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1, '0000-00-00', '0000-00-00', NULL),
(1269, 81, 'Th&uuml;ringen', 'THE', 1, '0000-00-00', '0000-00-00', NULL),
(1270, 82, 'Ashanti Region', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1272, 82, 'Central Region', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(1273, 82, 'Eastern Region', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(1274, 82, 'Greater Accra Region', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(1275, 82, 'Northern Region', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(1276, 82, 'Upper East Region', 'UE', 1, '0000-00-00', '0000-00-00', NULL),
(1277, 82, 'Upper West Region', 'UW', 1, '0000-00-00', '0000-00-00', NULL),
(1278, 82, 'Volta Region', 'VO', 1, '0000-00-00', '0000-00-00', NULL),
(1279, 82, 'Western Region', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(1280, 84, 'Attica', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(1281, 84, 'Central Greece', 'CN', 1, '0000-00-00', '0000-00-00', NULL),
(1282, 84, 'Central Macedonia', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(1283, 84, 'Crete', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1, '0000-00-00', '0000-00-00', NULL),
(1285, 84, 'Epirus', 'EP', 1, '0000-00-00', '0000-00-00', NULL),
(1286, 84, 'Ionian Islands', 'II', 1, '0000-00-00', '0000-00-00', NULL),
(1287, 84, 'North Aegean', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(1288, 84, 'Peloponnesos', 'PP', 1, '0000-00-00', '0000-00-00', NULL),
(1289, 84, 'South Aegean', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(1290, 84, 'Thessaly', 'TH', 1, '0000-00-00', '0000-00-00', NULL),
(1291, 84, 'West Greece', 'WG', 1, '0000-00-00', '0000-00-00', NULL),
(1292, 84, 'West Macedonia', 'WM', 1, '0000-00-00', '0000-00-00', NULL),
(1293, 85, 'Avannaa', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(1294, 85, 'Tunu', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(1295, 85, 'Kitaa', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(1296, 86, 'Saint Andrew', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(1297, 86, 'Saint David', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(1298, 86, 'Saint George', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(1299, 86, 'Saint John', 'J', 1, '0000-00-00', '0000-00-00', NULL),
(1300, 86, 'Saint Mark', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(1301, 86, 'Saint Patrick', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(1302, 86, 'Carriacou', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(1303, 86, 'Petit Martinique', 'Q', 1, '0000-00-00', '0000-00-00', NULL),
(1304, 89, 'Alta Verapaz', 'AV', 1, '0000-00-00', '0000-00-00', NULL),
(1305, 89, 'Baja Verapaz', 'BV', 1, '0000-00-00', '0000-00-00', NULL),
(1306, 89, 'Chimaltenango', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(1307, 89, 'Chiquimula', 'CQ', 1, '0000-00-00', '0000-00-00', NULL),
(1308, 89, 'El Peten', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(1309, 89, 'El Progreso', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(1310, 89, 'El Quiche', 'QC', 1, '0000-00-00', '0000-00-00', NULL),
(1311, 89, 'Escuintla', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(1312, 89, 'Guatemala', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(1313, 89, 'Huehuetenango', 'HU', 1, '0000-00-00', '0000-00-00', NULL),
(1314, 89, 'Izabal', 'IZ', 1, '0000-00-00', '0000-00-00', NULL),
(1315, 89, 'Jalapa', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1316, 89, 'Jutiapa', 'JU', 1, '0000-00-00', '0000-00-00', NULL),
(1317, 89, 'Quetzaltenango', 'QZ', 1, '0000-00-00', '0000-00-00', NULL),
(1318, 89, 'Retalhuleu', 'RE', 1, '0000-00-00', '0000-00-00', NULL),
(1319, 89, 'Sacatepequez', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(1320, 89, 'San Marcos', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(1321, 89, 'Santa Rosa', 'SR', 1, '0000-00-00', '0000-00-00', NULL),
(1322, 89, 'Solola', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(1323, 89, 'Suchitepequez', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(1324, 89, 'Totonicapan', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(1325, 89, 'Zacapa', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(1326, 90, 'Conakry', 'CNK', 1, '0000-00-00', '0000-00-00', NULL),
(1327, 90, 'Beyla', 'BYL', 1, '0000-00-00', '0000-00-00', NULL),
(1328, 90, 'Boffa', 'BFA', 1, '0000-00-00', '0000-00-00', NULL),
(1329, 90, 'Boke', 'BOK', 1, '0000-00-00', '0000-00-00', NULL),
(1330, 90, 'Coyah', 'COY', 1, '0000-00-00', '0000-00-00', NULL),
(1331, 90, 'Dabola', 'DBL', 1, '0000-00-00', '0000-00-00', NULL),
(1332, 90, 'Dalaba', 'DLB', 1, '0000-00-00', '0000-00-00', NULL),
(1333, 90, 'Dinguiraye', 'DGR', 1, '0000-00-00', '0000-00-00', NULL),
(1334, 90, 'Dubreka', 'DBR', 1, '0000-00-00', '0000-00-00', NULL),
(1335, 90, 'Faranah', 'FRN', 1, '0000-00-00', '0000-00-00', NULL),
(1336, 90, 'Forecariah', 'FRC', 1, '0000-00-00', '0000-00-00', NULL),
(1337, 90, 'Fria', 'FRI', 1, '0000-00-00', '0000-00-00', NULL),
(1338, 90, 'Gaoual', 'GAO', 1, '0000-00-00', '0000-00-00', NULL),
(1339, 90, 'Gueckedou', 'GCD', 1, '0000-00-00', '0000-00-00', NULL),
(1340, 90, 'Kankan', 'KNK', 1, '0000-00-00', '0000-00-00', NULL),
(1341, 90, 'Kerouane', 'KRN', 1, '0000-00-00', '0000-00-00', NULL),
(1342, 90, 'Kindia', 'KND', 1, '0000-00-00', '0000-00-00', NULL),
(1343, 90, 'Kissidougou', 'KSD', 1, '0000-00-00', '0000-00-00', NULL),
(1344, 90, 'Koubia', 'KBA', 1, '0000-00-00', '0000-00-00', NULL),
(1345, 90, 'Koundara', 'KDA', 1, '0000-00-00', '0000-00-00', NULL),
(1346, 90, 'Kouroussa', 'KRA', 1, '0000-00-00', '0000-00-00', NULL),
(1347, 90, 'Labe', 'LAB', 1, '0000-00-00', '0000-00-00', NULL),
(1348, 90, 'Lelouma', 'LLM', 1, '0000-00-00', '0000-00-00', NULL),
(1349, 90, 'Lola', 'LOL', 1, '0000-00-00', '0000-00-00', NULL),
(1350, 90, 'Macenta', 'MCT', 1, '0000-00-00', '0000-00-00', NULL),
(1351, 90, 'Mali', 'MAL', 1, '0000-00-00', '0000-00-00', NULL),
(1352, 90, 'Mamou', 'MAM', 1, '0000-00-00', '0000-00-00', NULL),
(1353, 90, 'Mandiana', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(1354, 90, 'Nzerekore', 'NZR', 1, '0000-00-00', '0000-00-00', NULL),
(1355, 90, 'Pita', 'PIT', 1, '0000-00-00', '0000-00-00', NULL),
(1356, 90, 'Siguiri', 'SIG', 1, '0000-00-00', '0000-00-00', NULL),
(1357, 90, 'Telimele', 'TLM', 1, '0000-00-00', '0000-00-00', NULL),
(1358, 90, 'Tougue', 'TOG', 1, '0000-00-00', '0000-00-00', NULL),
(1359, 90, 'Yomou', 'YOM', 1, '0000-00-00', '0000-00-00', NULL),
(1360, 91, 'Bafata Region', 'BF', 1, '0000-00-00', '0000-00-00', NULL),
(1361, 91, 'Biombo Region', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(1362, 91, 'Bissau Region', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(1363, 91, 'Bolama Region', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(1364, 91, 'Cacheu Region', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(1365, 91, 'Gabu Region', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(1366, 91, 'Oio Region', 'OI', 1, '0000-00-00', '0000-00-00', NULL),
(1367, 91, 'Quinara Region', 'QU', 1, '0000-00-00', '0000-00-00', NULL),
(1368, 91, 'Tombali Region', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(1369, 92, 'Barima-Waini', 'BW', 1, '0000-00-00', '0000-00-00', NULL),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(1371, 92, 'Demerara-Mahaica', 'DM', 1, '0000-00-00', '0000-00-00', NULL),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1, '0000-00-00', '0000-00-00', NULL),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1, '0000-00-00', '0000-00-00', NULL),
(1374, 92, 'Mahaica-Berbice', 'MB', 1, '0000-00-00', '0000-00-00', NULL),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1, '0000-00-00', '0000-00-00', NULL),
(1376, 92, 'Potaro-Siparuni', 'PI', 1, '0000-00-00', '0000-00-00', NULL),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1, '0000-00-00', '0000-00-00', NULL),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1, '0000-00-00', '0000-00-00', NULL),
(1379, 93, 'Artibonite', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(1380, 93, 'Centre', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(1381, 93, 'Grand\'Anse', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(1382, 93, 'Nord', 'ND', 1, '0000-00-00', '0000-00-00', NULL),
(1383, 93, 'Nord-Est', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(1384, 93, 'Nord-Ouest', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(1385, 93, 'Ouest', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(1386, 93, 'Sud', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(1387, 93, 'Sud-Est', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(1388, 94, 'Flat Island', 'F', 1, '0000-00-00', '0000-00-00', NULL),
(1389, 94, 'McDonald Island', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(1390, 94, 'Shag Island', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(1391, 94, 'Heard Island', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(1392, 95, 'Atlantida', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(1393, 95, 'Choluteca', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1394, 95, 'Colon', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(1395, 95, 'Comayagua', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(1396, 95, 'Copan', 'CP', 1, '0000-00-00', '0000-00-00', NULL),
(1397, 95, 'Cortes', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(1398, 95, 'El Paraiso', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(1399, 95, 'Francisco Morazan', 'FM', 1, '0000-00-00', '0000-00-00', NULL),
(1400, 95, 'Gracias a Dios', 'GD', 1, '0000-00-00', '0000-00-00', NULL),
(1401, 95, 'Intibuca', 'IN', 1, '0000-00-00', '0000-00-00', NULL),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1, '0000-00-00', '0000-00-00', NULL),
(1403, 95, 'La Paz', 'PZ', 1, '0000-00-00', '0000-00-00', NULL),
(1404, 95, 'Lempira', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(1405, 95, 'Ocotepeque', 'OC', 1, '0000-00-00', '0000-00-00', NULL),
(1406, 95, 'Olancho', 'OL', 1, '0000-00-00', '0000-00-00', NULL),
(1407, 95, 'Santa Barbara', 'SB', 1, '0000-00-00', '0000-00-00', NULL),
(1408, 95, 'Valle', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(1409, 95, 'Yoro', 'YO', 1, '0000-00-00', '0000-00-00', NULL),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1, '0000-00-00', '0000-00-00', NULL),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1, '0000-00-00', '0000-00-00', NULL),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1, '0000-00-00', '0000-00-00', NULL),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1, '0000-00-00', '0000-00-00', NULL),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1, '0000-00-00', '0000-00-00', NULL),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1, '0000-00-00', '0000-00-00', NULL),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1, '0000-00-00', '0000-00-00', NULL),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1, '0000-00-00', '0000-00-00', NULL),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1, '0000-00-00', '0000-00-00', NULL),
(1419, 96, 'Islands New Territories', 'NIS', 1, '0000-00-00', '0000-00-00', NULL),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1, '0000-00-00', '0000-00-00', NULL),
(1421, 96, 'North New Territories', 'NNO', 1, '0000-00-00', '0000-00-00', NULL),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1, '0000-00-00', '0000-00-00', NULL),
(1423, 96, 'Sha Tin New Territories', 'NST', 1, '0000-00-00', '0000-00-00', NULL),
(1424, 96, 'Tai Po New Territories', 'NTP', 1, '0000-00-00', '0000-00-00', NULL),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1, '0000-00-00', '0000-00-00', NULL),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1, '0000-00-00', '0000-00-00', NULL),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1, '0000-00-00', '0000-00-00', NULL),
(1467, 98, 'Austurland', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1, '0000-00-00', '0000-00-00', NULL),
(1469, 98, 'Norourland eystra', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(1470, 98, 'Norourland vestra', 'NV', 1, '0000-00-00', '0000-00-00', NULL),
(1471, 98, 'Suourland', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(1472, 98, 'Suournes', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(1473, 98, 'Vestfiroir', 'VF', 1, '0000-00-00', '0000-00-00', NULL),
(1474, 98, 'Vesturland', 'VL', 1, '0000-00-00', '0000-00-00', NULL),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(1476, 99, 'Andhra Pradesh', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(1477, 99, 'Arunachal Pradesh', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(1478, 99, 'Assam', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(1479, 99, 'Bihar', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(1480, 99, 'Chandigarh', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(1482, 99, 'Daman and Diu', 'DM', 1, '0000-00-00', '0000-00-00', NULL),
(1483, 99, 'Delhi', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(1484, 99, 'Goa', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(1485, 99, 'Gujarat', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(1486, 99, 'Haryana', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(1487, 99, 'Himachal Pradesh', 'HP', 1, '0000-00-00', '0000-00-00', NULL),
(1488, 99, 'Jammu and Kashmir', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1489, 99, 'Karnataka', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1490, 99, 'Kerala', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(1491, 99, 'Lakshadweep Islands', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(1492, 99, 'Madhya Pradesh', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(1493, 99, 'Maharashtra', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1494, 99, 'Manipur', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(1495, 99, 'Meghalaya', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(1496, 99, 'Mizoram', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(1497, 99, 'Nagaland', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(1498, 99, 'Orissa', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(1499, 99, 'Pondicherry', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(1500, 99, 'Punjab', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(1501, 99, 'Rajasthan', 'RA', 1, '0000-00-00', '0000-00-00', NULL),
(1502, 99, 'Sikkim', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(1503, 99, 'Tamil Nadu', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(1504, 99, 'Tripura', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(1505, 99, 'Uttar Pradesh', 'UP', 1, '0000-00-00', '0000-00-00', NULL),
(1506, 99, 'West Bengal', 'WB', 1, '0000-00-00', '0000-00-00', NULL),
(1507, 100, 'Aceh', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(1508, 100, 'Bali', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1509, 100, 'Banten', 'BT', 1, '0000-00-00', '0000-00-00', NULL),
(1510, 100, 'Bengkulu', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(1511, 100, 'BoDeTaBek', 'BD', 1, '0000-00-00', '0000-00-00', NULL),
(1512, 100, 'Gorontalo', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(1513, 100, 'Jakarta Raya', 'JK', 1, '0000-00-00', '0000-00-00', NULL),
(1514, 100, 'Jambi', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1515, 100, 'Jawa Barat', 'JB', 1, '0000-00-00', '0000-00-00', NULL),
(1516, 100, 'Jawa Tengah', 'JT', 1, '0000-00-00', '0000-00-00', NULL),
(1517, 100, 'Jawa Timur', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(1518, 100, 'Kalimantan Barat', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(1519, 100, 'Kalimantan Selatan', 'KS', 1, '0000-00-00', '0000-00-00', NULL),
(1520, 100, 'Kalimantan Tengah', 'KT', 1, '0000-00-00', '0000-00-00', NULL),
(1521, 100, 'Kalimantan Timur', 'KI', 1, '0000-00-00', '0000-00-00', NULL),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(1523, 100, 'Lampung', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(1524, 100, 'Maluku', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1525, 100, 'Maluku Utara', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1, '0000-00-00', '0000-00-00', NULL),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(1528, 100, 'Papua', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(1529, 100, 'Riau', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(1530, 100, 'Sulawesi Selatan', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(1531, 100, 'Sulawesi Tengah', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(1533, 100, 'Sulawesi Utara', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(1534, 100, 'Sumatera Barat', 'SB', 1, '0000-00-00', '0000-00-00', NULL),
(1535, 100, 'Sumatera Selatan', 'SS', 1, '0000-00-00', '0000-00-00', NULL),
(1536, 100, 'Sumatera Utara', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(1537, 100, 'Yogyakarta', 'YO', 1, '0000-00-00', '0000-00-00', NULL),
(1538, 101, 'Tehran', 'TEH', 1, '0000-00-00', '0000-00-00', NULL),
(1539, 101, 'Qom', 'QOM', 1, '0000-00-00', '0000-00-00', NULL),
(1540, 101, 'Markazi', 'MKZ', 1, '0000-00-00', '0000-00-00', NULL),
(1541, 101, 'Qazvin', 'QAZ', 1, '0000-00-00', '0000-00-00', NULL),
(1542, 101, 'Gilan', 'GIL', 1, '0000-00-00', '0000-00-00', NULL),
(1543, 101, 'Ardabil', 'ARD', 1, '0000-00-00', '0000-00-00', NULL),
(1544, 101, 'Zanjan', 'ZAN', 1, '0000-00-00', '0000-00-00', NULL),
(1545, 101, 'East Azarbaijan', 'EAZ', 1, '0000-00-00', '0000-00-00', NULL),
(1546, 101, 'West Azarbaijan', 'WEZ', 1, '0000-00-00', '0000-00-00', NULL),
(1547, 101, 'Kurdistan', 'KRD', 1, '0000-00-00', '0000-00-00', NULL),
(1548, 101, 'Hamadan', 'HMD', 1, '0000-00-00', '0000-00-00', NULL),
(1549, 101, 'Kermanshah', 'KRM', 1, '0000-00-00', '0000-00-00', NULL),
(1550, 101, 'Ilam', 'ILM', 1, '0000-00-00', '0000-00-00', NULL),
(1551, 101, 'Lorestan', 'LRS', 1, '0000-00-00', '0000-00-00', NULL),
(1552, 101, 'Khuzestan', 'KZT', 1, '0000-00-00', '0000-00-00', NULL),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1, '0000-00-00', '0000-00-00', NULL),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1, '0000-00-00', '0000-00-00', NULL),
(1555, 101, 'Bushehr', 'BSH', 1, '0000-00-00', '0000-00-00', NULL),
(1556, 101, 'Fars', 'FAR', 1, '0000-00-00', '0000-00-00', NULL),
(1557, 101, 'Hormozgan', 'HRM', 1, '0000-00-00', '0000-00-00', NULL),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1, '0000-00-00', '0000-00-00', NULL),
(1559, 101, 'Kerman', 'KRB', 1, '0000-00-00', '0000-00-00', NULL),
(1560, 101, 'Yazd', 'YZD', 1, '0000-00-00', '0000-00-00', NULL),
(1561, 101, 'Esfahan', 'EFH', 1, '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1562, 101, 'Semnan', 'SMN', 1, '0000-00-00', '0000-00-00', NULL),
(1563, 101, 'Mazandaran', 'MZD', 1, '0000-00-00', '0000-00-00', NULL),
(1564, 101, 'Golestan', 'GLS', 1, '0000-00-00', '0000-00-00', NULL),
(1565, 101, 'North Khorasan', 'NKH', 1, '0000-00-00', '0000-00-00', NULL),
(1566, 101, 'Razavi Khorasan', 'RKH', 1, '0000-00-00', '0000-00-00', NULL),
(1567, 101, 'South Khorasan', 'SKH', 1, '0000-00-00', '0000-00-00', NULL),
(1568, 102, 'Baghdad', 'BD', 1, '0000-00-00', '0000-00-00', NULL),
(1569, 102, 'Salah ad Din', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(1570, 102, 'Diyala', 'DY', 1, '0000-00-00', '0000-00-00', NULL),
(1571, 102, 'Wasit', 'WS', 1, '0000-00-00', '0000-00-00', NULL),
(1572, 102, 'Maysan', 'MY', 1, '0000-00-00', '0000-00-00', NULL),
(1573, 102, 'Al Basrah', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1574, 102, 'Dhi Qar', 'DQ', 1, '0000-00-00', '0000-00-00', NULL),
(1575, 102, 'Al Muthanna', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(1576, 102, 'Al Qadisyah', 'QA', 1, '0000-00-00', '0000-00-00', NULL),
(1577, 102, 'Babil', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(1578, 102, 'Al Karbala', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(1579, 102, 'An Najaf', 'NJ', 1, '0000-00-00', '0000-00-00', NULL),
(1580, 102, 'Al Anbar', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(1581, 102, 'Ninawa', 'NN', 1, '0000-00-00', '0000-00-00', NULL),
(1582, 102, 'Dahuk', 'DH', 1, '0000-00-00', '0000-00-00', NULL),
(1583, 102, 'Arbil', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1584, 102, 'At Ta\'mim', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(1585, 102, 'As Sulaymaniyah', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(1586, 103, 'Carlow', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(1587, 103, 'Cavan', 'CV', 1, '0000-00-00', '0000-00-00', NULL),
(1588, 103, 'Clare', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(1589, 103, 'Cork', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(1590, 103, 'Donegal', 'DO', 1, '0000-00-00', '0000-00-00', NULL),
(1591, 103, 'Dublin', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(1592, 103, 'Galway', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(1593, 103, 'Kerry', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(1594, 103, 'Kildare', 'KI', 1, '0000-00-00', '0000-00-00', NULL),
(1595, 103, 'Kilkenny', 'KL', 1, '0000-00-00', '0000-00-00', NULL),
(1596, 103, 'Laois', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(1597, 103, 'Leitrim', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(1598, 103, 'Limerick', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(1599, 103, 'Longford', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(1600, 103, 'Louth', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(1601, 103, 'Mayo', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1602, 103, 'Meath', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(1603, 103, 'Monaghan', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(1604, 103, 'Offaly', 'OF', 1, '0000-00-00', '0000-00-00', NULL),
(1605, 103, 'Roscommon', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(1606, 103, 'Sligo', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(1607, 103, 'Tipperary', 'TI', 1, '0000-00-00', '0000-00-00', NULL),
(1608, 103, 'Waterford', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(1609, 103, 'Westmeath', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(1610, 103, 'Wexford', 'WX', 1, '0000-00-00', '0000-00-00', NULL),
(1611, 103, 'Wicklow', 'WI', 1, '0000-00-00', '0000-00-00', NULL),
(1612, 104, 'Be\'er Sheva', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(1614, 104, 'Eilat and Arava', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(1615, 104, 'Galil', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(1616, 104, 'Haifa', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(1617, 104, 'Jehuda Mountains', 'JM', 1, '0000-00-00', '0000-00-00', NULL),
(1618, 104, 'Jerusalem', 'JE', 1, '0000-00-00', '0000-00-00', NULL),
(1619, 104, 'Negev', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(1620, 104, 'Semaria', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(1621, 104, 'Sharon', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3860, 105, 'Caltanissetta', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(3842, 105, 'Agrigento', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(3843, 105, 'Alessandria', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(3844, 105, 'Ancona', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(3845, 105, 'Aosta', 'AO', 1, '0000-00-00', '0000-00-00', NULL),
(3846, 105, 'Arezzo', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3847, 105, 'Ascoli Piceno', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(3848, 105, 'Asti', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(3849, 105, 'Avellino', 'AV', 1, '0000-00-00', '0000-00-00', NULL),
(3850, 105, 'Bari', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(3851, 105, 'Belluno', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(3852, 105, 'Benevento', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(3853, 105, 'Bergamo', 'BG', 1, '0000-00-00', '0000-00-00', NULL),
(3854, 105, 'Biella', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(3855, 105, 'Bologna', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(3856, 105, 'Bolzano', 'BZ', 1, '0000-00-00', '0000-00-00', NULL),
(3857, 105, 'Brescia', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(3858, 105, 'Brindisi', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(3859, 105, 'Cagliari', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(1643, 106, 'Clarendon Parish', 'CLA', 1, '0000-00-00', '0000-00-00', NULL),
(1644, 106, 'Hanover Parish', 'HAN', 1, '0000-00-00', '0000-00-00', NULL),
(1645, 106, 'Kingston Parish', 'KIN', 1, '0000-00-00', '0000-00-00', NULL),
(1646, 106, 'Manchester Parish', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(1647, 106, 'Portland Parish', 'POR', 1, '0000-00-00', '0000-00-00', NULL),
(1648, 106, 'Saint Andrew Parish', 'AND', 1, '0000-00-00', '0000-00-00', NULL),
(1649, 106, 'Saint Ann Parish', 'ANN', 1, '0000-00-00', '0000-00-00', NULL),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1, '0000-00-00', '0000-00-00', NULL),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1, '0000-00-00', '0000-00-00', NULL),
(1652, 106, 'Saint James Parish', 'JAM', 1, '0000-00-00', '0000-00-00', NULL),
(1653, 106, 'Saint Mary Parish', 'MAR', 1, '0000-00-00', '0000-00-00', NULL),
(1654, 106, 'Saint Thomas Parish', 'THO', 1, '0000-00-00', '0000-00-00', NULL),
(1655, 106, 'Trelawny Parish', 'TRL', 1, '0000-00-00', '0000-00-00', NULL),
(1656, 106, 'Westmoreland Parish', 'WML', 1, '0000-00-00', '0000-00-00', NULL),
(1657, 107, 'Aichi', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(1658, 107, 'Akita', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(1659, 107, 'Aomori', 'AO', 1, '0000-00-00', '0000-00-00', NULL),
(1660, 107, 'Chiba', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1661, 107, 'Ehime', 'EH', 1, '0000-00-00', '0000-00-00', NULL),
(1662, 107, 'Fukui', 'FK', 1, '0000-00-00', '0000-00-00', NULL),
(1663, 107, 'Fukuoka', 'FU', 1, '0000-00-00', '0000-00-00', NULL),
(1664, 107, 'Fukushima', 'FS', 1, '0000-00-00', '0000-00-00', NULL),
(1665, 107, 'Gifu', 'GI', 1, '0000-00-00', '0000-00-00', NULL),
(1666, 107, 'Gumma', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(1667, 107, 'Hiroshima', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(1668, 107, 'Hokkaido', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(1669, 107, 'Hyogo', 'HY', 1, '0000-00-00', '0000-00-00', NULL),
(1670, 107, 'Ibaraki', 'IB', 1, '0000-00-00', '0000-00-00', NULL),
(1671, 107, 'Ishikawa', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(1672, 107, 'Iwate', 'IW', 1, '0000-00-00', '0000-00-00', NULL),
(1673, 107, 'Kagawa', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1674, 107, 'Kagoshima', 'KG', 1, '0000-00-00', '0000-00-00', NULL),
(1675, 107, 'Kanagawa', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(1676, 107, 'Kochi', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(1677, 107, 'Kumamoto', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(1678, 107, 'Kyoto', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(1679, 107, 'Mie', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(1680, 107, 'Miyagi', 'MY', 1, '0000-00-00', '0000-00-00', NULL),
(1681, 107, 'Miyazaki', 'MZ', 1, '0000-00-00', '0000-00-00', NULL),
(1682, 107, 'Nagano', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(1683, 107, 'Nagasaki', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(1684, 107, 'Nara', 'NR', 1, '0000-00-00', '0000-00-00', NULL),
(1685, 107, 'Niigata', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(1686, 107, 'Oita', 'OI', 1, '0000-00-00', '0000-00-00', NULL),
(1687, 107, 'Okayama', 'OK', 1, '0000-00-00', '0000-00-00', NULL),
(1688, 107, 'Okinawa', 'ON', 1, '0000-00-00', '0000-00-00', NULL),
(1689, 107, 'Osaka', 'OS', 1, '0000-00-00', '0000-00-00', NULL),
(1690, 107, 'Saga', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(1691, 107, 'Saitama', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(1692, 107, 'Shiga', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(1693, 107, 'Shimane', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(1694, 107, 'Shizuoka', 'SZ', 1, '0000-00-00', '0000-00-00', NULL),
(1695, 107, 'Tochigi', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(1696, 107, 'Tokushima', 'TS', 1, '0000-00-00', '0000-00-00', NULL),
(1697, 107, 'Tokyo', 'TK', 1, '0000-00-00', '0000-00-00', NULL),
(1698, 107, 'Tottori', 'TT', 1, '0000-00-00', '0000-00-00', NULL),
(1699, 107, 'Toyama', 'TY', 1, '0000-00-00', '0000-00-00', NULL),
(1700, 107, 'Wakayama', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(1701, 107, 'Yamagata', 'YA', 1, '0000-00-00', '0000-00-00', NULL),
(1702, 107, 'Yamaguchi', 'YM', 1, '0000-00-00', '0000-00-00', NULL),
(1703, 107, 'Yamanashi', 'YN', 1, '0000-00-00', '0000-00-00', NULL),
(1704, 108, '\'Amman', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(1705, 108, 'Ajlun', 'AJ', 1, '0000-00-00', '0000-00-00', NULL),
(1706, 108, 'Al \'Aqabah', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(1707, 108, 'Al Balqa\'', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(1708, 108, 'Al Karak', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(1709, 108, 'Al Mafraq', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1710, 108, 'At Tafilah', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(1711, 108, 'Az Zarqa\'', 'AZ', 1, '0000-00-00', '0000-00-00', NULL),
(1712, 108, 'Irbid', 'IR', 1, '0000-00-00', '0000-00-00', NULL),
(1713, 108, 'Jarash', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1714, 108, 'Ma\'an', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1715, 108, 'Madaba', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(1716, 109, 'Almaty', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1717, 109, 'Almaty City', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(1718, 109, 'Aqmola', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(1719, 109, 'Aqtobe', 'AQ', 1, '0000-00-00', '0000-00-00', NULL),
(1720, 109, 'Astana City', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(1721, 109, 'Atyrau', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(1722, 109, 'Batys Qazaqstan', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1723, 109, 'Bayqongyr City', 'BY', 1, '0000-00-00', '0000-00-00', NULL),
(1724, 109, 'Mangghystau', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1, '0000-00-00', '0000-00-00', NULL),
(1726, 109, 'Pavlodar', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(1727, 109, 'Qaraghandy', 'QA', 1, '0000-00-00', '0000-00-00', NULL),
(1728, 109, 'Qostanay', 'QO', 1, '0000-00-00', '0000-00-00', NULL),
(1729, 109, 'Qyzylorda', 'QY', 1, '0000-00-00', '0000-00-00', NULL),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(1732, 109, 'Zhambyl', 'ZH', 1, '0000-00-00', '0000-00-00', NULL),
(1733, 110, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(1734, 110, 'Coast', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(1735, 110, 'Eastern', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(1736, 110, 'Nairobi Area', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(1737, 110, 'North Eastern', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(1738, 110, 'Nyanza', 'NY', 1, '0000-00-00', '0000-00-00', NULL),
(1739, 110, 'Rift Valley', 'RV', 1, '0000-00-00', '0000-00-00', NULL),
(1740, 110, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(1741, 111, 'Abaiang', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(1742, 111, 'Abemama', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(1743, 111, 'Aranuka', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(1744, 111, 'Arorae', 'AO', 1, '0000-00-00', '0000-00-00', NULL),
(1745, 111, 'Banaba', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1746, 111, 'Beru', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(1747, 111, 'Butaritari', 'bT', 1, '0000-00-00', '0000-00-00', NULL),
(1748, 111, 'Kanton', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1749, 111, 'Kiritimati', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(1750, 111, 'Kuria', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(1751, 111, 'Maiana', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(1752, 111, 'Makin', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(1753, 111, 'Marakei', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(1754, 111, 'Nikunau', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(1755, 111, 'Nonouti', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(1756, 111, 'Onotoa', 'ON', 1, '0000-00-00', '0000-00-00', NULL),
(1757, 111, 'Tabiteuea', 'TT', 1, '0000-00-00', '0000-00-00', NULL),
(1758, 111, 'Tabuaeran', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(1759, 111, 'Tamana', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(1760, 111, 'Tarawa', 'TW', 1, '0000-00-00', '0000-00-00', NULL),
(1761, 111, 'Teraina', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(1762, 112, 'Chagang-do', 'CHA', 1, '0000-00-00', '0000-00-00', NULL),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1, '0000-00-00', '0000-00-00', NULL),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1, '0000-00-00', '0000-00-00', NULL),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1, '0000-00-00', '0000-00-00', NULL),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1, '0000-00-00', '0000-00-00', NULL),
(1767, 112, 'Kangwon-do', 'KAN', 1, '0000-00-00', '0000-00-00', NULL),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1, '0000-00-00', '0000-00-00', NULL),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1, '0000-00-00', '0000-00-00', NULL),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1, '0000-00-00', '0000-00-00', NULL),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1, '0000-00-00', '0000-00-00', NULL),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1, '0000-00-00', '0000-00-00', NULL),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1775, 113, 'Cheju-do', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(1776, 113, 'Cholla-bukto', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(1777, 113, 'Cholla-namdo', 'CN', 1, '0000-00-00', '0000-00-00', NULL),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1, '0000-00-00', '0000-00-00', NULL),
(1779, 113, 'Kangwon-do', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1, '0000-00-00', '0000-00-00', NULL),
(1781, 113, 'Kyonggi-do', 'KD', 1, '0000-00-00', '0000-00-00', NULL),
(1782, 113, 'Kyongsang-bukto', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(1783, 113, 'Kyongsang-namdo', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1, '0000-00-00', '0000-00-00', NULL),
(1788, 114, 'Al \'Asimah', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1789, 114, 'Al Ahmadi', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(1790, 114, 'Al Farwaniyah', 'AF', 1, '0000-00-00', '0000-00-00', NULL),
(1791, 114, 'Al Jahra\'', 'AJ', 1, '0000-00-00', '0000-00-00', NULL),
(1792, 114, 'Hawalli', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(1793, 115, 'Bishkek', 'GB', 1, '0000-00-00', '0000-00-00', NULL),
(1794, 115, 'Batken', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(1795, 115, 'Chu', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(1796, 115, 'Jalal-Abad', 'J', 1, '0000-00-00', '0000-00-00', NULL),
(1797, 115, 'Naryn', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(1798, 115, 'Osh', 'O', 1, '0000-00-00', '0000-00-00', NULL),
(1799, 115, 'Talas', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(1800, 115, 'Ysyk-Kol', 'Y', 1, '0000-00-00', '0000-00-00', NULL),
(1801, 116, 'Vientiane', 'VT', 1, '0000-00-00', '0000-00-00', NULL),
(1802, 116, 'Attapu', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(1803, 116, 'Bokeo', 'BK', 1, '0000-00-00', '0000-00-00', NULL),
(1804, 116, 'Bolikhamxai', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(1805, 116, 'Champasak', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(1806, 116, 'Houaphan', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(1807, 116, 'Khammouan', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(1808, 116, 'Louang Namtha', 'LM', 1, '0000-00-00', '0000-00-00', NULL),
(1809, 116, 'Louangphabang', 'LP', 1, '0000-00-00', '0000-00-00', NULL),
(1810, 116, 'Oudomxai', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(1811, 116, 'Phongsali', 'PH', 1, '0000-00-00', '0000-00-00', NULL),
(1812, 116, 'Salavan', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(1813, 116, 'Savannakhet', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(1814, 116, 'Vientiane', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(1815, 116, 'Xaignabouli', 'XA', 1, '0000-00-00', '0000-00-00', NULL),
(1816, 116, 'Xekong', 'XE', 1, '0000-00-00', '0000-00-00', NULL),
(1817, 116, 'Xiangkhoang', 'XI', 1, '0000-00-00', '0000-00-00', NULL),
(1818, 116, 'Xaisomboun', 'XN', 1, '0000-00-00', '0000-00-00', NULL),
(1852, 119, 'Berea', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(1853, 119, 'Butha-Buthe', 'BB', 1, '0000-00-00', '0000-00-00', NULL),
(1854, 119, 'Leribe', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(1855, 119, 'Mafeteng', 'MF', 1, '0000-00-00', '0000-00-00', NULL),
(1856, 119, 'Maseru', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1, '0000-00-00', '0000-00-00', NULL),
(1858, 119, 'Mokhotlong', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(1859, 119, 'Qacha\'s Nek', 'QN', 1, '0000-00-00', '0000-00-00', NULL),
(1860, 119, 'Quthing', 'QT', 1, '0000-00-00', '0000-00-00', NULL),
(1861, 119, 'Thaba-Tseka', 'TT', 1, '0000-00-00', '0000-00-00', NULL),
(1862, 120, 'Bomi', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(1863, 120, 'Bong', 'BG', 1, '0000-00-00', '0000-00-00', NULL),
(1864, 120, 'Grand Bassa', 'GB', 1, '0000-00-00', '0000-00-00', NULL),
(1865, 120, 'Grand Cape Mount', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(1866, 120, 'Grand Gedeh', 'GG', 1, '0000-00-00', '0000-00-00', NULL),
(1867, 120, 'Grand Kru', 'GK', 1, '0000-00-00', '0000-00-00', NULL),
(1868, 120, 'Lofa', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(1869, 120, 'Margibi', 'MG', 1, '0000-00-00', '0000-00-00', NULL),
(1870, 120, 'Maryland', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(1871, 120, 'Montserrado', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(1872, 120, 'Nimba', 'NB', 1, '0000-00-00', '0000-00-00', NULL),
(1873, 120, 'River Cess', 'RC', 1, '0000-00-00', '0000-00-00', NULL),
(1874, 120, 'Sinoe', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(1875, 121, 'Ajdabiya', 'AJ', 1, '0000-00-00', '0000-00-00', NULL),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1, '0000-00-00', '0000-00-00', NULL),
(1877, 121, 'Al Fatih', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(1879, 121, 'Al Jufrah', 'JU', 1, '0000-00-00', '0000-00-00', NULL),
(1880, 121, 'Al Khums', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(1881, 121, 'Al Kufrah', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(1882, 121, 'An Nuqat al Khams', 'NK', 1, '0000-00-00', '0000-00-00', NULL),
(1883, 121, 'Ash Shati\'', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(1884, 121, 'Awbari', 'AW', 1, '0000-00-00', '0000-00-00', NULL),
(1885, 121, 'Az Zawiyah', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(1886, 121, 'Banghazi', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(1887, 121, 'Darnah', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(1888, 121, 'Ghadamis', 'GD', 1, '0000-00-00', '0000-00-00', NULL),
(1889, 121, 'Gharyan', 'GY', 1, '0000-00-00', '0000-00-00', NULL),
(1890, 121, 'Misratah', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(1891, 121, 'Murzuq', 'MZ', 1, '0000-00-00', '0000-00-00', NULL),
(1892, 121, 'Sabha', 'SB', 1, '0000-00-00', '0000-00-00', NULL),
(1893, 121, 'Sawfajjin', 'SW', 1, '0000-00-00', '0000-00-00', NULL),
(1894, 121, 'Surt', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1, '0000-00-00', '0000-00-00', NULL),
(1896, 121, 'Tarhunah', 'TH', 1, '0000-00-00', '0000-00-00', NULL),
(1897, 121, 'Tubruq', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(1898, 121, 'Yafran', 'YA', 1, '0000-00-00', '0000-00-00', NULL),
(1899, 121, 'Zlitan', 'ZL', 1, '0000-00-00', '0000-00-00', NULL),
(1900, 122, 'Vaduz', 'V', 1, '0000-00-00', '0000-00-00', NULL),
(1901, 122, 'Schaan', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(1902, 122, 'Balzers', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(1903, 122, 'Triesen', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(1904, 122, 'Eschen', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(1905, 122, 'Mauren', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(1906, 122, 'Triesenberg', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(1907, 122, 'Ruggell', 'R', 1, '0000-00-00', '0000-00-00', NULL),
(1908, 122, 'Gamprin', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(1909, 122, 'Schellenberg', 'L', 1, '0000-00-00', '0000-00-00', NULL),
(1910, 122, 'Planken', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(1911, 123, 'Alytus', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(1912, 123, 'Kaunas', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(1913, 123, 'Klaipeda', 'KL', 1, '0000-00-00', '0000-00-00', NULL),
(1914, 123, 'Marijampole', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(1915, 123, 'Panevezys', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(1916, 123, 'Siauliai', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(1917, 123, 'Taurage', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(1918, 123, 'Telsiai', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(1919, 123, 'Utena', 'UT', 1, '0000-00-00', '0000-00-00', NULL),
(1920, 123, 'Vilnius', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(1921, 124, 'Diekirch', 'DD', 1, '0000-00-00', '0000-00-00', NULL),
(1922, 124, 'Clervaux', 'DC', 1, '0000-00-00', '0000-00-00', NULL),
(1923, 124, 'Redange', 'DR', 1, '0000-00-00', '0000-00-00', NULL),
(1924, 124, 'Vianden', 'DV', 1, '0000-00-00', '0000-00-00', NULL),
(1925, 124, 'Wiltz', 'DW', 1, '0000-00-00', '0000-00-00', NULL),
(1926, 124, 'Grevenmacher', 'GG', 1, '0000-00-00', '0000-00-00', NULL),
(1927, 124, 'Echternach', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(1928, 124, 'Remich', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(1929, 124, 'Luxembourg', 'LL', 1, '0000-00-00', '0000-00-00', NULL),
(1930, 124, 'Capellen', 'LC', 1, '0000-00-00', '0000-00-00', NULL),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(1932, 124, 'Mersch', 'LM', 1, '0000-00-00', '0000-00-00', NULL),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1, '0000-00-00', '0000-00-00', NULL),
(1934, 125, 'St. Anthony Parish', 'ANT', 1, '0000-00-00', '0000-00-00', NULL),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1, '0000-00-00', '0000-00-00', NULL),
(1936, 125, 'Cathedral Parish', 'CAT', 1, '0000-00-00', '0000-00-00', NULL),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1, '0000-00-00', '0000-00-00', NULL),
(1938, 127, 'Antananarivo', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(1939, 127, 'Antsiranana', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(1940, 127, 'Fianarantsoa', 'FN', 1, '0000-00-00', '0000-00-00', NULL),
(1941, 127, 'Mahajanga', 'MJ', 1, '0000-00-00', '0000-00-00', NULL),
(1942, 127, 'Toamasina', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(1943, 127, 'Toliara', 'TL', 1, '0000-00-00', '0000-00-00', NULL),
(1944, 128, 'Balaka', 'BLK', 1, '0000-00-00', '0000-00-00', NULL),
(1945, 128, 'Blantyre', 'BLT', 1, '0000-00-00', '0000-00-00', NULL),
(1946, 128, 'Chikwawa', 'CKW', 1, '0000-00-00', '0000-00-00', NULL),
(1947, 128, 'Chiradzulu', 'CRD', 1, '0000-00-00', '0000-00-00', NULL),
(1948, 128, 'Chitipa', 'CTP', 1, '0000-00-00', '0000-00-00', NULL),
(1949, 128, 'Dedza', 'DDZ', 1, '0000-00-00', '0000-00-00', NULL),
(1950, 128, 'Dowa', 'DWA', 1, '0000-00-00', '0000-00-00', NULL),
(1951, 128, 'Karonga', 'KRG', 1, '0000-00-00', '0000-00-00', NULL),
(1952, 128, 'Kasungu', 'KSG', 1, '0000-00-00', '0000-00-00', NULL),
(1953, 128, 'Likoma', 'LKM', 1, '0000-00-00', '0000-00-00', NULL),
(1954, 128, 'Lilongwe', 'LLG', 1, '0000-00-00', '0000-00-00', NULL),
(1955, 128, 'Machinga', 'MCG', 1, '0000-00-00', '0000-00-00', NULL),
(1956, 128, 'Mangochi', 'MGC', 1, '0000-00-00', '0000-00-00', NULL),
(1957, 128, 'Mchinji', 'MCH', 1, '0000-00-00', '0000-00-00', NULL),
(1958, 128, 'Mulanje', 'MLJ', 1, '0000-00-00', '0000-00-00', NULL),
(1959, 128, 'Mwanza', 'MWZ', 1, '0000-00-00', '0000-00-00', NULL),
(1960, 128, 'Mzimba', 'MZM', 1, '0000-00-00', '0000-00-00', NULL),
(1961, 128, 'Ntcheu', 'NTU', 1, '0000-00-00', '0000-00-00', NULL),
(1962, 128, 'Nkhata Bay', 'NKB', 1, '0000-00-00', '0000-00-00', NULL),
(1963, 128, 'Nkhotakota', 'NKH', 1, '0000-00-00', '0000-00-00', NULL),
(1964, 128, 'Nsanje', 'NSJ', 1, '0000-00-00', '0000-00-00', NULL),
(1965, 128, 'Ntchisi', 'NTI', 1, '0000-00-00', '0000-00-00', NULL),
(1966, 128, 'Phalombe', 'PHL', 1, '0000-00-00', '0000-00-00', NULL),
(1967, 128, 'Rumphi', 'RMP', 1, '0000-00-00', '0000-00-00', NULL),
(1968, 128, 'Salima', 'SLM', 1, '0000-00-00', '0000-00-00', NULL),
(1969, 128, 'Thyolo', 'THY', 1, '0000-00-00', '0000-00-00', NULL),
(1970, 128, 'Zomba', 'ZBA', 1, '0000-00-00', '0000-00-00', NULL),
(1971, 129, 'Johor', 'MY-01', 1, '0000-00-00', '0000-00-00', NULL),
(1972, 129, 'Kedah', 'MY-02', 1, '0000-00-00', '0000-00-00', NULL),
(1973, 129, 'Kelantan', 'MY-03', 1, '0000-00-00', '0000-00-00', NULL),
(1974, 129, 'Labuan', 'MY-15', 1, '0000-00-00', '0000-00-00', NULL),
(1975, 129, 'Melaka', 'MY-04', 1, '0000-00-00', '0000-00-00', NULL),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1, '0000-00-00', '0000-00-00', NULL),
(1977, 129, 'Pahang', 'MY-06', 1, '0000-00-00', '0000-00-00', NULL),
(1978, 129, 'Perak', 'MY-08', 1, '0000-00-00', '0000-00-00', NULL),
(1979, 129, 'Perlis', 'MY-09', 1, '0000-00-00', '0000-00-00', NULL),
(1980, 129, 'Pulau Pinang', 'MY-07', 1, '0000-00-00', '0000-00-00', NULL),
(1981, 129, 'Sabah', 'MY-12', 1, '0000-00-00', '0000-00-00', NULL),
(1982, 129, 'Sarawak', 'MY-13', 1, '0000-00-00', '0000-00-00', NULL),
(1983, 129, 'Selangor', 'MY-10', 1, '0000-00-00', '0000-00-00', NULL),
(1984, 129, 'Terengganu', 'MY-11', 1, '0000-00-00', '0000-00-00', NULL),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1, '0000-00-00', '0000-00-00', NULL),
(4035, 129, 'Putrajaya', 'MY-16', 1, '0000-00-00', '0000-00-00', NULL),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1, '0000-00-00', '0000-00-00', NULL),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1, '0000-00-00', '0000-00-00', NULL),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1, '0000-00-00', '0000-00-00', NULL),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1, '0000-00-00', '0000-00-00', NULL),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1, '0000-00-00', '0000-00-00', NULL),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1, '0000-00-00', '0000-00-00', NULL),
(1992, 130, 'Faadhippolhu', 'FAA', 1, '0000-00-00', '0000-00-00', NULL),
(1993, 130, 'Male Atoll', 'MAA', 1, '0000-00-00', '0000-00-00', NULL),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1, '0000-00-00', '0000-00-00', NULL),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1, '0000-00-00', '0000-00-00', NULL),
(1996, 130, 'Felidhe Atoll', 'FEA', 1, '0000-00-00', '0000-00-00', NULL),
(1997, 130, 'Mulaku Atoll', 'MUA', 1, '0000-00-00', '0000-00-00', NULL),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1, '0000-00-00', '0000-00-00', NULL),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1, '0000-00-00', '0000-00-00', NULL),
(2000, 130, 'Kolhumadulu', 'KLH', 1, '0000-00-00', '0000-00-00', NULL),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1, '0000-00-00', '0000-00-00', NULL),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1, '0000-00-00', '0000-00-00', NULL),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1, '0000-00-00', '0000-00-00', NULL),
(2004, 130, 'Fua Mulaku', 'FMU', 1, '0000-00-00', '0000-00-00', NULL),
(2005, 130, 'Addu', 'ADD', 1, '0000-00-00', '0000-00-00', NULL),
(2006, 131, 'Gao', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2007, 131, 'Kayes', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(2008, 131, 'Kidal', 'KD', 1, '0000-00-00', '0000-00-00', NULL),
(2009, 131, 'Koulikoro', 'KL', 1, '0000-00-00', '0000-00-00', NULL),
(2010, 131, 'Mopti', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(2011, 131, 'Segou', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(2012, 131, 'Sikasso', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(2013, 131, 'Tombouctou', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(2014, 131, 'Bamako Capital District', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(2015, 132, 'Attard', 'ATT', 1, '0000-00-00', '0000-00-00', NULL),
(2016, 132, 'Balzan', 'BAL', 1, '0000-00-00', '0000-00-00', NULL),
(2017, 132, 'Birgu', 'BGU', 1, '0000-00-00', '0000-00-00', NULL),
(2018, 132, 'Birkirkara', 'BKK', 1, '0000-00-00', '0000-00-00', NULL),
(2019, 132, 'Birzebbuga', 'BRZ', 1, '0000-00-00', '0000-00-00', NULL),
(2020, 132, 'Bormla', 'BOR', 1, '0000-00-00', '0000-00-00', NULL),
(2021, 132, 'Dingli', 'DIN', 1, '0000-00-00', '0000-00-00', NULL),
(2022, 132, 'Fgura', 'FGU', 1, '0000-00-00', '0000-00-00', NULL),
(2023, 132, 'Floriana', 'FLO', 1, '0000-00-00', '0000-00-00', NULL),
(2024, 132, 'Gudja', 'GDJ', 1, '0000-00-00', '0000-00-00', NULL),
(2025, 132, 'Gzira', 'GZR', 1, '0000-00-00', '0000-00-00', NULL),
(2026, 132, 'Gargur', 'GRG', 1, '0000-00-00', '0000-00-00', NULL),
(2027, 132, 'Gaxaq', 'GXQ', 1, '0000-00-00', '0000-00-00', NULL),
(2028, 132, 'Hamrun', 'HMR', 1, '0000-00-00', '0000-00-00', NULL),
(2029, 132, 'Iklin', 'IKL', 1, '0000-00-00', '0000-00-00', NULL),
(2030, 132, 'Isla', 'ISL', 1, '0000-00-00', '0000-00-00', NULL),
(2031, 132, 'Kalkara', 'KLK', 1, '0000-00-00', '0000-00-00', NULL),
(2032, 132, 'Kirkop', 'KRK', 1, '0000-00-00', '0000-00-00', NULL),
(2033, 132, 'Lija', 'LIJ', 1, '0000-00-00', '0000-00-00', NULL),
(2034, 132, 'Luqa', 'LUQ', 1, '0000-00-00', '0000-00-00', NULL),
(2035, 132, 'Marsa', 'MRS', 1, '0000-00-00', '0000-00-00', NULL),
(2036, 132, 'Marsaskala', 'MKL', 1, '0000-00-00', '0000-00-00', NULL),
(2037, 132, 'Marsaxlokk', 'MXL', 1, '0000-00-00', '0000-00-00', NULL),
(2038, 132, 'Mdina', 'MDN', 1, '0000-00-00', '0000-00-00', NULL),
(2039, 132, 'Melliea', 'MEL', 1, '0000-00-00', '0000-00-00', NULL),
(2040, 132, 'Mgarr', 'MGR', 1, '0000-00-00', '0000-00-00', NULL),
(2041, 132, 'Mosta', 'MST', 1, '0000-00-00', '0000-00-00', NULL),
(2042, 132, 'Mqabba', 'MQA', 1, '0000-00-00', '0000-00-00', NULL),
(2043, 132, 'Msida', 'MSI', 1, '0000-00-00', '0000-00-00', NULL),
(2044, 132, 'Mtarfa', 'MTF', 1, '0000-00-00', '0000-00-00', NULL),
(2045, 132, 'Naxxar', 'NAX', 1, '0000-00-00', '0000-00-00', NULL),
(2046, 132, 'Paola', 'PAO', 1, '0000-00-00', '0000-00-00', NULL),
(2047, 132, 'Pembroke', 'PEM', 1, '0000-00-00', '0000-00-00', NULL),
(2048, 132, 'Pieta', 'PIE', 1, '0000-00-00', '0000-00-00', NULL),
(2049, 132, 'Qormi', 'QOR', 1, '0000-00-00', '0000-00-00', NULL),
(2050, 132, 'Qrendi', 'QRE', 1, '0000-00-00', '0000-00-00', NULL),
(2051, 132, 'Rabat', 'RAB', 1, '0000-00-00', '0000-00-00', NULL),
(2052, 132, 'Safi', 'SAF', 1, '0000-00-00', '0000-00-00', NULL),
(2053, 132, 'San Giljan', 'SGI', 1, '0000-00-00', '0000-00-00', NULL),
(2054, 132, 'Santa Lucija', 'SLU', 1, '0000-00-00', '0000-00-00', NULL),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1, '0000-00-00', '0000-00-00', NULL),
(2056, 132, 'San Gwann', 'SGW', 1, '0000-00-00', '0000-00-00', NULL),
(2057, 132, 'Santa Venera', 'SVE', 1, '0000-00-00', '0000-00-00', NULL),
(2058, 132, 'Siggiewi', 'SIG', 1, '0000-00-00', '0000-00-00', NULL),
(2059, 132, 'Sliema', 'SLM', 1, '0000-00-00', '0000-00-00', NULL),
(2060, 132, 'Swieqi', 'SWQ', 1, '0000-00-00', '0000-00-00', NULL),
(2061, 132, 'Ta Xbiex', 'TXB', 1, '0000-00-00', '0000-00-00', NULL),
(2062, 132, 'Tarxien', 'TRX', 1, '0000-00-00', '0000-00-00', NULL),
(2063, 132, 'Valletta', 'VLT', 1, '0000-00-00', '0000-00-00', NULL),
(2064, 132, 'Xgajra', 'XGJ', 1, '0000-00-00', '0000-00-00', NULL),
(2065, 132, 'Zabbar', 'ZBR', 1, '0000-00-00', '0000-00-00', NULL),
(2066, 132, 'Zebbug', 'ZBG', 1, '0000-00-00', '0000-00-00', NULL),
(2067, 132, 'Zejtun', 'ZJT', 1, '0000-00-00', '0000-00-00', NULL),
(2068, 132, 'Zurrieq', 'ZRQ', 1, '0000-00-00', '0000-00-00', NULL),
(2069, 132, 'Fontana', 'FNT', 1, '0000-00-00', '0000-00-00', NULL),
(2070, 132, 'Ghajnsielem', 'GHJ', 1, '0000-00-00', '0000-00-00', NULL),
(2071, 132, 'Gharb', 'GHR', 1, '0000-00-00', '0000-00-00', NULL),
(2072, 132, 'Ghasri', 'GHS', 1, '0000-00-00', '0000-00-00', NULL),
(2073, 132, 'Kercem', 'KRC', 1, '0000-00-00', '0000-00-00', NULL),
(2074, 132, 'Munxar', 'MUN', 1, '0000-00-00', '0000-00-00', NULL),
(2075, 132, 'Nadur', 'NAD', 1, '0000-00-00', '0000-00-00', NULL),
(2076, 132, 'Qala', 'QAL', 1, '0000-00-00', '0000-00-00', NULL),
(2077, 132, 'Victoria', 'VIC', 1, '0000-00-00', '0000-00-00', NULL),
(2078, 132, 'San Lawrenz', 'SLA', 1, '0000-00-00', '0000-00-00', NULL),
(2079, 132, 'Sannat', 'SNT', 1, '0000-00-00', '0000-00-00', NULL),
(2080, 132, 'Xagra', 'ZAG', 1, '0000-00-00', '0000-00-00', NULL),
(2081, 132, 'Xewkija', 'XEW', 1, '0000-00-00', '0000-00-00', NULL),
(2082, 132, 'Zebbug', 'ZEB', 1, '0000-00-00', '0000-00-00', NULL),
(2083, 133, 'Ailinginae', 'ALG', 1, '0000-00-00', '0000-00-00', NULL),
(2084, 133, 'Ailinglaplap', 'ALL', 1, '0000-00-00', '0000-00-00', NULL),
(2085, 133, 'Ailuk', 'ALK', 1, '0000-00-00', '0000-00-00', NULL),
(2086, 133, 'Arno', 'ARN', 1, '0000-00-00', '0000-00-00', NULL),
(2087, 133, 'Aur', 'AUR', 1, '0000-00-00', '0000-00-00', NULL),
(2088, 133, 'Bikar', 'BKR', 1, '0000-00-00', '0000-00-00', NULL),
(2089, 133, 'Bikini', 'BKN', 1, '0000-00-00', '0000-00-00', NULL),
(2090, 133, 'Bokak', 'BKK', 1, '0000-00-00', '0000-00-00', NULL),
(2091, 133, 'Ebon', 'EBN', 1, '0000-00-00', '0000-00-00', NULL),
(2092, 133, 'Enewetak', 'ENT', 1, '0000-00-00', '0000-00-00', NULL),
(2093, 133, 'Erikub', 'EKB', 1, '0000-00-00', '0000-00-00', NULL),
(2094, 133, 'Jabat', 'JBT', 1, '0000-00-00', '0000-00-00', NULL),
(2095, 133, 'Jaluit', 'JLT', 1, '0000-00-00', '0000-00-00', NULL),
(2096, 133, 'Jemo', 'JEM', 1, '0000-00-00', '0000-00-00', NULL),
(2097, 133, 'Kili', 'KIL', 1, '0000-00-00', '0000-00-00', NULL),
(2098, 133, 'Kwajalein', 'KWJ', 1, '0000-00-00', '0000-00-00', NULL),
(2099, 133, 'Lae', 'LAE', 1, '0000-00-00', '0000-00-00', NULL),
(2100, 133, 'Lib', 'LIB', 1, '0000-00-00', '0000-00-00', NULL),
(2101, 133, 'Likiep', 'LKP', 1, '0000-00-00', '0000-00-00', NULL),
(2102, 133, 'Majuro', 'MJR', 1, '0000-00-00', '0000-00-00', NULL),
(2103, 133, 'Maloelap', 'MLP', 1, '0000-00-00', '0000-00-00', NULL),
(2104, 133, 'Mejit', 'MJT', 1, '0000-00-00', '0000-00-00', NULL),
(2105, 133, 'Mili', 'MIL', 1, '0000-00-00', '0000-00-00', NULL),
(2106, 133, 'Namorik', 'NMK', 1, '0000-00-00', '0000-00-00', NULL),
(2107, 133, 'Namu', 'NAM', 1, '0000-00-00', '0000-00-00', NULL),
(2108, 133, 'Rongelap', 'RGL', 1, '0000-00-00', '0000-00-00', NULL),
(2109, 133, 'Rongrik', 'RGK', 1, '0000-00-00', '0000-00-00', NULL),
(2110, 133, 'Toke', 'TOK', 1, '0000-00-00', '0000-00-00', NULL),
(2111, 133, 'Ujae', 'UJA', 1, '0000-00-00', '0000-00-00', NULL),
(2112, 133, 'Ujelang', 'UJL', 1, '0000-00-00', '0000-00-00', NULL),
(2113, 133, 'Utirik', 'UTK', 1, '0000-00-00', '0000-00-00', NULL),
(2114, 133, 'Wotho', 'WTH', 1, '0000-00-00', '0000-00-00', NULL),
(2115, 133, 'Wotje', 'WTJ', 1, '0000-00-00', '0000-00-00', NULL),
(2116, 135, 'Adrar', 'AD', 1, '0000-00-00', '0000-00-00', NULL),
(2117, 135, 'Assaba', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2118, 135, 'Brakna', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1, '0000-00-00', '0000-00-00', NULL),
(2120, 135, 'Gorgol', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(2121, 135, 'Guidimaka', 'GM', 1, '0000-00-00', '0000-00-00', NULL),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1, '0000-00-00', '0000-00-00', NULL),
(2123, 135, 'Hodh El Gharbi', 'HG', 1, '0000-00-00', '0000-00-00', NULL),
(2124, 135, 'Inchiri', 'IN', 1, '0000-00-00', '0000-00-00', NULL),
(2125, 135, 'Tagant', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2126, 135, 'Tiris Zemmour', 'TZ', 1, '0000-00-00', '0000-00-00', NULL),
(2127, 135, 'Trarza', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(2128, 135, 'Nouakchott', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2130, 136, 'Curepipe', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(2131, 136, 'Port Louis', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(2132, 136, 'Quatre Bornes', 'QB', 1, '0000-00-00', '0000-00-00', NULL),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1, '0000-00-00', '0000-00-00', NULL),
(2134, 136, 'Agalega Islands', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(2136, 136, 'Rodrigues', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(2137, 136, 'Black River', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(2138, 136, 'Flacq', 'FL', 1, '0000-00-00', '0000-00-00', NULL),
(2139, 136, 'Grand Port', 'GP', 1, '0000-00-00', '0000-00-00', NULL),
(2140, 136, 'Moka', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(2141, 136, 'Pamplemousses', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2142, 136, 'Plaines Wilhems', 'PW', 1, '0000-00-00', '0000-00-00', NULL),
(2143, 136, 'Port Louis', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(2144, 136, 'Riviere du Rempart', 'RR', 1, '0000-00-00', '0000-00-00', NULL),
(2145, 136, 'Savanne', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2146, 138, 'Baja California Norte', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(2147, 138, 'Baja California Sur', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(2148, 138, 'Campeche', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2149, 138, 'Chiapas', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(2150, 138, 'Chihuahua', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1, '0000-00-00', '0000-00-00', NULL),
(2152, 138, 'Colima', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2153, 138, 'Distrito Federal', 'DF', 1, '0000-00-00', '0000-00-00', NULL),
(2154, 138, 'Durango', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(2155, 138, 'Guanajuato', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2156, 138, 'Guerrero', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(2157, 138, 'Hidalgo', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(2158, 138, 'Jalisco', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(2159, 138, 'Mexico', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(2161, 138, 'Morelos', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(2162, 138, 'Nayarit', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2163, 138, 'Nuevo Leon', 'NL', 1, '0000-00-00', '0000-00-00', NULL),
(2164, 138, 'Oaxaca', 'OA', 1, '0000-00-00', '0000-00-00', NULL),
(2165, 138, 'Puebla', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1, '0000-00-00', '0000-00-00', NULL),
(2167, 138, 'Quintana Roo', 'QR', 1, '0000-00-00', '0000-00-00', NULL),
(2168, 138, 'San Luis Potosi', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2169, 138, 'Sinaloa', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(2170, 138, 'Sonora', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2171, 138, 'Tabasco', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(2172, 138, 'Tamaulipas', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(2173, 138, 'Tlaxcala', 'TL', 1, '0000-00-00', '0000-00-00', NULL),
(2174, 138, 'Veracruz-Llave', 'VE', 1, '0000-00-00', '0000-00-00', NULL),
(2175, 138, 'Yucatan', 'YU', 1, '0000-00-00', '0000-00-00', NULL),
(2176, 138, 'Zacatecas', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(2177, 139, 'Chuuk', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(2178, 139, 'Kosrae', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(2179, 139, 'Pohnpei', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2180, 139, 'Yap', 'Y', 1, '0000-00-00', '0000-00-00', NULL),
(2181, 140, 'Gagauzia', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2182, 140, 'Chisinau', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(2183, 140, 'Balti', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2184, 140, 'Cahul', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2185, 140, 'Edinet', 'ED', 1, '0000-00-00', '0000-00-00', NULL),
(2186, 140, 'Lapusna', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(2187, 140, 'Orhei', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(2188, 140, 'Soroca', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2189, 140, 'Tighina', 'TI', 1, '0000-00-00', '0000-00-00', NULL),
(2190, 140, 'Ungheni', 'UN', 1, '0000-00-00', '0000-00-00', NULL),
(2191, 140, 'St‚nga Nistrului', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(2192, 141, 'Fontvieille', 'FV', 1, '0000-00-00', '0000-00-00', NULL),
(2193, 141, 'La Condamine', 'LC', 1, '0000-00-00', '0000-00-00', NULL),
(2194, 141, 'Monaco-Ville', 'MV', 1, '0000-00-00', '0000-00-00', NULL),
(2195, 141, 'Monte-Carlo', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(2196, 142, 'Ulanbaatar', '1', 1, '0000-00-00', '0000-00-00', NULL),
(2197, 142, 'Orhon', '035', 1, '0000-00-00', '0000-00-00', NULL),
(2198, 142, 'Darhan uul', '037', 1, '0000-00-00', '0000-00-00', NULL),
(2199, 142, 'Hentiy', '039', 1, '0000-00-00', '0000-00-00', NULL),
(2200, 142, 'Hovsgol', '041', 1, '0000-00-00', '0000-00-00', NULL),
(2201, 142, 'Hovd', '043', 1, '0000-00-00', '0000-00-00', NULL),
(2202, 142, 'Uvs', '046', 1, '0000-00-00', '0000-00-00', NULL),
(2203, 142, 'Tov', '047', 1, '0000-00-00', '0000-00-00', NULL),
(2204, 142, 'Selenge', '049', 1, '0000-00-00', '0000-00-00', NULL),
(2205, 142, 'Suhbaatar', '051', 1, '0000-00-00', '0000-00-00', NULL),
(2206, 142, 'Omnogovi', '053', 1, '0000-00-00', '0000-00-00', NULL),
(2207, 142, 'Ovorhangay', '055', 1, '0000-00-00', '0000-00-00', NULL),
(2208, 142, 'Dzavhan', '057', 1, '0000-00-00', '0000-00-00', NULL),
(2209, 142, 'DundgovL', '059', 1, '0000-00-00', '0000-00-00', NULL),
(2210, 142, 'Dornod', '061', 1, '0000-00-00', '0000-00-00', NULL),
(2211, 142, 'Dornogov', '063', 1, '0000-00-00', '0000-00-00', NULL),
(2212, 142, 'Govi-Sumber', '064', 1, '0000-00-00', '0000-00-00', NULL),
(2213, 142, 'Govi-Altay', '065', 1, '0000-00-00', '0000-00-00', NULL),
(2214, 142, 'Bulgan', '067', 1, '0000-00-00', '0000-00-00', NULL),
(2215, 142, 'Bayanhongor', '069', 1, '0000-00-00', '0000-00-00', NULL),
(2216, 142, 'Bayan-Olgiy', '071', 1, '0000-00-00', '0000-00-00', NULL),
(2217, 142, 'Arhangay', '073', 1, '0000-00-00', '0000-00-00', NULL),
(2218, 143, 'Saint Anthony', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(2219, 143, 'Saint Georges', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(2220, 143, 'Saint Peter', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2221, 144, 'Agadir', 'AGD', 1, '0000-00-00', '0000-00-00', NULL),
(2222, 144, 'Al Hoceima', 'HOC', 1, '0000-00-00', '0000-00-00', NULL),
(2223, 144, 'Azilal', 'AZI', 1, '0000-00-00', '0000-00-00', NULL),
(2224, 144, 'Beni Mellal', 'BME', 1, '0000-00-00', '0000-00-00', NULL),
(2225, 144, 'Ben Slimane', 'BSL', 1, '0000-00-00', '0000-00-00', NULL),
(2226, 144, 'Boulemane', 'BLM', 1, '0000-00-00', '0000-00-00', NULL),
(2227, 144, 'Casablanca', 'CBL', 1, '0000-00-00', '0000-00-00', NULL),
(2228, 144, 'Chaouen', 'CHA', 1, '0000-00-00', '0000-00-00', NULL),
(2229, 144, 'El Jadida', 'EJA', 1, '0000-00-00', '0000-00-00', NULL),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1, '0000-00-00', '0000-00-00', NULL),
(2231, 144, 'Er Rachidia', 'ERA', 1, '0000-00-00', '0000-00-00', NULL),
(2232, 144, 'Essaouira', 'ESS', 1, '0000-00-00', '0000-00-00', NULL),
(2233, 144, 'Fes', 'FES', 1, '0000-00-00', '0000-00-00', NULL),
(2234, 144, 'Figuig', 'FIG', 1, '0000-00-00', '0000-00-00', NULL),
(2235, 144, 'Guelmim', 'GLM', 1, '0000-00-00', '0000-00-00', NULL),
(2236, 144, 'Ifrane', 'IFR', 1, '0000-00-00', '0000-00-00', NULL),
(2237, 144, 'Kenitra', 'KEN', 1, '0000-00-00', '0000-00-00', NULL),
(2238, 144, 'Khemisset', 'KHM', 1, '0000-00-00', '0000-00-00', NULL),
(2239, 144, 'Khenifra', 'KHN', 1, '0000-00-00', '0000-00-00', NULL),
(2240, 144, 'Khouribga', 'KHO', 1, '0000-00-00', '0000-00-00', NULL),
(2241, 144, 'Laayoune', 'LYN', 1, '0000-00-00', '0000-00-00', NULL),
(2242, 144, 'Larache', 'LAR', 1, '0000-00-00', '0000-00-00', NULL),
(2243, 144, 'Marrakech', 'MRK', 1, '0000-00-00', '0000-00-00', NULL),
(2244, 144, 'Meknes', 'MKN', 1, '0000-00-00', '0000-00-00', NULL),
(2245, 144, 'Nador', 'NAD', 1, '0000-00-00', '0000-00-00', NULL),
(2246, 144, 'Ouarzazate', 'ORZ', 1, '0000-00-00', '0000-00-00', NULL),
(2247, 144, 'Oujda', 'OUJ', 1, '0000-00-00', '0000-00-00', NULL),
(2248, 144, 'Rabat-Sale', 'RSA', 1, '0000-00-00', '0000-00-00', NULL),
(2249, 144, 'Safi', 'SAF', 1, '0000-00-00', '0000-00-00', NULL),
(2250, 144, 'Settat', 'SET', 1, '0000-00-00', '0000-00-00', NULL),
(2251, 144, 'Sidi Kacem', 'SKA', 1, '0000-00-00', '0000-00-00', NULL),
(2252, 144, 'Tangier', 'TGR', 1, '0000-00-00', '0000-00-00', NULL),
(2253, 144, 'Tan-Tan', 'TAN', 1, '0000-00-00', '0000-00-00', NULL),
(2254, 144, 'Taounate', 'TAO', 1, '0000-00-00', '0000-00-00', NULL),
(2255, 144, 'Taroudannt', 'TRD', 1, '0000-00-00', '0000-00-00', NULL),
(2256, 144, 'Tata', 'TAT', 1, '0000-00-00', '0000-00-00', NULL),
(2257, 144, 'Taza', 'TAZ', 1, '0000-00-00', '0000-00-00', NULL),
(2258, 144, 'Tetouan', 'TET', 1, '0000-00-00', '0000-00-00', NULL),
(2259, 144, 'Tiznit', 'TIZ', 1, '0000-00-00', '0000-00-00', NULL),
(2260, 144, 'Ad Dakhla', 'ADK', 1, '0000-00-00', '0000-00-00', NULL),
(2261, 144, 'Boujdour', 'BJD', 1, '0000-00-00', '0000-00-00', NULL),
(2262, 144, 'Es Smara', 'ESM', 1, '0000-00-00', '0000-00-00', NULL),
(2263, 145, 'Cabo Delgado', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(2264, 145, 'Gaza', 'GZ', 1, '0000-00-00', '0000-00-00', NULL),
(2265, 145, 'Inhambane', 'IN', 1, '0000-00-00', '0000-00-00', NULL),
(2266, 145, 'Manica', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(2267, 145, 'Maputo (city)', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(2268, 145, 'Maputo', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(2269, 145, 'Nampula', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2270, 145, 'Niassa', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2271, 145, 'Sofala', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2272, 145, 'Tete', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(2273, 145, 'Zambezia', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(2274, 146, 'Ayeyarwady', 'AY', 1, '0000-00-00', '0000-00-00', NULL),
(2275, 146, 'Bago', 'BG', 1, '0000-00-00', '0000-00-00', NULL),
(2276, 146, 'Magway', 'MG', 1, '0000-00-00', '0000-00-00', NULL),
(2277, 146, 'Mandalay', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(2278, 146, 'Sagaing', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(2279, 146, 'Tanintharyi', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(2280, 146, 'Yangon', 'YG', 1, '0000-00-00', '0000-00-00', NULL),
(2281, 146, 'Chin State', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2282, 146, 'Kachin State', 'KC', 1, '0000-00-00', '0000-00-00', NULL),
(2283, 146, 'Kayah State', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(2284, 146, 'Kayin State', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(2285, 146, 'Mon State', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(2286, 146, 'Rakhine State', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(2287, 146, 'Shan State', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(2288, 147, 'Caprivi', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2289, 147, 'Erongo', 'ER', 1, '0000-00-00', '0000-00-00', NULL),
(2290, 147, 'Hardap', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(2291, 147, 'Karas', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(2292, 147, 'Kavango', 'KV', 1, '0000-00-00', '0000-00-00', NULL),
(2293, 147, 'Khomas', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(2294, 147, 'Kunene', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(2295, 147, 'Ohangwena', 'OW', 1, '0000-00-00', '0000-00-00', NULL),
(2296, 147, 'Omaheke', 'OK', 1, '0000-00-00', '0000-00-00', NULL),
(2297, 147, 'Omusati', 'OT', 1, '0000-00-00', '0000-00-00', NULL),
(2298, 147, 'Oshana', 'ON', 1, '0000-00-00', '0000-00-00', NULL),
(2299, 147, 'Oshikoto', 'OO', 1, '0000-00-00', '0000-00-00', NULL),
(2300, 147, 'Otjozondjupa', 'OJ', 1, '0000-00-00', '0000-00-00', NULL),
(2301, 148, 'Aiwo', 'AO', 1, '0000-00-00', '0000-00-00', NULL),
(2302, 148, 'Anabar', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(2303, 148, 'Anetan', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(2304, 148, 'Anibare', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(2305, 148, 'Baiti', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2306, 148, 'Boe', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(2307, 148, 'Buada', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(2308, 148, 'Denigomodu', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(2309, 148, 'Ewa', 'EW', 1, '0000-00-00', '0000-00-00', NULL),
(2310, 148, 'Ijuw', 'IJ', 1, '0000-00-00', '0000-00-00', NULL),
(2311, 148, 'Meneng', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(2312, 148, 'Nibok', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2313, 148, 'Uaboe', 'UA', 1, '0000-00-00', '0000-00-00', NULL),
(2314, 148, 'Yaren', 'YA', 1, '0000-00-00', '0000-00-00', NULL),
(2315, 149, 'Bagmati', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2316, 149, 'Bheri', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(2317, 149, 'Dhawalagiri', 'DH', 1, '0000-00-00', '0000-00-00', NULL),
(2318, 149, 'Gandaki', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2319, 149, 'Janakpur', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(2320, 149, 'Karnali', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(2321, 149, 'Kosi', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2322, 149, 'Lumbini', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(2323, 149, 'Mahakali', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(2324, 149, 'Mechi', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(2325, 149, 'Narayani', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2326, 149, 'Rapti', 'RA', 1, '0000-00-00', '0000-00-00', NULL),
(2327, 149, 'Sagarmatha', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2328, 149, 'Seti', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(2329, 150, 'Drenthe', 'DR', 1, '0000-00-00', '0000-00-00', NULL),
(2330, 150, 'Flevoland', 'FL', 1, '0000-00-00', '0000-00-00', NULL),
(2331, 150, 'Friesland', 'FR', 1, '0000-00-00', '0000-00-00', NULL),
(2332, 150, 'Gelderland', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(2333, 150, 'Groningen', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(2334, 150, 'Limburg', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(2335, 150, 'Noord Brabant', 'NB', 1, '0000-00-00', '0000-00-00', NULL),
(2336, 150, 'Noord Holland', 'NH', 1, '0000-00-00', '0000-00-00', NULL),
(2337, 150, 'Overijssel', 'OV', 1, '0000-00-00', '0000-00-00', NULL),
(2338, 150, 'Utrecht', 'UT', 1, '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2339, 150, 'Zeeland', 'ZE', 1, '0000-00-00', '0000-00-00', NULL),
(2340, 150, 'Zuid Holland', 'ZH', 1, '0000-00-00', '0000-00-00', NULL),
(2341, 152, 'Iles Loyaute', 'L', 1, '0000-00-00', '0000-00-00', NULL),
(2342, 152, 'Nord', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(2343, 152, 'Sud', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(2344, 153, 'Auckland', 'AUK', 1, '0000-00-00', '0000-00-00', NULL),
(2345, 153, 'Bay of Plenty', 'BOP', 1, '0000-00-00', '0000-00-00', NULL),
(2346, 153, 'Canterbury', 'CAN', 1, '0000-00-00', '0000-00-00', NULL),
(2347, 153, 'Coromandel', 'COR', 1, '0000-00-00', '0000-00-00', NULL),
(2348, 153, 'Gisborne', 'GIS', 1, '0000-00-00', '0000-00-00', NULL),
(2349, 153, 'Fiordland', 'FIO', 1, '0000-00-00', '0000-00-00', NULL),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1, '0000-00-00', '0000-00-00', NULL),
(2351, 153, 'Marlborough', 'MBH', 1, '0000-00-00', '0000-00-00', NULL),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1, '0000-00-00', '0000-00-00', NULL),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1, '0000-00-00', '0000-00-00', NULL),
(2354, 153, 'Nelson', 'NSN', 1, '0000-00-00', '0000-00-00', NULL),
(2355, 153, 'Northland', 'NTL', 1, '0000-00-00', '0000-00-00', NULL),
(2356, 153, 'Otago', 'OTA', 1, '0000-00-00', '0000-00-00', NULL),
(2357, 153, 'Southland', 'STL', 1, '0000-00-00', '0000-00-00', NULL),
(2358, 153, 'Taranaki', 'TKI', 1, '0000-00-00', '0000-00-00', NULL),
(2359, 153, 'Wellington', 'WGN', 1, '0000-00-00', '0000-00-00', NULL),
(2360, 153, 'Waikato', 'WKO', 1, '0000-00-00', '0000-00-00', NULL),
(2361, 153, 'Wairarapa', 'WAI', 1, '0000-00-00', '0000-00-00', NULL),
(2362, 153, 'West Coast', 'WTC', 1, '0000-00-00', '0000-00-00', NULL),
(2363, 154, 'Atlantico Norte', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2364, 154, 'Atlantico Sur', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2365, 154, 'Boaco', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(2366, 154, 'Carazo', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2367, 154, 'Chinandega', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(2368, 154, 'Chontales', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(2369, 154, 'Esteli', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(2370, 154, 'Granada', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(2371, 154, 'Jinotega', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(2372, 154, 'Leon', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(2373, 154, 'Madriz', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(2374, 154, 'Managua', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(2375, 154, 'Masaya', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(2376, 154, 'Matagalpa', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(2377, 154, 'Nuevo Segovia', 'NS', 1, '0000-00-00', '0000-00-00', NULL),
(2378, 154, 'Rio San Juan', 'RS', 1, '0000-00-00', '0000-00-00', NULL),
(2379, 154, 'Rivas', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(2380, 155, 'Agadez', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(2381, 155, 'Diffa', 'DF', 1, '0000-00-00', '0000-00-00', NULL),
(2382, 155, 'Dosso', 'DS', 1, '0000-00-00', '0000-00-00', NULL),
(2383, 155, 'Maradi', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(2384, 155, 'Niamey', 'NM', 1, '0000-00-00', '0000-00-00', NULL),
(2385, 155, 'Tahoua', 'TH', 1, '0000-00-00', '0000-00-00', NULL),
(2386, 155, 'Tillaberi', 'TL', 1, '0000-00-00', '0000-00-00', NULL),
(2387, 155, 'Zinder', 'ZD', 1, '0000-00-00', '0000-00-00', NULL),
(2388, 156, 'Abia', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(2390, 156, 'Adamawa', 'AD', 1, '0000-00-00', '0000-00-00', NULL),
(2391, 156, 'Akwa Ibom', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(2392, 156, 'Anambra', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2393, 156, 'Bauchi', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(2394, 156, 'Bayelsa', 'BY', 1, '0000-00-00', '0000-00-00', NULL),
(2395, 156, 'Benue', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(2396, 156, 'Borno', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(2397, 156, 'Cross River', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(2398, 156, 'Delta', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(2399, 156, 'Ebonyi', 'EB', 1, '0000-00-00', '0000-00-00', NULL),
(2400, 156, 'Edo', 'ED', 1, '0000-00-00', '0000-00-00', NULL),
(2401, 156, 'Ekiti', 'EK', 1, '0000-00-00', '0000-00-00', NULL),
(2402, 156, 'Enugu', 'EN', 1, '0000-00-00', '0000-00-00', NULL),
(2403, 156, 'Gombe', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(2404, 156, 'Imo', 'IM', 1, '0000-00-00', '0000-00-00', NULL),
(2405, 156, 'Jigawa', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(2406, 156, 'Kaduna', 'KD', 1, '0000-00-00', '0000-00-00', NULL),
(2407, 156, 'Kano', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(2408, 156, 'Katsina', 'KT', 1, '0000-00-00', '0000-00-00', NULL),
(2409, 156, 'Kebbi', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(2410, 156, 'Kogi', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2411, 156, 'Kwara', 'KW', 1, '0000-00-00', '0000-00-00', NULL),
(2412, 156, 'Lagos', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(2413, 156, 'Nassarawa', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2414, 156, 'Niger', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2415, 156, 'Ogun', 'OG', 1, '0000-00-00', '0000-00-00', NULL),
(2416, 156, 'Ondo', 'ONG', 1, '0000-00-00', '0000-00-00', NULL),
(2417, 156, 'Osun', 'OS', 1, '0000-00-00', '0000-00-00', NULL),
(2418, 156, 'Oyo', 'OY', 1, '0000-00-00', '0000-00-00', NULL),
(2419, 156, 'Plateau', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(2420, 156, 'Rivers', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(2421, 156, 'Sokoto', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2422, 156, 'Taraba', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2423, 156, 'Yobe', 'YO', 1, '0000-00-00', '0000-00-00', NULL),
(2424, 156, 'Zamfara', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(2425, 159, 'Northern Islands', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(2426, 159, 'Rota', 'R', 1, '0000-00-00', '0000-00-00', NULL),
(2427, 159, 'Saipan', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(2428, 159, 'Tinian', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(2429, 160, 'Akershus', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(2430, 160, 'Aust-Agder', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(2431, 160, 'Buskerud', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(2432, 160, 'Finnmark', 'FM', 1, '0000-00-00', '0000-00-00', NULL),
(2433, 160, 'Hedmark', 'HM', 1, '0000-00-00', '0000-00-00', NULL),
(2434, 160, 'Hordaland', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(2435, 160, 'More og Romdal', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(2436, 160, 'Nord-Trondelag', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(2437, 160, 'Nordland', 'NL', 1, '0000-00-00', '0000-00-00', NULL),
(2438, 160, 'Ostfold', 'OF', 1, '0000-00-00', '0000-00-00', NULL),
(2439, 160, 'Oppland', 'OP', 1, '0000-00-00', '0000-00-00', NULL),
(2440, 160, 'Oslo', 'OL', 1, '0000-00-00', '0000-00-00', NULL),
(2441, 160, 'Rogaland', 'RL', 1, '0000-00-00', '0000-00-00', NULL),
(2442, 160, 'Sor-Trondelag', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(2444, 160, 'Svalbard', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(2445, 160, 'Telemark', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(2446, 160, 'Troms', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(2447, 160, 'Vest-Agder', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(2448, 160, 'Vestfold', 'VF', 1, '0000-00-00', '0000-00-00', NULL),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(2450, 161, 'Al Batinah', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2451, 161, 'Al Wusta', 'WU', 1, '0000-00-00', '0000-00-00', NULL),
(2452, 161, 'Ash Sharqiyah', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(2453, 161, 'Az Zahirah', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(2454, 161, 'Masqat', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(2455, 161, 'Musandam', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(2456, 161, 'Zufar', 'ZU', 1, '0000-00-00', '0000-00-00', NULL),
(2457, 162, 'Balochistan', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(2459, 162, 'Islamabad Capital Territory', 'I', 1, '0000-00-00', '0000-00-00', NULL),
(2460, 162, 'North-West Frontier', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(2461, 162, 'Punjab', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2462, 162, 'Sindh', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(2463, 163, 'Aimeliik', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(2464, 163, 'Airai', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2465, 163, 'Angaur', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2466, 163, 'Hatohobei', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(2467, 163, 'Kayangel', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(2468, 163, 'Koror', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2469, 163, 'Melekeok', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(2470, 163, 'Ngaraard', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2471, 163, 'Ngarchelong', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(2472, 163, 'Ngardmau', 'ND', 1, '0000-00-00', '0000-00-00', NULL),
(2473, 163, 'Ngatpang', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(2474, 163, 'Ngchesar', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(2475, 163, 'Ngeremlengui', 'NR', 1, '0000-00-00', '0000-00-00', NULL),
(2476, 163, 'Ngiwal', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(2477, 163, 'Peleliu', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(2478, 163, 'Sonsorol', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2479, 164, 'Bocas del Toro', 'BT', 1, '0000-00-00', '0000-00-00', NULL),
(2480, 164, 'Chiriqui', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2481, 164, 'Cocle', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(2482, 164, 'Colon', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2483, 164, 'Darien', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(2484, 164, 'Herrera', 'HE', 1, '0000-00-00', '0000-00-00', NULL),
(2485, 164, 'Los Santos', 'LS', 1, '0000-00-00', '0000-00-00', NULL),
(2486, 164, 'Panama', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2487, 164, 'San Blas', 'SB', 1, '0000-00-00', '0000-00-00', NULL),
(2488, 164, 'Veraguas', 'VG', 1, '0000-00-00', '0000-00-00', NULL),
(2489, 165, 'Bougainville', 'BV', 1, '0000-00-00', '0000-00-00', NULL),
(2490, 165, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(2491, 165, 'Chimbu', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2492, 165, 'Eastern Highlands', 'EH', 1, '0000-00-00', '0000-00-00', NULL),
(2493, 165, 'East New Britain', 'EB', 1, '0000-00-00', '0000-00-00', NULL),
(2494, 165, 'East Sepik', 'ES', 1, '0000-00-00', '0000-00-00', NULL),
(2495, 165, 'Enga', 'EN', 1, '0000-00-00', '0000-00-00', NULL),
(2496, 165, 'Gulf', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(2497, 165, 'Madang', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(2498, 165, 'Manus', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(2499, 165, 'Milne Bay', 'MB', 1, '0000-00-00', '0000-00-00', NULL),
(2500, 165, 'Morobe', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(2501, 165, 'National Capital', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(2502, 165, 'New Ireland', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2503, 165, 'Northern', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(2504, 165, 'Sandaun', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2505, 165, 'Southern Highlands', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(2506, 165, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(2507, 165, 'Western Highlands', 'WH', 1, '0000-00-00', '0000-00-00', NULL),
(2508, 165, 'West New Britain', 'WB', 1, '0000-00-00', '0000-00-00', NULL),
(2509, 166, 'Alto Paraguay', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(2510, 166, 'Alto Parana', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2511, 166, 'Amambay', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(2512, 166, 'Asuncion', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2513, 166, 'Boqueron', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(2514, 166, 'Caaguazu', 'CG', 1, '0000-00-00', '0000-00-00', NULL),
(2515, 166, 'Caazapa', 'CZ', 1, '0000-00-00', '0000-00-00', NULL),
(2516, 166, 'Canindeyu', 'CN', 1, '0000-00-00', '0000-00-00', NULL),
(2517, 166, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(2518, 166, 'Concepcion', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(2519, 166, 'Cordillera', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(2520, 166, 'Guaira', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(2521, 166, 'Itapua', 'IT', 1, '0000-00-00', '0000-00-00', NULL),
(2522, 166, 'Misiones', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(2523, 166, 'Neembucu', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(2524, 166, 'Paraguari', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2525, 166, 'Presidente Hayes', 'PH', 1, '0000-00-00', '0000-00-00', NULL),
(2526, 166, 'San Pedro', 'SP', 1, '0000-00-00', '0000-00-00', NULL),
(2527, 167, 'Amazonas', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(2528, 167, 'Ancash', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2529, 167, 'Apurimac', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(2530, 167, 'Arequipa', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2531, 167, 'Ayacucho', 'AY', 1, '0000-00-00', '0000-00-00', NULL),
(2532, 167, 'Cajamarca', 'CJ', 1, '0000-00-00', '0000-00-00', NULL),
(2533, 167, 'Callao', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2534, 167, 'Cusco', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(2535, 167, 'Huancavelica', 'HV', 1, '0000-00-00', '0000-00-00', NULL),
(2536, 167, 'Huanuco', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(2537, 167, 'Ica', 'IC', 1, '0000-00-00', '0000-00-00', NULL),
(2538, 167, 'Junin', 'JU', 1, '0000-00-00', '0000-00-00', NULL),
(2539, 167, 'La Libertad', 'LD', 1, '0000-00-00', '0000-00-00', NULL),
(2540, 167, 'Lambayeque', 'LY', 1, '0000-00-00', '0000-00-00', NULL),
(2541, 167, 'Lima', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(2542, 167, 'Loreto', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(2543, 167, 'Madre de Dios', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(2544, 167, 'Moquegua', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(2545, 167, 'Pasco', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2546, 167, 'Piura', 'PI', 1, '0000-00-00', '0000-00-00', NULL),
(2547, 167, 'Puno', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(2548, 167, 'San Martin', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(2549, 167, 'Tacna', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2550, 167, 'Tumbes', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(2551, 167, 'Ucayali', 'UC', 1, '0000-00-00', '0000-00-00', NULL),
(2552, 168, 'Abra', 'ABR', 1, '0000-00-00', '0000-00-00', NULL),
(2553, 168, 'Agusan del Norte', 'ANO', 1, '0000-00-00', '0000-00-00', NULL),
(2554, 168, 'Agusan del Sur', 'ASU', 1, '0000-00-00', '0000-00-00', NULL),
(2555, 168, 'Aklan', 'AKL', 1, '0000-00-00', '0000-00-00', NULL),
(2556, 168, 'Albay', 'ALB', 1, '0000-00-00', '0000-00-00', NULL),
(2557, 168, 'Antique', 'ANT', 1, '0000-00-00', '0000-00-00', NULL),
(2558, 168, 'Apayao', 'APY', 1, '0000-00-00', '0000-00-00', NULL),
(2559, 168, 'Aurora', 'AUR', 1, '0000-00-00', '0000-00-00', NULL),
(2560, 168, 'Basilan', 'BAS', 1, '0000-00-00', '0000-00-00', NULL),
(2561, 168, 'Bataan', 'BTA', 1, '0000-00-00', '0000-00-00', NULL),
(2562, 168, 'Batanes', 'BTE', 1, '0000-00-00', '0000-00-00', NULL),
(2563, 168, 'Batangas', 'BTG', 1, '0000-00-00', '0000-00-00', NULL),
(2564, 168, 'Biliran', 'BLR', 1, '0000-00-00', '0000-00-00', NULL),
(2565, 168, 'Benguet', 'BEN', 1, '0000-00-00', '0000-00-00', NULL),
(2566, 168, 'Bohol', 'BOL', 1, '0000-00-00', '0000-00-00', NULL),
(2567, 168, 'Bukidnon', 'BUK', 1, '0000-00-00', '0000-00-00', NULL),
(2568, 168, 'Bulacan', 'BUL', 1, '0000-00-00', '0000-00-00', NULL),
(2569, 168, 'Cagayan', 'CAG', 1, '0000-00-00', '0000-00-00', NULL),
(2570, 168, 'Camarines Norte', 'CNO', 1, '0000-00-00', '0000-00-00', NULL),
(2571, 168, 'Camarines Sur', 'CSU', 1, '0000-00-00', '0000-00-00', NULL),
(2572, 168, 'Camiguin', 'CAM', 1, '0000-00-00', '0000-00-00', NULL),
(2573, 168, 'Capiz', 'CAP', 1, '0000-00-00', '0000-00-00', NULL),
(2574, 168, 'Catanduanes', 'CAT', 1, '0000-00-00', '0000-00-00', NULL),
(2575, 168, 'Cavite', 'CAV', 1, '0000-00-00', '0000-00-00', NULL),
(2576, 168, 'Cebu', 'CEB', 1, '0000-00-00', '0000-00-00', NULL),
(2577, 168, 'Compostela', 'CMP', 1, '0000-00-00', '0000-00-00', NULL),
(2578, 168, 'Davao del Norte', 'DNO', 1, '0000-00-00', '0000-00-00', NULL),
(2579, 168, 'Davao del Sur', 'DSU', 1, '0000-00-00', '0000-00-00', NULL),
(2580, 168, 'Davao Oriental', 'DOR', 1, '0000-00-00', '0000-00-00', NULL),
(2581, 168, 'Eastern Samar', 'ESA', 1, '0000-00-00', '0000-00-00', NULL),
(2582, 168, 'Guimaras', 'GUI', 1, '0000-00-00', '0000-00-00', NULL),
(2583, 168, 'Ifugao', 'IFU', 1, '0000-00-00', '0000-00-00', NULL),
(2584, 168, 'Ilocos Norte', 'INO', 1, '0000-00-00', '0000-00-00', NULL),
(2585, 168, 'Ilocos Sur', 'ISU', 1, '0000-00-00', '0000-00-00', NULL),
(2586, 168, 'Iloilo', 'ILO', 1, '0000-00-00', '0000-00-00', NULL),
(2587, 168, 'Isabela', 'ISA', 1, '0000-00-00', '0000-00-00', NULL),
(2588, 168, 'Kalinga', 'KAL', 1, '0000-00-00', '0000-00-00', NULL),
(2589, 168, 'Laguna', 'LAG', 1, '0000-00-00', '0000-00-00', NULL),
(2590, 168, 'Lanao del Norte', 'LNO', 1, '0000-00-00', '0000-00-00', NULL),
(2591, 168, 'Lanao del Sur', 'LSU', 1, '0000-00-00', '0000-00-00', NULL),
(2592, 168, 'La Union', 'UNI', 1, '0000-00-00', '0000-00-00', NULL),
(2593, 168, 'Leyte', 'LEY', 1, '0000-00-00', '0000-00-00', NULL),
(2594, 168, 'Maguindanao', 'MAG', 1, '0000-00-00', '0000-00-00', NULL),
(2595, 168, 'Marinduque', 'MRN', 1, '0000-00-00', '0000-00-00', NULL),
(2596, 168, 'Masbate', 'MSB', 1, '0000-00-00', '0000-00-00', NULL),
(2597, 168, 'Mindoro Occidental', 'MIC', 1, '0000-00-00', '0000-00-00', NULL),
(2598, 168, 'Mindoro Oriental', 'MIR', 1, '0000-00-00', '0000-00-00', NULL),
(2599, 168, 'Misamis Occidental', 'MSC', 1, '0000-00-00', '0000-00-00', NULL),
(2600, 168, 'Misamis Oriental', 'MOR', 1, '0000-00-00', '0000-00-00', NULL),
(2601, 168, 'Mountain', 'MOP', 1, '0000-00-00', '0000-00-00', NULL),
(2602, 168, 'Negros Occidental', 'NOC', 1, '0000-00-00', '0000-00-00', NULL),
(2603, 168, 'Negros Oriental', 'NOR', 1, '0000-00-00', '0000-00-00', NULL),
(2604, 168, 'North Cotabato', 'NCT', 1, '0000-00-00', '0000-00-00', NULL),
(2605, 168, 'Northern Samar', 'NSM', 1, '0000-00-00', '0000-00-00', NULL),
(2606, 168, 'Nueva Ecija', 'NEC', 1, '0000-00-00', '0000-00-00', NULL),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1, '0000-00-00', '0000-00-00', NULL),
(2608, 168, 'Palawan', 'PLW', 1, '0000-00-00', '0000-00-00', NULL),
(2609, 168, 'Pampanga', 'PMP', 1, '0000-00-00', '0000-00-00', NULL),
(2610, 168, 'Pangasinan', 'PNG', 1, '0000-00-00', '0000-00-00', NULL),
(2611, 168, 'Quezon', 'QZN', 1, '0000-00-00', '0000-00-00', NULL),
(2612, 168, 'Quirino', 'QRN', 1, '0000-00-00', '0000-00-00', NULL),
(2613, 168, 'Rizal', 'RIZ', 1, '0000-00-00', '0000-00-00', NULL),
(2614, 168, 'Romblon', 'ROM', 1, '0000-00-00', '0000-00-00', NULL),
(2615, 168, 'Samar', 'SMR', 1, '0000-00-00', '0000-00-00', NULL),
(2616, 168, 'Sarangani', 'SRG', 1, '0000-00-00', '0000-00-00', NULL),
(2617, 168, 'Siquijor', 'SQJ', 1, '0000-00-00', '0000-00-00', NULL),
(2618, 168, 'Sorsogon', 'SRS', 1, '0000-00-00', '0000-00-00', NULL),
(2619, 168, 'South Cotabato', 'SCO', 1, '0000-00-00', '0000-00-00', NULL),
(2620, 168, 'Southern Leyte', 'SLE', 1, '0000-00-00', '0000-00-00', NULL),
(2621, 168, 'Sultan Kudarat', 'SKU', 1, '0000-00-00', '0000-00-00', NULL),
(2622, 168, 'Sulu', 'SLU', 1, '0000-00-00', '0000-00-00', NULL),
(2623, 168, 'Surigao del Norte', 'SNO', 1, '0000-00-00', '0000-00-00', NULL),
(2624, 168, 'Surigao del Sur', 'SSU', 1, '0000-00-00', '0000-00-00', NULL),
(2625, 168, 'Tarlac', 'TAR', 1, '0000-00-00', '0000-00-00', NULL),
(2626, 168, 'Tawi-Tawi', 'TAW', 1, '0000-00-00', '0000-00-00', NULL),
(2627, 168, 'Zambales', 'ZBL', 1, '0000-00-00', '0000-00-00', NULL),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1, '0000-00-00', '0000-00-00', NULL),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1, '0000-00-00', '0000-00-00', NULL),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1, '0000-00-00', '0000-00-00', NULL),
(2631, 170, 'Dolnoslaskie', 'DO', 1, '0000-00-00', '0000-00-00', NULL),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1, '0000-00-00', '0000-00-00', NULL),
(2633, 170, 'Lodzkie', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(2634, 170, 'Lubelskie', 'LL', 1, '0000-00-00', '0000-00-00', NULL),
(2635, 170, 'Lubuskie', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(2636, 170, 'Malopolskie', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(2637, 170, 'Mazowieckie', 'MZ', 1, '0000-00-00', '0000-00-00', NULL),
(2638, 170, 'Opolskie', 'OP', 1, '0000-00-00', '0000-00-00', NULL),
(2639, 170, 'Podkarpackie', 'PP', 1, '0000-00-00', '0000-00-00', NULL),
(2640, 170, 'Podlaskie', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(2641, 170, 'Pomorskie', 'PM', 1, '0000-00-00', '0000-00-00', NULL),
(2642, 170, 'Slaskie', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(2643, 170, 'Swietokrzyskie', 'SW', 1, '0000-00-00', '0000-00-00', NULL),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1, '0000-00-00', '0000-00-00', NULL),
(2645, 170, 'Wielkopolskie', 'WP', 1, '0000-00-00', '0000-00-00', NULL),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(2647, 198, 'Saint Pierre', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2648, 198, 'Miquelon', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(2649, 171, 'A&ccedil;ores', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(2650, 171, 'Aveiro', 'AV', 1, '0000-00-00', '0000-00-00', NULL),
(2651, 171, 'Beja', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(2652, 171, 'Braga', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2654, 171, 'Castelo Branco', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(2655, 171, 'Coimbra', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(2656, 171, '&Eacute;vora', 'EV', 1, '0000-00-00', '0000-00-00', NULL),
(2657, 171, 'Faro', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(2658, 171, 'Guarda', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(2659, 171, 'Leiria', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(2660, 171, 'Lisboa', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(2661, 171, 'Madeira', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(2662, 171, 'Portalegre', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(2663, 171, 'Porto', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(2664, 171, 'Santar&eacute;m', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2665, 171, 'Set&uacute;bal', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(2666, 171, 'Viana do Castelo', 'VC', 1, '0000-00-00', '0000-00-00', NULL),
(2667, 171, 'Vila Real', 'VR', 1, '0000-00-00', '0000-00-00', NULL),
(2668, 171, 'Viseu', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(2669, 173, 'Ad Dawhah', 'DW', 1, '0000-00-00', '0000-00-00', NULL),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1, '0000-00-00', '0000-00-00', NULL),
(2671, 173, 'Al Jumayliyah', 'JM', 1, '0000-00-00', '0000-00-00', NULL),
(2672, 173, 'Al Khawr', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(2673, 173, 'Al Wakrah', 'WK', 1, '0000-00-00', '0000-00-00', NULL),
(2674, 173, 'Ar Rayyan', 'RN', 1, '0000-00-00', '0000-00-00', NULL),
(2675, 173, 'Jarayan al Batinah', 'JB', 1, '0000-00-00', '0000-00-00', NULL),
(2676, 173, 'Madinat ash Shamal', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(2677, 173, 'Umm Sa\'id', 'UD', 1, '0000-00-00', '0000-00-00', NULL),
(2678, 173, 'Umm Salal', 'UL', 1, '0000-00-00', '0000-00-00', NULL),
(2679, 175, 'Alba', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(2680, 175, 'Arad', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2681, 175, 'Arges', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(2682, 175, 'Bacau', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(2683, 175, 'Bihor', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(2685, 175, 'Botosani', 'BT', 1, '0000-00-00', '0000-00-00', NULL),
(2686, 175, 'Brasov', 'BV', 1, '0000-00-00', '0000-00-00', NULL),
(2687, 175, 'Braila', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2688, 175, 'Bucuresti', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(2689, 175, 'Buzau', 'BZ', 1, '0000-00-00', '0000-00-00', NULL),
(2690, 175, 'Caras-Severin', 'CS', 1, '0000-00-00', '0000-00-00', NULL),
(2691, 175, 'Calarasi', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2692, 175, 'Cluj', 'CJ', 1, '0000-00-00', '0000-00-00', NULL),
(2693, 175, 'Constanta', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(2694, 175, 'Covasna', 'CV', 1, '0000-00-00', '0000-00-00', NULL),
(2695, 175, 'Dimbovita', 'DB', 1, '0000-00-00', '0000-00-00', NULL),
(2696, 175, 'Dolj', 'DJ', 1, '0000-00-00', '0000-00-00', NULL),
(2697, 175, 'Galati', 'GL', 1, '0000-00-00', '0000-00-00', NULL),
(2698, 175, 'Giurgiu', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(2699, 175, 'Gorj', 'GJ', 1, '0000-00-00', '0000-00-00', NULL),
(2700, 175, 'Harghita', 'HR', 1, '0000-00-00', '0000-00-00', NULL),
(2701, 175, 'Hunedoara', 'HD', 1, '0000-00-00', '0000-00-00', NULL),
(2702, 175, 'Ialomita', 'IL', 1, '0000-00-00', '0000-00-00', NULL),
(2703, 175, 'Iasi', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(2704, 175, 'Ilfov', 'IF', 1, '0000-00-00', '0000-00-00', NULL),
(2705, 175, 'Maramures', 'MM', 1, '0000-00-00', '0000-00-00', NULL),
(2706, 175, 'Mehedinti', 'MH', 1, '0000-00-00', '0000-00-00', NULL),
(2707, 175, 'Mures', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(2708, 175, 'Neamt', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(2709, 175, 'Olt', 'OT', 1, '0000-00-00', '0000-00-00', NULL),
(2710, 175, 'Prahova', 'PH', 1, '0000-00-00', '0000-00-00', NULL),
(2711, 175, 'Satu-Mare', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(2712, 175, 'Salaj', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(2713, 175, 'Sibiu', 'SB', 1, '0000-00-00', '0000-00-00', NULL),
(2714, 175, 'Suceava', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(2715, 175, 'Teleorman', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(2716, 175, 'Timis', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(2717, 175, 'Tulcea', 'TL', 1, '0000-00-00', '0000-00-00', NULL),
(2718, 175, 'Vaslui', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(2719, 175, 'Valcea', 'VL', 1, '0000-00-00', '0000-00-00', NULL),
(2720, 175, 'Vrancea', 'VN', 1, '0000-00-00', '0000-00-00', NULL),
(2721, 176, 'Abakan', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(2722, 176, 'Aginskoye', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(2723, 176, 'Anadyr', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2724, 176, 'Arkahangelsk', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2725, 176, 'Astrakhan', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2726, 176, 'Barnaul', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2727, 176, 'Belgorod', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(2728, 176, 'Birobidzhan', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(2729, 176, 'Blagoveshchensk', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(2730, 176, 'Bryansk', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2731, 176, 'Cheboksary', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2732, 176, 'Chelyabinsk', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2733, 176, 'Cherkessk', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(2734, 176, 'Chita', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(2735, 176, 'Dudinka', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(2736, 176, 'Elista', 'EL', 1, '0000-00-00', '0000-00-00', NULL),
(2737, 176, 'Gomo-Altaysk', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(2738, 176, 'Gorno-Altaysk', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2739, 176, 'Groznyy', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(2740, 176, 'Irkutsk', 'IR', 1, '0000-00-00', '0000-00-00', NULL),
(2741, 176, 'Ivanovo', 'IV', 1, '0000-00-00', '0000-00-00', NULL),
(2742, 176, 'Izhevsk', 'IZ', 1, '0000-00-00', '0000-00-00', NULL),
(2743, 176, 'Kalinigrad', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(2744, 176, 'Kaluga', 'KL', 1, '0000-00-00', '0000-00-00', NULL),
(2745, 176, 'Kasnodar', 'KS', 1, '0000-00-00', '0000-00-00', NULL),
(2746, 176, 'Kazan', 'KZ', 1, '0000-00-00', '0000-00-00', NULL),
(2747, 176, 'Kemerovo', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(2748, 176, 'Khabarovsk', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1, '0000-00-00', '0000-00-00', NULL),
(2750, 176, 'Kostroma', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2751, 176, 'Krasnodar', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(2752, 176, 'Krasnoyarsk', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(2753, 176, 'Kudymkar', 'KU', 1, '0000-00-00', '0000-00-00', NULL),
(2754, 176, 'Kurgan', 'KG', 1, '0000-00-00', '0000-00-00', NULL),
(2755, 176, 'Kursk', 'KK', 1, '0000-00-00', '0000-00-00', NULL),
(2756, 176, 'Kyzyl', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(2757, 176, 'Lipetsk', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(2758, 176, 'Magadan', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(2759, 176, 'Makhachkala', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(2760, 176, 'Maykop', 'MY', 1, '0000-00-00', '0000-00-00', NULL),
(2761, 176, 'Moscow', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(2762, 176, 'Murmansk', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(2763, 176, 'Nalchik', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(2764, 176, 'Naryan Mar', 'NR', 1, '0000-00-00', '0000-00-00', NULL),
(2765, 176, 'Nazran', 'NZ', 1, '0000-00-00', '0000-00-00', NULL),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2767, 176, 'Novgorod', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(2768, 176, 'Novosibirsk', 'NV', 1, '0000-00-00', '0000-00-00', NULL),
(2769, 176, 'Omsk', 'OM', 1, '0000-00-00', '0000-00-00', NULL),
(2770, 176, 'Orel', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(2771, 176, 'Orenburg', 'OE', 1, '0000-00-00', '0000-00-00', NULL),
(2772, 176, 'Palana', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2773, 176, 'Penza', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(2774, 176, 'Perm', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1, '0000-00-00', '0000-00-00', NULL),
(2776, 176, 'Petrozavodsk', 'PT', 1, '0000-00-00', '0000-00-00', NULL),
(2777, 176, 'Pskov', 'PS', 1, '0000-00-00', '0000-00-00', NULL),
(2778, 176, 'Rostov-na-Donu', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(2779, 176, 'Ryazan', 'RY', 1, '0000-00-00', '0000-00-00', NULL),
(2780, 176, 'Salekhard', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(2781, 176, 'Samara', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2782, 176, 'Saransk', 'SR', 1, '0000-00-00', '0000-00-00', NULL),
(2783, 176, 'Saratov', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(2784, 176, 'Smolensk', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(2785, 176, 'St. Petersburg', 'SP', 1, '0000-00-00', '0000-00-00', NULL),
(2786, 176, 'Stavropol', 'ST', 1, '0000-00-00', '0000-00-00', NULL),
(2787, 176, 'Syktyvkar', 'SY', 1, '0000-00-00', '0000-00-00', NULL),
(2788, 176, 'Tambov', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2789, 176, 'Tomsk', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(2790, 176, 'Tula', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(2791, 176, 'Tura', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(2792, 176, 'Tver', 'TV', 1, '0000-00-00', '0000-00-00', NULL),
(2793, 176, 'Tyumen', 'TY', 1, '0000-00-00', '0000-00-00', NULL),
(2794, 176, 'Ufa', 'UF', 1, '0000-00-00', '0000-00-00', NULL),
(2795, 176, 'Ul\'yanovsk', 'UL', 1, '0000-00-00', '0000-00-00', NULL),
(2796, 176, 'Ulan-Ude', 'UU', 1, '0000-00-00', '0000-00-00', NULL),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1, '0000-00-00', '0000-00-00', NULL),
(2798, 176, 'Vladikavkaz', 'VL', 1, '0000-00-00', '0000-00-00', NULL),
(2799, 176, 'Vladimir', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(2800, 176, 'Vladivostok', 'VV', 1, '0000-00-00', '0000-00-00', NULL),
(2801, 176, 'Volgograd', 'VG', 1, '0000-00-00', '0000-00-00', NULL),
(2802, 176, 'Vologda', 'VD', 1, '0000-00-00', '0000-00-00', NULL),
(2803, 176, 'Voronezh', 'VO', 1, '0000-00-00', '0000-00-00', NULL),
(2804, 176, 'Vyatka', 'VY', 1, '0000-00-00', '0000-00-00', NULL),
(2805, 176, 'Yakutsk', 'YA', 1, '0000-00-00', '0000-00-00', NULL),
(2806, 176, 'Yaroslavl', 'YR', 1, '0000-00-00', '0000-00-00', NULL),
(2807, 176, 'Yekaterinburg', 'YE', 1, '0000-00-00', '0000-00-00', NULL),
(2808, 176, 'Yoshkar-Ola', 'YO', 1, '0000-00-00', '0000-00-00', NULL),
(2809, 177, 'Butare', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(2810, 177, 'Byumba', 'BY', 1, '0000-00-00', '0000-00-00', NULL),
(2811, 177, 'Cyangugu', 'CY', 1, '0000-00-00', '0000-00-00', NULL),
(2812, 177, 'Gikongoro', 'GK', 1, '0000-00-00', '0000-00-00', NULL),
(2813, 177, 'Gisenyi', 'GS', 1, '0000-00-00', '0000-00-00', NULL),
(2814, 177, 'Gitarama', 'GT', 1, '0000-00-00', '0000-00-00', NULL),
(2815, 177, 'Kibungo', 'KG', 1, '0000-00-00', '0000-00-00', NULL),
(2816, 177, 'Kibuye', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(2817, 177, 'Kigali Rurale', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(2818, 177, 'Kigali-ville', 'KV', 1, '0000-00-00', '0000-00-00', NULL),
(2819, 177, 'Ruhengeri', 'RU', 1, '0000-00-00', '0000-00-00', NULL),
(2820, 177, 'Umutara', 'UM', 1, '0000-00-00', '0000-00-00', NULL),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1, '0000-00-00', '0000-00-00', NULL),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1, '0000-00-00', '0000-00-00', NULL),
(2823, 178, 'Saint George Basseterre', 'SGB', 1, '0000-00-00', '0000-00-00', NULL),
(2824, 178, 'Saint George Gingerland', 'SGG', 1, '0000-00-00', '0000-00-00', NULL),
(2825, 178, 'Saint James Windward', 'SJW', 1, '0000-00-00', '0000-00-00', NULL),
(2826, 178, 'Saint John Capesterre', 'SJC', 1, '0000-00-00', '0000-00-00', NULL),
(2827, 178, 'Saint John Figtree', 'SJF', 1, '0000-00-00', '0000-00-00', NULL),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1, '0000-00-00', '0000-00-00', NULL),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1, '0000-00-00', '0000-00-00', NULL),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1, '0000-00-00', '0000-00-00', NULL),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1, '0000-00-00', '0000-00-00', NULL),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1, '0000-00-00', '0000-00-00', NULL),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1, '0000-00-00', '0000-00-00', NULL),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1, '0000-00-00', '0000-00-00', NULL),
(2835, 179, 'Anse-la-Raye', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2836, 179, 'Castries', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2837, 179, 'Choiseul', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2838, 179, 'Dauphin', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(2839, 179, 'Dennery', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(2840, 179, 'Gros-Islet', 'GI', 1, '0000-00-00', '0000-00-00', NULL),
(2841, 179, 'Laborie', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(2842, 179, 'Micoud', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(2843, 179, 'Praslin', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(2844, 179, 'Soufriere', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(2845, 179, 'Vieux-Fort', 'VF', 1, '0000-00-00', '0000-00-00', NULL),
(2846, 180, 'Charlotte', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(2847, 180, 'Grenadines', 'R', 1, '0000-00-00', '0000-00-00', NULL),
(2848, 180, 'Saint Andrew', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(2849, 180, 'Saint David', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(2850, 180, 'Saint George', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(2851, 180, 'Saint Patrick', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2852, 181, 'A\'ana', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(2854, 181, 'Atua', 'AT', 1, '0000-00-00', '0000-00-00', NULL),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(2856, 181, 'Gaga\'emauga', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(2857, 181, 'Gagaifomauga', 'GF', 1, '0000-00-00', '0000-00-00', NULL),
(2858, 181, 'Palauli', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(2859, 181, 'Satupa\'itea', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2860, 181, 'Tuamasaga', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1, '0000-00-00', '0000-00-00', NULL),
(2862, 181, 'Vaisigano', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(2863, 182, 'Acquaviva', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(2864, 182, 'Borgo Maggiore', 'BM', 1, '0000-00-00', '0000-00-00', NULL),
(2865, 182, 'Chiesanuova', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2866, 182, 'Domagnano', 'DO', 1, '0000-00-00', '0000-00-00', NULL),
(2867, 182, 'Faetano', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(2868, 182, 'Fiorentino', 'FI', 1, '0000-00-00', '0000-00-00', NULL),
(2869, 182, 'Montegiardino', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(2870, 182, 'Citta di San Marino', 'SM', 1, '0000-00-00', '0000-00-00', NULL),
(2871, 182, 'Serravalle', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(2872, 183, 'Sao Tome', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(2873, 183, 'Principe', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(2874, 184, 'Al Bahah', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1, '0000-00-00', '0000-00-00', NULL),
(2876, 184, 'Al Jawf', 'JF', 1, '0000-00-00', '0000-00-00', NULL),
(2877, 184, 'Al Madinah', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(2878, 184, 'Al Qasim', 'QS', 1, '0000-00-00', '0000-00-00', NULL),
(2879, 184, 'Ar Riyad', 'RD', 1, '0000-00-00', '0000-00-00', NULL),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1, '0000-00-00', '0000-00-00', NULL),
(2881, 184, '\'Asir', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2882, 184, 'Ha\'il', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(2883, 184, 'Jizan', 'JZ', 1, '0000-00-00', '0000-00-00', NULL),
(2884, 184, 'Makkah', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(2885, 184, 'Najran', 'NR', 1, '0000-00-00', '0000-00-00', NULL),
(2886, 184, 'Tabuk', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(2887, 185, 'Dakar', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(2888, 185, 'Diourbel', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(2889, 185, 'Fatick', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(2890, 185, 'Kaolack', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(2891, 185, 'Kolda', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2892, 185, 'Louga', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(2893, 185, 'Matam', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(2894, 185, 'Saint-Louis', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(2895, 185, 'Tambacounda', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2896, 185, 'Thies', 'TH', 1, '0000-00-00', '0000-00-00', NULL),
(2897, 185, 'Ziguinchor', 'ZI', 1, '0000-00-00', '0000-00-00', NULL),
(2898, 186, 'Anse aux Pins', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(2899, 186, 'Anse Boileau', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(2900, 186, 'Anse Etoile', 'AE', 1, '0000-00-00', '0000-00-00', NULL),
(2901, 186, 'Anse Louis', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(2902, 186, 'Anse Royale', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(2903, 186, 'Baie Lazare', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(2904, 186, 'Baie Sainte Anne', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(2905, 186, 'Beau Vallon', 'BV', 1, '0000-00-00', '0000-00-00', NULL),
(2906, 186, 'Bel Air', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2907, 186, 'Bel Ombre', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(2908, 186, 'Cascade', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2909, 186, 'Glacis', 'GL', 1, '0000-00-00', '0000-00-00', NULL),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1, '0000-00-00', '0000-00-00', NULL),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1, '0000-00-00', '0000-00-00', NULL),
(2912, 186, 'La Digue', 'DG', 1, '0000-00-00', '0000-00-00', NULL),
(2913, 186, 'La Riviere Anglaise', 'RA', 1, '0000-00-00', '0000-00-00', NULL),
(2914, 186, 'Mont Buxton', 'MB', 1, '0000-00-00', '0000-00-00', NULL),
(2915, 186, 'Mont Fleuri', 'MF', 1, '0000-00-00', '0000-00-00', NULL),
(2916, 186, 'Plaisance', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(2917, 186, 'Pointe La Rue', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(2918, 186, 'Port Glaud', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(2919, 186, 'Saint Louis', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(2920, 186, 'Takamaka', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(2921, 187, 'Eastern', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(2922, 187, 'Northern', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(2923, 187, 'Southern', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(2924, 187, 'Western', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(2925, 189, 'Banskobystrický', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2926, 189, 'Bratislavský', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2927, 189, 'Košický', 'KO', 1, '0000-00-00', '0000-00-00', NULL),
(2928, 189, 'Nitriansky', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(2929, 189, 'Prešovský', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(2930, 189, 'Trenčiansky', 'TC', 1, '0000-00-00', '0000-00-00', NULL),
(2931, 189, 'Trnavský', 'TV', 1, '0000-00-00', '0000-00-00', NULL),
(2932, 189, 'Žilinský', 'ZI', 1, '0000-00-00', '0000-00-00', NULL),
(2933, 191, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(2934, 191, 'Choiseul', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(2935, 191, 'Guadalcanal', 'GC', 1, '0000-00-00', '0000-00-00', NULL),
(2936, 191, 'Honiara', 'HO', 1, '0000-00-00', '0000-00-00', NULL),
(2937, 191, 'Isabel', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(2938, 191, 'Makira', 'MK', 1, '0000-00-00', '0000-00-00', NULL),
(2939, 191, 'Malaita', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(2940, 191, 'Rennell and Bellona', 'RB', 1, '0000-00-00', '0000-00-00', NULL),
(2941, 191, 'Temotu', 'TM', 1, '0000-00-00', '0000-00-00', NULL),
(2942, 191, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(2943, 192, 'Awdal', 'AW', 1, '0000-00-00', '0000-00-00', NULL),
(2944, 192, 'Bakool', 'BK', 1, '0000-00-00', '0000-00-00', NULL),
(2945, 192, 'Banaadir', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(2946, 192, 'Bari', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(2947, 192, 'Bay', 'BY', 1, '0000-00-00', '0000-00-00', NULL),
(2948, 192, 'Galguduud', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(2949, 192, 'Gedo', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(2950, 192, 'Hiiraan', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(2951, 192, 'Jubbada Dhexe', 'JD', 1, '0000-00-00', '0000-00-00', NULL),
(2952, 192, 'Jubbada Hoose', 'JH', 1, '0000-00-00', '0000-00-00', NULL),
(2953, 192, 'Mudug', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(2954, 192, 'Nugaal', 'NU', 1, '0000-00-00', '0000-00-00', NULL),
(2955, 192, 'Sanaag', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(2958, 192, 'Sool', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(2959, 192, 'Togdheer', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1, '0000-00-00', '0000-00-00', NULL),
(2961, 193, 'Eastern Cape', 'EC', 1, '0000-00-00', '0000-00-00', NULL),
(2962, 193, 'Free State', 'FS', 1, '0000-00-00', '0000-00-00', NULL),
(2963, 193, 'Gauteng', 'GT', 1, '0000-00-00', '0000-00-00', NULL),
(2964, 193, 'KwaZulu-Natal', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(2965, 193, 'Limpopo', 'LP', 1, '0000-00-00', '0000-00-00', NULL),
(2966, 193, 'Mpumalanga', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(2967, 193, 'North West', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(2968, 193, 'Northern Cape', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(2969, 193, 'Western Cape', 'WC', 1, '0000-00-00', '0000-00-00', NULL),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(2971, 195, '&Aacute;lava', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(2972, 195, 'Albacete', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(2973, 195, 'Alicante', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(2974, 195, 'Almeria', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(2975, 195, 'Asturias', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(2976, 195, '&Aacute;vila', 'AV', 1, '0000-00-00', '0000-00-00', NULL),
(2977, 195, 'Badajoz', 'BJ', 1, '0000-00-00', '0000-00-00', NULL),
(2978, 195, 'Baleares', 'IB', 1, '0000-00-00', '0000-00-00', NULL),
(2979, 195, 'Barcelona', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(2980, 195, 'Burgos', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(2981, 195, 'C&aacute;ceres', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(2982, 195, 'C&aacute;diz', 'CZ', 1, '0000-00-00', '0000-00-00', NULL),
(2983, 195, 'Cantabria', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(2984, 195, 'Castell&oacute;n', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(2985, 195, 'Ceuta', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(2986, 195, 'Ciudad Real', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(2987, 195, 'C&oacute;rdoba', 'CD', 1, '0000-00-00', '0000-00-00', NULL),
(2988, 195, 'Cuenca', 'CU', 1, '0000-00-00', '0000-00-00', NULL),
(2989, 195, 'Girona', 'GI', 1, '0000-00-00', '0000-00-00', NULL),
(2990, 195, 'Granada', 'GD', 1, '0000-00-00', '0000-00-00', NULL),
(2991, 195, 'Guadalajara', 'GJ', 1, '0000-00-00', '0000-00-00', NULL),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1, '0000-00-00', '0000-00-00', NULL),
(2993, 195, 'Huelva', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(2994, 195, 'Huesca', 'HS', 1, '0000-00-00', '0000-00-00', NULL),
(2995, 195, 'Ja&eacute;n', 'JN', 1, '0000-00-00', '0000-00-00', NULL),
(2996, 195, 'La Rioja', 'RJ', 1, '0000-00-00', '0000-00-00', NULL),
(2997, 195, 'Las Palmas', 'PM', 1, '0000-00-00', '0000-00-00', NULL),
(2998, 195, 'Leon', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(2999, 195, 'Lleida', 'LL', 1, '0000-00-00', '0000-00-00', NULL),
(3000, 195, 'Lugo', 'LG', 1, '0000-00-00', '0000-00-00', NULL),
(3001, 195, 'Madrid', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(3002, 195, 'Malaga', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3003, 195, 'Melilla', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(3004, 195, 'Murcia', 'MU', 1, '0000-00-00', '0000-00-00', NULL),
(3005, 195, 'Navarra', 'NV', 1, '0000-00-00', '0000-00-00', NULL),
(3006, 195, 'Ourense', 'OU', 1, '0000-00-00', '0000-00-00', NULL),
(3007, 195, 'Palencia', 'PL', 1, '0000-00-00', '0000-00-00', NULL),
(3008, 195, 'Pontevedra', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(3009, 195, 'Salamanca', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(3011, 195, 'Segovia', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(3012, 195, 'Sevilla', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(3013, 195, 'Soria', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3014, 195, 'Tarragona', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3015, 195, 'Teruel', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(3016, 195, 'Toledo', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3017, 195, 'Valencia', 'VC', 1, '0000-00-00', '0000-00-00', NULL),
(3018, 195, 'Valladolid', 'VD', 1, '0000-00-00', '0000-00-00', NULL),
(3019, 195, 'Vizcaya', 'VZ', 1, '0000-00-00', '0000-00-00', NULL),
(3020, 195, 'Zamora', 'ZM', 1, '0000-00-00', '0000-00-00', NULL),
(3021, 195, 'Zaragoza', 'ZR', 1, '0000-00-00', '0000-00-00', NULL),
(3022, 196, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(3023, 196, 'Eastern', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(3024, 196, 'North Central', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(3025, 196, 'Northern', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(3026, 196, 'North Western', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(3027, 196, 'Sabaragamuwa', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(3028, 196, 'Southern', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3029, 196, 'Uva', 'UV', 1, '0000-00-00', '0000-00-00', NULL),
(3030, 196, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(3032, 197, 'Saint Helena', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(3034, 199, 'A\'ali an Nil', 'ANL', 1, '0000-00-00', '0000-00-00', NULL),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1, '0000-00-00', '0000-00-00', NULL),
(3036, 199, 'Al Buhayrat', 'BRT', 1, '0000-00-00', '0000-00-00', NULL),
(3037, 199, 'Al Jazirah', 'JZR', 1, '0000-00-00', '0000-00-00', NULL),
(3038, 199, 'Al Khartum', 'KRT', 1, '0000-00-00', '0000-00-00', NULL),
(3039, 199, 'Al Qadarif', 'QDR', 1, '0000-00-00', '0000-00-00', NULL),
(3040, 199, 'Al Wahdah', 'WDH', 1, '0000-00-00', '0000-00-00', NULL),
(3041, 199, 'An Nil al Abyad', 'ANB', 1, '0000-00-00', '0000-00-00', NULL),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1, '0000-00-00', '0000-00-00', NULL),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1, '0000-00-00', '0000-00-00', NULL),
(3044, 199, 'Bahr al Jabal', 'BJA', 1, '0000-00-00', '0000-00-00', NULL),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1, '0000-00-00', '0000-00-00', NULL),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1, '0000-00-00', '0000-00-00', NULL),
(3047, 199, 'Gharb Darfur', 'GDA', 1, '0000-00-00', '0000-00-00', NULL),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1, '0000-00-00', '0000-00-00', NULL),
(3049, 199, 'Janub Darfur', 'JDA', 1, '0000-00-00', '0000-00-00', NULL),
(3050, 199, 'Janub Kurdufan', 'JKU', 1, '0000-00-00', '0000-00-00', NULL),
(3051, 199, 'Junqali', 'JQL', 1, '0000-00-00', '0000-00-00', NULL),
(3052, 199, 'Kassala', 'KSL', 1, '0000-00-00', '0000-00-00', NULL),
(3053, 199, 'Nahr an Nil', 'NNL', 1, '0000-00-00', '0000-00-00', NULL),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1, '0000-00-00', '0000-00-00', NULL),
(3055, 199, 'Shamal Darfur', 'SDA', 1, '0000-00-00', '0000-00-00', NULL),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1, '0000-00-00', '0000-00-00', NULL),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1, '0000-00-00', '0000-00-00', NULL),
(3058, 199, 'Sinnar', 'SNR', 1, '0000-00-00', '0000-00-00', NULL),
(3059, 199, 'Warab', 'WRB', 1, '0000-00-00', '0000-00-00', NULL),
(3060, 200, 'Brokopondo', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(3061, 200, 'Commewijne', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(3062, 200, 'Coronie', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(3063, 200, 'Marowijne', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3064, 200, 'Nickerie', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(3065, 200, 'Para', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(3066, 200, 'Paramaribo', 'PM', 1, '0000-00-00', '0000-00-00', NULL),
(3067, 200, 'Saramacca', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(3068, 200, 'Sipaliwini', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(3069, 200, 'Wanica', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(3070, 202, 'Hhohho', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(3071, 202, 'Lubombo', 'L', 1, '0000-00-00', '0000-00-00', NULL),
(3072, 202, 'Manzini', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(3073, 202, 'Shishelweni', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(3074, 203, 'Blekinge', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(3075, 203, 'Dalarna', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(3076, 203, 'G&auml;vleborg', 'X', 1, '0000-00-00', '0000-00-00', NULL),
(3077, 203, 'Gotland', 'I', 1, '0000-00-00', '0000-00-00', NULL),
(3078, 203, 'Halland', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(3079, 203, 'J&auml;mtland', 'Z', 1, '0000-00-00', '0000-00-00', NULL),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1, '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3081, 203, 'Kalmar', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(3082, 203, 'Kronoberg', 'G', 1, '0000-00-00', '0000-00-00', NULL),
(3083, 203, 'Norrbotten', 'BD', 1, '0000-00-00', '0000-00-00', NULL),
(3084, 203, '&Ouml;rebro', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1, '0000-00-00', '0000-00-00', NULL),
(3086, 203, 'Sk&aring;ne', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(3087, 203, 'S&ouml;dermanland', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(3088, 203, 'Stockholm', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(3089, 203, 'Uppsala', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(3090, 203, 'V&auml;rmland', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(3091, 203, 'V&auml;sterbotten', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(3092, 203, 'V&auml;sternorrland', 'Y', 1, '0000-00-00', '0000-00-00', NULL),
(3093, 203, 'V&auml;stmanland', 'U', 1, '0000-00-00', '0000-00-00', NULL),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1, '0000-00-00', '0000-00-00', NULL),
(3095, 204, 'Aargau', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1, '0000-00-00', '0000-00-00', NULL),
(3098, 204, 'Basel-Stadt', 'BS', 1, '0000-00-00', '0000-00-00', NULL),
(3099, 204, 'Basel-Landschaft', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(3100, 204, 'Bern', 'BE', 1, '0000-00-00', '0000-00-00', NULL),
(3101, 204, 'Fribourg', 'FR', 1, '0000-00-00', '0000-00-00', NULL),
(3102, 204, 'Gen&egrave;ve', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(3103, 204, 'Glarus', 'GL', 1, '0000-00-00', '0000-00-00', NULL),
(3104, 204, 'Graub&uuml;nden', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(3105, 204, 'Jura', 'JU', 1, '0000-00-00', '0000-00-00', NULL),
(3106, 204, 'Luzern', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(3108, 204, 'Nidwald', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(3109, 204, 'Obwald', 'OW', 1, '0000-00-00', '0000-00-00', NULL),
(3110, 204, 'St. Gallen', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(3111, 204, 'Schaffhausen', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(3112, 204, 'Schwyz', 'SZ', 1, '0000-00-00', '0000-00-00', NULL),
(3113, 204, 'Solothurn', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3114, 204, 'Thurgau', 'TG', 1, '0000-00-00', '0000-00-00', NULL),
(3115, 204, 'Ticino', 'TI', 1, '0000-00-00', '0000-00-00', NULL),
(3116, 204, 'Uri', 'UR', 1, '0000-00-00', '0000-00-00', NULL),
(3117, 204, 'Valais', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(3118, 204, 'Vaud', 'VD', 1, '0000-00-00', '0000-00-00', NULL),
(3119, 204, 'Zug', 'ZG', 1, '0000-00-00', '0000-00-00', NULL),
(3120, 204, 'Z&uuml;rich', 'ZH', 1, '0000-00-00', '0000-00-00', NULL),
(3121, 205, 'Al Hasakah', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(3123, 205, 'Al Qunaytirah', 'QU', 1, '0000-00-00', '0000-00-00', NULL),
(3124, 205, 'Ar Raqqah', 'RQ', 1, '0000-00-00', '0000-00-00', NULL),
(3125, 205, 'As Suwayda', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(3126, 205, 'Dara', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(3127, 205, 'Dayr az Zawr', 'DZ', 1, '0000-00-00', '0000-00-00', NULL),
(3128, 205, 'Dimashq', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(3129, 205, 'Halab', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(3130, 205, 'Hamah', 'HM', 1, '0000-00-00', '0000-00-00', NULL),
(3131, 205, 'Hims', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(3132, 205, 'Idlib', 'ID', 1, '0000-00-00', '0000-00-00', NULL),
(3133, 205, 'Rif Dimashq', 'RD', 1, '0000-00-00', '0000-00-00', NULL),
(3134, 205, 'Tartus', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3135, 206, 'Chang-hua', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(3136, 206, 'Chia-i', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(3137, 206, 'Hsin-chu', 'HS', 1, '0000-00-00', '0000-00-00', NULL),
(3138, 206, 'Hua-lien', 'HL', 1, '0000-00-00', '0000-00-00', NULL),
(3139, 206, 'I-lan', 'IL', 1, '0000-00-00', '0000-00-00', NULL),
(3140, 206, 'Kao-hsiung county', 'KH', 1, '0000-00-00', '0000-00-00', NULL),
(3141, 206, 'Kin-men', 'KM', 1, '0000-00-00', '0000-00-00', NULL),
(3142, 206, 'Lien-chiang', 'LC', 1, '0000-00-00', '0000-00-00', NULL),
(3143, 206, 'Miao-li', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(3144, 206, 'Nan-t\'ou', 'NT', 1, '0000-00-00', '0000-00-00', NULL),
(3145, 206, 'P\'eng-hu', 'PH', 1, '0000-00-00', '0000-00-00', NULL),
(3146, 206, 'P\'ing-tung', 'PT', 1, '0000-00-00', '0000-00-00', NULL),
(3147, 206, 'T\'ai-chung', 'TG', 1, '0000-00-00', '0000-00-00', NULL),
(3148, 206, 'T\'ai-nan', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3149, 206, 'T\'ai-pei county', 'TP', 1, '0000-00-00', '0000-00-00', NULL),
(3150, 206, 'T\'ai-tung', 'TT', 1, '0000-00-00', '0000-00-00', NULL),
(3151, 206, 'T\'ao-yuan', 'TY', 1, '0000-00-00', '0000-00-00', NULL),
(3152, 206, 'Yun-lin', 'YL', 1, '0000-00-00', '0000-00-00', NULL),
(3153, 206, 'Chia-i city', 'CC', 1, '0000-00-00', '0000-00-00', NULL),
(3154, 206, 'Chi-lung', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(3155, 206, 'Hsin-chu', 'HC', 1, '0000-00-00', '0000-00-00', NULL),
(3156, 206, 'T\'ai-chung', 'TH', 1, '0000-00-00', '0000-00-00', NULL),
(3157, 206, 'T\'ai-nan', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(3158, 206, 'Kao-hsiung city', 'KC', 1, '0000-00-00', '0000-00-00', NULL),
(3159, 206, 'T\'ai-pei city', 'TC', 1, '0000-00-00', '0000-00-00', NULL),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1, '0000-00-00', '0000-00-00', NULL),
(3161, 207, 'Khatlon', 'KT', 1, '0000-00-00', '0000-00-00', NULL),
(3162, 207, 'Sughd', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(3163, 208, 'Arusha', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3164, 208, 'Dar es Salaam', 'DS', 1, '0000-00-00', '0000-00-00', NULL),
(3165, 208, 'Dodoma', 'DO', 1, '0000-00-00', '0000-00-00', NULL),
(3166, 208, 'Iringa', 'IR', 1, '0000-00-00', '0000-00-00', NULL),
(3167, 208, 'Kagera', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(3168, 208, 'Kigoma', 'KI', 1, '0000-00-00', '0000-00-00', NULL),
(3169, 208, 'Kilimanjaro', 'KJ', 1, '0000-00-00', '0000-00-00', NULL),
(3170, 208, 'Lindi', 'LN', 1, '0000-00-00', '0000-00-00', NULL),
(3171, 208, 'Manyara', 'MY', 1, '0000-00-00', '0000-00-00', NULL),
(3172, 208, 'Mara', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(3173, 208, 'Mbeya', 'MB', 1, '0000-00-00', '0000-00-00', NULL),
(3174, 208, 'Morogoro', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3175, 208, 'Mtwara', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(3176, 208, 'Mwanza', 'MW', 1, '0000-00-00', '0000-00-00', NULL),
(3177, 208, 'Pemba North', 'PN', 1, '0000-00-00', '0000-00-00', NULL),
(3178, 208, 'Pemba South', 'PS', 1, '0000-00-00', '0000-00-00', NULL),
(3179, 208, 'Pwani', 'PW', 1, '0000-00-00', '0000-00-00', NULL),
(3180, 208, 'Rukwa', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(3181, 208, 'Ruvuma', 'RV', 1, '0000-00-00', '0000-00-00', NULL),
(3182, 208, 'Shinyanga', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(3183, 208, 'Singida', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(3184, 208, 'Tabora', 'TB', 1, '0000-00-00', '0000-00-00', NULL),
(3185, 208, 'Tanga', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1, '0000-00-00', '0000-00-00', NULL),
(3187, 208, 'Zanzibar North', 'ZN', 1, '0000-00-00', '0000-00-00', NULL),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1, '0000-00-00', '0000-00-00', NULL),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1, '0000-00-00', '0000-00-00', NULL),
(3190, 209, 'Ang Thong', 'Ang Thong', 1, '0000-00-00', '0000-00-00', NULL),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1, '0000-00-00', '0000-00-00', NULL),
(3192, 209, 'Bangkok', 'Bangkok', 1, '0000-00-00', '0000-00-00', NULL),
(3193, 209, 'Buriram', 'Buriram', 1, '0000-00-00', '0000-00-00', NULL),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1, '0000-00-00', '0000-00-00', NULL),
(3195, 209, 'Chai Nat', 'Chai Nat', 1, '0000-00-00', '0000-00-00', NULL),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1, '0000-00-00', '0000-00-00', NULL),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1, '0000-00-00', '0000-00-00', NULL),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1, '0000-00-00', '0000-00-00', NULL),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1, '0000-00-00', '0000-00-00', NULL),
(3200, 209, 'Chon Buri', 'Chon Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3201, 209, 'Chumphon', 'Chumphon', 1, '0000-00-00', '0000-00-00', NULL),
(3202, 209, 'Kalasin', 'Kalasin', 1, '0000-00-00', '0000-00-00', NULL),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1, '0000-00-00', '0000-00-00', NULL),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1, '0000-00-00', '0000-00-00', NULL),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1, '0000-00-00', '0000-00-00', NULL),
(3206, 209, 'Krabi', 'Krabi', 1, '0000-00-00', '0000-00-00', NULL),
(3207, 209, 'Lampang', 'Lampang', 1, '0000-00-00', '0000-00-00', NULL),
(3208, 209, 'Lamphun', 'Lamphun', 1, '0000-00-00', '0000-00-00', NULL),
(3209, 209, 'Loei', 'Loei', 1, '0000-00-00', '0000-00-00', NULL),
(3210, 209, 'Lop Buri', 'Lop Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1, '0000-00-00', '0000-00-00', NULL),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1, '0000-00-00', '0000-00-00', NULL),
(3213, 209, 'Mukdahan', 'Mukdahan', 1, '0000-00-00', '0000-00-00', NULL),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1, '0000-00-00', '0000-00-00', NULL),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1, '0000-00-00', '0000-00-00', NULL),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1, '0000-00-00', '0000-00-00', NULL),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1, '0000-00-00', '0000-00-00', NULL),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1, '0000-00-00', '0000-00-00', NULL),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1, '0000-00-00', '0000-00-00', NULL),
(3220, 209, 'Nan', 'Nan', 1, '0000-00-00', '0000-00-00', NULL),
(3221, 209, 'Narathiwat', 'Narathiwat', 1, '0000-00-00', '0000-00-00', NULL),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1, '0000-00-00', '0000-00-00', NULL),
(3223, 209, 'Nong Khai', 'Nong Khai', 1, '0000-00-00', '0000-00-00', NULL),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1, '0000-00-00', '0000-00-00', NULL),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1, '0000-00-00', '0000-00-00', NULL),
(3226, 209, 'Pattani', 'Pattani', 1, '0000-00-00', '0000-00-00', NULL),
(3227, 209, 'Phangnga', 'Phangnga', 1, '0000-00-00', '0000-00-00', NULL),
(3228, 209, 'Phatthalung', 'Phatthalung', 1, '0000-00-00', '0000-00-00', NULL),
(3229, 209, 'Phayao', 'Phayao', 1, '0000-00-00', '0000-00-00', NULL),
(3230, 209, 'Phetchabun', 'Phetchabun', 1, '0000-00-00', '0000-00-00', NULL),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1, '0000-00-00', '0000-00-00', NULL),
(3232, 209, 'Phichit', 'Phichit', 1, '0000-00-00', '0000-00-00', NULL),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1, '0000-00-00', '0000-00-00', NULL),
(3234, 209, 'Phrae', 'Phrae', 1, '0000-00-00', '0000-00-00', NULL),
(3235, 209, 'Phuket', 'Phuket', 1, '0000-00-00', '0000-00-00', NULL),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1, '0000-00-00', '0000-00-00', NULL),
(3238, 209, 'Ranong', 'Ranong', 1, '0000-00-00', '0000-00-00', NULL),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1, '0000-00-00', '0000-00-00', NULL),
(3240, 209, 'Rayong', 'Rayong', 1, '0000-00-00', '0000-00-00', NULL),
(3241, 209, 'Roi Et', 'Roi Et', 1, '0000-00-00', '0000-00-00', NULL),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1, '0000-00-00', '0000-00-00', NULL),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1, '0000-00-00', '0000-00-00', NULL),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1, '0000-00-00', '0000-00-00', NULL),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1, '0000-00-00', '0000-00-00', NULL),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1, '0000-00-00', '0000-00-00', NULL),
(3247, 209, 'Sara Buri', 'Sara Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3248, 209, 'Satun', 'Satun', 1, '0000-00-00', '0000-00-00', NULL),
(3249, 209, 'Sing Buri', 'Sing Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3250, 209, 'Sisaket', 'Sisaket', 1, '0000-00-00', '0000-00-00', NULL),
(3251, 209, 'Songkhla', 'Songkhla', 1, '0000-00-00', '0000-00-00', NULL),
(3252, 209, 'Sukhothai', 'Sukhothai', 1, '0000-00-00', '0000-00-00', NULL),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1, '0000-00-00', '0000-00-00', NULL),
(3254, 209, 'Surat Thani', 'Surat Thani', 1, '0000-00-00', '0000-00-00', NULL),
(3255, 209, 'Surin', 'Surin', 1, '0000-00-00', '0000-00-00', NULL),
(3256, 209, 'Tak', 'Tak', 1, '0000-00-00', '0000-00-00', NULL),
(3257, 209, 'Trang', 'Trang', 1, '0000-00-00', '0000-00-00', NULL),
(3258, 209, 'Trat', 'Trat', 1, '0000-00-00', '0000-00-00', NULL),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1, '0000-00-00', '0000-00-00', NULL),
(3260, 209, 'Udon Thani', 'Udon Thani', 1, '0000-00-00', '0000-00-00', NULL),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1, '0000-00-00', '0000-00-00', NULL),
(3262, 209, 'Uttaradit', 'Uttaradit', 1, '0000-00-00', '0000-00-00', NULL),
(3263, 209, 'Yala', 'Yala', 1, '0000-00-00', '0000-00-00', NULL),
(3264, 209, 'Yasothon', 'Yasothon', 1, '0000-00-00', '0000-00-00', NULL),
(3265, 210, 'Kara', 'K', 1, '0000-00-00', '0000-00-00', NULL),
(3266, 210, 'Plateaux', 'P', 1, '0000-00-00', '0000-00-00', NULL),
(3267, 210, 'Savanes', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(3268, 210, 'Centrale', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(3269, 210, 'Maritime', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(3270, 211, 'Atafu', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(3271, 211, 'Fakaofo', 'F', 1, '0000-00-00', '0000-00-00', NULL),
(3272, 211, 'Nukunonu', 'N', 1, '0000-00-00', '0000-00-00', NULL),
(3273, 212, 'Ha\'apai', 'H', 1, '0000-00-00', '0000-00-00', NULL),
(3274, 212, 'Tongatapu', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(3275, 212, 'Vava\'u', 'V', 1, '0000-00-00', '0000-00-00', NULL),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(3277, 213, 'Diego Martin', 'DM', 1, '0000-00-00', '0000-00-00', NULL),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(3279, 213, 'Penal/Debe', 'PD', 1, '0000-00-00', '0000-00-00', NULL),
(3280, 213, 'Princes Town', 'PT', 1, '0000-00-00', '0000-00-00', NULL),
(3281, 213, 'Sangre Grande', 'SG', 1, '0000-00-00', '0000-00-00', NULL),
(3282, 213, 'San Juan/Laventille', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(3283, 213, 'Siparia', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1, '0000-00-00', '0000-00-00', NULL),
(3285, 213, 'Port of Spain', 'PS', 1, '0000-00-00', '0000-00-00', NULL),
(3286, 213, 'San Fernando', 'SF', 1, '0000-00-00', '0000-00-00', NULL),
(3287, 213, 'Arima', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3288, 213, 'Point Fortin', 'PF', 1, '0000-00-00', '0000-00-00', NULL),
(3289, 213, 'Chaguanas', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(3290, 213, 'Tobago', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3291, 214, 'Ariana', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3292, 214, 'Beja', 'BJ', 1, '0000-00-00', '0000-00-00', NULL),
(3293, 214, 'Ben Arous', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(3294, 214, 'Bizerte', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(3295, 214, 'Gabes', 'GB', 1, '0000-00-00', '0000-00-00', NULL),
(3296, 214, 'Gafsa', 'GF', 1, '0000-00-00', '0000-00-00', NULL),
(3297, 214, 'Jendouba', 'JE', 1, '0000-00-00', '0000-00-00', NULL),
(3298, 214, 'Kairouan', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(3299, 214, 'Kasserine', 'KS', 1, '0000-00-00', '0000-00-00', NULL),
(3300, 214, 'Kebili', 'KB', 1, '0000-00-00', '0000-00-00', NULL),
(3301, 214, 'Kef', 'KF', 1, '0000-00-00', '0000-00-00', NULL),
(3302, 214, 'Mahdia', 'MH', 1, '0000-00-00', '0000-00-00', NULL),
(3303, 214, 'Manouba', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(3304, 214, 'Medenine', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(3305, 214, 'Monastir', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3306, 214, 'Nabeul', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(3307, 214, 'Sfax', 'SF', 1, '0000-00-00', '0000-00-00', NULL),
(3308, 214, 'Sidi', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(3309, 214, 'Siliana', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(3310, 214, 'Sousse', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3311, 214, 'Tataouine', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3312, 214, 'Tozeur', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3313, 214, 'Tunis', 'TU', 1, '0000-00-00', '0000-00-00', NULL),
(3314, 214, 'Zaghouan', 'ZA', 1, '0000-00-00', '0000-00-00', NULL),
(3315, 215, 'Adana', 'ADA', 1, '0000-00-00', '0000-00-00', NULL),
(3316, 215, 'Adıyaman', 'ADI', 1, '0000-00-00', '0000-00-00', NULL),
(3317, 215, 'Afyonkarahisar', 'AFY', 1, '0000-00-00', '0000-00-00', NULL),
(3318, 215, 'Ağrı', 'AGR', 1, '0000-00-00', '0000-00-00', NULL),
(3319, 215, 'Aksaray', 'AKS', 1, '0000-00-00', '0000-00-00', NULL),
(3320, 215, 'Amasya', 'AMA', 1, '0000-00-00', '0000-00-00', NULL),
(3321, 215, 'Ankara', 'ANK', 1, '0000-00-00', '0000-00-00', NULL),
(3322, 215, 'Antalya', 'ANT', 1, '0000-00-00', '0000-00-00', NULL),
(3323, 215, 'Ardahan', 'ARD', 1, '0000-00-00', '0000-00-00', NULL),
(3324, 215, 'Artvin', 'ART', 1, '0000-00-00', '0000-00-00', NULL),
(3325, 215, 'Aydın', 'AYI', 1, '0000-00-00', '0000-00-00', NULL),
(3326, 215, 'Balıkesir', 'BAL', 1, '0000-00-00', '0000-00-00', NULL),
(3327, 215, 'Bartın', 'BAR', 1, '0000-00-00', '0000-00-00', NULL),
(3328, 215, 'Batman', 'BAT', 1, '0000-00-00', '0000-00-00', NULL),
(3329, 215, 'Bayburt', 'BAY', 1, '0000-00-00', '0000-00-00', NULL),
(3330, 215, 'Bilecik', 'BIL', 1, '0000-00-00', '0000-00-00', NULL),
(3331, 215, 'Bingöl', 'BIN', 1, '0000-00-00', '0000-00-00', NULL),
(3332, 215, 'Bitlis', 'BIT', 1, '0000-00-00', '0000-00-00', NULL),
(3333, 215, 'Bolu', 'BOL', 1, '0000-00-00', '0000-00-00', NULL),
(3334, 215, 'Burdur', 'BRD', 1, '0000-00-00', '0000-00-00', NULL),
(3335, 215, 'Bursa', 'BRS', 1, '0000-00-00', '0000-00-00', NULL),
(3336, 215, 'Çanakkale', 'CKL', 1, '0000-00-00', '0000-00-00', NULL),
(3337, 215, 'Çankırı', 'CKR', 1, '0000-00-00', '0000-00-00', NULL),
(3338, 215, 'Çorum', 'COR', 1, '0000-00-00', '0000-00-00', NULL),
(3339, 215, 'Denizli', 'DEN', 1, '0000-00-00', '0000-00-00', NULL),
(3340, 215, 'Diyarbakır', 'DIY', 1, '0000-00-00', '0000-00-00', NULL),
(3341, 215, 'Düzce', 'DUZ', 1, '0000-00-00', '0000-00-00', NULL),
(3342, 215, 'Edirne', 'EDI', 1, '0000-00-00', '0000-00-00', NULL),
(3343, 215, 'Elazığ', 'ELA', 1, '0000-00-00', '0000-00-00', NULL),
(3344, 215, 'Erzincan', 'EZC', 1, '0000-00-00', '0000-00-00', NULL),
(3345, 215, 'Erzurum', 'EZR', 1, '0000-00-00', '0000-00-00', NULL),
(3346, 215, 'Eskişehir', 'ESK', 1, '0000-00-00', '0000-00-00', NULL),
(3347, 215, 'Gaziantep', 'GAZ', 1, '0000-00-00', '0000-00-00', NULL),
(3348, 215, 'Giresun', 'GIR', 1, '0000-00-00', '0000-00-00', NULL),
(3349, 215, 'Gümüşhane', 'GMS', 1, '0000-00-00', '0000-00-00', NULL),
(3350, 215, 'Hakkari', 'HKR', 1, '0000-00-00', '0000-00-00', NULL),
(3351, 215, 'Hatay', 'HTY', 1, '0000-00-00', '0000-00-00', NULL),
(3352, 215, 'Iğdır', 'IGD', 1, '0000-00-00', '0000-00-00', NULL),
(3353, 215, 'Isparta', 'ISP', 1, '0000-00-00', '0000-00-00', NULL),
(3354, 215, 'İstanbul', 'IST', 1, '0000-00-00', '0000-00-00', NULL),
(3355, 215, 'İzmir', 'IZM', 1, '0000-00-00', '0000-00-00', NULL),
(3356, 215, 'Kahramanmaraş', 'KAH', 1, '0000-00-00', '0000-00-00', NULL),
(3357, 215, 'Karabük', 'KRB', 1, '0000-00-00', '0000-00-00', NULL),
(3358, 215, 'Karaman', 'KRM', 1, '0000-00-00', '0000-00-00', NULL),
(3359, 215, 'Kars', 'KRS', 1, '0000-00-00', '0000-00-00', NULL),
(3360, 215, 'Kastamonu', 'KAS', 1, '0000-00-00', '0000-00-00', NULL),
(3361, 215, 'Kayseri', 'KAY', 1, '0000-00-00', '0000-00-00', NULL),
(3362, 215, 'Kilis', 'KLS', 1, '0000-00-00', '0000-00-00', NULL),
(3363, 215, 'Kırıkkale', 'KRK', 1, '0000-00-00', '0000-00-00', NULL),
(3364, 215, 'Kırklareli', 'KLR', 1, '0000-00-00', '0000-00-00', NULL),
(3365, 215, 'Kırşehir', 'KRH', 1, '0000-00-00', '0000-00-00', NULL),
(3366, 215, 'Kocaeli', 'KOC', 1, '0000-00-00', '0000-00-00', NULL),
(3367, 215, 'Konya', 'KON', 1, '0000-00-00', '0000-00-00', NULL),
(3368, 215, 'Kütahya', 'KUT', 1, '0000-00-00', '0000-00-00', NULL),
(3369, 215, 'Malatya', 'MAL', 1, '0000-00-00', '0000-00-00', NULL),
(3370, 215, 'Manisa', 'MAN', 1, '0000-00-00', '0000-00-00', NULL),
(3371, 215, 'Mardin', 'MAR', 1, '0000-00-00', '0000-00-00', NULL),
(3372, 215, 'Mersin', 'MER', 1, '0000-00-00', '0000-00-00', NULL),
(3373, 215, 'Muğla', 'MUG', 1, '0000-00-00', '0000-00-00', NULL),
(3374, 215, 'Muş', 'MUS', 1, '0000-00-00', '0000-00-00', NULL),
(3375, 215, 'Nevşehir', 'NEV', 1, '0000-00-00', '0000-00-00', NULL),
(3376, 215, 'Niğde', 'NIG', 1, '0000-00-00', '0000-00-00', NULL),
(3377, 215, 'Ordu', 'ORD', 1, '0000-00-00', '0000-00-00', NULL),
(3378, 215, 'Osmaniye', 'OSM', 1, '0000-00-00', '0000-00-00', NULL),
(3379, 215, 'Rize', 'RIZ', 1, '0000-00-00', '0000-00-00', NULL),
(3380, 215, 'Sakarya', 'SAK', 1, '0000-00-00', '0000-00-00', NULL),
(3381, 215, 'Samsun', 'SAM', 1, '0000-00-00', '0000-00-00', NULL),
(3382, 215, 'Şanlıurfa', 'SAN', 1, '0000-00-00', '0000-00-00', NULL),
(3383, 215, 'Siirt', 'SII', 1, '0000-00-00', '0000-00-00', NULL),
(3384, 215, 'Sinop', 'SIN', 1, '0000-00-00', '0000-00-00', NULL),
(3385, 215, 'Şırnak', 'SIR', 1, '0000-00-00', '0000-00-00', NULL),
(3386, 215, 'Sivas', 'SIV', 1, '0000-00-00', '0000-00-00', NULL),
(3387, 215, 'Tekirdağ', 'TEL', 1, '0000-00-00', '0000-00-00', NULL),
(3388, 215, 'Tokat', 'TOK', 1, '0000-00-00', '0000-00-00', NULL),
(3389, 215, 'Trabzon', 'TRA', 1, '0000-00-00', '0000-00-00', NULL),
(3390, 215, 'Tunceli', 'TUN', 1, '0000-00-00', '0000-00-00', NULL),
(3391, 215, 'Uşak', 'USK', 1, '0000-00-00', '0000-00-00', NULL),
(3392, 215, 'Van', 'VAN', 1, '0000-00-00', '0000-00-00', NULL),
(3393, 215, 'Yalova', 'YAL', 1, '0000-00-00', '0000-00-00', NULL),
(3394, 215, 'Yozgat', 'YOZ', 1, '0000-00-00', '0000-00-00', NULL),
(3395, 215, 'Zonguldak', 'ZON', 1, '0000-00-00', '0000-00-00', NULL),
(3396, 216, 'Ahal Welayaty', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(3397, 216, 'Balkan Welayaty', 'B', 1, '0000-00-00', '0000-00-00', NULL),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1, '0000-00-00', '0000-00-00', NULL),
(3399, 216, 'Lebap Welayaty', 'L', 1, '0000-00-00', '0000-00-00', NULL),
(3400, 216, 'Mary Welayaty', 'M', 1, '0000-00-00', '0000-00-00', NULL),
(3401, 217, 'Ambergris Cays', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(3402, 217, 'Dellis Cay', 'DC', 1, '0000-00-00', '0000-00-00', NULL),
(3403, 217, 'French Cay', 'FC', 1, '0000-00-00', '0000-00-00', NULL),
(3404, 217, 'Little Water Cay', 'LW', 1, '0000-00-00', '0000-00-00', NULL),
(3405, 217, 'Parrot Cay', 'RC', 1, '0000-00-00', '0000-00-00', NULL),
(3406, 217, 'Pine Cay', 'PN', 1, '0000-00-00', '0000-00-00', NULL),
(3407, 217, 'Salt Cay', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(3408, 217, 'Grand Turk', 'GT', 1, '0000-00-00', '0000-00-00', NULL),
(3409, 217, 'South Caicos', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(3410, 217, 'East Caicos', 'EC', 1, '0000-00-00', '0000-00-00', NULL),
(3411, 217, 'Middle Caicos', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(3412, 217, 'North Caicos', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(3413, 217, 'Providenciales', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(3414, 217, 'West Caicos', 'WC', 1, '0000-00-00', '0000-00-00', NULL),
(3415, 218, 'Nanumanga', 'NMG', 1, '0000-00-00', '0000-00-00', NULL),
(3416, 218, 'Niulakita', 'NLK', 1, '0000-00-00', '0000-00-00', NULL),
(3417, 218, 'Niutao', 'NTO', 1, '0000-00-00', '0000-00-00', NULL),
(3418, 218, 'Funafuti', 'FUN', 1, '0000-00-00', '0000-00-00', NULL),
(3419, 218, 'Nanumea', 'NME', 1, '0000-00-00', '0000-00-00', NULL),
(3420, 218, 'Nui', 'NUI', 1, '0000-00-00', '0000-00-00', NULL),
(3421, 218, 'Nukufetau', 'NFT', 1, '0000-00-00', '0000-00-00', NULL),
(3422, 218, 'Nukulaelae', 'NLL', 1, '0000-00-00', '0000-00-00', NULL),
(3423, 218, 'Vaitupu', 'VAI', 1, '0000-00-00', '0000-00-00', NULL),
(3424, 219, 'Kalangala', 'KAL', 1, '0000-00-00', '0000-00-00', NULL),
(3425, 219, 'Kampala', 'KMP', 1, '0000-00-00', '0000-00-00', NULL),
(3426, 219, 'Kayunga', 'KAY', 1, '0000-00-00', '0000-00-00', NULL),
(3427, 219, 'Kiboga', 'KIB', 1, '0000-00-00', '0000-00-00', NULL),
(3428, 219, 'Luwero', 'LUW', 1, '0000-00-00', '0000-00-00', NULL),
(3429, 219, 'Masaka', 'MAS', 1, '0000-00-00', '0000-00-00', NULL),
(3430, 219, 'Mpigi', 'MPI', 1, '0000-00-00', '0000-00-00', NULL),
(3431, 219, 'Mubende', 'MUB', 1, '0000-00-00', '0000-00-00', NULL),
(3432, 219, 'Mukono', 'MUK', 1, '0000-00-00', '0000-00-00', NULL),
(3433, 219, 'Nakasongola', 'NKS', 1, '0000-00-00', '0000-00-00', NULL),
(3434, 219, 'Rakai', 'RAK', 1, '0000-00-00', '0000-00-00', NULL),
(3435, 219, 'Sembabule', 'SEM', 1, '0000-00-00', '0000-00-00', NULL),
(3436, 219, 'Wakiso', 'WAK', 1, '0000-00-00', '0000-00-00', NULL),
(3437, 219, 'Bugiri', 'BUG', 1, '0000-00-00', '0000-00-00', NULL),
(3438, 219, 'Busia', 'BUS', 1, '0000-00-00', '0000-00-00', NULL),
(3439, 219, 'Iganga', 'IGA', 1, '0000-00-00', '0000-00-00', NULL),
(3440, 219, 'Jinja', 'JIN', 1, '0000-00-00', '0000-00-00', NULL),
(3441, 219, 'Kaberamaido', 'KAB', 1, '0000-00-00', '0000-00-00', NULL),
(3442, 219, 'Kamuli', 'KML', 1, '0000-00-00', '0000-00-00', NULL),
(3443, 219, 'Kapchorwa', 'KPC', 1, '0000-00-00', '0000-00-00', NULL),
(3444, 219, 'Katakwi', 'KTK', 1, '0000-00-00', '0000-00-00', NULL),
(3445, 219, 'Kumi', 'KUM', 1, '0000-00-00', '0000-00-00', NULL),
(3446, 219, 'Mayuge', 'MAY', 1, '0000-00-00', '0000-00-00', NULL),
(3447, 219, 'Mbale', 'MBA', 1, '0000-00-00', '0000-00-00', NULL),
(3448, 219, 'Pallisa', 'PAL', 1, '0000-00-00', '0000-00-00', NULL),
(3449, 219, 'Sironko', 'SIR', 1, '0000-00-00', '0000-00-00', NULL),
(3450, 219, 'Soroti', 'SOR', 1, '0000-00-00', '0000-00-00', NULL),
(3451, 219, 'Tororo', 'TOR', 1, '0000-00-00', '0000-00-00', NULL),
(3452, 219, 'Adjumani', 'ADJ', 1, '0000-00-00', '0000-00-00', NULL),
(3453, 219, 'Apac', 'APC', 1, '0000-00-00', '0000-00-00', NULL),
(3454, 219, 'Arua', 'ARU', 1, '0000-00-00', '0000-00-00', NULL),
(3455, 219, 'Gulu', 'GUL', 1, '0000-00-00', '0000-00-00', NULL),
(3456, 219, 'Kitgum', 'KIT', 1, '0000-00-00', '0000-00-00', NULL),
(3457, 219, 'Kotido', 'KOT', 1, '0000-00-00', '0000-00-00', NULL),
(3458, 219, 'Lira', 'LIR', 1, '0000-00-00', '0000-00-00', NULL),
(3459, 219, 'Moroto', 'MRT', 1, '0000-00-00', '0000-00-00', NULL),
(3460, 219, 'Moyo', 'MOY', 1, '0000-00-00', '0000-00-00', NULL),
(3461, 219, 'Nakapiripirit', 'NAK', 1, '0000-00-00', '0000-00-00', NULL),
(3462, 219, 'Nebbi', 'NEB', 1, '0000-00-00', '0000-00-00', NULL),
(3463, 219, 'Pader', 'PAD', 1, '0000-00-00', '0000-00-00', NULL),
(3464, 219, 'Yumbe', 'YUM', 1, '0000-00-00', '0000-00-00', NULL),
(3465, 219, 'Bundibugyo', 'BUN', 1, '0000-00-00', '0000-00-00', NULL),
(3466, 219, 'Bushenyi', 'BSH', 1, '0000-00-00', '0000-00-00', NULL),
(3467, 219, 'Hoima', 'HOI', 1, '0000-00-00', '0000-00-00', NULL),
(3468, 219, 'Kabale', 'KBL', 1, '0000-00-00', '0000-00-00', NULL),
(3469, 219, 'Kabarole', 'KAR', 1, '0000-00-00', '0000-00-00', NULL),
(3470, 219, 'Kamwenge', 'KAM', 1, '0000-00-00', '0000-00-00', NULL),
(3471, 219, 'Kanungu', 'KAN', 1, '0000-00-00', '0000-00-00', NULL),
(3472, 219, 'Kasese', 'KAS', 1, '0000-00-00', '0000-00-00', NULL),
(3473, 219, 'Kibaale', 'KBA', 1, '0000-00-00', '0000-00-00', NULL),
(3474, 219, 'Kisoro', 'KIS', 1, '0000-00-00', '0000-00-00', NULL),
(3475, 219, 'Kyenjojo', 'KYE', 1, '0000-00-00', '0000-00-00', NULL),
(3476, 219, 'Masindi', 'MSN', 1, '0000-00-00', '0000-00-00', NULL),
(3477, 219, 'Mbarara', 'MBR', 1, '0000-00-00', '0000-00-00', NULL),
(3478, 219, 'Ntungamo', 'NTU', 1, '0000-00-00', '0000-00-00', NULL),
(3479, 219, 'Rukungiri', 'RUK', 1, '0000-00-00', '0000-00-00', NULL),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1, '0000-00-00', '0000-00-00', NULL),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1, '0000-00-00', '0000-00-00', NULL),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1, '0000-00-00', '0000-00-00', NULL),
(3483, 220, 'Crimea', '43', 1, '0000-00-00', '0000-00-00', NULL),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1, '0000-00-00', '0000-00-00', NULL),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1, '0000-00-00', '0000-00-00', NULL),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1, '0000-00-00', '0000-00-00', NULL),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1, '0000-00-00', '0000-00-00', NULL),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1, '0000-00-00', '0000-00-00', NULL),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1, '0000-00-00', '0000-00-00', NULL),
(3490, 220, 'Kyiv', '30', 1, '0000-00-00', '0000-00-00', NULL),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1, '0000-00-00', '0000-00-00', NULL),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1, '0000-00-00', '0000-00-00', NULL),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1, '0000-00-00', '0000-00-00', NULL),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1, '0000-00-00', '0000-00-00', NULL),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1, '0000-00-00', '0000-00-00', NULL),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1, '0000-00-00', '0000-00-00', NULL),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1, '0000-00-00', '0000-00-00', NULL),
(3498, 220, 'Sevastopol\'', '40', 1, '0000-00-00', '0000-00-00', NULL),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1, '0000-00-00', '0000-00-00', NULL),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1, '0000-00-00', '0000-00-00', NULL),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1, '0000-00-00', '0000-00-00', NULL),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1, '0000-00-00', '0000-00-00', NULL),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1, '0000-00-00', '0000-00-00', NULL),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1, '0000-00-00', '0000-00-00', NULL),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1, '0000-00-00', '0000-00-00', NULL),
(3506, 221, 'Abu Dhabi', 'ADH', 1, '0000-00-00', '0000-00-00', NULL),
(3507, 221, '\'Ajman', 'AJ', 1, '0000-00-00', '0000-00-00', NULL),
(3508, 221, 'Al Fujayrah', 'FU', 1, '0000-00-00', '0000-00-00', NULL),
(3509, 221, 'Ash Shariqah', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(3510, 221, 'Dubai', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(3511, 221, 'R\'as al Khaymah', 'RK', 1, '0000-00-00', '0000-00-00', NULL),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1, '0000-00-00', '0000-00-00', NULL),
(3513, 222, 'Aberdeen', 'ABN', 1, '0000-00-00', '0000-00-00', NULL),
(3514, 222, 'Aberdeenshire', 'ABNS', 1, '0000-00-00', '0000-00-00', NULL),
(3515, 222, 'Anglesey', 'ANG', 1, '0000-00-00', '0000-00-00', NULL),
(3516, 222, 'Angus', 'AGS', 1, '0000-00-00', '0000-00-00', NULL),
(3517, 222, 'Argyll and Bute', 'ARY', 1, '0000-00-00', '0000-00-00', NULL),
(3518, 222, 'Bedfordshire', 'BEDS', 1, '0000-00-00', '0000-00-00', NULL),
(3519, 222, 'Berkshire', 'BERKS', 1, '0000-00-00', '0000-00-00', NULL),
(3520, 222, 'Blaenau Gwent', 'BLA', 1, '0000-00-00', '0000-00-00', NULL),
(3521, 222, 'Bridgend', 'BRI', 1, '0000-00-00', '0000-00-00', NULL),
(3522, 222, 'Bristol', 'BSTL', 1, '0000-00-00', '0000-00-00', NULL),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1, '0000-00-00', '0000-00-00', NULL),
(3524, 222, 'Caerphilly', 'CAE', 1, '0000-00-00', '0000-00-00', NULL),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1, '0000-00-00', '0000-00-00', NULL),
(3526, 222, 'Cardiff', 'CDF', 1, '0000-00-00', '0000-00-00', NULL),
(3527, 222, 'Carmarthenshire', 'CARM', 1, '0000-00-00', '0000-00-00', NULL),
(3528, 222, 'Ceredigion', 'CDGN', 1, '0000-00-00', '0000-00-00', NULL),
(3529, 222, 'Cheshire', 'CHES', 1, '0000-00-00', '0000-00-00', NULL),
(3530, 222, 'Clackmannanshire', 'CLACK', 1, '0000-00-00', '0000-00-00', NULL),
(3531, 222, 'Conwy', 'CON', 1, '0000-00-00', '0000-00-00', NULL),
(3532, 222, 'Cornwall', 'CORN', 1, '0000-00-00', '0000-00-00', NULL),
(3533, 222, 'Denbighshire', 'DNBG', 1, '0000-00-00', '0000-00-00', NULL),
(3534, 222, 'Derbyshire', 'DERBY', 1, '0000-00-00', '0000-00-00', NULL),
(3535, 222, 'Devon', 'DVN', 1, '0000-00-00', '0000-00-00', NULL),
(3536, 222, 'Dorset', 'DOR', 1, '0000-00-00', '0000-00-00', NULL),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1, '0000-00-00', '0000-00-00', NULL),
(3538, 222, 'Dundee', 'DUND', 1, '0000-00-00', '0000-00-00', NULL),
(3539, 222, 'Durham', 'DHM', 1, '0000-00-00', '0000-00-00', NULL),
(3540, 222, 'East Ayrshire', 'ARYE', 1, '0000-00-00', '0000-00-00', NULL),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1, '0000-00-00', '0000-00-00', NULL),
(3542, 222, 'East Lothian', 'LOTE', 1, '0000-00-00', '0000-00-00', NULL),
(3543, 222, 'East Renfrewshire', 'RENE', 1, '0000-00-00', '0000-00-00', NULL),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1, '0000-00-00', '0000-00-00', NULL),
(3545, 222, 'East Sussex', 'SXE', 1, '0000-00-00', '0000-00-00', NULL),
(3546, 222, 'Edinburgh', 'EDIN', 1, '0000-00-00', '0000-00-00', NULL),
(3547, 222, 'Essex', 'ESX', 1, '0000-00-00', '0000-00-00', NULL),
(3548, 222, 'Falkirk', 'FALK', 1, '0000-00-00', '0000-00-00', NULL),
(3549, 222, 'Fife', 'FFE', 1, '0000-00-00', '0000-00-00', NULL),
(3550, 222, 'Flintshire', 'FLINT', 1, '0000-00-00', '0000-00-00', NULL),
(3551, 222, 'Glasgow', 'GLAS', 1, '0000-00-00', '0000-00-00', NULL),
(3552, 222, 'Gloucestershire', 'GLOS', 1, '0000-00-00', '0000-00-00', NULL),
(3553, 222, 'Greater London', 'LDN', 1, '0000-00-00', '0000-00-00', NULL),
(3554, 222, 'Greater Manchester', 'MCH', 1, '0000-00-00', '0000-00-00', NULL),
(3555, 222, 'Gwynedd', 'GDD', 1, '0000-00-00', '0000-00-00', NULL),
(3556, 222, 'Hampshire', 'HANTS', 1, '0000-00-00', '0000-00-00', NULL),
(3557, 222, 'Herefordshire', 'HWR', 1, '0000-00-00', '0000-00-00', NULL),
(3558, 222, 'Hertfordshire', 'HERTS', 1, '0000-00-00', '0000-00-00', NULL),
(3559, 222, 'Highlands', 'HLD', 1, '0000-00-00', '0000-00-00', NULL),
(3560, 222, 'Inverclyde', 'IVER', 1, '0000-00-00', '0000-00-00', NULL),
(3561, 222, 'Isle of Wight', 'IOW', 1, '0000-00-00', '0000-00-00', NULL),
(3562, 222, 'Kent', 'KNT', 1, '0000-00-00', '0000-00-00', NULL),
(3563, 222, 'Lancashire', 'LANCS', 1, '0000-00-00', '0000-00-00', NULL),
(3564, 222, 'Leicestershire', 'LEICS', 1, '0000-00-00', '0000-00-00', NULL),
(3565, 222, 'Lincolnshire', 'LINCS', 1, '0000-00-00', '0000-00-00', NULL),
(3566, 222, 'Merseyside', 'MSY', 1, '0000-00-00', '0000-00-00', NULL),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1, '0000-00-00', '0000-00-00', NULL),
(3568, 222, 'Midlothian', 'MLOT', 1, '0000-00-00', '0000-00-00', NULL),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1, '0000-00-00', '0000-00-00', NULL),
(3570, 222, 'Moray', 'MORAY', 1, '0000-00-00', '0000-00-00', NULL),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1, '0000-00-00', '0000-00-00', NULL),
(3572, 222, 'Newport', 'NEWPT', 1, '0000-00-00', '0000-00-00', NULL),
(3573, 222, 'Norfolk', 'NOR', 1, '0000-00-00', '0000-00-00', NULL),
(3574, 222, 'North Ayrshire', 'ARYN', 1, '0000-00-00', '0000-00-00', NULL),
(3575, 222, 'North Lanarkshire', 'LANN', 1, '0000-00-00', '0000-00-00', NULL),
(3576, 222, 'North Yorkshire', 'YSN', 1, '0000-00-00', '0000-00-00', NULL),
(3577, 222, 'Northamptonshire', 'NHM', 1, '0000-00-00', '0000-00-00', NULL),
(3578, 222, 'Northumberland', 'NLD', 1, '0000-00-00', '0000-00-00', NULL),
(3579, 222, 'Nottinghamshire', 'NOT', 1, '0000-00-00', '0000-00-00', NULL),
(3580, 222, 'Orkney Islands', 'ORK', 1, '0000-00-00', '0000-00-00', NULL),
(3581, 222, 'Oxfordshire', 'OFE', 1, '0000-00-00', '0000-00-00', NULL),
(3582, 222, 'Pembrokeshire', 'PEM', 1, '0000-00-00', '0000-00-00', NULL),
(3583, 222, 'Perth and Kinross', 'PERTH', 1, '0000-00-00', '0000-00-00', NULL),
(3584, 222, 'Powys', 'PWS', 1, '0000-00-00', '0000-00-00', NULL),
(3585, 222, 'Renfrewshire', 'REN', 1, '0000-00-00', '0000-00-00', NULL),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1, '0000-00-00', '0000-00-00', NULL),
(3587, 222, 'Rutland', 'RUT', 1, '0000-00-00', '0000-00-00', NULL),
(3588, 222, 'Scottish Borders', 'BOR', 1, '0000-00-00', '0000-00-00', NULL),
(3589, 222, 'Shetland Islands', 'SHET', 1, '0000-00-00', '0000-00-00', NULL),
(3590, 222, 'Shropshire', 'SPE', 1, '0000-00-00', '0000-00-00', NULL),
(3591, 222, 'Somerset', 'SOM', 1, '0000-00-00', '0000-00-00', NULL),
(3592, 222, 'South Ayrshire', 'ARYS', 1, '0000-00-00', '0000-00-00', NULL),
(3593, 222, 'South Lanarkshire', 'LANS', 1, '0000-00-00', '0000-00-00', NULL),
(3594, 222, 'South Yorkshire', 'YSS', 1, '0000-00-00', '0000-00-00', NULL),
(3595, 222, 'Staffordshire', 'SFD', 1, '0000-00-00', '0000-00-00', NULL),
(3596, 222, 'Stirling', 'STIR', 1, '0000-00-00', '0000-00-00', NULL),
(3597, 222, 'Suffolk', 'SFK', 1, '0000-00-00', '0000-00-00', NULL),
(3598, 222, 'Surrey', 'SRY', 1, '0000-00-00', '0000-00-00', NULL),
(3599, 222, 'Swansea', 'SWAN', 1, '0000-00-00', '0000-00-00', NULL),
(3600, 222, 'Torfaen', 'TORF', 1, '0000-00-00', '0000-00-00', NULL),
(3601, 222, 'Tyne and Wear', 'TWR', 1, '0000-00-00', '0000-00-00', NULL),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1, '0000-00-00', '0000-00-00', NULL),
(3603, 222, 'Warwickshire', 'WARKS', 1, '0000-00-00', '0000-00-00', NULL),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1, '0000-00-00', '0000-00-00', NULL),
(3605, 222, 'West Lothian', 'WLOT', 1, '0000-00-00', '0000-00-00', NULL),
(3606, 222, 'West Midlands', 'WMD', 1, '0000-00-00', '0000-00-00', NULL),
(3607, 222, 'West Sussex', 'SXW', 1, '0000-00-00', '0000-00-00', NULL),
(3608, 222, 'West Yorkshire', 'YSW', 1, '0000-00-00', '0000-00-00', NULL),
(3609, 222, 'Western Isles', 'WIL', 1, '0000-00-00', '0000-00-00', NULL),
(3610, 222, 'Wiltshire', 'WLT', 1, '0000-00-00', '0000-00-00', NULL),
(3611, 222, 'Worcestershire', 'WORCS', 1, '0000-00-00', '0000-00-00', NULL),
(3612, 222, 'Wrexham', 'WRX', 1, '0000-00-00', '0000-00-00', NULL),
(3613, 223, 'Alabama', 'AL', 1, '0000-00-00', '0000-00-00', NULL),
(3614, 223, 'Alaska', 'AK', 1, '0000-00-00', '0000-00-00', NULL),
(3615, 223, 'American Samoa', 'AS', 1, '0000-00-00', '0000-00-00', NULL),
(3616, 223, 'Arizona', 'AZ', 1, '0000-00-00', '0000-00-00', NULL),
(3617, 223, 'Arkansas', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3618, 223, 'Armed Forces Africa', 'AF', 1, '0000-00-00', '0000-00-00', NULL),
(3619, 223, 'Armed Forces Americas', 'AA', 1, '0000-00-00', '0000-00-00', NULL),
(3620, 223, 'Armed Forces Canada', 'AC', 1, '0000-00-00', '0000-00-00', NULL),
(3621, 223, 'Armed Forces Europe', 'AE', 1, '0000-00-00', '0000-00-00', NULL),
(3622, 223, 'Armed Forces Middle East', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(3623, 223, 'Armed Forces Pacific', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(3624, 223, 'California', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(3625, 223, 'Colorado', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(3626, 223, 'Connecticut', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(3627, 223, 'Delaware', 'DE', 1, '0000-00-00', '0000-00-00', NULL),
(3628, 223, 'District of Columbia', 'DC', 1, '0000-00-00', '0000-00-00', NULL),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1, '0000-00-00', '0000-00-00', NULL),
(3630, 223, 'Florida', 'FL', 1, '0000-00-00', '0000-00-00', NULL),
(3631, 223, 'Georgia', 'GA', 1, '0000-00-00', '0000-00-00', NULL),
(3632, 223, 'Guam', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(3633, 223, 'Hawaii', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(3634, 223, 'Idaho', 'ID', 1, '0000-00-00', '0000-00-00', NULL),
(3635, 223, 'Illinois', 'IL', 1, '0000-00-00', '0000-00-00', NULL),
(3636, 223, 'Indiana', 'IN', 1, '0000-00-00', '0000-00-00', NULL),
(3637, 223, 'Iowa', 'IA', 1, '0000-00-00', '0000-00-00', NULL),
(3638, 223, 'Kansas', 'KS', 1, '0000-00-00', '0000-00-00', NULL),
(3639, 223, 'Kentucky', 'KY', 1, '0000-00-00', '0000-00-00', NULL),
(3640, 223, 'Louisiana', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(3641, 223, 'Maine', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(3642, 223, 'Marshall Islands', 'MH', 1, '0000-00-00', '0000-00-00', NULL),
(3643, 223, 'Maryland', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(3644, 223, 'Massachusetts', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3645, 223, 'Michigan', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(3646, 223, 'Minnesota', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(3647, 223, 'Mississippi', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(3648, 223, 'Missouri', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3649, 223, 'Montana', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(3650, 223, 'Nebraska', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(3651, 223, 'Nevada', 'NV', 1, '0000-00-00', '0000-00-00', NULL),
(3652, 223, 'New Hampshire', 'NH', 1, '0000-00-00', '0000-00-00', NULL),
(3653, 223, 'New Jersey', 'NJ', 1, '0000-00-00', '0000-00-00', NULL),
(3654, 223, 'New Mexico', 'NM', 1, '0000-00-00', '0000-00-00', NULL),
(3655, 223, 'New York', 'NY', 1, '0000-00-00', '0000-00-00', NULL),
(3656, 223, 'North Carolina', 'NC', 1, '0000-00-00', '0000-00-00', NULL),
(3657, 223, 'North Dakota', 'ND', 1, '0000-00-00', '0000-00-00', NULL),
(3658, 223, 'Northern Mariana Islands', 'MP', 1, '0000-00-00', '0000-00-00', NULL),
(3659, 223, 'Ohio', 'OH', 1, '0000-00-00', '0000-00-00', NULL),
(3660, 223, 'Oklahoma', 'OK', 1, '0000-00-00', '0000-00-00', NULL),
(3661, 223, 'Oregon', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(3662, 223, 'Palau', 'PW', 1, '0000-00-00', '0000-00-00', NULL),
(3663, 223, 'Pennsylvania', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(3664, 223, 'Puerto Rico', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(3665, 223, 'Rhode Island', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(3666, 223, 'South Carolina', 'SC', 1, '0000-00-00', '0000-00-00', NULL),
(3667, 223, 'South Dakota', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(3668, 223, 'Tennessee', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(3669, 223, 'Texas', 'TX', 1, '0000-00-00', '0000-00-00', NULL),
(3670, 223, 'Utah', 'UT', 1, '0000-00-00', '0000-00-00', NULL),
(3671, 223, 'Vermont', 'VT', 1, '0000-00-00', '0000-00-00', NULL),
(3672, 223, 'Virgin Islands', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(3673, 223, 'Virginia', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(3674, 223, 'Washington', 'WA', 1, '0000-00-00', '0000-00-00', NULL),
(3675, 223, 'West Virginia', 'WV', 1, '0000-00-00', '0000-00-00', NULL),
(3676, 223, 'Wisconsin', 'WI', 1, '0000-00-00', '0000-00-00', NULL),
(3677, 223, 'Wyoming', 'WY', 1, '0000-00-00', '0000-00-00', NULL),
(3678, 224, 'Baker Island', 'BI', 1, '0000-00-00', '0000-00-00', NULL),
(3679, 224, 'Howland Island', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(3680, 224, 'Jarvis Island', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(3681, 224, 'Johnston Atoll', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(3682, 224, 'Kingman Reef', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(3683, 224, 'Midway Atoll', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3684, 224, 'Navassa Island', 'NI', 1, '0000-00-00', '0000-00-00', NULL),
(3685, 224, 'Palmyra Atoll', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(3686, 224, 'Wake Island', 'WI', 1, '0000-00-00', '0000-00-00', NULL),
(3687, 225, 'Artigas', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3688, 225, 'Canelones', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(3689, 225, 'Cerro Largo', 'CL', 1, '0000-00-00', '0000-00-00', NULL),
(3690, 225, 'Colonia', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(3691, 225, 'Durazno', 'DU', 1, '0000-00-00', '0000-00-00', NULL),
(3692, 225, 'Flores', 'FS', 1, '0000-00-00', '0000-00-00', NULL),
(3693, 225, 'Florida', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(3694, 225, 'Lavalleja', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(3695, 225, 'Maldonado', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3696, 225, 'Montevideo', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3697, 225, 'Paysandu', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(3698, 225, 'Rio Negro', 'RN', 1, '0000-00-00', '0000-00-00', NULL),
(3699, 225, 'Rivera', 'RV', 1, '0000-00-00', '0000-00-00', NULL),
(3700, 225, 'Rocha', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(3701, 225, 'Salto', 'SL', 1, '0000-00-00', '0000-00-00', NULL),
(3702, 225, 'San Jose', 'SJ', 1, '0000-00-00', '0000-00-00', NULL),
(3703, 225, 'Soriano', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3704, 225, 'Tacuarembo', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3705, 225, 'Treinta y Tres', 'TT', 1, '0000-00-00', '0000-00-00', NULL),
(3706, 226, 'Andijon', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(3707, 226, 'Buxoro', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(3708, 226, 'Farg\'ona', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(3709, 226, 'Jizzax', 'JI', 1, '0000-00-00', '0000-00-00', NULL),
(3710, 226, 'Namangan', 'NG', 1, '0000-00-00', '0000-00-00', NULL),
(3711, 226, 'Navoiy', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(3712, 226, 'Qashqadaryo', 'QA', 1, '0000-00-00', '0000-00-00', NULL),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1, '0000-00-00', '0000-00-00', NULL),
(3714, 226, 'Samarqand', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(3715, 226, 'Sirdaryo', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(3716, 226, 'Surxondaryo', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(3717, 226, 'Toshkent City', 'TK', 1, '0000-00-00', '0000-00-00', NULL),
(3718, 226, 'Toshkent Region', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3719, 226, 'Xorazm', 'XO', 1, '0000-00-00', '0000-00-00', NULL),
(3720, 227, 'Malampa', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3721, 227, 'Penama', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(3722, 227, 'Sanma', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(3723, 227, 'Shefa', 'SH', 1, '0000-00-00', '0000-00-00', NULL),
(3724, 227, 'Tafea', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3725, 227, 'Torba', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3726, 229, 'Amazonas', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(3727, 229, 'Anzoategui', 'AN', 1, '0000-00-00', '0000-00-00', NULL),
(3728, 229, 'Apure', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(3729, 229, 'Aragua', 'AR', 1, '0000-00-00', '0000-00-00', NULL),
(3730, 229, 'Barinas', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(3731, 229, 'Bolivar', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(3732, 229, 'Carabobo', 'CA', 1, '0000-00-00', '0000-00-00', NULL),
(3733, 229, 'Cojedes', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(3734, 229, 'Delta Amacuro', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(3735, 229, 'Dependencias Federales', 'DF', 1, '0000-00-00', '0000-00-00', NULL),
(3736, 229, 'Distrito Federal', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(3737, 229, 'Falcon', 'FA', 1, '0000-00-00', '0000-00-00', NULL),
(3738, 229, 'Guarico', 'GU', 1, '0000-00-00', '0000-00-00', NULL),
(3739, 229, 'Lara', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(3740, 229, 'Merida', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(3741, 229, 'Miranda', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(3742, 229, 'Monagas', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3743, 229, 'Nueva Esparta', 'NE', 1, '0000-00-00', '0000-00-00', NULL),
(3744, 229, 'Portuguesa', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(3745, 229, 'Sucre', 'SU', 1, '0000-00-00', '0000-00-00', NULL),
(3746, 229, 'Tachira', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3747, 229, 'Trujillo', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(3748, 229, 'Vargas', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(3749, 229, 'Yaracuy', 'YA', 1, '0000-00-00', '0000-00-00', NULL),
(3750, 229, 'Zulia', 'ZU', 1, '0000-00-00', '0000-00-00', NULL),
(3751, 230, 'An Giang', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(3752, 230, 'Bac Giang', 'BG', 1, '0000-00-00', '0000-00-00', NULL),
(3753, 230, 'Bac Kan', 'BK', 1, '0000-00-00', '0000-00-00', NULL),
(3754, 230, 'Bac Lieu', 'BL', 1, '0000-00-00', '0000-00-00', NULL),
(3755, 230, 'Bac Ninh', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1, '0000-00-00', '0000-00-00', NULL),
(3757, 230, 'Ben Tre', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(3758, 230, 'Binh Dinh', 'BH', 1, '0000-00-00', '0000-00-00', NULL),
(3759, 230, 'Binh Duong', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(3760, 230, 'Binh Phuoc', 'BP', 1, '0000-00-00', '0000-00-00', NULL),
(3761, 230, 'Binh Thuan', 'BT', 1, '0000-00-00', '0000-00-00', NULL),
(3762, 230, 'Ca Mau', 'CM', 1, '0000-00-00', '0000-00-00', NULL),
(3763, 230, 'Can Tho', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(3764, 230, 'Cao Bang', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(3765, 230, 'Dak Lak', 'DL', 1, '0000-00-00', '0000-00-00', NULL),
(3766, 230, 'Dak Nong', 'DG', 1, '0000-00-00', '0000-00-00', NULL),
(3767, 230, 'Da Nang', 'DN', 1, '0000-00-00', '0000-00-00', NULL),
(3768, 230, 'Dien Bien', 'DB', 1, '0000-00-00', '0000-00-00', NULL),
(3769, 230, 'Dong Nai', 'DI', 1, '0000-00-00', '0000-00-00', NULL),
(3770, 230, 'Dong Thap', 'DT', 1, '0000-00-00', '0000-00-00', NULL),
(3771, 230, 'Gia Lai', 'GL', 1, '0000-00-00', '0000-00-00', NULL),
(3772, 230, 'Ha Giang', 'HG', 1, '0000-00-00', '0000-00-00', NULL),
(3773, 230, 'Hai Duong', 'HD', 1, '0000-00-00', '0000-00-00', NULL),
(3774, 230, 'Hai Phong', 'HP', 1, '0000-00-00', '0000-00-00', NULL),
(3775, 230, 'Ha Nam', 'HM', 1, '0000-00-00', '0000-00-00', NULL),
(3776, 230, 'Ha Noi', 'HI', 1, '0000-00-00', '0000-00-00', NULL),
(3777, 230, 'Ha Tay', 'HT', 1, '0000-00-00', '0000-00-00', NULL),
(3778, 230, 'Ha Tinh', 'HH', 1, '0000-00-00', '0000-00-00', NULL),
(3779, 230, 'Hoa Binh', 'HB', 1, '0000-00-00', '0000-00-00', NULL),
(3780, 230, 'Ho Chi Minh City', 'HC', 1, '0000-00-00', '0000-00-00', NULL),
(3781, 230, 'Hau Giang', 'HU', 1, '0000-00-00', '0000-00-00', NULL),
(3782, 230, 'Hung Yen', 'HY', 1, '0000-00-00', '0000-00-00', NULL),
(3783, 232, 'Saint Croix', 'C', 1, '0000-00-00', '0000-00-00', NULL),
(3784, 232, 'Saint John', 'J', 1, '0000-00-00', '0000-00-00', NULL),
(3785, 232, 'Saint Thomas', 'T', 1, '0000-00-00', '0000-00-00', NULL),
(3786, 233, 'Alo', 'A', 1, '0000-00-00', '0000-00-00', NULL),
(3787, 233, 'Sigave', 'S', 1, '0000-00-00', '0000-00-00', NULL),
(3788, 233, 'Wallis', 'W', 1, '0000-00-00', '0000-00-00', NULL),
(3789, 235, 'Abyan', 'AB', 1, '0000-00-00', '0000-00-00', NULL),
(3790, 235, 'Adan', 'AD', 1, '0000-00-00', '0000-00-00', NULL),
(3791, 235, 'Amran', 'AM', 1, '0000-00-00', '0000-00-00', NULL),
(3792, 235, 'Al Bayda', 'BA', 1, '0000-00-00', '0000-00-00', NULL),
(3793, 235, 'Ad Dali', 'DA', 1, '0000-00-00', '0000-00-00', NULL),
(3794, 235, 'Dhamar', 'DH', 1, '0000-00-00', '0000-00-00', NULL),
(3795, 235, 'Hadramawt', 'HD', 1, '0000-00-00', '0000-00-00', NULL),
(3796, 235, 'Hajjah', 'HJ', 1, '0000-00-00', '0000-00-00', NULL),
(3797, 235, 'Al Hudaydah', 'HU', 1, '0000-00-00', '0000-00-00', NULL),
(3798, 235, 'Ibb', 'IB', 1, '0000-00-00', '0000-00-00', NULL),
(3799, 235, 'Al Jawf', 'JA', 1, '0000-00-00', '0000-00-00', NULL),
(3800, 235, 'Lahij', 'LA', 1, '0000-00-00', '0000-00-00', NULL),
(3801, 235, 'Ma\'rib', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3802, 235, 'Al Mahrah', 'MR', 1, '0000-00-00', '0000-00-00', NULL),
(3803, 235, 'Al Mahwit', 'MW', 1, '0000-00-00', '0000-00-00', NULL),
(3804, 235, 'Sa\'dah', 'SD', 1, '0000-00-00', '0000-00-00', NULL),
(3805, 235, 'San\'a', 'SN', 1, '0000-00-00', '0000-00-00', NULL),
(3806, 235, 'Shabwah', 'SH', 1, '0000-00-00', '0000-00-00', NULL);
INSERT INTO `oc_zone` (`id`, `country_id`, `name`, `code`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3807, 235, 'Ta\'izz', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3812, 237, 'Bas-Congo', 'BC', 1, '0000-00-00', '0000-00-00', NULL),
(3813, 237, 'Bandundu', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(3814, 237, 'Equateur', 'EQ', 1, '0000-00-00', '0000-00-00', NULL),
(3815, 237, 'Katanga', 'KA', 1, '0000-00-00', '0000-00-00', NULL),
(3816, 237, 'Kasai-Oriental', 'KE', 1, '0000-00-00', '0000-00-00', NULL),
(3817, 237, 'Kinshasa', 'KN', 1, '0000-00-00', '0000-00-00', NULL),
(3818, 237, 'Kasai-Occidental', 'KW', 1, '0000-00-00', '0000-00-00', NULL),
(3819, 237, 'Maniema', 'MA', 1, '0000-00-00', '0000-00-00', NULL),
(3820, 237, 'Nord-Kivu', 'NK', 1, '0000-00-00', '0000-00-00', NULL),
(3821, 237, 'Orientale', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(3822, 237, 'Sud-Kivu', 'SK', 1, '0000-00-00', '0000-00-00', NULL),
(3823, 238, 'Central', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(3824, 238, 'Copperbelt', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(3825, 238, 'Eastern', 'EA', 1, '0000-00-00', '0000-00-00', NULL),
(3826, 238, 'Luapula', 'LP', 1, '0000-00-00', '0000-00-00', NULL),
(3827, 238, 'Lusaka', 'LK', 1, '0000-00-00', '0000-00-00', NULL),
(3828, 238, 'Northern', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(3829, 238, 'North-Western', 'NW', 1, '0000-00-00', '0000-00-00', NULL),
(3830, 238, 'Southern', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3831, 238, 'Western', 'WE', 1, '0000-00-00', '0000-00-00', NULL),
(3832, 239, 'Bulawayo', 'BU', 1, '0000-00-00', '0000-00-00', NULL),
(3833, 239, 'Harare', 'HA', 1, '0000-00-00', '0000-00-00', NULL),
(3834, 239, 'Manicaland', 'ML', 1, '0000-00-00', '0000-00-00', NULL),
(3835, 239, 'Mashonaland Central', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(3836, 239, 'Mashonaland East', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(3837, 239, 'Mashonaland West', 'MW', 1, '0000-00-00', '0000-00-00', NULL),
(3838, 239, 'Masvingo', 'MV', 1, '0000-00-00', '0000-00-00', NULL),
(3839, 239, 'Matabeleland North', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(3840, 239, 'Matabeleland South', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(3841, 239, 'Midlands', 'MD', 1, '0000-00-00', '0000-00-00', NULL),
(3861, 105, 'Campobasso', 'CB', 1, '0000-00-00', '0000-00-00', NULL),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1, '0000-00-00', '0000-00-00', NULL),
(3863, 105, 'Caserta', 'CE', 1, '0000-00-00', '0000-00-00', NULL),
(3864, 105, 'Catania', 'CT', 1, '0000-00-00', '0000-00-00', NULL),
(3865, 105, 'Catanzaro', 'CZ', 1, '0000-00-00', '0000-00-00', NULL),
(3866, 105, 'Chieti', 'CH', 1, '0000-00-00', '0000-00-00', NULL),
(3867, 105, 'Como', 'CO', 1, '0000-00-00', '0000-00-00', NULL),
(3868, 105, 'Cosenza', 'CS', 1, '0000-00-00', '0000-00-00', NULL),
(3869, 105, 'Cremona', 'CR', 1, '0000-00-00', '0000-00-00', NULL),
(3870, 105, 'Crotone', 'KR', 1, '0000-00-00', '0000-00-00', NULL),
(3871, 105, 'Cuneo', 'CN', 1, '0000-00-00', '0000-00-00', NULL),
(3872, 105, 'Enna', 'EN', 1, '0000-00-00', '0000-00-00', NULL),
(3873, 105, 'Ferrara', 'FE', 1, '0000-00-00', '0000-00-00', NULL),
(3874, 105, 'Firenze', 'FI', 1, '0000-00-00', '0000-00-00', NULL),
(3875, 105, 'Foggia', 'FG', 1, '0000-00-00', '0000-00-00', NULL),
(3876, 105, 'Forli-Cesena', 'FC', 1, '0000-00-00', '0000-00-00', NULL),
(3877, 105, 'Frosinone', 'FR', 1, '0000-00-00', '0000-00-00', NULL),
(3878, 105, 'Genova', 'GE', 1, '0000-00-00', '0000-00-00', NULL),
(3879, 105, 'Gorizia', 'GO', 1, '0000-00-00', '0000-00-00', NULL),
(3880, 105, 'Grosseto', 'GR', 1, '0000-00-00', '0000-00-00', NULL),
(3881, 105, 'Imperia', 'IM', 1, '0000-00-00', '0000-00-00', NULL),
(3882, 105, 'Isernia', 'IS', 1, '0000-00-00', '0000-00-00', NULL),
(3883, 105, 'L&#39;Aquila', 'AQ', 1, '0000-00-00', '0000-00-00', NULL),
(3884, 105, 'La Spezia', 'SP', 1, '0000-00-00', '0000-00-00', NULL),
(3885, 105, 'Latina', 'LT', 1, '0000-00-00', '0000-00-00', NULL),
(3886, 105, 'Lecce', 'LE', 1, '0000-00-00', '0000-00-00', NULL),
(3887, 105, 'Lecco', 'LC', 1, '0000-00-00', '0000-00-00', NULL),
(3888, 105, 'Livorno', 'LI', 1, '0000-00-00', '0000-00-00', NULL),
(3889, 105, 'Lodi', 'LO', 1, '0000-00-00', '0000-00-00', NULL),
(3890, 105, 'Lucca', 'LU', 1, '0000-00-00', '0000-00-00', NULL),
(3891, 105, 'Macerata', 'MC', 1, '0000-00-00', '0000-00-00', NULL),
(3892, 105, 'Mantova', 'MN', 1, '0000-00-00', '0000-00-00', NULL),
(3893, 105, 'Massa-Carrara', 'MS', 1, '0000-00-00', '0000-00-00', NULL),
(3894, 105, 'Matera', 'MT', 1, '0000-00-00', '0000-00-00', NULL),
(3895, 105, 'Medio Campidano', 'VS', 1, '0000-00-00', '0000-00-00', NULL),
(3896, 105, 'Messina', 'ME', 1, '0000-00-00', '0000-00-00', NULL),
(3897, 105, 'Milano', 'MI', 1, '0000-00-00', '0000-00-00', NULL),
(3898, 105, 'Modena', 'MO', 1, '0000-00-00', '0000-00-00', NULL),
(3899, 105, 'Napoli', 'NA', 1, '0000-00-00', '0000-00-00', NULL),
(3900, 105, 'Novara', 'NO', 1, '0000-00-00', '0000-00-00', NULL),
(3901, 105, 'Nuoro', 'NU', 1, '0000-00-00', '0000-00-00', NULL),
(3902, 105, 'Ogliastra', 'OG', 1, '0000-00-00', '0000-00-00', NULL),
(3903, 105, 'Olbia-Tempio', 'OT', 1, '0000-00-00', '0000-00-00', NULL),
(3904, 105, 'Oristano', 'OR', 1, '0000-00-00', '0000-00-00', NULL),
(3905, 105, 'Padova', 'PD', 1, '0000-00-00', '0000-00-00', NULL),
(3906, 105, 'Palermo', 'PA', 1, '0000-00-00', '0000-00-00', NULL),
(3907, 105, 'Parma', 'PR', 1, '0000-00-00', '0000-00-00', NULL),
(3908, 105, 'Pavia', 'PV', 1, '0000-00-00', '0000-00-00', NULL),
(3909, 105, 'Perugia', 'PG', 1, '0000-00-00', '0000-00-00', NULL),
(3910, 105, 'Pesaro e Urbino', 'PU', 1, '0000-00-00', '0000-00-00', NULL),
(3911, 105, 'Pescara', 'PE', 1, '0000-00-00', '0000-00-00', NULL),
(3912, 105, 'Piacenza', 'PC', 1, '0000-00-00', '0000-00-00', NULL),
(3913, 105, 'Pisa', 'PI', 1, '0000-00-00', '0000-00-00', NULL),
(3914, 105, 'Pistoia', 'PT', 1, '0000-00-00', '0000-00-00', NULL),
(3915, 105, 'Pordenone', 'PN', 1, '0000-00-00', '0000-00-00', NULL),
(3916, 105, 'Potenza', 'PZ', 1, '0000-00-00', '0000-00-00', NULL),
(3917, 105, 'Prato', 'PO', 1, '0000-00-00', '0000-00-00', NULL),
(3918, 105, 'Ragusa', 'RG', 1, '0000-00-00', '0000-00-00', NULL),
(3919, 105, 'Ravenna', 'RA', 1, '0000-00-00', '0000-00-00', NULL),
(3920, 105, 'Reggio Calabria', 'RC', 1, '0000-00-00', '0000-00-00', NULL),
(3921, 105, 'Reggio Emilia', 'RE', 1, '0000-00-00', '0000-00-00', NULL),
(3922, 105, 'Rieti', 'RI', 1, '0000-00-00', '0000-00-00', NULL),
(3923, 105, 'Rimini', 'RN', 1, '0000-00-00', '0000-00-00', NULL),
(3924, 105, 'Roma', 'RM', 1, '0000-00-00', '0000-00-00', NULL),
(3925, 105, 'Rovigo', 'RO', 1, '0000-00-00', '0000-00-00', NULL),
(3926, 105, 'Salerno', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(3927, 105, 'Sassari', 'SS', 1, '0000-00-00', '0000-00-00', NULL),
(3928, 105, 'Savona', 'SV', 1, '0000-00-00', '0000-00-00', NULL),
(3929, 105, 'Siena', 'SI', 1, '0000-00-00', '0000-00-00', NULL),
(3930, 105, 'Siracusa', 'SR', 1, '0000-00-00', '0000-00-00', NULL),
(3931, 105, 'Sondrio', 'SO', 1, '0000-00-00', '0000-00-00', NULL),
(3932, 105, 'Taranto', 'TA', 1, '0000-00-00', '0000-00-00', NULL),
(3933, 105, 'Teramo', 'TE', 1, '0000-00-00', '0000-00-00', NULL),
(3934, 105, 'Terni', 'TR', 1, '0000-00-00', '0000-00-00', NULL),
(3935, 105, 'Torino', 'TO', 1, '0000-00-00', '0000-00-00', NULL),
(3936, 105, 'Trapani', 'TP', 1, '0000-00-00', '0000-00-00', NULL),
(3937, 105, 'Trento', 'TN', 1, '0000-00-00', '0000-00-00', NULL),
(3938, 105, 'Treviso', 'TV', 1, '0000-00-00', '0000-00-00', NULL),
(3939, 105, 'Trieste', 'TS', 1, '0000-00-00', '0000-00-00', NULL),
(3940, 105, 'Udine', 'UD', 1, '0000-00-00', '0000-00-00', NULL),
(3941, 105, 'Varese', 'VA', 1, '0000-00-00', '0000-00-00', NULL),
(3942, 105, 'Venezia', 'VE', 1, '0000-00-00', '0000-00-00', NULL),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1, '0000-00-00', '0000-00-00', NULL),
(3944, 105, 'Vercelli', 'VC', 1, '0000-00-00', '0000-00-00', NULL),
(3945, 105, 'Verona', 'VR', 1, '0000-00-00', '0000-00-00', NULL),
(3946, 105, 'Vibo Valentia', 'VV', 1, '0000-00-00', '0000-00-00', NULL),
(3947, 105, 'Vicenza', 'VI', 1, '0000-00-00', '0000-00-00', NULL),
(3948, 105, 'Viterbo', 'VT', 1, '0000-00-00', '0000-00-00', NULL),
(3949, 222, 'County Antrim', 'ANT', 1, '0000-00-00', '0000-00-00', NULL),
(3950, 222, 'County Armagh', 'ARM', 1, '0000-00-00', '0000-00-00', NULL),
(3951, 222, 'County Down', 'DOW', 1, '0000-00-00', '0000-00-00', NULL),
(3952, 222, 'County Fermanagh', 'FER', 1, '0000-00-00', '0000-00-00', NULL),
(3953, 222, 'County Londonderry', 'LDY', 1, '0000-00-00', '0000-00-00', NULL),
(3954, 222, 'County Tyrone', 'TYR', 1, '0000-00-00', '0000-00-00', NULL),
(3955, 222, 'Cumbria', 'CMA', 1, '0000-00-00', '0000-00-00', NULL),
(3956, 190, 'Pomurska', '1', 1, '0000-00-00', '0000-00-00', NULL),
(3957, 190, 'Podravska', '2', 1, '0000-00-00', '0000-00-00', NULL),
(3958, 190, 'Koroška', '3', 1, '0000-00-00', '0000-00-00', NULL),
(3959, 190, 'Savinjska', '4', 1, '0000-00-00', '0000-00-00', NULL),
(3960, 190, 'Zasavska', '5', 1, '0000-00-00', '0000-00-00', NULL),
(3961, 190, 'Spodnjeposavska', '6', 1, '0000-00-00', '0000-00-00', NULL),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1, '0000-00-00', '0000-00-00', NULL),
(3963, 190, 'Osrednjeslovenska', '8', 1, '0000-00-00', '0000-00-00', NULL),
(3964, 190, 'Gorenjska', '9', 1, '0000-00-00', '0000-00-00', NULL),
(3965, 190, 'Notranjsko-kraška', '10', 1, '0000-00-00', '0000-00-00', NULL),
(3966, 190, 'Goriška', '11', 1, '0000-00-00', '0000-00-00', NULL),
(3967, 190, 'Obalno-kraška', '12', 1, '0000-00-00', '0000-00-00', NULL),
(3968, 33, 'Ruse', '', 1, '0000-00-00', '0000-00-00', NULL),
(3969, 101, 'Alborz', 'ALB', 1, '0000-00-00', '0000-00-00', NULL),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1, '0000-00-00', '0000-00-00', NULL),
(3971, 138, 'Aguascalientes', 'AG', 1, '0000-00-00', '0000-00-00', NULL),
(3973, 242, 'Andrijevica', '01', 1, '0000-00-00', '0000-00-00', NULL),
(3974, 242, 'Bar', '02', 1, '0000-00-00', '0000-00-00', NULL),
(3975, 242, 'Berane', '03', 1, '0000-00-00', '0000-00-00', NULL),
(3976, 242, 'Bijelo Polje', '04', 1, '0000-00-00', '0000-00-00', NULL),
(3977, 242, 'Budva', '05', 1, '0000-00-00', '0000-00-00', NULL),
(3978, 242, 'Cetinje', '06', 1, '0000-00-00', '0000-00-00', NULL),
(3979, 242, 'Danilovgrad', '07', 1, '0000-00-00', '0000-00-00', NULL),
(3980, 242, 'Herceg-Novi', '08', 1, '0000-00-00', '0000-00-00', NULL),
(3981, 242, 'Kolašin', '09', 1, '0000-00-00', '0000-00-00', NULL),
(3982, 242, 'Kotor', '10', 1, '0000-00-00', '0000-00-00', NULL),
(3983, 242, 'Mojkovac', '11', 1, '0000-00-00', '0000-00-00', NULL),
(3984, 242, 'Nikšić', '12', 1, '0000-00-00', '0000-00-00', NULL),
(3985, 242, 'Plav', '13', 1, '0000-00-00', '0000-00-00', NULL),
(3986, 242, 'Pljevlja', '14', 1, '0000-00-00', '0000-00-00', NULL),
(3987, 242, 'Plužine', '15', 1, '0000-00-00', '0000-00-00', NULL),
(3988, 242, 'Podgorica', '16', 1, '0000-00-00', '0000-00-00', NULL),
(3989, 242, 'Rožaje', '17', 1, '0000-00-00', '0000-00-00', NULL),
(3990, 242, 'Šavnik', '18', 1, '0000-00-00', '0000-00-00', NULL),
(3991, 242, 'Tivat', '19', 1, '0000-00-00', '0000-00-00', NULL),
(3992, 242, 'Ulcinj', '20', 1, '0000-00-00', '0000-00-00', NULL),
(3993, 242, 'Žabljak', '21', 1, '0000-00-00', '0000-00-00', NULL),
(3994, 243, 'Belgrade', '00', 1, '0000-00-00', '0000-00-00', NULL),
(3995, 243, 'North Bačka', '01', 1, '0000-00-00', '0000-00-00', NULL),
(3996, 243, 'Central Banat', '02', 1, '0000-00-00', '0000-00-00', NULL),
(3997, 243, 'North Banat', '03', 1, '0000-00-00', '0000-00-00', NULL),
(3998, 243, 'South Banat', '04', 1, '0000-00-00', '0000-00-00', NULL),
(3999, 243, 'West Bačka', '05', 1, '0000-00-00', '0000-00-00', NULL),
(4000, 243, 'South Bačka', '06', 1, '0000-00-00', '0000-00-00', NULL),
(4001, 243, 'Srem', '07', 1, '0000-00-00', '0000-00-00', NULL),
(4002, 243, 'Mačva', '08', 1, '0000-00-00', '0000-00-00', NULL),
(4003, 243, 'Kolubara', '09', 1, '0000-00-00', '0000-00-00', NULL),
(4004, 243, 'Podunavlje', '10', 1, '0000-00-00', '0000-00-00', NULL),
(4005, 243, 'Braničevo', '11', 1, '0000-00-00', '0000-00-00', NULL),
(4006, 243, 'Šumadija', '12', 1, '0000-00-00', '0000-00-00', NULL),
(4007, 243, 'Pomoravlje', '13', 1, '0000-00-00', '0000-00-00', NULL),
(4008, 243, 'Bor', '14', 1, '0000-00-00', '0000-00-00', NULL),
(4009, 243, 'Zaječar', '15', 1, '0000-00-00', '0000-00-00', NULL),
(4010, 243, 'Zlatibor', '16', 1, '0000-00-00', '0000-00-00', NULL),
(4011, 243, 'Moravica', '17', 1, '0000-00-00', '0000-00-00', NULL),
(4012, 243, 'Raška', '18', 1, '0000-00-00', '0000-00-00', NULL),
(4013, 243, 'Rasina', '19', 1, '0000-00-00', '0000-00-00', NULL),
(4014, 243, 'Nišava', '20', 1, '0000-00-00', '0000-00-00', NULL),
(4015, 243, 'Toplica', '21', 1, '0000-00-00', '0000-00-00', NULL),
(4016, 243, 'Pirot', '22', 1, '0000-00-00', '0000-00-00', NULL),
(4017, 243, 'Jablanica', '23', 1, '0000-00-00', '0000-00-00', NULL),
(4018, 243, 'Pčinja', '24', 1, '0000-00-00', '0000-00-00', NULL),
(4020, 245, 'Bonaire', 'BO', 1, '0000-00-00', '0000-00-00', NULL),
(4021, 245, 'Saba', 'SA', 1, '0000-00-00', '0000-00-00', NULL),
(4022, 245, 'Sint Eustatius', 'SE', 1, '0000-00-00', '0000-00-00', NULL),
(4023, 248, 'Central Equatoria', 'EC', 1, '0000-00-00', '0000-00-00', NULL),
(4024, 248, 'Eastern Equatoria', 'EE', 1, '0000-00-00', '0000-00-00', NULL),
(4025, 248, 'Jonglei', 'JG', 1, '0000-00-00', '0000-00-00', NULL),
(4026, 248, 'Lakes', 'LK', 1, '0000-00-00', '0000-00-00', NULL),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1, '0000-00-00', '0000-00-00', NULL),
(4028, 248, 'Unity', 'UY', 1, '0000-00-00', '0000-00-00', NULL),
(4029, 248, 'Upper Nile', 'NU', 1, '0000-00-00', '0000-00-00', NULL),
(4030, 248, 'Warrap', 'WR', 1, '0000-00-00', '0000-00-00', NULL),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1, '0000-00-00', '0000-00-00', NULL),
(4032, 248, 'Western Equatoria', 'EW', 1, '0000-00-00', '0000-00-00', NULL),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1, '0000-00-00', '0000-00-00', NULL),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1, '0000-00-00', '0000-00-00', NULL),
(4038, 117, 'Aizkraukles novads', '0320200', 1, '0000-00-00', '0000-00-00', NULL),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1, '0000-00-00', '0000-00-00', NULL),
(4040, 117, 'Aizputes novads', '0640600', 1, '0000-00-00', '0000-00-00', NULL),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1, '0000-00-00', '0000-00-00', NULL),
(4042, 117, 'Aknīstes novads', '0560800', 1, '0000-00-00', '0000-00-00', NULL),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1, '0000-00-00', '0000-00-00', NULL),
(4044, 117, 'Alojas novads', '0661000', 1, '0000-00-00', '0000-00-00', NULL),
(4045, 117, 'Alsungas novads', '0624200', 1, '0000-00-00', '0000-00-00', NULL),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1, '0000-00-00', '0000-00-00', NULL),
(4047, 117, 'Alūksnes novads', '0360200', 1, '0000-00-00', '0000-00-00', NULL),
(4048, 117, 'Amatas novads', '0424701', 1, '0000-00-00', '0000-00-00', NULL),
(4049, 117, 'Ape, Apes novads', '0360805', 1, '0000-00-00', '0000-00-00', NULL),
(4050, 117, 'Apes novads', '0360800', 1, '0000-00-00', '0000-00-00', NULL),
(4051, 117, 'Auce, Auces novads', '0460805', 1, '0000-00-00', '0000-00-00', NULL),
(4052, 117, 'Auces novads', '0460800', 1, '0000-00-00', '0000-00-00', NULL),
(4053, 117, 'Ādažu novads', '0804400', 1, '0000-00-00', '0000-00-00', NULL),
(4054, 117, 'Babītes novads', '0804900', 1, '0000-00-00', '0000-00-00', NULL),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1, '0000-00-00', '0000-00-00', NULL),
(4056, 117, 'Baldones novads', '0800600', 1, '0000-00-00', '0000-00-00', NULL),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1, '0000-00-00', '0000-00-00', NULL),
(4058, 117, 'Baltinavas novads', '0384400', 1, '0000-00-00', '0000-00-00', NULL),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1, '0000-00-00', '0000-00-00', NULL),
(4060, 117, 'Balvu novads', '0380200', 1, '0000-00-00', '0000-00-00', NULL),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1, '0000-00-00', '0000-00-00', NULL),
(4062, 117, 'Bauskas novads', '0400200', 1, '0000-00-00', '0000-00-00', NULL),
(4063, 117, 'Beverīnas novads', '0964700', 1, '0000-00-00', '0000-00-00', NULL),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1, '0000-00-00', '0000-00-00', NULL),
(4065, 117, 'Brocēnu novads', '0840601', 1, '0000-00-00', '0000-00-00', NULL),
(4066, 117, 'Burtnieku novads', '0967101', 1, '0000-00-00', '0000-00-00', NULL),
(4067, 117, 'Carnikavas novads', '0805200', 1, '0000-00-00', '0000-00-00', NULL),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1, '0000-00-00', '0000-00-00', NULL),
(4069, 117, 'Cesvaines novads', '0700800', 1, '0000-00-00', '0000-00-00', NULL),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1, '0000-00-00', '0000-00-00', NULL),
(4071, 117, 'Cēsu novads', '0420200', 1, '0000-00-00', '0000-00-00', NULL),
(4072, 117, 'Ciblas novads', '0684901', 1, '0000-00-00', '0000-00-00', NULL),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1, '0000-00-00', '0000-00-00', NULL),
(4074, 117, 'Dagdas novads', '0601000', 1, '0000-00-00', '0000-00-00', NULL),
(4075, 117, 'Daugavpils', '0050000', 1, '0000-00-00', '0000-00-00', NULL),
(4076, 117, 'Daugavpils novads', '0440200', 1, '0000-00-00', '0000-00-00', NULL),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1, '0000-00-00', '0000-00-00', NULL),
(4078, 117, 'Dobeles novads', '0460200', 1, '0000-00-00', '0000-00-00', NULL),
(4079, 117, 'Dundagas novads', '0885100', 1, '0000-00-00', '0000-00-00', NULL),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1, '0000-00-00', '0000-00-00', NULL),
(4081, 117, 'Durbes novads', '0640801', 1, '0000-00-00', '0000-00-00', NULL),
(4082, 117, 'Engures novads', '0905100', 1, '0000-00-00', '0000-00-00', NULL),
(4083, 117, 'Ērgļu novads', '0705500', 1, '0000-00-00', '0000-00-00', NULL),
(4084, 117, 'Garkalnes novads', '0806000', 1, '0000-00-00', '0000-00-00', NULL),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1, '0000-00-00', '0000-00-00', NULL),
(4086, 117, 'Grobiņas novads', '0641000', 1, '0000-00-00', '0000-00-00', NULL),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1, '0000-00-00', '0000-00-00', NULL),
(4088, 117, 'Gulbenes novads', '0500200', 1, '0000-00-00', '0000-00-00', NULL),
(4089, 117, 'Iecavas novads', '0406400', 1, '0000-00-00', '0000-00-00', NULL),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1, '0000-00-00', '0000-00-00', NULL),
(4091, 117, 'Ikšķiles novads', '0740600', 1, '0000-00-00', '0000-00-00', NULL),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1, '0000-00-00', '0000-00-00', NULL),
(4093, 117, 'Ilūkstes novads', '0440801', 1, '0000-00-00', '0000-00-00', NULL),
(4094, 117, 'Inčukalna novads', '0801800', 1, '0000-00-00', '0000-00-00', NULL),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1, '0000-00-00', '0000-00-00', NULL),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1, '0000-00-00', '0000-00-00', NULL),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1, '0000-00-00', '0000-00-00', NULL),
(4098, 117, 'Jaunpils novads', '0905700', 1, '0000-00-00', '0000-00-00', NULL),
(4099, 117, 'Jelgava', '0090000', 1, '0000-00-00', '0000-00-00', NULL),
(4100, 117, 'Jelgavas novads', '0540200', 1, '0000-00-00', '0000-00-00', NULL),
(4101, 117, 'Jēkabpils', '0110000', 1, '0000-00-00', '0000-00-00', NULL),
(4102, 117, 'Jēkabpils novads', '0560200', 1, '0000-00-00', '0000-00-00', NULL),
(4103, 117, 'Jūrmala', '0130000', 1, '0000-00-00', '0000-00-00', NULL),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1, '0000-00-00', '0000-00-00', NULL),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1, '0000-00-00', '0000-00-00', NULL),
(4106, 117, 'Kandavas novads', '0901201', 1, '0000-00-00', '0000-00-00', NULL),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1, '0000-00-00', '0000-00-00', NULL),
(4108, 117, 'Kārsavas novads', '0681000', 1, '0000-00-00', '0000-00-00', NULL),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1, '0000-00-00', '0000-00-00', NULL),
(4110, 117, 'Kokneses novads', '0326100', 1, '0000-00-00', '0000-00-00', NULL),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1, '0000-00-00', '0000-00-00', NULL),
(4112, 117, 'Krāslavas novads', '0600202', 1, '0000-00-00', '0000-00-00', NULL),
(4113, 117, 'Krimuldas novads', '0806900', 1, '0000-00-00', '0000-00-00', NULL),
(4114, 117, 'Krustpils novads', '0566900', 1, '0000-00-00', '0000-00-00', NULL),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1, '0000-00-00', '0000-00-00', NULL),
(4116, 117, 'Kuldīgas novads', '0620200', 1, '0000-00-00', '0000-00-00', NULL),
(4117, 117, 'Ķeguma novads', '0741001', 1, '0000-00-00', '0000-00-00', NULL),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1, '0000-00-00', '0000-00-00', NULL),
(4119, 117, 'Ķekavas novads', '0800800', 1, '0000-00-00', '0000-00-00', NULL),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1, '0000-00-00', '0000-00-00', NULL),
(4121, 117, 'Lielvārdes novads', '0741401', 1, '0000-00-00', '0000-00-00', NULL),
(4122, 117, 'Liepāja', '0170000', 1, '0000-00-00', '0000-00-00', NULL),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1, '0000-00-00', '0000-00-00', NULL),
(4124, 117, 'Limbažu novads', '0660200', 1, '0000-00-00', '0000-00-00', NULL),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1, '0000-00-00', '0000-00-00', NULL),
(4126, 117, 'Līgatnes novads', '0421200', 1, '0000-00-00', '0000-00-00', NULL),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1, '0000-00-00', '0000-00-00', NULL),
(4128, 117, 'Līvānu novads', '0761201', 1, '0000-00-00', '0000-00-00', NULL),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1, '0000-00-00', '0000-00-00', NULL),
(4130, 117, 'Lubānas novads', '0701400', 1, '0000-00-00', '0000-00-00', NULL),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1, '0000-00-00', '0000-00-00', NULL),
(4132, 117, 'Ludzas novads', '0680200', 1, '0000-00-00', '0000-00-00', NULL),
(4133, 117, 'Madona, Madonas novads', '0700201', 1, '0000-00-00', '0000-00-00', NULL),
(4134, 117, 'Madonas novads', '0700200', 1, '0000-00-00', '0000-00-00', NULL),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1, '0000-00-00', '0000-00-00', NULL),
(4136, 117, 'Mazsalacas novads', '0961000', 1, '0000-00-00', '0000-00-00', NULL),
(4137, 117, 'Mālpils novads', '0807400', 1, '0000-00-00', '0000-00-00', NULL),
(4138, 117, 'Mārupes novads', '0807600', 1, '0000-00-00', '0000-00-00', NULL),
(4139, 117, 'Mērsraga novads', '0887600', 1, '0000-00-00', '0000-00-00', NULL),
(4140, 117, 'Naukšēnu novads', '0967300', 1, '0000-00-00', '0000-00-00', NULL),
(4141, 117, 'Neretas novads', '0327100', 1, '0000-00-00', '0000-00-00', NULL),
(4142, 117, 'Nīcas novads', '0647900', 1, '0000-00-00', '0000-00-00', NULL),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1, '0000-00-00', '0000-00-00', NULL),
(4144, 117, 'Ogres novads', '0740202', 1, '0000-00-00', '0000-00-00', NULL),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1, '0000-00-00', '0000-00-00', NULL),
(4146, 117, 'Olaines novads', '0801000', 1, '0000-00-00', '0000-00-00', NULL),
(4147, 117, 'Ozolnieku novads', '0546701', 1, '0000-00-00', '0000-00-00', NULL),
(4148, 117, 'Pārgaujas novads', '0427500', 1, '0000-00-00', '0000-00-00', NULL),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1, '0000-00-00', '0000-00-00', NULL),
(4150, 117, 'Pāvilostas novads', '0641401', 1, '0000-00-00', '0000-00-00', NULL),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1, '0000-00-00', '0000-00-00', NULL),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1, '0000-00-00', '0000-00-00', NULL),
(4153, 117, 'Pļaviņu novads', '0321400', 1, '0000-00-00', '0000-00-00', NULL),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1, '0000-00-00', '0000-00-00', NULL),
(4155, 117, 'Preiļu novads', '0760202', 1, '0000-00-00', '0000-00-00', NULL),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1, '0000-00-00', '0000-00-00', NULL),
(4157, 117, 'Priekules novads', '0641600', 1, '0000-00-00', '0000-00-00', NULL),
(4158, 117, 'Priekuļu novads', '0427300', 1, '0000-00-00', '0000-00-00', NULL),
(4159, 117, 'Raunas novads', '0427700', 1, '0000-00-00', '0000-00-00', NULL),
(4160, 117, 'Rēzekne', '0210000', 1, '0000-00-00', '0000-00-00', NULL),
(4161, 117, 'Rēzeknes novads', '0780200', 1, '0000-00-00', '0000-00-00', NULL),
(4162, 117, 'Riebiņu novads', '0766300', 1, '0000-00-00', '0000-00-00', NULL),
(4163, 117, 'Rīga', '0010000', 1, '0000-00-00', '0000-00-00', NULL),
(4164, 117, 'Rojas novads', '0888300', 1, '0000-00-00', '0000-00-00', NULL),
(4165, 117, 'Ropažu novads', '0808400', 1, '0000-00-00', '0000-00-00', NULL),
(4166, 117, 'Rucavas novads', '0648500', 1, '0000-00-00', '0000-00-00', NULL),
(4167, 117, 'Rugāju novads', '0387500', 1, '0000-00-00', '0000-00-00', NULL),
(4168, 117, 'Rundāles novads', '0407700', 1, '0000-00-00', '0000-00-00', NULL),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1, '0000-00-00', '0000-00-00', NULL),
(4170, 117, 'Rūjienas novads', '0961600', 1, '0000-00-00', '0000-00-00', NULL),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1, '0000-00-00', '0000-00-00', NULL),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1, '0000-00-00', '0000-00-00', NULL),
(4173, 117, 'Salacgrīvas novads', '0661400', 1, '0000-00-00', '0000-00-00', NULL),
(4174, 117, 'Salas novads', '0568700', 1, '0000-00-00', '0000-00-00', NULL),
(4175, 117, 'Salaspils novads', '0801200', 1, '0000-00-00', '0000-00-00', NULL),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1, '0000-00-00', '0000-00-00', NULL),
(4177, 117, 'Saldus novads', '0840200', 1, '0000-00-00', '0000-00-00', NULL),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1, '0000-00-00', '0000-00-00', NULL),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1, '0000-00-00', '0000-00-00', NULL),
(4180, 117, 'Saulkrastu novads', '0801400', 1, '0000-00-00', '0000-00-00', NULL),
(4181, 117, 'Seda, Strenču novads', '0941813', 1, '0000-00-00', '0000-00-00', NULL),
(4182, 117, 'Sējas novads', '0809200', 1, '0000-00-00', '0000-00-00', NULL),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1, '0000-00-00', '0000-00-00', NULL),
(4184, 117, 'Siguldas novads', '0801601', 1, '0000-00-00', '0000-00-00', NULL),
(4185, 117, 'Skrīveru novads', '0328200', 1, '0000-00-00', '0000-00-00', NULL),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1, '0000-00-00', '0000-00-00', NULL),
(4187, 117, 'Skrundas novads', '0621200', 1, '0000-00-00', '0000-00-00', NULL),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1, '0000-00-00', '0000-00-00', NULL),
(4189, 117, 'Smiltenes novads', '0941600', 1, '0000-00-00', '0000-00-00', NULL),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1, '0000-00-00', '0000-00-00', NULL),
(4191, 117, 'Stende, Talsu novads', '0880215', 1, '0000-00-00', '0000-00-00', NULL),
(4192, 117, 'Stopiņu novads', '0809600', 1, '0000-00-00', '0000-00-00', NULL),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1, '0000-00-00', '0000-00-00', NULL),
(4194, 117, 'Strenču novads', '0941800', 1, '0000-00-00', '0000-00-00', NULL),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1, '0000-00-00', '0000-00-00', NULL),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1, '0000-00-00', '0000-00-00', NULL),
(4197, 117, 'Talsu novads', '0880200', 1, '0000-00-00', '0000-00-00', NULL),
(4198, 117, 'Tērvetes novads', '0468900', 1, '0000-00-00', '0000-00-00', NULL),
(4199, 117, 'Tukuma novads', '0900200', 1, '0000-00-00', '0000-00-00', NULL),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1, '0000-00-00', '0000-00-00', NULL),
(4201, 117, 'Vaiņodes novads', '0649300', 1, '0000-00-00', '0000-00-00', NULL),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1, '0000-00-00', '0000-00-00', NULL),
(4203, 117, 'Valka, Valkas novads', '0940201', 1, '0000-00-00', '0000-00-00', NULL),
(4204, 117, 'Valkas novads', '0940200', 1, '0000-00-00', '0000-00-00', NULL),
(4205, 117, 'Valmiera', '0250000', 1, '0000-00-00', '0000-00-00', NULL),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1, '0000-00-00', '0000-00-00', NULL),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1, '0000-00-00', '0000-00-00', NULL),
(4208, 117, 'Varakļānu novads', '0701800', 1, '0000-00-00', '0000-00-00', NULL),
(4209, 117, 'Vārkavas novads', '0769101', 1, '0000-00-00', '0000-00-00', NULL),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1, '0000-00-00', '0000-00-00', NULL),
(4211, 117, 'Vecumnieku novads', '0409500', 1, '0000-00-00', '0000-00-00', NULL),
(4212, 117, 'Ventspils', '0270000', 1, '0000-00-00', '0000-00-00', NULL),
(4213, 117, 'Ventspils novads', '0980200', 1, '0000-00-00', '0000-00-00', NULL),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1, '0000-00-00', '0000-00-00', NULL),
(4215, 117, 'Viesītes novads', '0561800', 1, '0000-00-00', '0000-00-00', NULL),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1, '0000-00-00', '0000-00-00', NULL),
(4217, 117, 'Viļakas novads', '0381600', 1, '0000-00-00', '0000-00-00', NULL),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1, '0000-00-00', '0000-00-00', NULL),
(4219, 117, 'Viļānu novads', '0781800', 1, '0000-00-00', '0000-00-00', NULL),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1, '0000-00-00', '0000-00-00', NULL),
(4221, 117, 'Zilupes novads', '0681801', 1, '0000-00-00', '0000-00-00', NULL),
(4222, 43, 'Arica y Parinacota', 'AP', 1, '0000-00-00', '0000-00-00', NULL),
(4223, 43, 'Los Rios', 'LR', 1, '0000-00-00', '0000-00-00', NULL),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1, '0000-00-00', '0000-00-00', NULL),
(4225, 118, 'Beirut', 'LB-BR', 1, '0000-00-00', '0000-00-00', NULL),
(4226, 118, 'Bekaa', 'LB-BE', 1, '0000-00-00', '0000-00-00', NULL),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1, '0000-00-00', '0000-00-00', NULL),
(4228, 118, 'Nabatieh', 'LB-NB', 1, '0000-00-00', '0000-00-00', NULL),
(4229, 118, 'North', 'LB-NR', 1, '0000-00-00', '0000-00-00', NULL),
(4230, 118, 'South', 'LB-ST', 1, '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_of_identity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_number` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `districts` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_kode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `user_group_id`, `name`, `email`, `password`, `username`, `type_of_identity`, `identity_number`, `gender`, `phone`, `job`, `address`, `districts`, `city`, `province`, `zip_kode`, `picture`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Muhammad Akil', 'akil@admin.com', '$2y$10$MjTllzuhNkESq54KTja1mOEVW9Sta2MVcU4xQ8YxZif9zy0dtMJNe', 'akilsagitarius', 'KTP', '737571712910001', 'L', '082271111545', 'Cleaner source code', 'BTN ANTARA B7/12', 'Tamalanrea', 'Makassar', 'Sulawesi Selatan', '90245', 'akil.jpg', 'DiP1vl1cbNVAc5xXFQzziERgVX4SXdNHEd0MAg4edTd5GuJlPLUaWTenfpjV', '2017-08-25 16:00:00', '2017-08-28 05:22:13', NULL),
(2, 1, 'akshdkasjhd', 'kjhkajsh@akjs.com', '$2y$10$MjTllzuhNkESq54KTja1mOEVW9Sta2MVcU4xQ8YxZif9zy0dtMJNe', 'jhkjhkj', 'kjhk', 'kjasd', 'kjhkj', 'kjhkj', 'kjhkj', 'kjhkj', 'kjhkj', 'kjhkj', 'kjhkjh', 'kjhkj', 'hkjhkjh', '478douGAmqlaqOfqBh5NMCToJa0vdFPEgE2Zx1CWmEdpjt8G5laVCV1Z14Lm', '2017-08-28 10:56:00', '2017-09-03 07:47:47', NULL),
(3, 1, 'test', 'test@akil.com', '$2y$10$q3.b0zWjcz7UKy7x0k52LOo/4dtOLRIV9xLq5liEAvLUMvi/yZHG2', 'testing', 'KTP', '19823791873981', 'L', '085255633552', 'Programer', 'Kandea Komp. Unhas No.6', 'Bontoala', 'Makassar', 'Sulawesi Selatan', '90245', 'test.jpg', 'x8deWefd5gLGWYvL7hedz2ahqcwYR9UnCMlKcykBPTySjpHBly3p7Xb9d8h1', '2017-08-28 11:01:02', '2017-09-03 07:49:24', NULL),
(4, 10, 'anakgagah', 'anak@gagah.com', '1', 'anakgagah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-28 11:01:55', '2017-08-28 11:02:10', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_banner_image_description`
--
ALTER TABLE `oc_banner_image_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`id`,`store_id`);

--
-- Indexes for table `oc_confirm`
--
ALTER TABLE `oc_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_confirm_to_store`
--
ALTER TABLE `oc_confirm_to_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_country_hpwd`
--
ALTER TABLE `oc_country_hpwd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer_ban_ip`
--
ALTER TABLE `oc_customer_ban_ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_product_shipping_filtered`
--
ALTER TABLE `oc_product_shipping_filtered`
  ADD PRIMARY KEY (`filtered_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_affiliate`
--
ALTER TABLE `oc_affiliate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_affiliate_activity`
--
ALTER TABLE `oc_affiliate_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_affiliate_login`
--
ALTER TABLE `oc_affiliate_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_affiliate_transaction`
--
ALTER TABLE `oc_affiliate_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `oc_confirm`
--
ALTER TABLE `oc_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `oc_country_hpwd`
--
ALTER TABLE `oc_country_hpwd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `oc_customer_ban_ip`
--
ALTER TABLE `oc_customer_ban_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=451;
--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=262;
--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `oc_order_custom_field`
--
ALTER TABLE `oc_order_custom_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=442;
--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2368;
--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=546;
--
-- AUTO_INCREMENT for table `oc_product_shipping_filtered`
--
ALTER TABLE `oc_product_shipping_filtered`
  MODIFY `filtered_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=440;
--
-- AUTO_INCREMENT for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4384;
--
-- AUTO_INCREMENT for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_url_alias`
--
ALTER TABLE `oc_url_alias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=851;
--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4231;
--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
