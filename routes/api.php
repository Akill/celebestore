<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::resource('oc_addresses', 'oc_addressAPIController');

Route::resource('oc_affiliates', 'oc_affiliateAPIController');

Route::resource('oc_affiliate_activities', 'oc_affiliate_activityAPIController');

Route::resource('oc_affiliate_logins', 'oc_affiliate_loginAPIController');

Route::resource('oc_affiliate_transactions', 'oc_affiliate_transactionAPIController');

Route::resource('oc_apis', 'oc_apiAPIController');

Route::resource('oc_attributes', 'oc_attributeAPIController');

Route::resource('oc_attribute_descriptions', 'oc_attribute_descriptionAPIController');

Route::resource('oc_attribute_groups', 'oc_attribute_groupAPIController');

Route::resource('oc_attribute_group_descriptions', 'oc_attribute_group_descriptionAPIController');

Route::resource('oc_banners', 'oc_bannerAPIController');

Route::resource('oc_banner_images', 'oc_banner_imageAPIController');

Route::resource('oc_banner_image_descriptions', 'oc_banner_image_descriptionAPIController');

Route::resource('oc_categories', 'oc_categoryAPIController');

Route::resource('oc_category_descriptions', 'oc_category_descriptionAPIController');

Route::resource('oc_category_filters', 'oc_category_filterAPIController');

Route::resource('oc_category_paths', 'oc_category_pathAPIController');

Route::resource('oc_category_to_layouts', 'oc_category_to_layoutAPIController');

Route::resource('oc_category_to_stores', 'oc_category_to_storeAPIController');

Route::resource('oc_confirms', 'oc_confirmAPIController');

Route::resource('oc_confirm_to_stores', 'oc_confirm_to_storeAPIController');

Route::resource('oc_countries', 'oc_countryAPIController');

Route::resource('oc_country_hpwds', 'oc_country_hpwdAPIController');

Route::resource('oc_coupons', 'oc_couponAPIController');

Route::resource('oc_coupon_categories', 'oc_coupon_categoryAPIController');

Route::resource('oc_coupon_histories', 'oc_coupon_historyAPIController');

Route::resource('oc_coupon_products', 'oc_coupon_productAPIController');

Route::resource('oc_currencies', 'oc_currencyAPIController');

Route::resource('oc_customers', 'oc_customerAPIController');

Route::resource('oc_customer_activities', 'oc_customer_activityAPIController');

Route::resource('oc_customer_ban_ips', 'oc_customer_ban_ipAPIController');

Route::resource('oc_customer_groups', 'oc_customer_groupAPIController');

Route::resource('oc_customer_group_descriptions', 'oc_customer_group_descriptionAPIController');

Route::resource('oc_customer_histories', 'oc_customer_historyAPIController');

Route::resource('oc_customer_ips', 'oc_customer_ipAPIController');

Route::resource('oc_customer_logins', 'oc_customer_loginAPIController');

Route::resource('oc_customer_onlines', 'oc_customer_onlineAPIController');

Route::resource('oc_customer_rewards', 'oc_customer_rewardAPIController');

Route::resource('oc_customer_transactions', 'oc_customer_transactionAPIController');

Route::resource('oc_custom_fields', 'oc_custom_fieldAPIController');

Route::resource('oc_custom_field_customer_groups', 'oc_custom_field_customer_groupAPIController');

Route::resource('oc_custom_field_descriptions', 'oc_custom_field_descriptionAPIController');

Route::resource('oc_custom_field_values', 'oc_custom_field_valueAPIController');

Route::resource('oc_custom_field_descriptions', 'oc_custom_field_value_descriptionAPIController');

Route::resource('oc_downloads', 'oc_downloadAPIController');

Route::resource('oc_download_descriptions', 'oc_download_descriptionAPIController');

Route::resource('oc_events', 'oc_eventAPIController');

Route::resource('oc_extensions', 'oc_extensionAPIController');

Route::resource('oc_filters', 'oc_filterAPIController');

Route::resource('oc_filter_descriptions', 'oc_filter_descriptionAPIController');

Route::resource('oc_filter_groups', 'oc_filter_groupAPIController');

Route::resource('oc_filter_group_descriptions', 'oc_filter_group_descriptionAPIController');

Route::resource('oc_geo_zones', 'oc_geo_zoneAPIController');

Route::resource('oc_informations', 'oc_informationAPIController');

Route::resource('oc_information_descriptions', 'oc_information_descriptionAPIController');

Route::resource('oc_information_to_layouts', 'oc_information_to_layoutAPIController');

Route::resource('oc_information_to_stores', 'oc_information_to_storeAPIController');

Route::resource('oc_languages', 'oc_languageAPIController');

Route::resource('oc_layouts', 'oc_layoutAPIController');

Route::resource('oc_layout_modules', 'oc_layout_moduleAPIController');

Route::resource('oc_layout_routes', 'oc_layout_routeAPIController');

Route::resource('oc_length_classes', 'oc_length_classAPIController');

Route::resource('oc_length_class_descriptions', 'oc_length_class_descriptionAPIController');

Route::resource('oc_locations', 'oc_locationAPIController');

Route::resource('oc_manufacturers', 'oc_manufacturerAPIController');

Route::resource('oc_manufacturer_to_stores', 'oc_manufacturer_to_storeAPIController');

Route::resource('oc_marketings', 'oc_marketingAPIController');

Route::resource('oc_modifications', 'oc_modificationAPIController');

Route::resource('oc_modules', 'oc_moduleAPIController');

Route::resource('oc_options', 'oc_optionAPIController');

Route::resource('oc_option_descriptions', 'oc_option_descriptionAPIController');

Route::resource('oc_option_values', 'oc_option_valueAPIController');

Route::resource('oc_option_value_descriptions', 'oc_option_value_descriptionAPIController');

Route::resource('oc_orders', 'oc_orderAPIController');

Route::resource('oc_order_custom_fields', 'oc_order_custom_fieldAPIController');

Route::resource('oc_order_histories', 'oc_order_historyAPIController');

Route::resource('oc_order_options', 'oc_order_optionAPIController');

Route::resource('oc_order_products', 'oc_order_productAPIController');

Route::resource('oc_order_recurrings', 'oc_order_recurringAPIController');

Route::resource('oc_order_recurring_transactions', 'oc_order_recurring_transactionAPIController');

Route::resource('oc_order_statuses', 'oc_order_statusAPIController');

Route::resource('oc_order_totals', 'oc_order_totalAPIController');

Route::resource('oc_order_vouchers', 'oc_order_voucherAPIController');

Route::resource('oc_products', 'oc_productAPIController');

Route::resource('oc_product_attributes', 'oc_product_attributeAPIController');

Route::resource('oc_product_descriptions', 'oc_product_descriptionAPIController');

Route::resource('oc_product_discounts', 'oc_product_discountAPIController');

Route::resource('oc_product_filters', 'oc_product_filterAPIController');

Route::resource('oc_product_images', 'oc_product_imageAPIController');

Route::resource('oc_product_options', 'oc_product_optionAPIController');

Route::resource('oc_product_option_values', 'oc_product_option_valueAPIController');

Route::resource('oc_product_recurrings', 'oc_product_recurringAPIController');

Route::resource('oc_product_relateds', 'oc_product_relatedAPIController');

Route::resource('oc_product_rewards', 'oc_product_rewardAPIController');

Route::resource('oc_product_shipping_filtereds', 'oc_product_shipping_filteredAPIController');

Route::resource('oc_product_specials', 'oc_product_specialAPIController');

Route::resource('oc_product_to_categories', 'oc_product_to_categoryAPIController');

Route::resource('oc_product_to_downloads', 'oc_product_to_downloadAPIController');

Route::resource('oc_product_to_layouts', 'oc_product_to_layoutAPIController');

Route::resource('oc_product_to_stores', 'oc_product_to_storeAPIController');

Route::resource('oc_recurrings', 'oc_recurringAPIController');

Route::resource('oc_recurring_descriptions', 'oc_recurring_descriptionAPIController');

Route::resource('oc_returns', 'oc_returnAPIController');

Route::resource('oc_return_actions', 'oc_return_actionAPIController');

Route::resource('oc_return_histories', 'oc_return_historyAPIController');

Route::resource('oc_return_reasons', 'oc_return_reasonAPIController');

Route::resource('oc_return_statuses', 'oc_return_statusAPIController');

Route::resource('oc_reviews', 'oc_reviewAPIController');

Route::resource('oc_settings', 'oc_settingAPIController');

Route::resource('oc_stock_statuses', 'oc_stock_statusAPIController');

Route::resource('oc_stores', 'oc_storeAPIController');

Route::resource('oc_tax_classes', 'oc_tax_classAPIController');

Route::resource('oc_tax_rates', 'oc_tax_rateAPIController');

Route::resource('oc_tax_rate_to_customer_groups', 'oc_tax_rate_to_customer_groupAPIController');

Route::resource('oc_tax_rules', 'oc_tax_ruleAPIController');

Route::resource('oc_uploads', 'oc_uploadAPIController');

Route::resource('oc_url_aliases', 'oc_url_aliasAPIController');

Route::resource('oc_users', 'oc_userAPIController');

Route::resource('oc_user_groups', 'oc_user_groupAPIController');

Route::resource('oc_vouchers', 'oc_voucherAPIController');

Route::resource('oc_voucher_histories', 'oc_voucher_historyAPIController');

Route::resource('oc_voucher_themes', 'oc_voucher_themeAPIController');

Route::resource('oc_voucher_theme_descriptions', 'oc_voucher_theme_descriptionAPIController');

Route::resource('oc_weight_classes', 'oc_weight_classAPIController');

Route::resource('oc_weight_class_descriptions', 'oc_weight_class_descriptionAPIController');

Route::resource('oc_zones', 'oc_zoneAPIController');

Route::resource('oc_zone_to_geo_zones', 'oc_zone_to_geo_zoneAPIController');