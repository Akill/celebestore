<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
// Route::get('/', function () {
//   return view('contents.home.index');
// });

Auth::routes();

Route::get('/', 'front_homeController@index')->name('front.home');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function()
{

	Route::resource('ocAddresses', 'oc_addressController');

	Route::resource('ocAffiliates', 'oc_affiliateController');

	Route::resource('ocAffiliateActivities', 'oc_affiliate_activityController');

	Route::resource('ocAffiliateLogins', 'oc_affiliate_loginController');

	Route::resource('ocAffiliateTransactions', 'oc_affiliate_transactionController');

	Route::resource('ocApis', 'oc_apiController');

	Route::resource('ocAttributes', 'oc_attributeController');

	Route::resource('ocAttributeDescriptions', 'oc_attribute_descriptionController');

	Route::resource('ocAttributeGroups', 'oc_attribute_groupController');

	Route::resource('ocAttributeGroupDescriptions', 'oc_attribute_group_descriptionController');

	Route::resource('ocBanners', 'oc_bannerController');

	Route::resource('ocBannerImages', 'oc_banner_imageController');

	Route::resource('ocBannerImageDescriptions', 'oc_banner_image_descriptionController');

	Route::resource('ocCategories', 'oc_categoryController');

	Route::resource('ocCategoryDescriptions', 'oc_category_descriptionController');

	Route::resource('ocCategoryFilters', 'oc_category_filterController');

	Route::resource('ocCategoryPaths', 'oc_category_pathController');

	Route::resource('ocCategoryToLayouts', 'oc_category_to_layoutController');

	Route::resource('ocCategoryToStores', 'oc_category_to_storeController');

	Route::resource('ocConfirms', 'oc_confirmController');

	Route::resource('ocConfirmToStores', 'oc_confirm_to_storeController');

	Route::resource('ocCountries', 'oc_countryController');

	Route::resource('ocCountryHpwds', 'oc_country_hpwdController');

	Route::resource('ocCoupons', 'oc_couponController');

	Route::resource('ocCouponCategories', 'oc_coupon_categoryController');

	Route::resource('ocCouponHistories', 'oc_coupon_historyController');

	Route::resource('ocCouponProducts', 'oc_coupon_productController');

	Route::resource('ocCurrencies', 'oc_currencyController');

	Route::resource('ocCustomers', 'oc_customerController');

	Route::resource('ocCustomerActivities', 'oc_customer_activityController');

	Route::resource('ocCustomerBanIps', 'oc_customer_ban_ipController');

	Route::resource('ocCustomerGroups', 'oc_customer_groupController');

	Route::resource('ocCustomerGroupDescriptions', 'oc_customer_group_descriptionController');

	Route::resource('ocCustomerHistories', 'oc_customer_historyController');

	Route::resource('ocCustomerIps', 'oc_customer_ipController');

	Route::resource('ocCustomerLogins', 'oc_customer_loginController');

	Route::resource('ocCustomerOnlines', 'oc_customer_onlineController');

	Route::resource('ocCustomerRewards', 'oc_customer_rewardController');

	Route::resource('ocCustomerTransactions', 'oc_customer_transactionController');

	Route::resource('ocCustomFields', 'oc_custom_fieldController');

	Route::resource('ocCustomFieldCustomerGroups', 'oc_custom_field_customer_groupController');

	Route::resource('ocCustomFieldDescriptions', 'oc_custom_field_descriptionController');

	Route::resource('ocCustomFieldValues', 'oc_custom_field_valueController');

	Route::resource('ocCustomFieldValueDescriptions', 'oc_custom_field_value_descriptionController');

	Route::resource('ocDownloads', 'oc_downloadController');

	Route::resource('ocDownloadDescriptions', 'oc_download_descriptionController');

	Route::resource('ocEvents', 'oc_eventController');

	Route::resource('ocExtensions', 'oc_extensionController');

	Route::resource('ocFilters', 'oc_filterController');

	Route::resource('ocFilterDescriptions', 'oc_filter_descriptionController');

	Route::resource('ocGeoZones', 'oc_geo_zoneController');

	Route::resource('ocInformations', 'oc_informationController');

	Route::resource('ocInformationDescriptions', 'oc_information_descriptionController');

	Route::resource('ocLanguages', 'oc_languageController');

	Route::resource('ocLayoutModules', 'oc_layout_moduleController');

	Route::resource('ocLayoutRoutes', 'oc_layout_routeController');

	Route::resource('ocLengthClassDescriptions', 'oc_length_class_descriptionController');

	Route::resource('ocLocations', 'oc_locationController');

	Route::resource('ocManufacturers', 'oc_manufacturerController');

	Route::resource('ocMarketings', 'oc_marketingController');

	Route::resource('ocModifications', 'oc_modificationController');

	Route::resource('ocModules', 'oc_moduleController');

	Route::resource('ocOptionValues', 'oc_option_valueController');

	Route::resource('ocOptionValueDescriptions', 'oc_option_value_descriptionController');

	Route::resource('ocOrders', 'oc_orderController');

	Route::resource('ocOrderCustomFields', 'oc_order_custom_fieldController');

	Route::resource('ocOrderHistories', 'oc_order_historyController');

	Route::resource('ocOrderOptions', 'oc_order_optionController');

	Route::resource('ocOrderProducts', 'oc_order_productController');

	Route::resource('ocOrderRecurrings', 'oc_order_recurringController');

	Route::resource('ocOrderRecurringTransactions', 'oc_order_recurring_transactionController');

	Route::resource('ocOrderTotals', 'oc_order_totalController');

	Route::resource('ocOrderVouchers', 'oc_order_voucherController');

	Route::resource('ocProducts', 'oc_productController');

	Route::resource('ocProductAttributes', 'oc_product_attributeController');

	Route::resource('ocProductDescriptions', 'oc_product_descriptionController');

	Route::resource('ocProductDiscounts', 'oc_product_discountController');

	Route::resource('ocProductImages', 'oc_product_imageController');

	Route::resource('ocProductOptions', 'oc_product_optionController');

	Route::resource('ocProductOptionValues', 'oc_product_option_valueController');

	Route::resource('ocProductRewards', 'oc_product_rewardController');

	Route::resource('ocProductSpecials', 'oc_product_specialController');

	Route::resource('ocRecurrings', 'oc_recurringController');

	Route::resource('ocReturns', 'oc_returnController');

	Route::resource('ocReturnHistories', 'oc_return_historyController');

	Route::resource('ocReviews', 'oc_reviewController');

	Route::resource('ocSettings', 'oc_settingController');

	Route::resource('ocStores', 'oc_storeController');

	Route::resource('ocTaxClasses', 'oc_tax_classController');

	Route::resource('ocTaxRates', 'oc_tax_rateController');

	Route::resource('ocTaxRules', 'oc_tax_ruleController');

	Route::resource('ocUploads', 'oc_uploadController');

	Route::resource('ocUsers', 'oc_userController');

	Route::resource('ocUserGroups', 'oc_user_groupController');

	Route::resource('ocVouchers', 'oc_voucherController');

	Route::resource('ocVoucherHistories', 'oc_voucher_historyController');

	Route::resource('ocWeightClassDescriptions', 'oc_weight_class_descriptionController');

	Route::resource('ocZones', 'oc_zoneController');

	Route::resource('ocZoneToGeoZones', 'oc_zone_to_geo_zoneController');

	Route::resource('users', 'userController');

	Route::resource('ocProducts', 'oc_productController');

	Route::resource('ocProductAttributes', 'oc_product_attributeController');

	Route::resource('ocProductDescriptions', 'oc_product_descriptionController');

	Route::resource('ocProductDiscounts', 'oc_product_discountController');

	Route::resource('ocCustomerGroupDescriptions', 'oc_customer_group_descriptionController');

	Route::resource('ocProductToCategories', 'oc_product_to_categoryController');

	Route::resource('ocProductToDownloads', 'oc_product_to_downloadController');

	Route::resource('ocProductToLayouts', 'oc_product_to_layoutController');

	Route::resource('ocProductToStores', 'oc_product_to_storeController');

	Route::resource('ocCategoryDescriptions', 'oc_category_descriptionController');

	Route::resource('ocProductToCategories', 'oc_product_to_categoryController');

});


Route::group(['prefix' => '/cart'], function() {
	Route::get('/', 'front_categoryController@cart')->name('front.cart');
	Route::post('/add', 'front_categoryController@add')->name('cart.add');
	Route::get('/clear', 'front_categoryController@clear')->name('cart.clear');
	Route::post('/remove/{id}', 'front_categoryController@remove')->name('cart.remove');
	Route::post('/update/{id}', 'front_categoryController@update')->name('cart.update');
});

Route::get('/{category}', 'front_categoryController@show')->name('front.category');