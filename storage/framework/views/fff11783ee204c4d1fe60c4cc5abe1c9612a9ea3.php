<!-- Sub District Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sub_district_id', 'Sub District Id:'); ?>

    <?php echo Form::number('sub_district_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('customer_id', 'Customer Id:'); ?>

    <?php echo Form::number('customer_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('firstname', 'Firstname:'); ?>

    <?php echo Form::text('firstname', null, ['class' => 'form-control']); ?>

</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('lastname', 'Lastname:'); ?>

    <?php echo Form::text('lastname', null, ['class' => 'form-control']); ?>

</div>

<!-- Company Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('company', 'Company:'); ?>

    <?php echo Form::text('company', null, ['class' => 'form-control']); ?>

</div>

<!-- Address 1 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('address_1', 'Address 1:'); ?>

    <?php echo Form::text('address_1', null, ['class' => 'form-control']); ?>

</div>

<!-- Address 2 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('address_2', 'Address 2:'); ?>

    <?php echo Form::text('address_2', null, ['class' => 'form-control']); ?>

</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('city', 'City:'); ?>

    <?php echo Form::text('city', null, ['class' => 'form-control']); ?>

</div>

<!-- Postcode Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('postcode', 'Postcode:'); ?>

    <?php echo Form::text('postcode', null, ['class' => 'form-control']); ?>

</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('country_id', 'Country Id:'); ?>

    <?php echo Form::number('country_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Zone Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('zone_id', 'Zone Id:'); ?>

    <?php echo Form::number('zone_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('custom_field', 'Custom Field:'); ?>

    <?php echo Form::textarea('custom_field', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocAddresses.index'); ?>" class="btn btn-default">Cancel</a>
</div>
