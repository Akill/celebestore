<!-- Product Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('product_id', 'Product:'); ?>

    <?php  $items = App\Models\oc_product::has('detail')->get();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e(App\Models\oc_product_description::find($i->id)->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Attribute Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('attribute_id', 'Attribute Id:'); ?>

    <?php  $items = App\Models\oc_attribute::all();  ?>
    <select class="form-control" id="attribute_id" name="attribute_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e(App\Models\oc_attribute_description::where('attribute_id',$i->id)->first()->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <?php  $items = App\Models\oc_language::all(['name','id']);  ?>
    <select class="form-control" id="language_id" name="language_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Text Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('text', 'Text:'); ?>

    <?php echo Form::textarea('text', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductAttributes.index'); ?>" class="btn btn-default">Cancel</a>
</div>

<!-- textarea script from npm (bootstrap3-wysihtml5-bower) -->