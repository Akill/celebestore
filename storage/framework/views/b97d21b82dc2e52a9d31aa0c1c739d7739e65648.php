<table class="table table-responsive" id="ocEvents-table">
    <thead>
        <th>Code</th>
        <th>Trigger</th>
        <th>Action</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocEvents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocEvent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocEvent->code; ?></td>
            <td><?php echo $ocEvent->trigger; ?></td>
            <td><?php echo $ocEvent->action; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocEvents.destroy', $ocEvent->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocEvents.show', [$ocEvent->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocEvents.edit', [$ocEvent->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>