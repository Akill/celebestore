<table class="table table-responsive" id="ocAffiliateLogins-table">
    <thead>
        <th>Email</th>
        <th>Ip</th>
        <th>Total</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAffiliateLogins; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAffiliateLogin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAffiliateLogin->email; ?></td>
            <td><?php echo $ocAffiliateLogin->ip; ?></td>
            <td><?php echo $ocAffiliateLogin->total; ?></td>
            <td><?php echo $ocAffiliateLogin->date_added; ?></td>
            <td><?php echo $ocAffiliateLogin->date_modified; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAffiliateLogins.destroy', $ocAffiliateLogin->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAffiliateLogins.show', [$ocAffiliateLogin->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAffiliateLogins.edit', [$ocAffiliateLogin->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>