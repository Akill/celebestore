<table class="table table-responsive" id="ocBannerImages-table">
    <thead>
        <th>Banner Id</th>
        <th>Link</th>
        <th>Image</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocBannerImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocBannerImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocBannerImage->banner_id; ?></td>
            <td><?php echo $ocBannerImage->link; ?></td>
            <td><?php echo $ocBannerImage->image; ?></td>
            <td><?php echo $ocBannerImage->sort_order; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocBannerImages.destroy', $ocBannerImage->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocBannerImages.show', [$ocBannerImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocBannerImages.edit', [$ocBannerImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>