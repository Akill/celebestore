<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocBannerImageDescription->id; ?></p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <p><?php echo $ocBannerImageDescription->language_id; ?></p>
</div>

<!-- Banner Id Field -->
<div class="form-group">
    <?php echo Form::label('banner_id', 'Banner Id:'); ?>

    <p><?php echo $ocBannerImageDescription->banner_id; ?></p>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('title', 'Title:'); ?>

    <p><?php echo $ocBannerImageDescription->title; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocBannerImageDescription->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocBannerImageDescription->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocBannerImageDescription->deleted_at; ?></p>
</div>

