<?php  
$active = Request::is('ocOrders*') ? 'active' : Request::is('ocOrderCustomFields*') ? 'active' : Request::is('ocOrderHistories*') ? 'active' : Request::is('ocOrderOptions*') ? 'active' : Request::is('ocOrderProducts*') ? 'active' : Request::is('ocOrderRecurrings*') ? 'active' : Request::is('ocOrderRecurringTransactions*') ? 'active' : Request::is('ocOrderTotals*') ? 'active' : Request::is('ocOrderVouchers*') ? 'active' : ''; 
 ?>

<li class="<?php echo e($active); ?> treeview menu-open">
    <a href="#">
        <span>Orders</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocOrders*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrders.index'); ?>"><i class="fa fa-table"></i><span>orders</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderCustomFields*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderCustomFields.index'); ?>"><i class="fa fa-table"></i><span>order custom fields</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderHistories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderHistories.index'); ?>"><i class="fa fa-table"></i><span>order histories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderOptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderOptions.index'); ?>"><i class="fa fa-table"></i><span>order options</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderProducts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderProducts.index'); ?>"><i class="fa fa-table"></i><span>order products</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderRecurrings*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderRecurrings.index'); ?>"><i class="fa fa-table"></i><span>order recurrings</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderRecurringTransactions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderRecurringTransactions.index'); ?>"><i class="fa fa-table"></i><span>order recurring transactions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderTotals*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderTotals.index'); ?>"><i class="fa fa-table"></i><span>order totals</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocOrderVouchers*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocOrderVouchers.index'); ?>"><i class="fa fa-table"></i><span>order vouchers</span></a>
        </li>
    </ul>
</li>