<!-- Product Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('product_id', 'Product Id:'); ?>

    <?php echo Form::number('product_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('category_id', 'Category Id:'); ?>

    <?php echo Form::number('category_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductToCategories.index'); ?>" class="btn btn-default">Cancel</a>
</div>
