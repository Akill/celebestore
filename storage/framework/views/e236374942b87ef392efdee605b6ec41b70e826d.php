<!-- Model Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('model', 'Model:'); ?>

    <?php echo Form::text('model', null, ['class' => 'form-control']); ?>

</div>

<!-- Sku Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sku', 'Sku:'); ?>

    <?php echo Form::text('sku', null, ['class' => 'form-control']); ?>

</div>

<!-- Upc Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('upc', 'Upc:'); ?>

    <?php echo Form::text('upc', null, ['class' => 'form-control']); ?>

</div>

<!-- Ean Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('ean', 'Ean:'); ?>

    <?php echo Form::text('ean', null, ['class' => 'form-control']); ?>

</div>

<!-- Jan Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('jan', 'Jan:'); ?>

    <?php echo Form::text('jan', null, ['class' => 'form-control']); ?>

</div>

<!-- Isbn Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('isbn', 'Isbn:'); ?>

    <?php echo Form::text('isbn', null, ['class' => 'form-control']); ?>

</div>

<!-- Mpn Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('mpn', 'Mpn:'); ?>

    <?php echo Form::text('mpn', null, ['class' => 'form-control']); ?>

</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('location', 'Location:'); ?>

    <?php echo Form::text('location', null, ['class' => 'form-control']); ?>

</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <?php echo Form::number('quantity', null, ['class' => 'form-control']); ?>

</div>

<!-- Stock Status Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('stock_status_id', 'Stock Status:'); ?>

    <?php  $items = App\Models\oc_stock_status::all(['name','id'])  ?>
    <select class="form-control" id="stock_status_id" name="stock_status_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<div class="form-group col-sm-6">
<img src="http://placehold.it/250x250" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::file('image',null,array('id'=>'image','class'=>'form-control')); ?>

</div>

<!-- Manufacturer Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('manufacturer_id', 'Manufacturer Id:'); ?>

    <?php  $items = App\Models\oc_manufacturer::all(['name','id'])  ?>
    <select class="form-control" id="manufacturer_id" name="manufacturer_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Shipping Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping', 'Shipping:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('shipping', false); ?>

        <?php echo Form::checkbox('shipping', '1', '0'); ?> 1
    </label>
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('price', 'Price:'); ?>

    <?php echo Form::number('price', null, ['class' => 'form-control']); ?>

</div>

<!-- Points Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('points', 'Points:'); ?>

    <?php echo Form::number('points', null, ['class' => 'form-control']); ?>

</div>

<!-- Tax Class Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('tax_class_id', 'Tax Class Id:'); ?>

    <?php  $items = App\Models\oc_tax_class::all(['title','id'])  ?>
    <select class="form-control" id="tax_class_id" name="tax_class_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->title); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Date Available Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_available', 'Date Available:'); ?>

    <?php echo Form::date('date_available', null, ['class' => 'form-control']); ?>

</div>

<!-- Weight Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('weight', 'Weight:'); ?>

    <?php echo Form::number('weight', null, ['class' => 'form-control']); ?>

</div>

<!-- Weight Class Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('weight_class_id', 'Weight Class Id:'); ?>

    <?php  $items = App\Models\oc_weight_class::all(['value','id'])  ?>
    <select class="form-control" id="weight_class_id" name="weight_class_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->value); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Length Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('length', 'Length:'); ?>

    <?php echo Form::number('length', null, ['class' => 'form-control']); ?>

</div>

<!-- Width Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('width', 'Width:'); ?>

    <?php echo Form::number('width', null, ['class' => 'form-control']); ?>

</div>

<!-- Height Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('height', 'Height:'); ?>

    <?php echo Form::number('height', null, ['class' => 'form-control']); ?>

</div>

<!-- Length Class Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('length_class_id', 'Length Class Id:'); ?>

    <?php  $items = App\Models\oc_length_class::all(['value','id'])  ?>
    <select class="form-control" id="length_class_id" name="length_class_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->value); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Subtract Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('subtract', 'Subtract:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('subtract', false); ?>

        <?php echo Form::checkbox('subtract', '1', '0'); ?> 1
    </label>
</div>

<!-- Minimum Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('minimum', 'Minimum:'); ?>

    <?php echo Form::number('minimum', null, ['class' => 'form-control']); ?>

</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <?php echo Form::number('sort_order', null, ['class' => 'form-control']); ?>

</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('status', false); ?>

        <?php echo Form::checkbox('status', '1', '0'); ?> 1
    </label>
</div>

<!-- Viewed Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('viewed', 'Viewed:'); ?>

    <?php echo Form::number('viewed', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <?php echo Form::date('date_added', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_modified', 'Date Modified:'); ?>

    <?php echo Form::date('date_modified', null, ['class' => 'form-control']); ?>

</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProducts.index'); ?>" class="btn btn-default">Cancel</a>
</div>
