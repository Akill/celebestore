<!-- Product Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('product_id', 'Product:'); ?>

    <?php  $items = App\Models\oc_product::has('detail')->get();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e(App\Models\oc_product_description::find($i->id)->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Option Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('option_id', 'Option Id:'); ?>

    <?php echo Form::number('option_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('value', 'Value:'); ?>

    <?php echo Form::textarea('value', null, ['class' => 'form-control']); ?>

</div>

<!-- Required Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('required', 'Required:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('required', false); ?>

        <?php echo Form::checkbox('required', '1', null); ?> 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductOptions.index'); ?>" class="btn btn-default">Cancel</a>
</div>
