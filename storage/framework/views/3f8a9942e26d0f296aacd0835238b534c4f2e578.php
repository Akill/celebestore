<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('code', 'Code:'); ?>

    <?php echo Form::text('code', null, ['class' => 'form-control']); ?>

</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <?php echo Form::text('status', null, ['class' => 'form-control']); ?>

</div>

<!-- Address Format Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('address_format', 'Address Format:'); ?>

    <?php echo Form::text('address_format', null, ['class' => 'form-control']); ?>

</div>

<!-- Iso Code 2 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('iso_code_2', 'Iso Code 2:'); ?>

    <?php echo Form::text('iso_code_2', null, ['class' => 'form-control']); ?>

</div>

<!-- Iso Code 3 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('iso_code_3', 'Iso Code 3:'); ?>

    <?php echo Form::text('iso_code_3', null, ['class' => 'form-control']); ?>

</div>

<!-- Postcode Required Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('postcode_required', 'Postcode Required:'); ?>

    <?php echo Form::text('postcode_required', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocCountries.index'); ?>" class="btn btn-default">Cancel</a>
</div>
