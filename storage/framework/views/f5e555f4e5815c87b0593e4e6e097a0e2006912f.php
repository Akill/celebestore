<table class="table table-responsive" id="ocProductToLayouts-table">
    <thead>
        <th>Store Id</th>
        <th>Layout Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductToLayouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductToLayout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductToLayout->store_id; ?></td>
            <td><?php echo $ocProductToLayout->layout_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductToLayouts.destroy', $ocProductToLayout->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductToLayouts.show', [$ocProductToLayout->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductToLayouts.edit', [$ocProductToLayout->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>