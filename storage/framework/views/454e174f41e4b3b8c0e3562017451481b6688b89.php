<table class="table table-responsive" id="ocProductDescriptions-table">
    <thead>
        <th>Product</th>
        <th>Language</th>
        <th>Name</th>
        <!-- <th>Description</th> -->
        <th>Tag</th>
        <th>Meta Title</th>
        <!-- <th>Meta Description</th> -->
        <!-- <th>Meta Keyword</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductDescription->product_id; ?></td>
            <td><?php echo App\Models\oc_language::find($ocProductDescription->language_id)->name; ?></td>
            <td><?php echo $ocProductDescription->name; ?></td>
            <!-- <td><?php echo $ocProductDescription->description; ?></td> -->
            <td><?php echo $ocProductDescription->tag; ?></td>
            <td><?php echo $ocProductDescription->meta_title; ?></td>
            <!-- <td><?php echo $ocProductDescription->meta_description; ?></td> -->
            <!-- <td><?php echo $ocProductDescription->meta_keyword; ?></td> -->
            <td>
                <?php echo Form::open(['route' => ['ocProductDescriptions.destroy', $ocProductDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductDescriptions.show', [$ocProductDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductDescriptions.edit', [$ocProductDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>