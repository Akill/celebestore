<table class="table table-responsive" id="ocCategoryDescriptions-table">
    <thead>
        <th>Oc Category Id</th>
        <th>Language Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Meta Title</th>
        <th>Meta Description</th>
        <th>Meta Keyword</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCategoryDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCategoryDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCategoryDescription->oc_category_id; ?></td>
            <td><?php echo $ocCategoryDescription->language_id; ?></td>
            <td><?php echo $ocCategoryDescription->name; ?></td>
            <td><?php echo $ocCategoryDescription->description; ?></td>
            <td><?php echo $ocCategoryDescription->meta_title; ?></td>
            <td><?php echo $ocCategoryDescription->meta_description; ?></td>
            <td><?php echo $ocCategoryDescription->meta_keyword; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCategoryDescriptions.destroy', $ocCategoryDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCategoryDescriptions.show', [$ocCategoryDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCategoryDescriptions.edit', [$ocCategoryDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>