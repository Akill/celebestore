<table class="table table-responsive" id="ocZoneToGeoZones-table">
    <thead>
        <th>Country Id</th>
        <th>Zone Id</th>
        <th>Geo Zone Id</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocZoneToGeoZones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocZoneToGeoZone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocZoneToGeoZone->country_id; ?></td>
            <td><?php echo $ocZoneToGeoZone->zone_id; ?></td>
            <td><?php echo $ocZoneToGeoZone->geo_zone_id; ?></td>
            <td><?php echo $ocZoneToGeoZone->date_added; ?></td>
            <td><?php echo $ocZoneToGeoZone->date_modified; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocZoneToGeoZones.destroy', $ocZoneToGeoZone->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocZoneToGeoZones.show', [$ocZoneToGeoZone->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocZoneToGeoZones.edit', [$ocZoneToGeoZone->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>