<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>
            Oc Banner Image Description
        </h1>
   </section>
   <div class="content">
       <?php echo $__env->make('adminlte-templates::common.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   <?php echo Form::model($ocBannerImageDescription, ['route' => ['ocBannerImageDescriptions.update', $ocBannerImageDescription->id], 'method' => 'patch']); ?>


                        <?php echo $__env->make('oc_banner_image_descriptions.fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                   <?php echo Form::close(); ?>

               </div>
           </div>
       </div>
   </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>