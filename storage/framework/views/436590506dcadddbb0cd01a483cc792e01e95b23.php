<table class="table table-responsive" id="ocProductDiscounts-table">
    <thead>
        <th>Product</th>
        <th>Customer Group</th>
        <th>Quantity</th>
        <th>Priority</th>
        <th>Price</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductDiscounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductDiscount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo App\Models\oc_product_description::where('product_id', $ocProductDiscount->product_id)->first()->name; ?></td>
            <td><?php echo App\Models\oc_customer_group_description::where('customer_group_id', $ocProductDiscount->customer_group_id)->first()->name; ?></td>
            <td><?php echo $ocProductDiscount->quantity; ?></td>
            <td><?php echo $ocProductDiscount->priority; ?></td>
            <td><?php echo $ocProductDiscount->price; ?></td>
            <td><?php echo $ocProductDiscount->date_start; ?></td>
            <td><?php echo $ocProductDiscount->date_end; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductDiscounts.destroy', $ocProductDiscount->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductDiscounts.show', [$ocProductDiscount->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductDiscounts.edit', [$ocProductDiscount->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>