<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocZone->id; ?></p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    <?php echo Form::label('country_id', 'Country Id:'); ?>

    <p><?php echo $ocZone->country_id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $ocZone->name; ?></p>
</div>

<!-- Code Field -->
<div class="form-group">
    <?php echo Form::label('code', 'Code:'); ?>

    <p><?php echo $ocZone->code; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $ocZone->status; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocZone->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocZone->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocZone->deleted_at; ?></p>
</div>

