<table class="table table-responsive" id="ocInformations-table">
    <thead>
        <th>Bottom</th>
        <th>Sort Order</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocInformations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocInformation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocInformation->bottom; ?></td>
            <td><?php echo $ocInformation->sort_order; ?></td>
            <td><?php echo $ocInformation->status; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocInformations.destroy', $ocInformation->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocInformations.show', [$ocInformation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocInformations.edit', [$ocInformation->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>