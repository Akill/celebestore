<!-- Oc Category Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('oc_category_id', 'Oc Category Id:'); ?>

    <?php echo Form::number('oc_category_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <?php echo Form::number('language_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('description', 'Description:'); ?>

    <?php echo Form::textarea('description', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_title', 'Meta Title:'); ?>

    <?php echo Form::text('meta_title', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Description Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_description', 'Meta Description:'); ?>

    <?php echo Form::text('meta_description', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Keyword Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_keyword', 'Meta Keyword:'); ?>

    <?php echo Form::text('meta_keyword', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocCategoryDescriptions.index'); ?>" class="btn btn-default">Cancel</a>
</div>
