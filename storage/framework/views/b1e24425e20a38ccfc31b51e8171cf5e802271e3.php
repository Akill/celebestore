<table class="table table-responsive" id="ocAffiliates-table">
    <thead>
        <th>Sub District Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Company</th>
        <th>Website</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>Postcode</th>
        <th>Country Id</th>
        <th>Zone Id</th>
        <th>Code</th>
        <th>Commission</th>
        <th>Tax</th>
        <th>Payment</th>
        <th>Cheque</th>
        <th>Paypal</th>
        <th>Bank Name</th>
        <th>Bank Branch Number</th>
        <th>Bank Swift Code</th>
        <th>Bank Account Name</th>
        <th>Bank Account Number</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Approved</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAffiliates; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAffiliate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAffiliate->sub_district_id; ?></td>
            <td><?php echo $ocAffiliate->firstname; ?></td>
            <td><?php echo $ocAffiliate->lastname; ?></td>
            <td><?php echo $ocAffiliate->email; ?></td>
            <td><?php echo $ocAffiliate->telephone; ?></td>
            <td><?php echo $ocAffiliate->fax; ?></td>
            <td><?php echo $ocAffiliate->password; ?></td>
            <td><?php echo $ocAffiliate->salt; ?></td>
            <td><?php echo $ocAffiliate->company; ?></td>
            <td><?php echo $ocAffiliate->website; ?></td>
            <td><?php echo $ocAffiliate->address_1; ?></td>
            <td><?php echo $ocAffiliate->address_2; ?></td>
            <td><?php echo $ocAffiliate->city; ?></td>
            <td><?php echo $ocAffiliate->postcode; ?></td>
            <td><?php echo $ocAffiliate->country_id; ?></td>
            <td><?php echo $ocAffiliate->zone_id; ?></td>
            <td><?php echo $ocAffiliate->code; ?></td>
            <td><?php echo $ocAffiliate->commission; ?></td>
            <td><?php echo $ocAffiliate->tax; ?></td>
            <td><?php echo $ocAffiliate->payment; ?></td>
            <td><?php echo $ocAffiliate->cheque; ?></td>
            <td><?php echo $ocAffiliate->paypal; ?></td>
            <td><?php echo $ocAffiliate->bank_name; ?></td>
            <td><?php echo $ocAffiliate->bank_branch_number; ?></td>
            <td><?php echo $ocAffiliate->bank_swift_code; ?></td>
            <td><?php echo $ocAffiliate->bank_account_name; ?></td>
            <td><?php echo $ocAffiliate->bank_account_number; ?></td>
            <td><?php echo $ocAffiliate->ip; ?></td>
            <td><?php echo $ocAffiliate->status; ?></td>
            <td><?php echo $ocAffiliate->approved; ?></td>
            <td><?php echo $ocAffiliate->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAffiliates.destroy', $ocAffiliate->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAffiliates.show', [$ocAffiliate->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAffiliates.edit', [$ocAffiliate->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>