<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocCategory->id; ?></p>
</div>

<!-- Image Field -->
<div class="form-group">
    <?php echo Form::label('image', 'Image:'); ?>

    <p><?php echo $ocCategory->image; ?></p>
</div>

<!-- Parent Id Field -->
<div class="form-group">
    <?php echo Form::label('parent_id', 'Parent Id:'); ?>

    <p><?php echo $ocCategory->parent_id; ?></p>
</div>

<!-- Top Field -->
<div class="form-group">
    <?php echo Form::label('top', 'Top:'); ?>

    <p><?php echo $ocCategory->top; ?></p>
</div>

<!-- Column Field -->
<div class="form-group">
    <?php echo Form::label('column', 'Column:'); ?>

    <p><?php echo $ocCategory->column; ?></p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <p><?php echo $ocCategory->sort_order; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $ocCategory->status; ?></p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <p><?php echo $ocCategory->date_added; ?></p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    <?php echo Form::label('date_modified', 'Date Modified:'); ?>

    <p><?php echo $ocCategory->date_modified; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocCategory->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocCategory->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocCategory->deleted_at; ?></p>
</div>

