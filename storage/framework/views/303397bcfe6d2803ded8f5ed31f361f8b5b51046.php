<div class="top-main-area">
    <div class="container">
        <div class="row">
            <!-- <div class="col-md-1"> -->
                <!-- <a href="/" class="logo mt5"> -->
                    <!-- <img src="/cs.jpg" height="35" width="35" alt="Image Alternative text" title="Image Title" /> -->
                <!-- </a> -->
            <!-- </div> -->
            <div class="col-md-2"><h4>Logo <?php echo e(config('app.name', 'AFREL')); ?></h4></div>
            <div class="col-md-6 col-md-offset-4">
                <div class="pull-right">
                    <ul class="login-register">
                        <li class="shopping-cart shopping-cart-white"><a href="<?php echo e(url('/cart')); ?>"><i class="fa fa-shopping-cart"></i>My Cart ( <?php echo e(Cart::count()); ?> ) </a>
                            <div class="shopping-cart-box">
                                <ul class="shopping-cart-items">
                                <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cart): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e(url('#')); ?>">
                                            <img src="image/product/sampul/<?php echo e(App\Models\oc_product::where('id',$cart->id)->first()['model'].'/'.App\Models\oc_product::where('id',$cart->id)->first()['image']); ?>" alt="Image Alternative text" title="AMaze" />
                                            <h5><?php echo e($cart->name); ?></h5><span class="shopping-cart-item-price"><?php echo e($cart->price); ?></span>
                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <ul class="list-inline text-center">
                                    <li><a href="<?php echo e(url('/cart')); ?>"><i class="fa fa-shopping-cart"></i> View Cart</a>
                                    </li>
                                    <li><a href="<?php echo e(url('/checkout')); ?>"><i class="fa fa-check-square"></i> Checkout</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php if(Route::has('login')): ?>
                            <?php if(Auth::check()): ?>
                            <li>
                                <a class="popup-text" href="#menu-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-sign-in"></i><?php echo e(Auth::user()->name); ?>

                                </a>
                            </li>
                            <?php else: ?>
                            <li>
                                <a class="popup-text" href="#login-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-sign-in"></i>Sign in
                                </a>
                            </li>
                            <li>
                                <a class="popup-text" href="#register-dialog" data-effect="mfp-move-from-top">
                                    <i class="fa fa-edit"></i>Sign up
                                </a>
                            </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<header class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flexnav-menu-button" id="flexnav-menu-button">Menu</div>
                <nav>
                    <ul class="nav nav-pills flexnav" id="flexnav" data-breakpoint="800">
                        <!-- <li class="active"><a href="index.html">Home</a>
                            <ul>
                                <li><a href="index-shop-layout-1.html">Shop Layout</a>
                                    <ul>
                                        <li><a href="index-shop-layout-1.html">Layout 1</a>
                                        </li>
                                        <li><a href="index-shop-layout-2.html">Layout 2</a>
                                        </li>
                                        <li><a href="index-shop-layout-3.html">Layout 3</a>
                                        </li>
                                        <li><a href="index-shop-layout-4.html">Layout 4</a>
                                        </li>
                                        <li><a href="index-shop-layout-5.html">Layout 5</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="index-coupon-layout-1.html">Coupon Layout</a>
                                    <ul>
                                        <li><a href="index-coupon-layout-1.html">Layout 1</a>
                                        </li>
                                        <li><a href="index-coupon-layout-2.html">Layout 2</a>
                                        </li>
                                        <li><a href="index-coupon-layout-3.html">Layout 3</a>
                                        </li>
                                        <li><a href="index.html">Layout 4</a>
                                        </li>
                                        <li><a href="index-coupon-layout-5.html">Layout 5</a>
                                        </li>
                                        <li><a href="index-coupon-layout-6.html">Layout 6</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="active"><a href="index-header-layout-1.html">Headers</a>
                                    <ul>
                                        <li><a href="index-header-layout-1.html">Layout 1</a>
                                        </li>
                                        <li><a href="index-header-layout-2.html">Layout 2</a>
                                        </li>
                                        <li class="active"><a href="index-header-layout-3.html">Layout 3</a>
                                        </li>
                                        <li><a href="index-header-layout-4.html">Layout 4</a>
                                        </li>
                                        <li><a href="index-header-layout-5.html">Layout 5</a>
                                        </li>
                                        <li><a href="index-header-logged-user.html">Logged User</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li> -->
                        <li><a href="/">Celebestore</a></li>
                        <!-- <li><a href="#">Pulsa</a></li> -->
                        <!-- <li><a href="#">Paket Data</a></li> -->
                        <!-- <li><a href="#">Listrik PLN</a></li> -->
                        <!-- <li><a href="#">Voucher Game</a></li> -->
                        <!-- <li><a href="#">Virtual Akun</a></li> -->
                        <!-- <li><a href="#">Tiket Kereta</a></li> -->
                        <!-- <li><a href="#">Tiket Pesawat</a></li> -->
                        <!-- <li><a href="#">Zakat Profesi</a></li> -->
                        <!-- 
                        <li><a href="category-page-shop.html">Category</a>
                            <ul>
                                <li><a href="category-page-shop.html">Shop</a>
                                </li>
                                <li><a href="category-page-coupon.html">Coupon</a>
                                </li>
                                <li><a href="category-page-thumbnails-shop-layout-1.html">Thumbnails</a>
                                    <ul>
                                        <li><a href="category-page-thumbnails-shop-layout-1.html">Shop</a>
                                            <ul>
                                                <li><a href="category-page-thumbnails-shop-layout-1.html">Layout 1</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-layout-2.html">Layout 2</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-layout-3.html">Layout 3</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-layout-4.html">layout 4</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-layout-5.html">Layout 5</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-layout-6.html">Layout 6</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-shop-horizontal.html">Horizontal</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="category-page-thumbnails-coupon-layout-1.html">Coupon</a>
                                            <ul>
                                                <li><a href="category-page-thumbnails-coupon-layout-1.html">Layout 1</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-2.html">Layout 2</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-3.html">Layout 3</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-4.html">Layout 4</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-5.html">Layout 5</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-6.html">Layout 6</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-7.html">Layout 7</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-layout-8.html">Layout 8</a>
                                                </li>
                                                <li><a href="category-page-thumbnails-coupon-horizontal.html">Horizontal</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="category-page-thumbnails-breadcrumbs.html">Breadcrumbs</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="product-shop-sidebar.html">Product </a>
                            <ul>
                                <li><a href="product-shop-sidebar.html">Shop</a>
                                    <ul>
                                        <li><a href="product-shop-sidebar.html">Sidebar</a>
                                        </li>
                                        <li><a href="product-shop-sidebar-left.html">Sidebar Left</a>
                                        </li>
                                        <li><a href="product-shop-centered.html">Centered</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="product-coupon-default.html">Coupon</a>
                                    <ul>
                                        <li><a href="product-coupon-default.html">Default</a>
                                        </li>
                                        <li><a href="product-coupon-meta-right.html">Meta right</a>
                                        </li>
                                        <li><a href="product-coupon-gallery.html">Gallery</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="features-typography.html">Features</a>
                            <ul>
                                <li><a href="features-typography.html">Typography</a>
                                </li>
                                <li><a href="features-elements.html">Elements</a>
                                </li>
                                <li><a href="features-grid.html">Grid</a>
                                </li>
                                <li><a href="features-icons.html">Icons</a>
                                </li>
                                <li><a href="features-image-hover.html">Image Hovers</a>
                                </li>
                                <li><a href="features-sliders.html">Sliders</a>
                                </li>
                                <li><a href="features-media.html">Media</a>
                                </li>
                                <li><a href="features-lightbox.html">Lightbox</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="blog-sidebar-right.html">Blog</a>
                            <ul>
                                <li><a href="blog-sidebar-right.html">Sidebar Right</a>
                                </li>
                                <li><a href="blog-sidebar-left.html">Sidebar Left</a>
                                </li>
                                <li><a href="blog-full-width.html">Full Width</a>
                                </li>
                                <li><a href="post-sidebar-right.html">Post</a>
                                    <ul>
                                        <li><a href="post-sidebar-right.html">Sidebar Right</a>
                                        </li>
                                        <li><a href="post-sidebar-left.html">Sidebar Left</a>
                                        </li>
                                        <li><a href="post-full-width.html">Full Width</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="page-full-width.html">Pages</a>
                            <ul>
                                <li><a href="page-my-account-settings.html">My Account</a>
                                    <ul>
                                        <li><a href="page-my-account-settings.html">Settings</a>
                                        </li>
                                        <li><a href="page-my-account-addresses.html">Address Book</a>
                                        </li>
                                        <li><a href="page-my-account-orders.html">Orders History</a>
                                        </li>
                                        <li><a href="page-my-account-wishlist.html">Wishlist</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="page-full-width.html">Full Width</a>
                                </li>
                                <li><a href="page-sidebar-right.html">Sidebar Right</a>
                                </li>
                                <li><a href="page-sidebar-left.html">Sidebar Left</a>
                                </li>
                                <li><a href="page-faq.html">Faq</a>
                                </li>
                                <li><a href="page-about-us.html">About us</a>
                                </li>
                                <li><a href="page-team.html">Team</a>
                                </li>
                                <li><a href="page-cart.html">Shopping Cart</a>
                                </li>
                                <li><a href="page-checkout.html">Checkout</a>
                                </li>
                                <li><a href="page-404.html">404</a>
                                </li>
                                <li><a href="page-search.html">Search</a>
                                    <ul>
                                        <li><a href="page-search-black.html">Black</a>
                                        </li>
                                        <li><a href="page-search-white.html">White</a>
                                        </li>
                                        <li><a href="page-search-sticky.html">Sticky</a>
                                        </li>
                                        <li><a href="page-search-no-search.html">No Search</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="page-contact.html">Contact</a>
                                </li>
                            </ul>
                        </li> 
                        -->
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
<!-- LOGIN REGISTER LINKS CONTENT -->
<div id="login-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="fa fa-sign-in dialog-icon"></i>
    <h3>Member Login</h3>
    <h5>Selamat datang kembali. Silahkan login</h5>
    <form class="dialog-form" method="post" action="<?php echo e(url('/login')); ?>">
        <?php echo csrf_field(); ?>

        <div class="form-group <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
            <label>E-mail</label>
            <input type="text" placeholder="email@saya.com" class="form-control" name="email" value="<?php echo e(old('email')); ?>">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="password anda" class="form-control" name="password">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox">Ingatkan saya
            </label>
        </div>
        <input type="submit" value="Masuk" class="btn btn-primary">
    </form>
    <ul class="dialog-alt-links">
        <li><a class="popup-text" href="#register-dialog" data-effect="mfp-zoom-out">Belum punya akun</a>
        </li>
        <li><a class="popup-text" href="#password-recover-dialog" data-effect="mfp-zoom-out">Lupa password</a>
        </li>
    </ul>
</div>

<div id="register-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="fa fa-edit dialog-icon"></i>
    <h3>Member Register</h3>
    <h5>Silahkan register untuk mendaftarkan akun anda.</h5>
    <form class="dialog-form" method="post" action="<?php echo e(url('/register')); ?>">
        <?php echo csrf_field(); ?>

        <div class="form-group">
            <label>Nama anda</label>
            <input type="text" placeholder="nama anda" class="form-control" name="name" value="<?php echo e(old('name')); ?>">
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input type="text" placeholder="email@saya.com" class="form-control" name="email" value="<?php echo e(old('email')); ?>">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" placeholder="password anda" name="password" class="form-control">
        </div>
        <div class="form-group">
            <label>Repeat Password</label>
            <input type="password" placeholder="Silahkan ulangi password anda" name="password_confirmation" class="form-control">
        </div>
        <input type="submit" value="Daftar" class="btn btn-primary">
    </form>
    <ul class="dialog-alt-links">
        <li><a class="popup-text" href="#login-dialog" data-effect="mfp-zoom-out">Sudah mempunyai akun</a>
        </li>
    </ul>
</div>

<div id="password-recover-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="icon-retweet dialog-icon"></i>
    <h3>Password Recovery</h3>
    <h5>Anda lupa password? Jangan khawatir kami akan memperbaikinya.</h5>
    <form class="dialog-form" method="post" action="<?php echo e(url('/password/email')); ?>">
        <label>E-mail</label>
        <input type="text" placeholder="email@saya.com" class="span12">
        <input type="submit" value="Request new password" class="btn btn-primary">
    </form>
</div>
<div id="menu-dialog" class="mfp-with-anim mfp-hide mfp-dialog clearfix">
    <i class="icon-retweet fa-user dialog-icon"></i>
    <h3>Menu User</h3>
    <div class="pull-left">
        <a href="<?php echo e(url('/home')); ?>" class="btn btn-default btn-flat">Profile</a>
    </div>
    <div class="pull-right">
        <a href="<?php echo url('/logout'); ?>" class="btn btn-default btn-flat"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Sign out
        </a>
        <form id="logout-form" action="<?php echo e(url('/logout')); ?>" method="POST"
              style="display: none;">
            <?php echo e(csrf_field()); ?>

        </form>
    </div>
</div>
<!-- END LOGIN REGISTER LINKS CONTENT -->