<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocAddress->id; ?></p>
</div>

<!-- Sub District Id Field -->
<div class="form-group">
    <?php echo Form::label('sub_district_id', 'Sub District Id:'); ?>

    <p><?php echo $ocAddress->sub_district_id; ?></p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    <?php echo Form::label('customer_id', 'Customer Id:'); ?>

    <p><?php echo $ocAddress->customer_id; ?></p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    <?php echo Form::label('firstname', 'Firstname:'); ?>

    <p><?php echo $ocAddress->firstname; ?></p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    <?php echo Form::label('lastname', 'Lastname:'); ?>

    <p><?php echo $ocAddress->lastname; ?></p>
</div>

<!-- Company Field -->
<div class="form-group">
    <?php echo Form::label('company', 'Company:'); ?>

    <p><?php echo $ocAddress->company; ?></p>
</div>

<!-- Address 1 Field -->
<div class="form-group">
    <?php echo Form::label('address_1', 'Address 1:'); ?>

    <p><?php echo $ocAddress->address_1; ?></p>
</div>

<!-- Address 2 Field -->
<div class="form-group">
    <?php echo Form::label('address_2', 'Address 2:'); ?>

    <p><?php echo $ocAddress->address_2; ?></p>
</div>

<!-- City Field -->
<div class="form-group">
    <?php echo Form::label('city', 'City:'); ?>

    <p><?php echo $ocAddress->city; ?></p>
</div>

<!-- Postcode Field -->
<div class="form-group">
    <?php echo Form::label('postcode', 'Postcode:'); ?>

    <p><?php echo $ocAddress->postcode; ?></p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    <?php echo Form::label('country_id', 'Country Id:'); ?>

    <p><?php echo $ocAddress->country_id; ?></p>
</div>

<!-- Zone Id Field -->
<div class="form-group">
    <?php echo Form::label('zone_id', 'Zone Id:'); ?>

    <p><?php echo $ocAddress->zone_id; ?></p>
</div>

<!-- Custom Field Field -->
<div class="form-group">
    <?php echo Form::label('custom_field', 'Custom Field:'); ?>

    <p><?php echo $ocAddress->custom_field; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocAddress->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocAddress->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocAddress->deleted_at; ?></p>
</div>

