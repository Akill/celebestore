<table class="table table-responsive" id="ocVouchers-table">
    <thead>
        <th>Order Id</th>
        <th>Code</th>
        <th>From Name</th>
        <th>From Email</th>
        <th>To Name</th>
        <th>To Email</th>
        <th>Voucher Theme Id</th>
        <th>Message</th>
        <th>Amount</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocVouchers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocVoucher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocVoucher->order_id; ?></td>
            <td><?php echo $ocVoucher->code; ?></td>
            <td><?php echo $ocVoucher->from_name; ?></td>
            <td><?php echo $ocVoucher->from_email; ?></td>
            <td><?php echo $ocVoucher->to_name; ?></td>
            <td><?php echo $ocVoucher->to_email; ?></td>
            <td><?php echo $ocVoucher->voucher_theme_id; ?></td>
            <td><?php echo $ocVoucher->message; ?></td>
            <td><?php echo $ocVoucher->amount; ?></td>
            <td><?php echo $ocVoucher->status; ?></td>
            <td><?php echo $ocVoucher->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocVouchers.destroy', $ocVoucher->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocVouchers.show', [$ocVoucher->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocVouchers.edit', [$ocVoucher->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>