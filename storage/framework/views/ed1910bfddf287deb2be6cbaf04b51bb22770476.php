<table class="table table-responsive" id="ocCustomers-table">
    <thead>
        <th>Customer Group Id</th>
        <th>Store Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Cart</th>
        <th>Wishlist</th>
        <th>Newsletter</th>
        <th>Address Id</th>
        <th>Custom Field</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Approved</th>
        <th>Safe</th>
        <th>Token</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCustomers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCustomer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCustomer->customer_group_id; ?></td>
            <td><?php echo $ocCustomer->store_id; ?></td>
            <td><?php echo $ocCustomer->firstname; ?></td>
            <td><?php echo $ocCustomer->lastname; ?></td>
            <td><?php echo $ocCustomer->email; ?></td>
            <td><?php echo $ocCustomer->telephone; ?></td>
            <td><?php echo $ocCustomer->fax; ?></td>
            <td><?php echo $ocCustomer->password; ?></td>
            <td><?php echo $ocCustomer->salt; ?></td>
            <td><?php echo $ocCustomer->cart; ?></td>
            <td><?php echo $ocCustomer->wishlist; ?></td>
            <td><?php echo $ocCustomer->newsletter; ?></td>
            <td><?php echo $ocCustomer->address_id; ?></td>
            <td><?php echo $ocCustomer->custom_field; ?></td>
            <td><?php echo $ocCustomer->ip; ?></td>
            <td><?php echo $ocCustomer->status; ?></td>
            <td><?php echo $ocCustomer->approved; ?></td>
            <td><?php echo $ocCustomer->safe; ?></td>
            <td><?php echo $ocCustomer->token; ?></td>
            <td><?php echo $ocCustomer->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCustomers.destroy', $ocCustomer->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCustomers.show', [$ocCustomer->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCustomers.edit', [$ocCustomer->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>