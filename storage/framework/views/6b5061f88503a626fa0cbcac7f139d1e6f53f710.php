<table class="table table-responsive" id="ocCategories-table">
    <thead>
        <th>Image</th>
        <th>Parent Id</th>
        <th>Top</th>
        <th>Column</th>
        <th>Sort Order</th>
        <th>Status</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCategory->image; ?></td>
            <td><?php echo $ocCategory->parent_id; ?></td>
            <td><?php echo $ocCategory->top; ?></td>
            <td><?php echo $ocCategory->column; ?></td>
            <td><?php echo $ocCategory->sort_order; ?></td>
            <td><?php echo $ocCategory->status; ?></td>
            <td><?php echo $ocCategory->date_added; ?></td>
            <td><?php echo $ocCategory->date_modified; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCategories.destroy', $ocCategory->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCategories.show', [$ocCategory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCategories.edit', [$ocCategory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>