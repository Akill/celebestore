<table class="table table-responsive" id="ocBannerImageDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Banner Id</th>
        <th>Title</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocBannerImageDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocBannerImageDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocBannerImageDescription->language_id; ?></td>
            <td><?php echo $ocBannerImageDescription->banner_id; ?></td>
            <td><?php echo $ocBannerImageDescription->title; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocBannerImageDescriptions.destroy', $ocBannerImageDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocBannerImageDescriptions.show', [$ocBannerImageDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocBannerImageDescriptions.edit', [$ocBannerImageDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>