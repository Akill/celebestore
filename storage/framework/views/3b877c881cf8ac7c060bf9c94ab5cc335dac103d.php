<table class="table table-responsive" id="ocCategoryToLayouts-table">
    <thead>
        <th>Store Id</th>
        <th>Layout Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCategoryToLayouts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCategoryToLayout): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCategoryToLayout->store_id; ?></td>
            <td><?php echo $ocCategoryToLayout->layout_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCategoryToLayouts.destroy', $ocCategoryToLayout->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCategoryToLayouts.show', [$ocCategoryToLayout->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCategoryToLayouts.edit', [$ocCategoryToLayout->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>