<!-- Language Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <?php echo Form::number('language_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Banner Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('banner_id', 'Banner Id:'); ?>

    <?php echo Form::number('banner_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('title', 'Title:'); ?>

    <?php echo Form::text('title', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocBannerImageDescriptions.index'); ?>" class="btn btn-default">Cancel</a>
</div>
