<table class="table table-responsive" id="ocAddresses-table">
    <thead>
        <!-- <th>Sub District Id</th> -->
        <!-- <th>Customer Id</th> -->
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Company</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>Postcode</th>
        <!-- <th>Country Id</th> -->
        <!-- <th>Zone Id</th> -->
        <!-- <th>Custom Field</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAddresses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAddress): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <!-- <td><?php echo $ocAddress->sub_district_id; ?></td> -->
            <!-- <td><?php echo $ocAddress->customer_id; ?></td> -->
            <td><?php echo $ocAddress->firstname; ?></td>
            <td><?php echo $ocAddress->lastname; ?></td>
            <td><?php echo $ocAddress->company; ?></td>
            <td><?php echo $ocAddress->address_1; ?></td>
            <td><?php echo $ocAddress->address_2; ?></td>
            <td><?php echo $ocAddress->city; ?></td>
            <td><?php echo $ocAddress->postcode; ?></td>
            <!-- <td><?php echo $ocAddress->country_id; ?></td> -->
            <!-- <td><?php echo $ocAddress->zone_id; ?></td> -->
            <!-- <td><?php echo $ocAddress->custom_field; ?></td> -->
            <td>
                <?php echo Form::open(['route' => ['ocAddresses.destroy', $ocAddress->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAddresses.show', [$ocAddress->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAddresses.edit', [$ocAddress->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>