<table class="table table-responsive" id="ocProducts-table">
    <thead>
        <th>Model</th>
        <!-- <th>Sku</th> -->
        <!-- <th>Upc</th> -->
        <!-- <th>Ean</th> -->
        <!-- <th>Jan</th> -->
        <!-- <th>Isbn</th> -->
        <!-- <th>Mpn</th> -->
        <!-- <th>Location</th> -->
        <th>Price</th>
        <th>Quantity</th>
        <th>Status</th>
        <th>Stock Status</th>
        <th>Image</th>
        <!-- <th>Manufacturer Id</th> -->
        <!-- <th>Shipping</th> -->
        <!-- <th>Points</th> -->
        <!-- <th>Tax Class Id</th> -->
        <!-- <th>Date Available</th> -->
        <!-- <th>Weight</th> -->
        <!-- <th>Weight Class Id</th> -->
        <!-- <th>Length</th> -->
        <!-- <th>Width</th> -->
        <!-- <th>Height</th> -->
        <!-- <th>Length Class Id</th> -->
        <!-- <th>Subtract</th> -->
        <!-- <th>Minimum</th> -->
        <!-- <th>Sort Order</th> -->
        <!-- <th>Viewed</th> -->
        <!-- <th>Date Added</th> -->
        <!-- <th>Date Modified</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProduct->model; ?></td>
            <!-- <td><?php echo $ocProduct->sku; ?></td> -->
            <!-- <td><?php echo $ocProduct->upc; ?></td> -->
            <!-- <td><?php echo $ocProduct->ean; ?></td> -->
            <!-- <td><?php echo $ocProduct->jan; ?></td> -->
            <!-- <td><?php echo $ocProduct->isbn; ?></td> -->
            <!-- <td><?php echo $ocProduct->mpn; ?></td> -->
            <!-- <td><?php echo $ocProduct->location; ?></td> -->
            <td><?php echo $ocProduct->price; ?></td>
            <td><?php echo $ocProduct->quantity; ?></td>
            <td><?php echo $ocProduct->status; ?></td>
            <td><?php echo App\Models\oc_stock_status::find($ocProduct->stock_status_id)->name; ?></td>
            <td><img src="image/product/sampul/<?php echo $ocProduct->model.'/'.$ocProduct->image; ?>" alt="Image" width="80" height="80"></td>
            <!-- <td><?php echo $ocProduct->manufacturer_id; ?></td> -->
            <!-- <td><?php echo $ocProduct->shipping; ?></td> -->
            <!-- <td><?php echo $ocProduct->points; ?></td> -->
            <!-- <td><?php echo $ocProduct->tax_class_id; ?></td> -->
            <!-- <td><?php echo $ocProduct->date_available; ?></td> -->
            <!-- <td><?php echo $ocProduct->weight; ?></td> -->
            <!-- <td><?php echo $ocProduct->weight_class_id; ?></td> -->
            <!-- <td><?php echo $ocProduct->length; ?></td> -->
            <!-- <td><?php echo $ocProduct->width; ?></td> -->
            <!-- <td><?php echo $ocProduct->height; ?></td> -->
            <!-- <td><?php echo $ocProduct->length_class_id; ?></td> -->
            <!-- <td><?php echo $ocProduct->subtract; ?></td> -->
            <!-- <td><?php echo $ocProduct->minimum; ?></td> -->
            <!-- <td><?php echo $ocProduct->sort_order; ?></td> -->
            <!-- <td><?php echo $ocProduct->viewed; ?></td> -->
            <!-- <td><?php echo $ocProduct->date_added; ?></td> -->
            <!-- <td><?php echo $ocProduct->date_modified; ?></td> -->
            <td>
                <?php echo Form::open(['route' => ['ocProducts.destroy', $ocProduct->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProducts.show', [$ocProduct->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProducts.edit', [$ocProduct->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>