<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocProductDiscount->id; ?></p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    <?php echo Form::label('product_id', 'Product Id:'); ?>

    <p><?php echo App\Models\oc_product_description::find($ocProductDiscount->product_id)->name; ?></p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    <?php echo Form::label('customer_group_id', 'Customer Group Id:'); ?>

    <p><?php echo $ocProductDiscount->customer_group_id; ?></p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <p><?php echo $ocProductDiscount->quantity; ?></p>
</div>

<!-- Priority Field -->
<div class="form-group">
    <?php echo Form::label('priority', 'Priority:'); ?>

    <p><?php echo $ocProductDiscount->priority; ?></p>
</div>

<!-- Price Field -->
<div class="form-group">
    <?php echo Form::label('price', 'Price:'); ?>

    <p><?php echo $ocProductDiscount->price; ?></p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    <?php echo Form::label('date_start', 'Date Start:'); ?>

    <p><?php echo $ocProductDiscount->date_start; ?></p>
</div>

<!-- Date End Field -->
<div class="form-group">
    <?php echo Form::label('date_end', 'Date End:'); ?>

    <p><?php echo $ocProductDiscount->date_end; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocProductDiscount->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocProductDiscount->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocProductDiscount->deleted_at; ?></p>
</div>

