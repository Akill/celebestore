<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $user->id; ?></p>
</div>

<!-- User Group Id Field -->
<div class="form-group">
    <?php echo Form::label('user_group_id', 'User Group:'); ?>

    <p><?php echo App\Models\oc_user_group::find($user->user_group_id)->name; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $user->name; ?></p>
</div>

<!-- Email Field -->
<div class="form-group">
    <?php echo Form::label('email', 'Email:'); ?>

    <p><?php echo $user->email; ?></p>
</div>

<!-- Password Field -->
<div class="form-group">
    <?php echo Form::label('password', 'Password:'); ?>

    <p><?php echo $user->password; ?></p>
</div>

<!-- Username Field -->
<div class="form-group">
    <?php echo Form::label('username', 'Username:'); ?>

    <p><?php echo $user->username; ?></p>
</div>

<!-- Type Of Identity Field -->
<div class="form-group">
    <?php echo Form::label('type_of_identity', 'Type Of Identity:'); ?>

    <p><?php echo $user->type_of_identity; ?></p>
</div>

<!-- Identity Number Field -->
<div class="form-group">
    <?php echo Form::label('identity_number', 'Identity Number:'); ?>

    <p><?php echo $user->identity_number; ?></p>
</div>

<!-- Gender Field -->
<div class="form-group">
    <?php echo Form::label('gender', 'Gender:'); ?>

    <p><?php echo $user->gender; ?></p>
</div>

<!-- Phone Field -->
<div class="form-group">
    <?php echo Form::label('phone', 'Phone:'); ?>

    <p><?php echo $user->phone; ?></p>
</div>

<!-- Job Field -->
<div class="form-group">
    <?php echo Form::label('job', 'Job:'); ?>

    <p><?php echo $user->job; ?></p>
</div>

<!-- Address Field -->
<div class="form-group">
    <?php echo Form::label('address', 'Address:'); ?>

    <p><?php echo $user->address; ?></p>
</div>

<!-- Districts Field -->
<div class="form-group">
    <?php echo Form::label('districts', 'Districts:'); ?>

    <p><?php echo $user->districts; ?></p>
</div>

<!-- City Field -->
<div class="form-group">
    <?php echo Form::label('city', 'City:'); ?>

    <p><?php echo $user->city; ?></p>
</div>

<!-- Province Field -->
<div class="form-group">
    <?php echo Form::label('province', 'Province:'); ?>

    <p><?php echo $user->province; ?></p>
</div>

<!-- Zip Kode Field -->
<div class="form-group">
    <?php echo Form::label('zip_kode', 'Zip Kode:'); ?>

    <p><?php echo $user->zip_kode; ?></p>
</div>

<!-- Picture Field -->
<div class="form-group">
    <?php echo Form::label('picture', 'Picture:'); ?>

    <p><?php echo $user->picture; ?></p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    <?php echo Form::label('remember_token', 'Remember Token:'); ?>

    <p><?php echo $user->remember_token; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $user->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $user->updated_at; ?></p>
</div>

