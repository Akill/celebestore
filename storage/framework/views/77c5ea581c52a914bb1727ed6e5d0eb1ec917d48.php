<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <?php echo Form::number('sort_order', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocAttributeGroups.index'); ?>" class="btn btn-default">Cancel</a>
</div>
