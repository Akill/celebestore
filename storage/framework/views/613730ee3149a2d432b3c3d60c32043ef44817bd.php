<!-- Product Image Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocProductImage->id; ?></p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    <?php echo Form::label('product_id', 'Product Id:'); ?>

    <p><?php echo App\Models\oc_product_description::find($ocProductImage->product_id)->name; ?></p>
</div>

<!-- Image Field -->
<div class="form-group">
    <?php echo Form::label('image', 'Image:'); ?>

    <p><?php echo $ocProductImage->image; ?></p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <p><?php echo $ocProductImage->sort_order; ?></p>
</div>

