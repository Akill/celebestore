<!-- Invoice No Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('invoice_no', 'Invoice No:'); ?>

    <?php echo Form::number('invoice_no', null, ['class' => 'form-control']); ?>

</div>

<!-- Invoice Prefix Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('invoice_prefix', 'Invoice Prefix:'); ?>

    <?php echo Form::text('invoice_prefix', null, ['class' => 'form-control']); ?>

</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('store_id', 'Store Id:'); ?>

    <?php echo Form::number('store_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Store Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('store_name', 'Store Name:'); ?>

    <?php echo Form::text('store_name', null, ['class' => 'form-control']); ?>

</div>

<!-- Store Url Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('store_url', 'Store Url:'); ?>

    <?php echo Form::text('store_url', null, ['class' => 'form-control']); ?>

</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('customer_id', 'Customer Id:'); ?>

    <?php echo Form::number('customer_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('customer_group_id', 'Customer Group Id:'); ?>

    <?php echo Form::number('customer_group_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('firstname', 'Firstname:'); ?>

    <?php echo Form::text('firstname', null, ['class' => 'form-control']); ?>

</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('lastname', 'Lastname:'); ?>

    <?php echo Form::text('lastname', null, ['class' => 'form-control']); ?>

</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('email', 'Email:'); ?>

    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

</div>

<!-- Telephone Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('telephone', 'Telephone:'); ?>

    <?php echo Form::text('telephone', null, ['class' => 'form-control']); ?>

</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('fax', 'Fax:'); ?>

    <?php echo Form::text('fax', null, ['class' => 'form-control']); ?>

</div>

<!-- Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('custom_field', 'Custom Field:'); ?>

    <?php echo Form::textarea('custom_field', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Firstname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_firstname', 'Payment Firstname:'); ?>

    <?php echo Form::text('payment_firstname', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Lastname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_lastname', 'Payment Lastname:'); ?>

    <?php echo Form::text('payment_lastname', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Company Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_company', 'Payment Company:'); ?>

    <?php echo Form::text('payment_company', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Address 1 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_address_1', 'Payment Address 1:'); ?>

    <?php echo Form::text('payment_address_1', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Address 2 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_address_2', 'Payment Address 2:'); ?>

    <?php echo Form::text('payment_address_2', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment City Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_city', 'Payment City:'); ?>

    <?php echo Form::text('payment_city', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Postcode Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_postcode', 'Payment Postcode:'); ?>

    <?php echo Form::text('payment_postcode', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Country Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_country', 'Payment Country:'); ?>

    <?php echo Form::text('payment_country', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Country Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_country_id', 'Payment Country Id:'); ?>

    <?php echo Form::number('payment_country_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Zone Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_zone', 'Payment Zone:'); ?>

    <?php echo Form::text('payment_zone', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Zone Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_zone_id', 'Payment Zone Id:'); ?>

    <?php echo Form::number('payment_zone_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Sub District Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_sub_district_id', 'Payment Sub District Id:'); ?>

    <?php echo Form::number('payment_sub_district_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Address Format Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('payment_address_format', 'Payment Address Format:'); ?>

    <?php echo Form::textarea('payment_address_format', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('payment_custom_field', 'Payment Custom Field:'); ?>

    <?php echo Form::textarea('payment_custom_field', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_method', 'Payment Method:'); ?>

    <?php echo Form::text('payment_method', null, ['class' => 'form-control']); ?>

</div>

<!-- Payment Code Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('payment_code', 'Payment Code:'); ?>

    <?php echo Form::text('payment_code', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Firstname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_firstname', 'Shipping Firstname:'); ?>

    <?php echo Form::text('shipping_firstname', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Lastname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_lastname', 'Shipping Lastname:'); ?>

    <?php echo Form::text('shipping_lastname', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Company Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_company', 'Shipping Company:'); ?>

    <?php echo Form::text('shipping_company', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Address 1 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_address_1', 'Shipping Address 1:'); ?>

    <?php echo Form::text('shipping_address_1', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Address 2 Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_address_2', 'Shipping Address 2:'); ?>

    <?php echo Form::text('shipping_address_2', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping City Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_city', 'Shipping City:'); ?>

    <?php echo Form::text('shipping_city', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Postcode Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_postcode', 'Shipping Postcode:'); ?>

    <?php echo Form::text('shipping_postcode', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Country Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_country', 'Shipping Country:'); ?>

    <?php echo Form::text('shipping_country', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Country Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_country_id', 'Shipping Country Id:'); ?>

    <?php echo Form::number('shipping_country_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Zone Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_zone', 'Shipping Zone:'); ?>

    <?php echo Form::text('shipping_zone', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Zone Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_zone_id', 'Shipping Zone Id:'); ?>

    <?php echo Form::number('shipping_zone_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Sub District Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_sub_district_id', 'Shipping Sub District Id:'); ?>

    <?php echo Form::number('shipping_sub_district_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Address Format Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('shipping_address_format', 'Shipping Address Format:'); ?>

    <?php echo Form::textarea('shipping_address_format', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('shipping_custom_field', 'Shipping Custom Field:'); ?>

    <?php echo Form::textarea('shipping_custom_field', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Method Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_method', 'Shipping Method:'); ?>

    <?php echo Form::text('shipping_method', null, ['class' => 'form-control']); ?>

</div>

<!-- Shipping Code Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('shipping_code', 'Shipping Code:'); ?>

    <?php echo Form::text('shipping_code', null, ['class' => 'form-control']); ?>

</div>

<!-- Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('comment', 'Comment:'); ?>

    <?php echo Form::textarea('comment', null, ['class' => 'form-control']); ?>

</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('total', 'Total:'); ?>

    <?php echo Form::number('total', null, ['class' => 'form-control']); ?>

</div>

<!-- Order Status Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('order_status_id', 'Order Status Id:'); ?>

    <?php echo Form::number('order_status_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Affiliate Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('affiliate_id', 'Affiliate Id:'); ?>

    <?php echo Form::number('affiliate_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('commission', 'Commission:'); ?>

    <?php echo Form::number('commission', null, ['class' => 'form-control']); ?>

</div>

<!-- Marketing Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('marketing_id', 'Marketing Id:'); ?>

    <?php echo Form::number('marketing_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Tracking Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('tracking', 'Tracking:'); ?>

    <?php echo Form::text('tracking', null, ['class' => 'form-control']); ?>

</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <?php echo Form::number('language_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Currency Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('currency_id', 'Currency Id:'); ?>

    <?php echo Form::number('currency_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('currency_code', 'Currency Code:'); ?>

    <?php echo Form::text('currency_code', null, ['class' => 'form-control']); ?>

</div>

<!-- Currency Value Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('currency_value', 'Currency Value:'); ?>

    <?php echo Form::number('currency_value', null, ['class' => 'form-control']); ?>

</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('ip', 'Ip:'); ?>

    <?php echo Form::text('ip', null, ['class' => 'form-control']); ?>

</div>

<!-- Forwarded Ip Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('forwarded_ip', 'Forwarded Ip:'); ?>

    <?php echo Form::text('forwarded_ip', null, ['class' => 'form-control']); ?>

</div>

<!-- User Agent Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('user_agent', 'User Agent:'); ?>

    <?php echo Form::text('user_agent', null, ['class' => 'form-control']); ?>

</div>

<!-- Accept Language Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('accept_language', 'Accept Language:'); ?>

    <?php echo Form::text('accept_language', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <?php echo Form::date('date_added', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_modified', 'Date Modified:'); ?>

    <?php echo Form::date('date_modified', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocOrders.index'); ?>" class="btn btn-default">Cancel</a>
</div>
