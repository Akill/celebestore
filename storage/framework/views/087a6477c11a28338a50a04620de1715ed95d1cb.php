<table class="table table-responsive" id="ocAffiliateActivities-table">
    <thead>
        <th>Affiliate Id</th>
        <th>Key</th>
        <th>Data</th>
        <th>Ip</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAffiliateActivities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAffiliateActivity): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAffiliateActivity->affiliate_id; ?></td>
            <td><?php echo $ocAffiliateActivity->key; ?></td>
            <td><?php echo $ocAffiliateActivity->data; ?></td>
            <td><?php echo $ocAffiliateActivity->ip; ?></td>
            <td><?php echo $ocAffiliateActivity->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAffiliateActivities.destroy', $ocAffiliateActivity->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAffiliateActivities.show', [$ocAffiliateActivity->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAffiliateActivities.edit', [$ocAffiliateActivity->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>