<table class="table table-responsive" id="ocProductImages-table">
    <thead>
        <th>Product</th>
        <th>Image</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductImages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductImage): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo App\Models\oc_product_description::where('product_id', $ocProductImage->product_id)->first()->name; ?></td>
            <td><img src="<?php echo 'image/product/'.$ocProductImage->product_id.'/'.$ocProductImage->image; ?>" alt="Image" width="150" height="100"></td>
            <td><?php echo $ocProductImage->sort_order; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductImages.destroy', $ocProductImage->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductImages.show', [$ocProductImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductImages.edit', [$ocProductImage->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>