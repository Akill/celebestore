<!-- Product Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('product_id', 'Product:'); ?>

    <?php  $items = App\Models\oc_product::has('detail','<',1)->get();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->id); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php if(Route::is('ocProductDescriptions.edit')): ?>
    <option value="<?php echo e($ocProductDescription->id); ?>"><?php echo e($ocProductDescription->id); ?></option>
    <?php endif; ?>
    </select>
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <?php  $items = App\Models\oc_language::all(['name','id']);  ?>
    <select class="form-control" id="language_id" name="language_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('description', 'Description:'); ?>

    <?php echo Form::textarea('description', null, ['class' => 'form-control']); ?>

</div>

<!-- Tag Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('tag', 'Tag:'); ?>

    <?php echo Form::textarea('tag', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Title Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_title', 'Meta Title:'); ?>

    <?php echo Form::text('meta_title', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Description Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_description', 'Meta Description:'); ?>

    <?php echo Form::text('meta_description', null, ['class' => 'form-control']); ?>

</div>

<!-- Meta Keyword Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('meta_keyword', 'Meta Keyword:'); ?>

    <?php echo Form::text('meta_keyword', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductDescriptions.index'); ?>" class="btn btn-default">Cancel</a>
</div>
