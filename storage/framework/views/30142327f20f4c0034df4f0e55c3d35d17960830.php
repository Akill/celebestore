<table class="table table-responsive" id="ocUserGroups-table">
    <thead>
        <th>Name</th>
        <th>Permission</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocUserGroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocUserGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocUserGroup->name; ?></td>
            <td><?php echo $ocUserGroup->permission; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocUserGroups.destroy', $ocUserGroup->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocUserGroups.show', [$ocUserGroup->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocUserGroups.edit', [$ocUserGroup->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>