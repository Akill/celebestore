<table class="table table-responsive" id="ocProductOptions-table">
    <thead>
        <th>Product</th>
        <th>Option</th>
        <th>Value</th>
        <th>Required</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductOptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductOption): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductOption->product_id; ?></td>
            <td><?php echo $ocProductOption->option_id; ?></td>
            <td><?php echo $ocProductOption->value; ?></td>
            <td><?php echo $ocProductOption->required; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductOptions.destroy', $ocProductOption->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductOptions.show', [$ocProductOption->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductOptions.edit', [$ocProductOption->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>