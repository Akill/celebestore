<li class="<?php echo e(Request::is('home') ? 'active' : ''); ?>">
    <a href="<?php echo route('home'); ?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li class="<?php echo e(Request::is('ocAddresses*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocAddresses.index'); ?>"><i class="fa fa-edit"></i><span>addresses</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Affiliates</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocAffiliates*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliates.index'); ?>"><i class="fa fa-edit"></i><span>affiliates</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateActivities*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateActivities.index'); ?>"><i class="fa fa-edit"></i><span>affiliate activities</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateLogins*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateLogins.index'); ?>"><i class="fa fa-edit"></i><span>affiliate logins</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateTransactions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateTransactions.index'); ?>"><i class="fa fa-edit"></i><span>affiliate transactions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocApis*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocApis.index'); ?>"><i class="fa fa-edit"></i><span>apis</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Attributes</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocAttributes*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributes.index'); ?>"><i class="fa fa-edit"></i><span>attributes</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>attribute descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeGroups.index'); ?>"><i class="fa fa-edit"></i><span>attribute groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeGroupDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeGroupDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>attribute group descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Banners</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocBanners*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBanners.index'); ?>"><i class="fa fa-edit"></i><span>banners</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocBannerImages*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBannerImages.index'); ?>"><i class="fa fa-edit"></i><span>banner images</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocBannerImageDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBannerImageDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>banner image descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Confirms</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocConfirms*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocConfirms.index'); ?>"><i class="fa fa-edit"></i><span>confirms</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocConfirmToStores*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocConfirmToStores.index'); ?>"><i class="fa fa-edit"></i><span>confirm to stores</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocCountries*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCountries.index'); ?>"><i class="fa fa-edit"></i><span>countries</span></a>
</li>

<li class="<?php echo e(Request::is('ocCountryHpwds*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCountryHpwds.index'); ?>"><i class="fa fa-edit"></i><span>country hpwds</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Coupons</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCoupons*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCoupons.index'); ?>"><i class="fa fa-edit"></i><span>coupons</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponCategories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponCategories.index'); ?>"><i class="fa fa-edit"></i><span>coupon categories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponHistories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponHistories.index'); ?>"><i class="fa fa-edit"></i><span>coupon histories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponProducts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponProducts.index'); ?>"><i class="fa fa-edit"></i><span>coupon products</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocCurrencies*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCurrencies.index'); ?>"><i class="fa fa-edit"></i><span>currencies</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCustomers*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomers.index'); ?>"><i class="fa fa-edit"></i><span>customers</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerActivities*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerActivities.index'); ?>"><i class="fa fa-edit"></i><span>customer activities</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerBanIps*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerBanIps.index'); ?>"><i class="fa fa-edit"></i><span>customer ban ips</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerGroups.index'); ?>"><i class="fa fa-edit"></i><span>customer groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerGroupDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerGroupDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>customer group descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerHistories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerHistories.index'); ?>"><i class="fa fa-edit"></i><span>customer histories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerIps*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerIps.index'); ?>"><i class="fa fa-edit"></i><span>customer ips</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerLogins*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerLogins.index'); ?>"><i class="fa fa-edit"></i><span>customer logins</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerOnlines*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerOnlines.index'); ?>"><i class="fa fa-edit"></i><span>customer onlines</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerRewards*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerRewards.index'); ?>"><i class="fa fa-edit"></i><span>customer rewards</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerTransactions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerTransactions.index'); ?>"><i class="fa fa-edit"></i><span>customer transactions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Custom Fields</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCustomFields*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFields.index'); ?>"><i class="fa fa-edit"></i><span>custom fields</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldCustomerGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldCustomerGroups.index'); ?>"><i class="fa fa-edit"></i><span>custom field customer groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>custom field descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldValues*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldValues.index'); ?>"><i class="fa fa-edit"></i><span>custom field values</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldValueDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldValueDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>custom field value descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Downloads</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocDownloads*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocDownloads.index'); ?>"><i class="fa fa-edit"></i><span>downloads</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocDownloadDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocDownloadDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>download descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocEvents*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocEvents.index'); ?>"><i class="fa fa-edit"></i><span>events</span></a>
</li>

<li class="<?php echo e(Request::is('ocExtensions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocExtensions.index'); ?>"><i class="fa fa-edit"></i><span>extensions</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Filters</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocFilters*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocFilters.index'); ?>"><i class="fa fa-edit"></i><span>filters</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocFilterDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocFilterDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>filter descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocGeoZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocGeoZones.index'); ?>"><i class="fa fa-edit"></i><span>geo zones</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Informations</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocInformations*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocInformations.index'); ?>"><i class="fa fa-edit"></i><span>informations</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocInformationDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocInformationDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>information descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocLanguages*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLanguages.index'); ?>"><i class="fa fa-edit"></i><span>languages</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Layout Modules</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocLayoutModules*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocLayoutModules.index'); ?>"><i class="fa fa-edit"></i><span>layout modules</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocLayoutRoutes*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocLayoutRoutes.index'); ?>"><i class="fa fa-edit"></i><span>layout routes</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocLengthClassDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLengthClassDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>length class descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocLocations*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLocations.index'); ?>"><i class="fa fa-edit"></i><span>locations</span></a>
</li>

<li class="<?php echo e(Request::is('ocManufacturers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocManufacturers.index'); ?>"><i class="fa fa-edit"></i><span>manufacturers</span></a>
</li>

<li class="<?php echo e(Request::is('ocMarketings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocMarketings.index'); ?>"><i class="fa fa-edit"></i><span>marketings</span></a>
</li>

<li class="<?php echo e(Request::is('ocModifications*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocModifications.index'); ?>"><i class="fa fa-edit"></i><span>modifications</span></a>
</li>

<li class="<?php echo e(Request::is('ocModules*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocModules.index'); ?>"><i class="fa fa-edit"></i><span>modules</span></a>
</li>

<li class="<?php echo e(Request::is('ocOptionValues*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOptionValues.index'); ?>"><i class="fa fa-edit"></i><span>option values</span></a>
</li>

<li class="<?php echo e(Request::is('ocOptionValueDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOptionValueDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>option value descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrders*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrders.index'); ?>"><i class="fa fa-edit"></i><span>orders</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderCustomFields*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderCustomFields.index'); ?>"><i class="fa fa-edit"></i><span>order custom fields</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderHistories.index'); ?>"><i class="fa fa-edit"></i><span>order histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderOptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderOptions.index'); ?>"><i class="fa fa-edit"></i><span>order options</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderProducts*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderProducts.index'); ?>"><i class="fa fa-edit"></i><span>order products</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderRecurrings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderRecurrings.index'); ?>"><i class="fa fa-edit"></i><span>order recurrings</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderRecurringTransactions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderRecurringTransactions.index'); ?>"><i class="fa fa-edit"></i><span>order recurring transactions</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderTotals*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderTotals.index'); ?>"><i class="fa fa-edit"></i><span>order totals</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderVouchers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderVouchers.index'); ?>"><i class="fa fa-edit"></i><span>order vouchers</span></a>
</li>

<li class="<?php echo e(Request::is('ocProducts*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProducts.index'); ?>"><i class="fa fa-edit"></i><span>products</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductAttributes*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductAttributes.index'); ?>"><i class="fa fa-edit"></i><span>product attributes</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>product descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductDiscounts*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductDiscounts.index'); ?>"><i class="fa fa-edit"></i><span>product discounts</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductImages*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductImages.index'); ?>"><i class="fa fa-edit"></i><span>product images</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductOptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductOptions.index'); ?>"><i class="fa fa-edit"></i><span>product options</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductOptionValues*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductOptionValues.index'); ?>"><i class="fa fa-edit"></i><span>product option values</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductRewards*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductRewards.index'); ?>"><i class="fa fa-edit"></i><span>product rewards</span></a>
</li>

<li class="<?php echo e(Request::is('ocProductSpecials*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductSpecials.index'); ?>"><i class="fa fa-edit"></i><span>product specials</span></a>
</li>

<li class="<?php echo e(Request::is('ocRecurrings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocRecurrings.index'); ?>"><i class="fa fa-edit"></i><span>recurrings</span></a>
</li>

<li class="<?php echo e(Request::is('ocReturns*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReturns.index'); ?>"><i class="fa fa-edit"></i><span>returns</span></a>
</li>

<li class="<?php echo e(Request::is('ocReturnHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReturnHistories.index'); ?>"><i class="fa fa-edit"></i><span>return histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocReviews*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReviews.index'); ?>"><i class="fa fa-edit"></i><span>reviews</span></a>
</li>

<li class="<?php echo e(Request::is('ocSettings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocSettings.index'); ?>"><i class="fa fa-edit"></i><span>settings</span></a>
</li>

<li class="<?php echo e(Request::is('ocStores*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocStores.index'); ?>"><i class="fa fa-edit"></i><span>stores</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxClasses*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxClasses.index'); ?>"><i class="fa fa-edit"></i><span>tax classes</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxRates*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxRates.index'); ?>"><i class="fa fa-edit"></i><span>tax rates</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxRules*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxRules.index'); ?>"><i class="fa fa-edit"></i><span>tax rules</span></a>
</li>

<li class="<?php echo e(Request::is('ocUploads*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUploads.index'); ?>"><i class="fa fa-edit"></i><span>uploads</span></a>
</li>

<li class="<?php echo e(Request::is('ocUsers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUsers.index'); ?>"><i class="fa fa-edit"></i><span>users</span></a>
</li>

<li class="<?php echo e(Request::is('ocUserGroups*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUserGroups.index'); ?>"><i class="fa fa-edit"></i><span>user groups</span></a>
</li>

<li class="<?php echo e(Request::is('ocVouchers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocVouchers.index'); ?>"><i class="fa fa-edit"></i><span>vouchers</span></a>
</li>

<li class="<?php echo e(Request::is('ocVoucherHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocVoucherHistories.index'); ?>"><i class="fa fa-edit"></i><span>voucher histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocWeightClassDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocWeightClassDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>weight class descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocZones.index'); ?>"><i class="fa fa-edit"></i><span>zones</span></a>
</li>

<li class="<?php echo e(Request::is('ocZoneToGeoZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocZoneToGeoZones.index'); ?>"><i class="fa fa-edit"></i><span>zone to geo zones</span></a>
</li>

<li class="<?php echo e(Request::is('users*') ? 'active' : ''); ?>">
    <a href="<?php echo route('users.index'); ?>"><i class="fa fa-edit"></i><span>users</span></a>
</li>


<!-- Sudah di edit -->

<li class="<?php echo e(Request::is('ocAddresses*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocAddresses.index'); ?>"><i class="fa fa-edit"></i><span>addresses</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Affiliates</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocAffiliates*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliates.index'); ?>"><i class="fa fa-edit"></i><span>affiliates</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateActivities*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateActivities.index'); ?>"><i class="fa fa-edit"></i><span>affiliate activities</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateLogins*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateLogins.index'); ?>"><i class="fa fa-edit"></i><span>affiliate logins</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAffiliateTransactions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAffiliateTransactions.index'); ?>"><i class="fa fa-edit"></i><span>affiliate transactions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocApis*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocApis.index'); ?>"><i class="fa fa-edit"></i><span>apis</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Attributes</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocAttributes*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributes.index'); ?>"><i class="fa fa-edit"></i><span>attributes</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>attribute descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeGroups.index'); ?>"><i class="fa fa-edit"></i><span>attribute groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocAttributeGroupDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocAttributeGroupDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>attribute group descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Banners</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocBanners*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBanners.index'); ?>"><i class="fa fa-edit"></i><span>banners</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocBannerImages*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBannerImages.index'); ?>"><i class="fa fa-edit"></i><span>banner images</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocBannerImageDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocBannerImageDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>banner image descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Categories</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCategories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategories.index'); ?>"><i class="fa fa-edit"></i><span>categories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>category descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryFilters*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryFilters.index'); ?>"><i class="fa fa-edit"></i><span>category filters</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryPaths*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryPaths.index'); ?>"><i class="fa fa-edit"></i><span>category paths</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryToLayouts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryToLayouts.index'); ?>"><i class="fa fa-edit"></i><span>category to layouts</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryToStores*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryToStores.index'); ?>"><i class="fa fa-edit"></i><span>category to stores</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Confirms</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocConfirms*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocConfirms.index'); ?>"><i class="fa fa-edit"></i><span>confirms</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocConfirmToStores*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocConfirmToStores.index'); ?>"><i class="fa fa-edit"></i><span>confirm to stores</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocCountries*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCountries.index'); ?>"><i class="fa fa-edit"></i><span>countries</span></a>
</li>

<li class="<?php echo e(Request::is('ocCountryHpwds*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCountryHpwds.index'); ?>"><i class="fa fa-edit"></i><span>country hpwds</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Coupons</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCoupons*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCoupons.index'); ?>"><i class="fa fa-edit"></i><span>coupons</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponCategories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponCategories.index'); ?>"><i class="fa fa-edit"></i><span>coupon categories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponHistories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponHistories.index'); ?>"><i class="fa fa-edit"></i><span>coupon histories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCouponProducts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCouponProducts.index'); ?>"><i class="fa fa-edit"></i><span>coupon products</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocCurrencies*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCurrencies.index'); ?>"><i class="fa fa-edit"></i><span>currencies</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCustomers*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomers.index'); ?>"><i class="fa fa-edit"></i><span>customers</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerActivities*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerActivities.index'); ?>"><i class="fa fa-edit"></i><span>customer activities</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerBanIps*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerBanIps.index'); ?>"><i class="fa fa-edit"></i><span>customer ban ips</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerGroups.index'); ?>"><i class="fa fa-edit"></i><span>customer groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerGroupDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerGroupDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>customer group descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerHistories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerHistories.index'); ?>"><i class="fa fa-edit"></i><span>customer histories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerIps*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerIps.index'); ?>"><i class="fa fa-edit"></i><span>customer ips</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerLogins*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerLogins.index'); ?>"><i class="fa fa-edit"></i><span>customer logins</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerOnlines*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerOnlines.index'); ?>"><i class="fa fa-edit"></i><span>customer onlines</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerRewards*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerRewards.index'); ?>"><i class="fa fa-edit"></i><span>customer rewards</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomerTransactions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomerTransactions.index'); ?>"><i class="fa fa-edit"></i><span>customer transactions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Custom Fields</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCustomFields*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFields.index'); ?>"><i class="fa fa-edit"></i><span>custom fields</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldCustomerGroups*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldCustomerGroups.index'); ?>"><i class="fa fa-edit"></i><span>custom field customer groups</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>custom field descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldValues*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldValues.index'); ?>"><i class="fa fa-edit"></i><span>custom field values</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCustomFieldValueDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCustomFieldValueDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>custom field value descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Downloads</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocDownloads*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocDownloads.index'); ?>"><i class="fa fa-edit"></i><span>downloads</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocDownloadDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocDownloadDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>download descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocEvents*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocEvents.index'); ?>"><i class="fa fa-edit"></i><span>events</span></a>
</li>

<li class="<?php echo e(Request::is('ocExtensions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocExtensions.index'); ?>"><i class="fa fa-edit"></i><span>extensions</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Filters</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocFilters*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocFilters.index'); ?>"><i class="fa fa-edit"></i><span>filters</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocFilterDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocFilterDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>filter descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocGeoZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocGeoZones.index'); ?>"><i class="fa fa-edit"></i><span>geo zones</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Informations</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocInformations*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocInformations.index'); ?>"><i class="fa fa-edit"></i><span>informations</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocInformationDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocInformationDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>information descriptions</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocLanguages*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLanguages.index'); ?>"><i class="fa fa-edit"></i><span>languages</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Layout Modules</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocLayoutModules*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocLayoutModules.index'); ?>"><i class="fa fa-edit"></i><span>layout modules</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocLayoutRoutes*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocLayoutRoutes.index'); ?>"><i class="fa fa-edit"></i><span>layout routes</span></a>
        </li>
    </ul>
</li>

<li class="<?php echo e(Request::is('ocLengthClassDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLengthClassDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>length class descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocLocations*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocLocations.index'); ?>"><i class="fa fa-edit"></i><span>locations</span></a>
</li>

<li class="<?php echo e(Request::is('ocManufacturers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocManufacturers.index'); ?>"><i class="fa fa-edit"></i><span>manufacturers</span></a>
</li>

<li class="<?php echo e(Request::is('ocMarketings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocMarketings.index'); ?>"><i class="fa fa-edit"></i><span>marketings</span></a>
</li>

<li class="<?php echo e(Request::is('ocModifications*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocModifications.index'); ?>"><i class="fa fa-edit"></i><span>modifications</span></a>
</li>

<li class="<?php echo e(Request::is('ocModules*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocModules.index'); ?>"><i class="fa fa-edit"></i><span>modules</span></a>
</li>

<li class="<?php echo e(Request::is('ocOptionValues*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOptionValues.index'); ?>"><i class="fa fa-edit"></i><span>option values</span></a>
</li>

<li class="<?php echo e(Request::is('ocOptionValueDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOptionValueDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>option value descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrders*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrders.index'); ?>"><i class="fa fa-edit"></i><span>orders</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderCustomFields*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderCustomFields.index'); ?>"><i class="fa fa-edit"></i><span>order custom fields</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderHistories.index'); ?>"><i class="fa fa-edit"></i><span>order histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderOptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderOptions.index'); ?>"><i class="fa fa-edit"></i><span>order options</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderProducts*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderProducts.index'); ?>"><i class="fa fa-edit"></i><span>order products</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderRecurrings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderRecurrings.index'); ?>"><i class="fa fa-edit"></i><span>order recurrings</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderRecurringTransactions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderRecurringTransactions.index'); ?>"><i class="fa fa-edit"></i><span>order recurring transactions</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderTotals*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderTotals.index'); ?>"><i class="fa fa-edit"></i><span>order totals</span></a>
</li>

<li class="<?php echo e(Request::is('ocOrderVouchers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocOrderVouchers.index'); ?>"><i class="fa fa-edit"></i><span>order vouchers</span></a>
</li>

<li class="<?php echo e(Request::is('ocRecurrings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocRecurrings.index'); ?>"><i class="fa fa-edit"></i><span>recurrings</span></a>
</li>

<li class="<?php echo e(Request::is('ocReturns*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReturns.index'); ?>"><i class="fa fa-edit"></i><span>returns</span></a>
</li>

<li class="<?php echo e(Request::is('ocReturnHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReturnHistories.index'); ?>"><i class="fa fa-edit"></i><span>return histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocReviews*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocReviews.index'); ?>"><i class="fa fa-edit"></i><span>reviews</span></a>
</li>

<li class="<?php echo e(Request::is('ocSettings*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocSettings.index'); ?>"><i class="fa fa-edit"></i><span>settings</span></a>
</li>

<li class="<?php echo e(Request::is('ocStores*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocStores.index'); ?>"><i class="fa fa-edit"></i><span>stores</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxClasses*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxClasses.index'); ?>"><i class="fa fa-edit"></i><span>tax classes</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxRates*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxRates.index'); ?>"><i class="fa fa-edit"></i><span>tax rates</span></a>
</li>

<li class="<?php echo e(Request::is('ocTaxRules*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocTaxRules.index'); ?>"><i class="fa fa-edit"></i><span>tax rules</span></a>
</li>

<li class="<?php echo e(Request::is('ocUploads*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUploads.index'); ?>"><i class="fa fa-edit"></i><span>uploads</span></a>
</li>

<li class="<?php echo e(Request::is('ocUsers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUsers.index'); ?>"><i class="fa fa-edit"></i><span>users</span></a>
</li>

<li class="<?php echo e(Request::is('ocUserGroups*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocUserGroups.index'); ?>"><i class="fa fa-edit"></i><span>user groups</span></a>
</li>

<li class="<?php echo e(Request::is('ocVouchers*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocVouchers.index'); ?>"><i class="fa fa-edit"></i><span>vouchers</span></a>
</li>

<li class="<?php echo e(Request::is('ocVoucherHistories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocVoucherHistories.index'); ?>"><i class="fa fa-edit"></i><span>voucher histories</span></a>
</li>

<li class="<?php echo e(Request::is('ocWeightClassDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocWeightClassDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>weight class descriptions</span></a>
</li>

<li class="<?php echo e(Request::is('ocZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocZones.index'); ?>"><i class="fa fa-edit"></i><span>zones</span></a>
</li>

<li class="<?php echo e(Request::is('ocZoneToGeoZones*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocZoneToGeoZones.index'); ?>"><i class="fa fa-edit"></i><span>zone to geo zones</span></a>
</li>