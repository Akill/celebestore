<!-- Product Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('product_id', 'Product:'); ?>

    <?php  $items = App\Models\oc_product::has('detail')->get();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e(App\Models\oc_product_description::find($i->id)->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('customer_group_id', 'Customer Group Id:'); ?>

    <?php  $items = App\Models\oc_customer_group_description::all();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <?php echo Form::number('quantity', null, ['class' => 'form-control']); ?>

</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('priority', 'Priority:'); ?>

    <?php echo Form::number('priority', null, ['class' => 'form-control']); ?>

</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('price', 'Price:'); ?>

    <?php echo Form::number('price', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_start', 'Date Start:'); ?>

    <?php echo Form::date('date_start', null, ['class' => 'form-control']); ?>

</div>

<!-- Date End Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_end', 'Date End:'); ?>

    <?php echo Form::date('date_end', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductDiscounts.index'); ?>" class="btn btn-default">Cancel</a>
</div>
