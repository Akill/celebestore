<?php  
$active = Request::is('ocProducts*') ? 'active' : Request::is('ocProductAttributes*') ? 'active' : Request::is('ocProductDescriptions*') ? 'active' : Request::is('ocProductDiscounts*') ? 'active' : Request::is('ocProductImages*') ? 'active' : Request::is('ocProductOptions*') ? 'active' : Request::is('ocProductOptionValues*') ? 'active' : Request::is('ocProductRewards*') ? 'active' : Request::is('ocProductSpecials*') ? 'active' : Request::is('ocProductToCategories*') ? 'active' : 
Request::is('ocProductToDownloads*') ? 'active' : Request::is('ocProductToLayouts*') ? 'active' : 
Request::is('ocProductToStores*') ? 'active' : ''; 
 ?>

<li class="<?php echo e($active); ?> treeview menu-open">
    <a href="#">
        <span>Products</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocProducts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProducts.index'); ?>"><i class="fa fa-table"></i><span>product </span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductAttributes*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductAttributes.index'); ?>"><i class="fa fa-table"></i><span>product attributes</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductDescriptions.index'); ?>"><i class="fa fa-table"></i><span>product descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductDiscounts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductDiscounts.index'); ?>"><i class="fa fa-table"></i><span>product discounts</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductImages*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductImages.index'); ?>"><i class="fa fa-table"></i><span>product images</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductOptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductOptions.index'); ?>"><i class="fa fa-table"></i><span>product options</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductOptionValues*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductOptionValues.index'); ?>"><i class="fa fa-table"></i><span>product option values</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductRewards*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductRewards.index'); ?>"><i class="fa fa-table"></i><span>product rewards</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductSpecials*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductSpecials.index'); ?>"><i class="fa fa-table"></i><span>product <s></s>pecials</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductToCategories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductToCategories.index'); ?>"><i class="fa fa-table"></i><span>product to categories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductToDownloads*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductToDownloads.index'); ?>"><i class="fa fa-table"></i><span>product to downloads</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductToLayouts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductToLayouts.index'); ?>"><i class="fa fa-table"></i><span>product to layouts</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocProductToStores*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocProductToStores.index'); ?>"><i class="fa fa-table"></i><span>product to stores</span></a>
        </li>
    </ul>
</li>