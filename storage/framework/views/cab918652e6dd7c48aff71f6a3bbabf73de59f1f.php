<table class="table table-responsive" id="ocCustomerGroupDescriptions-table">
    <thead>
        <th>Customer Group Id</th>
        <th>Language Id</th>
        <th>Name</th>
        <th>Description</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCustomerGroupDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCustomerGroupDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCustomerGroupDescription->customer_group_id; ?></td>
            <td><?php echo $ocCustomerGroupDescription->language_id; ?></td>
            <td><?php echo $ocCustomerGroupDescription->name; ?></td>
            <td><?php echo $ocCustomerGroupDescription->description; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCustomerGroupDescriptions.destroy', $ocCustomerGroupDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCustomerGroupDescriptions.show', [$ocCustomerGroupDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCustomerGroupDescriptions.edit', [$ocCustomerGroupDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>