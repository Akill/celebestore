<li class="<?php echo e(Request::is('home') ? 'active' : ''); ?>">
    <a href="<?php echo route('home'); ?>">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
<?php  
$catalog = Request::is('ocProducts*') ? 'active' : Request::is('ocProductAttributes*') ? 'active' : Request::is('ocProductDescriptions*') ? 'active' : Request::is('ocProductDiscounts*') ? 'active' : Request::is('ocProductImages*') ? 'active' : Request::is('ocProductOptions*') ? 'active' : Request::is('ocProductOptionValues*') ? 'active' : Request::is('ocProductRewards*') ? 'active' : Request::is('ocProductSpecials*') ? 'active' : Request::is('ocProductToCategories*') ? 'active' : 
Request::is('ocProductToDownloads*') ? 'active' : Request::is('ocProductToLayouts*') ? 'active' : 
Request::is('ocProductToStores*') ? 'active' : Request::is('ocCategories*') ? 'active' : Request::is('ocCategoryDescriptions*') ? 'active' : Request::is('ocCategoryFilters*') ? 'active' : Request::is('ocCategoryPaths*') ? 'active' : Request::is('ocCategoryToLayouts*') ? 'active' : Request::is('ocCategoryToStores*') ? 'active' : ''; 
 ?>
<li class="<?php echo e($catalog); ?> treeview menu-open">
    <a href="#">
        <i class="fa fa-tags"></i> <span>Catalog</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
	<?php echo $__env->make('layouts.menu.products.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('layouts.menu.categories.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-puzzle-piece"></i> <span>Extension</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-television"></i> <span>Design</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<?php  
$sales = Request::is('ocOrders*') ? 'active' : Request::is('ocOrderCustomFields*') ? 'active' : Request::is('ocOrderHistories*') ? 'active' : Request::is('ocOrderOptions*') ? 'active' : Request::is('ocOrderProducts*') ? 'active' : Request::is('ocOrderRecurrings*') ? 'active' : Request::is('ocOrderRecurringTransactions*') ? 'active' : Request::is('ocOrderTotals*') ? 'active' : Request::is('ocOrderVouchers*') ? 'active' : ''; 
 ?>
<li class="<?php echo e($sales); ?> treeview menu-open">
    <a href="#">
        <i class="fa fa-shopping-cart"></i> <span>Sales</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
	<?php echo $__env->make('layouts.menu.orders.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-user"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-share-alt"></i> <span>Marketing</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-cog"></i> <span>System</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>

<li class="<?php echo e(Request::is('users*') ? 'active' : ''); ?>">
    <a href="<?php echo route('users.index'); ?>"><i class="fa fa-table"></i><span>users</span></a>
</li>


<li class="<?php echo e(Request::is('ocCategoryDescriptions*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocCategoryDescriptions.index'); ?>"><i class="fa fa-edit"></i><span>oc_category_descriptions</span></a>
</li>



<li class="<?php echo e(Request::is('ocProductToCategories*') ? 'active' : ''); ?>">
    <a href="<?php echo route('ocProductToCategories.index'); ?>"><i class="fa fa-edit"></i><span>oc_product_to_categories</span></a>
</li>

