<table class="table table-responsive" id="ocAttributes-table">
    <thead>
        <th>Attribute Group Id</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAttribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAttribute->attribute_group_id; ?></td>
            <td><?php echo $ocAttribute->sort_order; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAttributes.destroy', $ocAttribute->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAttributes.show', [$ocAttribute->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAttributes.edit', [$ocAttribute->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>