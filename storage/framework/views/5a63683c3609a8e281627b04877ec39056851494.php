<!-- Image Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::text('image', null, ['class' => 'form-control']); ?>

</div>

<!-- Parent Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('parent_id', 'Parent Id:'); ?>

    <?php echo Form::number('parent_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Top Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('top', 'Top:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('top', false); ?>

        <?php echo Form::checkbox('top', '1', null); ?> 1
    </label>
</div>

<!-- Column Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('column', 'Column:'); ?>

    <?php echo Form::number('column', null, ['class' => 'form-control']); ?>

</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <?php echo Form::number('sort_order', null, ['class' => 'form-control']); ?>

</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('status', false); ?>

        <?php echo Form::checkbox('status', '1', null); ?> 1
    </label>
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <?php echo Form::date('date_added', null, ['class' => 'form-control']); ?>

</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_modified', 'Date Modified:'); ?>

    <?php echo Form::date('date_modified', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocCategories.index'); ?>" class="btn btn-default">Cancel</a>
</div>
