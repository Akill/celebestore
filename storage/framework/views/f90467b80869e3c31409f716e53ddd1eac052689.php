<table class="table table-responsive" id="ocUsers-table">
    <thead>
        <th>User Group Id</th>
        <th>Username</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Image</th>
        <th>Code</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocUsers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocUser->user_group_id; ?></td>
            <td><?php echo $ocUser->username; ?></td>
            <td><?php echo $ocUser->password; ?></td>
            <td><?php echo $ocUser->salt; ?></td>
            <td><?php echo $ocUser->firstname; ?></td>
            <td><?php echo $ocUser->lastname; ?></td>
            <td><?php echo $ocUser->email; ?></td>
            <td><?php echo $ocUser->image; ?></td>
            <td><?php echo $ocUser->code; ?></td>
            <td><?php echo $ocUser->ip; ?></td>
            <td><?php echo $ocUser->status; ?></td>
            <td><?php echo $ocUser->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocUsers.destroy', $ocUser->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocUsers.show', [$ocUser->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocUsers.edit', [$ocUser->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>