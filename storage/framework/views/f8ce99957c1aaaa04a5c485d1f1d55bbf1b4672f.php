<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocProduct->id; ?></p>
</div>

<!-- Model Field -->
<div class="form-group">
    <?php echo Form::label('model', 'Model:'); ?>

    <p><?php echo $ocProduct->model; ?></p>
</div>

<!-- Sku Field -->
<div class="form-group">
    <?php echo Form::label('sku', 'Sku:'); ?>

    <p><?php echo $ocProduct->sku; ?></p>
</div>

<!-- Upc Field -->
<div class="form-group">
    <?php echo Form::label('upc', 'Upc:'); ?>

    <p><?php echo $ocProduct->upc; ?></p>
</div>

<!-- Ean Field -->
<div class="form-group">
    <?php echo Form::label('ean', 'Ean:'); ?>

    <p><?php echo $ocProduct->ean; ?></p>
</div>

<!-- Jan Field -->
<div class="form-group">
    <?php echo Form::label('jan', 'Jan:'); ?>

    <p><?php echo $ocProduct->jan; ?></p>
</div>

<!-- Isbn Field -->
<div class="form-group">
    <?php echo Form::label('isbn', 'Isbn:'); ?>

    <p><?php echo $ocProduct->isbn; ?></p>
</div>

<!-- Mpn Field -->
<div class="form-group">
    <?php echo Form::label('mpn', 'Mpn:'); ?>

    <p><?php echo $ocProduct->mpn; ?></p>
</div>

<!-- Location Field -->
<div class="form-group">
    <?php echo Form::label('location', 'Location:'); ?>

    <p><?php echo $ocProduct->location; ?></p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    <?php echo Form::label('quantity', 'Quantity:'); ?>

    <p><?php echo $ocProduct->quantity; ?></p>
</div>

<!-- Stock Status Id Field -->
<div class="form-group">
    <?php echo Form::label('stock_status_id', 'Stock Status Id:'); ?>

    <p><?php echo $ocProduct->stock_status_id; ?></p>
</div>

<!-- Image Field -->
<div class="form-group">
    <?php echo Form::label('image', 'Image:'); ?>

    <p><?php echo $ocProduct->image; ?></p>
</div>

<!-- Manufacturer Id Field -->
<div class="form-group">
    <?php echo Form::label('manufacturer_id', 'Manufacturer Id:'); ?>

    <p><?php echo $ocProduct->manufacturer_id; ?></p>
</div>

<!-- Shipping Field -->
<div class="form-group">
    <?php echo Form::label('shipping', 'Shipping:'); ?>

    <p><?php echo $ocProduct->shipping; ?></p>
</div>

<!-- Price Field -->
<div class="form-group">
    <?php echo Form::label('price', 'Price:'); ?>

    <p><?php echo $ocProduct->price; ?></p>
</div>

<!-- Points Field -->
<div class="form-group">
    <?php echo Form::label('points', 'Points:'); ?>

    <p><?php echo $ocProduct->points; ?></p>
</div>

<!-- Tax Class Id Field -->
<div class="form-group">
    <?php echo Form::label('tax_class_id', 'Tax Class Id:'); ?>

    <p><?php echo $ocProduct->tax_class_id; ?></p>
</div>

<!-- Date Available Field -->
<div class="form-group">
    <?php echo Form::label('date_available', 'Date Available:'); ?>

    <p><?php echo $ocProduct->date_available; ?></p>
</div>

<!-- Weight Field -->
<div class="form-group">
    <?php echo Form::label('weight', 'Weight:'); ?>

    <p><?php echo $ocProduct->weight; ?></p>
</div>

<!-- Weight Class Id Field -->
<div class="form-group">
    <?php echo Form::label('weight_class_id', 'Weight Class Id:'); ?>

    <p><?php echo $ocProduct->weight_class_id; ?></p>
</div>

<!-- Length Field -->
<div class="form-group">
    <?php echo Form::label('length', 'Length:'); ?>

    <p><?php echo $ocProduct->length; ?></p>
</div>

<!-- Width Field -->
<div class="form-group">
    <?php echo Form::label('width', 'Width:'); ?>

    <p><?php echo $ocProduct->width; ?></p>
</div>

<!-- Height Field -->
<div class="form-group">
    <?php echo Form::label('height', 'Height:'); ?>

    <p><?php echo $ocProduct->height; ?></p>
</div>

<!-- Length Class Id Field -->
<div class="form-group">
    <?php echo Form::label('length_class_id', 'Length Class Id:'); ?>

    <p><?php echo $ocProduct->length_class_id; ?></p>
</div>

<!-- Subtract Field -->
<div class="form-group">
    <?php echo Form::label('subtract', 'Subtract:'); ?>

    <p><?php echo $ocProduct->subtract; ?></p>
</div>

<!-- Minimum Field -->
<div class="form-group">
    <?php echo Form::label('minimum', 'Minimum:'); ?>

    <p><?php echo $ocProduct->minimum; ?></p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <p><?php echo $ocProduct->sort_order; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $ocProduct->status; ?></p>
</div>

<!-- Viewed Field -->
<div class="form-group">
    <?php echo Form::label('viewed', 'Viewed:'); ?>

    <p><?php echo $ocProduct->viewed; ?></p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <p><?php echo $ocProduct->date_added; ?></p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    <?php echo Form::label('date_modified', 'Date Modified:'); ?>

    <p><?php echo $ocProduct->date_modified; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocProduct->deleted_at; ?></p>
</div>

