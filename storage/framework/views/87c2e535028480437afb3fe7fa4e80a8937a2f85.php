<table class="table table-responsive" id="ocOrderTotals-table">
    <thead>
        <th>Order Id</th>
        <th>Code</th>
        <th>Title</th>
        <th>Value</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocOrderTotals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocOrderTotal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocOrderTotal->order_id; ?></td>
            <td><?php echo $ocOrderTotal->code; ?></td>
            <td><?php echo $ocOrderTotal->title; ?></td>
            <td><?php echo $ocOrderTotal->value; ?></td>
            <td><?php echo $ocOrderTotal->sort_order; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocOrderTotals.destroy', $ocOrderTotal->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocOrderTotals.show', [$ocOrderTotal->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocOrderTotals.edit', [$ocOrderTotal->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>