<table class="table table-responsive" id="ocAttributeDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAttributeDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAttributeDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAttributeDescription->language_id; ?></td>
            <td><?php echo $ocAttributeDescription->name; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAttributeDescriptions.destroy', $ocAttributeDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAttributeDescriptions.show', [$ocAttributeDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAttributeDescriptions.edit', [$ocAttributeDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>