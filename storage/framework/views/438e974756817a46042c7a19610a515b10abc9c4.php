<!-- User Group Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('user_group_id', 'User Group Id:'); ?>

    <?php echo Form::number('user_group_id', null, ['class' => 'form-control']); ?>

</div>

<!-- Username Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('username', 'Username:'); ?>

    <?php echo Form::text('username', null, ['class' => 'form-control']); ?>

</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('password', 'Password:'); ?>

    <?php echo Form::password('password', ['class' => 'form-control']); ?>

</div>

<!-- Salt Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('salt', 'Salt:'); ?>

    <?php echo Form::text('salt', null, ['class' => 'form-control']); ?>

</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('firstname', 'Firstname:'); ?>

    <?php echo Form::text('firstname', null, ['class' => 'form-control']); ?>

</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('lastname', 'Lastname:'); ?>

    <?php echo Form::text('lastname', null, ['class' => 'form-control']); ?>

</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('email', 'Email:'); ?>

    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::text('image', null, ['class' => 'form-control']); ?>

</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('code', 'Code:'); ?>

    <?php echo Form::text('code', null, ['class' => 'form-control']); ?>

</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('ip', 'Ip:'); ?>

    <?php echo Form::text('ip', null, ['class' => 'form-control']); ?>

</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('status', 'Status:'); ?>

    <label class="checkbox-inline">
        <?php echo Form::hidden('status', false); ?>

        <?php echo Form::checkbox('status', '1', null); ?> 1
    </label>
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('date_added', 'Date Added:'); ?>

    <?php echo Form::date('date_added', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocUsers.index'); ?>" class="btn btn-default">Cancel</a>
</div>
