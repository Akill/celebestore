<table class="table table-responsive" id="ocOrderVouchers-table">
    <thead>
        <th>Order Id</th>
        <th>Voucher Id</th>
        <th>Description</th>
        <th>Code</th>
        <th>From Name</th>
        <th>From Email</th>
        <th>To Name</th>
        <th>To Email</th>
        <th>Voucher Theme Id</th>
        <th>Message</th>
        <th>Amount</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocOrderVouchers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocOrderVoucher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocOrderVoucher->order_id; ?></td>
            <td><?php echo $ocOrderVoucher->voucher_id; ?></td>
            <td><?php echo $ocOrderVoucher->description; ?></td>
            <td><?php echo $ocOrderVoucher->code; ?></td>
            <td><?php echo $ocOrderVoucher->from_name; ?></td>
            <td><?php echo $ocOrderVoucher->from_email; ?></td>
            <td><?php echo $ocOrderVoucher->to_name; ?></td>
            <td><?php echo $ocOrderVoucher->to_email; ?></td>
            <td><?php echo $ocOrderVoucher->voucher_theme_id; ?></td>
            <td><?php echo $ocOrderVoucher->message; ?></td>
            <td><?php echo $ocOrderVoucher->amount; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocOrderVouchers.destroy', $ocOrderVoucher->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocOrderVouchers.show', [$ocOrderVoucher->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocOrderVouchers.edit', [$ocOrderVoucher->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>