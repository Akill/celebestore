<table class="table table-responsive" id="users-table">
    <thead>
        <th>User Group</th>
        <th>Name</th>
        <th>Email</th>
        <!-- <th>Password</th> -->
        <th>Username</th>
        <!-- <th>Type Of Identity</th> -->
        <!-- <th>Identity Number</th> -->
        <!-- <th>Gender</th> -->
        <th>Phone</th>
        <!-- <th>Job</th> -->
        <!-- <th>Address</th> -->
        <!-- <th>Districts</th> -->
        <!-- <th>City</th> -->
        <!-- <th>Province</th> -->
        <!-- <th>Zip Kode</th> -->
        <!-- <th>Picture</th> -->
        <!-- <th>Remember Token</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo App\Models\oc_user_group::find($user->user_group_id)->name; ?></td>
            <td><?php echo $user->name; ?></td>
            <td><?php echo $user->email; ?></td>
            <!-- <td><?php echo $user->password; ?></td> -->
            <td><?php echo $user->username; ?></td>
            <!-- <td><?php echo $user->type_of_identity; ?></td> -->
            <!-- <td><?php echo $user->identity_number; ?></td> -->
            <!-- <td><?php echo $user->gender; ?></td> -->
            <td><?php echo $user->phone; ?></td>
            <!-- <td><?php echo $user->job; ?></td> -->
            <!-- <td><?php echo $user->address; ?></td> -->
            <!-- <td><?php echo $user->districts; ?></td> -->
            <!-- <td><?php echo $user->city; ?></td> -->
            <!-- <td><?php echo $user->province; ?></td> -->
            <!-- <td><?php echo $user->zip_kode; ?></td> -->
            <!-- <td><?php echo $user->picture; ?></td> -->
            <!-- <td><?php echo $user->remember_token; ?></td> -->
            <td>
                <?php echo Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('users.show', [$user->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('users.edit', [$user->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>