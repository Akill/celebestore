<table class="table table-responsive" id="ocProductOptionValues-table">
    <thead>
        <th>Product Option Id</th>
        <th>Product Id</th>
        <th>Option Id</th>
        <th>Option Value Id</th>
        <th>Quantity</th>
        <th>Subtract</th>
        <th>Price</th>
        <th>Price Prefix</th>
        <th>Points</th>
        <th>Points Prefix</th>
        <th>Weight</th>
        <th>Weight Prefix</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductOptionValues; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductOptionValue): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductOptionValue->product_option_id; ?></td>
            <td><?php echo $ocProductOptionValue->product_id; ?></td>
            <td><?php echo $ocProductOptionValue->option_id; ?></td>
            <td><?php echo $ocProductOptionValue->option_value_id; ?></td>
            <td><?php echo $ocProductOptionValue->quantity; ?></td>
            <td><?php echo $ocProductOptionValue->subtract; ?></td>
            <td><?php echo $ocProductOptionValue->price; ?></td>
            <td><?php echo $ocProductOptionValue->price_prefix; ?></td>
            <td><?php echo $ocProductOptionValue->points; ?></td>
            <td><?php echo $ocProductOptionValue->points_prefix; ?></td>
            <td><?php echo $ocProductOptionValue->weight; ?></td>
            <td><?php echo $ocProductOptionValue->weight_prefix; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductOptionValues.destroy', $ocProductOptionValue->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductOptionValues.show', [$ocProductOptionValue->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductOptionValues.edit', [$ocProductOptionValue->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>