<table class="table table-responsive" id="ocAttributeGroups-table">
    <thead>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAttributeGroups; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAttributeGroup): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAttributeGroup->sort_order; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAttributeGroups.destroy', $ocAttributeGroup->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAttributeGroups.show', [$ocAttributeGroup->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAttributeGroups.edit', [$ocAttributeGroup->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>