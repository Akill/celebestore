<table class="table table-responsive" id="ocProductAttributes-table">
    <thead>
        <th>Product</th>
        <th>Attribute</th>
        <th>Language</th>
        <th>Text</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductAttributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductAttribute): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo App\Models\oc_product_description::where('product_id',$ocProductAttribute->product_id)->first()->name; ?></td>
            <td><?php echo App\Models\oc_attribute_description::where('attribute_id',$ocProductAttribute->attribute_id)->first()->name; ?></td>
            <td><?php echo App\Models\oc_language::find($ocProductAttribute->language_id)->name; ?></td>
            <td><?php echo $ocProductAttribute->text; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductAttributes.destroy', $ocProductAttribute->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductAttributes.show', [$ocProductAttribute->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductAttributes.edit', [$ocProductAttribute->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>