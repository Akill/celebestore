<?php  
$active = Request::is('ocCategories*') ? 'active' : 
Request::is('ocCategoryDescriptions*') ? 'active' : 
Request::is('ocCategoryFilters*') ? 'active' : 
Request::is('ocCategoryPaths*') ? 'active' : 
Request::is('ocCategoryToLayouts*') ? 'active' : 
Request::is('ocCategoryToStores*') ? 'active' : ''; 
 ?>

<li class="<?php echo e($active); ?> treeview menu-open">
    <a href="#">
        <span>Categories</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo e(Request::is('ocCategories*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategories.index'); ?>"><i class="fa fa-table"></i><span>categories</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryDescriptions*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryDescriptions.index'); ?>"><i class="fa fa-table"></i><span>category descriptions</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryFilters*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryFilters.index'); ?>"><i class="fa fa-table"></i><span>category filters</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryPaths*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryPaths.index'); ?>"><i class="fa fa-table"></i><span>category paths</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryToLayouts*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryToLayouts.index'); ?>"><i class="fa fa-table"></i><span>category to layouts</span></a>
        </li>

        <li class="<?php echo e(Request::is('ocCategoryToStores*') ? 'active' : ''); ?>">
            <a href="<?php echo route('ocCategoryToStores.index'); ?>"><i class="fa fa-table"></i><span>category to stores</span></a>
        </li>
    </ul>
</li>