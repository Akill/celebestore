<table class="table table-responsive" id="ocProductSpecials-table">
    <thead>
        <th>Product Id</th>
        <th>Customer Group Id</th>
        <th>Priority</th>
        <th>Price</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductSpecials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductSpecial): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductSpecial->product_id; ?></td>
            <td><?php echo $ocProductSpecial->customer_group_id; ?></td>
            <td><?php echo $ocProductSpecial->priority; ?></td>
            <td><?php echo $ocProductSpecial->price; ?></td>
            <td><?php echo $ocProductSpecial->date_start; ?></td>
            <td><?php echo $ocProductSpecial->date_end; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductSpecials.destroy', $ocProductSpecial->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductSpecials.show', [$ocProductSpecial->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductSpecials.edit', [$ocProductSpecial->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>