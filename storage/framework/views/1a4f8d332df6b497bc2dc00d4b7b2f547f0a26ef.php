<table class="table table-responsive" id="ocCountries-table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th>Address Format</th>
        <th>Iso Code 2</th>
        <th>Iso Code 3</th>
        <th>Postcode Required</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCountries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCountry): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCountry->name; ?></td>
            <td><?php echo $ocCountry->code; ?></td>
            <td><?php echo $ocCountry->status; ?></td>
            <td><?php echo $ocCountry->address_format; ?></td>
            <td><?php echo $ocCountry->iso_code_2; ?></td>
            <td><?php echo $ocCountry->iso_code_3; ?></td>
            <td><?php echo $ocCountry->postcode_required; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCountries.destroy', $ocCountry->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCountries.show', [$ocCountry->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCountries.edit', [$ocCountry->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>