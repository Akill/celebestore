<!-- User Group Id Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('user_group_id', 'User Group Id:'); ?>

    <?php  $items = App\Models\oc_user_group::all(['name','id'])  ?>
    <select class="form-control" id="user_group_id" name="user_group_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e($i->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('name', 'Name:'); ?>

    <?php echo Form::text('name', null, ['class' => 'form-control']); ?>

</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('email', 'Email:'); ?>

    <?php echo Form::email('email', null, ['class' => 'form-control']); ?>

</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('password', 'Password:'); ?>

    <?php echo Form::password('password', ['class' => 'form-control']); ?>

</div>

<!-- Username Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('username', 'Username:'); ?>

    <?php echo Form::text('username', null, ['class' => 'form-control']); ?>

</div>

<!-- Type Of Identity Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('type_of_identity', 'Type Of Identity:'); ?>

    <?php echo Form::text('type_of_identity', null, ['class' => 'form-control']); ?>

</div>

<!-- Identity Number Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('identity_number', 'Identity Number:'); ?>

    <?php echo Form::textarea('identity_number', null, ['class' => 'form-control']); ?>

</div>

<!-- Gender Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('gender', 'Gender:'); ?>

    <?php echo Form::text('gender', null, ['class' => 'form-control']); ?>

</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('phone', 'Phone:'); ?>

    <?php echo Form::text('phone', null, ['class' => 'form-control']); ?>

</div>

<!-- Job Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('job', 'Job:'); ?>

    <?php echo Form::text('job', null, ['class' => 'form-control']); ?>

</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('address', 'Address:'); ?>

    <?php echo Form::text('address', null, ['class' => 'form-control']); ?>

</div>

<!-- Districts Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('districts', 'Districts:'); ?>

    <?php echo Form::text('districts', null, ['class' => 'form-control']); ?>

</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('city', 'City:'); ?>

    <?php echo Form::text('city', null, ['class' => 'form-control']); ?>

</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('province', 'Province:'); ?>

    <?php echo Form::text('province', null, ['class' => 'form-control']); ?>

</div>

<!-- Zip Kode Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('zip_kode', 'Zip Kode:'); ?>

    <?php echo Form::text('zip_kode', null, ['class' => 'form-control']); ?>

</div>

<!-- Picture Field -->
<div class="form-group col-sm-12 col-lg-12">
    <?php echo Form::label('picture', 'Picture:'); ?>

    <?php echo Form::textarea('picture', null, ['class' => 'form-control']); ?>

</div>

<!-- Remember Token Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('remember_token', 'Remember Token:'); ?>

    <?php echo Form::text('remember_token', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('users.index'); ?>" class="btn btn-default">Cancel</a>
</div>
