<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocProductDescription->id; ?></p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    <?php echo Form::label('product_id', 'Product Id:'); ?>

    <p><?php echo $ocProductDescription->product_id; ?></p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <p><?php echo App\Models\oc_language::find($ocProductDescription->language_id)->name; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $ocProductDescription->name; ?></p>
</div>

<!-- Description Field -->
<div class="form-group">
    <?php echo Form::label('description', 'Description:'); ?>

    <p><?php echo $ocProductDescription->description; ?></p>
</div>

<!-- Tag Field -->
<div class="form-group">
    <?php echo Form::label('tag', 'Tag:'); ?>

    <p><?php echo $ocProductDescription->tag; ?></p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    <?php echo Form::label('meta_title', 'Meta Title:'); ?>

    <p><?php echo $ocProductDescription->meta_title; ?></p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    <?php echo Form::label('meta_description', 'Meta Description:'); ?>

    <p><?php echo $ocProductDescription->meta_description; ?></p>
</div>

<!-- Meta Keyword Field -->
<div class="form-group">
    <?php echo Form::label('meta_keyword', 'Meta Keyword:'); ?>

    <p><?php echo $ocProductDescription->meta_keyword; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocProductDescription->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocProductDescription->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocProductDescription->deleted_at; ?></p>
</div>

