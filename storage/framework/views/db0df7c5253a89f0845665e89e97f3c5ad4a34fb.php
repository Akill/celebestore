<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocCountry->id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $ocCountry->name; ?></p>
</div>

<!-- Code Field -->
<div class="form-group">
    <?php echo Form::label('code', 'Code:'); ?>

    <p><?php echo $ocCountry->code; ?></p>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('status', 'Status:'); ?>

    <p><?php echo $ocCountry->status; ?></p>
</div>

<!-- Address Format Field -->
<div class="form-group">
    <?php echo Form::label('address_format', 'Address Format:'); ?>

    <p><?php echo $ocCountry->address_format; ?></p>
</div>

<!-- Iso Code 2 Field -->
<div class="form-group">
    <?php echo Form::label('iso_code_2', 'Iso Code 2:'); ?>

    <p><?php echo $ocCountry->iso_code_2; ?></p>
</div>

<!-- Iso Code 3 Field -->
<div class="form-group">
    <?php echo Form::label('iso_code_3', 'Iso Code 3:'); ?>

    <p><?php echo $ocCountry->iso_code_3; ?></p>
</div>

<!-- Postcode Required Field -->
<div class="form-group">
    <?php echo Form::label('postcode_required', 'Postcode Required:'); ?>

    <p><?php echo $ocCountry->postcode_required; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocCountry->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocCountry->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocCountry->deleted_at; ?></p>
</div>

