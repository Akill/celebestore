<table class="table table-responsive" id="ocWeightClassDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Title</th>
        <th>Unit</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocWeightClassDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocWeightClassDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocWeightClassDescription->language_id; ?></td>
            <td><?php echo $ocWeightClassDescription->title; ?></td>
            <td><?php echo $ocWeightClassDescription->unit; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocWeightClassDescriptions.destroy', $ocWeightClassDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocWeightClassDescriptions.show', [$ocWeightClassDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocWeightClassDescriptions.edit', [$ocWeightClassDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>