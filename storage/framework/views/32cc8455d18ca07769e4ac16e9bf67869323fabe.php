<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <aside class="sidebar-left">
                <!-- <h3 class="mb20 text-center">I am looking for</h3> -->
                <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
                    <li><a href="#"><i class="fa fa-cutlery"></i>Food & Drink</a>
                    </li>
                    <li><a href="#"><i class="fa fa-calendar"></i>Events</a>
                    </li>
                    <li><a href="#"><i class="fa fa-female"></i>Beauty</a>
                    </li>
                    <li><a href="#"><i class="fa fa-bolt"></i>Fitness</a>
                    </li>
                    <li><a href="#"><i class="fa fa-headphones"></i>Electronics</a>
                    </li>
                    <li><a href="#"><i class="fa fa-image"></i>Furniture</a>
                    </li>
                    <li><a href="#"><i class="fa fa-umbrella"></i>Fashion</a>
                    </li>
                    <li><a href="#"><i class="fa fa-shopping-cart"></i>Shopping</a>
                    </li>
                    <li><a href="#"><i class="fa fa-home"></i>Home & Graden</a>
                    </li>
                    <li><a href="#"><i class="fa fa-plane"></i>Travel</a>
                    </li>
                </ul>
            </aside>
        </div>
        <div class="col-md-9">
            <!-- <h1 class="mb20 text-center">Weekly Featured <small><a href="#">View All</a></small></h1> -->
            <div class="row row-wrap">
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/green_furniture_800x600.jpg" alt="Image Alternative text" title="Green Furniture" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="3.7/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Green Furniture Pack</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$177</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/hot_mixer_800x600.jpg" alt="Image Alternative text" title="Hot mixer" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="3/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Modern DJ Set</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$190</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/waipio_valley_800x600.jpg" alt="Image Alternative text" title="waipio valley" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="4.1/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-half-empty"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Awesome Vacation</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$135</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="gap"></div> -->
</div>
<!-- 
<div class="bg-holder">
    <div class="bg-mask"></div>
    <div class="bg-blur" style="background-image:url(img/backgrounds/phone.jpg)"></div>
    <div class="container bg-holder-content">
        <div class="gap gap-big text-center">
            <h1 class="mb30 text-white">Weekly Featured Items</h1>
            <div class="row row-wrap">
                <a class="col-md-3" href="#">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/iphone_5_ipad_mini_ipad_3_800x600.jpg" alt="Image Alternative text" title="iPhone 5 iPad mini iPad 3" />
                        </header>
                        <div class="product-inner">
                            <h5 class="product-title">Electronics Big Deal</h5>
                            <p class="product-desciption">Quam habitant aliquam aptent platea platea elit adipiscing</p>
                            <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 4 days 42 h remaining</span>
                                <ul class="product-price-list">
                                    <li><span class="product-price">$172</span>
                                    </li>
                                    <li><span class="product-old-price">$269</span>
                                    </li>
                                    <li><span class="product-save">Save 64%</span>
                                    </li>
                                </ul>
                            </div>
                            <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                        </div>
                    </div>
                </a>
                <a class="col-md-3" href="#">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/a_turn_800x600.jpg" alt="Image Alternative text" title="a turn" />
                        </header>
                        <div class="product-inner">
                            <h5 class="product-title">Diving with Sharks</h5>
                            <p class="product-desciption">Quam habitant aliquam aptent platea platea elit adipiscing</p>
                            <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 10 days 53 h remaining</span>
                                <ul class="product-price-list">
                                    <li><span class="product-price">$87</span>
                                    </li>
                                    <li><span class="product-old-price">$164</span>
                                    </li>
                                    <li><span class="product-save">Save 53%</span>
                                    </li>
                                </ul>
                            </div>
                            <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                        </div>
                    </div>
                </a>
                <a class="col-md-3" href="#">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/nikon_prime_love_800x600.jpg" alt="Image Alternative text" title="Nikon Prime love" />
                        </header>
                        <div class="product-inner">
                            <h5 class="product-title">Best Camera Lenthes</h5>
                            <p class="product-desciption">Quam habitant aliquam aptent platea platea elit adipiscing</p>
                            <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 1 day 11 h remaining</span>
                                <ul class="product-price-list">
                                    <li><span class="product-price">$76</span>
                                    </li>
                                    <li><span class="product-old-price">$230</span>
                                    </li>
                                    <li><span class="product-save">Save 33%</span>
                                    </li>
                                </ul>
                            </div>
                            <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                        </div>
                    </div>
                </a>
                <a class="col-md-3" href="#">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/my_ice_cream_and_your_ice_cream_spoons_800x600.jpg" alt="Image Alternative text" title="My Ice Cream and Your Ice Cream Spoons" />
                        </header>
                        <div class="product-inner">
                            <h5 class="product-title">Lovely Ice Cream Spoons</h5>
                            <p class="product-desciption">Quam habitant aliquam aptent platea platea elit adipiscing</p>
                            <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i>  13 h remaining</span>
                                <ul class="product-price-list">
                                    <li><span class="product-price">$166</span>
                                    </li>
                                    <li><span class="product-old-price">$244</span>
                                    </li>
                                    <li><span class="product-save">Save 68%</span>
                                    </li>
                                </ul>
                            </div>
                            <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                        </div>
                    </div>
                </a>
            </div>	<a href="#" class="btn btn-white btn-lg btn-ghost">Find More</a>
        </div>
    </div>
</div> 
-->
<!-- <div class="gap"></div> -->
<div class="container">
    <div class="text-center">
        <h2 class="mb30">New Deals in Your City</h2>
        <div class="row row-wrap" id="masonry">
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/cup_on_red_800x600.jpg" alt="Image Alternative text" title="Cup on red" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Fancy Coffe Cup</h5>
                        <div class="product-desciption">Nostra at maecenas bibendum netus</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 4 days 36 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$86</span>
                                </li>
                                <li><span class="product-old-price">$132</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/amaze_800x600.jpg" alt="Image Alternative text" title="AMaze" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">New Glass Collection</h5>
                        <div class="product-desciption">Tempus nostra iaculis aliquet orci</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 1 day 3 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$67</span>
                                </li>
                                <li><span class="product-old-price">$148</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/hot_mixer_800x600.jpg" alt="Image Alternative text" title="Hot mixer" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Modern DJ Set</h5>
                        <div class="product-desciption">Auctor dolor eleifend venenatis inceptos</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 10 days 17 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$204</span>
                                </li>
                                <li><span class="product-old-price">$292</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/aspen_lounge_chair_800x600.jpg" alt="Image Alternative text" title="Aspen Lounge Chair" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Aspen Lounge Chair</h5>
                        <div class="product-desciption">Etiam elementum non egestas nascetur</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 3 days 40 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$197</span>
                                </li>
                                <li><span class="product-old-price">$294</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/a_turn_800x600.jpg" alt="Image Alternative text" title="a turn" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Diving with Sharks</h5>
                        <div class="product-desciption">Netus urna sociosqu suscipit feugiat vel</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 5 days 36 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$103</span>
                                </li>
                                <li><span class="product-old-price">$263</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/cascada_800x600.jpg" alt="Image Alternative text" title="cascada" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Adventure in Woods</h5>
                        <div class="product-desciption">Turpis primis nostra placerat facilisi suscipit</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 8 days 30 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$95</span>
                                </li>
                                <li><span class="product-old-price">$164</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/waipio_valley_800x600.jpg" alt="Image Alternative text" title="waipio valley" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Awesome Vacation</h5>
                        <div class="product-desciption">Vestibulum lacinia fermentum sollicitudin tempus aliquam tempus</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 1 day 32 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$189</span>
                                </li>
                                <li><span class="product-old-price">$270</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/gamer_chick_800x600.jpg" alt="Image Alternative text" title="Gamer Chick" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Playstation Accessories</h5>
                        <div class="product-desciption">Ridiculus posuere commodo sociosqu eleifend</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 5 days 12 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$110</span>
                                </li>
                                <li><span class="product-old-price">$289</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/the_violin_800x600.jpg" alt="Image Alternative text" title="The Violin" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Violin Lessons</h5>
                        <div class="product-desciption">Faucibus mauris metus dictumst turpis dui</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 5 days 29 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$152</span>
                                </li>
                                <li><span class="product-old-price">$254</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/my_ice_cream_and_your_ice_cream_spoons_800x600.jpg" alt="Image Alternative text" title="My Ice Cream and Your Ice Cream Spoons" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Lovely Ice Cream Spoons</h5>
                        <div class="product-desciption">Commodo interdum metus suspendisse sollicitudin posuere</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 4 days 44 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$140</span>
                                </li>
                                <li><span class="product-old-price">$200</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/green_furniture_800x600.jpg" alt="Image Alternative text" title="Green Furniture" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Green Furniture Pack</h5>
                        <div class="product-desciption">Nunc dolor leo taciti interdum</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 5 days 16 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$68</span>
                                </li>
                                <li><span class="product-old-price">$159</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/iphone_5_ipad_mini_ipad_3_800x600.jpg" alt="Image Alternative text" title="iPhone 5 iPad mini iPad 3" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Electronics Big Deal</h5>
                        <div class="product-desciption">Per parturient orci ante venenatis</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 3 days 18 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$118</span>
                                </li>
                                <li><span class="product-old-price">$203</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/nikon_prime_love_800x600.jpg" alt="Image Alternative text" title="Nikon Prime love" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Best Camera Lenthes</h5>
                        <div class="product-desciption">Nibh blandit ullamcorper luctus blandit torquent himenaeos</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 9 days 16 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$82</span>
                                </li>
                                <li><span class="product-old-price">$135</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/food_is_pride_800x600.jpg" alt="Image Alternative text" title="Food is Pride" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Best Pasta</h5>
                        <div class="product-desciption">Praesent dapibus lobortis facilisis interdum etiam varius</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 10 days 20 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$62</span>
                                </li>
                                <li><span class="product-old-price">$141</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/ana_29_800x600.jpg" alt="Image Alternative text" title="Ana 29" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Hot Summer Collection</h5>
                        <div class="product-desciption">Dapibus habitasse leo eros sed fames lorem</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 6 days 40 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$62</span>
                                </li>
                                <li><span class="product-old-price">$132</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/the_hidden_power_of_the_heart_800x600.jpg" alt="Image Alternative text" title="The Hidden Power of the Heart" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Beach Holidays</h5>
                        <div class="product-desciption">Tincidunt est dis porta porta montes</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 4 days 25 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$132</span>
                                </li>
                                <li><span class="product-old-price">$227</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/our_coffee_miss_u_800x600.jpg" alt="Image Alternative text" title="Our Coffee miss u" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Coffe Shop Discount</h5>
                        <div class="product-desciption">Senectus metus vitae posuere suspendisse ultrices</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 3 days 8 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$65</span>
                                </li>
                                <li><span class="product-old-price">$150</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
            <a class="col-md-2 col-masonry" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="img/the_best_mode_of_transport_here_in_maldives_800x600.jpg" alt="Image Alternative text" title="the best mode of transport here in maldives" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">Finshing in Maldives</h5>
                        <div class="product-desciption">Tristique velit fringilla neque montes habitant</div>
                        <div class="product-meta"><span class="product-time"><i class="fa fa-clock-o"></i> 3 days 36 h</span>
                            <ul class="product-price-list">
                                <li><span class="product-price">$85</span>
                                </li>
                                <li><span class="product-old-price">$243</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> Boston</p>
                    </div>
                </div>
            </a>
        </div>
        <a href="#" class="btn btn-primary btn-ghost">Explore All New Deals</a>
    </div>
    <div class="gap"></div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layoutsfrontend.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>