<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocCategoryDescription->id; ?></p>
</div>

<!-- Oc Category Id Field -->
<div class="form-group">
    <?php echo Form::label('oc_category_id', 'Oc Category Id:'); ?>

    <p><?php echo $ocCategoryDescription->oc_category_id; ?></p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <p><?php echo $ocCategoryDescription->language_id; ?></p>
</div>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:'); ?>

    <p><?php echo $ocCategoryDescription->name; ?></p>
</div>

<!-- Description Field -->
<div class="form-group">
    <?php echo Form::label('description', 'Description:'); ?>

    <p><?php echo $ocCategoryDescription->description; ?></p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    <?php echo Form::label('meta_title', 'Meta Title:'); ?>

    <p><?php echo $ocCategoryDescription->meta_title; ?></p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    <?php echo Form::label('meta_description', 'Meta Description:'); ?>

    <p><?php echo $ocCategoryDescription->meta_description; ?></p>
</div>

<!-- Meta Keyword Field -->
<div class="form-group">
    <?php echo Form::label('meta_keyword', 'Meta Keyword:'); ?>

    <p><?php echo $ocCategoryDescription->meta_keyword; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocCategoryDescription->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocCategoryDescription->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocCategoryDescription->deleted_at; ?></p>
</div>

