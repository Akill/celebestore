<table class="table table-responsive" id="ocOrderCustomFields-table">
    <thead>
        <th>Order Id</th>
        <th>Custom Field Id</th>
        <th>Custom Field Value Id</th>
        <th>Name</th>
        <th>Value</th>
        <th>Type</th>
        <th>Location</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocOrderCustomFields; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocOrderCustomField): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocOrderCustomField->order_id; ?></td>
            <td><?php echo $ocOrderCustomField->custom_field_id; ?></td>
            <td><?php echo $ocOrderCustomField->custom_field_value_id; ?></td>
            <td><?php echo $ocOrderCustomField->name; ?></td>
            <td><?php echo $ocOrderCustomField->value; ?></td>
            <td><?php echo $ocOrderCustomField->type; ?></td>
            <td><?php echo $ocOrderCustomField->location; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocOrderCustomFields.destroy', $ocOrderCustomField->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocOrderCustomFields.show', [$ocOrderCustomField->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocOrderCustomFields.edit', [$ocOrderCustomField->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>