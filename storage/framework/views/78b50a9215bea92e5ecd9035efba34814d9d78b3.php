<table class="table table-responsive" id="ocProductRewards-table">
    <thead>
        <th>Product Id</th>
        <th>Customer Group Id</th>
        <th>Points</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductRewards; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductReward): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductReward->product_id; ?></td>
            <td><?php echo $ocProductReward->customer_group_id; ?></td>
            <td><?php echo $ocProductReward->points; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductRewards.destroy', $ocProductReward->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductRewards.show', [$ocProductReward->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductRewards.edit', [$ocProductReward->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>