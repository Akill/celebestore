<table class="table table-responsive" id="ocCategoryFilters-table">
    <thead>
        <th>Filter Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCategoryFilters; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCategoryFilter): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCategoryFilter->filter_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCategoryFilters.destroy', $ocCategoryFilter->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCategoryFilters.show', [$ocCategoryFilter->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCategoryFilters.edit', [$ocCategoryFilter->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>