<!-- Id Field -->
<div class="form-group">
    <?php echo Form::label('id', 'Id:'); ?>

    <p><?php echo $ocProductAttribute->id; ?></p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    <?php echo Form::label('product_id', 'Product Id:'); ?>

    <p><?php echo App\Models\oc_product_description::find($ocProductAttribute->product_id)->name; ?></p>
</div>

<!-- Attribute Id Field -->
<div class="form-group">
    <?php echo Form::label('attribute_id', 'Attribute Id:'); ?>

    <p><?php echo App\Models\oc_attribute_description::find($ocProductAttribute->attribute_id)->name; ?></p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    <?php echo Form::label('language_id', 'Language Id:'); ?>

    <p><?php echo App\Models\oc_language::find($ocProductAttribute->language_id)->name; ?></p>
</div>

<!-- Text Field -->
<div class="form-group">
    <?php echo Form::label('text', 'Text:'); ?>

    <p><?php echo $ocProductAttribute->text; ?></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    <?php echo Form::label('created_at', 'Created At:'); ?>

    <p><?php echo $ocProductAttribute->created_at; ?></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    <?php echo Form::label('updated_at', 'Updated At:'); ?>

    <p><?php echo $ocProductAttribute->updated_at; ?></p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    <?php echo Form::label('deleted_at', 'Deleted At:'); ?>

    <p><?php echo $ocProductAttribute->deleted_at; ?></p>
</div>

