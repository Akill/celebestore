<!-- Product Id Field -->
<div class="form-group col-sm-12">
    <?php echo Form::label('product_id', 'Product:'); ?>

    <?php  $items = App\Models\oc_product::has('detail')->get();  ?>
    <select class="form-control" id="product_id" name="product_id">
    <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <option value="<?php echo e($i->id); ?>"><?php echo e(App\Models\oc_product_description::find($i->id)->name); ?></option>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </select>
</div>
<div class="form-group col-sm-6">
<img src="http://placehold.it/250x250" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    <?php echo Form::label('image', 'Image:'); ?>

    <?php echo Form::file('image',null,array('id'=>'image','class'=>'form-control')); ?>

</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    <?php echo Form::label('sort_order', 'Sort Order:'); ?>

    <?php echo Form::number('sort_order', null, ['class' => 'form-control']); ?>

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('ocProductImages.index'); ?>" class="btn btn-default">Cancel</a>
</div>
