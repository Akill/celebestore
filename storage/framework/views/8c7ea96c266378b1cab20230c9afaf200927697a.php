<table class="table table-responsive" id="ocCategoryToStores-table">
    <thead>
        <th>Store Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCategoryToStores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCategoryToStore): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCategoryToStore->store_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCategoryToStores.destroy', $ocCategoryToStore->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCategoryToStores.show', [$ocCategoryToStore->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCategoryToStores.edit', [$ocCategoryToStore->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>