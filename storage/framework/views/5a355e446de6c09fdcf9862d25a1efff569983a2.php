<table class="table table-responsive" id="ocInformationDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Meta Title</th>
        <th>Meta Description</th>
        <th>Meta Keyword</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocInformationDescriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocInformationDescription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocInformationDescription->language_id; ?></td>
            <td><?php echo $ocInformationDescription->title; ?></td>
            <td><?php echo $ocInformationDescription->description; ?></td>
            <td><?php echo $ocInformationDescription->meta_title; ?></td>
            <td><?php echo $ocInformationDescription->meta_description; ?></td>
            <td><?php echo $ocInformationDescription->meta_keyword; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocInformationDescriptions.destroy', $ocInformationDescription->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocInformationDescriptions.show', [$ocInformationDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocInformationDescriptions.edit', [$ocInformationDescription->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>