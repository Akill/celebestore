<table class="table table-responsive" id="ocAffiliateTransactions-table">
    <thead>
        <th>Affiliate Id</th>
        <th>Order Id</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocAffiliateTransactions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocAffiliateTransaction): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocAffiliateTransaction->affiliate_id; ?></td>
            <td><?php echo $ocAffiliateTransaction->order_id; ?></td>
            <td><?php echo $ocAffiliateTransaction->description; ?></td>
            <td><?php echo $ocAffiliateTransaction->amount; ?></td>
            <td><?php echo $ocAffiliateTransaction->date_added; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocAffiliateTransactions.destroy', $ocAffiliateTransaction->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocAffiliateTransactions.show', [$ocAffiliateTransaction->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocAffiliateTransactions.edit', [$ocAffiliateTransaction->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>