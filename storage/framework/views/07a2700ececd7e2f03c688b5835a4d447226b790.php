<table class="table table-responsive" id="ocProductToCategories-table">
    <thead>
        <th>Product Id</th>
        <th>Category Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocProductToCategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocProductToCategory): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocProductToCategory->product_id; ?></td>
            <td><?php echo $ocProductToCategory->category_id; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocProductToCategories.destroy', $ocProductToCategory->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocProductToCategories.show', [$ocProductToCategory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocProductToCategories.edit', [$ocProductToCategory->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>