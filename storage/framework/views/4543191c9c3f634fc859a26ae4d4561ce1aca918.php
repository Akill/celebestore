<table class="table table-responsive" id="ocCountryHpwds-table">
    <thead>
        <th>Name</th>
        <th>Iso Code 2</th>
        <th>Iso Code 3</th>
        <th>Address Format</th>
        <th>Postcode Required</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocCountryHpwds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocCountryHpwd): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocCountryHpwd->name; ?></td>
            <td><?php echo $ocCountryHpwd->iso_code_2; ?></td>
            <td><?php echo $ocCountryHpwd->iso_code_3; ?></td>
            <td><?php echo $ocCountryHpwd->address_format; ?></td>
            <td><?php echo $ocCountryHpwd->postcode_required; ?></td>
            <td><?php echo $ocCountryHpwd->status; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocCountryHpwds.destroy', $ocCountryHpwd->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocCountryHpwds.show', [$ocCountryHpwd->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocCountryHpwds.edit', [$ocCountryHpwd->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>