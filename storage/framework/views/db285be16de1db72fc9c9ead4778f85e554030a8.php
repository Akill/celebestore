<table class="table table-responsive" id="ocOrders-table">
    <thead>
        <th>Invoice No</th>
        <th>Invoice Prefix</th>
        <th>Store Id</th>
        <th>Store Name</th>
        <th>Store Url</th>
        <th>Customer Id</th>
        <th>Customer Group Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Custom Field</th>
        <th>Payment Firstname</th>
        <th>Payment Lastname</th>
        <th>Payment Company</th>
        <th>Payment Address 1</th>
        <th>Payment Address 2</th>
        <th>Payment City</th>
        <th>Payment Postcode</th>
        <th>Payment Country</th>
        <th>Payment Country Id</th>
        <th>Payment Zone</th>
        <th>Payment Zone Id</th>
        <th>Payment Sub District Id</th>
        <th>Payment Address Format</th>
        <th>Payment Custom Field</th>
        <th>Payment Method</th>
        <th>Payment Code</th>
        <th>Shipping Firstname</th>
        <th>Shipping Lastname</th>
        <th>Shipping Company</th>
        <th>Shipping Address 1</th>
        <th>Shipping Address 2</th>
        <th>Shipping City</th>
        <th>Shipping Postcode</th>
        <th>Shipping Country</th>
        <th>Shipping Country Id</th>
        <th>Shipping Zone</th>
        <th>Shipping Zone Id</th>
        <th>Shipping Sub District Id</th>
        <th>Shipping Address Format</th>
        <th>Shipping Custom Field</th>
        <th>Shipping Method</th>
        <th>Shipping Code</th>
        <th>Comment</th>
        <th>Total</th>
        <th>Order Status Id</th>
        <th>Affiliate Id</th>
        <th>Commission</th>
        <th>Marketing Id</th>
        <th>Tracking</th>
        <th>Language Id</th>
        <th>Currency Id</th>
        <th>Currency Code</th>
        <th>Currency Value</th>
        <th>Ip</th>
        <th>Forwarded Ip</th>
        <th>User Agent</th>
        <th>Accept Language</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocOrders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocOrder): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocOrder->invoice_no; ?></td>
            <td><?php echo $ocOrder->invoice_prefix; ?></td>
            <td><?php echo $ocOrder->store_id; ?></td>
            <td><?php echo $ocOrder->store_name; ?></td>
            <td><?php echo $ocOrder->store_url; ?></td>
            <td><?php echo $ocOrder->customer_id; ?></td>
            <td><?php echo $ocOrder->customer_group_id; ?></td>
            <td><?php echo $ocOrder->firstname; ?></td>
            <td><?php echo $ocOrder->lastname; ?></td>
            <td><?php echo $ocOrder->email; ?></td>
            <td><?php echo $ocOrder->telephone; ?></td>
            <td><?php echo $ocOrder->fax; ?></td>
            <td><?php echo $ocOrder->custom_field; ?></td>
            <td><?php echo $ocOrder->payment_firstname; ?></td>
            <td><?php echo $ocOrder->payment_lastname; ?></td>
            <td><?php echo $ocOrder->payment_company; ?></td>
            <td><?php echo $ocOrder->payment_address_1; ?></td>
            <td><?php echo $ocOrder->payment_address_2; ?></td>
            <td><?php echo $ocOrder->payment_city; ?></td>
            <td><?php echo $ocOrder->payment_postcode; ?></td>
            <td><?php echo $ocOrder->payment_country; ?></td>
            <td><?php echo $ocOrder->payment_country_id; ?></td>
            <td><?php echo $ocOrder->payment_zone; ?></td>
            <td><?php echo $ocOrder->payment_zone_id; ?></td>
            <td><?php echo $ocOrder->payment_sub_district_id; ?></td>
            <td><?php echo $ocOrder->payment_address_format; ?></td>
            <td><?php echo $ocOrder->payment_custom_field; ?></td>
            <td><?php echo $ocOrder->payment_method; ?></td>
            <td><?php echo $ocOrder->payment_code; ?></td>
            <td><?php echo $ocOrder->shipping_firstname; ?></td>
            <td><?php echo $ocOrder->shipping_lastname; ?></td>
            <td><?php echo $ocOrder->shipping_company; ?></td>
            <td><?php echo $ocOrder->shipping_address_1; ?></td>
            <td><?php echo $ocOrder->shipping_address_2; ?></td>
            <td><?php echo $ocOrder->shipping_city; ?></td>
            <td><?php echo $ocOrder->shipping_postcode; ?></td>
            <td><?php echo $ocOrder->shipping_country; ?></td>
            <td><?php echo $ocOrder->shipping_country_id; ?></td>
            <td><?php echo $ocOrder->shipping_zone; ?></td>
            <td><?php echo $ocOrder->shipping_zone_id; ?></td>
            <td><?php echo $ocOrder->shipping_sub_district_id; ?></td>
            <td><?php echo $ocOrder->shipping_address_format; ?></td>
            <td><?php echo $ocOrder->shipping_custom_field; ?></td>
            <td><?php echo $ocOrder->shipping_method; ?></td>
            <td><?php echo $ocOrder->shipping_code; ?></td>
            <td><?php echo $ocOrder->comment; ?></td>
            <td><?php echo $ocOrder->total; ?></td>
            <td><?php echo $ocOrder->order_status_id; ?></td>
            <td><?php echo $ocOrder->affiliate_id; ?></td>
            <td><?php echo $ocOrder->commission; ?></td>
            <td><?php echo $ocOrder->marketing_id; ?></td>
            <td><?php echo $ocOrder->tracking; ?></td>
            <td><?php echo $ocOrder->language_id; ?></td>
            <td><?php echo $ocOrder->currency_id; ?></td>
            <td><?php echo $ocOrder->currency_code; ?></td>
            <td><?php echo $ocOrder->currency_value; ?></td>
            <td><?php echo $ocOrder->ip; ?></td>
            <td><?php echo $ocOrder->forwarded_ip; ?></td>
            <td><?php echo $ocOrder->user_agent; ?></td>
            <td><?php echo $ocOrder->accept_language; ?></td>
            <td><?php echo $ocOrder->date_added; ?></td>
            <td><?php echo $ocOrder->date_modified; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocOrders.destroy', $ocOrder->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocOrders.show', [$ocOrder->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocOrders.edit', [$ocOrder->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>