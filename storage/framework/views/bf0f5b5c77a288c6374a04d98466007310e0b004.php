<table class="table table-responsive" id="ocZones-table">
    <thead>
        <th>Country Id</th>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocZones; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocZone): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocZone->country_id; ?></td>
            <td><?php echo $ocZone->name; ?></td>
            <td><?php echo $ocZone->code; ?></td>
            <td><?php echo $ocZone->status; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocZones.destroy', $ocZone->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocZones.show', [$ocZone->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocZones.edit', [$ocZone->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>