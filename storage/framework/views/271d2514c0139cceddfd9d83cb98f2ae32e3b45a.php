<table class="table table-responsive" id="ocApis-table">
    <thead>
        <th>Username</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Password</th>
        <th>Status</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    <?php $__currentLoopData = $ocApis; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ocApi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo $ocApi->username; ?></td>
            <td><?php echo $ocApi->firstname; ?></td>
            <td><?php echo $ocApi->lastname; ?></td>
            <td><?php echo $ocApi->password; ?></td>
            <td><?php echo $ocApi->status; ?></td>
            <td><?php echo $ocApi->date_added; ?></td>
            <td><?php echo $ocApi->date_modified; ?></td>
            <td>
                <?php echo Form::open(['route' => ['ocApis.destroy', $ocApi->id], 'method' => 'delete']); ?>

                <div class='btn-group'>
                    <a href="<?php echo route('ocApis.show', [$ocApi->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="<?php echo route('ocApis.edit', [$ocApi->id]); ?>" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    <?php echo Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]); ?>

                </div>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>