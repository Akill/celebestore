<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * Class oc_order_recurring
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property string reference
 * @property integer product_id
 * @property string product_name
 * @property integer product_quantity
 * @property integer recurring_id
 * @property string recurring_name
 * @property string recurring_description
 * @property string recurring_frequency
 * @property smallInteger recurring_cycle
 * @property smallInteger recurring_duration
 * @property decimal recurring_price
 * @property boolean trial
 * @property string trial_frequency
 * @property smallInteger trial_cycle
 * @property smallInteger trial_duration
 * @property decimal trial_price
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $order_id
 * @property string $reference
 * @property int $product_id
 * @property string $product_name
 * @property int $product_quantity
 * @property int $recurring_id
 * @property string $recurring_name
 * @property string $recurring_description
 * @property string $recurring_frequency
 * @property int $recurring_cycle
 * @property int $recurring_duration
 * @property float $recurring_price
 * @property bool $trial
 * @property string $trial_frequency
 * @property int $trial_cycle
 * @property int $trial_duration
 * @property float $trial_price
 * @property bool $status
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereProductQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereRecurringPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereTrial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereTrialCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereTrialDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereTrialFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereTrialPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring withoutTrashed()
 */
	class oc_order_recurring extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_length_class_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer language_id
 * @property string title
 * @property string unit
 * @property int $id
 * @property int $language_id
 * @property string $title
 * @property string $unit
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class_description withoutTrashed()
 */
	class oc_length_class_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_recurring_transaction
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_recurring_id
 * @property string reference
 * @property string type
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $order_recurring_id
 * @property string $reference
 * @property string $type
 * @property float $amount
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring_transaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereOrderRecurringId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereReference($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_recurring_transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring_transaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_recurring_transaction withoutTrashed()
 */
	class oc_order_recurring_transaction extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_attribute_group_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group_description withoutTrashed()
 */
	class oc_attribute_group_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_to_store
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer store_id
 * @property int $product_id
 * @property int $store_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_store whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_store whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_store withoutTrashed()
 */
	class oc_product_to_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_attribute_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_description withoutTrashed()
 */
	class oc_attribute_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_event
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string code
 * @property string trigger
 * @property string action
 * @property int $id
 * @property string $code
 * @property string $trigger
 * @property string $action
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_event onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereTrigger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_event withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_event withoutTrashed()
 */
	class oc_event extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category_path
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer path_id
 * @property integer level
 * @property int $id
 * @property int $path_id
 * @property int $level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_path onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path wherePathId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_path whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_path withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_path withoutTrashed()
 */
	class oc_category_path extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_online
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property string url
 * @property string referer
 * @property string|\Carbon\Carbon date_added
 * @property string $ip
 * @property int $customer_id
 * @property string $url
 * @property string $referer
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_online onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereReferer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_online whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_online withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_online withoutTrashed()
 */
	class oc_customer_online extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_recurring
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property decimal price
 * @property string frequency
 * @property integer duration
 * @property integer cycle
 * @property boolean trial_status
 * @property decimal trial_price
 * @property string trial_frequency
 * @property integer trial_duration
 * @property integer trial_cycle
 * @property boolean status
 * @property integer sort_order
 * @property int $id
 * @property float $price
 * @property string $frequency
 * @property int $duration
 * @property int $cycle
 * @property bool $trial_status
 * @property float $trial_price
 * @property string $trial_frequency
 * @property int $trial_duration
 * @property int $trial_cycle
 * @property bool $status
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereTrialCycle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereTrialDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereTrialFrequency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereTrialPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereTrialStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring withoutTrashed()
 */
	class oc_recurring extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_confirm
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string email
 * @property string order_id
 * @property date payment_date
 * @property integer total_amount
 * @property string destination_bank
 * @property string resi
 * @property string payment_method
 * @property string sender_name
 * @property string bank_origin
 * @property string code
 * @property string no_receipt
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $email
 * @property string $order_id
 * @property \Carbon\Carbon $payment_date
 * @property int $total_amount
 * @property string $destination_bank
 * @property string $resi
 * @property string $payment_method
 * @property string $sender_name
 * @property string $bank_origin
 * @property string $code
 * @property string $no_receipt
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereBankOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereDestinationBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereNoReceipt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm wherePaymentDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereResi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereSenderName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereTotalAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm withoutTrashed()
 */
	class oc_confirm extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category_to_store
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer store_id
 * @property int $id
 * @property int $store_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_store whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_store withoutTrashed()
 */
	class oc_category_to_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_filter_group
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer sort_order
 * @property int $id
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group withoutTrashed()
 */
	class oc_filter_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_to_download
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer download_id
 * @property int $product_id
 * @property int $download_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_download onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_download whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_download whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_download whereDownloadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_download whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_download whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_download withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_download withoutTrashed()
 */
	class oc_product_to_download extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_extension
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string type
 * @property string code
 * @property int $id
 * @property string $type
 * @property string $code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_extension onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_extension whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_extension withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_extension withoutTrashed()
 */
	class oc_extension extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_option
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property integer order_product_id
 * @property integer product_option_id
 * @property integer product_option_value_id
 * @property string name
 * @property string value
 * @property string type
 * @property int $id
 * @property int $order_id
 * @property int $order_product_id
 * @property int $product_option_id
 * @property int $product_option_value_id
 * @property string $name
 * @property string $value
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_option onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereOrderProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereProductOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereProductOptionValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_option whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_option withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_option withoutTrashed()
 */
	class oc_order_option extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_option
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property integer option_id
 * @property string value
 * @property boolean required
 * @property int $id
 * @property int $product_id
 * @property int $option_id
 * @property string $value
 * @property bool $required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option withoutTrashed()
 */
	class oc_product_option extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_custom_field_customer_group
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_group_id
 * @property boolean required
 * @property int $id
 * @property int $customer_group_id
 * @property bool $required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_customer_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_customer_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_customer_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_customer_group withoutTrashed()
 */
	class oc_custom_field_customer_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_country
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string name
 * @property string code
 * @property string status
 * @property string address_format
 * @property string iso_code_2
 * @property string iso_code_3
 * @property string postcode_required
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $status
 * @property string $address_format
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property string $postcode_required
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereAddressFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereIsoCode2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereIsoCode3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country wherePostcodeRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country withoutTrashed()
 */
	class oc_country extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer language_id
 * @property string name
 * @property string description
 * @property string tag
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property string $tag
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereMetaKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereTag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_description withoutTrashed()
 */
	class oc_product_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_affiliate_activity
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer affiliate_id
 * @property string key
 * @property string data
 * @property string ip
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $affiliate_id
 * @property string $key
 * @property string $data
 * @property string $ip
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_activity onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereAffiliateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_activity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_activity withoutTrashed()
 */
	class oc_affiliate_activity extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer language_id
 * @property string name
 * @property string description
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereMetaKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_description withoutTrashed()
 */
	class oc_category_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_custom_field
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string type
 * @property string value
 * @property string location
 * @property boolean status
 * @property integer sort_order
 * @property int $id
 * @property string $type
 * @property string $value
 * @property string $location
 * @property bool $status
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field withoutTrashed()
 */
	class oc_custom_field extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_affiliate_login
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string email
 * @property string ip
 * @property integer total
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $email
 * @property string $ip
 * @property int $total
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_login onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_login whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_login withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_login withoutTrashed()
 */
	class oc_affiliate_login extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_shipping_filtered
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property string shipping_code
 * @property int $filtered_id
 * @property int $product_id
 * @property string $shipping_code
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_shipping_filtered onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereFilteredId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereShippingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_shipping_filtered whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_shipping_filtered withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_shipping_filtered withoutTrashed()
 */
	class oc_product_shipping_filtered extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_banner
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string name
 * @property boolean status
 * @property int $id
 * @property string $name
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner withoutTrashed()
 */
	class oc_banner extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_language
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string code
 * @property string locale
 * @property string image
 * @property string directory
 * @property integer sort_order
 * @property boolean status
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $locale
 * @property string $image
 * @property string $directory
 * @property int $sort_order
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_language onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereDirectory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_language withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_language withoutTrashed()
 */
	class oc_language extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_coupon_category
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer category_id
 * @property int $id
 * @property int $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_category withoutTrashed()
 */
	class oc_coupon_category extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_tax_rule
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer tax_class_id
 * @property integer tax_rate_id
 * @property string based
 * @property integer priority
 * @property int $id
 * @property int $tax_class_id
 * @property int $tax_rate_id
 * @property string $based
 * @property int $priority
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rule onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereBased($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereTaxClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereTaxRateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rule withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rule withoutTrashed()
 */
	class oc_tax_rule extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_return_action
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_action onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_action whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_action withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_action withoutTrashed()
 */
	class oc_return_action extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_manufacturer_to_store
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer store_id
 * @property int $id
 * @property int $store_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer_to_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer_to_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer_to_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer_to_store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer_to_store whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer_to_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer_to_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer_to_store withoutTrashed()
 */
	class oc_manufacturer_to_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property string model
 * @property string sku
 * @property string upc
 * @property string ean
 * @property string jan
 * @property string isbn
 * @property string mpn
 * @property string location
 * @property integer quantity
 * @property integer stock_status_id
 * @property string image
 * @property integer manufacturer_id
 * @property boolean shipping
 * @property decimal price
 * @property integer points
 * @property integer tax_class_id
 * @property date date_available
 * @property decimal weight
 * @property integer weight_class_id
 * @property decimal length
 * @property decimal width
 * @property decimal height
 * @property integer length_class_id
 * @property boolean subtract
 * @property integer minimum
 * @property integer sort_order
 * @property boolean status
 * @property integer viewed
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $model
 * @property string $sku
 * @property string $upc
 * @property string $ean
 * @property string $jan
 * @property string $isbn
 * @property string $mpn
 * @property string $location
 * @property int $quantity
 * @property int $stock_status_id
 * @property string $image
 * @property int $manufacturer_id
 * @property bool $shipping
 * @property float $price
 * @property int $points
 * @property int $tax_class_id
 * @property \Carbon\Carbon $date_available
 * @property float $weight
 * @property int $weight_class_id
 * @property float $length
 * @property float $width
 * @property float $height
 * @property int $length_class_id
 * @property bool $subtract
 * @property int $minimum
 * @property int $sort_order
 * @property bool $status
 * @property int $viewed
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereDateAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereEan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereIsbn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereJan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereLengthClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereManufacturerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereMinimum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereMpn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereShipping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereStockStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereSubtract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereTaxClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereUpc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereViewed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereWeightClassId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product whereWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product withoutTrashed()
 */
	class oc_product extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_attribute
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer attribute_group_id
 * @property integer sort_order
 * @property int $id
 * @property int $attribute_group_id
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereAttributeGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute withoutTrashed()
 */
	class oc_attribute extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_zone_to_geo_zone
 *
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 * @property integer country_id
 * @property integer zone_id
 * @property integer geo_zone_id
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property int $country_id
 * @property int $zone_id
 * @property int $geo_zone_id
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone_to_geo_zone onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereGeoZoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone_to_geo_zone whereZoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone_to_geo_zone withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone_to_geo_zone withoutTrashed()
 */
	class oc_zone_to_geo_zone extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_transaction
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property integer order_id
 * @property string description
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_id
 * @property int $order_id
 * @property string $description
 * @property float $amount
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_transaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_transaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_transaction withoutTrashed()
 */
	class oc_customer_transaction extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_product
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property integer product_id
 * @property string name
 * @property string model
 * @property integer quantity
 * @property decimal price
 * @property decimal total
 * @property decimal tax
 * @property integer reward
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $name
 * @property string $model
 * @property int $quantity
 * @property float $price
 * @property float $total
 * @property float $tax
 * @property int $reward
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_product onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_product withoutTrashed()
 */
	class oc_order_product extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_voucher_theme
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string image
 * @property int $id
 * @property string $image
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme withoutTrashed()
 */
	class oc_voucher_theme extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_information_to_store
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer store_id
 * @property int $id
 * @property int $store_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_store whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_store withoutTrashed()
 */
	class oc_information_to_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_download
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string filename
 * @property string mask
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $filename
 * @property string $mask
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereMask($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download withoutTrashed()
 */
	class oc_download extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_banner_image
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer banner_id
 * @property string link
 * @property string image
 * @property integer sort_order
 * @property int $id
 * @property int $banner_id
 * @property string $link
 * @property string $image
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image withoutTrashed()
 */
	class oc_banner_image extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_filter_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer language_id
 * @property integer filter_group_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property int $filter_group_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereFilterGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_description withoutTrashed()
 */
	class oc_filter_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_recurring
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer recurring_id
 * @property integer customer_group_id
 * @property int $product_id
 * @property int $recurring_id
 * @property int $customer_group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_recurring onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereRecurringId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_recurring whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_recurring withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_recurring withoutTrashed()
 */
	class oc_product_recurring extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_ip
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property string ip
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_id
 * @property string $ip
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ip onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ip whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ip withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ip withoutTrashed()
 */
	class oc_customer_ip extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category_to_layout
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer store_id
 * @property integer layout_id
 * @property int $id
 * @property int $store_id
 * @property int $layout_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_layout onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereLayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_to_layout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_layout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_to_layout withoutTrashed()
 */
	class oc_category_to_layout extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_return_history
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer return_id
 * @property integer return_status_id
 * @property boolean notify
 * @property string comment
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $return_id
 * @property int $return_status_id
 * @property bool $notify
 * @property string $comment
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_history onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereReturnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereReturnStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_history whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_history withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_history withoutTrashed()
 */
	class oc_return_history extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_activity
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property string key
 * @property string data
 * @property string ip
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_id
 * @property string $key
 * @property string $data
 * @property string $ip
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_activity onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_activity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_activity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_activity withoutTrashed()
 */
	class oc_customer_activity extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_image
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property string image
 * @property integer sort_order
 * @property int $id
 * @property int $product_id
 * @property string $image
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_image onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_image withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_image withoutTrashed()
 */
	class oc_product_image extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_stock_status
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_stock_status onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_stock_status whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_stock_status withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_stock_status withoutTrashed()
 */
	class oc_stock_status extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_marketing
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string description
 * @property string code
 * @property integer clicks
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property int $clicks
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_marketing onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereClicks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_marketing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_marketing withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_marketing withoutTrashed()
 */
	class oc_marketing extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_history
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property string comment
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_id
 * @property string $comment
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_history onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_history whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_history withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_history withoutTrashed()
 */
	class oc_customer_history extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_discount
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property integer customer_group_id
 * @property integer quantity
 * @property integer priority
 * @property decimal price
 * @property date date_start
 * @property date date_end
 * @property int $id
 * @property int $product_id
 * @property int $customer_group_id
 * @property int $quantity
 * @property int $priority
 * @property float $price
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_discount onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_discount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_discount withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_discount withoutTrashed()
 */
	class oc_product_discount extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_option_value
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer option_id
 * @property string image
 * @property integer sort_order
 * @property int $id
 * @property int $option_id
 * @property string $image
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value withoutTrashed()
 */
	class oc_option_value extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_location
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string address
 * @property string telephone
 * @property string fax
 * @property string geocode
 * @property string image
 * @property string open
 * @property string comment
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $telephone
 * @property string $fax
 * @property string $geocode
 * @property string $image
 * @property string $open
 * @property string $comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_location onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereGeocode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereOpen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_location whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_location withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_location withoutTrashed()
 */
	class oc_location extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_length_class
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property decimal value
 * @property int $id
 * @property float $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_length_class whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_length_class withoutTrashed()
 */
	class oc_length_class extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_return_status
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_status onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_status whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_status withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_status withoutTrashed()
 */
	class oc_return_status extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_total
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property string code
 * @property string title
 * @property decimal value
 * @property integer sort_order
 * @property int $id
 * @property int $order_id
 * @property string $code
 * @property string $title
 * @property float $value
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_total onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_total whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_total withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_total withoutTrashed()
 */
	class oc_order_total extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_tax_rate
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer geo_zone_id
 * @property string name
 * @property decimal rate
 * @property string type
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property int $geo_zone_id
 * @property string $name
 * @property float $rate
 * @property string $type
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereGeoZoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate withoutTrashed()
 */
	class oc_tax_rate extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_special
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property integer customer_group_id
 * @property integer priority
 * @property decimal price
 * @property date date_start
 * @property date date_end
 * @property int $id
 * @property int $product_id
 * @property int $customer_group_id
 * @property int $priority
 * @property float $price
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_special onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special wherePriority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_special whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_special withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_special withoutTrashed()
 */
	class oc_product_special extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_information_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer language_id
 * @property string title
 * @property string description
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 * @property int $id
 * @property int $language_id
 * @property string $title
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereMetaKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereMetaTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_description withoutTrashed()
 */
	class oc_information_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_module
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string code
 * @property string setting
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $setting
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_module onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereSetting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_module withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_module withoutTrashed()
 */
	class oc_module extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_coupon_product
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer coupon_id
 * @property integer product_id
 * @property int $id
 * @property int $coupon_id
 * @property int $product_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_product onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_product withoutTrashed()
 */
	class oc_coupon_product extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_manufacturer
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string image
 * @property integer sort_order
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_manufacturer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_manufacturer withoutTrashed()
 */
	class oc_manufacturer extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_reward
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_id
 * @property integer order_id
 * @property string description
 * @property integer points
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_id
 * @property int $order_id
 * @property string $description
 * @property int $points
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_reward onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_reward whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_reward withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_reward withoutTrashed()
 */
	class oc_customer_reward extends \Eloquent {}
}

namespace App\Models{
/**
 * Class user
 *
 * @package App\Models
 * @version August 28, 2017, 10:46 am UTC
 * @property integer user_group_id
 * @property string name
 * @property string email
 * @property string password
 * @property string username
 * @property string type_of_identity
 * @property string identity_number
 * @property string gender
 * @property string phone
 * @property string job
 * @property string address
 * @property string districts
 * @property string city
 * @property string province
 * @property string zip_kode
 * @property string picture
 * @property string remember_token
 * @property int $id
 * @property int $user_group_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $username
 * @property string $type_of_identity
 * @property string $identity_number
 * @property string $gender
 * @property string $phone
 * @property string $job
 * @property string $address
 * @property string $districts
 * @property string $city
 * @property string $province
 * @property string $zip_kode
 * @property string $picture
 * @property string $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\oc_user_group[] $userGroup
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\user onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereDistricts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereIdentityNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereTypeOfIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereUserGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\user whereZipKode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\user withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\user withoutTrashed()
 */
	class user extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_affiliate_transaction
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer affiliate_id
 * @property integer order_id
 * @property string description
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $affiliate_id
 * @property int $order_id
 * @property string $description
 * @property float $amount
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_transaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereAffiliateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate_transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_transaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate_transaction withoutTrashed()
 */
	class oc_affiliate_transaction extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_filter
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer filter_id
 * @property int $product_id
 * @property int $filter_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_filter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_filter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_filter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_filter whereFilterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_filter whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_filter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_filter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_filter withoutTrashed()
 */
	class oc_product_filter extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_to_category
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer category_id
 * @property int $product_id
 * @property int $category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_category whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_category whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_category withoutTrashed()
 */
	class oc_product_to_category extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_custom_field_value
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer custom_field_id
 * @property integer sort_order
 * @property int $id
 * @property int $custom_field_id
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereCustomFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value withoutTrashed()
 */
	class oc_custom_field_value extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_information_to_layout
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer store_id
 * @property integer layout_id
 * @property int $id
 * @property int $store_id
 * @property int $layout_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_layout onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereLayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information_to_layout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_layout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information_to_layout withoutTrashed()
 */
	class oc_information_to_layout extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_setting
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer store_id
 * @property string code
 * @property string key
 * @property string value
 * @property boolean serialized
 * @property int $id
 * @property int $store_id
 * @property string $code
 * @property string $key
 * @property string $value
 * @property bool $serialized
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_setting onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereSerialized($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_setting whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_setting withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_setting withoutTrashed()
 */
	class oc_setting extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_status
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_status onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_status whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_status withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_status withoutTrashed()
 */
	class oc_order_status extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_login
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string email
 * @property string ip
 * @property integer total
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $email
 * @property string $ip
 * @property int $total
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_login onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_login whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_login withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_login withoutTrashed()
 */
	class oc_customer_login extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_coupon_history
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer coupon_id
 * @property integer order_id
 * @property integer customer_id
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $coupon_id
 * @property int $order_id
 * @property int $customer_id
 * @property float $amount
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_history onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon_history whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_history withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon_history withoutTrashed()
 */
	class oc_coupon_history extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_coupon
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string name
 * @property string code
 * @property string type
 * @property decimal discount
 * @property boolean logged
 * @property boolean shipping
 * @property decimal total
 * @property date date_start
 * @property date date_end
 * @property integer uses_total
 * @property string uses_customer
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $type
 * @property float $discount
 * @property bool $logged
 * @property bool $shipping
 * @property float $total
 * @property \Carbon\Carbon $date_start
 * @property \Carbon\Carbon $date_end
 * @property int $uses_total
 * @property string $uses_customer
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $date_added
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereLogged($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereShipping($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereUsesCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_coupon whereUsesTotal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_coupon withoutTrashed()
 */
	class oc_coupon extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_country_hpwd
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string name
 * @property string iso_code_2
 * @property string iso_code_3
 * @property string address_format
 * @property boolean postcode_required
 * @property boolean status
 * @property int $id
 * @property string $name
 * @property string $iso_code_2
 * @property string $iso_code_3
 * @property string $address_format
 * @property bool $postcode_required
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country_hpwd onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereAddressFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereIsoCode2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereIsoCode3($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd wherePostcodeRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_country_hpwd whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country_hpwd withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_country_hpwd withoutTrashed()
 */
	class oc_country_hpwd extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_group
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer approval
 * @property integer sort_order
 * @property int $id
 * @property int $approval
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereApproval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group withoutTrashed()
 */
	class oc_customer_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_voucher
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer order_id
 * @property string code
 * @property string from_name
 * @property string from_email
 * @property string to_name
 * @property string to_email
 * @property integer voucher_theme_id
 * @property string message
 * @property decimal amount
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $order_id
 * @property string $code
 * @property string $from_name
 * @property string $from_email
 * @property string $to_name
 * @property string $to_email
 * @property int $voucher_theme_id
 * @property string $message
 * @property float $amount
 * @property bool $status
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereFromEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereToEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereToName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher whereVoucherThemeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher withoutTrashed()
 */
	class oc_voucher extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_custom_field
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property integer custom_field_id
 * @property integer custom_field_value_id
 * @property string name
 * @property string value
 * @property string type
 * @property string location
 * @property int $id
 * @property int $order_id
 * @property int $custom_field_id
 * @property int $custom_field_value_id
 * @property string $name
 * @property string $value
 * @property string $type
 * @property string $location
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_custom_field onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereCustomFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereCustomFieldValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_custom_field whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_custom_field withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_custom_field withoutTrashed()
 */
	class oc_order_custom_field extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_layout_route
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer layout_id
 * @property integer store_id
 * @property string route
 * @property int $id
 * @property int $layout_id
 * @property int $store_id
 * @property string $route
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_route onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereLayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_route whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_route withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_route withoutTrashed()
 */
	class oc_layout_route extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_option_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_description withoutTrashed()
 */
	class oc_option_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_api
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string username
 * @property string firstname
 * @property string lastname
 * @property string password
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $username
 * @property string $firstname
 * @property string $lastname
 * @property string $password
 * @property bool $status
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_api onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_api whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_api withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_api withoutTrashed()
 */
	class oc_api extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer invoice_no
 * @property string invoice_prefix
 * @property integer store_id
 * @property string store_name
 * @property string store_url
 * @property integer customer_id
 * @property integer customer_group_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string custom_field
 * @property string payment_firstname
 * @property string payment_lastname
 * @property string payment_company
 * @property string payment_address_1
 * @property string payment_address_2
 * @property string payment_city
 * @property string payment_postcode
 * @property string payment_country
 * @property integer payment_country_id
 * @property string payment_zone
 * @property integer payment_zone_id
 * @property integer payment_sub_district_id
 * @property string payment_address_format
 * @property string payment_custom_field
 * @property string payment_method
 * @property string payment_code
 * @property string shipping_firstname
 * @property string shipping_lastname
 * @property string shipping_company
 * @property string shipping_address_1
 * @property string shipping_address_2
 * @property string shipping_city
 * @property string shipping_postcode
 * @property string shipping_country
 * @property integer shipping_country_id
 * @property string shipping_zone
 * @property integer shipping_zone_id
 * @property integer shipping_sub_district_id
 * @property string shipping_address_format
 * @property string shipping_custom_field
 * @property string shipping_method
 * @property string shipping_code
 * @property string comment
 * @property decimal total
 * @property integer order_status_id
 * @property integer affiliate_id
 * @property decimal commission
 * @property integer marketing_id
 * @property string tracking
 * @property integer language_id
 * @property integer currency_id
 * @property string currency_code
 * @property decimal currency_value
 * @property string ip
 * @property string forwarded_ip
 * @property string user_agent
 * @property string accept_language
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property int $invoice_no
 * @property string $invoice_prefix
 * @property int $store_id
 * @property string $store_name
 * @property string $store_url
 * @property int $customer_id
 * @property int $customer_group_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $telephone
 * @property string $fax
 * @property string $custom_field
 * @property string $payment_firstname
 * @property string $payment_lastname
 * @property string $payment_company
 * @property string $payment_address_1
 * @property string $payment_address_2
 * @property string $payment_city
 * @property string $payment_postcode
 * @property string $payment_country
 * @property int $payment_country_id
 * @property string $payment_zone
 * @property int $payment_zone_id
 * @property int $payment_sub_district_id
 * @property string $payment_address_format
 * @property string $payment_custom_field
 * @property string $payment_method
 * @property string $payment_code
 * @property string $shipping_firstname
 * @property string $shipping_lastname
 * @property string $shipping_company
 * @property string $shipping_address_1
 * @property string $shipping_address_2
 * @property string $shipping_city
 * @property string $shipping_postcode
 * @property string $shipping_country
 * @property int $shipping_country_id
 * @property string $shipping_zone
 * @property int $shipping_zone_id
 * @property int $shipping_sub_district_id
 * @property string $shipping_address_format
 * @property string $shipping_custom_field
 * @property string $shipping_method
 * @property string $shipping_code
 * @property string $comment
 * @property float $total
 * @property int $order_status_id
 * @property int $affiliate_id
 * @property float $commission
 * @property int $marketing_id
 * @property string $tracking
 * @property int $language_id
 * @property int $currency_id
 * @property string $currency_code
 * @property float $currency_value
 * @property string $ip
 * @property string $forwarded_ip
 * @property string $user_agent
 * @property string $accept_language
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereAcceptLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereAffiliateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCurrencyCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCurrencyValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCustomField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereForwardedIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereInvoiceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereInvoicePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereMarketingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentAddressFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentCustomField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentPostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentSubDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentZone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order wherePaymentZoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingAddressFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingCustomField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingPostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingSubDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingZone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereShippingZoneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereStoreName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereStoreUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereTracking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order whereUserAgent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order withoutTrashed()
 */
	class oc_order extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_user_group
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string name
 * @property string permission
 * @property int $id
 * @property string $name
 * @property string $permission
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group wherePermission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user_group withoutTrashed()
 */
	class oc_user_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_store
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string name
 * @property string url
 * @property string ssl
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $ssl
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereSsl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_store whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_store withoutTrashed()
 */
	class oc_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_custom_field_value_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer language_id
 * @property integer custom_field_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property int $custom_field_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereCustomFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_value_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_value_description withoutTrashed()
 */
	class oc_custom_field_value_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_url_alias
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string query
 * @property string keyword
 * @property int $id
 * @property string $query
 * @property string $keyword
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_url_alias onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereQuery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_url_alias whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_url_alias withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_url_alias withoutTrashed()
 */
	class oc_url_alias extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_related
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer related_id
 * @property int $product_id
 * @property int $related_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_related onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_related whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_related whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_related whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_related whereRelatedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_related whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_related withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_related withoutTrashed()
 */
	class oc_product_related extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_attribute
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer attribute_id
 * @property integer language_id
 * @property string text
 * @property int $product_id
 * @property int $attribute_id
 * @property int $language_id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_attribute onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereAttributeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_attribute withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_attribute withoutTrashed()
 */
	class oc_product_attribute extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_review
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer product_id
 * @property integer customer_id
 * @property string author
 * @property string text
 * @property integer rating
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property int $product_id
 * @property int $customer_id
 * @property string $author
 * @property string $text
 * @property int $rating
 * @property bool $status
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_review onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_review withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_review withoutTrashed()
 */
	class oc_review extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_filter
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer filter_group_id
 * @property integer sort_order
 * @property int $id
 * @property int $filter_group_id
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereFilterGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter withoutTrashed()
 */
	class oc_filter extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_voucher_history
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer voucher_id
 * @property integer order_id
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $voucher_id
 * @property int $order_id
 * @property float $amount
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_history onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_history whereVoucherId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_history withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_history withoutTrashed()
 */
	class oc_voucher_history extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_geo_zone
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string description
 * @property string|\Carbon\Carbon date_modified
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $date_modified
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_geo_zone onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_geo_zone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_geo_zone withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_geo_zone withoutTrashed()
 */
	class oc_geo_zone extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_layout_module
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer layout_id
 * @property string code
 * @property string position
 * @property integer sort_order
 * @property int $id
 * @property int $layout_id
 * @property string $code
 * @property string $position
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_module onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereLayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout_module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_module withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout_module withoutTrashed()
 */
	class oc_layout_module extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_currency
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string title
 * @property string code
 * @property string symbol_left
 * @property string symbol_right
 * @property string decimal_place
 * @property float value
 * @property boolean status
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $title
 * @property string $code
 * @property string $symbol_left
 * @property string $symbol_right
 * @property string $decimal_place
 * @property float $value
 * @property bool $status
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_currency onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereDecimalPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereSymbolLeft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereSymbolRight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_currency whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_currency withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_currency withoutTrashed()
 */
	class oc_currency extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property string image
 * @property integer parent_id
 * @property boolean top
 * @property integer column
 * @property integer sort_order
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $image
 * @property int $parent_id
 * @property bool $top
 * @property int $column
 * @property int $sort_order
 * @property bool $status
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereColumn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category withoutTrashed()
 */
	class oc_category extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_option
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property string type
 * @property integer sort_order
 * @property int $id
 * @property string $type
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option withoutTrashed()
 */
	class oc_option extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_user
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer user_group_id
 * @property string username
 * @property string password
 * @property string salt
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string image
 * @property string code
 * @property string ip
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $user_group_id
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $image
 * @property string $code
 * @property string $ip
 * @property bool $status
 * @property string $date_added
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereSalt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereUserGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_user whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_user withoutTrashed()
 */
	class oc_user extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_affiliate
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer sub_district_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string password
 * @property string salt
 * @property string company
 * @property string website
 * @property string address_1
 * @property string address_2
 * @property string city
 * @property string postcode
 * @property integer country_id
 * @property integer zone_id
 * @property string code
 * @property decimal commission
 * @property string tax
 * @property string payment
 * @property string cheque
 * @property string paypal
 * @property string bank_name
 * @property string bank_branch_number
 * @property string bank_swift_code
 * @property string bank_account_name
 * @property string bank_account_number
 * @property string ip
 * @property boolean status
 * @property boolean approved
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $sub_district_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $telephone
 * @property string $fax
 * @property string $password
 * @property string $salt
 * @property string $company
 * @property string $website
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $postcode
 * @property int $country_id
 * @property int $zone_id
 * @property string $code
 * @property float $commission
 * @property string $tax
 * @property string $payment
 * @property string $cheque
 * @property string $paypal
 * @property string $bank_name
 * @property string $bank_branch_number
 * @property string $bank_swift_code
 * @property string $bank_account_name
 * @property string $bank_account_number
 * @property string $ip
 * @property bool $status
 * @property bool $approved
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereBankAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereBankAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereBankBranchNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereBankSwiftCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCheque($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate wherePaypal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereSalt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereSubDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_affiliate whereZoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_affiliate withoutTrashed()
 */
	class oc_affiliate extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_to_layout
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer store_id
 * @property integer layout_id
 * @property int $product_id
 * @property int $store_id
 * @property int $layout_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_layout onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereLayoutId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_to_layout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_layout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_to_layout withoutTrashed()
 */
	class oc_product_to_layout extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_download_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_download_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_download_description withoutTrashed()
 */
	class oc_download_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_return
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer order_id
 * @property integer product_id
 * @property integer customer_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string product
 * @property string model
 * @property integer quantity
 * @property boolean opened
 * @property integer return_reason_id
 * @property integer return_action_id
 * @property integer return_status_id
 * @property string comment
 * @property date date_ordered
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int $customer_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $telephone
 * @property string $product
 * @property string $model
 * @property int $quantity
 * @property bool $opened
 * @property int $return_reason_id
 * @property int $return_action_id
 * @property int $return_status_id
 * @property string $comment
 * @property \Carbon\Carbon $date_ordered
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereDateOrdered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereOpened($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereReturnActionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereReturnReasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereReturnStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return withoutTrashed()
 */
	class oc_return extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_information
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer bottom
 * @property integer sort_order
 * @property boolean status
 * @property int $id
 * @property int $bottom
 * @property int $sort_order
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereBottom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_information whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_information withoutTrashed()
 */
	class oc_information extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_zone
 *
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 * @property integer country_id
 * @property string name
 * @property string code
 * @property boolean status
 * @property int $id
 * @property int $country_id
 * @property string $name
 * @property string $code
 * @property bool $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_zone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_zone withoutTrashed()
 */
	class oc_zone extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_filter_group_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_filter_group_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_filter_group_description withoutTrashed()
 */
	class oc_filter_group_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_weight_class_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 * @property integer language_id
 * @property string title
 * @property string unit
 * @property int $id
 * @property int $language_id
 * @property string $title
 * @property string $unit
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class_description withoutTrashed()
 */
	class oc_weight_class_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_weight_class
 *
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 * @property decimal value
 * @property int $id
 * @property float $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_weight_class whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_weight_class withoutTrashed()
 */
	class oc_weight_class extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_layout
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_layout whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_layout withoutTrashed()
 */
	class oc_layout extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_banner_image_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer language_id
 * @property integer banner_id
 * @property string title
 * @property int $id
 * @property int $language_id
 * @property int $banner_id
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereBannerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_banner_image_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_banner_image_description withoutTrashed()
 */
	class oc_banner_image_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_attribute_group
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer sort_order
 * @property int $id
 * @property int $sort_order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_attribute_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_attribute_group withoutTrashed()
 */
	class oc_attribute_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_ban_ip
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property string ip
 * @property int $id
 * @property string $ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ban_ip onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ban_ip whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ban_ip whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ban_ip whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ban_ip whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_ban_ip whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ban_ip withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_ban_ip withoutTrashed()
 */
	class oc_customer_ban_ip extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_option_value_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer language_id
 * @property integer option_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property int $option_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_option_value_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_option_value_description withoutTrashed()
 */
	class oc_option_value_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_custom_field_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_custom_field_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_custom_field_description withoutTrashed()
 */
	class oc_custom_field_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer_group_description
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer language_id
 * @property string name
 * @property string description
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer_group_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer_group_description withoutTrashed()
 */
	class oc_customer_group_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_voucher_theme_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_voucher_theme_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_voucher_theme_description withoutTrashed()
 */
	class oc_voucher_theme_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_option_value
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_option_id
 * @property integer product_id
 * @property integer option_id
 * @property integer option_value_id
 * @property integer quantity
 * @property boolean subtract
 * @property decimal price
 * @property string price_prefix
 * @property integer points
 * @property string points_prefix
 * @property decimal weight
 * @property string weight_prefix
 * @property int $id
 * @property int $product_option_id
 * @property int $product_id
 * @property int $option_id
 * @property int $option_value_id
 * @property int $quantity
 * @property bool $subtract
 * @property float $price
 * @property string $price_prefix
 * @property int $points
 * @property string $points_prefix
 * @property float $weight
 * @property string $weight_prefix
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option_value onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereOptionValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value wherePointsPrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value wherePricePrefix($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereProductOptionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereSubtract($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereWeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_option_value whereWeightPrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option_value withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_option_value withoutTrashed()
 */
	class oc_product_option_value extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_confirm_to_store
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer store_id
 * @property int $id
 * @property int $store_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm_to_store onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm_to_store whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm_to_store whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm_to_store whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm_to_store whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_confirm_to_store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm_to_store withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_confirm_to_store withoutTrashed()
 */
	class oc_confirm_to_store extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_tax_class
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string title
 * @property string description
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $date_added
 * @property string $date_modified
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_class onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereDateModified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_class whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_class withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_class withoutTrashed()
 */
	class oc_tax_class extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_upload
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property string name
 * @property string filename
 * @property string code
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $name
 * @property string $filename
 * @property string $code
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_upload onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_upload whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_upload withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_upload withoutTrashed()
 */
	class oc_upload extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_category_filter
 *
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 * @property integer filter_id
 * @property int $id
 * @property int $filter_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_filter onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_filter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_filter whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_filter whereFilterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_filter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_category_filter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_filter withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_category_filter withoutTrashed()
 */
	class oc_category_filter extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_recurring_description
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring_description onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_recurring_description whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring_description withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_recurring_description withoutTrashed()
 */
	class oc_recurring_description extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_tax_rate_to_customer_group
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer customer_group_id
 * @property int $tax_rate_id
 * @property int $customer_group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate_to_customer_group onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate_to_customer_group whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate_to_customer_group whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate_to_customer_group whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate_to_customer_group whereTaxRateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_tax_rate_to_customer_group whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate_to_customer_group withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_tax_rate_to_customer_group withoutTrashed()
 */
	class oc_tax_rate_to_customer_group extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_return_reason
 *
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 * @property integer language_id
 * @property string name
 * @property int $id
 * @property int $language_id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_reason onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_return_reason whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_reason withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_return_reason withoutTrashed()
 */
	class oc_return_reason extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_product_reward
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer product_id
 * @property integer customer_group_id
 * @property integer points
 * @property int $id
 * @property int $product_id
 * @property int $customer_group_id
 * @property int $points
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_reward onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_product_reward whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_reward withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_product_reward withoutTrashed()
 */
	class oc_product_reward extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_history
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property integer order_status_id
 * @property boolean notify
 * @property string comment
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $order_id
 * @property int $order_status_id
 * @property bool $notify
 * @property string $comment
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_history onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereOrderStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_history whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_history withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_history withoutTrashed()
 */
	class oc_order_history extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_modification
 *
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 * @property string name
 * @property string code
 * @property string author
 * @property string version
 * @property string link
 * @property string xml
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $author
 * @property string $version
 * @property string $link
 * @property string $xml
 * @property bool $status
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_modification onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_modification whereXml($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_modification withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_modification withoutTrashed()
 */
	class oc_modification extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_address
 *
 * @package App\Models
 * @version August 26, 2017, 7:34 am UTC
 * @property integer sub_district_id
 * @property integer customer_id
 * @property string firstname
 * @property string lastname
 * @property string company
 * @property string address_1
 * @property string address_2
 * @property string city
 * @property string postcode
 * @property integer country_id
 * @property integer zone_id
 * @property string custom_field
 * @property int $id
 * @property int $sub_district_id
 * @property int $customer_id
 * @property string $firstname
 * @property string $lastname
 * @property string $company
 * @property string $address_1
 * @property string $address_2
 * @property string $city
 * @property string $postcode
 * @property int $country_id
 * @property int $zone_id
 * @property string $custom_field
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_address onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCustomField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereSubDistrictId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_address whereZoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_address withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_address withoutTrashed()
 */
	class oc_address extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_customer
 *
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 * @property integer customer_group_id
 * @property integer store_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string password
 * @property string salt
 * @property string cart
 * @property string wishlist
 * @property boolean newsletter
 * @property integer address_id
 * @property string custom_field
 * @property string ip
 * @property boolean status
 * @property boolean approved
 * @property boolean safe
 * @property string token
 * @property string|\Carbon\Carbon date_added
 * @property int $id
 * @property int $customer_group_id
 * @property int $store_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $telephone
 * @property string $fax
 * @property string $password
 * @property string $salt
 * @property string $cart
 * @property string $wishlist
 * @property bool $newsletter
 * @property int $address_id
 * @property string $custom_field
 * @property string $ip
 * @property bool $status
 * @property bool $approved
 * @property bool $safe
 * @property string $token
 * @property string $date_added
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereCart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereCustomField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereDateAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereNewsletter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereSafe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereSalt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_customer whereWishlist($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_customer withoutTrashed()
 */
	class oc_customer extends \Eloquent {}
}

namespace App\Models{
/**
 * Class oc_order_voucher
 *
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 * @property integer order_id
 * @property integer voucher_id
 * @property string description
 * @property string code
 * @property string from_name
 * @property string from_email
 * @property string to_name
 * @property string to_email
 * @property integer voucher_theme_id
 * @property string message
 * @property decimal amount
 * @property int $id
 * @property int $order_id
 * @property int $voucher_id
 * @property string $description
 * @property string $code
 * @property string $from_name
 * @property string $from_email
 * @property string $to_name
 * @property string $to_email
 * @property int $voucher_theme_id
 * @property string $message
 * @property float $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_voucher onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereFromEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereFromName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereToEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereToName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereVoucherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\oc_order_voucher whereVoucherThemeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_voucher withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\oc_order_voucher withoutTrashed()
 */
	class oc_order_voucher extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property int $user_group_id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $username
 * @property string|null $type_of_identity
 * @property string|null $identity_number
 * @property string|null $gender
 * @property string|null $phone
 * @property string|null $job
 * @property string|null $address
 * @property string|null $districts
 * @property string|null $city
 * @property string|null $province
 * @property string|null $zip_kode
 * @property string|null $picture
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read mixed $kelamin
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDistricts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIdentityNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereJob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTypeOfIdentity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUserGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereZipKode($value)
 */
	class User extends \Eloquent {}
}

