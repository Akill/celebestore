@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Recurring Transaction
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocOrderRecurringTransactions.store']) !!}

                        @include('oc_order_recurring_transactions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
