<!-- Order Recurring Transaction Id Field -->
<div class="form-group">
    {!! Form::label('order_recurring_transaction_id', 'Order Recurring Transaction Id:') !!}
    <p>{!! $ocOrderRecurringTransaction->order_recurring_transaction_id !!}</p>
</div>

<!-- Order Recurring Id Field -->
<div class="form-group">
    {!! Form::label('order_recurring_id', 'Order Recurring Id:') !!}
    <p>{!! $ocOrderRecurringTransaction->order_recurring_id !!}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{!! $ocOrderRecurringTransaction->reference !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $ocOrderRecurringTransaction->type !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $ocOrderRecurringTransaction->amount !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocOrderRecurringTransaction->date_added !!}</p>
</div>

