<table class="table table-responsive" id="ocOrderRecurringTransactions-table">
    <thead>
        <th>Order Recurring Id</th>
        <th>Reference</th>
        <th>Type</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderRecurringTransactions as $ocOrderRecurringTransaction)
        <tr>
            <td>{!! $ocOrderRecurringTransaction->order_recurring_id !!}</td>
            <td>{!! $ocOrderRecurringTransaction->reference !!}</td>
            <td>{!! $ocOrderRecurringTransaction->type !!}</td>
            <td>{!! $ocOrderRecurringTransaction->amount !!}</td>
            <td>{!! $ocOrderRecurringTransaction->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderRecurringTransactions.destroy', $ocOrderRecurringTransaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderRecurringTransactions.show', [$ocOrderRecurringTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderRecurringTransactions.edit', [$ocOrderRecurringTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>