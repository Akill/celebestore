<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocDownload->id !!}</p>
</div>

<!-- Filename Field -->
<div class="form-group">
    {!! Form::label('filename', 'Filename:') !!}
    <p>{!! $ocDownload->filename !!}</p>
</div>

<!-- Mask Field -->
<div class="form-group">
    {!! Form::label('mask', 'Mask:') !!}
    <p>{!! $ocDownload->mask !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocDownload->date_added !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocDownload->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocDownload->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocDownload->deleted_at !!}</p>
</div>

