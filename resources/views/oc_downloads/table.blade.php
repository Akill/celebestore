<table class="table table-responsive" id="ocDownloads-table">
    <thead>
        <th>Filename</th>
        <th>Mask</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocDownloads as $ocDownload)
        <tr>
            <td>{!! $ocDownload->filename !!}</td>
            <td>{!! $ocDownload->mask !!}</td>
            <td>{!! $ocDownload->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocDownloads.destroy', $ocDownload->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocDownloads.show', [$ocDownload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocDownloads.edit', [$ocDownload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>