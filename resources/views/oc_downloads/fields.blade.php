<!-- Filename Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filename', 'Filename:') !!}
    {!! Form::text('filename', null, ['class' => 'form-control']) !!}
</div>

<!-- Mask Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mask', 'Mask:') !!}
    {!! Form::text('mask', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocDownloads.index') !!}" class="btn btn-default">Cancel</a>
</div>
