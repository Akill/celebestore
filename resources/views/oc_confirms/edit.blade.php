@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Confirm
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocConfirm, ['route' => ['ocConfirms.update', $ocConfirm->id], 'method' => 'patch']) !!}

                        @include('oc_confirms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection