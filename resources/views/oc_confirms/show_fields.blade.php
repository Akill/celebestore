<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocConfirm->id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocConfirm->email !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocConfirm->order_id !!}</p>
</div>

<!-- Payment Date Field -->
<div class="form-group">
    {!! Form::label('payment_date', 'Payment Date:') !!}
    <p>{!! $ocConfirm->payment_date !!}</p>
</div>

<!-- Total Amount Field -->
<div class="form-group">
    {!! Form::label('total_amount', 'Total Amount:') !!}
    <p>{!! $ocConfirm->total_amount !!}</p>
</div>

<!-- Destination Bank Field -->
<div class="form-group">
    {!! Form::label('destination_bank', 'Destination Bank:') !!}
    <p>{!! $ocConfirm->destination_bank !!}</p>
</div>

<!-- Resi Field -->
<div class="form-group">
    {!! Form::label('resi', 'Resi:') !!}
    <p>{!! $ocConfirm->resi !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $ocConfirm->payment_method !!}</p>
</div>

<!-- Sender Name Field -->
<div class="form-group">
    {!! Form::label('sender_name', 'Sender Name:') !!}
    <p>{!! $ocConfirm->sender_name !!}</p>
</div>

<!-- Bank Origin Field -->
<div class="form-group">
    {!! Form::label('bank_origin', 'Bank Origin:') !!}
    <p>{!! $ocConfirm->bank_origin !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocConfirm->code !!}</p>
</div>

<!-- No Receipt Field -->
<div class="form-group">
    {!! Form::label('no_receipt', 'No Receipt:') !!}
    <p>{!! $ocConfirm->no_receipt !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocConfirm->date_added !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocConfirm->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocConfirm->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocConfirm->deleted_at !!}</p>
</div>

