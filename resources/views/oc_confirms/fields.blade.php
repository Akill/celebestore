<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::text('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_date', 'Payment Date:') !!}
    {!! Form::date('payment_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_amount', 'Total Amount:') !!}
    {!! Form::number('total_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Destination Bank Field -->
<div class="form-group col-sm-6">
    {!! Form::label('destination_bank', 'Destination Bank:') !!}
    {!! Form::text('destination_bank', null, ['class' => 'form-control']) !!}
</div>

<!-- Resi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resi', 'Resi:') !!}
    {!! Form::text('resi', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Sender Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sender_name', 'Sender Name:') !!}
    {!! Form::text('sender_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Origin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_origin', 'Bank Origin:') !!}
    {!! Form::text('bank_origin', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- No Receipt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_receipt', 'No Receipt:') !!}
    {!! Form::text('no_receipt', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocConfirms.index') !!}" class="btn btn-default">Cancel</a>
</div>
