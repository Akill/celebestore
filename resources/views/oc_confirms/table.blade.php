<table class="table table-responsive" id="ocConfirms-table">
    <thead>
        <th>Email</th>
        <th>Order Id</th>
        <th>Payment Date</th>
        <th>Total Amount</th>
        <th>Destination Bank</th>
        <th>Resi</th>
        <th>Payment Method</th>
        <th>Sender Name</th>
        <th>Bank Origin</th>
        <th>Code</th>
        <th>No Receipt</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocConfirms as $ocConfirm)
        <tr>
            <td>{!! $ocConfirm->email !!}</td>
            <td>{!! $ocConfirm->order_id !!}</td>
            <td>{!! $ocConfirm->payment_date !!}</td>
            <td>{!! $ocConfirm->total_amount !!}</td>
            <td>{!! $ocConfirm->destination_bank !!}</td>
            <td>{!! $ocConfirm->resi !!}</td>
            <td>{!! $ocConfirm->payment_method !!}</td>
            <td>{!! $ocConfirm->sender_name !!}</td>
            <td>{!! $ocConfirm->bank_origin !!}</td>
            <td>{!! $ocConfirm->code !!}</td>
            <td>{!! $ocConfirm->no_receipt !!}</td>
            <td>{!! $ocConfirm->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocConfirms.destroy', $ocConfirm->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocConfirms.show', [$ocConfirm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocConfirms.edit', [$ocConfirm->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>