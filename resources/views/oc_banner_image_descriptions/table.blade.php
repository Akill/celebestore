<table class="table table-responsive" id="ocBannerImageDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Banner Id</th>
        <th>Title</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocBannerImageDescriptions as $ocBannerImageDescription)
        <tr>
            <td>{!! $ocBannerImageDescription->language_id !!}</td>
            <td>{!! $ocBannerImageDescription->banner_id !!}</td>
            <td>{!! $ocBannerImageDescription->title !!}</td>
            <td>
                {!! Form::open(['route' => ['ocBannerImageDescriptions.destroy', $ocBannerImageDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocBannerImageDescriptions.show', [$ocBannerImageDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocBannerImageDescriptions.edit', [$ocBannerImageDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>