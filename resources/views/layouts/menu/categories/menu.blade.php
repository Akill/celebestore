@php 
$active = Request::is('ocCategories*') ? 'active' : 
Request::is('ocCategoryDescriptions*') ? 'active' : 
Request::is('ocCategoryFilters*') ? 'active' : 
Request::is('ocCategoryPaths*') ? 'active' : 
Request::is('ocCategoryToLayouts*') ? 'active' : 
Request::is('ocCategoryToStores*') ? 'active' : ''; 
@endphp

<li class="{{ $active }} treeview menu-open">
    <a href="#">
        <span>Categories</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocCategories.index') !!}"><i class="fa fa-table"></i><span>categories</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryDescriptions.index') !!}"><i class="fa fa-table"></i><span>category descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryFilters*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryFilters.index') !!}"><i class="fa fa-table"></i><span>category filters</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryPaths*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryPaths.index') !!}"><i class="fa fa-table"></i><span>category paths</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryToLayouts*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryToLayouts.index') !!}"><i class="fa fa-table"></i><span>category to layouts</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryToStores*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryToStores.index') !!}"><i class="fa fa-table"></i><span>category to stores</span></a>
        </li>
    </ul>
</li>