@php 
$active = Request::is('ocProducts*') ? 'active' : Request::is('ocProductAttributes*') ? 'active' : Request::is('ocProductDescriptions*') ? 'active' : Request::is('ocProductDiscounts*') ? 'active' : Request::is('ocProductImages*') ? 'active' : Request::is('ocProductOptions*') ? 'active' : Request::is('ocProductOptionValues*') ? 'active' : Request::is('ocProductRewards*') ? 'active' : Request::is('ocProductSpecials*') ? 'active' : Request::is('ocProductToCategories*') ? 'active' : 
Request::is('ocProductToDownloads*') ? 'active' : Request::is('ocProductToLayouts*') ? 'active' : 
Request::is('ocProductToStores*') ? 'active' : ''; 
@endphp

<li class="{{ $active }} treeview menu-open">
    <a href="#">
        <span>Products</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocProducts*') ? 'active' : '' }}">
            <a href="{!! route('ocProducts.index') !!}"><i class="fa fa-table"></i><span>product </span></a>
        </li>

        <li class="{{ Request::is('ocProductAttributes*') ? 'active' : '' }}">
            <a href="{!! route('ocProductAttributes.index') !!}"><i class="fa fa-table"></i><span>product attributes</span></a>
        </li>

        <li class="{{ Request::is('ocProductDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocProductDescriptions.index') !!}"><i class="fa fa-table"></i><span>product descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocProductDiscounts*') ? 'active' : '' }}">
            <a href="{!! route('ocProductDiscounts.index') !!}"><i class="fa fa-table"></i><span>product discounts</span></a>
        </li>

        <li class="{{ Request::is('ocProductImages*') ? 'active' : '' }}">
            <a href="{!! route('ocProductImages.index') !!}"><i class="fa fa-table"></i><span>product images</span></a>
        </li>

        <li class="{{ Request::is('ocProductOptions*') ? 'active' : '' }}">
            <a href="{!! route('ocProductOptions.index') !!}"><i class="fa fa-table"></i><span>product options</span></a>
        </li>

        <li class="{{ Request::is('ocProductOptionValues*') ? 'active' : '' }}">
            <a href="{!! route('ocProductOptionValues.index') !!}"><i class="fa fa-table"></i><span>product option values</span></a>
        </li>

        <li class="{{ Request::is('ocProductRewards*') ? 'active' : '' }}">
            <a href="{!! route('ocProductRewards.index') !!}"><i class="fa fa-table"></i><span>product rewards</span></a>
        </li>

        <li class="{{ Request::is('ocProductSpecials*') ? 'active' : '' }}">
            <a href="{!! route('ocProductSpecials.index') !!}"><i class="fa fa-table"></i><span>product <s></s>pecials</span></a>
        </li>

        <li class="{{ Request::is('ocProductToCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocProductToCategories.index') !!}"><i class="fa fa-table"></i><span>product to categories</span></a>
        </li>

        <li class="{{ Request::is('ocProductToDownloads*') ? 'active' : '' }}">
            <a href="{!! route('ocProductToDownloads.index') !!}"><i class="fa fa-table"></i><span>product to downloads</span></a>
        </li>

        <li class="{{ Request::is('ocProductToLayouts*') ? 'active' : '' }}">
            <a href="{!! route('ocProductToLayouts.index') !!}"><i class="fa fa-table"></i><span>product to layouts</span></a>
        </li>

        <li class="{{ Request::is('ocProductToStores*') ? 'active' : '' }}">
            <a href="{!! route('ocProductToStores.index') !!}"><i class="fa fa-table"></i><span>product to stores</span></a>
        </li>
    </ul>
</li>