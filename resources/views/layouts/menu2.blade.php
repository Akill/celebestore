<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>

<li class="{{ Request::is('ocAddresses*') ? 'active' : '' }}">
    <a href="{!! route('ocAddresses.index') !!}"><i class="fa fa-edit"></i><span>addresses</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Affiliates</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocAffiliates*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliates.index') !!}"><i class="fa fa-edit"></i><span>affiliates</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateActivities*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateActivities.index') !!}"><i class="fa fa-edit"></i><span>affiliate activities</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateLogins*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateLogins.index') !!}"><i class="fa fa-edit"></i><span>affiliate logins</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateTransactions*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateTransactions.index') !!}"><i class="fa fa-edit"></i><span>affiliate transactions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocApis*') ? 'active' : '' }}">
    <a href="{!! route('ocApis.index') !!}"><i class="fa fa-edit"></i><span>apis</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Attributes</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocAttributes*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributes.index') !!}"><i class="fa fa-edit"></i><span>attributes</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeDescriptions.index') !!}"><i class="fa fa-edit"></i><span>attribute descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeGroups.index') !!}"><i class="fa fa-edit"></i><span>attribute groups</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeGroupDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeGroupDescriptions.index') !!}"><i class="fa fa-edit"></i><span>attribute group descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Banners</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocBanners*') ? 'active' : '' }}">
            <a href="{!! route('ocBanners.index') !!}"><i class="fa fa-edit"></i><span>banners</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImages*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImages.index') !!}"><i class="fa fa-edit"></i><span>banner images</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImageDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImageDescriptions.index') !!}"><i class="fa fa-edit"></i><span>banner image descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Confirms</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocConfirms*') ? 'active' : '' }}">
            <a href="{!! route('ocConfirms.index') !!}"><i class="fa fa-edit"></i><span>confirms</span></a>
        </li>

        <li class="{{ Request::is('ocConfirmToStores*') ? 'active' : '' }}">
            <a href="{!! route('ocConfirmToStores.index') !!}"><i class="fa fa-edit"></i><span>confirm to stores</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocCountries*') ? 'active' : '' }}">
    <a href="{!! route('ocCountries.index') !!}"><i class="fa fa-edit"></i><span>countries</span></a>
</li>

<li class="{{ Request::is('ocCountryHpwds*') ? 'active' : '' }}">
    <a href="{!! route('ocCountryHpwds.index') !!}"><i class="fa fa-edit"></i><span>country hpwds</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Coupons</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCoupons*') ? 'active' : '' }}">
            <a href="{!! route('ocCoupons.index') !!}"><i class="fa fa-edit"></i><span>coupons</span></a>
        </li>

        <li class="{{ Request::is('ocCouponCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponCategories.index') !!}"><i class="fa fa-edit"></i><span>coupon categories</span></a>
        </li>

        <li class="{{ Request::is('ocCouponHistories*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponHistories.index') !!}"><i class="fa fa-edit"></i><span>coupon histories</span></a>
        </li>

        <li class="{{ Request::is('ocCouponProducts*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponProducts.index') !!}"><i class="fa fa-edit"></i><span>coupon products</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocCurrencies*') ? 'active' : '' }}">
    <a href="{!! route('ocCurrencies.index') !!}"><i class="fa fa-edit"></i><span>currencies</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCustomers*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomers.index') !!}"><i class="fa fa-edit"></i><span>customers</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerActivities*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerActivities.index') !!}"><i class="fa fa-edit"></i><span>customer activities</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerBanIps*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerBanIps.index') !!}"><i class="fa fa-edit"></i><span>customer ban ips</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerGroups.index') !!}"><i class="fa fa-edit"></i><span>customer groups</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerGroupDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerGroupDescriptions.index') !!}"><i class="fa fa-edit"></i><span>customer group descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerHistories*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerHistories.index') !!}"><i class="fa fa-edit"></i><span>customer histories</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerIps*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerIps.index') !!}"><i class="fa fa-edit"></i><span>customer ips</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerLogins*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerLogins.index') !!}"><i class="fa fa-edit"></i><span>customer logins</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerOnlines*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerOnlines.index') !!}"><i class="fa fa-edit"></i><span>customer onlines</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerRewards*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerRewards.index') !!}"><i class="fa fa-edit"></i><span>customer rewards</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerTransactions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerTransactions.index') !!}"><i class="fa fa-edit"></i><span>customer transactions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Custom Fields</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCustomFields*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFields.index') !!}"><i class="fa fa-edit"></i><span>custom fields</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldCustomerGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldCustomerGroups.index') !!}"><i class="fa fa-edit"></i><span>custom field customer groups</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldDescriptions.index') !!}"><i class="fa fa-edit"></i><span>custom field descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldValues*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldValues.index') !!}"><i class="fa fa-edit"></i><span>custom field values</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldValueDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldValueDescriptions.index') !!}"><i class="fa fa-edit"></i><span>custom field value descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Downloads</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocDownloads*') ? 'active' : '' }}">
            <a href="{!! route('ocDownloads.index') !!}"><i class="fa fa-edit"></i><span>downloads</span></a>
        </li>

        <li class="{{ Request::is('ocDownloadDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocDownloadDescriptions.index') !!}"><i class="fa fa-edit"></i><span>download descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocEvents*') ? 'active' : '' }}">
    <a href="{!! route('ocEvents.index') !!}"><i class="fa fa-edit"></i><span>events</span></a>
</li>

<li class="{{ Request::is('ocExtensions*') ? 'active' : '' }}">
    <a href="{!! route('ocExtensions.index') !!}"><i class="fa fa-edit"></i><span>extensions</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Filters</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocFilters*') ? 'active' : '' }}">
            <a href="{!! route('ocFilters.index') !!}"><i class="fa fa-edit"></i><span>filters</span></a>
        </li>

        <li class="{{ Request::is('ocFilterDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocFilterDescriptions.index') !!}"><i class="fa fa-edit"></i><span>filter descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocGeoZones*') ? 'active' : '' }}">
    <a href="{!! route('ocGeoZones.index') !!}"><i class="fa fa-edit"></i><span>geo zones</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Informations</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocInformations*') ? 'active' : '' }}">
            <a href="{!! route('ocInformations.index') !!}"><i class="fa fa-edit"></i><span>informations</span></a>
        </li>

        <li class="{{ Request::is('ocInformationDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocInformationDescriptions.index') !!}"><i class="fa fa-edit"></i><span>information descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocLanguages*') ? 'active' : '' }}">
    <a href="{!! route('ocLanguages.index') !!}"><i class="fa fa-edit"></i><span>languages</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Layout Modules</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocLayoutModules*') ? 'active' : '' }}">
            <a href="{!! route('ocLayoutModules.index') !!}"><i class="fa fa-edit"></i><span>layout modules</span></a>
        </li>

        <li class="{{ Request::is('ocLayoutRoutes*') ? 'active' : '' }}">
            <a href="{!! route('ocLayoutRoutes.index') !!}"><i class="fa fa-edit"></i><span>layout routes</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocLengthClassDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocLengthClassDescriptions.index') !!}"><i class="fa fa-edit"></i><span>length class descriptions</span></a>
</li>

<li class="{{ Request::is('ocLocations*') ? 'active' : '' }}">
    <a href="{!! route('ocLocations.index') !!}"><i class="fa fa-edit"></i><span>locations</span></a>
</li>

<li class="{{ Request::is('ocManufacturers*') ? 'active' : '' }}">
    <a href="{!! route('ocManufacturers.index') !!}"><i class="fa fa-edit"></i><span>manufacturers</span></a>
</li>

<li class="{{ Request::is('ocMarketings*') ? 'active' : '' }}">
    <a href="{!! route('ocMarketings.index') !!}"><i class="fa fa-edit"></i><span>marketings</span></a>
</li>

<li class="{{ Request::is('ocModifications*') ? 'active' : '' }}">
    <a href="{!! route('ocModifications.index') !!}"><i class="fa fa-edit"></i><span>modifications</span></a>
</li>

<li class="{{ Request::is('ocModules*') ? 'active' : '' }}">
    <a href="{!! route('ocModules.index') !!}"><i class="fa fa-edit"></i><span>modules</span></a>
</li>

<li class="{{ Request::is('ocOptionValues*') ? 'active' : '' }}">
    <a href="{!! route('ocOptionValues.index') !!}"><i class="fa fa-edit"></i><span>option values</span></a>
</li>

<li class="{{ Request::is('ocOptionValueDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocOptionValueDescriptions.index') !!}"><i class="fa fa-edit"></i><span>option value descriptions</span></a>
</li>

<li class="{{ Request::is('ocOrders*') ? 'active' : '' }}">
    <a href="{!! route('ocOrders.index') !!}"><i class="fa fa-edit"></i><span>orders</span></a>
</li>

<li class="{{ Request::is('ocOrderCustomFields*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderCustomFields.index') !!}"><i class="fa fa-edit"></i><span>order custom fields</span></a>
</li>

<li class="{{ Request::is('ocOrderHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderHistories.index') !!}"><i class="fa fa-edit"></i><span>order histories</span></a>
</li>

<li class="{{ Request::is('ocOrderOptions*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderOptions.index') !!}"><i class="fa fa-edit"></i><span>order options</span></a>
</li>

<li class="{{ Request::is('ocOrderProducts*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderProducts.index') !!}"><i class="fa fa-edit"></i><span>order products</span></a>
</li>

<li class="{{ Request::is('ocOrderRecurrings*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderRecurrings.index') !!}"><i class="fa fa-edit"></i><span>order recurrings</span></a>
</li>

<li class="{{ Request::is('ocOrderRecurringTransactions*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderRecurringTransactions.index') !!}"><i class="fa fa-edit"></i><span>order recurring transactions</span></a>
</li>

<li class="{{ Request::is('ocOrderTotals*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderTotals.index') !!}"><i class="fa fa-edit"></i><span>order totals</span></a>
</li>

<li class="{{ Request::is('ocOrderVouchers*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderVouchers.index') !!}"><i class="fa fa-edit"></i><span>order vouchers</span></a>
</li>

<li class="{{ Request::is('ocProducts*') ? 'active' : '' }}">
    <a href="{!! route('ocProducts.index') !!}"><i class="fa fa-edit"></i><span>products</span></a>
</li>

<li class="{{ Request::is('ocProductAttributes*') ? 'active' : '' }}">
    <a href="{!! route('ocProductAttributes.index') !!}"><i class="fa fa-edit"></i><span>product attributes</span></a>
</li>

<li class="{{ Request::is('ocProductDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocProductDescriptions.index') !!}"><i class="fa fa-edit"></i><span>product descriptions</span></a>
</li>

<li class="{{ Request::is('ocProductDiscounts*') ? 'active' : '' }}">
    <a href="{!! route('ocProductDiscounts.index') !!}"><i class="fa fa-edit"></i><span>product discounts</span></a>
</li>

<li class="{{ Request::is('ocProductImages*') ? 'active' : '' }}">
    <a href="{!! route('ocProductImages.index') !!}"><i class="fa fa-edit"></i><span>product images</span></a>
</li>

<li class="{{ Request::is('ocProductOptions*') ? 'active' : '' }}">
    <a href="{!! route('ocProductOptions.index') !!}"><i class="fa fa-edit"></i><span>product options</span></a>
</li>

<li class="{{ Request::is('ocProductOptionValues*') ? 'active' : '' }}">
    <a href="{!! route('ocProductOptionValues.index') !!}"><i class="fa fa-edit"></i><span>product option values</span></a>
</li>

<li class="{{ Request::is('ocProductRewards*') ? 'active' : '' }}">
    <a href="{!! route('ocProductRewards.index') !!}"><i class="fa fa-edit"></i><span>product rewards</span></a>
</li>

<li class="{{ Request::is('ocProductSpecials*') ? 'active' : '' }}">
    <a href="{!! route('ocProductSpecials.index') !!}"><i class="fa fa-edit"></i><span>product specials</span></a>
</li>

<li class="{{ Request::is('ocRecurrings*') ? 'active' : '' }}">
    <a href="{!! route('ocRecurrings.index') !!}"><i class="fa fa-edit"></i><span>recurrings</span></a>
</li>

<li class="{{ Request::is('ocReturns*') ? 'active' : '' }}">
    <a href="{!! route('ocReturns.index') !!}"><i class="fa fa-edit"></i><span>returns</span></a>
</li>

<li class="{{ Request::is('ocReturnHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocReturnHistories.index') !!}"><i class="fa fa-edit"></i><span>return histories</span></a>
</li>

<li class="{{ Request::is('ocReviews*') ? 'active' : '' }}">
    <a href="{!! route('ocReviews.index') !!}"><i class="fa fa-edit"></i><span>reviews</span></a>
</li>

<li class="{{ Request::is('ocSettings*') ? 'active' : '' }}">
    <a href="{!! route('ocSettings.index') !!}"><i class="fa fa-edit"></i><span>settings</span></a>
</li>

<li class="{{ Request::is('ocStores*') ? 'active' : '' }}">
    <a href="{!! route('ocStores.index') !!}"><i class="fa fa-edit"></i><span>stores</span></a>
</li>

<li class="{{ Request::is('ocTaxClasses*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxClasses.index') !!}"><i class="fa fa-edit"></i><span>tax classes</span></a>
</li>

<li class="{{ Request::is('ocTaxRates*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxRates.index') !!}"><i class="fa fa-edit"></i><span>tax rates</span></a>
</li>

<li class="{{ Request::is('ocTaxRules*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxRules.index') !!}"><i class="fa fa-edit"></i><span>tax rules</span></a>
</li>

<li class="{{ Request::is('ocUploads*') ? 'active' : '' }}">
    <a href="{!! route('ocUploads.index') !!}"><i class="fa fa-edit"></i><span>uploads</span></a>
</li>

<li class="{{ Request::is('ocUsers*') ? 'active' : '' }}">
    <a href="{!! route('ocUsers.index') !!}"><i class="fa fa-edit"></i><span>users</span></a>
</li>

<li class="{{ Request::is('ocUserGroups*') ? 'active' : '' }}">
    <a href="{!! route('ocUserGroups.index') !!}"><i class="fa fa-edit"></i><span>user groups</span></a>
</li>

<li class="{{ Request::is('ocVouchers*') ? 'active' : '' }}">
    <a href="{!! route('ocVouchers.index') !!}"><i class="fa fa-edit"></i><span>vouchers</span></a>
</li>

<li class="{{ Request::is('ocVoucherHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocVoucherHistories.index') !!}"><i class="fa fa-edit"></i><span>voucher histories</span></a>
</li>

<li class="{{ Request::is('ocWeightClassDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocWeightClassDescriptions.index') !!}"><i class="fa fa-edit"></i><span>weight class descriptions</span></a>
</li>

<li class="{{ Request::is('ocZones*') ? 'active' : '' }}">
    <a href="{!! route('ocZones.index') !!}"><i class="fa fa-edit"></i><span>zones</span></a>
</li>

<li class="{{ Request::is('ocZoneToGeoZones*') ? 'active' : '' }}">
    <a href="{!! route('ocZoneToGeoZones.index') !!}"><i class="fa fa-edit"></i><span>zone to geo zones</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>users</span></a>
</li>


<!-- Sudah di edit -->

<li class="{{ Request::is('ocAddresses*') ? 'active' : '' }}">
    <a href="{!! route('ocAddresses.index') !!}"><i class="fa fa-edit"></i><span>addresses</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Affiliates</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocAffiliates*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliates.index') !!}"><i class="fa fa-edit"></i><span>affiliates</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateActivities*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateActivities.index') !!}"><i class="fa fa-edit"></i><span>affiliate activities</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateLogins*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateLogins.index') !!}"><i class="fa fa-edit"></i><span>affiliate logins</span></a>
        </li>

        <li class="{{ Request::is('ocAffiliateTransactions*') ? 'active' : '' }}">
            <a href="{!! route('ocAffiliateTransactions.index') !!}"><i class="fa fa-edit"></i><span>affiliate transactions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocApis*') ? 'active' : '' }}">
    <a href="{!! route('ocApis.index') !!}"><i class="fa fa-edit"></i><span>apis</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Attributes</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocAttributes*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributes.index') !!}"><i class="fa fa-edit"></i><span>attributes</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeDescriptions.index') !!}"><i class="fa fa-edit"></i><span>attribute descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeGroups.index') !!}"><i class="fa fa-edit"></i><span>attribute groups</span></a>
        </li>

        <li class="{{ Request::is('ocAttributeGroupDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocAttributeGroupDescriptions.index') !!}"><i class="fa fa-edit"></i><span>attribute group descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Banners</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocBanners*') ? 'active' : '' }}">
            <a href="{!! route('ocBanners.index') !!}"><i class="fa fa-edit"></i><span>banners</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImages*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImages.index') !!}"><i class="fa fa-edit"></i><span>banner images</span></a>
        </li>

        <li class="{{ Request::is('ocBannerImageDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocBannerImageDescriptions.index') !!}"><i class="fa fa-edit"></i><span>banner image descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Categories</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocCategories.index') !!}"><i class="fa fa-edit"></i><span>categories</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryDescriptions.index') !!}"><i class="fa fa-edit"></i><span>category descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryFilters*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryFilters.index') !!}"><i class="fa fa-edit"></i><span>category filters</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryPaths*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryPaths.index') !!}"><i class="fa fa-edit"></i><span>category paths</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryToLayouts*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryToLayouts.index') !!}"><i class="fa fa-edit"></i><span>category to layouts</span></a>
        </li>

        <li class="{{ Request::is('ocCategoryToStores*') ? 'active' : '' }}">
            <a href="{!! route('ocCategoryToStores.index') !!}"><i class="fa fa-edit"></i><span>category to stores</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Confirms</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocConfirms*') ? 'active' : '' }}">
            <a href="{!! route('ocConfirms.index') !!}"><i class="fa fa-edit"></i><span>confirms</span></a>
        </li>

        <li class="{{ Request::is('ocConfirmToStores*') ? 'active' : '' }}">
            <a href="{!! route('ocConfirmToStores.index') !!}"><i class="fa fa-edit"></i><span>confirm to stores</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocCountries*') ? 'active' : '' }}">
    <a href="{!! route('ocCountries.index') !!}"><i class="fa fa-edit"></i><span>countries</span></a>
</li>

<li class="{{ Request::is('ocCountryHpwds*') ? 'active' : '' }}">
    <a href="{!! route('ocCountryHpwds.index') !!}"><i class="fa fa-edit"></i><span>country hpwds</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Coupons</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCoupons*') ? 'active' : '' }}">
            <a href="{!! route('ocCoupons.index') !!}"><i class="fa fa-edit"></i><span>coupons</span></a>
        </li>

        <li class="{{ Request::is('ocCouponCategories*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponCategories.index') !!}"><i class="fa fa-edit"></i><span>coupon categories</span></a>
        </li>

        <li class="{{ Request::is('ocCouponHistories*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponHistories.index') !!}"><i class="fa fa-edit"></i><span>coupon histories</span></a>
        </li>

        <li class="{{ Request::is('ocCouponProducts*') ? 'active' : '' }}">
            <a href="{!! route('ocCouponProducts.index') !!}"><i class="fa fa-edit"></i><span>coupon products</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocCurrencies*') ? 'active' : '' }}">
    <a href="{!! route('ocCurrencies.index') !!}"><i class="fa fa-edit"></i><span>currencies</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCustomers*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomers.index') !!}"><i class="fa fa-edit"></i><span>customers</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerActivities*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerActivities.index') !!}"><i class="fa fa-edit"></i><span>customer activities</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerBanIps*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerBanIps.index') !!}"><i class="fa fa-edit"></i><span>customer ban ips</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerGroups.index') !!}"><i class="fa fa-edit"></i><span>customer groups</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerGroupDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerGroupDescriptions.index') !!}"><i class="fa fa-edit"></i><span>customer group descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerHistories*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerHistories.index') !!}"><i class="fa fa-edit"></i><span>customer histories</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerIps*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerIps.index') !!}"><i class="fa fa-edit"></i><span>customer ips</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerLogins*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerLogins.index') !!}"><i class="fa fa-edit"></i><span>customer logins</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerOnlines*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerOnlines.index') !!}"><i class="fa fa-edit"></i><span>customer onlines</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerRewards*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerRewards.index') !!}"><i class="fa fa-edit"></i><span>customer rewards</span></a>
        </li>

        <li class="{{ Request::is('ocCustomerTransactions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomerTransactions.index') !!}"><i class="fa fa-edit"></i><span>customer transactions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Custom Fields</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocCustomFields*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFields.index') !!}"><i class="fa fa-edit"></i><span>custom fields</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldCustomerGroups*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldCustomerGroups.index') !!}"><i class="fa fa-edit"></i><span>custom field customer groups</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldDescriptions.index') !!}"><i class="fa fa-edit"></i><span>custom field descriptions</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldValues*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldValues.index') !!}"><i class="fa fa-edit"></i><span>custom field values</span></a>
        </li>

        <li class="{{ Request::is('ocCustomFieldValueDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocCustomFieldValueDescriptions.index') !!}"><i class="fa fa-edit"></i><span>custom field value descriptions</span></a>
        </li>
    </ul>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Downloads</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocDownloads*') ? 'active' : '' }}">
            <a href="{!! route('ocDownloads.index') !!}"><i class="fa fa-edit"></i><span>downloads</span></a>
        </li>

        <li class="{{ Request::is('ocDownloadDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocDownloadDescriptions.index') !!}"><i class="fa fa-edit"></i><span>download descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocEvents*') ? 'active' : '' }}">
    <a href="{!! route('ocEvents.index') !!}"><i class="fa fa-edit"></i><span>events</span></a>
</li>

<li class="{{ Request::is('ocExtensions*') ? 'active' : '' }}">
    <a href="{!! route('ocExtensions.index') !!}"><i class="fa fa-edit"></i><span>extensions</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Filters</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocFilters*') ? 'active' : '' }}">
            <a href="{!! route('ocFilters.index') !!}"><i class="fa fa-edit"></i><span>filters</span></a>
        </li>

        <li class="{{ Request::is('ocFilterDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocFilterDescriptions.index') !!}"><i class="fa fa-edit"></i><span>filter descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocGeoZones*') ? 'active' : '' }}">
    <a href="{!! route('ocGeoZones.index') !!}"><i class="fa fa-edit"></i><span>geo zones</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Informations</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocInformations*') ? 'active' : '' }}">
            <a href="{!! route('ocInformations.index') !!}"><i class="fa fa-edit"></i><span>informations</span></a>
        </li>

        <li class="{{ Request::is('ocInformationDescriptions*') ? 'active' : '' }}">
            <a href="{!! route('ocInformationDescriptions.index') !!}"><i class="fa fa-edit"></i><span>information descriptions</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocLanguages*') ? 'active' : '' }}">
    <a href="{!! route('ocLanguages.index') !!}"><i class="fa fa-edit"></i><span>languages</span></a>
</li>

<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-dashboard"></i> <span>Layout Modules</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="{{ Request::is('ocLayoutModules*') ? 'active' : '' }}">
            <a href="{!! route('ocLayoutModules.index') !!}"><i class="fa fa-edit"></i><span>layout modules</span></a>
        </li>

        <li class="{{ Request::is('ocLayoutRoutes*') ? 'active' : '' }}">
            <a href="{!! route('ocLayoutRoutes.index') !!}"><i class="fa fa-edit"></i><span>layout routes</span></a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('ocLengthClassDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocLengthClassDescriptions.index') !!}"><i class="fa fa-edit"></i><span>length class descriptions</span></a>
</li>

<li class="{{ Request::is('ocLocations*') ? 'active' : '' }}">
    <a href="{!! route('ocLocations.index') !!}"><i class="fa fa-edit"></i><span>locations</span></a>
</li>

<li class="{{ Request::is('ocManufacturers*') ? 'active' : '' }}">
    <a href="{!! route('ocManufacturers.index') !!}"><i class="fa fa-edit"></i><span>manufacturers</span></a>
</li>

<li class="{{ Request::is('ocMarketings*') ? 'active' : '' }}">
    <a href="{!! route('ocMarketings.index') !!}"><i class="fa fa-edit"></i><span>marketings</span></a>
</li>

<li class="{{ Request::is('ocModifications*') ? 'active' : '' }}">
    <a href="{!! route('ocModifications.index') !!}"><i class="fa fa-edit"></i><span>modifications</span></a>
</li>

<li class="{{ Request::is('ocModules*') ? 'active' : '' }}">
    <a href="{!! route('ocModules.index') !!}"><i class="fa fa-edit"></i><span>modules</span></a>
</li>

<li class="{{ Request::is('ocOptionValues*') ? 'active' : '' }}">
    <a href="{!! route('ocOptionValues.index') !!}"><i class="fa fa-edit"></i><span>option values</span></a>
</li>

<li class="{{ Request::is('ocOptionValueDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocOptionValueDescriptions.index') !!}"><i class="fa fa-edit"></i><span>option value descriptions</span></a>
</li>

<li class="{{ Request::is('ocOrders*') ? 'active' : '' }}">
    <a href="{!! route('ocOrders.index') !!}"><i class="fa fa-edit"></i><span>orders</span></a>
</li>

<li class="{{ Request::is('ocOrderCustomFields*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderCustomFields.index') !!}"><i class="fa fa-edit"></i><span>order custom fields</span></a>
</li>

<li class="{{ Request::is('ocOrderHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderHistories.index') !!}"><i class="fa fa-edit"></i><span>order histories</span></a>
</li>

<li class="{{ Request::is('ocOrderOptions*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderOptions.index') !!}"><i class="fa fa-edit"></i><span>order options</span></a>
</li>

<li class="{{ Request::is('ocOrderProducts*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderProducts.index') !!}"><i class="fa fa-edit"></i><span>order products</span></a>
</li>

<li class="{{ Request::is('ocOrderRecurrings*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderRecurrings.index') !!}"><i class="fa fa-edit"></i><span>order recurrings</span></a>
</li>

<li class="{{ Request::is('ocOrderRecurringTransactions*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderRecurringTransactions.index') !!}"><i class="fa fa-edit"></i><span>order recurring transactions</span></a>
</li>

<li class="{{ Request::is('ocOrderTotals*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderTotals.index') !!}"><i class="fa fa-edit"></i><span>order totals</span></a>
</li>

<li class="{{ Request::is('ocOrderVouchers*') ? 'active' : '' }}">
    <a href="{!! route('ocOrderVouchers.index') !!}"><i class="fa fa-edit"></i><span>order vouchers</span></a>
</li>

<li class="{{ Request::is('ocRecurrings*') ? 'active' : '' }}">
    <a href="{!! route('ocRecurrings.index') !!}"><i class="fa fa-edit"></i><span>recurrings</span></a>
</li>

<li class="{{ Request::is('ocReturns*') ? 'active' : '' }}">
    <a href="{!! route('ocReturns.index') !!}"><i class="fa fa-edit"></i><span>returns</span></a>
</li>

<li class="{{ Request::is('ocReturnHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocReturnHistories.index') !!}"><i class="fa fa-edit"></i><span>return histories</span></a>
</li>

<li class="{{ Request::is('ocReviews*') ? 'active' : '' }}">
    <a href="{!! route('ocReviews.index') !!}"><i class="fa fa-edit"></i><span>reviews</span></a>
</li>

<li class="{{ Request::is('ocSettings*') ? 'active' : '' }}">
    <a href="{!! route('ocSettings.index') !!}"><i class="fa fa-edit"></i><span>settings</span></a>
</li>

<li class="{{ Request::is('ocStores*') ? 'active' : '' }}">
    <a href="{!! route('ocStores.index') !!}"><i class="fa fa-edit"></i><span>stores</span></a>
</li>

<li class="{{ Request::is('ocTaxClasses*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxClasses.index') !!}"><i class="fa fa-edit"></i><span>tax classes</span></a>
</li>

<li class="{{ Request::is('ocTaxRates*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxRates.index') !!}"><i class="fa fa-edit"></i><span>tax rates</span></a>
</li>

<li class="{{ Request::is('ocTaxRules*') ? 'active' : '' }}">
    <a href="{!! route('ocTaxRules.index') !!}"><i class="fa fa-edit"></i><span>tax rules</span></a>
</li>

<li class="{{ Request::is('ocUploads*') ? 'active' : '' }}">
    <a href="{!! route('ocUploads.index') !!}"><i class="fa fa-edit"></i><span>uploads</span></a>
</li>

<li class="{{ Request::is('ocUsers*') ? 'active' : '' }}">
    <a href="{!! route('ocUsers.index') !!}"><i class="fa fa-edit"></i><span>users</span></a>
</li>

<li class="{{ Request::is('ocUserGroups*') ? 'active' : '' }}">
    <a href="{!! route('ocUserGroups.index') !!}"><i class="fa fa-edit"></i><span>user groups</span></a>
</li>

<li class="{{ Request::is('ocVouchers*') ? 'active' : '' }}">
    <a href="{!! route('ocVouchers.index') !!}"><i class="fa fa-edit"></i><span>vouchers</span></a>
</li>

<li class="{{ Request::is('ocVoucherHistories*') ? 'active' : '' }}">
    <a href="{!! route('ocVoucherHistories.index') !!}"><i class="fa fa-edit"></i><span>voucher histories</span></a>
</li>

<li class="{{ Request::is('ocWeightClassDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocWeightClassDescriptions.index') !!}"><i class="fa fa-edit"></i><span>weight class descriptions</span></a>
</li>

<li class="{{ Request::is('ocZones*') ? 'active' : '' }}">
    <a href="{!! route('ocZones.index') !!}"><i class="fa fa-edit"></i><span>zones</span></a>
</li>

<li class="{{ Request::is('ocZoneToGeoZones*') ? 'active' : '' }}">
    <a href="{!! route('ocZoneToGeoZones.index') !!}"><i class="fa fa-edit"></i><span>zone to geo zones</span></a>
</li>