<li class="{{ Request::is('home') ? 'active' : '' }}">
    <a href="{!! route('home') !!}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
@php 
$catalog = Request::is('ocProducts*') ? 'active' : Request::is('ocProductAttributes*') ? 'active' : Request::is('ocProductDescriptions*') ? 'active' : Request::is('ocProductDiscounts*') ? 'active' : Request::is('ocProductImages*') ? 'active' : Request::is('ocProductOptions*') ? 'active' : Request::is('ocProductOptionValues*') ? 'active' : Request::is('ocProductRewards*') ? 'active' : Request::is('ocProductSpecials*') ? 'active' : Request::is('ocProductToCategories*') ? 'active' : 
Request::is('ocProductToDownloads*') ? 'active' : Request::is('ocProductToLayouts*') ? 'active' : 
Request::is('ocProductToStores*') ? 'active' : Request::is('ocCategories*') ? 'active' : Request::is('ocCategoryDescriptions*') ? 'active' : Request::is('ocCategoryFilters*') ? 'active' : Request::is('ocCategoryPaths*') ? 'active' : Request::is('ocCategoryToLayouts*') ? 'active' : Request::is('ocCategoryToStores*') ? 'active' : ''; 
@endphp
<li class="{{ $catalog }} treeview menu-open">
    <a href="#">
        <i class="fa fa-tags"></i> <span>Catalog</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
	@include('layouts.menu.products.menu')
	@include('layouts.menu.categories.menu')
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-puzzle-piece"></i> <span>Extension</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-television"></i> <span>Design</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
@php 
$sales = Request::is('ocOrders*') ? 'active' : Request::is('ocOrderCustomFields*') ? 'active' : Request::is('ocOrderHistories*') ? 'active' : Request::is('ocOrderOptions*') ? 'active' : Request::is('ocOrderProducts*') ? 'active' : Request::is('ocOrderRecurrings*') ? 'active' : Request::is('ocOrderRecurringTransactions*') ? 'active' : Request::is('ocOrderTotals*') ? 'active' : Request::is('ocOrderVouchers*') ? 'active' : ''; 
@endphp
<li class="{{$sales}} treeview menu-open">
    <a href="#">
        <i class="fa fa-shopping-cart"></i> <span>Sales</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
	@include('layouts.menu.orders.menu')
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-user"></i> <span>Customers</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-share-alt"></i> <span>Marketing</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-cog"></i> <span>System</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>
<li class="treeview menu-open">
    <a href="#">
        <i class="fa fa-bar-chart-o"></i> <span>Reports</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
    </ul>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-table"></i><span>users</span></a>
</li>


<li class="{{ Request::is('ocCategoryDescriptions*') ? 'active' : '' }}">
    <a href="{!! route('ocCategoryDescriptions.index') !!}"><i class="fa fa-edit"></i><span>oc_category_descriptions</span></a>
</li>

{{-- @include('layouts.menu2') --}}

<li class="{{ Request::is('ocProductToCategories*') ? 'active' : '' }}">
    <a href="{!! route('ocProductToCategories.index') !!}"><i class="fa fa-edit"></i><span>oc_product_to_categories</span></a>
</li>

