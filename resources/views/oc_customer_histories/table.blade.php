<table class="table table-responsive" id="ocCustomerHistories-table">
    <thead>
        <th>Customer Id</th>
        <th>Comment</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerHistories as $ocCustomerHistory)
        <tr>
            <td>{!! $ocCustomerHistory->customer_id !!}</td>
            <td>{!! $ocCustomerHistory->comment !!}</td>
            <td>{!! $ocCustomerHistory->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerHistories.destroy', $ocCustomerHistory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerHistories.show', [$ocCustomerHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerHistories.edit', [$ocCustomerHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>