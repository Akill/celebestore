<table class="table table-responsive" id="ocProductDescriptions-table">
    <thead>
        <th>Product</th>
        <th>Language</th>
        <th>Name</th>
        <!-- <th>Description</th> -->
        <th>Tag</th>
        <th>Meta Title</th>
        <!-- <th>Meta Description</th> -->
        <!-- <th>Meta Keyword</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductDescriptions as $ocProductDescription)
        <tr>
            <td>{!! $ocProductDescription->product_id !!}</td>
            <td>{!! App\Models\oc_language::find($ocProductDescription->language_id)->name !!}</td>
            <td>{!! $ocProductDescription->name !!}</td>
            <!-- <td>{!! $ocProductDescription->description !!}</td> -->
            <td>{!! $ocProductDescription->tag !!}</td>
            <td>{!! $ocProductDescription->meta_title !!}</td>
            <!-- <td>{!! $ocProductDescription->meta_description !!}</td> -->
            <!-- <td>{!! $ocProductDescription->meta_keyword !!}</td> -->
            <td>
                {!! Form::open(['route' => ['ocProductDescriptions.destroy', $ocProductDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductDescriptions.show', [$ocProductDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductDescriptions.edit', [$ocProductDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>