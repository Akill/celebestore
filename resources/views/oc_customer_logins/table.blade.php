<table class="table table-responsive" id="ocCustomerLogins-table">
    <thead>
        <th>Email</th>
        <th>Ip</th>
        <th>Total</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerLogins as $ocCustomerLogin)
        <tr>
            <td>{!! $ocCustomerLogin->email !!}</td>
            <td>{!! $ocCustomerLogin->ip !!}</td>
            <td>{!! $ocCustomerLogin->total !!}</td>
            <td>{!! $ocCustomerLogin->date_added !!}</td>
            <td>{!! $ocCustomerLogin->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerLogins.destroy', $ocCustomerLogin->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerLogins.show', [$ocCustomerLogin->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerLogins.edit', [$ocCustomerLogin->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>