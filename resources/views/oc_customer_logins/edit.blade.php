@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Login
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomerLogin, ['route' => ['ocCustomerLogins.update', $ocCustomerLogin->id], 'method' => 'patch']) !!}

                        @include('oc_customer_logins.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection