<table class="table table-responsive" id="ocAffiliateActivities-table">
    <thead>
        <th>Affiliate Id</th>
        <th>Key</th>
        <th>Data</th>
        <th>Ip</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAffiliateActivities as $ocAffiliateActivity)
        <tr>
            <td>{!! $ocAffiliateActivity->affiliate_id !!}</td>
            <td>{!! $ocAffiliateActivity->key !!}</td>
            <td>{!! $ocAffiliateActivity->data !!}</td>
            <td>{!! $ocAffiliateActivity->ip !!}</td>
            <td>{!! $ocAffiliateActivity->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAffiliateActivities.destroy', $ocAffiliateActivity->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAffiliateActivities.show', [$ocAffiliateActivity->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAffiliateActivities.edit', [$ocAffiliateActivity->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>