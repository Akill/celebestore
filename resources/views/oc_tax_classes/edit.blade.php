@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Tax Class
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocTaxClass, ['route' => ['ocTaxClasses.update', $ocTaxClass->id], 'method' => 'patch']) !!}

                        @include('oc_tax_classes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection