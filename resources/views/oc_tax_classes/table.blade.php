<table class="table table-responsive" id="ocTaxClasses-table">
    <thead>
        <th>Title</th>
        <th>Description</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocTaxClasses as $ocTaxClass)
        <tr>
            <td>{!! $ocTaxClass->title !!}</td>
            <td>{!! $ocTaxClass->description !!}</td>
            <td>{!! $ocTaxClass->date_added !!}</td>
            <td>{!! $ocTaxClass->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocTaxClasses.destroy', $ocTaxClass->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocTaxClasses.show', [$ocTaxClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocTaxClasses.edit', [$ocTaxClass->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>