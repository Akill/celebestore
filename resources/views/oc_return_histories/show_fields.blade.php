<!-- Return History Id Field -->
<div class="form-group">
    {!! Form::label('return_history_id', 'Return History Id:') !!}
    <p>{!! $ocReturnHistory->return_history_id !!}</p>
</div>

<!-- Return Id Field -->
<div class="form-group">
    {!! Form::label('return_id', 'Return Id:') !!}
    <p>{!! $ocReturnHistory->return_id !!}</p>
</div>

<!-- Return Status Id Field -->
<div class="form-group">
    {!! Form::label('return_status_id', 'Return Status Id:') !!}
    <p>{!! $ocReturnHistory->return_status_id !!}</p>
</div>

<!-- Notify Field -->
<div class="form-group">
    {!! Form::label('notify', 'Notify:') !!}
    <p>{!! $ocReturnHistory->notify !!}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{!! $ocReturnHistory->comment !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocReturnHistory->date_added !!}</p>
</div>

