<table class="table table-responsive" id="ocReturnHistories-table">
    <thead>
        <th>Return Id</th>
        <th>Return Status Id</th>
        <th>Notify</th>
        <th>Comment</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocReturnHistories as $ocReturnHistory)
        <tr>
            <td>{!! $ocReturnHistory->return_id !!}</td>
            <td>{!! $ocReturnHistory->return_status_id !!}</td>
            <td>{!! $ocReturnHistory->notify !!}</td>
            <td>{!! $ocReturnHistory->comment !!}</td>
            <td>{!! $ocReturnHistory->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocReturnHistories.destroy', $ocReturnHistory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocReturnHistories.show', [$ocReturnHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocReturnHistories.edit', [$ocReturnHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>