@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Return History
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocReturnHistory, ['route' => ['ocReturnHistories.update', $ocReturnHistory->id], 'method' => 'patch']) !!}

                        @include('oc_return_histories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection