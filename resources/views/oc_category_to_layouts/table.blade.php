<table class="table table-responsive" id="ocCategoryToLayouts-table">
    <thead>
        <th>Store Id</th>
        <th>Layout Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCategoryToLayouts as $ocCategoryToLayout)
        <tr>
            <td>{!! $ocCategoryToLayout->store_id !!}</td>
            <td>{!! $ocCategoryToLayout->layout_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryToLayouts.destroy', $ocCategoryToLayout->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryToLayouts.show', [$ocCategoryToLayout->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryToLayouts.edit', [$ocCategoryToLayout->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>