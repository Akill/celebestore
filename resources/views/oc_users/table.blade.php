<table class="table table-responsive" id="ocUsers-table">
    <thead>
        <th>User Group Id</th>
        <th>Username</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Image</th>
        <th>Code</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocUsers as $ocUser)
        <tr>
            <td>{!! $ocUser->user_group_id !!}</td>
            <td>{!! $ocUser->username !!}</td>
            <td>{!! $ocUser->password !!}</td>
            <td>{!! $ocUser->salt !!}</td>
            <td>{!! $ocUser->firstname !!}</td>
            <td>{!! $ocUser->lastname !!}</td>
            <td>{!! $ocUser->email !!}</td>
            <td>{!! $ocUser->image !!}</td>
            <td>{!! $ocUser->code !!}</td>
            <td>{!! $ocUser->ip !!}</td>
            <td>{!! $ocUser->status !!}</td>
            <td>{!! $ocUser->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocUsers.destroy', $ocUser->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocUsers.show', [$ocUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocUsers.edit', [$ocUser->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>