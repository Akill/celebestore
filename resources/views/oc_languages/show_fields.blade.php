<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocLanguage->language_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocLanguage->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocLanguage->code !!}</p>
</div>

<!-- Locale Field -->
<div class="form-group">
    {!! Form::label('locale', 'Locale:') !!}
    <p>{!! $ocLanguage->locale !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocLanguage->image !!}</p>
</div>

<!-- Directory Field -->
<div class="form-group">
    {!! Form::label('directory', 'Directory:') !!}
    <p>{!! $ocLanguage->directory !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocLanguage->sort_order !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocLanguage->status !!}</p>
</div>

