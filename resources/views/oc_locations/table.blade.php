<table class="table table-responsive" id="ocLocations-table">
    <thead>
        <th>Name</th>
        <th>Address</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Geocode</th>
        <th>Image</th>
        <th>Open</th>
        <th>Comment</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocLocations as $ocLocation)
        <tr>
            <td>{!! $ocLocation->name !!}</td>
            <td>{!! $ocLocation->address !!}</td>
            <td>{!! $ocLocation->telephone !!}</td>
            <td>{!! $ocLocation->fax !!}</td>
            <td>{!! $ocLocation->geocode !!}</td>
            <td>{!! $ocLocation->image !!}</td>
            <td>{!! $ocLocation->open !!}</td>
            <td>{!! $ocLocation->comment !!}</td>
            <td>
                {!! Form::open(['route' => ['ocLocations.destroy', $ocLocation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocLocations.show', [$ocLocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocLocations.edit', [$ocLocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>