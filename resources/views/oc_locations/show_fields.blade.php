<!-- Location Id Field -->
<div class="form-group">
    {!! Form::label('location_id', 'Location Id:') !!}
    <p>{!! $ocLocation->location_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocLocation->name !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $ocLocation->address !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $ocLocation->telephone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $ocLocation->fax !!}</p>
</div>

<!-- Geocode Field -->
<div class="form-group">
    {!! Form::label('geocode', 'Geocode:') !!}
    <p>{!! $ocLocation->geocode !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocLocation->image !!}</p>
</div>

<!-- Open Field -->
<div class="form-group">
    {!! Form::label('open', 'Open:') !!}
    <p>{!! $ocLocation->open !!}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{!! $ocLocation->comment !!}</p>
</div>

