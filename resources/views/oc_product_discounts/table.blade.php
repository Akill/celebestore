<table class="table table-responsive" id="ocProductDiscounts-table">
    <thead>
        <th>Product</th>
        <th>Customer Group</th>
        <th>Quantity</th>
        <th>Priority</th>
        <th>Price</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductDiscounts as $ocProductDiscount)
        <tr>
            <td>{!! App\Models\oc_product_description::where('product_id', $ocProductDiscount->product_id)->first()->name !!}</td>
            <td>{!! App\Models\oc_customer_group_description::where('customer_group_id', $ocProductDiscount->customer_group_id)->first()->name !!}</td>
            <td>{!! $ocProductDiscount->quantity !!}</td>
            <td>{!! $ocProductDiscount->priority !!}</td>
            <td>{!! $ocProductDiscount->price !!}</td>
            <td>{!! $ocProductDiscount->date_start !!}</td>
            <td>{!! $ocProductDiscount->date_end !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductDiscounts.destroy', $ocProductDiscount->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductDiscounts.show', [$ocProductDiscount->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductDiscounts.edit', [$ocProductDiscount->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>