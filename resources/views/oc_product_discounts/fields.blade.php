<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product:') !!}
    @php $items = App\Models\oc_product::has('detail')->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
</div>

<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    @php $items = App\Models\oc_customer_group_description::all(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ $i->name }}</option>
    @endforeach
    </select>
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::number('priority', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Date Start:') !!}
    {!! Form::date('date_start', null, ['class' => 'form-control']) !!}
</div>

<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'Date End:') !!}
    {!! Form::date('date_end', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductDiscounts.index') !!}" class="btn btn-default">Cancel</a>
</div>
