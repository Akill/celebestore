<table class="table table-responsive" id="ocCustomers-table">
    <thead>
        <th>Customer Group Id</th>
        <th>Store Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Cart</th>
        <th>Wishlist</th>
        <th>Newsletter</th>
        <th>Address Id</th>
        <th>Custom Field</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Approved</th>
        <th>Safe</th>
        <th>Token</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomers as $ocCustomer)
        <tr>
            <td>{!! $ocCustomer->customer_group_id !!}</td>
            <td>{!! $ocCustomer->store_id !!}</td>
            <td>{!! $ocCustomer->firstname !!}</td>
            <td>{!! $ocCustomer->lastname !!}</td>
            <td>{!! $ocCustomer->email !!}</td>
            <td>{!! $ocCustomer->telephone !!}</td>
            <td>{!! $ocCustomer->fax !!}</td>
            <td>{!! $ocCustomer->password !!}</td>
            <td>{!! $ocCustomer->salt !!}</td>
            <td>{!! $ocCustomer->cart !!}</td>
            <td>{!! $ocCustomer->wishlist !!}</td>
            <td>{!! $ocCustomer->newsletter !!}</td>
            <td>{!! $ocCustomer->address_id !!}</td>
            <td>{!! $ocCustomer->custom_field !!}</td>
            <td>{!! $ocCustomer->ip !!}</td>
            <td>{!! $ocCustomer->status !!}</td>
            <td>{!! $ocCustomer->approved !!}</td>
            <td>{!! $ocCustomer->safe !!}</td>
            <td>{!! $ocCustomer->token !!}</td>
            <td>{!! $ocCustomer->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomers.destroy', $ocCustomer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomers.show', [$ocCustomer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomers.edit', [$ocCustomer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>