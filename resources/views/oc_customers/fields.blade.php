<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    {!! Form::number('customer_group_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Store Id:') !!}
    {!! Form::number('store_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', 'Firstname:') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Lastname:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telephone', 'Telephone:') !!}
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax', 'Fax:') !!}
    {!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Salt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('salt', 'Salt:') !!}
    {!! Form::text('salt', null, ['class' => 'form-control']) !!}
</div>

<!-- Cart Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('cart', 'Cart:') !!}
    {!! Form::textarea('cart', null, ['class' => 'form-control']) !!}
</div>

<!-- Wishlist Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('wishlist', 'Wishlist:') !!}
    {!! Form::textarea('wishlist', null, ['class' => 'form-control']) !!}
</div>

<!-- Newsletter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('newsletter', 'Newsletter:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('newsletter', false) !!}
        {!! Form::checkbox('newsletter', '1', null) !!} 1
    </label>
</div>

<!-- Address Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_id', 'Address Id:') !!}
    {!! Form::number('address_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    {!! Form::textarea('custom_field', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip', 'Ip:') !!}
    {!! Form::text('ip', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Approved Field -->
<div class="form-group col-sm-6">
    {!! Form::label('approved', 'Approved:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('approved', false) !!}
        {!! Form::checkbox('approved', '1', null) !!} 1
    </label>
</div>

<!-- Safe Field -->
<div class="form-group col-sm-6">
    {!! Form::label('safe', 'Safe:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('safe', false) !!}
        {!! Form::checkbox('safe', '1', null) !!} 1
    </label>
</div>

<!-- Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('token', 'Token:') !!}
    {!! Form::text('token', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCustomers.index') !!}" class="btn btn-default">Cancel</a>
</div>
