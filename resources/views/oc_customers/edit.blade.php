@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomer, ['route' => ['ocCustomers.update', $ocCustomer->id], 'method' => 'patch']) !!}

                        @include('oc_customers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection