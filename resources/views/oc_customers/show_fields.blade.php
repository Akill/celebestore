<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCustomer->id !!}</p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    <p>{!! $ocCustomer->customer_group_id !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $ocCustomer->store_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $ocCustomer->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $ocCustomer->lastname !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocCustomer->email !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $ocCustomer->telephone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $ocCustomer->fax !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $ocCustomer->password !!}</p>
</div>

<!-- Salt Field -->
<div class="form-group">
    {!! Form::label('salt', 'Salt:') !!}
    <p>{!! $ocCustomer->salt !!}</p>
</div>

<!-- Cart Field -->
<div class="form-group">
    {!! Form::label('cart', 'Cart:') !!}
    <p>{!! $ocCustomer->cart !!}</p>
</div>

<!-- Wishlist Field -->
<div class="form-group">
    {!! Form::label('wishlist', 'Wishlist:') !!}
    <p>{!! $ocCustomer->wishlist !!}</p>
</div>

<!-- Newsletter Field -->
<div class="form-group">
    {!! Form::label('newsletter', 'Newsletter:') !!}
    <p>{!! $ocCustomer->newsletter !!}</p>
</div>

<!-- Address Id Field -->
<div class="form-group">
    {!! Form::label('address_id', 'Address Id:') !!}
    <p>{!! $ocCustomer->address_id !!}</p>
</div>

<!-- Custom Field Field -->
<div class="form-group">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    <p>{!! $ocCustomer->custom_field !!}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'Ip:') !!}
    <p>{!! $ocCustomer->ip !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCustomer->status !!}</p>
</div>

<!-- Approved Field -->
<div class="form-group">
    {!! Form::label('approved', 'Approved:') !!}
    <p>{!! $ocCustomer->approved !!}</p>
</div>

<!-- Safe Field -->
<div class="form-group">
    {!! Form::label('safe', 'Safe:') !!}
    <p>{!! $ocCustomer->safe !!}</p>
</div>

<!-- Token Field -->
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    <p>{!! $ocCustomer->token !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocCustomer->date_added !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCustomer->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCustomer->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCustomer->deleted_at !!}</p>
</div>

