<!-- Sub District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_district_id', 'Sub District Id:') !!}
    {!! Form::number('sub_district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', 'Firstname:') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Lastname:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telephone', 'Telephone:') !!}
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax', 'Fax:') !!}
    {!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

<!-- Salt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('salt', 'Salt:') !!}
    {!! Form::text('salt', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company', 'Company:') !!}
    {!! Form::text('company', null, ['class' => 'form-control']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_1', 'Address 1:') !!}
    {!! Form::text('address_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_2', 'Address 2:') !!}
    {!! Form::text('address_2', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode', 'Postcode:') !!}
    {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country Id:') !!}
    {!! Form::number('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    {!! Form::number('zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', 'Commission:') !!}
    {!! Form::number('commission', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::text('tax', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment', 'Payment:') !!}
    {!! Form::text('payment', null, ['class' => 'form-control']) !!}
</div>

<!-- Cheque Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cheque', 'Cheque:') !!}
    {!! Form::text('cheque', null, ['class' => 'form-control']) !!}
</div>

<!-- Paypal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('paypal', 'Paypal:') !!}
    {!! Form::text('paypal', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_name', 'Bank Name:') !!}
    {!! Form::text('bank_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Branch Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_branch_number', 'Bank Branch Number:') !!}
    {!! Form::text('bank_branch_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Swift Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_swift_code', 'Bank Swift Code:') !!}
    {!! Form::text('bank_swift_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_account_name', 'Bank Account Name:') !!}
    {!! Form::text('bank_account_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Bank Account Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_account_number', 'Bank Account Number:') !!}
    {!! Form::text('bank_account_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip', 'Ip:') !!}
    {!! Form::text('ip', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Approved Field -->
<div class="form-group col-sm-6">
    {!! Form::label('approved', 'Approved:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('approved', false) !!}
        {!! Form::checkbox('approved', '1', null) !!} 1
    </label>
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocAffiliates.index') !!}" class="btn btn-default">Cancel</a>
</div>
