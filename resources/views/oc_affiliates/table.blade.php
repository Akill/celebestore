<table class="table table-responsive" id="ocAffiliates-table">
    <thead>
        <th>Sub District Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Password</th>
        <th>Salt</th>
        <th>Company</th>
        <th>Website</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>Postcode</th>
        <th>Country Id</th>
        <th>Zone Id</th>
        <th>Code</th>
        <th>Commission</th>
        <th>Tax</th>
        <th>Payment</th>
        <th>Cheque</th>
        <th>Paypal</th>
        <th>Bank Name</th>
        <th>Bank Branch Number</th>
        <th>Bank Swift Code</th>
        <th>Bank Account Name</th>
        <th>Bank Account Number</th>
        <th>Ip</th>
        <th>Status</th>
        <th>Approved</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAffiliates as $ocAffiliate)
        <tr>
            <td>{!! $ocAffiliate->sub_district_id !!}</td>
            <td>{!! $ocAffiliate->firstname !!}</td>
            <td>{!! $ocAffiliate->lastname !!}</td>
            <td>{!! $ocAffiliate->email !!}</td>
            <td>{!! $ocAffiliate->telephone !!}</td>
            <td>{!! $ocAffiliate->fax !!}</td>
            <td>{!! $ocAffiliate->password !!}</td>
            <td>{!! $ocAffiliate->salt !!}</td>
            <td>{!! $ocAffiliate->company !!}</td>
            <td>{!! $ocAffiliate->website !!}</td>
            <td>{!! $ocAffiliate->address_1 !!}</td>
            <td>{!! $ocAffiliate->address_2 !!}</td>
            <td>{!! $ocAffiliate->city !!}</td>
            <td>{!! $ocAffiliate->postcode !!}</td>
            <td>{!! $ocAffiliate->country_id !!}</td>
            <td>{!! $ocAffiliate->zone_id !!}</td>
            <td>{!! $ocAffiliate->code !!}</td>
            <td>{!! $ocAffiliate->commission !!}</td>
            <td>{!! $ocAffiliate->tax !!}</td>
            <td>{!! $ocAffiliate->payment !!}</td>
            <td>{!! $ocAffiliate->cheque !!}</td>
            <td>{!! $ocAffiliate->paypal !!}</td>
            <td>{!! $ocAffiliate->bank_name !!}</td>
            <td>{!! $ocAffiliate->bank_branch_number !!}</td>
            <td>{!! $ocAffiliate->bank_swift_code !!}</td>
            <td>{!! $ocAffiliate->bank_account_name !!}</td>
            <td>{!! $ocAffiliate->bank_account_number !!}</td>
            <td>{!! $ocAffiliate->ip !!}</td>
            <td>{!! $ocAffiliate->status !!}</td>
            <td>{!! $ocAffiliate->approved !!}</td>
            <td>{!! $ocAffiliate->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAffiliates.destroy', $ocAffiliate->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAffiliates.show', [$ocAffiliate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAffiliates.edit', [$ocAffiliate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>