<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocAffiliate->id !!}</p>
</div>

<!-- Sub District Id Field -->
<div class="form-group">
    {!! Form::label('sub_district_id', 'Sub District Id:') !!}
    <p>{!! $ocAffiliate->sub_district_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $ocAffiliate->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $ocAffiliate->lastname !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocAffiliate->email !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $ocAffiliate->telephone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $ocAffiliate->fax !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $ocAffiliate->password !!}</p>
</div>

<!-- Salt Field -->
<div class="form-group">
    {!! Form::label('salt', 'Salt:') !!}
    <p>{!! $ocAffiliate->salt !!}</p>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $ocAffiliate->company !!}</p>
</div>

<!-- Website Field -->
<div class="form-group">
    {!! Form::label('website', 'Website:') !!}
    <p>{!! $ocAffiliate->website !!}</p>
</div>

<!-- Address 1 Field -->
<div class="form-group">
    {!! Form::label('address_1', 'Address 1:') !!}
    <p>{!! $ocAffiliate->address_1 !!}</p>
</div>

<!-- Address 2 Field -->
<div class="form-group">
    {!! Form::label('address_2', 'Address 2:') !!}
    <p>{!! $ocAffiliate->address_2 !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $ocAffiliate->city !!}</p>
</div>

<!-- Postcode Field -->
<div class="form-group">
    {!! Form::label('postcode', 'Postcode:') !!}
    <p>{!! $ocAffiliate->postcode !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $ocAffiliate->country_id !!}</p>
</div>

<!-- Zone Id Field -->
<div class="form-group">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    <p>{!! $ocAffiliate->zone_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocAffiliate->code !!}</p>
</div>

<!-- Commission Field -->
<div class="form-group">
    {!! Form::label('commission', 'Commission:') !!}
    <p>{!! $ocAffiliate->commission !!}</p>
</div>

<!-- Tax Field -->
<div class="form-group">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{!! $ocAffiliate->tax !!}</p>
</div>

<!-- Payment Field -->
<div class="form-group">
    {!! Form::label('payment', 'Payment:') !!}
    <p>{!! $ocAffiliate->payment !!}</p>
</div>

<!-- Cheque Field -->
<div class="form-group">
    {!! Form::label('cheque', 'Cheque:') !!}
    <p>{!! $ocAffiliate->cheque !!}</p>
</div>

<!-- Paypal Field -->
<div class="form-group">
    {!! Form::label('paypal', 'Paypal:') !!}
    <p>{!! $ocAffiliate->paypal !!}</p>
</div>

<!-- Bank Name Field -->
<div class="form-group">
    {!! Form::label('bank_name', 'Bank Name:') !!}
    <p>{!! $ocAffiliate->bank_name !!}</p>
</div>

<!-- Bank Branch Number Field -->
<div class="form-group">
    {!! Form::label('bank_branch_number', 'Bank Branch Number:') !!}
    <p>{!! $ocAffiliate->bank_branch_number !!}</p>
</div>

<!-- Bank Swift Code Field -->
<div class="form-group">
    {!! Form::label('bank_swift_code', 'Bank Swift Code:') !!}
    <p>{!! $ocAffiliate->bank_swift_code !!}</p>
</div>

<!-- Bank Account Name Field -->
<div class="form-group">
    {!! Form::label('bank_account_name', 'Bank Account Name:') !!}
    <p>{!! $ocAffiliate->bank_account_name !!}</p>
</div>

<!-- Bank Account Number Field -->
<div class="form-group">
    {!! Form::label('bank_account_number', 'Bank Account Number:') !!}
    <p>{!! $ocAffiliate->bank_account_number !!}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'Ip:') !!}
    <p>{!! $ocAffiliate->ip !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocAffiliate->status !!}</p>
</div>

<!-- Approved Field -->
<div class="form-group">
    {!! Form::label('approved', 'Approved:') !!}
    <p>{!! $ocAffiliate->approved !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocAffiliate->date_added !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocAffiliate->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocAffiliate->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocAffiliate->deleted_at !!}</p>
</div>

