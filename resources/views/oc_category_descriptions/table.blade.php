<table class="table table-responsive" id="ocCategoryDescriptions-table">
    <thead>
        <th>Oc Category Id</th>
        <th>Language Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Meta Title</th>
        <th>Meta Description</th>
        <th>Meta Keyword</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCategoryDescriptions as $ocCategoryDescription)
        <tr>
            <td>{!! $ocCategoryDescription->oc_category_id !!}</td>
            <td>{!! $ocCategoryDescription->language_id !!}</td>
            <td>{!! $ocCategoryDescription->name !!}</td>
            <td>{!! $ocCategoryDescription->description !!}</td>
            <td>{!! $ocCategoryDescription->meta_title !!}</td>
            <td>{!! $ocCategoryDescription->meta_description !!}</td>
            <td>{!! $ocCategoryDescription->meta_keyword !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryDescriptions.destroy', $ocCategoryDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryDescriptions.show', [$ocCategoryDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryDescriptions.edit', [$ocCategoryDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>