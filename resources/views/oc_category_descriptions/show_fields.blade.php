<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCategoryDescription->id !!}</p>
</div>

<!-- Oc Category Id Field -->
<div class="form-group">
    {!! Form::label('oc_category_id', 'Oc Category Id:') !!}
    <p>{!! $ocCategoryDescription->oc_category_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocCategoryDescription->language_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocCategoryDescription->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ocCategoryDescription->description !!}</p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    {!! Form::label('meta_title', 'Meta Title:') !!}
    <p>{!! $ocCategoryDescription->meta_title !!}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description:') !!}
    <p>{!! $ocCategoryDescription->meta_description !!}</p>
</div>

<!-- Meta Keyword Field -->
<div class="form-group">
    {!! Form::label('meta_keyword', 'Meta Keyword:') !!}
    <p>{!! $ocCategoryDescription->meta_keyword !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCategoryDescription->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCategoryDescription->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCategoryDescription->deleted_at !!}</p>
</div>

