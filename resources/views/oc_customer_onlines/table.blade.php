<table class="table table-responsive" id="ocCustomerOnlines-table">
    <thead>
        <th>Customer Id</th>
        <th>Url</th>
        <th>Referer</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerOnlines as $ocCustomerOnline)
        <tr>
            <td>{!! $ocCustomerOnline->customer_id !!}</td>
            <td>{!! $ocCustomerOnline->url !!}</td>
            <td>{!! $ocCustomerOnline->referer !!}</td>
            <td>{!! $ocCustomerOnline->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerOnlines.destroy', $ocCustomerOnline->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerOnlines.show', [$ocCustomerOnline->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerOnlines.edit', [$ocCustomerOnline->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>