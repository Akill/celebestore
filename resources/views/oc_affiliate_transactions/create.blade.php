@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Affiliate Transaction
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocAffiliateTransactions.store']) !!}

                        @include('oc_affiliate_transactions.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
