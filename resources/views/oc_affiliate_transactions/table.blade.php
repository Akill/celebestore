<table class="table table-responsive" id="ocAffiliateTransactions-table">
    <thead>
        <th>Affiliate Id</th>
        <th>Order Id</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAffiliateTransactions as $ocAffiliateTransaction)
        <tr>
            <td>{!! $ocAffiliateTransaction->affiliate_id !!}</td>
            <td>{!! $ocAffiliateTransaction->order_id !!}</td>
            <td>{!! $ocAffiliateTransaction->description !!}</td>
            <td>{!! $ocAffiliateTransaction->amount !!}</td>
            <td>{!! $ocAffiliateTransaction->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAffiliateTransactions.destroy', $ocAffiliateTransaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAffiliateTransactions.show', [$ocAffiliateTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAffiliateTransactions.edit', [$ocAffiliateTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>