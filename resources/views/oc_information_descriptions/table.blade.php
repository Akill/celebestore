<table class="table table-responsive" id="ocInformationDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Meta Title</th>
        <th>Meta Description</th>
        <th>Meta Keyword</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocInformationDescriptions as $ocInformationDescription)
        <tr>
            <td>{!! $ocInformationDescription->language_id !!}</td>
            <td>{!! $ocInformationDescription->title !!}</td>
            <td>{!! $ocInformationDescription->description !!}</td>
            <td>{!! $ocInformationDescription->meta_title !!}</td>
            <td>{!! $ocInformationDescription->meta_description !!}</td>
            <td>{!! $ocInformationDescription->meta_keyword !!}</td>
            <td>
                {!! Form::open(['route' => ['ocInformationDescriptions.destroy', $ocInformationDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocInformationDescriptions.show', [$ocInformationDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocInformationDescriptions.edit', [$ocInformationDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>