@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Information Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocInformationDescription, ['route' => ['ocInformationDescriptions.update', $ocInformationDescription->id], 'method' => 'patch']) !!}

                        @include('oc_information_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection