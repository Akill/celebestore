<!-- Information Id Field -->
<div class="form-group">
    {!! Form::label('information_id', 'Information Id:') !!}
    <p>{!! $ocInformationDescription->information_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocInformationDescription->language_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ocInformationDescription->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ocInformationDescription->description !!}</p>
</div>

<!-- Meta Title Field -->
<div class="form-group">
    {!! Form::label('meta_title', 'Meta Title:') !!}
    <p>{!! $ocInformationDescription->meta_title !!}</p>
</div>

<!-- Meta Description Field -->
<div class="form-group">
    {!! Form::label('meta_description', 'Meta Description:') !!}
    <p>{!! $ocInformationDescription->meta_description !!}</p>
</div>

<!-- Meta Keyword Field -->
<div class="form-group">
    {!! Form::label('meta_keyword', 'Meta Keyword:') !!}
    <p>{!! $ocInformationDescription->meta_keyword !!}</p>
</div>

