@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Banner Image
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocBannerImage, ['route' => ['ocBannerImages.update', $ocBannerImage->id], 'method' => 'patch']) !!}

                        @include('oc_banner_images.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection