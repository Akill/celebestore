<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_product_id', 'Order Product Id:') !!}
    {!! Form::number('order_product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_option_id', 'Product Option Id:') !!}
    {!! Form::number('product_option_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Option Value Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_option_value_id', 'Product Option Value Id:') !!}
    {!! Form::number('product_option_value_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocOrderOptions.index') !!}" class="btn btn-default">Cancel</a>
</div>
