<table class="table table-responsive" id="ocOrderOptions-table">
    <thead>
        <th>Order Id</th>
        <th>Order Product Id</th>
        <th>Product Option Id</th>
        <th>Product Option Value Id</th>
        <th>Name</th>
        <th>Value</th>
        <th>Type</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderOptions as $ocOrderOption)
        <tr>
            <td>{!! $ocOrderOption->order_id !!}</td>
            <td>{!! $ocOrderOption->order_product_id !!}</td>
            <td>{!! $ocOrderOption->product_option_id !!}</td>
            <td>{!! $ocOrderOption->product_option_value_id !!}</td>
            <td>{!! $ocOrderOption->name !!}</td>
            <td>{!! $ocOrderOption->value !!}</td>
            <td>{!! $ocOrderOption->type !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderOptions.destroy', $ocOrderOption->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderOptions.show', [$ocOrderOption->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderOptions.edit', [$ocOrderOption->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>