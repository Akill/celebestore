<!-- Order Option Id Field -->
<div class="form-group">
    {!! Form::label('order_option_id', 'Order Option Id:') !!}
    <p>{!! $ocOrderOption->order_option_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderOption->order_id !!}</p>
</div>

<!-- Order Product Id Field -->
<div class="form-group">
    {!! Form::label('order_product_id', 'Order Product Id:') !!}
    <p>{!! $ocOrderOption->order_product_id !!}</p>
</div>

<!-- Product Option Id Field -->
<div class="form-group">
    {!! Form::label('product_option_id', 'Product Option Id:') !!}
    <p>{!! $ocOrderOption->product_option_id !!}</p>
</div>

<!-- Product Option Value Id Field -->
<div class="form-group">
    {!! Form::label('product_option_value_id', 'Product Option Value Id:') !!}
    <p>{!! $ocOrderOption->product_option_value_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocOrderOption->name !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocOrderOption->value !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $ocOrderOption->type !!}</p>
</div>

