@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Option
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOrderOption, ['route' => ['ocOrderOptions.update', $ocOrderOption->id], 'method' => 'patch']) !!}

                        @include('oc_order_options.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection