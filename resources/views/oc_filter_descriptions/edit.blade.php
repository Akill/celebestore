@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Filter Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocFilterDescription, ['route' => ['ocFilterDescriptions.update', $ocFilterDescription->id], 'method' => 'patch']) !!}

                        @include('oc_filter_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection