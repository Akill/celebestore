<table class="table table-responsive" id="ocFilterDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Filter Group Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocFilterDescriptions as $ocFilterDescription)
        <tr>
            <td>{!! $ocFilterDescription->language_id !!}</td>
            <td>{!! $ocFilterDescription->filter_group_id !!}</td>
            <td>{!! $ocFilterDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocFilterDescriptions.destroy', $ocFilterDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocFilterDescriptions.show', [$ocFilterDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocFilterDescriptions.edit', [$ocFilterDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>