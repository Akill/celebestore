<!-- Filter Id Field -->
<div class="form-group">
    {!! Form::label('filter_id', 'Filter Id:') !!}
    <p>{!! $ocFilterDescription->filter_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocFilterDescription->language_id !!}</p>
</div>

<!-- Filter Group Id Field -->
<div class="form-group">
    {!! Form::label('filter_group_id', 'Filter Group Id:') !!}
    <p>{!! $ocFilterDescription->filter_group_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocFilterDescription->name !!}</p>
</div>

