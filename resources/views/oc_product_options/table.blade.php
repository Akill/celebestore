<table class="table table-responsive" id="ocProductOptions-table">
    <thead>
        <th>Product</th>
        <th>Option</th>
        <th>Value</th>
        <th>Required</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductOptions as $ocProductOption)
        <tr>
            <td>{!! $ocProductOption->product_id !!}</td>
            <td>{!! $ocProductOption->option_id !!}</td>
            <td>{!! $ocProductOption->value !!}</td>
            <td>{!! $ocProductOption->required !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductOptions.destroy', $ocProductOption->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductOptions.show', [$ocProductOption->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductOptions.edit', [$ocProductOption->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>