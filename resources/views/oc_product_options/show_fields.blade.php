<!-- Product Option Id Field -->
<div class="form-group">
    {!! Form::label('product_option_id', 'Product Option Id:') !!}
    <p>{!! $ocProductOption->product_option_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocProductOption->product_id !!}</p>
</div>

<!-- Option Id Field -->
<div class="form-group">
    {!! Form::label('option_id', 'Option Id:') !!}
    <p>{!! $ocProductOption->option_id !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocProductOption->value !!}</p>
</div>

<!-- Required Field -->
<div class="form-group">
    {!! Form::label('required', 'Required:') !!}
    <p>{!! $ocProductOption->required !!}</p>
</div>

