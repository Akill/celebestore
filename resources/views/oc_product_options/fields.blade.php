<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product:') !!}
    @php $items = App\Models\oc_product::has('detail')->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
</div>

<!-- Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_id', 'Option Id:') !!}
    @php $items = App\Models\oc_option::has('detail')->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
    {!! Form::number('option_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::textarea('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Required Field -->
<div class="form-group col-sm-6">
    {!! Form::label('required', 'Required:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('required', false) !!}
        {!! Form::checkbox('required', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductOptions.index') !!}" class="btn btn-default">Cancel</a>
</div>
