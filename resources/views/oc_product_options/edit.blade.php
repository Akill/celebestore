@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product Option
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductOption, ['route' => ['ocProductOptions.update', $ocProductOption->id], 'method' => 'patch']) !!}

                        @include('oc_product_options.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection