<table class="table table-responsive" id="ocCustomFieldValues-table">
    <thead>
        <th>Custom Field Id</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomFieldValues as $ocCustomFieldValue)
        <tr>
            <td>{!! $ocCustomFieldValue->custom_field_id !!}</td>
            <td>{!! $ocCustomFieldValue->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomFieldValues.destroy', $ocCustomFieldValue->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomFieldValues.show', [$ocCustomFieldValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomFieldValues.edit', [$ocCustomFieldValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>