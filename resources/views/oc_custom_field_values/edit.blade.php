@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Custom Field Value
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomFieldValue, ['route' => ['ocCustomFieldValues.update', $ocCustomFieldValue->id], 'method' => 'patch']) !!}

                        @include('oc_custom_field_values.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection