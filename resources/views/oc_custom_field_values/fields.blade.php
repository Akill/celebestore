<!-- Custom Field Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('custom_field_id', 'Custom Field Id:') !!}
    {!! Form::number('custom_field_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCustomFieldValues.index') !!}" class="btn btn-default">Cancel</a>
</div>
