<table class="table table-responsive" id="ocAttributeDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAttributeDescriptions as $ocAttributeDescription)
        <tr>
            <td>{!! $ocAttributeDescription->language_id !!}</td>
            <td>{!! $ocAttributeDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAttributeDescriptions.destroy', $ocAttributeDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAttributeDescriptions.show', [$ocAttributeDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAttributeDescriptions.edit', [$ocAttributeDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>