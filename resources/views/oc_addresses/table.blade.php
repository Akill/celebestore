<table class="table table-responsive" id="ocAddresses-table">
    <thead>
        <!-- <th>Sub District Id</th> -->
        <!-- <th>Customer Id</th> -->
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Company</th>
        <th>Address 1</th>
        <th>Address 2</th>
        <th>City</th>
        <th>Postcode</th>
        <!-- <th>Country Id</th> -->
        <!-- <th>Zone Id</th> -->
        <!-- <th>Custom Field</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAddresses as $ocAddress)
        <tr>
            <!-- <td>{!! $ocAddress->sub_district_id !!}</td> -->
            <!-- <td>{!! $ocAddress->customer_id !!}</td> -->
            <td>{!! $ocAddress->firstname !!}</td>
            <td>{!! $ocAddress->lastname !!}</td>
            <td>{!! $ocAddress->company !!}</td>
            <td>{!! $ocAddress->address_1 !!}</td>
            <td>{!! $ocAddress->address_2 !!}</td>
            <td>{!! $ocAddress->city !!}</td>
            <td>{!! $ocAddress->postcode !!}</td>
            <!-- <td>{!! $ocAddress->country_id !!}</td> -->
            <!-- <td>{!! $ocAddress->zone_id !!}</td> -->
            <!-- <td>{!! $ocAddress->custom_field !!}</td> -->
            <td>
                {!! Form::open(['route' => ['ocAddresses.destroy', $ocAddress->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAddresses.show', [$ocAddress->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAddresses.edit', [$ocAddress->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>