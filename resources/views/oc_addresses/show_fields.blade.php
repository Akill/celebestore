<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocAddress->id !!}</p>
</div>

<!-- Sub District Id Field -->
<div class="form-group">
    {!! Form::label('sub_district_id', 'Sub District Id:') !!}
    <p>{!! $ocAddress->sub_district_id !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $ocAddress->customer_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $ocAddress->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $ocAddress->lastname !!}</p>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $ocAddress->company !!}</p>
</div>

<!-- Address 1 Field -->
<div class="form-group">
    {!! Form::label('address_1', 'Address 1:') !!}
    <p>{!! $ocAddress->address_1 !!}</p>
</div>

<!-- Address 2 Field -->
<div class="form-group">
    {!! Form::label('address_2', 'Address 2:') !!}
    <p>{!! $ocAddress->address_2 !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $ocAddress->city !!}</p>
</div>

<!-- Postcode Field -->
<div class="form-group">
    {!! Form::label('postcode', 'Postcode:') !!}
    <p>{!! $ocAddress->postcode !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $ocAddress->country_id !!}</p>
</div>

<!-- Zone Id Field -->
<div class="form-group">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    <p>{!! $ocAddress->zone_id !!}</p>
</div>

<!-- Custom Field Field -->
<div class="form-group">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    <p>{!! $ocAddress->custom_field !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocAddress->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocAddress->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocAddress->deleted_at !!}</p>
</div>

