@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Address
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocAddress, ['route' => ['ocAddresses.update', $ocAddress->id], 'method' => 'patch']) !!}

                        @include('oc_addresses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection