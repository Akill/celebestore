<!-- Sub District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_district_id', 'Sub District Id:') !!}
    {!! Form::number('sub_district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', 'Firstname:') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Lastname:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Company Field -->
<div class="form-group col-sm-6">
    {!! Form::label('company', 'Company:') !!}
    {!! Form::text('company', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_1', 'Address 1:') !!}
    {!! Form::text('address_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Address 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_2', 'Address 2:') !!}
    {!! Form::text('address_2', null, ['class' => 'form-control']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode', 'Postcode:') !!}
    {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country Id:') !!}
    {!! Form::number('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    {!! Form::number('zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    {!! Form::textarea('custom_field', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocAddresses.index') !!}" class="btn btn-default">Cancel</a>
</div>
