<!-- Option Value Id Field -->
<div class="form-group">
    {!! Form::label('option_value_id', 'Option Value Id:') !!}
    <p>{!! $ocOptionValueDescription->option_value_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocOptionValueDescription->language_id !!}</p>
</div>

<!-- Option Id Field -->
<div class="form-group">
    {!! Form::label('option_id', 'Option Id:') !!}
    <p>{!! $ocOptionValueDescription->option_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocOptionValueDescription->name !!}</p>
</div>

