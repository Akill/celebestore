@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Option Value Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOptionValueDescription, ['route' => ['ocOptionValueDescriptions.update', $ocOptionValueDescription->id], 'method' => 'patch']) !!}

                        @include('oc_option_value_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection