<table class="table table-responsive" id="ocOptionValueDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Option Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOptionValueDescriptions as $ocOptionValueDescription)
        <tr>
            <td>{!! $ocOptionValueDescription->language_id !!}</td>
            <td>{!! $ocOptionValueDescription->option_id !!}</td>
            <td>{!! $ocOptionValueDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOptionValueDescriptions.destroy', $ocOptionValueDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOptionValueDescriptions.show', [$ocOptionValueDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOptionValueDescriptions.edit', [$ocOptionValueDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>