<!-- Layout Route Id Field -->
<div class="form-group">
    {!! Form::label('layout_route_id', 'Layout Route Id:') !!}
    <p>{!! $ocLayoutRoute->layout_route_id !!}</p>
</div>

<!-- Layout Id Field -->
<div class="form-group">
    {!! Form::label('layout_id', 'Layout Id:') !!}
    <p>{!! $ocLayoutRoute->layout_id !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $ocLayoutRoute->store_id !!}</p>
</div>

<!-- Route Field -->
<div class="form-group">
    {!! Form::label('route', 'Route:') !!}
    <p>{!! $ocLayoutRoute->route !!}</p>
</div>

