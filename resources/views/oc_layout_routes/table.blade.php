<table class="table table-responsive" id="ocLayoutRoutes-table">
    <thead>
        <th>Layout Id</th>
        <th>Store Id</th>
        <th>Route</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocLayoutRoutes as $ocLayoutRoute)
        <tr>
            <td>{!! $ocLayoutRoute->layout_id !!}</td>
            <td>{!! $ocLayoutRoute->store_id !!}</td>
            <td>{!! $ocLayoutRoute->route !!}</td>
            <td>
                {!! Form::open(['route' => ['ocLayoutRoutes.destroy', $ocLayoutRoute->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocLayoutRoutes.show', [$ocLayoutRoute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocLayoutRoutes.edit', [$ocLayoutRoute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>