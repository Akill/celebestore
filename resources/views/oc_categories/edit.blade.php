@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Category
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCategory, ['route' => ['ocCategories.update', $ocCategory->id], 'method' => 'patch']) !!}

                        @include('oc_categories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection