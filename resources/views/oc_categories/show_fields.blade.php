<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCategory->id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocCategory->image !!}</p>
</div>

<!-- Parent Id Field -->
<div class="form-group">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    <p>{!! $ocCategory->parent_id !!}</p>
</div>

<!-- Top Field -->
<div class="form-group">
    {!! Form::label('top', 'Top:') !!}
    <p>{!! $ocCategory->top !!}</p>
</div>

<!-- Column Field -->
<div class="form-group">
    {!! Form::label('column', 'Column:') !!}
    <p>{!! $ocCategory->column !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocCategory->sort_order !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCategory->status !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocCategory->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocCategory->date_modified !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCategory->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCategory->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCategory->deleted_at !!}</p>
</div>

