<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Symbol Left Field -->
<div class="form-group col-sm-6">
    {!! Form::label('symbol_left', 'Symbol Left:') !!}
    {!! Form::text('symbol_left', null, ['class' => 'form-control']) !!}
</div>

<!-- Symbol Right Field -->
<div class="form-group col-sm-6">
    {!! Form::label('symbol_right', 'Symbol Right:') !!}
    {!! Form::text('symbol_right', null, ['class' => 'form-control']) !!}
</div>

<!-- Decimal Place Field -->
<div class="form-group col-sm-6">
    {!! Form::label('decimal_place', 'Decimal Place:') !!}
    {!! Form::text('decimal_place', null, ['class' => 'form-control']) !!}
</div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::number('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    {!! Form::date('date_modified', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCurrencies.index') !!}" class="btn btn-default">Cancel</a>
</div>
