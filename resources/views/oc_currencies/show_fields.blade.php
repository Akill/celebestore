<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCurrency->id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ocCurrency->title !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocCurrency->code !!}</p>
</div>

<!-- Symbol Left Field -->
<div class="form-group">
    {!! Form::label('symbol_left', 'Symbol Left:') !!}
    <p>{!! $ocCurrency->symbol_left !!}</p>
</div>

<!-- Symbol Right Field -->
<div class="form-group">
    {!! Form::label('symbol_right', 'Symbol Right:') !!}
    <p>{!! $ocCurrency->symbol_right !!}</p>
</div>

<!-- Decimal Place Field -->
<div class="form-group">
    {!! Form::label('decimal_place', 'Decimal Place:') !!}
    <p>{!! $ocCurrency->decimal_place !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocCurrency->value !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCurrency->status !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocCurrency->date_modified !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCurrency->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCurrency->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCurrency->deleted_at !!}</p>
</div>

