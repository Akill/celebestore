<table class="table table-responsive" id="ocCurrencies-table">
    <thead>
        <th>Title</th>
        <th>Code</th>
        <th>Symbol Left</th>
        <th>Symbol Right</th>
        <th>Decimal Place</th>
        <th>Value</th>
        <th>Status</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCurrencies as $ocCurrency)
        <tr>
            <td>{!! $ocCurrency->title !!}</td>
            <td>{!! $ocCurrency->code !!}</td>
            <td>{!! $ocCurrency->symbol_left !!}</td>
            <td>{!! $ocCurrency->symbol_right !!}</td>
            <td>{!! $ocCurrency->decimal_place !!}</td>
            <td>{!! $ocCurrency->value !!}</td>
            <td>{!! $ocCurrency->status !!}</td>
            <td>{!! $ocCurrency->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCurrencies.destroy', $ocCurrency->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCurrencies.show', [$ocCurrency->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCurrencies.edit', [$ocCurrency->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>