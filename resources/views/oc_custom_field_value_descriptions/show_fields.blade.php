<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCustomFieldValueDescription->id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocCustomFieldValueDescription->language_id !!}</p>
</div>

<!-- Custom Field Id Field -->
<div class="form-group">
    {!! Form::label('custom_field_id', 'Custom Field Id:') !!}
    <p>{!! $ocCustomFieldValueDescription->custom_field_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocCustomFieldValueDescription->name !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCustomFieldValueDescription->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCustomFieldValueDescription->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCustomFieldValueDescription->deleted_at !!}</p>
</div>

