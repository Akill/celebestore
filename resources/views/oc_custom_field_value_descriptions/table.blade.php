<table class="table table-responsive" id="ocCustomFieldValueDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Custom Field Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomFieldValueDescriptions as $ocCustomFieldValueDescription)
        <tr>
            <td>{!! $ocCustomFieldValueDescription->language_id !!}</td>
            <td>{!! $ocCustomFieldValueDescription->custom_field_id !!}</td>
            <td>{!! $ocCustomFieldValueDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomFieldValueDescriptions.destroy', $ocCustomFieldValueDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomFieldValueDescriptions.show', [$ocCustomFieldValueDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomFieldValueDescriptions.edit', [$ocCustomFieldValueDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>