@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Download Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocDownloadDescription, ['route' => ['ocDownloadDescriptions.update', $ocDownloadDescription->id], 'method' => 'patch']) !!}

                        @include('oc_download_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection