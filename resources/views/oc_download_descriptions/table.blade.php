<table class="table table-responsive" id="ocDownloadDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocDownloadDescriptions as $ocDownloadDescription)
        <tr>
            <td>{!! $ocDownloadDescription->language_id !!}</td>
            <td>{!! $ocDownloadDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocDownloadDescriptions.destroy', $ocDownloadDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocDownloadDescriptions.show', [$ocDownloadDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocDownloadDescriptions.edit', [$ocDownloadDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>