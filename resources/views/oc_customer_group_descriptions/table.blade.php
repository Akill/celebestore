<table class="table table-responsive" id="ocCustomerGroupDescriptions-table">
    <thead>
        <th>Customer Group Id</th>
        <th>Language Id</th>
        <th>Name</th>
        <th>Description</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerGroupDescriptions as $ocCustomerGroupDescription)
        <tr>
            <td>{!! $ocCustomerGroupDescription->customer_group_id !!}</td>
            <td>{!! $ocCustomerGroupDescription->language_id !!}</td>
            <td>{!! $ocCustomerGroupDescription->name !!}</td>
            <td>{!! $ocCustomerGroupDescription->description !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerGroupDescriptions.destroy', $ocCustomerGroupDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerGroupDescriptions.show', [$ocCustomerGroupDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerGroupDescriptions.edit', [$ocCustomerGroupDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>