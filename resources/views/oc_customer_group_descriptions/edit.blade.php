@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Group Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomerGroupDescription, ['route' => ['ocCustomerGroupDescriptions.update', $ocCustomerGroupDescription->id], 'method' => 'patch']) !!}

                        @include('oc_customer_group_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection