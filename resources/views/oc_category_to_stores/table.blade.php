<table class="table table-responsive" id="ocCategoryToStores-table">
    <thead>
        <th>Store Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCategoryToStores as $ocCategoryToStore)
        <tr>
            <td>{!! $ocCategoryToStore->store_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryToStores.destroy', $ocCategoryToStore->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryToStores.show', [$ocCategoryToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryToStores.edit', [$ocCategoryToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>