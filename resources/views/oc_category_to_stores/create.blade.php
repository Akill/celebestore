@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Category To Store
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocCategoryToStores.store']) !!}

                        @include('oc_category_to_stores.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
