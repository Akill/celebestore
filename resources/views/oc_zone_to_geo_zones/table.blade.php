<table class="table table-responsive" id="ocZoneToGeoZones-table">
    <thead>
        <th>Country Id</th>
        <th>Zone Id</th>
        <th>Geo Zone Id</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocZoneToGeoZones as $ocZoneToGeoZone)
        <tr>
            <td>{!! $ocZoneToGeoZone->country_id !!}</td>
            <td>{!! $ocZoneToGeoZone->zone_id !!}</td>
            <td>{!! $ocZoneToGeoZone->geo_zone_id !!}</td>
            <td>{!! $ocZoneToGeoZone->date_added !!}</td>
            <td>{!! $ocZoneToGeoZone->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocZoneToGeoZones.destroy', $ocZoneToGeoZone->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocZoneToGeoZones.show', [$ocZoneToGeoZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocZoneToGeoZones.edit', [$ocZoneToGeoZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>