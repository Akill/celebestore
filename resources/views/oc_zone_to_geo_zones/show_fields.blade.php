<!-- Zone To Geo Zone Id Field -->
<div class="form-group">
    {!! Form::label('zone_to_geo_zone_id', 'Zone To Geo Zone Id:') !!}
    <p>{!! $ocZoneToGeoZone->zone_to_geo_zone_id !!}</p>
</div>

<!-- Country Id Field -->
<div class="form-group">
    {!! Form::label('country_id', 'Country Id:') !!}
    <p>{!! $ocZoneToGeoZone->country_id !!}</p>
</div>

<!-- Zone Id Field -->
<div class="form-group">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    <p>{!! $ocZoneToGeoZone->zone_id !!}</p>
</div>

<!-- Geo Zone Id Field -->
<div class="form-group">
    {!! Form::label('geo_zone_id', 'Geo Zone Id:') !!}
    <p>{!! $ocZoneToGeoZone->geo_zone_id !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocZoneToGeoZone->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocZoneToGeoZone->date_modified !!}</p>
</div>

