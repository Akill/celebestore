<!-- Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('country_id', 'Country Id:') !!}
    {!! Form::number('country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zone_id', 'Zone Id:') !!}
    {!! Form::number('zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Geo Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('geo_zone_id', 'Geo Zone Id:') !!}
    {!! Form::number('geo_zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    {!! Form::date('date_modified', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocZoneToGeoZones.index') !!}" class="btn btn-default">Cancel</a>
</div>
