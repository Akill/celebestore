@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Zone To Geo Zone
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocZoneToGeoZones.store']) !!}

                        @include('oc_zone_to_geo_zones.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
