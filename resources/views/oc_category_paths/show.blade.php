@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Category Path
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('oc_category_paths.show_fields')
                    <a href="{!! route('ocCategoryPaths.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
