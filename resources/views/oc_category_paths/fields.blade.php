<!-- Path Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('path_id', 'Path Id:') !!}
    {!! Form::number('path_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('level', 'Level:') !!}
    {!! Form::number('level', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCategoryPaths.index') !!}" class="btn btn-default">Cancel</a>
</div>
