@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Category Path
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCategoryPath, ['route' => ['ocCategoryPaths.update', $ocCategoryPath->id], 'method' => 'patch']) !!}

                        @include('oc_category_paths.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection