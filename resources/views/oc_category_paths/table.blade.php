<table class="table table-responsive" id="ocCategoryPaths-table">
    <thead>
        <th>Path Id</th>
        <th>Level</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCategoryPaths as $ocCategoryPath)
        <tr>
            <td>{!! $ocCategoryPath->path_id !!}</td>
            <td>{!! $ocCategoryPath->level !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryPaths.destroy', $ocCategoryPath->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryPaths.show', [$ocCategoryPath->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryPaths.edit', [$ocCategoryPath->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>