<!-- Product Id Field -->
<div class="form-group col-sm-12">
    {!! Form::label('product_id', 'Product:') !!}
    @php $items = App\Models\oc_product::has('detail')->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
</div>
<div class="form-group col-sm-6">
<img src="http://placehold.it/250x250" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image',null,array('id'=>'image','class'=>'form-control')) !!}
</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductImages.index') !!}" class="btn btn-default">Cancel</a>
</div>
