@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Coupon Category
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCouponCategory, ['route' => ['ocCouponCategories.update', $ocCouponCategory->id], 'method' => 'patch']) !!}

                        @include('oc_coupon_categories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection