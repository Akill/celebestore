<table class="table table-responsive" id="ocCouponCategories-table">
    <thead>
        <th>Category Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCouponCategories as $ocCouponCategory)
        <tr>
            <td>{!! $ocCouponCategory->category_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCouponCategories.destroy', $ocCouponCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCouponCategories.show', [$ocCouponCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCouponCategories.edit', [$ocCouponCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>