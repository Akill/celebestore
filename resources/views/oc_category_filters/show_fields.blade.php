<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCategoryFilter->id !!}</p>
</div>

<!-- Filter Id Field -->
<div class="form-group">
    {!! Form::label('filter_id', 'Filter Id:') !!}
    <p>{!! $ocCategoryFilter->filter_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCategoryFilter->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCategoryFilter->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCategoryFilter->deleted_at !!}</p>
</div>

