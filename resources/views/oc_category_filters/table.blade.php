<table class="table table-responsive" id="ocCategoryFilters-table">
    <thead>
        <th>Filter Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCategoryFilters as $ocCategoryFilter)
        <tr>
            <td>{!! $ocCategoryFilter->filter_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCategoryFilters.destroy', $ocCategoryFilter->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCategoryFilters.show', [$ocCategoryFilter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCategoryFilters.edit', [$ocCategoryFilter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>