<!-- Filter Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('filter_id', 'Filter Id:') !!}
    {!! Form::number('filter_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCategoryFilters.index') !!}" class="btn btn-default">Cancel</a>
</div>
