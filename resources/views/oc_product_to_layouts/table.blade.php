<table class="table table-responsive" id="ocProductToLayouts-table">
    <thead>
        <th>Store Id</th>
        <th>Layout Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductToLayouts as $ocProductToLayout)
        <tr>
            <td>{!! $ocProductToLayout->store_id !!}</td>
            <td>{!! $ocProductToLayout->layout_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductToLayouts.destroy', $ocProductToLayout->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductToLayouts.show', [$ocProductToLayout->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductToLayouts.edit', [$ocProductToLayout->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>