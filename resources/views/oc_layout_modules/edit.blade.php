@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Layout Module
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocLayoutModule, ['route' => ['ocLayoutModules.update', $ocLayoutModule->id], 'method' => 'patch']) !!}

                        @include('oc_layout_modules.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection