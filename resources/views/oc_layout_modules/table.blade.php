<table class="table table-responsive" id="ocLayoutModules-table">
    <thead>
        <th>Layout Id</th>
        <th>Code</th>
        <th>Position</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocLayoutModules as $ocLayoutModule)
        <tr>
            <td>{!! $ocLayoutModule->layout_id !!}</td>
            <td>{!! $ocLayoutModule->code !!}</td>
            <td>{!! $ocLayoutModule->position !!}</td>
            <td>{!! $ocLayoutModule->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocLayoutModules.destroy', $ocLayoutModule->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocLayoutModules.show', [$ocLayoutModule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocLayoutModules.edit', [$ocLayoutModule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>