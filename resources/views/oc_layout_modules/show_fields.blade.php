<!-- Layout Module Id Field -->
<div class="form-group">
    {!! Form::label('layout_module_id', 'Layout Module Id:') !!}
    <p>{!! $ocLayoutModule->layout_module_id !!}</p>
</div>

<!-- Layout Id Field -->
<div class="form-group">
    {!! Form::label('layout_id', 'Layout Id:') !!}
    <p>{!! $ocLayoutModule->layout_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocLayoutModule->code !!}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    <p>{!! $ocLayoutModule->position !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocLayoutModule->sort_order !!}</p>
</div>

