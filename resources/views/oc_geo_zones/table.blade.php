<table class="table table-responsive" id="ocGeoZones-table">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Date Modified</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocGeoZones as $ocGeoZone)
        <tr>
            <td>{!! $ocGeoZone->name !!}</td>
            <td>{!! $ocGeoZone->description !!}</td>
            <td>{!! $ocGeoZone->date_modified !!}</td>
            <td>{!! $ocGeoZone->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocGeoZones.destroy', $ocGeoZone->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocGeoZones.show', [$ocGeoZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocGeoZones.edit', [$ocGeoZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>