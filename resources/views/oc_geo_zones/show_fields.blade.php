<!-- Geo Zone Id Field -->
<div class="form-group">
    {!! Form::label('geo_zone_id', 'Geo Zone Id:') !!}
    <p>{!! $ocGeoZone->geo_zone_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocGeoZone->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ocGeoZone->description !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocGeoZone->date_modified !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocGeoZone->date_added !!}</p>
</div>

