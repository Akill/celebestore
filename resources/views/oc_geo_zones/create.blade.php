@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Geo Zone
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocGeoZones.store']) !!}

                        @include('oc_geo_zones.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
