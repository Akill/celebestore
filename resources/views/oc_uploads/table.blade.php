<table class="table table-responsive" id="ocUploads-table">
    <thead>
        <th>Name</th>
        <th>Filename</th>
        <th>Code</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocUploads as $ocUpload)
        <tr>
            <td>{!! $ocUpload->name !!}</td>
            <td>{!! $ocUpload->filename !!}</td>
            <td>{!! $ocUpload->code !!}</td>
            <td>{!! $ocUpload->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocUploads.destroy', $ocUpload->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocUploads.show', [$ocUpload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocUploads.edit', [$ocUpload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>