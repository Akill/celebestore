@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Activity
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomerActivity, ['route' => ['ocCustomerActivities.update', $ocCustomerActivity->id], 'method' => 'patch']) !!}

                        @include('oc_customer_activities.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection