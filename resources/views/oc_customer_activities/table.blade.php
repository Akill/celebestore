<table class="table table-responsive" id="ocCustomerActivities-table">
    <thead>
        <th>Customer Id</th>
        <th>Key</th>
        <th>Data</th>
        <th>Ip</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerActivities as $ocCustomerActivity)
        <tr>
            <td>{!! $ocCustomerActivity->customer_id !!}</td>
            <td>{!! $ocCustomerActivity->key !!}</td>
            <td>{!! $ocCustomerActivity->data !!}</td>
            <td>{!! $ocCustomerActivity->ip !!}</td>
            <td>{!! $ocCustomerActivity->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerActivities.destroy', $ocCustomerActivity->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerActivities.show', [$ocCustomerActivity->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerActivities.edit', [$ocCustomerActivity->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>