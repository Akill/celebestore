<!-- Manufacturer Id Field -->
<div class="form-group">
    {!! Form::label('manufacturer_id', 'Manufacturer Id:') !!}
    <p>{!! $ocManufacturer->manufacturer_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocManufacturer->name !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocManufacturer->image !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocManufacturer->sort_order !!}</p>
</div>

