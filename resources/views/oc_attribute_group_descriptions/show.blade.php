@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Attribute Group Description
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('oc_attribute_group_descriptions.show_fields')
                    <a href="{!! route('ocAttributeGroupDescriptions.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
