<table class="table table-responsive" id="ocAttributeGroupDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAttributeGroupDescriptions as $ocAttributeGroupDescription)
        <tr>
            <td>{!! $ocAttributeGroupDescription->language_id !!}</td>
            <td>{!! $ocAttributeGroupDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAttributeGroupDescriptions.destroy', $ocAttributeGroupDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAttributeGroupDescriptions.show', [$ocAttributeGroupDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAttributeGroupDescriptions.edit', [$ocAttributeGroupDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>