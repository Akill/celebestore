<table class="table table-responsive" id="ocCustomerTransactions-table">
    <thead>
        <th>Customer Id</th>
        <th>Order Id</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerTransactions as $ocCustomerTransaction)
        <tr>
            <td>{!! $ocCustomerTransaction->customer_id !!}</td>
            <td>{!! $ocCustomerTransaction->order_id !!}</td>
            <td>{!! $ocCustomerTransaction->description !!}</td>
            <td>{!! $ocCustomerTransaction->amount !!}</td>
            <td>{!! $ocCustomerTransaction->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerTransactions.destroy', $ocCustomerTransaction->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerTransactions.show', [$ocCustomerTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerTransactions.edit', [$ocCustomerTransaction->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>