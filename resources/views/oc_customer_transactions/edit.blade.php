@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Transaction
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomerTransaction, ['route' => ['ocCustomerTransactions.update', $ocCustomerTransaction->id], 'method' => 'patch']) !!}

                        @include('oc_customer_transactions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection