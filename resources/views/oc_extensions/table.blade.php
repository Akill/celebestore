<table class="table table-responsive" id="ocExtensions-table">
    <thead>
        <th>Type</th>
        <th>Code</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocExtensions as $ocExtension)
        <tr>
            <td>{!! $ocExtension->type !!}</td>
            <td>{!! $ocExtension->code !!}</td>
            <td>
                {!! Form::open(['route' => ['ocExtensions.destroy', $ocExtension->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocExtensions.show', [$ocExtension->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocExtensions.edit', [$ocExtension->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>