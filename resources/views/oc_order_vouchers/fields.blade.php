<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Voucher Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('voucher_id', 'Voucher Id:') !!}
    {!! Form::number('voucher_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- From Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_name', 'From Name:') !!}
    {!! Form::text('from_name', null, ['class' => 'form-control']) !!}
</div>

<!-- From Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('from_email', 'From Email:') !!}
    {!! Form::text('from_email', null, ['class' => 'form-control']) !!}
</div>

<!-- To Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_name', 'To Name:') !!}
    {!! Form::text('to_name', null, ['class' => 'form-control']) !!}
</div>

<!-- To Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('to_email', 'To Email:') !!}
    {!! Form::text('to_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Voucher Theme Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('voucher_theme_id', 'Voucher Theme Id:') !!}
    {!! Form::number('voucher_theme_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Message Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('message', 'Message:') !!}
    {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocOrderVouchers.index') !!}" class="btn btn-default">Cancel</a>
</div>
