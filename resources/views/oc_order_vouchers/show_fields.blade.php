<!-- Order Voucher Id Field -->
<div class="form-group">
    {!! Form::label('order_voucher_id', 'Order Voucher Id:') !!}
    <p>{!! $ocOrderVoucher->order_voucher_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderVoucher->order_id !!}</p>
</div>

<!-- Voucher Id Field -->
<div class="form-group">
    {!! Form::label('voucher_id', 'Voucher Id:') !!}
    <p>{!! $ocOrderVoucher->voucher_id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ocOrderVoucher->description !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocOrderVoucher->code !!}</p>
</div>

<!-- From Name Field -->
<div class="form-group">
    {!! Form::label('from_name', 'From Name:') !!}
    <p>{!! $ocOrderVoucher->from_name !!}</p>
</div>

<!-- From Email Field -->
<div class="form-group">
    {!! Form::label('from_email', 'From Email:') !!}
    <p>{!! $ocOrderVoucher->from_email !!}</p>
</div>

<!-- To Name Field -->
<div class="form-group">
    {!! Form::label('to_name', 'To Name:') !!}
    <p>{!! $ocOrderVoucher->to_name !!}</p>
</div>

<!-- To Email Field -->
<div class="form-group">
    {!! Form::label('to_email', 'To Email:') !!}
    <p>{!! $ocOrderVoucher->to_email !!}</p>
</div>

<!-- Voucher Theme Id Field -->
<div class="form-group">
    {!! Form::label('voucher_theme_id', 'Voucher Theme Id:') !!}
    <p>{!! $ocOrderVoucher->voucher_theme_id !!}</p>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', 'Message:') !!}
    <p>{!! $ocOrderVoucher->message !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $ocOrderVoucher->amount !!}</p>
</div>

