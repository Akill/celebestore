<table class="table table-responsive" id="ocOrderVouchers-table">
    <thead>
        <th>Order Id</th>
        <th>Voucher Id</th>
        <th>Description</th>
        <th>Code</th>
        <th>From Name</th>
        <th>From Email</th>
        <th>To Name</th>
        <th>To Email</th>
        <th>Voucher Theme Id</th>
        <th>Message</th>
        <th>Amount</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderVouchers as $ocOrderVoucher)
        <tr>
            <td>{!! $ocOrderVoucher->order_id !!}</td>
            <td>{!! $ocOrderVoucher->voucher_id !!}</td>
            <td>{!! $ocOrderVoucher->description !!}</td>
            <td>{!! $ocOrderVoucher->code !!}</td>
            <td>{!! $ocOrderVoucher->from_name !!}</td>
            <td>{!! $ocOrderVoucher->from_email !!}</td>
            <td>{!! $ocOrderVoucher->to_name !!}</td>
            <td>{!! $ocOrderVoucher->to_email !!}</td>
            <td>{!! $ocOrderVoucher->voucher_theme_id !!}</td>
            <td>{!! $ocOrderVoucher->message !!}</td>
            <td>{!! $ocOrderVoucher->amount !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderVouchers.destroy', $ocOrderVoucher->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderVouchers.show', [$ocOrderVoucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderVouchers.edit', [$ocOrderVoucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>