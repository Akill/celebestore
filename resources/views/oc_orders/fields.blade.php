<!-- Invoice No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_no', 'Invoice No:') !!}
    {!! Form::number('invoice_no', null, ['class' => 'form-control']) !!}
</div>

<!-- Invoice Prefix Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_prefix', 'Invoice Prefix:') !!}
    {!! Form::text('invoice_prefix', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_id', 'Store Id:') !!}
    {!! Form::number('store_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_name', 'Store Name:') !!}
    {!! Form::text('store_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Store Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_url', 'Store Url:') !!}
    {!! Form::text('store_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    {!! Form::number('customer_group_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', 'Firstname:') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Lastname:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telephone', 'Telephone:') !!}
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Fax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax', 'Fax:') !!}
    {!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>

<!-- Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    {!! Form::textarea('custom_field', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_firstname', 'Payment Firstname:') !!}
    {!! Form::text('payment_firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_lastname', 'Payment Lastname:') !!}
    {!! Form::text('payment_lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Company Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_company', 'Payment Company:') !!}
    {!! Form::text('payment_company', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Address 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_address_1', 'Payment Address 1:') !!}
    {!! Form::text('payment_address_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Address 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_address_2', 'Payment Address 2:') !!}
    {!! Form::text('payment_address_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_city', 'Payment City:') !!}
    {!! Form::text('payment_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_postcode', 'Payment Postcode:') !!}
    {!! Form::text('payment_postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_country', 'Payment Country:') !!}
    {!! Form::text('payment_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_country_id', 'Payment Country Id:') !!}
    {!! Form::number('payment_country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Zone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_zone', 'Payment Zone:') !!}
    {!! Form::text('payment_zone', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_zone_id', 'Payment Zone Id:') !!}
    {!! Form::number('payment_zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Sub District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_sub_district_id', 'Payment Sub District Id:') !!}
    {!! Form::number('payment_sub_district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Address Format Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('payment_address_format', 'Payment Address Format:') !!}
    {!! Form::textarea('payment_address_format', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('payment_custom_field', 'Payment Custom Field:') !!}
    {!! Form::textarea('payment_custom_field', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    {!! Form::text('payment_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment_code', 'Payment Code:') !!}
    {!! Form::text('payment_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_firstname', 'Shipping Firstname:') !!}
    {!! Form::text('shipping_firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_lastname', 'Shipping Lastname:') !!}
    {!! Form::text('shipping_lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Company Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_company', 'Shipping Company:') !!}
    {!! Form::text('shipping_company', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Address 1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_address_1', 'Shipping Address 1:') !!}
    {!! Form::text('shipping_address_1', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Address 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_address_2', 'Shipping Address 2:') !!}
    {!! Form::text('shipping_address_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_city', 'Shipping City:') !!}
    {!! Form::text('shipping_city', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Postcode Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_postcode', 'Shipping Postcode:') !!}
    {!! Form::text('shipping_postcode', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_country', 'Shipping Country:') !!}
    {!! Form::text('shipping_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Country Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_country_id', 'Shipping Country Id:') !!}
    {!! Form::number('shipping_country_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Zone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_zone', 'Shipping Zone:') !!}
    {!! Form::text('shipping_zone', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Zone Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_zone_id', 'Shipping Zone Id:') !!}
    {!! Form::number('shipping_zone_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Sub District Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_sub_district_id', 'Shipping Sub District Id:') !!}
    {!! Form::number('shipping_sub_district_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Address Format Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('shipping_address_format', 'Shipping Address Format:') !!}
    {!! Form::textarea('shipping_address_format', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Custom Field Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('shipping_custom_field', 'Shipping Custom Field:') !!}
    {!! Form::textarea('shipping_custom_field', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Method Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_method', 'Shipping Method:') !!}
    {!! Form::text('shipping_method', null, ['class' => 'form-control']) !!}
</div>

<!-- Shipping Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping_code', 'Shipping Code:') !!}
    {!! Form::text('shipping_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comment', 'Comment:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_status_id', 'Order Status Id:') !!}
    {!! Form::number('order_status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Affiliate Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('affiliate_id', 'Affiliate Id:') !!}
    {!! Form::number('affiliate_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', 'Commission:') !!}
    {!! Form::number('commission', null, ['class' => 'form-control']) !!}
</div>

<!-- Marketing Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marketing_id', 'Marketing Id:') !!}
    {!! Form::number('marketing_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tracking Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tracking', 'Tracking:') !!}
    {!! Form::text('tracking', null, ['class' => 'form-control']) !!}
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language_id', 'Language Id:') !!}
    {!! Form::number('language_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    {!! Form::number('currency_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    {!! Form::text('currency_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_value', 'Currency Value:') !!}
    {!! Form::number('currency_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ip', 'Ip:') !!}
    {!! Form::text('ip', null, ['class' => 'form-control']) !!}
</div>

<!-- Forwarded Ip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('forwarded_ip', 'Forwarded Ip:') !!}
    {!! Form::text('forwarded_ip', null, ['class' => 'form-control']) !!}
</div>

<!-- User Agent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_agent', 'User Agent:') !!}
    {!! Form::text('user_agent', null, ['class' => 'form-control']) !!}
</div>

<!-- Accept Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('accept_language', 'Accept Language:') !!}
    {!! Form::text('accept_language', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    {!! Form::date('date_modified', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocOrders.index') !!}" class="btn btn-default">Cancel</a>
</div>
