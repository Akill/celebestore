<table class="table table-responsive" id="ocOrders-table">
    <thead>
        <th>Invoice No</th>
        <th>Invoice Prefix</th>
        <th>Store Id</th>
        <th>Store Name</th>
        <th>Store Url</th>
        <th>Customer Id</th>
        <th>Customer Group Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Fax</th>
        <th>Custom Field</th>
        <th>Payment Firstname</th>
        <th>Payment Lastname</th>
        <th>Payment Company</th>
        <th>Payment Address 1</th>
        <th>Payment Address 2</th>
        <th>Payment City</th>
        <th>Payment Postcode</th>
        <th>Payment Country</th>
        <th>Payment Country Id</th>
        <th>Payment Zone</th>
        <th>Payment Zone Id</th>
        <th>Payment Sub District Id</th>
        <th>Payment Address Format</th>
        <th>Payment Custom Field</th>
        <th>Payment Method</th>
        <th>Payment Code</th>
        <th>Shipping Firstname</th>
        <th>Shipping Lastname</th>
        <th>Shipping Company</th>
        <th>Shipping Address 1</th>
        <th>Shipping Address 2</th>
        <th>Shipping City</th>
        <th>Shipping Postcode</th>
        <th>Shipping Country</th>
        <th>Shipping Country Id</th>
        <th>Shipping Zone</th>
        <th>Shipping Zone Id</th>
        <th>Shipping Sub District Id</th>
        <th>Shipping Address Format</th>
        <th>Shipping Custom Field</th>
        <th>Shipping Method</th>
        <th>Shipping Code</th>
        <th>Comment</th>
        <th>Total</th>
        <th>Order Status Id</th>
        <th>Affiliate Id</th>
        <th>Commission</th>
        <th>Marketing Id</th>
        <th>Tracking</th>
        <th>Language Id</th>
        <th>Currency Id</th>
        <th>Currency Code</th>
        <th>Currency Value</th>
        <th>Ip</th>
        <th>Forwarded Ip</th>
        <th>User Agent</th>
        <th>Accept Language</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrders as $ocOrder)
        <tr>
            <td>{!! $ocOrder->invoice_no !!}</td>
            <td>{!! $ocOrder->invoice_prefix !!}</td>
            <td>{!! $ocOrder->store_id !!}</td>
            <td>{!! $ocOrder->store_name !!}</td>
            <td>{!! $ocOrder->store_url !!}</td>
            <td>{!! $ocOrder->customer_id !!}</td>
            <td>{!! $ocOrder->customer_group_id !!}</td>
            <td>{!! $ocOrder->firstname !!}</td>
            <td>{!! $ocOrder->lastname !!}</td>
            <td>{!! $ocOrder->email !!}</td>
            <td>{!! $ocOrder->telephone !!}</td>
            <td>{!! $ocOrder->fax !!}</td>
            <td>{!! $ocOrder->custom_field !!}</td>
            <td>{!! $ocOrder->payment_firstname !!}</td>
            <td>{!! $ocOrder->payment_lastname !!}</td>
            <td>{!! $ocOrder->payment_company !!}</td>
            <td>{!! $ocOrder->payment_address_1 !!}</td>
            <td>{!! $ocOrder->payment_address_2 !!}</td>
            <td>{!! $ocOrder->payment_city !!}</td>
            <td>{!! $ocOrder->payment_postcode !!}</td>
            <td>{!! $ocOrder->payment_country !!}</td>
            <td>{!! $ocOrder->payment_country_id !!}</td>
            <td>{!! $ocOrder->payment_zone !!}</td>
            <td>{!! $ocOrder->payment_zone_id !!}</td>
            <td>{!! $ocOrder->payment_sub_district_id !!}</td>
            <td>{!! $ocOrder->payment_address_format !!}</td>
            <td>{!! $ocOrder->payment_custom_field !!}</td>
            <td>{!! $ocOrder->payment_method !!}</td>
            <td>{!! $ocOrder->payment_code !!}</td>
            <td>{!! $ocOrder->shipping_firstname !!}</td>
            <td>{!! $ocOrder->shipping_lastname !!}</td>
            <td>{!! $ocOrder->shipping_company !!}</td>
            <td>{!! $ocOrder->shipping_address_1 !!}</td>
            <td>{!! $ocOrder->shipping_address_2 !!}</td>
            <td>{!! $ocOrder->shipping_city !!}</td>
            <td>{!! $ocOrder->shipping_postcode !!}</td>
            <td>{!! $ocOrder->shipping_country !!}</td>
            <td>{!! $ocOrder->shipping_country_id !!}</td>
            <td>{!! $ocOrder->shipping_zone !!}</td>
            <td>{!! $ocOrder->shipping_zone_id !!}</td>
            <td>{!! $ocOrder->shipping_sub_district_id !!}</td>
            <td>{!! $ocOrder->shipping_address_format !!}</td>
            <td>{!! $ocOrder->shipping_custom_field !!}</td>
            <td>{!! $ocOrder->shipping_method !!}</td>
            <td>{!! $ocOrder->shipping_code !!}</td>
            <td>{!! $ocOrder->comment !!}</td>
            <td>{!! $ocOrder->total !!}</td>
            <td>{!! $ocOrder->order_status_id !!}</td>
            <td>{!! $ocOrder->affiliate_id !!}</td>
            <td>{!! $ocOrder->commission !!}</td>
            <td>{!! $ocOrder->marketing_id !!}</td>
            <td>{!! $ocOrder->tracking !!}</td>
            <td>{!! $ocOrder->language_id !!}</td>
            <td>{!! $ocOrder->currency_id !!}</td>
            <td>{!! $ocOrder->currency_code !!}</td>
            <td>{!! $ocOrder->currency_value !!}</td>
            <td>{!! $ocOrder->ip !!}</td>
            <td>{!! $ocOrder->forwarded_ip !!}</td>
            <td>{!! $ocOrder->user_agent !!}</td>
            <td>{!! $ocOrder->accept_language !!}</td>
            <td>{!! $ocOrder->date_added !!}</td>
            <td>{!! $ocOrder->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrders.destroy', $ocOrder->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrders.show', [$ocOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrders.edit', [$ocOrder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>