<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrder->order_id !!}</p>
</div>

<!-- Invoice No Field -->
<div class="form-group">
    {!! Form::label('invoice_no', 'Invoice No:') !!}
    <p>{!! $ocOrder->invoice_no !!}</p>
</div>

<!-- Invoice Prefix Field -->
<div class="form-group">
    {!! Form::label('invoice_prefix', 'Invoice Prefix:') !!}
    <p>{!! $ocOrder->invoice_prefix !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $ocOrder->store_id !!}</p>
</div>

<!-- Store Name Field -->
<div class="form-group">
    {!! Form::label('store_name', 'Store Name:') !!}
    <p>{!! $ocOrder->store_name !!}</p>
</div>

<!-- Store Url Field -->
<div class="form-group">
    {!! Form::label('store_url', 'Store Url:') !!}
    <p>{!! $ocOrder->store_url !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $ocOrder->customer_id !!}</p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    <p>{!! $ocOrder->customer_group_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $ocOrder->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $ocOrder->lastname !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocOrder->email !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $ocOrder->telephone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $ocOrder->fax !!}</p>
</div>

<!-- Custom Field Field -->
<div class="form-group">
    {!! Form::label('custom_field', 'Custom Field:') !!}
    <p>{!! $ocOrder->custom_field !!}</p>
</div>

<!-- Payment Firstname Field -->
<div class="form-group">
    {!! Form::label('payment_firstname', 'Payment Firstname:') !!}
    <p>{!! $ocOrder->payment_firstname !!}</p>
</div>

<!-- Payment Lastname Field -->
<div class="form-group">
    {!! Form::label('payment_lastname', 'Payment Lastname:') !!}
    <p>{!! $ocOrder->payment_lastname !!}</p>
</div>

<!-- Payment Company Field -->
<div class="form-group">
    {!! Form::label('payment_company', 'Payment Company:') !!}
    <p>{!! $ocOrder->payment_company !!}</p>
</div>

<!-- Payment Address 1 Field -->
<div class="form-group">
    {!! Form::label('payment_address_1', 'Payment Address 1:') !!}
    <p>{!! $ocOrder->payment_address_1 !!}</p>
</div>

<!-- Payment Address 2 Field -->
<div class="form-group">
    {!! Form::label('payment_address_2', 'Payment Address 2:') !!}
    <p>{!! $ocOrder->payment_address_2 !!}</p>
</div>

<!-- Payment City Field -->
<div class="form-group">
    {!! Form::label('payment_city', 'Payment City:') !!}
    <p>{!! $ocOrder->payment_city !!}</p>
</div>

<!-- Payment Postcode Field -->
<div class="form-group">
    {!! Form::label('payment_postcode', 'Payment Postcode:') !!}
    <p>{!! $ocOrder->payment_postcode !!}</p>
</div>

<!-- Payment Country Field -->
<div class="form-group">
    {!! Form::label('payment_country', 'Payment Country:') !!}
    <p>{!! $ocOrder->payment_country !!}</p>
</div>

<!-- Payment Country Id Field -->
<div class="form-group">
    {!! Form::label('payment_country_id', 'Payment Country Id:') !!}
    <p>{!! $ocOrder->payment_country_id !!}</p>
</div>

<!-- Payment Zone Field -->
<div class="form-group">
    {!! Form::label('payment_zone', 'Payment Zone:') !!}
    <p>{!! $ocOrder->payment_zone !!}</p>
</div>

<!-- Payment Zone Id Field -->
<div class="form-group">
    {!! Form::label('payment_zone_id', 'Payment Zone Id:') !!}
    <p>{!! $ocOrder->payment_zone_id !!}</p>
</div>

<!-- Payment Sub District Id Field -->
<div class="form-group">
    {!! Form::label('payment_sub_district_id', 'Payment Sub District Id:') !!}
    <p>{!! $ocOrder->payment_sub_district_id !!}</p>
</div>

<!-- Payment Address Format Field -->
<div class="form-group">
    {!! Form::label('payment_address_format', 'Payment Address Format:') !!}
    <p>{!! $ocOrder->payment_address_format !!}</p>
</div>

<!-- Payment Custom Field Field -->
<div class="form-group">
    {!! Form::label('payment_custom_field', 'Payment Custom Field:') !!}
    <p>{!! $ocOrder->payment_custom_field !!}</p>
</div>

<!-- Payment Method Field -->
<div class="form-group">
    {!! Form::label('payment_method', 'Payment Method:') !!}
    <p>{!! $ocOrder->payment_method !!}</p>
</div>

<!-- Payment Code Field -->
<div class="form-group">
    {!! Form::label('payment_code', 'Payment Code:') !!}
    <p>{!! $ocOrder->payment_code !!}</p>
</div>

<!-- Shipping Firstname Field -->
<div class="form-group">
    {!! Form::label('shipping_firstname', 'Shipping Firstname:') !!}
    <p>{!! $ocOrder->shipping_firstname !!}</p>
</div>

<!-- Shipping Lastname Field -->
<div class="form-group">
    {!! Form::label('shipping_lastname', 'Shipping Lastname:') !!}
    <p>{!! $ocOrder->shipping_lastname !!}</p>
</div>

<!-- Shipping Company Field -->
<div class="form-group">
    {!! Form::label('shipping_company', 'Shipping Company:') !!}
    <p>{!! $ocOrder->shipping_company !!}</p>
</div>

<!-- Shipping Address 1 Field -->
<div class="form-group">
    {!! Form::label('shipping_address_1', 'Shipping Address 1:') !!}
    <p>{!! $ocOrder->shipping_address_1 !!}</p>
</div>

<!-- Shipping Address 2 Field -->
<div class="form-group">
    {!! Form::label('shipping_address_2', 'Shipping Address 2:') !!}
    <p>{!! $ocOrder->shipping_address_2 !!}</p>
</div>

<!-- Shipping City Field -->
<div class="form-group">
    {!! Form::label('shipping_city', 'Shipping City:') !!}
    <p>{!! $ocOrder->shipping_city !!}</p>
</div>

<!-- Shipping Postcode Field -->
<div class="form-group">
    {!! Form::label('shipping_postcode', 'Shipping Postcode:') !!}
    <p>{!! $ocOrder->shipping_postcode !!}</p>
</div>

<!-- Shipping Country Field -->
<div class="form-group">
    {!! Form::label('shipping_country', 'Shipping Country:') !!}
    <p>{!! $ocOrder->shipping_country !!}</p>
</div>

<!-- Shipping Country Id Field -->
<div class="form-group">
    {!! Form::label('shipping_country_id', 'Shipping Country Id:') !!}
    <p>{!! $ocOrder->shipping_country_id !!}</p>
</div>

<!-- Shipping Zone Field -->
<div class="form-group">
    {!! Form::label('shipping_zone', 'Shipping Zone:') !!}
    <p>{!! $ocOrder->shipping_zone !!}</p>
</div>

<!-- Shipping Zone Id Field -->
<div class="form-group">
    {!! Form::label('shipping_zone_id', 'Shipping Zone Id:') !!}
    <p>{!! $ocOrder->shipping_zone_id !!}</p>
</div>

<!-- Shipping Sub District Id Field -->
<div class="form-group">
    {!! Form::label('shipping_sub_district_id', 'Shipping Sub District Id:') !!}
    <p>{!! $ocOrder->shipping_sub_district_id !!}</p>
</div>

<!-- Shipping Address Format Field -->
<div class="form-group">
    {!! Form::label('shipping_address_format', 'Shipping Address Format:') !!}
    <p>{!! $ocOrder->shipping_address_format !!}</p>
</div>

<!-- Shipping Custom Field Field -->
<div class="form-group">
    {!! Form::label('shipping_custom_field', 'Shipping Custom Field:') !!}
    <p>{!! $ocOrder->shipping_custom_field !!}</p>
</div>

<!-- Shipping Method Field -->
<div class="form-group">
    {!! Form::label('shipping_method', 'Shipping Method:') !!}
    <p>{!! $ocOrder->shipping_method !!}</p>
</div>

<!-- Shipping Code Field -->
<div class="form-group">
    {!! Form::label('shipping_code', 'Shipping Code:') !!}
    <p>{!! $ocOrder->shipping_code !!}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{!! $ocOrder->comment !!}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{!! $ocOrder->total !!}</p>
</div>

<!-- Order Status Id Field -->
<div class="form-group">
    {!! Form::label('order_status_id', 'Order Status Id:') !!}
    <p>{!! $ocOrder->order_status_id !!}</p>
</div>

<!-- Affiliate Id Field -->
<div class="form-group">
    {!! Form::label('affiliate_id', 'Affiliate Id:') !!}
    <p>{!! $ocOrder->affiliate_id !!}</p>
</div>

<!-- Commission Field -->
<div class="form-group">
    {!! Form::label('commission', 'Commission:') !!}
    <p>{!! $ocOrder->commission !!}</p>
</div>

<!-- Marketing Id Field -->
<div class="form-group">
    {!! Form::label('marketing_id', 'Marketing Id:') !!}
    <p>{!! $ocOrder->marketing_id !!}</p>
</div>

<!-- Tracking Field -->
<div class="form-group">
    {!! Form::label('tracking', 'Tracking:') !!}
    <p>{!! $ocOrder->tracking !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocOrder->language_id !!}</p>
</div>

<!-- Currency Id Field -->
<div class="form-group">
    {!! Form::label('currency_id', 'Currency Id:') !!}
    <p>{!! $ocOrder->currency_id !!}</p>
</div>

<!-- Currency Code Field -->
<div class="form-group">
    {!! Form::label('currency_code', 'Currency Code:') !!}
    <p>{!! $ocOrder->currency_code !!}</p>
</div>

<!-- Currency Value Field -->
<div class="form-group">
    {!! Form::label('currency_value', 'Currency Value:') !!}
    <p>{!! $ocOrder->currency_value !!}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'Ip:') !!}
    <p>{!! $ocOrder->ip !!}</p>
</div>

<!-- Forwarded Ip Field -->
<div class="form-group">
    {!! Form::label('forwarded_ip', 'Forwarded Ip:') !!}
    <p>{!! $ocOrder->forwarded_ip !!}</p>
</div>

<!-- User Agent Field -->
<div class="form-group">
    {!! Form::label('user_agent', 'User Agent:') !!}
    <p>{!! $ocOrder->user_agent !!}</p>
</div>

<!-- Accept Language Field -->
<div class="form-group">
    {!! Form::label('accept_language', 'Accept Language:') !!}
    <p>{!! $ocOrder->accept_language !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocOrder->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocOrder->date_modified !!}</p>
</div>

