<table class="table table-responsive" id="ocProducts-table">
    <thead>
        <th>Model</th>
        <!-- <th>Sku</th> -->
        <!-- <th>Upc</th> -->
        <!-- <th>Ean</th> -->
        <!-- <th>Jan</th> -->
        <!-- <th>Isbn</th> -->
        <!-- <th>Mpn</th> -->
        <!-- <th>Location</th> -->
        <th>Price</th>
        <th>Quantity</th>
        <th>Status</th>
        <th>Stock Status</th>
        <th>Image</th>
        <!-- <th>Manufacturer Id</th> -->
        <!-- <th>Shipping</th> -->
        <!-- <th>Points</th> -->
        <!-- <th>Tax Class Id</th> -->
        <!-- <th>Date Available</th> -->
        <!-- <th>Weight</th> -->
        <!-- <th>Weight Class Id</th> -->
        <!-- <th>Length</th> -->
        <!-- <th>Width</th> -->
        <!-- <th>Height</th> -->
        <!-- <th>Length Class Id</th> -->
        <!-- <th>Subtract</th> -->
        <!-- <th>Minimum</th> -->
        <!-- <th>Sort Order</th> -->
        <!-- <th>Viewed</th> -->
        <!-- <th>Date Added</th> -->
        <!-- <th>Date Modified</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProducts as $ocProduct)
        <tr>
            <td>{!! $ocProduct->model !!}</td>
            <!-- <td>{!! $ocProduct->sku !!}</td> -->
            <!-- <td>{!! $ocProduct->upc !!}</td> -->
            <!-- <td>{!! $ocProduct->ean !!}</td> -->
            <!-- <td>{!! $ocProduct->jan !!}</td> -->
            <!-- <td>{!! $ocProduct->isbn !!}</td> -->
            <!-- <td>{!! $ocProduct->mpn !!}</td> -->
            <!-- <td>{!! $ocProduct->location !!}</td> -->
            <td>{!! $ocProduct->price !!}</td>
            <td>{!! $ocProduct->quantity !!}</td>
            <td>{!! $ocProduct->status !!}</td>
            <td>{!! App\Models\oc_stock_status::find($ocProduct->stock_status_id)->name !!}</td>
            <td><img src="image/product/sampul/{!! $ocProduct->model.'/'.$ocProduct->image !!}" alt="Image" width="80" height="80"></td>
            <!-- <td>{!! $ocProduct->manufacturer_id !!}</td> -->
            <!-- <td>{!! $ocProduct->shipping !!}</td> -->
            <!-- <td>{!! $ocProduct->points !!}</td> -->
            <!-- <td>{!! $ocProduct->tax_class_id !!}</td> -->
            <!-- <td>{!! $ocProduct->date_available !!}</td> -->
            <!-- <td>{!! $ocProduct->weight !!}</td> -->
            <!-- <td>{!! $ocProduct->weight_class_id !!}</td> -->
            <!-- <td>{!! $ocProduct->length !!}</td> -->
            <!-- <td>{!! $ocProduct->width !!}</td> -->
            <!-- <td>{!! $ocProduct->height !!}</td> -->
            <!-- <td>{!! $ocProduct->length_class_id !!}</td> -->
            <!-- <td>{!! $ocProduct->subtract !!}</td> -->
            <!-- <td>{!! $ocProduct->minimum !!}</td> -->
            <!-- <td>{!! $ocProduct->sort_order !!}</td> -->
            <!-- <td>{!! $ocProduct->viewed !!}</td> -->
            <!-- <td>{!! $ocProduct->date_added !!}</td> -->
            <!-- <td>{!! $ocProduct->date_modified !!}</td> -->
            <td>
                {!! Form::open(['route' => ['ocProducts.destroy', $ocProduct->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProducts.show', [$ocProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProducts.edit', [$ocProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>