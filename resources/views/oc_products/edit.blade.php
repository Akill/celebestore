@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProduct, ['route' => ['ocProducts.update', $ocProduct->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('oc_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection