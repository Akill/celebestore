<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProduct->id !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $ocProduct->model !!}</p>
</div>

<!-- Sku Field -->
<div class="form-group">
    {!! Form::label('sku', 'Sku:') !!}
    <p>{!! $ocProduct->sku !!}</p>
</div>

<!-- Upc Field -->
<div class="form-group">
    {!! Form::label('upc', 'Upc:') !!}
    <p>{!! $ocProduct->upc !!}</p>
</div>

<!-- Ean Field -->
<div class="form-group">
    {!! Form::label('ean', 'Ean:') !!}
    <p>{!! $ocProduct->ean !!}</p>
</div>

<!-- Jan Field -->
<div class="form-group">
    {!! Form::label('jan', 'Jan:') !!}
    <p>{!! $ocProduct->jan !!}</p>
</div>

<!-- Isbn Field -->
<div class="form-group">
    {!! Form::label('isbn', 'Isbn:') !!}
    <p>{!! $ocProduct->isbn !!}</p>
</div>

<!-- Mpn Field -->
<div class="form-group">
    {!! Form::label('mpn', 'Mpn:') !!}
    <p>{!! $ocProduct->mpn !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $ocProduct->location !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $ocProduct->quantity !!}</p>
</div>

<!-- Stock Status Id Field -->
<div class="form-group">
    {!! Form::label('stock_status_id', 'Stock Status Id:') !!}
    <p>{!! $ocProduct->stock_status_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocProduct->image !!}</p>
</div>

<!-- Manufacturer Id Field -->
<div class="form-group">
    {!! Form::label('manufacturer_id', 'Manufacturer Id:') !!}
    <p>{!! $ocProduct->manufacturer_id !!}</p>
</div>

<!-- Shipping Field -->
<div class="form-group">
    {!! Form::label('shipping', 'Shipping:') !!}
    <p>{!! $ocProduct->shipping !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $ocProduct->price !!}</p>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    <p>{!! $ocProduct->points !!}</p>
</div>

<!-- Tax Class Id Field -->
<div class="form-group">
    {!! Form::label('tax_class_id', 'Tax Class Id:') !!}
    <p>{!! $ocProduct->tax_class_id !!}</p>
</div>

<!-- Date Available Field -->
<div class="form-group">
    {!! Form::label('date_available', 'Date Available:') !!}
    <p>{!! $ocProduct->date_available !!}</p>
</div>

<!-- Weight Field -->
<div class="form-group">
    {!! Form::label('weight', 'Weight:') !!}
    <p>{!! $ocProduct->weight !!}</p>
</div>

<!-- Weight Class Id Field -->
<div class="form-group">
    {!! Form::label('weight_class_id', 'Weight Class Id:') !!}
    <p>{!! $ocProduct->weight_class_id !!}</p>
</div>

<!-- Length Field -->
<div class="form-group">
    {!! Form::label('length', 'Length:') !!}
    <p>{!! $ocProduct->length !!}</p>
</div>

<!-- Width Field -->
<div class="form-group">
    {!! Form::label('width', 'Width:') !!}
    <p>{!! $ocProduct->width !!}</p>
</div>

<!-- Height Field -->
<div class="form-group">
    {!! Form::label('height', 'Height:') !!}
    <p>{!! $ocProduct->height !!}</p>
</div>

<!-- Length Class Id Field -->
<div class="form-group">
    {!! Form::label('length_class_id', 'Length Class Id:') !!}
    <p>{!! $ocProduct->length_class_id !!}</p>
</div>

<!-- Subtract Field -->
<div class="form-group">
    {!! Form::label('subtract', 'Subtract:') !!}
    <p>{!! $ocProduct->subtract !!}</p>
</div>

<!-- Minimum Field -->
<div class="form-group">
    {!! Form::label('minimum', 'Minimum:') !!}
    <p>{!! $ocProduct->minimum !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocProduct->sort_order !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocProduct->status !!}</p>
</div>

<!-- Viewed Field -->
<div class="form-group">
    {!! Form::label('viewed', 'Viewed:') !!}
    <p>{!! $ocProduct->viewed !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocProduct->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocProduct->date_modified !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocProduct->deleted_at !!}</p>
</div>

