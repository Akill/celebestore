<!-- Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('model', 'Model:') !!}
    {!! Form::text('model', null, ['class' => 'form-control']) !!}
</div>

<!-- Sku Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sku', 'Sku:') !!}
    {!! Form::text('sku', null, ['class' => 'form-control']) !!}
</div>

<!-- Upc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('upc', 'Upc:') !!}
    {!! Form::text('upc', null, ['class' => 'form-control']) !!}
</div>

<!-- Ean Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ean', 'Ean:') !!}
    {!! Form::text('ean', null, ['class' => 'form-control']) !!}
</div>

<!-- Jan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jan', 'Jan:') !!}
    {!! Form::text('jan', null, ['class' => 'form-control']) !!}
</div>

<!-- Isbn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('isbn', 'Isbn:') !!}
    {!! Form::text('isbn', null, ['class' => 'form-control']) !!}
</div>

<!-- Mpn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mpn', 'Mpn:') !!}
    {!! Form::text('mpn', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Stock Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock_status_id', 'Stock Status:') !!}
    @php $items = App\Models\oc_stock_status::all(['name','id']) @endphp
    <select class="form-control" id="stock_status_id" name="stock_status_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{$i->name}}</option>
    @endforeach
    </select>
</div>

<div class="form-group col-sm-6">
<img src="http://placehold.it/250x250" id="showgambar" style="max-width:400px;max-height:400px;float:left;" />
</div>
<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image',null,array('id'=>'image','class'=>'form-control')) !!}
</div>

<!-- Manufacturer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('manufacturer_id', 'Manufacturer Id:') !!}
    @php $items = App\Models\oc_manufacturer::all(['name','id']) @endphp
    <select class="form-control" id="manufacturer_id" name="manufacturer_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{$i->name}}</option>
    @endforeach
    </select>
</div>

<!-- Shipping Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping', 'Shipping:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('shipping', false) !!}
        {!! Form::checkbox('shipping', '1', '0') !!} 1
    </label>
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Points Field -->
<div class="form-group col-sm-6">
    {!! Form::label('points', 'Points:') !!}
    {!! Form::number('points', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Class Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_class_id', 'Tax Class Id:') !!}
    @php $items = App\Models\oc_tax_class::all(['title','id']) @endphp
    <select class="form-control" id="tax_class_id" name="tax_class_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{$i->title}}</option>
    @endforeach
    </select>
</div>

<!-- Date Available Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_available', 'Date Available:') !!}
    {!! Form::date('date_available', null, ['class' => 'form-control']) !!}
</div>

<!-- Weight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('weight', 'Weight:') !!}
    {!! Form::number('weight', null, ['class' => 'form-control']) !!}
</div>

<!-- Weight Class Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('weight_class_id', 'Weight Class Id:') !!}
    @php $items = App\Models\oc_weight_class::all(['value','id']) @endphp
    <select class="form-control" id="weight_class_id" name="weight_class_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{$i->value}}</option>
    @endforeach
    </select>
</div>

<!-- Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('length', 'Length:') !!}
    {!! Form::number('length', null, ['class' => 'form-control']) !!}
</div>

<!-- Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('width', 'Width:') !!}
    {!! Form::number('width', null, ['class' => 'form-control']) !!}
</div>

<!-- Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('height', 'Height:') !!}
    {!! Form::number('height', null, ['class' => 'form-control']) !!}
</div>

<!-- Length Class Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('length_class_id', 'Length Class Id:') !!}
    @php $items = App\Models\oc_length_class::all(['value','id']) @endphp
    <select class="form-control" id="length_class_id" name="length_class_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{$i->value}}</option>
    @endforeach
    </select>
</div>

<!-- Subtract Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtract', 'Subtract:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('subtract', false) !!}
        {!! Form::checkbox('subtract', '1', '0') !!} 1
    </label>
</div>

<!-- Minimum Field -->
<div class="form-group col-sm-6">
    {!! Form::label('minimum', 'Minimum:') !!}
    {!! Form::number('minimum', null, ['class' => 'form-control']) !!}
</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', '0') !!} 1
    </label>
</div>

<!-- Viewed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('viewed', 'Viewed:') !!}
    {!! Form::number('viewed', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    {!! Form::date('date_modified', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProducts.index') !!}" class="btn btn-default">Cancel</a>
</div>
