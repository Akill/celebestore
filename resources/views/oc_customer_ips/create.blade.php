@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Ip
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocCustomerIps.store']) !!}

                        @include('oc_customer_ips.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
