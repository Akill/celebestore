<table class="table table-responsive" id="ocCustomerIps-table">
    <thead>
        <th>Customer Id</th>
        <th>Ip</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerIps as $ocCustomerIp)
        <tr>
            <td>{!! $ocCustomerIp->customer_id !!}</td>
            <td>{!! $ocCustomerIp->ip !!}</td>
            <td>{!! $ocCustomerIp->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerIps.destroy', $ocCustomerIp->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerIps.show', [$ocCustomerIp->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerIps.edit', [$ocCustomerIp->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>