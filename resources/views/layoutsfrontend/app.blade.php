<!DOCTYPE HTML>
<html>
<head>
    <title>{{ config('app.name', 'AFREL') }}</title>
    <!-- meta info -->
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="celebestore" />
    <meta name="description" content="celebestore">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap styles -->
    <link rel="stylesheet" href="css/boostrap.css">
    <!-- Font Awesome styles (icons) -->
    <link rel="stylesheet" href="css/font_awesome.css">
    <!-- Main Template styles -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- IE 8 Fallback -->
    <!--
    [if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]
    -->

    <!-- Your custom styles (blank file) -->
    <link rel="stylesheet" href="css/mystyles.css">

    <link rel="stylesheet" href="css/switcher.css">
    <!-- Demo Examples -->
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/apple.css" title="apple" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/pink.css" title="pink" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/teal.css" title="teal" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/gold.css" title="gold" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/downy.css" title="downy" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/atlantis.css" title="atlantis" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/red.css" title="red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/violet.css" title="violet" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/pomegranate.css" title="pomegranate" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/violet-red.css" title="violet-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/mexican-red.css" title="mexican-red" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/victoria.css" title="victoria" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/orient.css" title="orient" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/jgger.css" title="jgger" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/de-york.css" title="de-york" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/blaze-orange.css" title="blaze-orange" media="all" />
    <link rel="alternate stylesheet" type="text/css" href="css/schemes/hot-pink.css" title="hot-pink" media="all" />
    <!-- END Demo Examples -->
    @yield('css')
</head>

<body class="bg-grey">


    <div class="global-wrap">


        @include('layoutsfrontend.setting')


        <!-- 
        //////////////////////////////////////
	    //////////////MAIN HEADER///////////// 
	    //////////////////////////////////////
        -->
        @include('layoutsfrontend.header')

        @yield('carousel')


        <!-- <div class="gap"></div> -->
        <!-- SEARCH AREA -->
        <form class="search-area form-group">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 clearfix">
                        <label><i class="fa fa-search"></i><span>Cari</span>
                        </label>
                        <div class="search-area-division search-area-division-input">
                            <input class="form-control" id="skills" type="text" placeholder="Semua kebutuhan" />
                        </div>
                    </div>
                    <div class="col-md-3 clearfix">
                        <label><i class="fa fa-map-marker"></i><span>In</span>
                        </label>
                        <div class="search-area-division search-area-division-location">
                            <input class="form-control" type="text" placeholder="Indonesia" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-block btn-white search-btn" type="submit">Search</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END SEARCH AREA -->

        <div class="gap"></div>

        <!-- 
        ///////////////////////////////////////
        //////////////END MAIN HEADER////////// 
        ///////////////////////////////////////
        -->


        <!-- 
        ///////////////////////////////////////
    	//////////////PAGE CONTENT///////////// 
    	///////////////////////////////////////
        -->


        @yield('content')



        <!-- 
        ///////////////////////////////////////
    	//////////////END PAGE CONTENT///////// 
    	///////////////////////////////////////
        -->



        <!-- 
        ///////////////////////////////////////
    	//////////////MAIN FOOTER////////////// 
    	///////////////////////////////////////
        -->

        @include('layoutsfrontend.footer')
            
        <!-- 
        ///////////////////////////////////////
    	//////////////END MAIN  FOOTER///////// 
    	///////////////////////////////////////
        -->



        <!-- Scripts queries -->
        <script src="js/jquery.js"></script>
        <script src="js/boostrap.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/flexnav.min.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/tweet.min.js"></script>
        <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script> -->
        <script src="js/fitvids.min.js"></script>
        <script src="js/mail.min.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/masonry.js"></script>
        <script src="js/nicescroll.js"></script>

        <!-- Custom scripts -->
        <script src="js/custom.js"></script>
        <script src="js/switcher.js"></script>
        @yield('js')
    </div>
</body>
</html>
