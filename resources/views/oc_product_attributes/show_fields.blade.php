<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocProductAttribute->id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! App\Models\oc_product_description::where('product_id',$ocProductAttribute->product_id)->first()->name !!}</p>
</div>

<!-- Attribute Id Field -->
<div class="form-group">
    {!! Form::label('attribute_id', 'Attribute Id:') !!}
    <p>{!! App\Models\oc_attribute_description::where('attribute_id',$ocProductAttribute->attribute_id)->first()->name !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! App\Models\oc_language::find($ocProductAttribute->language_id)->name !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $ocProductAttribute->text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocProductAttribute->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocProductAttribute->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocProductAttribute->deleted_at !!}</p>
</div>

