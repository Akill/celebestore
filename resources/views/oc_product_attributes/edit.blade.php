@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product Attribute
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductAttribute, ['route' => ['ocProductAttributes.update', $ocProductAttribute->id], 'method' => 'patch']) !!}

                        @include('oc_product_attributes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection