<table class="table table-responsive" id="ocProductAttributes-table">
    <thead>
        <th>Product</th>
        <th>Attribute</th>
        <th>Language</th>
        <th>Text</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductAttributes as $ocProductAttribute)
        <tr>
            <td>{!! App\Models\oc_product_description::where('product_id',$ocProductAttribute->product_id)->first()->name !!}</td>
            <td>{!! App\Models\oc_attribute_description::where('attribute_id',$ocProductAttribute->attribute_id)->first()->name !!}</td>
            <td>{!! App\Models\oc_language::find($ocProductAttribute->language_id)->name !!}</td>
            <td>{!! $ocProductAttribute->text !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductAttributes.destroy', $ocProductAttribute->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductAttributes.show', [$ocProductAttribute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductAttributes.edit', [$ocProductAttribute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>