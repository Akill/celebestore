@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product Attribute
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocProductAttributes.store']) !!}

                        @include('oc_product_attributes.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection