<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product:') !!}
    @php $items = App\Models\oc_product::has('detail')->get(); @endphp
    <select class="form-control" id="product_id" name="product_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_product_description::find($i->id)->name }}</option>
    @endforeach
    </select>
</div>

<!-- Attribute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('attribute_id', 'Attribute Id:') !!}
    @php $items = App\Models\oc_attribute::all(); @endphp
    <select class="form-control" id="attribute_id" name="attribute_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ App\Models\oc_attribute_description::where('attribute_id',$i->id)->first()->name }}</option>
    @endforeach
    </select>
</div>

<!-- Language Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language_id', 'Language Id:') !!}
    @php $items = App\Models\oc_language::all(['name','id']); @endphp
    <select class="form-control" id="language_id" name="language_id">
    @foreach($items as $i)
    <option value="{{$i->id}}">{{ $i->name }}</option>
    @endforeach
    </select>
</div>

<!-- Text Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductAttributes.index') !!}" class="btn btn-default">Cancel</a>
</div>

<!-- textarea script from npm (bootstrap3-wysihtml5-bower) -->