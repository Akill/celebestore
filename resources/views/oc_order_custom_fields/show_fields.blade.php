<!-- Order Custom Field Id Field -->
<div class="form-group">
    {!! Form::label('order_custom_field_id', 'Order Custom Field Id:') !!}
    <p>{!! $ocOrderCustomField->order_custom_field_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderCustomField->order_id !!}</p>
</div>

<!-- Custom Field Id Field -->
<div class="form-group">
    {!! Form::label('custom_field_id', 'Custom Field Id:') !!}
    <p>{!! $ocOrderCustomField->custom_field_id !!}</p>
</div>

<!-- Custom Field Value Id Field -->
<div class="form-group">
    {!! Form::label('custom_field_value_id', 'Custom Field Value Id:') !!}
    <p>{!! $ocOrderCustomField->custom_field_value_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocOrderCustomField->name !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocOrderCustomField->value !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $ocOrderCustomField->type !!}</p>
</div>

<!-- Location Field -->
<div class="form-group">
    {!! Form::label('location', 'Location:') !!}
    <p>{!! $ocOrderCustomField->location !!}</p>
</div>

