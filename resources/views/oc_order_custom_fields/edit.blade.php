@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Custom Field
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOrderCustomField, ['route' => ['ocOrderCustomFields.update', $ocOrderCustomField->id], 'method' => 'patch']) !!}

                        @include('oc_order_custom_fields.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection