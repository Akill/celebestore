<table class="table table-responsive" id="ocOrderCustomFields-table">
    <thead>
        <th>Order Id</th>
        <th>Custom Field Id</th>
        <th>Custom Field Value Id</th>
        <th>Name</th>
        <th>Value</th>
        <th>Type</th>
        <th>Location</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderCustomFields as $ocOrderCustomField)
        <tr>
            <td>{!! $ocOrderCustomField->order_id !!}</td>
            <td>{!! $ocOrderCustomField->custom_field_id !!}</td>
            <td>{!! $ocOrderCustomField->custom_field_value_id !!}</td>
            <td>{!! $ocOrderCustomField->name !!}</td>
            <td>{!! $ocOrderCustomField->value !!}</td>
            <td>{!! $ocOrderCustomField->type !!}</td>
            <td>{!! $ocOrderCustomField->location !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderCustomFields.destroy', $ocOrderCustomField->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderCustomFields.show', [$ocOrderCustomField->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderCustomFields.edit', [$ocOrderCustomField->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>