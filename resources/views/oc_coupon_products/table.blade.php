<table class="table table-responsive" id="ocCouponProducts-table">
    <thead>
        <th>Coupon Id</th>
        <th>Product Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCouponProducts as $ocCouponProduct)
        <tr>
            <td>{!! $ocCouponProduct->coupon_id !!}</td>
            <td>{!! $ocCouponProduct->product_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCouponProducts.destroy', $ocCouponProduct->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCouponProducts.show', [$ocCouponProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCouponProducts.edit', [$ocCouponProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>