@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Coupon Product
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCouponProduct, ['route' => ['ocCouponProducts.update', $ocCouponProduct->id], 'method' => 'patch']) !!}

                        @include('oc_coupon_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection