<!-- Voucher Id Field -->
<div class="form-group">
    {!! Form::label('voucher_id', 'Voucher Id:') !!}
    <p>{!! $ocVoucher->voucher_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocVoucher->order_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocVoucher->code !!}</p>
</div>

<!-- From Name Field -->
<div class="form-group">
    {!! Form::label('from_name', 'From Name:') !!}
    <p>{!! $ocVoucher->from_name !!}</p>
</div>

<!-- From Email Field -->
<div class="form-group">
    {!! Form::label('from_email', 'From Email:') !!}
    <p>{!! $ocVoucher->from_email !!}</p>
</div>

<!-- To Name Field -->
<div class="form-group">
    {!! Form::label('to_name', 'To Name:') !!}
    <p>{!! $ocVoucher->to_name !!}</p>
</div>

<!-- To Email Field -->
<div class="form-group">
    {!! Form::label('to_email', 'To Email:') !!}
    <p>{!! $ocVoucher->to_email !!}</p>
</div>

<!-- Voucher Theme Id Field -->
<div class="form-group">
    {!! Form::label('voucher_theme_id', 'Voucher Theme Id:') !!}
    <p>{!! $ocVoucher->voucher_theme_id !!}</p>
</div>

<!-- Message Field -->
<div class="form-group">
    {!! Form::label('message', 'Message:') !!}
    <p>{!! $ocVoucher->message !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $ocVoucher->amount !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocVoucher->status !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocVoucher->date_added !!}</p>
</div>

