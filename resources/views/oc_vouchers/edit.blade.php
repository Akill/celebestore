@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Voucher
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocVoucher, ['route' => ['ocVouchers.update', $ocVoucher->id], 'method' => 'patch']) !!}

                        @include('oc_vouchers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection