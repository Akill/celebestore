<table class="table table-responsive" id="ocVouchers-table">
    <thead>
        <th>Order Id</th>
        <th>Code</th>
        <th>From Name</th>
        <th>From Email</th>
        <th>To Name</th>
        <th>To Email</th>
        <th>Voucher Theme Id</th>
        <th>Message</th>
        <th>Amount</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocVouchers as $ocVoucher)
        <tr>
            <td>{!! $ocVoucher->order_id !!}</td>
            <td>{!! $ocVoucher->code !!}</td>
            <td>{!! $ocVoucher->from_name !!}</td>
            <td>{!! $ocVoucher->from_email !!}</td>
            <td>{!! $ocVoucher->to_name !!}</td>
            <td>{!! $ocVoucher->to_email !!}</td>
            <td>{!! $ocVoucher->voucher_theme_id !!}</td>
            <td>{!! $ocVoucher->message !!}</td>
            <td>{!! $ocVoucher->amount !!}</td>
            <td>{!! $ocVoucher->status !!}</td>
            <td>{!! $ocVoucher->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocVouchers.destroy', $ocVoucher->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocVouchers.show', [$ocVoucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocVouchers.edit', [$ocVoucher->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>