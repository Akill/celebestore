@extends('layoutsfrontend.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <aside class="sidebar-left">
                <!-- <h3 class="mb20 text-center">I am looking for</h3> -->
                <ul class="nav nav-tabs nav-stacked nav-coupon-category nav-coupon-category-left">
                    @forelse($ocCategories as $category)
                        @if($category->parent_id==0)
                        <li>
                            <a href="{{ $category->id }}">
                                {!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!}
                            </a>
                        </li>
                        @endif
                    @empty
                        <li>
                            <a href="#">
                                Category Tidak Ada
                            </a>
                        </li>
                    @endforelse
                </ul>
            </aside>
        </div>
        <div class="col-md-9">
            <div class="row row-wrap">
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/green_furniture_800x600.jpg" alt="Image Alternative text" title="Green Furniture" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="3.7/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Green Furniture Pack</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$177</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/hot_mixer_800x600.jpg" alt="Image Alternative text" title="Hot mixer" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="3/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                                <li><i class="fa fa-star-o"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Modern DJ Set</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$190</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product-thumb">
                        <header class="product-header">
                            <img src="img/waipio_valley_800x600.jpg" alt="Image Alternative text" title="waipio valley" />
                        </header>
                        <div class="product-inner">
                            <ul class="icon-group icon-list-rating" title="4.1/5 rating">
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star"></i>
                                </li>
                                <li><i class="fa fa-star-half-empty"></i>
                                </li>
                            </ul>
                            <h5 class="product-title">Awesome Vacation</h5>
                            <p class="product-desciption">Senectus ut luctus rhoncus proin mattis aenean cubilia</p>
                            <div class="product-meta">
                                <ul class="product-price-list">
                                    <li><span class="product-price">$135</span>
                                    </li>
                                </ul>
                                <ul class="product-actions-list">
                                    <li><a class="btn btn-sm" href="#"><i class="fa fa-shopping-cart"></i> To Cart</a>
                                    </li>
                                    <li><a class="btn btn-sm"><i class="fa fa-bars"></i> Details</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @forelse($ocCategories4 as $category)
        @if($category->parent_id==0)
        <h1 class="mb20">{!! App\Models\oc_category_description::where('oc_category_id',$category->id)->first()['name'] !!} <small><a href="#">View All</a></small></h1>
        <div class="row row-wrap">
            @foreach(App\Models\oc_product_to_category::where('category_id',$category->id)->get() as $product)
            <a class="col-md-3" href="#">
                <div class="product-thumb">
                    <header class="product-header">
                        <img src="image/product/sampul/{{ App\Models\oc_product::where('id',$product->product_id)->first()['model'].'/'.App\Models\oc_product::where('id',$product->product_id)->first()['image'] }}" alt="Image Alternative text" title="Food is Pride" />
                    </header>
                    <div class="product-inner">
                        <h5 class="product-title">{{App\Models\oc_product_description::where('product_id',$product->id)->first()['name']}}</h5>
                        <p class="product-desciption">{!! \Illuminate\Support\Str::words(App\Models\oc_product_description::where('product_id',$product->id)->first()['description'], 10,'....') !!}</p>
                        <div class="product-meta">
                            <ul class="product-price-list">
                                <li><span class="product-price">Rp. {{ App\Models\oc_product::where('id',$product->product_id)->first()['price'] }}</span>
                                </li>
                            </ul>
                        </div>
                        <p class="product-location"><i class="fa fa-map-marker"></i> {{ App\Models\oc_product::where('id',$product->product_id)->first()['location'] }}</p>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
        @endif
    @empty
        Category Tidak Ada
    @endforelse
</div>
<div class="gap"></div>
<div class="container">
    <div class="row row-wrap">
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-truck sale-point-icon"></i>
                <h5 class="sale-point-title">Cepat & Bebas pengiriman</h5>
                <p class="sale-point-description">Melayani dengan cepat pembelian anda, dan bebas biaya pengiriman untuk wilayah teretentu dan jenis barang-barang tertentu.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-tags sale-point-icon"></i>
                <h5 class="sale-point-title">Diskon terbaik</h5>
                <p class="sale-point-description">Dapatkan diskon setiap hari besar di indonesia, serta dapatkan promo barang-barang populer dengan tanda ini.</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="sale-point"><i class="fa fa-money sale-point-icon"></i>
                <h5 class="sale-point-title">Garansi uang kemabli</h5>
                <p class="sale-point-description">Kami melayani anda dengan mengutamakan pelayanan dan kualitas barang, jika merasa kurang silahkan hubungi kami.</p>
            </div>
        </div>
    </div>
</div>
@endsection