@extends('layoutsfrontend.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table class="table cart-table">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Name</th>
                        <th>QTY</th>
                        <th>Price</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($cart as $carts)
                    <tr>
                        <td class="cart-item-image">
                            <a href="#">
                                <img src="image/product/sampul/{{ App\Models\oc_product::where('id',$carts->id)->first()['model'].'/'.App\Models\oc_product::where('id',$carts->id)->first()['image'] }}" alt="Image Alternative text" title="AMaze" width="70" height="70" />
                            </a>
                        </td>
                        <td><a href="#">{{$carts->name}}</a>
                        </td>
                        <td class="cart-item-quantity"><i class="fa fa-minus cart-item-minus"></i>
                            <input type="text" name="cart-quantity" class="cart-quantity" value="{{$carts->qty}}" /><i class="fa fa-plus cart-item-plus"></i>
                        </td>
                        <td>{{$carts->price}}</td>
                        <td class="cart-item-remove">
                            <form role="form" method="POST" action="{!! url('cart/remove/'.$carts->rowId) !!}">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <button type="submit" class="btn btn-sm">
                                    <!-- <i class="fa fa-trash"></i> Hapus -->
                                    <i class="fa fa-times"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>	<a href="#" class="btn btn-primary">Update the cart</a>
        </div>
        <div class="col-md-3">
            <ul class="cart-total-list">
                <li><span>Sub Total</span><span>{{ Cart::subtotal() }}</span>
                </li>
                <li><span>Shipping</span><span>$0.00</span>
                </li>
                <li><span>Taxes</span><span>{{ Cart::tax() }}</span>
                </li>
                <li><span>Total</span><span>{{ Cart::total() }}</span>
                </li>
            </ul>
            <a href="#" class="btn btn-primary btn-lg">Checkout</a>
        </div>
    </div>
    <div class="gap"></div>
</div>
@endsection