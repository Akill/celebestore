<table class="table table-responsive" id="ocCustomerGroups-table">
    <thead>
        <th>Approval</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerGroups as $ocCustomerGroup)
        <tr>
            <td>{!! $ocCustomerGroup->approval !!}</td>
            <td>{!! $ocCustomerGroup->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerGroups.destroy', $ocCustomerGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerGroups.show', [$ocCustomerGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerGroups.edit', [$ocCustomerGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>