<table class="table table-responsive" id="ocEvents-table">
    <thead>
        <th>Code</th>
        <th>Trigger</th>
        <th>Action</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocEvents as $ocEvent)
        <tr>
            <td>{!! $ocEvent->code !!}</td>
            <td>{!! $ocEvent->trigger !!}</td>
            <td>{!! $ocEvent->action !!}</td>
            <td>
                {!! Form::open(['route' => ['ocEvents.destroy', $ocEvent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocEvents.show', [$ocEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocEvents.edit', [$ocEvent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>