<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Trigger Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('trigger', 'Trigger:') !!}
    {!! Form::textarea('trigger', null, ['class' => 'form-control']) !!}
</div>

<!-- Action Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('action', 'Action:') !!}
    {!! Form::textarea('action', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocEvents.index') !!}" class="btn btn-default">Cancel</a>
</div>
