<table class="table table-responsive" id="ocProductToDownloads-table">
    <thead>
        <th>Download Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductToDownloads as $ocProductToDownload)
        <tr>
            <td>{!! $ocProductToDownload->download_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductToDownloads.destroy', $ocProductToDownload->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductToDownloads.show', [$ocProductToDownload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductToDownloads.edit', [$ocProductToDownload->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>