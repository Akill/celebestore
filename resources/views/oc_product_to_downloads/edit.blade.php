@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product To Download
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductToDownload, ['route' => ['ocProductToDownloads.update', $ocProductToDownload->id], 'method' => 'patch']) !!}

                        @include('oc_product_to_downloads.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection