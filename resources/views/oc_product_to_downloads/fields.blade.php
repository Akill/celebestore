<!-- Download Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('download_id', 'Download Id:') !!}
    {!! Form::number('download_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductToDownloads.index') !!}" class="btn btn-default">Cancel</a>
</div>
