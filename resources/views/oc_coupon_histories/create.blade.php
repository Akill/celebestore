@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Coupon History
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocCouponHistories.store']) !!}

                        @include('oc_coupon_histories.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
