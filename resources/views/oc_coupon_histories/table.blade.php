<table class="table table-responsive" id="ocCouponHistories-table">
    <thead>
        <th>Coupon Id</th>
        <th>Order Id</th>
        <th>Customer Id</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCouponHistories as $ocCouponHistory)
        <tr>
            <td>{!! $ocCouponHistory->coupon_id !!}</td>
            <td>{!! $ocCouponHistory->order_id !!}</td>
            <td>{!! $ocCouponHistory->customer_id !!}</td>
            <td>{!! $ocCouponHistory->amount !!}</td>
            <td>{!! $ocCouponHistory->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCouponHistories.destroy', $ocCouponHistory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCouponHistories.show', [$ocCouponHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCouponHistories.edit', [$ocCouponHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>