<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $ocStore->store_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocStore->name !!}</p>
</div>

<!-- Url Field -->
<div class="form-group">
    {!! Form::label('url', 'Url:') !!}
    <p>{!! $ocStore->url !!}</p>
</div>

<!-- Ssl Field -->
<div class="form-group">
    {!! Form::label('ssl', 'Ssl:') !!}
    <p>{!! $ocStore->ssl !!}</p>
</div>

