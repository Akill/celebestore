<table class="table table-responsive" id="ocStores-table">
    <thead>
        <th>Name</th>
        <th>Url</th>
        <th>Ssl</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocStores as $ocStore)
        <tr>
            <td>{!! $ocStore->name !!}</td>
            <td>{!! $ocStore->url !!}</td>
            <td>{!! $ocStore->ssl !!}</td>
            <td>
                {!! Form::open(['route' => ['ocStores.destroy', $ocStore->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocStores.show', [$ocStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocStores.edit', [$ocStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>