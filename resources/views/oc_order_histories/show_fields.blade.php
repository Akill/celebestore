<!-- Order History Id Field -->
<div class="form-group">
    {!! Form::label('order_history_id', 'Order History Id:') !!}
    <p>{!! $ocOrderHistory->order_history_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderHistory->order_id !!}</p>
</div>

<!-- Order Status Id Field -->
<div class="form-group">
    {!! Form::label('order_status_id', 'Order Status Id:') !!}
    <p>{!! $ocOrderHistory->order_status_id !!}</p>
</div>

<!-- Notify Field -->
<div class="form-group">
    {!! Form::label('notify', 'Notify:') !!}
    <p>{!! $ocOrderHistory->notify !!}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{!! $ocOrderHistory->comment !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocOrderHistory->date_added !!}</p>
</div>

