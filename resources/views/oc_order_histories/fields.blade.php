<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Order Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_status_id', 'Order Status Id:') !!}
    {!! Form::number('order_status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Notify Field -->
<div class="form-group col-sm-6">
    {!! Form::label('notify', 'Notify:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('notify', false) !!}
        {!! Form::checkbox('notify', '1', null) !!} 1
    </label>
</div>

<!-- Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comment', 'Comment:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocOrderHistories.index') !!}" class="btn btn-default">Cancel</a>
</div>
