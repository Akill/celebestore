<table class="table table-responsive" id="ocOrderHistories-table">
    <thead>
        <th>Order Id</th>
        <th>Order Status Id</th>
        <th>Notify</th>
        <th>Comment</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderHistories as $ocOrderHistory)
        <tr>
            <td>{!! $ocOrderHistory->order_id !!}</td>
            <td>{!! $ocOrderHistory->order_status_id !!}</td>
            <td>{!! $ocOrderHistory->notify !!}</td>
            <td>{!! $ocOrderHistory->comment !!}</td>
            <td>{!! $ocOrderHistory->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderHistories.destroy', $ocOrderHistory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderHistories.show', [$ocOrderHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderHistories.edit', [$ocOrderHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>