@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order History
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOrderHistory, ['route' => ['ocOrderHistories.update', $ocOrderHistory->id], 'method' => 'patch']) !!}

                        @include('oc_order_histories.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection