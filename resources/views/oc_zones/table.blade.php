<table class="table table-responsive" id="ocZones-table">
    <thead>
        <th>Country Id</th>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocZones as $ocZone)
        <tr>
            <td>{!! $ocZone->country_id !!}</td>
            <td>{!! $ocZone->name !!}</td>
            <td>{!! $ocZone->code !!}</td>
            <td>{!! $ocZone->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocZones.destroy', $ocZone->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocZones.show', [$ocZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocZones.edit', [$ocZone->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>