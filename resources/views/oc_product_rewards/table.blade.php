<table class="table table-responsive" id="ocProductRewards-table">
    <thead>
        <th>Product Id</th>
        <th>Customer Group Id</th>
        <th>Points</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductRewards as $ocProductReward)
        <tr>
            <td>{!! $ocProductReward->product_id !!}</td>
            <td>{!! $ocProductReward->customer_group_id !!}</td>
            <td>{!! $ocProductReward->points !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductRewards.destroy', $ocProductReward->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductRewards.show', [$ocProductReward->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductRewards.edit', [$ocProductReward->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>