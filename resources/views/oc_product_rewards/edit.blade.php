@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product Reward
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductReward, ['route' => ['ocProductRewards.update', $ocProductReward->id], 'method' => 'patch']) !!}

                        @include('oc_product_rewards.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection