<!-- Product Reward Id Field -->
<div class="form-group">
    {!! Form::label('product_reward_id', 'Product Reward Id:') !!}
    <p>{!! $ocProductReward->product_reward_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocProductReward->product_id !!}</p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    <p>{!! $ocProductReward->customer_group_id !!}</p>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    <p>{!! $ocProductReward->points !!}</p>
</div>

