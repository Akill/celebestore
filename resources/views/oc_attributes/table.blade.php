<table class="table table-responsive" id="ocAttributes-table">
    <thead>
        <th>Attribute Group Id</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAttributes as $ocAttribute)
        <tr>
            <td>{!! $ocAttribute->attribute_group_id !!}</td>
            <td>{!! $ocAttribute->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAttributes.destroy', $ocAttribute->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAttributes.show', [$ocAttribute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAttributes.edit', [$ocAttribute->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>