<table class="table table-responsive" id="ocOrderTotals-table">
    <thead>
        <th>Order Id</th>
        <th>Code</th>
        <th>Title</th>
        <th>Value</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderTotals as $ocOrderTotal)
        <tr>
            <td>{!! $ocOrderTotal->order_id !!}</td>
            <td>{!! $ocOrderTotal->code !!}</td>
            <td>{!! $ocOrderTotal->title !!}</td>
            <td>{!! $ocOrderTotal->value !!}</td>
            <td>{!! $ocOrderTotal->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderTotals.destroy', $ocOrderTotal->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderTotals.show', [$ocOrderTotal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderTotals.edit', [$ocOrderTotal->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>