<!-- Order Total Id Field -->
<div class="form-group">
    {!! Form::label('order_total_id', 'Order Total Id:') !!}
    <p>{!! $ocOrderTotal->order_total_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderTotal->order_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocOrderTotal->code !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ocOrderTotal->title !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocOrderTotal->value !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocOrderTotal->sort_order !!}</p>
</div>

