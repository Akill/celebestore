<table class="table table-responsive" id="ocProductToCategories-table">
    <thead>
        <th>Product Id</th>
        <th>Category Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductToCategories as $ocProductToCategory)
        <tr>
            <td>{!! $ocProductToCategory->product_id !!}</td>
            <td>{!! $ocProductToCategory->category_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductToCategories.destroy', $ocProductToCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductToCategories.show', [$ocProductToCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductToCategories.edit', [$ocProductToCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>