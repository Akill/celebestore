<table class="table table-responsive" id="ocReviews-table">
    <thead>
        <th>Product Id</th>
        <th>Customer Id</th>
        <th>Author</th>
        <th>Text</th>
        <th>Rating</th>
        <th>Status</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocReviews as $ocReview)
        <tr>
            <td>{!! $ocReview->product_id !!}</td>
            <td>{!! $ocReview->customer_id !!}</td>
            <td>{!! $ocReview->author !!}</td>
            <td>{!! $ocReview->text !!}</td>
            <td>{!! $ocReview->rating !!}</td>
            <td>{!! $ocReview->status !!}</td>
            <td>{!! $ocReview->date_added !!}</td>
            <td>{!! $ocReview->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocReviews.destroy', $ocReview->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocReviews.show', [$ocReview->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocReviews.edit', [$ocReview->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>