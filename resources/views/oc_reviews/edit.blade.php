@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Review
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocReview, ['route' => ['ocReviews.update', $ocReview->id], 'method' => 'patch']) !!}

                        @include('oc_reviews.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection