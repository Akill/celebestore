<!-- Marketing Id Field -->
<div class="form-group">
    {!! Form::label('marketing_id', 'Marketing Id:') !!}
    <p>{!! $ocMarketing->marketing_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocMarketing->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ocMarketing->description !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocMarketing->code !!}</p>
</div>

<!-- Clicks Field -->
<div class="form-group">
    {!! Form::label('clicks', 'Clicks:') !!}
    <p>{!! $ocMarketing->clicks !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocMarketing->date_added !!}</p>
</div>

