<table class="table table-responsive" id="ocMarketings-table">
    <thead>
        <th>Name</th>
        <th>Description</th>
        <th>Code</th>
        <th>Clicks</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocMarketings as $ocMarketing)
        <tr>
            <td>{!! $ocMarketing->name !!}</td>
            <td>{!! $ocMarketing->description !!}</td>
            <td>{!! $ocMarketing->code !!}</td>
            <td>{!! $ocMarketing->clicks !!}</td>
            <td>{!! $ocMarketing->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocMarketings.destroy', $ocMarketing->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocMarketings.show', [$ocMarketing->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocMarketings.edit', [$ocMarketing->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>