@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Product
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('oc_order_products.show_fields')
                    <a href="{!! route('ocOrderProducts.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
