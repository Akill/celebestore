<table class="table table-responsive" id="ocOrderProducts-table">
    <thead>
        <th>Order Id</th>
        <th>Product Id</th>
        <th>Name</th>
        <th>Model</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
        <th>Tax</th>
        <th>Reward</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderProducts as $ocOrderProduct)
        <tr>
            <td>{!! $ocOrderProduct->order_id !!}</td>
            <td>{!! $ocOrderProduct->product_id !!}</td>
            <td>{!! $ocOrderProduct->name !!}</td>
            <td>{!! $ocOrderProduct->model !!}</td>
            <td>{!! $ocOrderProduct->quantity !!}</td>
            <td>{!! $ocOrderProduct->price !!}</td>
            <td>{!! $ocOrderProduct->total !!}</td>
            <td>{!! $ocOrderProduct->tax !!}</td>
            <td>{!! $ocOrderProduct->reward !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderProducts.destroy', $ocOrderProduct->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderProducts.show', [$ocOrderProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderProducts.edit', [$ocOrderProduct->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>