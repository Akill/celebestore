@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Product
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOrderProduct, ['route' => ['ocOrderProducts.update', $ocOrderProduct->id], 'method' => 'patch']) !!}

                        @include('oc_order_products.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection