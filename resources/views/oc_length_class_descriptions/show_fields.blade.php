<!-- Length Class Id Field -->
<div class="form-group">
    {!! Form::label('length_class_id', 'Length Class Id:') !!}
    <p>{!! $ocLengthClassDescription->length_class_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocLengthClassDescription->language_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ocLengthClassDescription->title !!}</p>
</div>

<!-- Unit Field -->
<div class="form-group">
    {!! Form::label('unit', 'Unit:') !!}
    <p>{!! $ocLengthClassDescription->unit !!}</p>
</div>

