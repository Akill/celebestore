@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Length Class Description
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocLengthClassDescription, ['route' => ['ocLengthClassDescriptions.update', $ocLengthClassDescription->id], 'method' => 'patch']) !!}

                        @include('oc_length_class_descriptions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection