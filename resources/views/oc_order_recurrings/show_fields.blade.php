<!-- Order Recurring Id Field -->
<div class="form-group">
    {!! Form::label('order_recurring_id', 'Order Recurring Id:') !!}
    <p>{!! $ocOrderRecurring->order_recurring_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocOrderRecurring->order_id !!}</p>
</div>

<!-- Reference Field -->
<div class="form-group">
    {!! Form::label('reference', 'Reference:') !!}
    <p>{!! $ocOrderRecurring->reference !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocOrderRecurring->product_id !!}</p>
</div>

<!-- Product Name Field -->
<div class="form-group">
    {!! Form::label('product_name', 'Product Name:') !!}
    <p>{!! $ocOrderRecurring->product_name !!}</p>
</div>

<!-- Product Quantity Field -->
<div class="form-group">
    {!! Form::label('product_quantity', 'Product Quantity:') !!}
    <p>{!! $ocOrderRecurring->product_quantity !!}</p>
</div>

<!-- Recurring Id Field -->
<div class="form-group">
    {!! Form::label('recurring_id', 'Recurring Id:') !!}
    <p>{!! $ocOrderRecurring->recurring_id !!}</p>
</div>

<!-- Recurring Name Field -->
<div class="form-group">
    {!! Form::label('recurring_name', 'Recurring Name:') !!}
    <p>{!! $ocOrderRecurring->recurring_name !!}</p>
</div>

<!-- Recurring Description Field -->
<div class="form-group">
    {!! Form::label('recurring_description', 'Recurring Description:') !!}
    <p>{!! $ocOrderRecurring->recurring_description !!}</p>
</div>

<!-- Recurring Frequency Field -->
<div class="form-group">
    {!! Form::label('recurring_frequency', 'Recurring Frequency:') !!}
    <p>{!! $ocOrderRecurring->recurring_frequency !!}</p>
</div>

<!-- Recurring Cycle Field -->
<div class="form-group">
    {!! Form::label('recurring_cycle', 'Recurring Cycle:') !!}
    <p>{!! $ocOrderRecurring->recurring_cycle !!}</p>
</div>

<!-- Recurring Duration Field -->
<div class="form-group">
    {!! Form::label('recurring_duration', 'Recurring Duration:') !!}
    <p>{!! $ocOrderRecurring->recurring_duration !!}</p>
</div>

<!-- Recurring Price Field -->
<div class="form-group">
    {!! Form::label('recurring_price', 'Recurring Price:') !!}
    <p>{!! $ocOrderRecurring->recurring_price !!}</p>
</div>

<!-- Trial Field -->
<div class="form-group">
    {!! Form::label('trial', 'Trial:') !!}
    <p>{!! $ocOrderRecurring->trial !!}</p>
</div>

<!-- Trial Frequency Field -->
<div class="form-group">
    {!! Form::label('trial_frequency', 'Trial Frequency:') !!}
    <p>{!! $ocOrderRecurring->trial_frequency !!}</p>
</div>

<!-- Trial Cycle Field -->
<div class="form-group">
    {!! Form::label('trial_cycle', 'Trial Cycle:') !!}
    <p>{!! $ocOrderRecurring->trial_cycle !!}</p>
</div>

<!-- Trial Duration Field -->
<div class="form-group">
    {!! Form::label('trial_duration', 'Trial Duration:') !!}
    <p>{!! $ocOrderRecurring->trial_duration !!}</p>
</div>

<!-- Trial Price Field -->
<div class="form-group">
    {!! Form::label('trial_price', 'Trial Price:') !!}
    <p>{!! $ocOrderRecurring->trial_price !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocOrderRecurring->status !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocOrderRecurring->date_added !!}</p>
</div>

