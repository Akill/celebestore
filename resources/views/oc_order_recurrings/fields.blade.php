<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Reference Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference', 'Reference:') !!}
    {!! Form::text('reference', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_name', 'Product Name:') !!}
    {!! Form::text('product_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_quantity', 'Product Quantity:') !!}
    {!! Form::number('product_quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_id', 'Recurring Id:') !!}
    {!! Form::number('recurring_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_name', 'Recurring Name:') !!}
    {!! Form::text('recurring_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_description', 'Recurring Description:') !!}
    {!! Form::text('recurring_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Frequency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_frequency', 'Recurring Frequency:') !!}
    {!! Form::text('recurring_frequency', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Cycle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_cycle', 'Recurring Cycle:') !!}
    {!! Form::number('recurring_cycle', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_duration', 'Recurring Duration:') !!}
    {!! Form::number('recurring_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Recurring Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recurring_price', 'Recurring Price:') !!}
    {!! Form::number('recurring_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial', 'Trial:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('trial', false) !!}
        {!! Form::checkbox('trial', '1', null) !!} 1
    </label>
</div>

<!-- Trial Frequency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_frequency', 'Trial Frequency:') !!}
    {!! Form::text('trial_frequency', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Cycle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_cycle', 'Trial Cycle:') !!}
    {!! Form::number('trial_cycle', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_duration', 'Trial Duration:') !!}
    {!! Form::number('trial_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_price', 'Trial Price:') !!}
    {!! Form::number('trial_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocOrderRecurrings.index') !!}" class="btn btn-default">Cancel</a>
</div>
