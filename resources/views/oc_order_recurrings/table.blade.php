<table class="table table-responsive" id="ocOrderRecurrings-table">
    <thead>
        <th>Order Id</th>
        <th>Reference</th>
        <th>Product Id</th>
        <th>Product Name</th>
        <th>Product Quantity</th>
        <th>Recurring Id</th>
        <th>Recurring Name</th>
        <th>Recurring Description</th>
        <th>Recurring Frequency</th>
        <th>Recurring Cycle</th>
        <th>Recurring Duration</th>
        <th>Recurring Price</th>
        <th>Trial</th>
        <th>Trial Frequency</th>
        <th>Trial Cycle</th>
        <th>Trial Duration</th>
        <th>Trial Price</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOrderRecurrings as $ocOrderRecurring)
        <tr>
            <td>{!! $ocOrderRecurring->order_id !!}</td>
            <td>{!! $ocOrderRecurring->reference !!}</td>
            <td>{!! $ocOrderRecurring->product_id !!}</td>
            <td>{!! $ocOrderRecurring->product_name !!}</td>
            <td>{!! $ocOrderRecurring->product_quantity !!}</td>
            <td>{!! $ocOrderRecurring->recurring_id !!}</td>
            <td>{!! $ocOrderRecurring->recurring_name !!}</td>
            <td>{!! $ocOrderRecurring->recurring_description !!}</td>
            <td>{!! $ocOrderRecurring->recurring_frequency !!}</td>
            <td>{!! $ocOrderRecurring->recurring_cycle !!}</td>
            <td>{!! $ocOrderRecurring->recurring_duration !!}</td>
            <td>{!! $ocOrderRecurring->recurring_price !!}</td>
            <td>{!! $ocOrderRecurring->trial !!}</td>
            <td>{!! $ocOrderRecurring->trial_frequency !!}</td>
            <td>{!! $ocOrderRecurring->trial_cycle !!}</td>
            <td>{!! $ocOrderRecurring->trial_duration !!}</td>
            <td>{!! $ocOrderRecurring->trial_price !!}</td>
            <td>{!! $ocOrderRecurring->status !!}</td>
            <td>{!! $ocOrderRecurring->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOrderRecurrings.destroy', $ocOrderRecurring->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOrderRecurrings.show', [$ocOrderRecurring->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOrderRecurrings.edit', [$ocOrderRecurring->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>