@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Order Recurring
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOrderRecurring, ['route' => ['ocOrderRecurrings.update', $ocOrderRecurring->id], 'method' => 'patch']) !!}

                        @include('oc_order_recurrings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection