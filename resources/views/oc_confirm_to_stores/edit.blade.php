@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Confirm To Store
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocConfirmToStore, ['route' => ['ocConfirmToStores.update', $ocConfirmToStore->id], 'method' => 'patch']) !!}

                        @include('oc_confirm_to_stores.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection