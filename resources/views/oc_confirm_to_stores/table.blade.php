<table class="table table-responsive" id="ocConfirmToStores-table">
    <thead>
        <th>Store Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocConfirmToStores as $ocConfirmToStore)
        <tr>
            <td>{!! $ocConfirmToStore->store_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocConfirmToStores.destroy', $ocConfirmToStore->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocConfirmToStores.show', [$ocConfirmToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocConfirmToStores.edit', [$ocConfirmToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>