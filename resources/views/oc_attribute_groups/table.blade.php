<table class="table table-responsive" id="ocAttributeGroups-table">
    <thead>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAttributeGroups as $ocAttributeGroup)
        <tr>
            <td>{!! $ocAttributeGroup->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAttributeGroups.destroy', $ocAttributeGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAttributeGroups.show', [$ocAttributeGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAttributeGroups.edit', [$ocAttributeGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>