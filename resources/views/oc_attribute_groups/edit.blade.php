@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Attribute Group
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocAttributeGroup, ['route' => ['ocAttributeGroups.update', $ocAttributeGroup->id], 'method' => 'patch']) !!}

                        @include('oc_attribute_groups.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection