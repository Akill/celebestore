<table class="table table-responsive" id="ocProductSpecials-table">
    <thead>
        <th>Product Id</th>
        <th>Customer Group Id</th>
        <th>Priority</th>
        <th>Price</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductSpecials as $ocProductSpecial)
        <tr>
            <td>{!! $ocProductSpecial->product_id !!}</td>
            <td>{!! $ocProductSpecial->customer_group_id !!}</td>
            <td>{!! $ocProductSpecial->priority !!}</td>
            <td>{!! $ocProductSpecial->price !!}</td>
            <td>{!! $ocProductSpecial->date_start !!}</td>
            <td>{!! $ocProductSpecial->date_end !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductSpecials.destroy', $ocProductSpecial->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductSpecials.show', [$ocProductSpecial->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductSpecials.edit', [$ocProductSpecial->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>