<!-- Product Special Id Field -->
<div class="form-group">
    {!! Form::label('product_special_id', 'Product Special Id:') !!}
    <p>{!! $ocProductSpecial->product_special_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocProductSpecial->product_id !!}</p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    <p>{!! $ocProductSpecial->customer_group_id !!}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{!! $ocProductSpecial->priority !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $ocProductSpecial->price !!}</p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{!! $ocProductSpecial->date_start !!}</p>
</div>

<!-- Date End Field -->
<div class="form-group">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{!! $ocProductSpecial->date_end !!}</p>
</div>

