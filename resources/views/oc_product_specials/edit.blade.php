@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Product Special
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocProductSpecial, ['route' => ['ocProductSpecials.update', $ocProductSpecial->id], 'method' => 'patch']) !!}

                        @include('oc_product_specials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection