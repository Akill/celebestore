<table class="table table-responsive" id="users-table">
    <thead>
        <th>User Group</th>
        <th>Name</th>
        <th>Email</th>
        <!-- <th>Password</th> -->
        <th>Username</th>
        <!-- <th>Type Of Identity</th> -->
        <!-- <th>Identity Number</th> -->
        <!-- <th>Gender</th> -->
        <th>Phone</th>
        <!-- <th>Job</th> -->
        <!-- <th>Address</th> -->
        <!-- <th>Districts</th> -->
        <!-- <th>City</th> -->
        <!-- <th>Province</th> -->
        <!-- <th>Zip Kode</th> -->
        <!-- <th>Picture</th> -->
        <!-- <th>Remember Token</th> -->
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! App\Models\oc_user_group::find($user->user_group_id)->name !!}</td>
            <td>{!! $user->name !!}</td>
            <td>{!! $user->email !!}</td>
            <!-- <td>{!! $user->password !!}</td> -->
            <td>{!! $user->username !!}</td>
            <!-- <td>{!! $user->type_of_identity !!}</td> -->
            <!-- <td>{!! $user->identity_number !!}</td> -->
            <!-- <td>{!! $user->gender !!}</td> -->
            <td>{!! $user->phone !!}</td>
            <!-- <td>{!! $user->job !!}</td> -->
            <!-- <td>{!! $user->address !!}</td> -->
            <!-- <td>{!! $user->districts !!}</td> -->
            <!-- <td>{!! $user->city !!}</td> -->
            <!-- <td>{!! $user->province !!}</td> -->
            <!-- <td>{!! $user->zip_kode !!}</td> -->
            <!-- <td>{!! $user->picture !!}</td> -->
            <!-- <td>{!! $user->remember_token !!}</td> -->
            <td>
                {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>