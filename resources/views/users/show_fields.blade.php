<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $user->id !!}</p>
</div>

<!-- User Group Id Field -->
<div class="form-group">
    {!! Form::label('user_group_id', 'User Group:') !!}
    <p>{!! App\Models\oc_user_group::find($user->user_group_id)->name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $user->name !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $user->email !!}</p>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{!! $user->password !!}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $user->username !!}</p>
</div>

<!-- Type Of Identity Field -->
<div class="form-group">
    {!! Form::label('type_of_identity', 'Type Of Identity:') !!}
    <p>{!! $user->type_of_identity !!}</p>
</div>

<!-- Identity Number Field -->
<div class="form-group">
    {!! Form::label('identity_number', 'Identity Number:') !!}
    <p>{!! $user->identity_number !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $user->gender !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $user->phone !!}</p>
</div>

<!-- Job Field -->
<div class="form-group">
    {!! Form::label('job', 'Job:') !!}
    <p>{!! $user->job !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $user->address !!}</p>
</div>

<!-- Districts Field -->
<div class="form-group">
    {!! Form::label('districts', 'Districts:') !!}
    <p>{!! $user->districts !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $user->city !!}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{!! $user->province !!}</p>
</div>

<!-- Zip Kode Field -->
<div class="form-group">
    {!! Form::label('zip_kode', 'Zip Kode:') !!}
    <p>{!! $user->zip_kode !!}</p>
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('picture', 'Picture:') !!}
    <p>{!! $user->picture !!}</p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{!! $user->remember_token !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $user->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $user->updated_at !!}</p>
</div>

