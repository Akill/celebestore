@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Option Value
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocOptionValue, ['route' => ['ocOptionValues.update', $ocOptionValue->id], 'method' => 'patch']) !!}

                        @include('oc_option_values.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection