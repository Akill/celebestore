<table class="table table-responsive" id="ocOptionValues-table">
    <thead>
        <th>Option Id</th>
        <th>Image</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocOptionValues as $ocOptionValue)
        <tr>
            <td>{!! $ocOptionValue->option_id !!}</td>
            <td>{!! $ocOptionValue->image !!}</td>
            <td>{!! $ocOptionValue->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocOptionValues.destroy', $ocOptionValue->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocOptionValues.show', [$ocOptionValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocOptionValues.edit', [$ocOptionValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>