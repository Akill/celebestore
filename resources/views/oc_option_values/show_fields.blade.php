<!-- Option Value Id Field -->
<div class="form-group">
    {!! Form::label('option_value_id', 'Option Value Id:') !!}
    <p>{!! $ocOptionValue->option_value_id !!}</p>
</div>

<!-- Option Id Field -->
<div class="form-group">
    {!! Form::label('option_id', 'Option Id:') !!}
    <p>{!! $ocOptionValue->option_id !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $ocOptionValue->image !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocOptionValue->sort_order !!}</p>
</div>

