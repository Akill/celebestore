@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Affiliate Login
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocAffiliateLogins.store']) !!}

                        @include('oc_affiliate_logins.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
