<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocAffiliateLogin->id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocAffiliateLogin->email !!}</p>
</div>

<!-- Ip Field -->
<div class="form-group">
    {!! Form::label('ip', 'Ip:') !!}
    <p>{!! $ocAffiliateLogin->ip !!}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{!! $ocAffiliateLogin->total !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocAffiliateLogin->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocAffiliateLogin->date_modified !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocAffiliateLogin->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocAffiliateLogin->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocAffiliateLogin->deleted_at !!}</p>
</div>

