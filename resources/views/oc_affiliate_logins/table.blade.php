<table class="table table-responsive" id="ocAffiliateLogins-table">
    <thead>
        <th>Email</th>
        <th>Ip</th>
        <th>Total</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocAffiliateLogins as $ocAffiliateLogin)
        <tr>
            <td>{!! $ocAffiliateLogin->email !!}</td>
            <td>{!! $ocAffiliateLogin->ip !!}</td>
            <td>{!! $ocAffiliateLogin->total !!}</td>
            <td>{!! $ocAffiliateLogin->date_added !!}</td>
            <td>{!! $ocAffiliateLogin->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocAffiliateLogins.destroy', $ocAffiliateLogin->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocAffiliateLogins.show', [$ocAffiliateLogin->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocAffiliateLogins.edit', [$ocAffiliateLogin->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>