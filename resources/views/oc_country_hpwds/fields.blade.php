<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso Code 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso_code_2', 'Iso Code 2:') !!}
    {!! Form::text('iso_code_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso Code 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso_code_3', 'Iso Code 3:') !!}
    {!! Form::text('iso_code_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Format Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('address_format', 'Address Format:') !!}
    {!! Form::textarea('address_format', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Required Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode_required', 'Postcode Required:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('postcode_required', false) !!}
        {!! Form::checkbox('postcode_required', '1', null) !!} 1
    </label>
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCountryHpwds.index') !!}" class="btn btn-default">Cancel</a>
</div>
