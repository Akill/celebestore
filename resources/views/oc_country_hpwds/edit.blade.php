@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Country Hpwd
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCountryHpwd, ['route' => ['ocCountryHpwds.update', $ocCountryHpwd->id], 'method' => 'patch']) !!}

                        @include('oc_country_hpwds.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection