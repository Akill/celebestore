<table class="table table-responsive" id="ocCountryHpwds-table">
    <thead>
        <th>Name</th>
        <th>Iso Code 2</th>
        <th>Iso Code 3</th>
        <th>Address Format</th>
        <th>Postcode Required</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCountryHpwds as $ocCountryHpwd)
        <tr>
            <td>{!! $ocCountryHpwd->name !!}</td>
            <td>{!! $ocCountryHpwd->iso_code_2 !!}</td>
            <td>{!! $ocCountryHpwd->iso_code_3 !!}</td>
            <td>{!! $ocCountryHpwd->address_format !!}</td>
            <td>{!! $ocCountryHpwd->postcode_required !!}</td>
            <td>{!! $ocCountryHpwd->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCountryHpwds.destroy', $ocCountryHpwd->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCountryHpwds.show', [$ocCountryHpwd->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCountryHpwds.edit', [$ocCountryHpwd->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>