@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Country Hpwd
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('oc_country_hpwds.show_fields')
                    <a href="{!! route('ocCountryHpwds.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
