<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCountryHpwd->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocCountryHpwd->name !!}</p>
</div>

<!-- Iso Code 2 Field -->
<div class="form-group">
    {!! Form::label('iso_code_2', 'Iso Code 2:') !!}
    <p>{!! $ocCountryHpwd->iso_code_2 !!}</p>
</div>

<!-- Iso Code 3 Field -->
<div class="form-group">
    {!! Form::label('iso_code_3', 'Iso Code 3:') !!}
    <p>{!! $ocCountryHpwd->iso_code_3 !!}</p>
</div>

<!-- Address Format Field -->
<div class="form-group">
    {!! Form::label('address_format', 'Address Format:') !!}
    <p>{!! $ocCountryHpwd->address_format !!}</p>
</div>

<!-- Postcode Required Field -->
<div class="form-group">
    {!! Form::label('postcode_required', 'Postcode Required:') !!}
    <p>{!! $ocCountryHpwd->postcode_required !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCountryHpwd->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCountryHpwd->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCountryHpwd->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCountryHpwd->deleted_at !!}</p>
</div>

