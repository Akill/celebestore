<table class="table table-responsive" id="ocModifications-table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Author</th>
        <th>Version</th>
        <th>Link</th>
        <th>Xml</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocModifications as $ocModification)
        <tr>
            <td>{!! $ocModification->name !!}</td>
            <td>{!! $ocModification->code !!}</td>
            <td>{!! $ocModification->author !!}</td>
            <td>{!! $ocModification->version !!}</td>
            <td>{!! $ocModification->link !!}</td>
            <td>{!! $ocModification->xml !!}</td>
            <td>{!! $ocModification->status !!}</td>
            <td>{!! $ocModification->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocModifications.destroy', $ocModification->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocModifications.show', [$ocModification->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocModifications.edit', [$ocModification->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>