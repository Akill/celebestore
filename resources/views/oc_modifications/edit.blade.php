@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Modification
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocModification, ['route' => ['ocModifications.update', $ocModification->id], 'method' => 'patch']) !!}

                        @include('oc_modifications.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection