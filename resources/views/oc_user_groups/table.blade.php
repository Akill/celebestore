<table class="table table-responsive" id="ocUserGroups-table">
    <thead>
        <th>Name</th>
        <th>Permission</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocUserGroups as $ocUserGroup)
        <tr>
            <td>{!! $ocUserGroup->name !!}</td>
            <td>{!! $ocUserGroup->permission !!}</td>
            <td>
                {!! Form::open(['route' => ['ocUserGroups.destroy', $ocUserGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocUserGroups.show', [$ocUserGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocUserGroups.edit', [$ocUserGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>