<!-- Return Id Field -->
<div class="form-group">
    {!! Form::label('return_id', 'Return Id:') !!}
    <p>{!! $ocReturn->return_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocReturn->order_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocReturn->product_id !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $ocReturn->customer_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $ocReturn->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $ocReturn->lastname !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $ocReturn->email !!}</p>
</div>

<!-- Telephone Field -->
<div class="form-group">
    {!! Form::label('telephone', 'Telephone:') !!}
    <p>{!! $ocReturn->telephone !!}</p>
</div>

<!-- Product Field -->
<div class="form-group">
    {!! Form::label('product', 'Product:') !!}
    <p>{!! $ocReturn->product !!}</p>
</div>

<!-- Model Field -->
<div class="form-group">
    {!! Form::label('model', 'Model:') !!}
    <p>{!! $ocReturn->model !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $ocReturn->quantity !!}</p>
</div>

<!-- Opened Field -->
<div class="form-group">
    {!! Form::label('opened', 'Opened:') !!}
    <p>{!! $ocReturn->opened !!}</p>
</div>

<!-- Return Reason Id Field -->
<div class="form-group">
    {!! Form::label('return_reason_id', 'Return Reason Id:') !!}
    <p>{!! $ocReturn->return_reason_id !!}</p>
</div>

<!-- Return Action Id Field -->
<div class="form-group">
    {!! Form::label('return_action_id', 'Return Action Id:') !!}
    <p>{!! $ocReturn->return_action_id !!}</p>
</div>

<!-- Return Status Id Field -->
<div class="form-group">
    {!! Form::label('return_status_id', 'Return Status Id:') !!}
    <p>{!! $ocReturn->return_status_id !!}</p>
</div>

<!-- Comment Field -->
<div class="form-group">
    {!! Form::label('comment', 'Comment:') !!}
    <p>{!! $ocReturn->comment !!}</p>
</div>

<!-- Date Ordered Field -->
<div class="form-group">
    {!! Form::label('date_ordered', 'Date Ordered:') !!}
    <p>{!! $ocReturn->date_ordered !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocReturn->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocReturn->date_modified !!}</p>
</div>

