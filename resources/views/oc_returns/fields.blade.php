<!-- Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('order_id', 'Order Id:') !!}
    {!! Form::number('order_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    {!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstname', 'Firstname:') !!}
    {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastname', 'Lastname:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Telephone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telephone', 'Telephone:') !!}
    {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product', 'Product:') !!}
    {!! Form::text('product', null, ['class' => 'form-control']) !!}
</div>

<!-- Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('model', 'Model:') !!}
    {!! Form::text('model', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Opened Field -->
<div class="form-group col-sm-6">
    {!! Form::label('opened', 'Opened:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('opened', false) !!}
        {!! Form::checkbox('opened', '1', null) !!} 1
    </label>
</div>

<!-- Return Reason Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_reason_id', 'Return Reason Id:') !!}
    {!! Form::number('return_reason_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Action Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_action_id', 'Return Action Id:') !!}
    {!! Form::number('return_action_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Return Status Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('return_status_id', 'Return Status Id:') !!}
    {!! Form::number('return_status_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Comment Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('comment', 'Comment:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Ordered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_ordered', 'Date Ordered:') !!}
    {!! Form::date('date_ordered', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Modified Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    {!! Form::date('date_modified', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocReturns.index') !!}" class="btn btn-default">Cancel</a>
</div>
