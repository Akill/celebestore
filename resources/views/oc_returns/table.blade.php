<table class="table table-responsive" id="ocReturns-table">
    <thead>
        <th>Order Id</th>
        <th>Product Id</th>
        <th>Customer Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
        <th>Telephone</th>
        <th>Product</th>
        <th>Model</th>
        <th>Quantity</th>
        <th>Opened</th>
        <th>Return Reason Id</th>
        <th>Return Action Id</th>
        <th>Return Status Id</th>
        <th>Comment</th>
        <th>Date Ordered</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocReturns as $ocReturn)
        <tr>
            <td>{!! $ocReturn->order_id !!}</td>
            <td>{!! $ocReturn->product_id !!}</td>
            <td>{!! $ocReturn->customer_id !!}</td>
            <td>{!! $ocReturn->firstname !!}</td>
            <td>{!! $ocReturn->lastname !!}</td>
            <td>{!! $ocReturn->email !!}</td>
            <td>{!! $ocReturn->telephone !!}</td>
            <td>{!! $ocReturn->product !!}</td>
            <td>{!! $ocReturn->model !!}</td>
            <td>{!! $ocReturn->quantity !!}</td>
            <td>{!! $ocReturn->opened !!}</td>
            <td>{!! $ocReturn->return_reason_id !!}</td>
            <td>{!! $ocReturn->return_action_id !!}</td>
            <td>{!! $ocReturn->return_status_id !!}</td>
            <td>{!! $ocReturn->comment !!}</td>
            <td>{!! $ocReturn->date_ordered !!}</td>
            <td>{!! $ocReturn->date_added !!}</td>
            <td>{!! $ocReturn->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocReturns.destroy', $ocReturn->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocReturns.show', [$ocReturn->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocReturns.edit', [$ocReturn->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>