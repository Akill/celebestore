<table class="table table-responsive" id="ocWeightClassDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Title</th>
        <th>Unit</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocWeightClassDescriptions as $ocWeightClassDescription)
        <tr>
            <td>{!! $ocWeightClassDescription->language_id !!}</td>
            <td>{!! $ocWeightClassDescription->title !!}</td>
            <td>{!! $ocWeightClassDescription->unit !!}</td>
            <td>
                {!! Form::open(['route' => ['ocWeightClassDescriptions.destroy', $ocWeightClassDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocWeightClassDescriptions.show', [$ocWeightClassDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocWeightClassDescriptions.edit', [$ocWeightClassDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>