<!-- Weight Class Id Field -->
<div class="form-group">
    {!! Form::label('weight_class_id', 'Weight Class Id:') !!}
    <p>{!! $ocWeightClassDescription->weight_class_id !!}</p>
</div>

<!-- Language Id Field -->
<div class="form-group">
    {!! Form::label('language_id', 'Language Id:') !!}
    <p>{!! $ocWeightClassDescription->language_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ocWeightClassDescription->title !!}</p>
</div>

<!-- Unit Field -->
<div class="form-group">
    {!! Form::label('unit', 'Unit:') !!}
    <p>{!! $ocWeightClassDescription->unit !!}</p>
</div>

