<table class="table table-responsive" id="ocCoupons-table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Type</th>
        <th>Discount</th>
        <th>Logged</th>
        <th>Shipping</th>
        <th>Total</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th>Uses Total</th>
        <th>Uses Customer</th>
        <th>Status</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCoupons as $ocCoupon)
        <tr>
            <td>{!! $ocCoupon->name !!}</td>
            <td>{!! $ocCoupon->code !!}</td>
            <td>{!! $ocCoupon->type !!}</td>
            <td>{!! $ocCoupon->discount !!}</td>
            <td>{!! $ocCoupon->logged !!}</td>
            <td>{!! $ocCoupon->shipping !!}</td>
            <td>{!! $ocCoupon->total !!}</td>
            <td>{!! $ocCoupon->date_start !!}</td>
            <td>{!! $ocCoupon->date_end !!}</td>
            <td>{!! $ocCoupon->uses_total !!}</td>
            <td>{!! $ocCoupon->uses_customer !!}</td>
            <td>{!! $ocCoupon->status !!}</td>
            <td>{!! $ocCoupon->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCoupons.destroy', $ocCoupon->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCoupons.show', [$ocCoupon->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCoupons.edit', [$ocCoupon->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>