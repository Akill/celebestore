<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCoupon->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocCoupon->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocCoupon->code !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $ocCoupon->type !!}</p>
</div>

<!-- Discount Field -->
<div class="form-group">
    {!! Form::label('discount', 'Discount:') !!}
    <p>{!! $ocCoupon->discount !!}</p>
</div>

<!-- Logged Field -->
<div class="form-group">
    {!! Form::label('logged', 'Logged:') !!}
    <p>{!! $ocCoupon->logged !!}</p>
</div>

<!-- Shipping Field -->
<div class="form-group">
    {!! Form::label('shipping', 'Shipping:') !!}
    <p>{!! $ocCoupon->shipping !!}</p>
</div>

<!-- Total Field -->
<div class="form-group">
    {!! Form::label('total', 'Total:') !!}
    <p>{!! $ocCoupon->total !!}</p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{!! $ocCoupon->date_start !!}</p>
</div>

<!-- Date End Field -->
<div class="form-group">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{!! $ocCoupon->date_end !!}</p>
</div>

<!-- Uses Total Field -->
<div class="form-group">
    {!! Form::label('uses_total', 'Uses Total:') !!}
    <p>{!! $ocCoupon->uses_total !!}</p>
</div>

<!-- Uses Customer Field -->
<div class="form-group">
    {!! Form::label('uses_customer', 'Uses Customer:') !!}
    <p>{!! $ocCoupon->uses_customer !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCoupon->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCoupon->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCoupon->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCoupon->deleted_at !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocCoupon->date_added !!}</p>
</div>

