<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Discount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('discount', 'Discount:') !!}
    {!! Form::number('discount', null, ['class' => 'form-control']) !!}
</div>

<!-- Logged Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logged', 'Logged:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('logged', false) !!}
        {!! Form::checkbox('logged', '1', null) !!} 1
    </label>
</div>

<!-- Shipping Field -->
<div class="form-group col-sm-6">
    {!! Form::label('shipping', 'Shipping:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('shipping', false) !!}
        {!! Form::checkbox('shipping', '1', null) !!} 1
    </label>
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Date Start:') !!}
    {!! Form::date('date_start', null, ['class' => 'form-control']) !!}
</div>

<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'Date End:') !!}
    {!! Form::date('date_end', null, ['class' => 'form-control']) !!}
</div>

<!-- Uses Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uses_total', 'Uses Total:') !!}
    {!! Form::number('uses_total', null, ['class' => 'form-control']) !!}
</div>

<!-- Uses Customer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uses_customer', 'Uses Customer:') !!}
    {!! Form::text('uses_customer', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Date Added Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_added', 'Date Added:') !!}
    {!! Form::date('date_added', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCoupons.index') !!}" class="btn btn-default">Cancel</a>
</div>
