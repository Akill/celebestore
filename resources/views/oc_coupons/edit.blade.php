@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Coupon
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCoupon, ['route' => ['ocCoupons.update', $ocCoupon->id], 'method' => 'patch']) !!}

                        @include('oc_coupons.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection