<table class="table table-responsive" id="ocVoucherHistories-table">
    <thead>
        <th>Voucher Id</th>
        <th>Order Id</th>
        <th>Amount</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocVoucherHistories as $ocVoucherHistory)
        <tr>
            <td>{!! $ocVoucherHistory->voucher_id !!}</td>
            <td>{!! $ocVoucherHistory->order_id !!}</td>
            <td>{!! $ocVoucherHistory->amount !!}</td>
            <td>{!! $ocVoucherHistory->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocVoucherHistories.destroy', $ocVoucherHistory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocVoucherHistories.show', [$ocVoucherHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocVoucherHistories.edit', [$ocVoucherHistory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>