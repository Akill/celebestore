<!-- Voucher History Id Field -->
<div class="form-group">
    {!! Form::label('voucher_history_id', 'Voucher History Id:') !!}
    <p>{!! $ocVoucherHistory->voucher_history_id !!}</p>
</div>

<!-- Voucher Id Field -->
<div class="form-group">
    {!! Form::label('voucher_id', 'Voucher Id:') !!}
    <p>{!! $ocVoucherHistory->voucher_id !!}</p>
</div>

<!-- Order Id Field -->
<div class="form-group">
    {!! Form::label('order_id', 'Order Id:') !!}
    <p>{!! $ocVoucherHistory->order_id !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $ocVoucherHistory->amount !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocVoucherHistory->date_added !!}</p>
</div>

