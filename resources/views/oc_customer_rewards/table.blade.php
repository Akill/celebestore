<table class="table table-responsive" id="ocCustomerRewards-table">
    <thead>
        <th>Customer Id</th>
        <th>Order Id</th>
        <th>Description</th>
        <th>Points</th>
        <th>Date Added</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerRewards as $ocCustomerReward)
        <tr>
            <td>{!! $ocCustomerReward->customer_id !!}</td>
            <td>{!! $ocCustomerReward->order_id !!}</td>
            <td>{!! $ocCustomerReward->description !!}</td>
            <td>{!! $ocCustomerReward->points !!}</td>
            <td>{!! $ocCustomerReward->date_added !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerRewards.destroy', $ocCustomerReward->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerRewards.show', [$ocCustomerReward->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerRewards.edit', [$ocCustomerReward->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>