<!-- Module Id Field -->
<div class="form-group">
    {!! Form::label('module_id', 'Module Id:') !!}
    <p>{!! $ocModule->module_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocModule->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocModule->code !!}</p>
</div>

<!-- Setting Field -->
<div class="form-group">
    {!! Form::label('setting', 'Setting:') !!}
    <p>{!! $ocModule->setting !!}</p>
</div>

