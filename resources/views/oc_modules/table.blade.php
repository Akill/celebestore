<table class="table table-responsive" id="ocModules-table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Setting</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocModules as $ocModule)
        <tr>
            <td>{!! $ocModule->name !!}</td>
            <td>{!! $ocModule->code !!}</td>
            <td>{!! $ocModule->setting !!}</td>
            <td>
                {!! Form::open(['route' => ['ocModules.destroy', $ocModule->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocModules.show', [$ocModule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocModules.edit', [$ocModule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>