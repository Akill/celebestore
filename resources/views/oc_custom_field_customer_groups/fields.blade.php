<!-- Customer Group Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    {!! Form::number('customer_group_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Required Field -->
<div class="form-group col-sm-6">
    {!! Form::label('required', 'Required:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('required', false) !!}
        {!! Form::checkbox('required', '1', null) !!} 1
    </label>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCustomFieldCustomerGroups.index') !!}" class="btn btn-default">Cancel</a>
</div>
