<table class="table table-responsive" id="ocCustomFieldCustomerGroups-table">
    <thead>
        <th>Customer Group Id</th>
        <th>Required</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomFieldCustomerGroups as $ocCustomFieldCustomerGroup)
        <tr>
            <td>{!! $ocCustomFieldCustomerGroup->customer_group_id !!}</td>
            <td>{!! $ocCustomFieldCustomerGroup->required !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomFieldCustomerGroups.destroy', $ocCustomFieldCustomerGroup->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomFieldCustomerGroups.show', [$ocCustomFieldCustomerGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomFieldCustomerGroups.edit', [$ocCustomFieldCustomerGroup->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>