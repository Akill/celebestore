<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->id !!}</p>
</div>

<!-- Customer Group Id Field -->
<div class="form-group">
    {!! Form::label('customer_group_id', 'Customer Group Id:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->customer_group_id !!}</p>
</div>

<!-- Required Field -->
<div class="form-group">
    {!! Form::label('required', 'Required:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->required !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCustomFieldCustomerGroup->deleted_at !!}</p>
</div>

