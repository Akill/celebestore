@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Custom Field Customer Group
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'ocCustomFieldCustomerGroups.store']) !!}

                        @include('oc_custom_field_customer_groups.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
