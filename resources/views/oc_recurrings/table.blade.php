<table class="table table-responsive" id="ocRecurrings-table">
    <thead>
        <th>Price</th>
        <th>Frequency</th>
        <th>Duration</th>
        <th>Cycle</th>
        <th>Trial Status</th>
        <th>Trial Price</th>
        <th>Trial Frequency</th>
        <th>Trial Duration</th>
        <th>Trial Cycle</th>
        <th>Status</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocRecurrings as $ocRecurring)
        <tr>
            <td>{!! $ocRecurring->price !!}</td>
            <td>{!! $ocRecurring->frequency !!}</td>
            <td>{!! $ocRecurring->duration !!}</td>
            <td>{!! $ocRecurring->cycle !!}</td>
            <td>{!! $ocRecurring->trial_status !!}</td>
            <td>{!! $ocRecurring->trial_price !!}</td>
            <td>{!! $ocRecurring->trial_frequency !!}</td>
            <td>{!! $ocRecurring->trial_duration !!}</td>
            <td>{!! $ocRecurring->trial_cycle !!}</td>
            <td>{!! $ocRecurring->status !!}</td>
            <td>{!! $ocRecurring->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocRecurrings.destroy', $ocRecurring->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocRecurrings.show', [$ocRecurring->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocRecurrings.edit', [$ocRecurring->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>