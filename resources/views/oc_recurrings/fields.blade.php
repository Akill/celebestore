<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Frequency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('frequency', 'Frequency:') !!}
    {!! Form::text('frequency', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::number('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Cycle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cycle', 'Cycle:') !!}
    {!! Form::number('cycle', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_status', 'Trial Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('trial_status', false) !!}
        {!! Form::checkbox('trial_status', '1', null) !!} 1
    </label>
</div>

<!-- Trial Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_price', 'Trial Price:') !!}
    {!! Form::number('trial_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Frequency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_frequency', 'Trial Frequency:') !!}
    {!! Form::text('trial_frequency', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_duration', 'Trial Duration:') !!}
    {!! Form::number('trial_duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Trial Cycle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trial_cycle', 'Trial Cycle:') !!}
    {!! Form::number('trial_cycle', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', false) !!}
        {!! Form::checkbox('status', '1', null) !!} 1
    </label>
</div>

<!-- Sort Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    {!! Form::number('sort_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocRecurrings.index') !!}" class="btn btn-default">Cancel</a>
</div>
