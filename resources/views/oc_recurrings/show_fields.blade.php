<!-- Recurring Id Field -->
<div class="form-group">
    {!! Form::label('recurring_id', 'Recurring Id:') !!}
    <p>{!! $ocRecurring->recurring_id !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $ocRecurring->price !!}</p>
</div>

<!-- Frequency Field -->
<div class="form-group">
    {!! Form::label('frequency', 'Frequency:') !!}
    <p>{!! $ocRecurring->frequency !!}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{!! $ocRecurring->duration !!}</p>
</div>

<!-- Cycle Field -->
<div class="form-group">
    {!! Form::label('cycle', 'Cycle:') !!}
    <p>{!! $ocRecurring->cycle !!}</p>
</div>

<!-- Trial Status Field -->
<div class="form-group">
    {!! Form::label('trial_status', 'Trial Status:') !!}
    <p>{!! $ocRecurring->trial_status !!}</p>
</div>

<!-- Trial Price Field -->
<div class="form-group">
    {!! Form::label('trial_price', 'Trial Price:') !!}
    <p>{!! $ocRecurring->trial_price !!}</p>
</div>

<!-- Trial Frequency Field -->
<div class="form-group">
    {!! Form::label('trial_frequency', 'Trial Frequency:') !!}
    <p>{!! $ocRecurring->trial_frequency !!}</p>
</div>

<!-- Trial Duration Field -->
<div class="form-group">
    {!! Form::label('trial_duration', 'Trial Duration:') !!}
    <p>{!! $ocRecurring->trial_duration !!}</p>
</div>

<!-- Trial Cycle Field -->
<div class="form-group">
    {!! Form::label('trial_cycle', 'Trial Cycle:') !!}
    <p>{!! $ocRecurring->trial_cycle !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocRecurring->status !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocRecurring->sort_order !!}</p>
</div>

