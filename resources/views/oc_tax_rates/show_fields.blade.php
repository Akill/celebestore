<!-- Tax Rate Id Field -->
<div class="form-group">
    {!! Form::label('tax_rate_id', 'Tax Rate Id:') !!}
    <p>{!! $ocTaxRate->tax_rate_id !!}</p>
</div>

<!-- Geo Zone Id Field -->
<div class="form-group">
    {!! Form::label('geo_zone_id', 'Geo Zone Id:') !!}
    <p>{!! $ocTaxRate->geo_zone_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocTaxRate->name !!}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', 'Rate:') !!}
    <p>{!! $ocTaxRate->rate !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $ocTaxRate->type !!}</p>
</div>

<!-- Date Added Field -->
<div class="form-group">
    {!! Form::label('date_added', 'Date Added:') !!}
    <p>{!! $ocTaxRate->date_added !!}</p>
</div>

<!-- Date Modified Field -->
<div class="form-group">
    {!! Form::label('date_modified', 'Date Modified:') !!}
    <p>{!! $ocTaxRate->date_modified !!}</p>
</div>

