@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Tax Rate
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocTaxRate, ['route' => ['ocTaxRates.update', $ocTaxRate->id], 'method' => 'patch']) !!}

                        @include('oc_tax_rates.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection