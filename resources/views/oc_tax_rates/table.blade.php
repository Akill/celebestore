<table class="table table-responsive" id="ocTaxRates-table">
    <thead>
        <th>Geo Zone Id</th>
        <th>Name</th>
        <th>Rate</th>
        <th>Type</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocTaxRates as $ocTaxRate)
        <tr>
            <td>{!! $ocTaxRate->geo_zone_id !!}</td>
            <td>{!! $ocTaxRate->name !!}</td>
            <td>{!! $ocTaxRate->rate !!}</td>
            <td>{!! $ocTaxRate->type !!}</td>
            <td>{!! $ocTaxRate->date_added !!}</td>
            <td>{!! $ocTaxRate->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocTaxRates.destroy', $ocTaxRate->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocTaxRates.show', [$ocTaxRate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocTaxRates.edit', [$ocTaxRate->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>