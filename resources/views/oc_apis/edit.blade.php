@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Api
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocApi, ['route' => ['ocApis.update', $ocApi->id], 'method' => 'patch']) !!}

                        @include('oc_apis.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection