<table class="table table-responsive" id="ocApis-table">
    <thead>
        <th>Username</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Password</th>
        <th>Status</th>
        <th>Date Added</th>
        <th>Date Modified</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocApis as $ocApi)
        <tr>
            <td>{!! $ocApi->username !!}</td>
            <td>{!! $ocApi->firstname !!}</td>
            <td>{!! $ocApi->lastname !!}</td>
            <td>{!! $ocApi->password !!}</td>
            <td>{!! $ocApi->status !!}</td>
            <td>{!! $ocApi->date_added !!}</td>
            <td>{!! $ocApi->date_modified !!}</td>
            <td>
                {!! Form::open(['route' => ['ocApis.destroy', $ocApi->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocApis.show', [$ocApi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocApis.edit', [$ocApi->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>