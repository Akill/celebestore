<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ocCountry->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $ocCountry->name !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocCountry->code !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocCountry->status !!}</p>
</div>

<!-- Address Format Field -->
<div class="form-group">
    {!! Form::label('address_format', 'Address Format:') !!}
    <p>{!! $ocCountry->address_format !!}</p>
</div>

<!-- Iso Code 2 Field -->
<div class="form-group">
    {!! Form::label('iso_code_2', 'Iso Code 2:') !!}
    <p>{!! $ocCountry->iso_code_2 !!}</p>
</div>

<!-- Iso Code 3 Field -->
<div class="form-group">
    {!! Form::label('iso_code_3', 'Iso Code 3:') !!}
    <p>{!! $ocCountry->iso_code_3 !!}</p>
</div>

<!-- Postcode Required Field -->
<div class="form-group">
    {!! Form::label('postcode_required', 'Postcode Required:') !!}
    <p>{!! $ocCountry->postcode_required !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ocCountry->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ocCountry->updated_at !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $ocCountry->deleted_at !!}</p>
</div>

