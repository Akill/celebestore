@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Country
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCountry, ['route' => ['ocCountries.update', $ocCountry->id], 'method' => 'patch']) !!}

                        @include('oc_countries.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection