<table class="table table-responsive" id="ocCountries-table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Status</th>
        <th>Address Format</th>
        <th>Iso Code 2</th>
        <th>Iso Code 3</th>
        <th>Postcode Required</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCountries as $ocCountry)
        <tr>
            <td>{!! $ocCountry->name !!}</td>
            <td>{!! $ocCountry->code !!}</td>
            <td>{!! $ocCountry->status !!}</td>
            <td>{!! $ocCountry->address_format !!}</td>
            <td>{!! $ocCountry->iso_code_2 !!}</td>
            <td>{!! $ocCountry->iso_code_3 !!}</td>
            <td>{!! $ocCountry->postcode_required !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCountries.destroy', $ocCountry->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCountries.show', [$ocCountry->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCountries.edit', [$ocCountry->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>