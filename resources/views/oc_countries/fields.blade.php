<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('code', 'Code:') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::text('status', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Format Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_format', 'Address Format:') !!}
    {!! Form::text('address_format', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso Code 2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso_code_2', 'Iso Code 2:') !!}
    {!! Form::text('iso_code_2', null, ['class' => 'form-control']) !!}
</div>

<!-- Iso Code 3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('iso_code_3', 'Iso Code 3:') !!}
    {!! Form::text('iso_code_3', null, ['class' => 'form-control']) !!}
</div>

<!-- Postcode Required Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postcode_required', 'Postcode Required:') !!}
    {!! Form::text('postcode_required', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocCountries.index') !!}" class="btn btn-default">Cancel</a>
</div>
