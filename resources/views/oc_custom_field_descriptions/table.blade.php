<table class="table table-responsive" id="ocCustomFieldDescriptions-table">
    <thead>
        <th>Language Id</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomFieldDescriptions as $ocCustomFieldDescription)
        <tr>
            <td>{!! $ocCustomFieldDescription->language_id !!}</td>
            <td>{!! $ocCustomFieldDescription->name !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomFieldDescriptions.destroy', $ocCustomFieldDescription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomFieldDescriptions.show', [$ocCustomFieldDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomFieldDescriptions.edit', [$ocCustomFieldDescription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>