<table class="table table-responsive" id="ocFilters-table">
    <thead>
        <th>Filter Group Id</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocFilters as $ocFilter)
        <tr>
            <td>{!! $ocFilter->filter_group_id !!}</td>
            <td>{!! $ocFilter->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocFilters.destroy', $ocFilter->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocFilters.show', [$ocFilter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocFilters.edit', [$ocFilter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>