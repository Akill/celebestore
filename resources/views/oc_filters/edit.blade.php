@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Filter
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocFilter, ['route' => ['ocFilters.update', $ocFilter->id], 'method' => 'patch']) !!}

                        @include('oc_filters.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection