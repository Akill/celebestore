@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Setting
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocSetting, ['route' => ['ocSettings.update', $ocSetting->id], 'method' => 'patch']) !!}

                        @include('oc_settings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection