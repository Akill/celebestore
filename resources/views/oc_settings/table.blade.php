<table class="table table-responsive" id="ocSettings-table">
    <thead>
        <th>Store Id</th>
        <th>Code</th>
        <th>Key</th>
        <th>Value</th>
        <th>Serialized</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocSettings as $ocSetting)
        <tr>
            <td>{!! $ocSetting->store_id !!}</td>
            <td>{!! $ocSetting->code !!}</td>
            <td>{!! $ocSetting->key !!}</td>
            <td>{!! $ocSetting->value !!}</td>
            <td>{!! $ocSetting->serialized !!}</td>
            <td>
                {!! Form::open(['route' => ['ocSettings.destroy', $ocSetting->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocSettings.show', [$ocSetting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocSettings.edit', [$ocSetting->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>