<!-- Setting Id Field -->
<div class="form-group">
    {!! Form::label('setting_id', 'Setting Id:') !!}
    <p>{!! $ocSetting->setting_id !!}</p>
</div>

<!-- Store Id Field -->
<div class="form-group">
    {!! Form::label('store_id', 'Store Id:') !!}
    <p>{!! $ocSetting->store_id !!}</p>
</div>

<!-- Code Field -->
<div class="form-group">
    {!! Form::label('code', 'Code:') !!}
    <p>{!! $ocSetting->code !!}</p>
</div>

<!-- Key Field -->
<div class="form-group">
    {!! Form::label('key', 'Key:') !!}
    <p>{!! $ocSetting->key !!}</p>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $ocSetting->value !!}</p>
</div>

<!-- Serialized Field -->
<div class="form-group">
    {!! Form::label('serialized', 'Serialized:') !!}
    <p>{!! $ocSetting->serialized !!}</p>
</div>

