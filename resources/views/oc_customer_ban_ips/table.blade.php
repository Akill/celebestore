<table class="table table-responsive" id="ocCustomerBanIps-table">
    <thead>
        <th>Ip</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomerBanIps as $ocCustomerBanIp)
        <tr>
            <td>{!! $ocCustomerBanIp->ip !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomerBanIps.destroy', $ocCustomerBanIp->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomerBanIps.show', [$ocCustomerBanIp->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomerBanIps.edit', [$ocCustomerBanIp->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>