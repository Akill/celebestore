@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Customer Ban Ip
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocCustomerBanIp, ['route' => ['ocCustomerBanIps.update', $ocCustomerBanIp->id], 'method' => 'patch']) !!}

                        @include('oc_customer_ban_ips.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection