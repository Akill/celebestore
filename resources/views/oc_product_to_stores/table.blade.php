<table class="table table-responsive" id="ocProductToStores-table">
    <thead>
        <th>Store Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductToStores as $ocProductToStore)
        <tr>
            <td>{!! $ocProductToStore->store_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductToStores.destroy', $ocProductToStore->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductToStores.show', [$ocProductToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductToStores.edit', [$ocProductToStore->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>