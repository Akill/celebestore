<!-- Product Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_option_id', 'Product Option Id:') !!}
    {!! Form::number('product_option_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_id', 'Product Id:') !!}
    {!! Form::number('product_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Option Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_id', 'Option Id:') !!}
    {!! Form::number('option_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Option Value Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_value_id', 'Option Value Id:') !!}
    {!! Form::number('option_value_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtract Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtract', 'Subtract:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('subtract', false) !!}
        {!! Form::checkbox('subtract', '1', null) !!} 1
    </label>
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Prefix Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price_prefix', 'Price Prefix:') !!}
    {!! Form::text('price_prefix', null, ['class' => 'form-control']) !!}
</div>

<!-- Points Field -->
<div class="form-group col-sm-6">
    {!! Form::label('points', 'Points:') !!}
    {!! Form::number('points', null, ['class' => 'form-control']) !!}
</div>

<!-- Points Prefix Field -->
<div class="form-group col-sm-6">
    {!! Form::label('points_prefix', 'Points Prefix:') !!}
    {!! Form::text('points_prefix', null, ['class' => 'form-control']) !!}
</div>

<!-- Weight Field -->
<div class="form-group col-sm-6">
    {!! Form::label('weight', 'Weight:') !!}
    {!! Form::number('weight', null, ['class' => 'form-control']) !!}
</div>

<!-- Weight Prefix Field -->
<div class="form-group col-sm-6">
    {!! Form::label('weight_prefix', 'Weight Prefix:') !!}
    {!! Form::text('weight_prefix', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocProductOptionValues.index') !!}" class="btn btn-default">Cancel</a>
</div>
