<!-- Product Option Value Id Field -->
<div class="form-group">
    {!! Form::label('product_option_value_id', 'Product Option Value Id:') !!}
    <p>{!! $ocProductOptionValue->product_option_value_id !!}</p>
</div>

<!-- Product Option Id Field -->
<div class="form-group">
    {!! Form::label('product_option_id', 'Product Option Id:') !!}
    <p>{!! $ocProductOptionValue->product_option_id !!}</p>
</div>

<!-- Product Id Field -->
<div class="form-group">
    {!! Form::label('product_id', 'Product Id:') !!}
    <p>{!! $ocProductOptionValue->product_id !!}</p>
</div>

<!-- Option Id Field -->
<div class="form-group">
    {!! Form::label('option_id', 'Option Id:') !!}
    <p>{!! $ocProductOptionValue->option_id !!}</p>
</div>

<!-- Option Value Id Field -->
<div class="form-group">
    {!! Form::label('option_value_id', 'Option Value Id:') !!}
    <p>{!! $ocProductOptionValue->option_value_id !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $ocProductOptionValue->quantity !!}</p>
</div>

<!-- Subtract Field -->
<div class="form-group">
    {!! Form::label('subtract', 'Subtract:') !!}
    <p>{!! $ocProductOptionValue->subtract !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $ocProductOptionValue->price !!}</p>
</div>

<!-- Price Prefix Field -->
<div class="form-group">
    {!! Form::label('price_prefix', 'Price Prefix:') !!}
    <p>{!! $ocProductOptionValue->price_prefix !!}</p>
</div>

<!-- Points Field -->
<div class="form-group">
    {!! Form::label('points', 'Points:') !!}
    <p>{!! $ocProductOptionValue->points !!}</p>
</div>

<!-- Points Prefix Field -->
<div class="form-group">
    {!! Form::label('points_prefix', 'Points Prefix:') !!}
    <p>{!! $ocProductOptionValue->points_prefix !!}</p>
</div>

<!-- Weight Field -->
<div class="form-group">
    {!! Form::label('weight', 'Weight:') !!}
    <p>{!! $ocProductOptionValue->weight !!}</p>
</div>

<!-- Weight Prefix Field -->
<div class="form-group">
    {!! Form::label('weight_prefix', 'Weight Prefix:') !!}
    <p>{!! $ocProductOptionValue->weight_prefix !!}</p>
</div>

