<table class="table table-responsive" id="ocProductOptionValues-table">
    <thead>
        <th>Product Option Id</th>
        <th>Product Id</th>
        <th>Option Id</th>
        <th>Option Value Id</th>
        <th>Quantity</th>
        <th>Subtract</th>
        <th>Price</th>
        <th>Price Prefix</th>
        <th>Points</th>
        <th>Points Prefix</th>
        <th>Weight</th>
        <th>Weight Prefix</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocProductOptionValues as $ocProductOptionValue)
        <tr>
            <td>{!! $ocProductOptionValue->product_option_id !!}</td>
            <td>{!! $ocProductOptionValue->product_id !!}</td>
            <td>{!! $ocProductOptionValue->option_id !!}</td>
            <td>{!! $ocProductOptionValue->option_value_id !!}</td>
            <td>{!! $ocProductOptionValue->quantity !!}</td>
            <td>{!! $ocProductOptionValue->subtract !!}</td>
            <td>{!! $ocProductOptionValue->price !!}</td>
            <td>{!! $ocProductOptionValue->price_prefix !!}</td>
            <td>{!! $ocProductOptionValue->points !!}</td>
            <td>{!! $ocProductOptionValue->points_prefix !!}</td>
            <td>{!! $ocProductOptionValue->weight !!}</td>
            <td>{!! $ocProductOptionValue->weight_prefix !!}</td>
            <td>
                {!! Form::open(['route' => ['ocProductOptionValues.destroy', $ocProductOptionValue->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocProductOptionValues.show', [$ocProductOptionValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocProductOptionValues.edit', [$ocProductOptionValue->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>