<!-- Information Id Field -->
<div class="form-group">
    {!! Form::label('information_id', 'Information Id:') !!}
    <p>{!! $ocInformation->information_id !!}</p>
</div>

<!-- Bottom Field -->
<div class="form-group">
    {!! Form::label('bottom', 'Bottom:') !!}
    <p>{!! $ocInformation->bottom !!}</p>
</div>

<!-- Sort Order Field -->
<div class="form-group">
    {!! Form::label('sort_order', 'Sort Order:') !!}
    <p>{!! $ocInformation->sort_order !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $ocInformation->status !!}</p>
</div>

