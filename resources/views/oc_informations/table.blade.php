<table class="table table-responsive" id="ocInformations-table">
    <thead>
        <th>Bottom</th>
        <th>Sort Order</th>
        <th>Status</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocInformations as $ocInformation)
        <tr>
            <td>{!! $ocInformation->bottom !!}</td>
            <td>{!! $ocInformation->sort_order !!}</td>
            <td>{!! $ocInformation->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ocInformations.destroy', $ocInformation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocInformations.show', [$ocInformation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocInformations.edit', [$ocInformation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>