@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Oc Tax Rule
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($ocTaxRule, ['route' => ['ocTaxRules.update', $ocTaxRule->id], 'method' => 'patch']) !!}

                        @include('oc_tax_rules.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection