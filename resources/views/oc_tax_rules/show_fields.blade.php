<!-- Tax Rule Id Field -->
<div class="form-group">
    {!! Form::label('tax_rule_id', 'Tax Rule Id:') !!}
    <p>{!! $ocTaxRule->tax_rule_id !!}</p>
</div>

<!-- Tax Class Id Field -->
<div class="form-group">
    {!! Form::label('tax_class_id', 'Tax Class Id:') !!}
    <p>{!! $ocTaxRule->tax_class_id !!}</p>
</div>

<!-- Tax Rate Id Field -->
<div class="form-group">
    {!! Form::label('tax_rate_id', 'Tax Rate Id:') !!}
    <p>{!! $ocTaxRule->tax_rate_id !!}</p>
</div>

<!-- Based Field -->
<div class="form-group">
    {!! Form::label('based', 'Based:') !!}
    <p>{!! $ocTaxRule->based !!}</p>
</div>

<!-- Priority Field -->
<div class="form-group">
    {!! Form::label('priority', 'Priority:') !!}
    <p>{!! $ocTaxRule->priority !!}</p>
</div>

