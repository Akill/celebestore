<table class="table table-responsive" id="ocTaxRules-table">
    <thead>
        <th>Tax Class Id</th>
        <th>Tax Rate Id</th>
        <th>Based</th>
        <th>Priority</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocTaxRules as $ocTaxRule)
        <tr>
            <td>{!! $ocTaxRule->tax_class_id !!}</td>
            <td>{!! $ocTaxRule->tax_rate_id !!}</td>
            <td>{!! $ocTaxRule->based !!}</td>
            <td>{!! $ocTaxRule->priority !!}</td>
            <td>
                {!! Form::open(['route' => ['ocTaxRules.destroy', $ocTaxRule->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocTaxRules.show', [$ocTaxRule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocTaxRules.edit', [$ocTaxRule->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>