<!-- Tax Class Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_class_id', 'Tax Class Id:') !!}
    {!! Form::number('tax_class_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Tax Rate Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_rate_id', 'Tax Rate Id:') !!}
    {!! Form::number('tax_rate_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Based Field -->
<div class="form-group col-sm-6">
    {!! Form::label('based', 'Based:') !!}
    {!! Form::text('based', null, ['class' => 'form-control']) !!}
</div>

<!-- Priority Field -->
<div class="form-group col-sm-6">
    {!! Form::label('priority', 'Priority:') !!}
    {!! Form::number('priority', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ocTaxRules.index') !!}" class="btn btn-default">Cancel</a>
</div>
