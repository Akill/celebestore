<table class="table table-responsive" id="ocCustomFields-table">
    <thead>
        <th>Type</th>
        <th>Value</th>
        <th>Location</th>
        <th>Status</th>
        <th>Sort Order</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ocCustomFields as $ocCustomField)
        <tr>
            <td>{!! $ocCustomField->type !!}</td>
            <td>{!! $ocCustomField->value !!}</td>
            <td>{!! $ocCustomField->location !!}</td>
            <td>{!! $ocCustomField->status !!}</td>
            <td>{!! $ocCustomField->sort_order !!}</td>
            <td>
                {!! Form::open(['route' => ['ocCustomFields.destroy', $ocCustomField->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ocCustomFields.show', [$ocCustomField->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ocCustomFields.edit', [$ocCustomField->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>