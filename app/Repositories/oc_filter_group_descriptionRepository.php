<?php

namespace App\Repositories;

use App\Models\oc_filter_group_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_filter_group_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_filter_group_description findWithoutFail($id, $columns = ['*'])
 * @method oc_filter_group_description find($id, $columns = ['*'])
 * @method oc_filter_group_description first($columns = ['*'])
*/
class oc_filter_group_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_filter_group_description::class;
    }
}
