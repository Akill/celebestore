<?php

namespace App\Repositories;

use App\Models\oc_customer_ip;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_ipRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_ip findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_ip find($id, $columns = ['*'])
 * @method oc_customer_ip first($columns = ['*'])
*/
class oc_customer_ipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'ip',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_ip::class;
    }
}
