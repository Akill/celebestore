<?php

namespace App\Repositories;

use App\Models\oc_weight_class_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_weight_class_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:03 am UTC
 *
 * @method oc_weight_class_description findWithoutFail($id, $columns = ['*'])
 * @method oc_weight_class_description find($id, $columns = ['*'])
 * @method oc_weight_class_description first($columns = ['*'])
*/
class oc_weight_class_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'title',
        'unit'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_weight_class_description::class;
    }
}
