<?php

namespace App\Repositories;

use App\Models\oc_option_value;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_option_valueRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_option_value findWithoutFail($id, $columns = ['*'])
 * @method oc_option_value find($id, $columns = ['*'])
 * @method oc_option_value first($columns = ['*'])
*/
class oc_option_valueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'option_id',
        'image',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_option_value::class;
    }
}
