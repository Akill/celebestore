<?php

namespace App\Repositories;

use App\Models\oc_custom_field_value;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_custom_field_valueRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_custom_field_value findWithoutFail($id, $columns = ['*'])
 * @method oc_custom_field_value find($id, $columns = ['*'])
 * @method oc_custom_field_value first($columns = ['*'])
*/
class oc_custom_field_valueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'custom_field_id',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_custom_field_value::class;
    }
}
