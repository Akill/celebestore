<?php

namespace App\Repositories;

use App\Models\oc_product;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_productRepository
 * @package App\Repositories
 * @version August 28, 2017, 7:43 pm UTC
 *
 * @method oc_product findWithoutFail($id, $columns = ['*'])
 * @method oc_product find($id, $columns = ['*'])
 * @method oc_product first($columns = ['*'])
*/
class oc_productRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'model',
        'sku',
        'upc',
        'ean',
        'jan',
        'isbn',
        'mpn',
        'location',
        'quantity',
        'stock_status_id',
        'image',
        'manufacturer_id',
        'shipping',
        'price',
        'points',
        'tax_class_id',
        'date_available',
        'weight',
        'weight_class_id',
        'length',
        'width',
        'height',
        'length_class_id',
        'subtract',
        'minimum',
        'sort_order',
        'status',
        'viewed',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product::class;
    }
}
