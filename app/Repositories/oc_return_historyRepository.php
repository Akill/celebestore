<?php

namespace App\Repositories;

use App\Models\oc_return_history;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_return_historyRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_return_history findWithoutFail($id, $columns = ['*'])
 * @method oc_return_history find($id, $columns = ['*'])
 * @method oc_return_history first($columns = ['*'])
*/
class oc_return_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'return_id',
        'return_status_id',
        'notify',
        'comment',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_return_history::class;
    }
}
