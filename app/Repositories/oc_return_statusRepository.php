<?php

namespace App\Repositories;

use App\Models\oc_return_status;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_return_statusRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_return_status findWithoutFail($id, $columns = ['*'])
 * @method oc_return_status find($id, $columns = ['*'])
 * @method oc_return_status first($columns = ['*'])
*/
class oc_return_statusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_return_status::class;
    }
}
