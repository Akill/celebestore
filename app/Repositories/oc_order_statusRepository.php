<?php

namespace App\Repositories;

use App\Models\oc_order_status;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_statusRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_status findWithoutFail($id, $columns = ['*'])
 * @method oc_order_status find($id, $columns = ['*'])
 * @method oc_order_status first($columns = ['*'])
*/
class oc_order_statusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_status::class;
    }
}
