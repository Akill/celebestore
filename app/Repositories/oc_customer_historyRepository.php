<?php

namespace App\Repositories;

use App\Models\oc_customer_history;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_historyRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_history findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_history find($id, $columns = ['*'])
 * @method oc_customer_history first($columns = ['*'])
*/
class oc_customer_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'comment',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_history::class;
    }
}
