<?php

namespace App\Repositories;

use App\Models\oc_weight_class;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_weight_classRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:03 am UTC
 *
 * @method oc_weight_class findWithoutFail($id, $columns = ['*'])
 * @method oc_weight_class find($id, $columns = ['*'])
 * @method oc_weight_class first($columns = ['*'])
*/
class oc_weight_classRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_weight_class::class;
    }
}
