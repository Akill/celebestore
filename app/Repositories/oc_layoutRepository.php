<?php

namespace App\Repositories;

use App\Models\oc_layout;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_layoutRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_layout findWithoutFail($id, $columns = ['*'])
 * @method oc_layout find($id, $columns = ['*'])
 * @method oc_layout first($columns = ['*'])
*/
class oc_layoutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_layout::class;
    }
}
