<?php

namespace App\Repositories;

use App\Models\oc_url_alias;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_url_aliasRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_url_alias findWithoutFail($id, $columns = ['*'])
 * @method oc_url_alias find($id, $columns = ['*'])
 * @method oc_url_alias first($columns = ['*'])
*/
class oc_url_aliasRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'query',
        'keyword'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_url_alias::class;
    }
}
