<?php

namespace App\Repositories;

use App\Models\oc_category;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_categoryRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_category findWithoutFail($id, $columns = ['*'])
 * @method oc_category find($id, $columns = ['*'])
 * @method oc_category first($columns = ['*'])
*/
class oc_categoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'parent_id',
        'top',
        'column',
        'sort_order',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category::class;
    }
}
