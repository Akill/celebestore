<?php

namespace App\Repositories;

use App\Models\oc_country;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_countryRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_country findWithoutFail($id, $columns = ['*'])
 * @method oc_country find($id, $columns = ['*'])
 * @method oc_country first($columns = ['*'])
*/
class oc_countryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'status',
        'address_format',
        'iso_code_2',
        'iso_code_3',
        'postcode_required'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_country::class;
    }
}
