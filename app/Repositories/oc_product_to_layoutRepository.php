<?php

namespace App\Repositories;

use App\Models\oc_product_to_layout;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_to_layoutRepository
 * @package App\Repositories
 * @version September 2, 2017, 4:51 pm UTC
 *
 * @method oc_product_to_layout findWithoutFail($id, $columns = ['*'])
 * @method oc_product_to_layout find($id, $columns = ['*'])
 * @method oc_product_to_layout first($columns = ['*'])
*/
class oc_product_to_layoutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'layout_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_to_layout::class;
    }
}
