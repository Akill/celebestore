<?php

namespace App\Repositories;

use App\Models\oc_country_hpwd;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_country_hpwdRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_country_hpwd findWithoutFail($id, $columns = ['*'])
 * @method oc_country_hpwd find($id, $columns = ['*'])
 * @method oc_country_hpwd first($columns = ['*'])
*/
class oc_country_hpwdRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'iso_code_2',
        'iso_code_3',
        'address_format',
        'postcode_required',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_country_hpwd::class;
    }
}
