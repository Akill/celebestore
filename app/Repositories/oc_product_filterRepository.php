<?php

namespace App\Repositories;

use App\Models\oc_product_filter;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_filterRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_filter findWithoutFail($id, $columns = ['*'])
 * @method oc_product_filter find($id, $columns = ['*'])
 * @method oc_product_filter first($columns = ['*'])
*/
class oc_product_filterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_filter::class;
    }
}
