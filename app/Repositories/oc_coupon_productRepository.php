<?php

namespace App\Repositories;

use App\Models\oc_coupon_product;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_coupon_productRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_coupon_product findWithoutFail($id, $columns = ['*'])
 * @method oc_coupon_product find($id, $columns = ['*'])
 * @method oc_coupon_product first($columns = ['*'])
*/
class oc_coupon_productRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coupon_id',
        'product_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_coupon_product::class;
    }
}
