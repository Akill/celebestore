<?php

namespace App\Repositories;

use App\Models\oc_tax_rule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_tax_ruleRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_tax_rule findWithoutFail($id, $columns = ['*'])
 * @method oc_tax_rule find($id, $columns = ['*'])
 * @method oc_tax_rule first($columns = ['*'])
*/
class oc_tax_ruleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tax_class_id',
        'tax_rate_id',
        'based',
        'priority'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_tax_rule::class;
    }
}
