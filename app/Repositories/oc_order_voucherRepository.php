<?php

namespace App\Repositories;

use App\Models\oc_order_voucher;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_voucherRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_voucher findWithoutFail($id, $columns = ['*'])
 * @method oc_order_voucher find($id, $columns = ['*'])
 * @method oc_order_voucher first($columns = ['*'])
*/
class oc_order_voucherRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'voucher_id',
        'description',
        'code',
        'from_name',
        'from_email',
        'to_name',
        'to_email',
        'voucher_theme_id',
        'message',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_voucher::class;
    }
}
