<?php

namespace App\Repositories;

use App\Models\oc_order_recurring;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_recurringRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_recurring findWithoutFail($id, $columns = ['*'])
 * @method oc_order_recurring find($id, $columns = ['*'])
 * @method oc_order_recurring first($columns = ['*'])
*/
class oc_order_recurringRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'reference',
        'product_id',
        'product_name',
        'product_quantity',
        'recurring_id',
        'recurring_name',
        'recurring_description',
        'recurring_frequency',
        'recurring_cycle',
        'recurring_duration',
        'recurring_price',
        'trial',
        'trial_frequency',
        'trial_cycle',
        'trial_duration',
        'trial_price',
        'status',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_recurring::class;
    }
}
