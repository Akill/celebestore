<?php

namespace App\Repositories;

use App\Models\oc_affiliate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_affiliateRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_affiliate findWithoutFail($id, $columns = ['*'])
 * @method oc_affiliate find($id, $columns = ['*'])
 * @method oc_affiliate first($columns = ['*'])
*/
class oc_affiliateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sub_district_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'password',
        'salt',
        'company',
        'website',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'code',
        'commission',
        'tax',
        'payment',
        'cheque',
        'paypal',
        'bank_name',
        'bank_branch_number',
        'bank_swift_code',
        'bank_account_name',
        'bank_account_number',
        'ip',
        'status',
        'approved',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_affiliate::class;
    }
}
