<?php

namespace App\Repositories;

use App\Models\oc_confirm;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_confirmRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_confirm findWithoutFail($id, $columns = ['*'])
 * @method oc_confirm find($id, $columns = ['*'])
 * @method oc_confirm first($columns = ['*'])
*/
class oc_confirmRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'order_id',
        'payment_date',
        'total_amount',
        'destination_bank',
        'resi',
        'payment_method',
        'sender_name',
        'bank_origin',
        'code',
        'no_receipt',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_confirm::class;
    }
}
