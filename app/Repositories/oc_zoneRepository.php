<?php

namespace App\Repositories;

use App\Models\oc_zone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_zoneRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:03 am UTC
 *
 * @method oc_zone findWithoutFail($id, $columns = ['*'])
 * @method oc_zone find($id, $columns = ['*'])
 * @method oc_zone first($columns = ['*'])
*/
class oc_zoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'name',
        'code',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_zone::class;
    }
}
