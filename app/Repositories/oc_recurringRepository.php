<?php

namespace App\Repositories;

use App\Models\oc_recurring;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_recurringRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_recurring findWithoutFail($id, $columns = ['*'])
 * @method oc_recurring find($id, $columns = ['*'])
 * @method oc_recurring first($columns = ['*'])
*/
class oc_recurringRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'price',
        'frequency',
        'duration',
        'cycle',
        'trial_status',
        'trial_price',
        'trial_frequency',
        'trial_duration',
        'trial_cycle',
        'status',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_recurring::class;
    }
}
