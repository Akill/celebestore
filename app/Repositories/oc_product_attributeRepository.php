<?php

namespace App\Repositories;

use App\Models\oc_product_attribute;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_attributeRepository
 * @package App\Repositories
 * @version August 28, 2017, 9:08 pm UTC
 *
 * @method oc_product_attribute findWithoutFail($id, $columns = ['*'])
 * @method oc_product_attribute find($id, $columns = ['*'])
 * @method oc_product_attribute first($columns = ['*'])
*/
class oc_product_attributeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'attribute_id',
        'language_id',
        'text'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_attribute::class;
    }
}
