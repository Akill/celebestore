<?php

namespace App\Repositories;

use App\Models\oc_currency;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_currencyRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_currency findWithoutFail($id, $columns = ['*'])
 * @method oc_currency find($id, $columns = ['*'])
 * @method oc_currency first($columns = ['*'])
*/
class oc_currencyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'code',
        'symbol_left',
        'symbol_right',
        'decimal_place',
        'value',
        'status',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_currency::class;
    }
}
