<?php

namespace App\Repositories;

use App\Models\oc_customer_group_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_group_descriptionRepository
 * @package App\Repositories
 * @version August 29, 2017, 1:15 am UTC
 *
 * @method oc_customer_group_description findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_group_description find($id, $columns = ['*'])
 * @method oc_customer_group_description first($columns = ['*'])
*/
class oc_customer_group_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_group_id',
        'language_id',
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_group_description::class;
    }
}
