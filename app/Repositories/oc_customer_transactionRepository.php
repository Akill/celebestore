<?php

namespace App\Repositories;

use App\Models\oc_customer_transaction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_transactionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_transaction findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_transaction find($id, $columns = ['*'])
 * @method oc_customer_transaction first($columns = ['*'])
*/
class oc_customer_transactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'order_id',
        'description',
        'amount',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_transaction::class;
    }
}
