<?php

namespace App\Repositories;

use App\Models\oc_coupon_history;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_coupon_historyRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_coupon_history findWithoutFail($id, $columns = ['*'])
 * @method oc_coupon_history find($id, $columns = ['*'])
 * @method oc_coupon_history first($columns = ['*'])
*/
class oc_coupon_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'coupon_id',
        'order_id',
        'customer_id',
        'amount',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_coupon_history::class;
    }
}
