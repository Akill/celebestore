<?php

namespace App\Repositories;

use App\Models\oc_affiliate_transaction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_affiliate_transactionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_affiliate_transaction findWithoutFail($id, $columns = ['*'])
 * @method oc_affiliate_transaction find($id, $columns = ['*'])
 * @method oc_affiliate_transaction first($columns = ['*'])
*/
class oc_affiliate_transactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'affiliate_id',
        'order_id',
        'description',
        'amount',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_affiliate_transaction::class;
    }
}
