<?php

namespace App\Repositories;

use App\Models\oc_user;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_userRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_user findWithoutFail($id, $columns = ['*'])
 * @method oc_user find($id, $columns = ['*'])
 * @method oc_user first($columns = ['*'])
*/
class oc_userRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_group_id',
        'username',
        'password',
        'salt',
        'firstname',
        'lastname',
        'email',
        'image',
        'code',
        'ip',
        'status',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_user::class;
    }
}
