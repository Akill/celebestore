<?php

namespace App\Repositories;

use App\Models\oc_order_option;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_optionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_option findWithoutFail($id, $columns = ['*'])
 * @method oc_order_option find($id, $columns = ['*'])
 * @method oc_order_option first($columns = ['*'])
*/
class oc_order_optionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'order_product_id',
        'product_option_id',
        'product_option_value_id',
        'name',
        'value',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_option::class;
    }
}
