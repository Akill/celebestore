<?php

namespace App\Repositories;

use App\Models\oc_return_reason;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_return_reasonRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_return_reason findWithoutFail($id, $columns = ['*'])
 * @method oc_return_reason find($id, $columns = ['*'])
 * @method oc_return_reason first($columns = ['*'])
*/
class oc_return_reasonRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_return_reason::class;
    }
}
