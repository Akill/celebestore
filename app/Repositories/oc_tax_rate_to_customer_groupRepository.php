<?php

namespace App\Repositories;

use App\Models\oc_tax_rate_to_customer_group;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_tax_rate_to_customer_groupRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_tax_rate_to_customer_group findWithoutFail($id, $columns = ['*'])
 * @method oc_tax_rate_to_customer_group find($id, $columns = ['*'])
 * @method oc_tax_rate_to_customer_group first($columns = ['*'])
*/
class oc_tax_rate_to_customer_groupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_group_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_tax_rate_to_customer_group::class;
    }
}
