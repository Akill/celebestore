<?php

namespace App\Repositories;

use App\Models\oc_affiliate_login;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_affiliate_loginRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_affiliate_login findWithoutFail($id, $columns = ['*'])
 * @method oc_affiliate_login find($id, $columns = ['*'])
 * @method oc_affiliate_login first($columns = ['*'])
*/
class oc_affiliate_loginRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'ip',
        'total',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_affiliate_login::class;
    }
}
