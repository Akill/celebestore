<?php

namespace App\Repositories;

use App\Models\oc_option_value_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_option_value_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_option_value_description findWithoutFail($id, $columns = ['*'])
 * @method oc_option_value_description find($id, $columns = ['*'])
 * @method oc_option_value_description first($columns = ['*'])
*/
class oc_option_value_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'option_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_option_value_description::class;
    }
}
