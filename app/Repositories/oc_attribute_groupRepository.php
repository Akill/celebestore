<?php

namespace App\Repositories;

use App\Models\oc_attribute_group;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_attribute_groupRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_attribute_group findWithoutFail($id, $columns = ['*'])
 * @method oc_attribute_group find($id, $columns = ['*'])
 * @method oc_attribute_group first($columns = ['*'])
*/
class oc_attribute_groupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_attribute_group::class;
    }
}
