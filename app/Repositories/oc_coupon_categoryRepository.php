<?php

namespace App\Repositories;

use App\Models\oc_coupon_category;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_coupon_categoryRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_coupon_category findWithoutFail($id, $columns = ['*'])
 * @method oc_coupon_category find($id, $columns = ['*'])
 * @method oc_coupon_category first($columns = ['*'])
*/
class oc_coupon_categoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_coupon_category::class;
    }
}
