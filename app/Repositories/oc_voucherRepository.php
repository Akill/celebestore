<?php

namespace App\Repositories;

use App\Models\oc_voucher;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_voucherRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_voucher findWithoutFail($id, $columns = ['*'])
 * @method oc_voucher find($id, $columns = ['*'])
 * @method oc_voucher first($columns = ['*'])
*/
class oc_voucherRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'code',
        'from_name',
        'from_email',
        'to_name',
        'to_email',
        'voucher_theme_id',
        'message',
        'amount',
        'status',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_voucher::class;
    }
}
