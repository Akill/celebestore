<?php

namespace App\Repositories;

use App\Models\oc_customer_activity;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_activityRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_activity findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_activity find($id, $columns = ['*'])
 * @method oc_customer_activity first($columns = ['*'])
*/
class oc_customer_activityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'key',
        'data',
        'ip',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_activity::class;
    }
}
