<?php

namespace App\Repositories;

use App\Models\oc_marketing;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_marketingRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_marketing findWithoutFail($id, $columns = ['*'])
 * @method oc_marketing find($id, $columns = ['*'])
 * @method oc_marketing first($columns = ['*'])
*/
class oc_marketingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'code',
        'clicks',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_marketing::class;
    }
}
