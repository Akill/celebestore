<?php

namespace App\Repositories;

use App\Models\oc_customer_reward;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_rewardRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_reward findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_reward find($id, $columns = ['*'])
 * @method oc_customer_reward first($columns = ['*'])
*/
class oc_customer_rewardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'order_id',
        'description',
        'points',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_reward::class;
    }
}
