<?php

namespace App\Repositories;

use App\Models\user;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class userRepository
 * @package App\Repositories
 * @version August 28, 2017, 10:46 am UTC
 *
 * @method user findWithoutFail($id, $columns = ['*'])
 * @method user find($id, $columns = ['*'])
 * @method user first($columns = ['*'])
*/
class userRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_group_id',
        'name',
        'email',
        'password',
        'username',
        'type_of_identity',
        'identity_number',
        'gender',
        'phone',
        'job',
        'address',
        'districts',
        'city',
        'province',
        'zip_kode',
        'picture',
        'remember_token'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return user::class;
    }
}
