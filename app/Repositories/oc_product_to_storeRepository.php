<?php

namespace App\Repositories;

use App\Models\oc_product_to_store;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_to_storeRepository
 * @package App\Repositories
 * @version September 2, 2017, 4:52 pm UTC
 *
 * @method oc_product_to_store findWithoutFail($id, $columns = ['*'])
 * @method oc_product_to_store find($id, $columns = ['*'])
 * @method oc_product_to_store first($columns = ['*'])
*/
class oc_product_to_storeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_to_store::class;
    }
}
