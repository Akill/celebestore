<?php

namespace App\Repositories;

use App\Models\oc_return_action;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_return_actionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_return_action findWithoutFail($id, $columns = ['*'])
 * @method oc_return_action find($id, $columns = ['*'])
 * @method oc_return_action first($columns = ['*'])
*/
class oc_return_actionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_return_action::class;
    }
}
