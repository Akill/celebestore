<?php

namespace App\Repositories;

use App\Models\oc_layout_module;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_layout_moduleRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_layout_module findWithoutFail($id, $columns = ['*'])
 * @method oc_layout_module find($id, $columns = ['*'])
 * @method oc_layout_module first($columns = ['*'])
*/
class oc_layout_moduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'layout_id',
        'code',
        'position',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_layout_module::class;
    }
}
