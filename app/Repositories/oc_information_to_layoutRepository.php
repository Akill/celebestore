<?php

namespace App\Repositories;

use App\Models\oc_information_to_layout;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_information_to_layoutRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_information_to_layout findWithoutFail($id, $columns = ['*'])
 * @method oc_information_to_layout find($id, $columns = ['*'])
 * @method oc_information_to_layout first($columns = ['*'])
*/
class oc_information_to_layoutRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'layout_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_information_to_layout::class;
    }
}
