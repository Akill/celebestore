<?php

namespace App\Repositories;

use App\Models\oc_upload;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_uploadRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_upload findWithoutFail($id, $columns = ['*'])
 * @method oc_upload find($id, $columns = ['*'])
 * @method oc_upload first($columns = ['*'])
*/
class oc_uploadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'filename',
        'code',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_upload::class;
    }
}
