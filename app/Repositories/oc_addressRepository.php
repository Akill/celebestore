<?php

namespace App\Repositories;

use App\Models\oc_address;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_addressRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:34 am UTC
 *
 * @method oc_address findWithoutFail($id, $columns = ['*'])
 * @method oc_address find($id, $columns = ['*'])
 * @method oc_address first($columns = ['*'])
*/
class oc_addressRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sub_district_id',
        'customer_id',
        'firstname',
        'lastname',
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'custom_field'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_address::class;
    }
}
