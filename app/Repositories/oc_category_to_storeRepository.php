<?php

namespace App\Repositories;

use App\Models\oc_category_to_store;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_category_to_storeRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_category_to_store findWithoutFail($id, $columns = ['*'])
 * @method oc_category_to_store find($id, $columns = ['*'])
 * @method oc_category_to_store first($columns = ['*'])
*/
class oc_category_to_storeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category_to_store::class;
    }
}
