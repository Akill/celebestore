<?php

namespace App\Repositories;

use App\Models\oc_custom_field;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_custom_fieldRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_custom_field findWithoutFail($id, $columns = ['*'])
 * @method oc_custom_field find($id, $columns = ['*'])
 * @method oc_custom_field first($columns = ['*'])
*/
class oc_custom_fieldRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'value',
        'location',
        'status',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_custom_field::class;
    }
}
