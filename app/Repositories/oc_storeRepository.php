<?php

namespace App\Repositories;

use App\Models\oc_store;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_storeRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_store findWithoutFail($id, $columns = ['*'])
 * @method oc_store find($id, $columns = ['*'])
 * @method oc_store first($columns = ['*'])
*/
class oc_storeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'url',
        'ssl'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_store::class;
    }
}
