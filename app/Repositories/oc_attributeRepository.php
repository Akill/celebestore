<?php

namespace App\Repositories;

use App\Models\oc_attribute;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_attributeRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_attribute findWithoutFail($id, $columns = ['*'])
 * @method oc_attribute find($id, $columns = ['*'])
 * @method oc_attribute first($columns = ['*'])
*/
class oc_attributeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'attribute_group_id',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_attribute::class;
    }
}
