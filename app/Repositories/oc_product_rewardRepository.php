<?php

namespace App\Repositories;

use App\Models\oc_product_reward;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_rewardRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_reward findWithoutFail($id, $columns = ['*'])
 * @method oc_product_reward find($id, $columns = ['*'])
 * @method oc_product_reward first($columns = ['*'])
*/
class oc_product_rewardRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'customer_group_id',
        'points'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_reward::class;
    }
}
