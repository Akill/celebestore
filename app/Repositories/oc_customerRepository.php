<?php

namespace App\Repositories;

use App\Models\oc_customer;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customerRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer findWithoutFail($id, $columns = ['*'])
 * @method oc_customer find($id, $columns = ['*'])
 * @method oc_customer first($columns = ['*'])
*/
class oc_customerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_group_id',
        'store_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'password',
        'salt',
        'cart',
        'wishlist',
        'newsletter',
        'address_id',
        'custom_field',
        'ip',
        'status',
        'approved',
        'safe',
        'token',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer::class;
    }
}
