<?php

namespace App\Repositories;

use App\Models\oc_voucher_theme_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_voucher_theme_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:03 am UTC
 *
 * @method oc_voucher_theme_description findWithoutFail($id, $columns = ['*'])
 * @method oc_voucher_theme_description find($id, $columns = ['*'])
 * @method oc_voucher_theme_description first($columns = ['*'])
*/
class oc_voucher_theme_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_voucher_theme_description::class;
    }
}
