<?php

namespace App\Repositories;

use App\Models\oc_coupon;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_couponRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_coupon findWithoutFail($id, $columns = ['*'])
 * @method oc_coupon find($id, $columns = ['*'])
 * @method oc_coupon first($columns = ['*'])
*/
class oc_couponRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'type',
        'discount',
        'logged',
        'shipping',
        'total',
        'date_start',
        'date_end',
        'uses_total',
        'uses_customer',
        'status',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_coupon::class;
    }
}
