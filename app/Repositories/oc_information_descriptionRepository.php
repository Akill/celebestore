<?php

namespace App\Repositories;

use App\Models\oc_information_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_information_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_information_description findWithoutFail($id, $columns = ['*'])
 * @method oc_information_description find($id, $columns = ['*'])
 * @method oc_information_description first($columns = ['*'])
*/
class oc_information_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'title',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_information_description::class;
    }
}
