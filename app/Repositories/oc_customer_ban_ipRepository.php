<?php

namespace App\Repositories;

use App\Models\oc_customer_ban_ip;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_ban_ipRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_ban_ip findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_ban_ip find($id, $columns = ['*'])
 * @method oc_customer_ban_ip first($columns = ['*'])
*/
class oc_customer_ban_ipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ip'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_ban_ip::class;
    }
}
