<?php

namespace App\Repositories;

use App\Models\oc_order_recurring_transaction;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_recurring_transactionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_recurring_transaction findWithoutFail($id, $columns = ['*'])
 * @method oc_order_recurring_transaction find($id, $columns = ['*'])
 * @method oc_order_recurring_transaction first($columns = ['*'])
*/
class oc_order_recurring_transactionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_recurring_id',
        'reference',
        'type',
        'amount',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_recurring_transaction::class;
    }
}
