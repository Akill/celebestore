<?php

namespace App\Repositories;

use App\Models\oc_product_shipping_filtered;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_shipping_filteredRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_shipping_filtered findWithoutFail($id, $columns = ['*'])
 * @method oc_product_shipping_filtered find($id, $columns = ['*'])
 * @method oc_product_shipping_filtered first($columns = ['*'])
*/
class oc_product_shipping_filteredRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'shipping_code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_shipping_filtered::class;
    }
}
