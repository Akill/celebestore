<?php

namespace App\Repositories;

use App\Models\oc_custom_field_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_custom_field_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_custom_field_description findWithoutFail($id, $columns = ['*'])
 * @method oc_custom_field_description find($id, $columns = ['*'])
 * @method oc_custom_field_description first($columns = ['*'])
*/
class oc_custom_field_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_custom_field_description::class;
    }
}
