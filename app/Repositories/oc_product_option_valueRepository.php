<?php

namespace App\Repositories;

use App\Models\oc_product_option_value;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_option_valueRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_option_value findWithoutFail($id, $columns = ['*'])
 * @method oc_product_option_value find($id, $columns = ['*'])
 * @method oc_product_option_value first($columns = ['*'])
*/
class oc_product_option_valueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_option_id',
        'product_id',
        'option_id',
        'option_value_id',
        'quantity',
        'subtract',
        'price',
        'price_prefix',
        'points',
        'points_prefix',
        'weight',
        'weight_prefix'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_option_value::class;
    }
}
