<?php

namespace App\Repositories;

use App\Models\oc_user_group;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_user_groupRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_user_group findWithoutFail($id, $columns = ['*'])
 * @method oc_user_group find($id, $columns = ['*'])
 * @method oc_user_group first($columns = ['*'])
*/
class oc_user_groupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'permission'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_user_group::class;
    }
}
