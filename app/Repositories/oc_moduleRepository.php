<?php

namespace App\Repositories;

use App\Models\oc_module;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_moduleRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_module findWithoutFail($id, $columns = ['*'])
 * @method oc_module find($id, $columns = ['*'])
 * @method oc_module first($columns = ['*'])
*/
class oc_moduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'setting'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_module::class;
    }
}
