<?php

namespace App\Repositories;

use App\Models\oc_voucher_theme;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_voucher_themeRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_voucher_theme findWithoutFail($id, $columns = ['*'])
 * @method oc_voucher_theme find($id, $columns = ['*'])
 * @method oc_voucher_theme first($columns = ['*'])
*/
class oc_voucher_themeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_voucher_theme::class;
    }
}
