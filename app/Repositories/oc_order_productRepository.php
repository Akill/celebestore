<?php

namespace App\Repositories;

use App\Models\oc_order_product;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_productRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_product findWithoutFail($id, $columns = ['*'])
 * @method oc_order_product find($id, $columns = ['*'])
 * @method oc_order_product first($columns = ['*'])
*/
class oc_order_productRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'product_id',
        'name',
        'model',
        'quantity',
        'price',
        'total',
        'tax',
        'reward'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_product::class;
    }
}
