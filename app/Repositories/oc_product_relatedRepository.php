<?php

namespace App\Repositories;

use App\Models\oc_product_related;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_relatedRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_related findWithoutFail($id, $columns = ['*'])
 * @method oc_product_related find($id, $columns = ['*'])
 * @method oc_product_related first($columns = ['*'])
*/
class oc_product_relatedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'related_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_related::class;
    }
}
