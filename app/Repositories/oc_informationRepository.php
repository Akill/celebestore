<?php

namespace App\Repositories;

use App\Models\oc_information;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_informationRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_information findWithoutFail($id, $columns = ['*'])
 * @method oc_information find($id, $columns = ['*'])
 * @method oc_information first($columns = ['*'])
*/
class oc_informationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bottom',
        'sort_order',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_information::class;
    }
}
