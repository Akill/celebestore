<?php

namespace App\Repositories;

use App\Models\oc_geo_zone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_geo_zoneRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_geo_zone findWithoutFail($id, $columns = ['*'])
 * @method oc_geo_zone find($id, $columns = ['*'])
 * @method oc_geo_zone first($columns = ['*'])
*/
class oc_geo_zoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'date_modified',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_geo_zone::class;
    }
}
