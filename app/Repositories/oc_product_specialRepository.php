<?php

namespace App\Repositories;

use App\Models\oc_product_special;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_specialRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_special findWithoutFail($id, $columns = ['*'])
 * @method oc_product_special find($id, $columns = ['*'])
 * @method oc_product_special first($columns = ['*'])
*/
class oc_product_specialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'customer_group_id',
        'priority',
        'price',
        'date_start',
        'date_end'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_special::class;
    }
}
