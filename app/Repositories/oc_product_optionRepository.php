<?php

namespace App\Repositories;

use App\Models\oc_product_option;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_optionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_option findWithoutFail($id, $columns = ['*'])
 * @method oc_product_option find($id, $columns = ['*'])
 * @method oc_product_option first($columns = ['*'])
*/
class oc_product_optionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'option_id',
        'value',
        'required'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_option::class;
    }
}
