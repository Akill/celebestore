<?php

namespace App\Repositories;

use App\Models\oc_attribute_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_attribute_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_attribute_description findWithoutFail($id, $columns = ['*'])
 * @method oc_attribute_description find($id, $columns = ['*'])
 * @method oc_attribute_description first($columns = ['*'])
*/
class oc_attribute_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_attribute_description::class;
    }
}
