<?php

namespace App\Repositories;

use App\Models\oc_custom_field_customer_group;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_custom_field_customer_groupRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_custom_field_customer_group findWithoutFail($id, $columns = ['*'])
 * @method oc_custom_field_customer_group find($id, $columns = ['*'])
 * @method oc_custom_field_customer_group first($columns = ['*'])
*/
class oc_custom_field_customer_groupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_group_id',
        'required'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_custom_field_customer_group::class;
    }
}
