<?php

namespace App\Repositories;

use App\Models\oc_filter;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_filterRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_filter findWithoutFail($id, $columns = ['*'])
 * @method oc_filter find($id, $columns = ['*'])
 * @method oc_filter first($columns = ['*'])
*/
class oc_filterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_group_id',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_filter::class;
    }
}
