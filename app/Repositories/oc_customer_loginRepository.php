<?php

namespace App\Repositories;

use App\Models\oc_customer_login;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_loginRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_login findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_login find($id, $columns = ['*'])
 * @method oc_customer_login first($columns = ['*'])
*/
class oc_customer_loginRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'ip',
        'total',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_login::class;
    }
}
