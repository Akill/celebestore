<?php

namespace App\Repositories;

use App\Models\oc_option;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_optionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_option findWithoutFail($id, $columns = ['*'])
 * @method oc_option find($id, $columns = ['*'])
 * @method oc_option first($columns = ['*'])
*/
class oc_optionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_option::class;
    }
}
