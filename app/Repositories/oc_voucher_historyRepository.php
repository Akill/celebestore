<?php

namespace App\Repositories;

use App\Models\oc_voucher_history;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_voucher_historyRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_voucher_history findWithoutFail($id, $columns = ['*'])
 * @method oc_voucher_history find($id, $columns = ['*'])
 * @method oc_voucher_history first($columns = ['*'])
*/
class oc_voucher_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'voucher_id',
        'order_id',
        'amount',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_voucher_history::class;
    }
}
