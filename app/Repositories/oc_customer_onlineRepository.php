<?php

namespace App\Repositories;

use App\Models\oc_customer_online;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_customer_onlineRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_customer_online findWithoutFail($id, $columns = ['*'])
 * @method oc_customer_online find($id, $columns = ['*'])
 * @method oc_customer_online first($columns = ['*'])
*/
class oc_customer_onlineRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'customer_id',
        'url',
        'referer',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_customer_online::class;
    }
}
