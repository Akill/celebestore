<?php

namespace App\Repositories;

use App\Models\oc_download;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_downloadRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_download findWithoutFail($id, $columns = ['*'])
 * @method oc_download find($id, $columns = ['*'])
 * @method oc_download first($columns = ['*'])
*/
class oc_downloadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filename',
        'mask',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_download::class;
    }
}
