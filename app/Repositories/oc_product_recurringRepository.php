<?php

namespace App\Repositories;

use App\Models\oc_product_recurring;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_recurringRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_product_recurring findWithoutFail($id, $columns = ['*'])
 * @method oc_product_recurring find($id, $columns = ['*'])
 * @method oc_product_recurring first($columns = ['*'])
*/
class oc_product_recurringRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'recurring_id',
        'customer_group_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_recurring::class;
    }
}
