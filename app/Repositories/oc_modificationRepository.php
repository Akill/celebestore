<?php

namespace App\Repositories;

use App\Models\oc_modification;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_modificationRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_modification findWithoutFail($id, $columns = ['*'])
 * @method oc_modification find($id, $columns = ['*'])
 * @method oc_modification first($columns = ['*'])
*/
class oc_modificationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'author',
        'version',
        'link',
        'xml',
        'status',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_modification::class;
    }
}
