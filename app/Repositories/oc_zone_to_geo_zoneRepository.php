<?php

namespace App\Repositories;

use App\Models\oc_zone_to_geo_zone;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_zone_to_geo_zoneRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:03 am UTC
 *
 * @method oc_zone_to_geo_zone findWithoutFail($id, $columns = ['*'])
 * @method oc_zone_to_geo_zone find($id, $columns = ['*'])
 * @method oc_zone_to_geo_zone first($columns = ['*'])
*/
class oc_zone_to_geo_zoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'zone_id',
        'geo_zone_id',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_zone_to_geo_zone::class;
    }
}
