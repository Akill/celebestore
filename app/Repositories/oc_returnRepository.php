<?php

namespace App\Repositories;

use App\Models\oc_return;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_returnRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_return findWithoutFail($id, $columns = ['*'])
 * @method oc_return find($id, $columns = ['*'])
 * @method oc_return first($columns = ['*'])
*/
class oc_returnRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'product_id',
        'customer_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'product',
        'model',
        'quantity',
        'opened',
        'return_reason_id',
        'return_action_id',
        'return_status_id',
        'comment',
        'date_ordered',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_return::class;
    }
}
