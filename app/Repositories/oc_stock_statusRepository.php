<?php

namespace App\Repositories;

use App\Models\oc_stock_status;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_stock_statusRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_stock_status findWithoutFail($id, $columns = ['*'])
 * @method oc_stock_status find($id, $columns = ['*'])
 * @method oc_stock_status first($columns = ['*'])
*/
class oc_stock_statusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_stock_status::class;
    }
}
