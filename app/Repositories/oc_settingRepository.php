<?php

namespace App\Repositories;

use App\Models\oc_setting;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_settingRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_setting findWithoutFail($id, $columns = ['*'])
 * @method oc_setting find($id, $columns = ['*'])
 * @method oc_setting first($columns = ['*'])
*/
class oc_settingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id',
        'code',
        'key',
        'value',
        'serialized'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_setting::class;
    }
}
