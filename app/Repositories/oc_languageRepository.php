<?php

namespace App\Repositories;

use App\Models\oc_language;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_languageRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_language findWithoutFail($id, $columns = ['*'])
 * @method oc_language find($id, $columns = ['*'])
 * @method oc_language first($columns = ['*'])
*/
class oc_languageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'code',
        'locale',
        'image',
        'directory',
        'sort_order',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_language::class;
    }
}
