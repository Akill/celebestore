<?php

namespace App\Repositories;

use App\Models\oc_recurring_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_recurring_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_recurring_description findWithoutFail($id, $columns = ['*'])
 * @method oc_recurring_description find($id, $columns = ['*'])
 * @method oc_recurring_description first($columns = ['*'])
*/
class oc_recurring_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_recurring_description::class;
    }
}
