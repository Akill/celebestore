<?php

namespace App\Repositories;

use App\Models\oc_extension;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_extensionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_extension findWithoutFail($id, $columns = ['*'])
 * @method oc_extension find($id, $columns = ['*'])
 * @method oc_extension first($columns = ['*'])
*/
class oc_extensionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'code'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_extension::class;
    }
}
