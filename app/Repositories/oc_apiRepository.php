<?php

namespace App\Repositories;

use App\Models\oc_api;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_apiRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_api findWithoutFail($id, $columns = ['*'])
 * @method oc_api find($id, $columns = ['*'])
 * @method oc_api first($columns = ['*'])
*/
class oc_apiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'firstname',
        'lastname',
        'password',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_api::class;
    }
}
