<?php

namespace App\Repositories;

use App\Models\oc_category_path;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_category_pathRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_category_path findWithoutFail($id, $columns = ['*'])
 * @method oc_category_path find($id, $columns = ['*'])
 * @method oc_category_path first($columns = ['*'])
*/
class oc_category_pathRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'path_id',
        'level'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category_path::class;
    }
}
