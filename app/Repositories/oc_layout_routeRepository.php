<?php

namespace App\Repositories;

use App\Models\oc_layout_route;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_layout_routeRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_layout_route findWithoutFail($id, $columns = ['*'])
 * @method oc_layout_route find($id, $columns = ['*'])
 * @method oc_layout_route first($columns = ['*'])
*/
class oc_layout_routeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'layout_id',
        'store_id',
        'route'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_layout_route::class;
    }
}
