<?php

namespace App\Repositories;

use App\Models\oc_filter_group;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_filter_groupRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_filter_group findWithoutFail($id, $columns = ['*'])
 * @method oc_filter_group find($id, $columns = ['*'])
 * @method oc_filter_group first($columns = ['*'])
*/
class oc_filter_groupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_filter_group::class;
    }
}
