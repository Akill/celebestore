<?php

namespace App\Repositories;

use App\Models\oc_location;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_locationRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_location findWithoutFail($id, $columns = ['*'])
 * @method oc_location find($id, $columns = ['*'])
 * @method oc_location first($columns = ['*'])
*/
class oc_locationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'telephone',
        'fax',
        'geocode',
        'image',
        'open',
        'comment'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_location::class;
    }
}
