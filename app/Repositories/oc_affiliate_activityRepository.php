<?php

namespace App\Repositories;

use App\Models\oc_affiliate_activity;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_affiliate_activityRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_affiliate_activity findWithoutFail($id, $columns = ['*'])
 * @method oc_affiliate_activity find($id, $columns = ['*'])
 * @method oc_affiliate_activity first($columns = ['*'])
*/
class oc_affiliate_activityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'affiliate_id',
        'key',
        'data',
        'ip',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_affiliate_activity::class;
    }
}
