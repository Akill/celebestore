<?php

namespace App\Repositories;

use App\Models\oc_tax_rate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_tax_rateRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_tax_rate findWithoutFail($id, $columns = ['*'])
 * @method oc_tax_rate find($id, $columns = ['*'])
 * @method oc_tax_rate first($columns = ['*'])
*/
class oc_tax_rateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'geo_zone_id',
        'name',
        'rate',
        'type',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_tax_rate::class;
    }
}
