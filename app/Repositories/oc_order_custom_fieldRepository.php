<?php

namespace App\Repositories;

use App\Models\oc_order_custom_field;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_custom_fieldRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_custom_field findWithoutFail($id, $columns = ['*'])
 * @method oc_order_custom_field find($id, $columns = ['*'])
 * @method oc_order_custom_field first($columns = ['*'])
*/
class oc_order_custom_fieldRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'custom_field_id',
        'custom_field_value_id',
        'name',
        'value',
        'type',
        'location'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_custom_field::class;
    }
}
