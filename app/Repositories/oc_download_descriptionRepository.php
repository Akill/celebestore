<?php

namespace App\Repositories;

use App\Models\oc_download_description;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_download_descriptionRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_download_description findWithoutFail($id, $columns = ['*'])
 * @method oc_download_description find($id, $columns = ['*'])
 * @method oc_download_description first($columns = ['*'])
*/
class oc_download_descriptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_download_description::class;
    }
}
