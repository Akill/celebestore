<?php

namespace App\Repositories;

use App\Models\oc_review;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_reviewRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_review findWithoutFail($id, $columns = ['*'])
 * @method oc_review find($id, $columns = ['*'])
 * @method oc_review first($columns = ['*'])
*/
class oc_reviewRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'customer_id',
        'author',
        'text',
        'rating',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_review::class;
    }
}
