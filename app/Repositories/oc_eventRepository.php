<?php

namespace App\Repositories;

use App\Models\oc_event;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_eventRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:38 am UTC
 *
 * @method oc_event findWithoutFail($id, $columns = ['*'])
 * @method oc_event find($id, $columns = ['*'])
 * @method oc_event first($columns = ['*'])
*/
class oc_eventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'code',
        'trigger',
        'action'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_event::class;
    }
}
