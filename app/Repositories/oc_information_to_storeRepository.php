<?php

namespace App\Repositories;

use App\Models\oc_information_to_store;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_information_to_storeRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:00 am UTC
 *
 * @method oc_information_to_store findWithoutFail($id, $columns = ['*'])
 * @method oc_information_to_store find($id, $columns = ['*'])
 * @method oc_information_to_store first($columns = ['*'])
*/
class oc_information_to_storeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'store_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_information_to_store::class;
    }
}
