<?php

namespace App\Repositories;

use App\Models\oc_category_filter;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_category_filterRepository
 * @package App\Repositories
 * @version August 26, 2017, 7:37 am UTC
 *
 * @method oc_category_filter findWithoutFail($id, $columns = ['*'])
 * @method oc_category_filter find($id, $columns = ['*'])
 * @method oc_category_filter first($columns = ['*'])
*/
class oc_category_filterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'filter_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_category_filter::class;
    }
}
