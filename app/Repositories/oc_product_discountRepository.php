<?php

namespace App\Repositories;

use App\Models\oc_product_discount;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_discountRepository
 * @package App\Repositories
 * @version August 28, 2017, 11:57 pm UTC
 *
 * @method oc_product_discount findWithoutFail($id, $columns = ['*'])
 * @method oc_product_discount find($id, $columns = ['*'])
 * @method oc_product_discount first($columns = ['*'])
*/
class oc_product_discountRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'product_id',
        'customer_group_id',
        'quantity',
        'priority',
        'price',
        'date_start',
        'date_end'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_discount::class;
    }
}
