<?php

namespace App\Repositories;

use App\Models\oc_product_to_download;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_product_to_downloadRepository
 * @package App\Repositories
 * @version September 2, 2017, 4:51 pm UTC
 *
 * @method oc_product_to_download findWithoutFail($id, $columns = ['*'])
 * @method oc_product_to_download find($id, $columns = ['*'])
 * @method oc_product_to_download first($columns = ['*'])
*/
class oc_product_to_downloadRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'download_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_product_to_download::class;
    }
}
