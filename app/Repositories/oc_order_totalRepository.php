<?php

namespace App\Repositories;

use App\Models\oc_order_total;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_totalRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_total findWithoutFail($id, $columns = ['*'])
 * @method oc_order_total find($id, $columns = ['*'])
 * @method oc_order_total first($columns = ['*'])
*/
class oc_order_totalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'code',
        'title',
        'value',
        'sort_order'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_total::class;
    }
}
