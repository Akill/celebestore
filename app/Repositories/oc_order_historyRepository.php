<?php

namespace App\Repositories;

use App\Models\oc_order_history;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_order_historyRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:01 am UTC
 *
 * @method oc_order_history findWithoutFail($id, $columns = ['*'])
 * @method oc_order_history find($id, $columns = ['*'])
 * @method oc_order_history first($columns = ['*'])
*/
class oc_order_historyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order_id',
        'order_status_id',
        'notify',
        'comment',
        'date_added'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_order_history::class;
    }
}
