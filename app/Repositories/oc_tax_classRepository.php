<?php

namespace App\Repositories;

use App\Models\oc_tax_class;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class oc_tax_classRepository
 * @package App\Repositories
 * @version August 26, 2017, 8:02 am UTC
 *
 * @method oc_tax_class findWithoutFail($id, $columns = ['*'])
 * @method oc_tax_class find($id, $columns = ['*'])
 * @method oc_tax_class first($columns = ['*'])
*/
class oc_tax_classRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'date_added',
        'date_modified'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return oc_tax_class::class;
    }
}
