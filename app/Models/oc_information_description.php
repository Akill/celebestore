<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_information_description
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer language_id
 * @property string title
 * @property string description
 * @property string meta_title
 * @property string meta_description
 * @property string meta_keyword
 */
class oc_information_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_information_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_id',
        'title',
        'description',
        'meta_title',
        'meta_description',
        'meta_keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'information_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'meta_title' => 'string',
        'meta_description' => 'string',
        'meta_keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
