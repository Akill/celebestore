<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_custom_field_value_description
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer language_id
 * @property integer custom_field_id
 * @property string name
 */
class oc_custom_field_value_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_custom_field_value_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_id',
        'custom_field_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'language_id' => 'integer',
        'custom_field_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
