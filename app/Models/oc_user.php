<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_user
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer user_group_id
 * @property string username
 * @property string password
 * @property string salt
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string image
 * @property string code
 * @property string ip
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 */
class oc_user extends Model
{
    use SoftDeletes;

    public $table = 'oc_user';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_group_id',
        'username',
        'password',
        'salt',
        'firstname',
        'lastname',
        'email',
        'image',
        'code',
        'ip',
        'status',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'user_group_id' => 'integer',
        'username' => 'string',
        'password' => 'string',
        'salt' => 'string',
        'firstname' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'image' => 'string',
        'code' => 'string',
        'ip' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
