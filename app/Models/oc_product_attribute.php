<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_attribute
 * @package App\Models
 * @version August 28, 2017, 9:08 pm UTC
 *
 * @property integer product_id
 * @property integer attribute_id
 * @property integer language_id
 * @property string text
 */
class oc_product_attribute extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_attribute';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'attribute_id',
        'language_id',
        'text'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'product_id' => 'integer',
        'attribute_id' => 'integer',
        'language_id' => 'integer',
        'text' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
