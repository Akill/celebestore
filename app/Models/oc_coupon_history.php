<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_coupon_history
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property integer coupon_id
 * @property integer order_id
 * @property integer customer_id
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 */
class oc_coupon_history extends Model
{
    use SoftDeletes;

    public $table = 'oc_coupon_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'coupon_id',
        'order_id',
        'customer_id',
        'amount',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'coupon_id' => 'integer',
        'order_id' => 'integer',
        'customer_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
