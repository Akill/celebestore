<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\oc_user_group;
/**
 * Class user
 * @package App\Models
 * @version August 28, 2017, 10:46 am UTC
 *
 * @property integer user_group_id
 * @property string name
 * @property string email
 * @property string password
 * @property string username
 * @property string type_of_identity
 * @property string identity_number
 * @property string gender
 * @property string phone
 * @property string job
 * @property string address
 * @property string districts
 * @property string city
 * @property string province
 * @property string zip_kode
 * @property string picture
 * @property string remember_token
 */
class user extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_group_id',
        'name',
        'email',
        'password',
        'username',
        'type_of_identity',
        'identity_number',
        'gender',
        'phone',
        'job',
        'address',
        'districts',
        'city',
        'province',
        'zip_kode',
        'picture',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_group_id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'password' => 'string',
        'username' => 'string',
        'type_of_identity' => 'string',
        'identity_number' => 'string',
        'gender' => 'string',
        'phone' => 'string',
        'job' => 'string',
        'address' => 'string',
        'districts' => 'string',
        'city' => 'string',
        'province' => 'string',
        'zip_kode' => 'string',
        'picture' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function userGroup(){
        return $this->hasMany(oc_user_group::class);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = is_null($password) ? : bcrypt($password);
    }
}
