<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_location
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string address
 * @property string telephone
 * @property string fax
 * @property string geocode
 * @property string image
 * @property string open
 * @property string comment
 */
class oc_location extends Model
{
    use SoftDeletes;

    public $table = 'oc_location';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'address',
        'telephone',
        'fax',
        'geocode',
        'image',
        'open',
        'comment'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'location_id' => 'integer',
        'name' => 'string',
        'address' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'geocode' => 'string',
        'image' => 'string',
        'open' => 'string',
        'comment' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
