<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_tax_rule
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer tax_class_id
 * @property integer tax_rate_id
 * @property string based
 * @property integer priority
 */
class oc_tax_rule extends Model
{
    use SoftDeletes;

    public $table = 'oc_tax_rule';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'tax_class_id',
        'tax_rate_id',
        'based',
        'priority'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tax_rule_id' => 'integer',
        'tax_class_id' => 'integer',
        'tax_rate_id' => 'integer',
        'based' => 'string',
        'priority' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
