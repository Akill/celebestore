<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_tax_class
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property string title
 * @property string description
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_tax_class extends Model
{
    use SoftDeletes;

    public $table = 'oc_tax_class';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'description',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tax_class_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
