<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_customer_group_description
 * @package App\Models
 * @version August 29, 2017, 1:15 am UTC
 *
 * @property integer customer_group_id
 * @property integer language_id
 * @property string name
 * @property string description
 */
class oc_customer_group_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_customer_group_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_group_id',
        'language_id',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_group_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
