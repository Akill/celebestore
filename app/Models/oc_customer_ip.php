<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_customer_ip
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer customer_id
 * @property string ip
 * @property string|\Carbon\Carbon date_added
 */
class oc_customer_ip extends Model
{
    use SoftDeletes;

    public $table = 'oc_customer_ip';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'ip',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'ip' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
