<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_coupon
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string name
 * @property string code
 * @property string type
 * @property decimal discount
 * @property boolean logged
 * @property boolean shipping
 * @property decimal total
 * @property date date_start
 * @property date date_end
 * @property integer uses_total
 * @property string uses_customer
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 */
class oc_coupon extends Model
{
    use SoftDeletes;

    public $table = 'oc_coupon';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'type',
        'discount',
        'logged',
        'shipping',
        'total',
        'date_start',
        'date_end',
        'uses_total',
        'uses_customer',
        'status',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'type' => 'string',
        'logged' => 'boolean',
        'shipping' => 'boolean',
        'date_start' => 'date',
        'date_end' => 'date',
        'uses_total' => 'integer',
        'uses_customer' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
