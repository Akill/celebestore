<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer invoice_no
 * @property string invoice_prefix
 * @property integer store_id
 * @property string store_name
 * @property string store_url
 * @property integer customer_id
 * @property integer customer_group_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string custom_field
 * @property string payment_firstname
 * @property string payment_lastname
 * @property string payment_company
 * @property string payment_address_1
 * @property string payment_address_2
 * @property string payment_city
 * @property string payment_postcode
 * @property string payment_country
 * @property integer payment_country_id
 * @property string payment_zone
 * @property integer payment_zone_id
 * @property integer payment_sub_district_id
 * @property string payment_address_format
 * @property string payment_custom_field
 * @property string payment_method
 * @property string payment_code
 * @property string shipping_firstname
 * @property string shipping_lastname
 * @property string shipping_company
 * @property string shipping_address_1
 * @property string shipping_address_2
 * @property string shipping_city
 * @property string shipping_postcode
 * @property string shipping_country
 * @property integer shipping_country_id
 * @property string shipping_zone
 * @property integer shipping_zone_id
 * @property integer shipping_sub_district_id
 * @property string shipping_address_format
 * @property string shipping_custom_field
 * @property string shipping_method
 * @property string shipping_code
 * @property string comment
 * @property decimal total
 * @property integer order_status_id
 * @property integer affiliate_id
 * @property decimal commission
 * @property integer marketing_id
 * @property string tracking
 * @property integer language_id
 * @property integer currency_id
 * @property string currency_code
 * @property decimal currency_value
 * @property string ip
 * @property string forwarded_ip
 * @property string user_agent
 * @property string accept_language
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_order extends Model
{
    use SoftDeletes;

    public $table = 'oc_order';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'invoice_no',
        'invoice_prefix',
        'store_id',
        'store_name',
        'store_url',
        'customer_id',
        'customer_group_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'custom_field',
        'payment_firstname',
        'payment_lastname',
        'payment_company',
        'payment_address_1',
        'payment_address_2',
        'payment_city',
        'payment_postcode',
        'payment_country',
        'payment_country_id',
        'payment_zone',
        'payment_zone_id',
        'payment_sub_district_id',
        'payment_address_format',
        'payment_custom_field',
        'payment_method',
        'payment_code',
        'shipping_firstname',
        'shipping_lastname',
        'shipping_company',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_city',
        'shipping_postcode',
        'shipping_country',
        'shipping_country_id',
        'shipping_zone',
        'shipping_zone_id',
        'shipping_sub_district_id',
        'shipping_address_format',
        'shipping_custom_field',
        'shipping_method',
        'shipping_code',
        'comment',
        'total',
        'order_status_id',
        'affiliate_id',
        'commission',
        'marketing_id',
        'tracking',
        'language_id',
        'currency_id',
        'currency_code',
        'currency_value',
        'ip',
        'forwarded_ip',
        'user_agent',
        'accept_language',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_id' => 'integer',
        'invoice_no' => 'integer',
        'invoice_prefix' => 'string',
        'store_id' => 'integer',
        'store_name' => 'string',
        'store_url' => 'string',
        'customer_id' => 'integer',
        'customer_group_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'custom_field' => 'string',
        'payment_firstname' => 'string',
        'payment_lastname' => 'string',
        'payment_company' => 'string',
        'payment_address_1' => 'string',
        'payment_address_2' => 'string',
        'payment_city' => 'string',
        'payment_postcode' => 'string',
        'payment_country' => 'string',
        'payment_country_id' => 'integer',
        'payment_zone' => 'string',
        'payment_zone_id' => 'integer',
        'payment_sub_district_id' => 'integer',
        'payment_address_format' => 'string',
        'payment_custom_field' => 'string',
        'payment_method' => 'string',
        'payment_code' => 'string',
        'shipping_firstname' => 'string',
        'shipping_lastname' => 'string',
        'shipping_company' => 'string',
        'shipping_address_1' => 'string',
        'shipping_address_2' => 'string',
        'shipping_city' => 'string',
        'shipping_postcode' => 'string',
        'shipping_country' => 'string',
        'shipping_country_id' => 'integer',
        'shipping_zone' => 'string',
        'shipping_zone_id' => 'integer',
        'shipping_sub_district_id' => 'integer',
        'shipping_address_format' => 'string',
        'shipping_custom_field' => 'string',
        'shipping_method' => 'string',
        'shipping_code' => 'string',
        'comment' => 'string',
        'order_status_id' => 'integer',
        'affiliate_id' => 'integer',
        'marketing_id' => 'integer',
        'tracking' => 'string',
        'language_id' => 'integer',
        'currency_id' => 'integer',
        'currency_code' => 'string',
        'ip' => 'string',
        'forwarded_ip' => 'string',
        'user_agent' => 'string',
        'accept_language' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
