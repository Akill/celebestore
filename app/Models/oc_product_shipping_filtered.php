<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_shipping_filtered
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_id
 * @property string shipping_code
 */
class oc_product_shipping_filtered extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_shipping_filtered';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'shipping_code'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'filtered_id' => 'integer',
        'product_id' => 'integer',
        'shipping_code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
