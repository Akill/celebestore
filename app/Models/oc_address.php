<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_address
 * @package App\Models
 * @version August 26, 2017, 7:34 am UTC
 *
 * @property integer sub_district_id
 * @property integer customer_id
 * @property string firstname
 * @property string lastname
 * @property string company
 * @property string address_1
 * @property string address_2
 * @property string city
 * @property string postcode
 * @property integer country_id
 * @property integer zone_id
 * @property string custom_field
 */
class oc_address extends Model
{
    use SoftDeletes;

    public $table = 'oc_address';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sub_district_id',
        'customer_id',
        'firstname',
        'lastname',
        'company',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'custom_field'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sub_district_id' => 'integer',
        'customer_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'company' => 'string',
        'address_1' => 'string',
        'address_2' => 'string',
        'city' => 'string',
        'postcode' => 'string',
        'country_id' => 'integer',
        'zone_id' => 'integer',
        'custom_field' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
