<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_download
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property string filename
 * @property string mask
 * @property string|\Carbon\Carbon date_added
 */
class oc_download extends Model
{
    use SoftDeletes;

    public $table = 'oc_download';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'filename',
        'mask',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'filename' => 'string',
        'mask' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
