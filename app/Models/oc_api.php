<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_api
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string username
 * @property string firstname
 * @property string lastname
 * @property string password
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_api extends Model
{
    use SoftDeletes;

    public $table = 'oc_api';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'firstname',
        'lastname',
        'password',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'firstname' => 'string',
        'lastname' => 'string',
        'password' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
