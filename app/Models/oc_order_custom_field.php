<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_custom_field
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property integer custom_field_id
 * @property integer custom_field_value_id
 * @property string name
 * @property string value
 * @property string type
 * @property string location
 */
class oc_order_custom_field extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_custom_field';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'custom_field_id',
        'custom_field_value_id',
        'name',
        'value',
        'type',
        'location'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_custom_field_id' => 'integer',
        'order_id' => 'integer',
        'custom_field_id' => 'integer',
        'custom_field_value_id' => 'integer',
        'name' => 'string',
        'value' => 'string',
        'type' => 'string',
        'location' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
