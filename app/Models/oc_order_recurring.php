<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_recurring
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property string reference
 * @property integer product_id
 * @property string product_name
 * @property integer product_quantity
 * @property integer recurring_id
 * @property string recurring_name
 * @property string recurring_description
 * @property string recurring_frequency
 * @property smallInteger recurring_cycle
 * @property smallInteger recurring_duration
 * @property decimal recurring_price
 * @property boolean trial
 * @property string trial_frequency
 * @property smallInteger trial_cycle
 * @property smallInteger trial_duration
 * @property decimal trial_price
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 */
class oc_order_recurring extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_recurring';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'reference',
        'product_id',
        'product_name',
        'product_quantity',
        'recurring_id',
        'recurring_name',
        'recurring_description',
        'recurring_frequency',
        'recurring_cycle',
        'recurring_duration',
        'recurring_price',
        'trial',
        'trial_frequency',
        'trial_cycle',
        'trial_duration',
        'trial_price',
        'status',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_recurring_id' => 'integer',
        'order_id' => 'integer',
        'reference' => 'string',
        'product_id' => 'integer',
        'product_name' => 'string',
        'product_quantity' => 'integer',
        'recurring_id' => 'integer',
        'recurring_name' => 'string',
        'recurring_description' => 'string',
        'recurring_frequency' => 'string',
        'trial' => 'boolean',
        'trial_frequency' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
