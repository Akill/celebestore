<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_layout_module
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer layout_id
 * @property string code
 * @property string position
 * @property integer sort_order
 */
class oc_layout_module extends Model
{
    use SoftDeletes;

    public $table = 'oc_layout_module';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'layout_id',
        'code',
        'position',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'layout_module_id' => 'integer',
        'layout_id' => 'integer',
        'code' => 'string',
        'position' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
