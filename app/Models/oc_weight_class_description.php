<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_weight_class_description
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 *
 * @property integer language_id
 * @property string title
 * @property string unit
 */
class oc_weight_class_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_weight_class_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_id',
        'title',
        'unit'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'weight_class_id' => 'integer',
        'language_id' => 'integer',
        'title' => 'string',
        'unit' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
