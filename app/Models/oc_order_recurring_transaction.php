<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_recurring_transaction
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_recurring_id
 * @property string reference
 * @property string type
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 */
class oc_order_recurring_transaction extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_recurring_transaction';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_recurring_id',
        'reference',
        'type',
        'amount',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_recurring_transaction_id' => 'integer',
        'order_recurring_id' => 'integer',
        'reference' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
