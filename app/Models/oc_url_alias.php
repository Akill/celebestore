<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_url_alias
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property string query
 * @property string keyword
 */
class oc_url_alias extends Model
{
    use SoftDeletes;

    public $table = 'oc_url_alias';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'query',
        'keyword'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'url_alias_id' => 'integer',
        'query' => 'string',
        'keyword' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
