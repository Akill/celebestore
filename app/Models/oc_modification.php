<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_modification
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string code
 * @property string author
 * @property string version
 * @property string link
 * @property string xml
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 */
class oc_modification extends Model
{
    use SoftDeletes;

    public $table = 'oc_modification';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'author',
        'version',
        'link',
        'xml',
        'status',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'modification_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'author' => 'string',
        'version' => 'string',
        'link' => 'string',
        'xml' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
