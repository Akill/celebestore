<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_history
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property integer order_status_id
 * @property boolean notify
 * @property string comment
 * @property string|\Carbon\Carbon date_added
 */
class oc_order_history extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'order_status_id',
        'notify',
        'comment',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_history_id' => 'integer',
        'order_id' => 'integer',
        'order_status_id' => 'integer',
        'notify' => 'boolean',
        'comment' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
