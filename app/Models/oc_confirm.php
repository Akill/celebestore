<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_confirm
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string email
 * @property string order_id
 * @property date payment_date
 * @property integer total_amount
 * @property string destination_bank
 * @property string resi
 * @property string payment_method
 * @property string sender_name
 * @property string bank_origin
 * @property string code
 * @property string no_receipt
 * @property string|\Carbon\Carbon date_added
 */
class oc_confirm extends Model
{
    use SoftDeletes;

    public $table = 'oc_confirm';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'email',
        'order_id',
        'payment_date',
        'total_amount',
        'destination_bank',
        'resi',
        'payment_method',
        'sender_name',
        'bank_origin',
        'code',
        'no_receipt',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'email' => 'string',
        'order_id' => 'string',
        'payment_date' => 'date',
        'total_amount' => 'integer',
        'destination_bank' => 'string',
        'resi' => 'string',
        'payment_method' => 'string',
        'sender_name' => 'string',
        'bank_origin' => 'string',
        'code' => 'string',
        'no_receipt' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
