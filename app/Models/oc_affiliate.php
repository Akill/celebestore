<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_affiliate
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property integer sub_district_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string password
 * @property string salt
 * @property string company
 * @property string website
 * @property string address_1
 * @property string address_2
 * @property string city
 * @property string postcode
 * @property integer country_id
 * @property integer zone_id
 * @property string code
 * @property decimal commission
 * @property string tax
 * @property string payment
 * @property string cheque
 * @property string paypal
 * @property string bank_name
 * @property string bank_branch_number
 * @property string bank_swift_code
 * @property string bank_account_name
 * @property string bank_account_number
 * @property string ip
 * @property boolean status
 * @property boolean approved
 * @property string|\Carbon\Carbon date_added
 */
class oc_affiliate extends Model
{
    use SoftDeletes;

    public $table = 'oc_affiliate';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sub_district_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'password',
        'salt',
        'company',
        'website',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'country_id',
        'zone_id',
        'code',
        'commission',
        'tax',
        'payment',
        'cheque',
        'paypal',
        'bank_name',
        'bank_branch_number',
        'bank_swift_code',
        'bank_account_name',
        'bank_account_number',
        'ip',
        'status',
        'approved',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'sub_district_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'password' => 'string',
        'salt' => 'string',
        'company' => 'string',
        'website' => 'string',
        'address_1' => 'string',
        'address_2' => 'string',
        'city' => 'string',
        'postcode' => 'string',
        'country_id' => 'integer',
        'zone_id' => 'integer',
        'code' => 'string',
        'tax' => 'string',
        'payment' => 'string',
        'cheque' => 'string',
        'paypal' => 'string',
        'bank_name' => 'string',
        'bank_branch_number' => 'string',
        'bank_swift_code' => 'string',
        'bank_account_name' => 'string',
        'bank_account_number' => 'string',
        'ip' => 'string',
        'status' => 'boolean',
        'approved' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
