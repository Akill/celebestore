<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_special
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_id
 * @property integer customer_group_id
 * @property integer priority
 * @property decimal price
 * @property date date_start
 * @property date date_end
 */
class oc_product_special extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_special';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'customer_group_id',
        'priority',
        'price',
        'date_start',
        'date_end'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_special_id' => 'integer',
        'product_id' => 'integer',
        'customer_group_id' => 'integer',
        'priority' => 'integer',
        'date_start' => 'date',
        'date_end' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
