<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_voucher
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer order_id
 * @property string code
 * @property string from_name
 * @property string from_email
 * @property string to_name
 * @property string to_email
 * @property integer voucher_theme_id
 * @property string message
 * @property decimal amount
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 */
class oc_voucher extends Model
{
    use SoftDeletes;

    public $table = 'oc_voucher';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'code',
        'from_name',
        'from_email',
        'to_name',
        'to_email',
        'voucher_theme_id',
        'message',
        'amount',
        'status',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'voucher_id' => 'integer',
        'order_id' => 'integer',
        'code' => 'string',
        'from_name' => 'string',
        'from_email' => 'string',
        'to_name' => 'string',
        'to_email' => 'string',
        'voucher_theme_id' => 'integer',
        'message' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
