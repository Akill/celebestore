<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_tax_rate_to_customer_group
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer customer_group_id
 */
class oc_tax_rate_to_customer_group extends Model
{
    use SoftDeletes;

    public $table = 'oc_tax_rate_to_customer_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_group_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tax_rate_id' => 'integer',
        'customer_group_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
