<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_review
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer product_id
 * @property integer customer_id
 * @property string author
 * @property string text
 * @property integer rating
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_review extends Model
{
    use SoftDeletes;

    public $table = 'oc_review';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'customer_id',
        'author',
        'text',
        'rating',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'review_id' => 'integer',
        'product_id' => 'integer',
        'customer_id' => 'integer',
        'author' => 'string',
        'text' => 'string',
        'rating' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
