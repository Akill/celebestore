<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_layout_route
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer layout_id
 * @property integer store_id
 * @property string route
 */
class oc_layout_route extends Model
{
    use SoftDeletes;

    public $table = 'oc_layout_route';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'layout_id',
        'store_id',
        'route'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'layout_route_id' => 'integer',
        'layout_id' => 'integer',
        'store_id' => 'integer',
        'route' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
