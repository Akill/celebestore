<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_option
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_id
 * @property integer option_id
 * @property string value
 * @property boolean required
 */
class oc_product_option extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_option';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'option_id',
        'value',
        'required'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_option_id' => 'integer',
        'product_id' => 'integer',
        'option_id' => 'integer',
        'value' => 'string',
        'required' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
