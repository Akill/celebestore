<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_country_hpwd
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string name
 * @property string iso_code_2
 * @property string iso_code_3
 * @property string address_format
 * @property boolean postcode_required
 * @property boolean status
 */
class oc_country_hpwd extends Model
{
    use SoftDeletes;

    public $table = 'oc_country_hpwd';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'iso_code_2',
        'iso_code_3',
        'address_format',
        'postcode_required',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'iso_code_2' => 'string',
        'iso_code_3' => 'string',
        'address_format' => 'string',
        'postcode_required' => 'boolean',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
