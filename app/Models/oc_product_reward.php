<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_reward
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_id
 * @property integer customer_group_id
 * @property integer points
 */
class oc_product_reward extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_reward';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'customer_group_id',
        'points'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_reward_id' => 'integer',
        'product_id' => 'integer',
        'customer_group_id' => 'integer',
        'points' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
