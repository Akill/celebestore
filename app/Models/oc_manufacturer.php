<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_manufacturer
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string image
 * @property integer sort_order
 */
class oc_manufacturer extends Model
{
    use SoftDeletes;

    public $table = 'oc_manufacturer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'image',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'manufacturer_id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
