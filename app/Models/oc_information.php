<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_information
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer bottom
 * @property integer sort_order
 * @property boolean status
 */
class oc_information extends Model
{
    use SoftDeletes;

    public $table = 'oc_information';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'bottom',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'information_id' => 'integer',
        'bottom' => 'integer',
        'sort_order' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
