<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_custom_field
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property string type
 * @property string value
 * @property string location
 * @property boolean status
 * @property integer sort_order
 */
class oc_custom_field extends Model
{
    use SoftDeletes;

    public $table = 'oc_custom_field';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'type',
        'value',
        'location',
        'status',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type' => 'string',
        'value' => 'string',
        'location' => 'string',
        'status' => 'boolean',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
