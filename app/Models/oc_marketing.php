<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_marketing
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string description
 * @property string code
 * @property integer clicks
 * @property string|\Carbon\Carbon date_added
 */
class oc_marketing extends Model
{
    use SoftDeletes;

    public $table = 'oc_marketing';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'code',
        'clicks',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'marketing_id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'code' => 'string',
        'clicks' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
