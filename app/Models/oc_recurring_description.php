<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_recurring_description
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer language_id
 * @property string name
 */
class oc_recurring_description extends Model
{
    use SoftDeletes;

    public $table = 'oc_recurring_description';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'language_id',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'recurring_id' => 'integer',
        'language_id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
