<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_return
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer order_id
 * @property integer product_id
 * @property integer customer_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string product
 * @property string model
 * @property integer quantity
 * @property boolean opened
 * @property integer return_reason_id
 * @property integer return_action_id
 * @property integer return_status_id
 * @property string comment
 * @property date date_ordered
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_return extends Model
{
    use SoftDeletes;

    public $table = 'oc_return';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'product_id',
        'customer_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'product',
        'model',
        'quantity',
        'opened',
        'return_reason_id',
        'return_action_id',
        'return_status_id',
        'comment',
        'date_ordered',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'return_id' => 'integer',
        'order_id' => 'integer',
        'product_id' => 'integer',
        'customer_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'telephone' => 'string',
        'product' => 'string',
        'model' => 'string',
        'quantity' => 'integer',
        'opened' => 'boolean',
        'return_reason_id' => 'integer',
        'return_action_id' => 'integer',
        'return_status_id' => 'integer',
        'comment' => 'string',
        'date_ordered' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
