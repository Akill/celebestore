<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_to_layout
 * @package App\Models
 * @version September 2, 2017, 4:51 pm UTC
 *
 * @property integer store_id
 * @property integer layout_id
 */
class oc_product_to_layout extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_to_layout';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'store_id',
        'layout_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'store_id' => 'integer',
        'layout_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
