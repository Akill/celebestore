<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_store
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property string name
 * @property string url
 * @property string ssl
 */
class oc_store extends Model
{
    use SoftDeletes;

    public $table = 'oc_store';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'url',
        'ssl'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'store_id' => 'integer',
        'name' => 'string',
        'url' => 'string',
        'ssl' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
