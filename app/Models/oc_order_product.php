<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_product
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property integer product_id
 * @property string name
 * @property string model
 * @property integer quantity
 * @property decimal price
 * @property decimal total
 * @property decimal tax
 * @property integer reward
 */
class oc_order_product extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_product';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'product_id',
        'name',
        'model',
        'quantity',
        'price',
        'total',
        'tax',
        'reward'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_product_id' => 'integer',
        'order_id' => 'integer',
        'product_id' => 'integer',
        'name' => 'string',
        'model' => 'string',
        'quantity' => 'integer',
        'reward' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
