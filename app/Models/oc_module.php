<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_module
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string code
 * @property string setting
 */
class oc_module extends Model
{
    use SoftDeletes;

    public $table = 'oc_module';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'setting'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'module_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'setting' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
