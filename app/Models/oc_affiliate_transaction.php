<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_affiliate_transaction
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property integer affiliate_id
 * @property integer order_id
 * @property string description
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 */
class oc_affiliate_transaction extends Model
{
    use SoftDeletes;

    public $table = 'oc_affiliate_transaction';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'affiliate_id',
        'order_id',
        'description',
        'amount',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'affiliate_id' => 'integer',
        'order_id' => 'integer',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
