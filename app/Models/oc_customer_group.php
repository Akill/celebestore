<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_customer_group
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer approval
 * @property integer sort_order
 */
class oc_customer_group extends Model
{
    use SoftDeletes;

    public $table = 'oc_customer_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'approval',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'approval' => 'integer',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
