<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_information_to_layout
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer store_id
 * @property integer layout_id
 */
class oc_information_to_layout extends Model
{
    use SoftDeletes;

    public $table = 'oc_information_to_layout';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'store_id',
        'layout_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'information_id' => 'integer',
        'store_id' => 'integer',
        'layout_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
