<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_currency
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property string title
 * @property string code
 * @property string symbol_left
 * @property string symbol_right
 * @property string decimal_place
 * @property float value
 * @property boolean status
 * @property string|\Carbon\Carbon date_modified
 */
class oc_currency extends Model
{
    use SoftDeletes;

    public $table = 'oc_currency';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'code',
        'symbol_left',
        'symbol_right',
        'decimal_place',
        'value',
        'status',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'code' => 'string',
        'symbol_left' => 'string',
        'symbol_right' => 'string',
        'decimal_place' => 'string',
        'value' => 'float',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
