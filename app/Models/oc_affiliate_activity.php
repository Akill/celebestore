<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_affiliate_activity
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property integer affiliate_id
 * @property string key
 * @property string data
 * @property string ip
 * @property string|\Carbon\Carbon date_added
 */
class oc_affiliate_activity extends Model
{
    use SoftDeletes;

    public $table = 'oc_affiliate_activity';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'affiliate_id',
        'key',
        'data',
        'ip',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'affiliate_id' => 'integer',
        'key' => 'string',
        'data' => 'string',
        'ip' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
