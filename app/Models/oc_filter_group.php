<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_filter_group
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property integer sort_order
 */
class oc_filter_group extends Model
{
    use SoftDeletes;

    public $table = 'oc_filter_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'filter_group_id' => 'integer',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
