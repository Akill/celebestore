<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_category
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string image
 * @property integer parent_id
 * @property boolean top
 * @property integer column
 * @property integer sort_order
 * @property boolean status
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_category extends Model
{
    use SoftDeletes;

    public $table = 'oc_category';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'image',
        'parent_id',
        'top',
        'column',
        'sort_order',
        'status',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image' => 'string',
        'parent_id' => 'integer',
        'top' => 'boolean',
        'column' => 'integer',
        'sort_order' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
