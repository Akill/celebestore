<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_filter
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer filter_group_id
 * @property integer sort_order
 */
class oc_filter extends Model
{
    use SoftDeletes;

    public $table = 'oc_filter';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'filter_group_id',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'filter_group_id' => 'integer',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
