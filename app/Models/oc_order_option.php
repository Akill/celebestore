<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_option
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property integer order_product_id
 * @property integer product_option_id
 * @property integer product_option_value_id
 * @property string name
 * @property string value
 * @property string type
 */
class oc_order_option extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_option';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'order_product_id',
        'product_option_id',
        'product_option_value_id',
        'name',
        'value',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_option_id' => 'integer',
        'order_id' => 'integer',
        'order_product_id' => 'integer',
        'product_option_id' => 'integer',
        'product_option_value_id' => 'integer',
        'name' => 'string',
        'value' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
