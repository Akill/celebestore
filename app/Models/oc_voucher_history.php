<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_voucher_history
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer voucher_id
 * @property integer order_id
 * @property decimal amount
 * @property string|\Carbon\Carbon date_added
 */
class oc_voucher_history extends Model
{
    use SoftDeletes;

    public $table = 'oc_voucher_history';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'voucher_id',
        'order_id',
        'amount',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'voucher_history_id' => 'integer',
        'voucher_id' => 'integer',
        'order_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
