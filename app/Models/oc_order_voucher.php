<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_order_voucher
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer order_id
 * @property integer voucher_id
 * @property string description
 * @property string code
 * @property string from_name
 * @property string from_email
 * @property string to_name
 * @property string to_email
 * @property integer voucher_theme_id
 * @property string message
 * @property decimal amount
 */
class oc_order_voucher extends Model
{
    use SoftDeletes;

    public $table = 'oc_order_voucher';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order_id',
        'voucher_id',
        'description',
        'code',
        'from_name',
        'from_email',
        'to_name',
        'to_email',
        'voucher_theme_id',
        'message',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'order_voucher_id' => 'integer',
        'order_id' => 'integer',
        'voucher_id' => 'integer',
        'description' => 'string',
        'code' => 'string',
        'from_name' => 'string',
        'from_email' => 'string',
        'to_name' => 'string',
        'to_email' => 'string',
        'voucher_theme_id' => 'integer',
        'message' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
