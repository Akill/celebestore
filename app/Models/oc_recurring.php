<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_recurring
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property decimal price
 * @property string frequency
 * @property integer duration
 * @property integer cycle
 * @property boolean trial_status
 * @property decimal trial_price
 * @property string trial_frequency
 * @property integer trial_duration
 * @property integer trial_cycle
 * @property boolean status
 * @property integer sort_order
 */
class oc_recurring extends Model
{
    use SoftDeletes;

    public $table = 'oc_recurring';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'price',
        'frequency',
        'duration',
        'cycle',
        'trial_status',
        'trial_price',
        'trial_frequency',
        'trial_duration',
        'trial_cycle',
        'status',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'recurring_id' => 'integer',
        'frequency' => 'string',
        'duration' => 'integer',
        'cycle' => 'integer',
        'trial_status' => 'boolean',
        'trial_frequency' => 'string',
        'trial_duration' => 'integer',
        'trial_cycle' => 'integer',
        'status' => 'boolean',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
