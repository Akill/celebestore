<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_tax_rate
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer geo_zone_id
 * @property string name
 * @property decimal rate
 * @property string type
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_tax_rate extends Model
{
    use SoftDeletes;

    public $table = 'oc_tax_rate';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'geo_zone_id',
        'name',
        'rate',
        'type',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tax_rate_id' => 'integer',
        'geo_zone_id' => 'integer',
        'name' => 'string',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
