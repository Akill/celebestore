<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\user;

/**
 * Class oc_user_group
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property string name
 * @property string permission
 */
class oc_user_group extends Model
{
    use SoftDeletes;

    public $table = 'oc_user_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'permission'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'permission' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function user(){
        return $this->belongsTo(user::class);
    }
}
