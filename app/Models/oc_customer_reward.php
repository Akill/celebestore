<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_customer_reward
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer customer_id
 * @property integer order_id
 * @property string description
 * @property integer points
 * @property string|\Carbon\Carbon date_added
 */
class oc_customer_reward extends Model
{
    use SoftDeletes;

    public $table = 'oc_customer_reward';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_id',
        'order_id',
        'description',
        'points',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'order_id' => 'integer',
        'description' => 'string',
        'points' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
