<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_image
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_id
 * @property string image
 * @property integer sort_order
 */
class oc_product_image extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_image';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_id',
        'image',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_image_id' => 'integer',
        'product_id' => 'integer',
        'image' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
