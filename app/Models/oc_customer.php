<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_customer
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer customer_group_id
 * @property integer store_id
 * @property string firstname
 * @property string lastname
 * @property string email
 * @property string telephone
 * @property string fax
 * @property string password
 * @property string salt
 * @property string cart
 * @property string wishlist
 * @property boolean newsletter
 * @property integer address_id
 * @property string custom_field
 * @property string ip
 * @property boolean status
 * @property boolean approved
 * @property boolean safe
 * @property string token
 * @property string|\Carbon\Carbon date_added
 */
class oc_customer extends Model
{
    use SoftDeletes;

    public $table = 'oc_customer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_group_id',
        'store_id',
        'firstname',
        'lastname',
        'email',
        'telephone',
        'fax',
        'password',
        'salt',
        'cart',
        'wishlist',
        'newsletter',
        'address_id',
        'custom_field',
        'ip',
        'status',
        'approved',
        'safe',
        'token',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_group_id' => 'integer',
        'store_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'email' => 'string',
        'telephone' => 'string',
        'fax' => 'string',
        'password' => 'string',
        'salt' => 'string',
        'cart' => 'string',
        'wishlist' => 'string',
        'newsletter' => 'boolean',
        'address_id' => 'integer',
        'custom_field' => 'string',
        'ip' => 'string',
        'status' => 'boolean',
        'approved' => 'boolean',
        'safe' => 'boolean',
        'token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
