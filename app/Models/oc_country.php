<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_country
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property string name
 * @property string code
 * @property string status
 * @property string address_format
 * @property string iso_code_2
 * @property string iso_code_3
 * @property string postcode_required
 */
class oc_country extends Model
{
    use SoftDeletes;

    public $table = 'oc_country';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'status',
        'address_format',
        'iso_code_2',
        'iso_code_3',
        'postcode_required'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'status' => 'string',
        'address_format' => 'string',
        'iso_code_2' => 'string',
        'iso_code_3' => 'string',
        'postcode_required' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
