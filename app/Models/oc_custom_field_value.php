<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_custom_field_value
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer custom_field_id
 * @property integer sort_order
 */
class oc_custom_field_value extends Model
{
    use SoftDeletes;

    public $table = 'oc_custom_field_value';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'custom_field_id',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'custom_field_id' => 'integer',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
