<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_setting
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property integer store_id
 * @property string code
 * @property string key
 * @property string value
 * @property boolean serialized
 */
class oc_setting extends Model
{
    use SoftDeletes;

    public $table = 'oc_setting';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'store_id',
        'code',
        'key',
        'value',
        'serialized'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'setting_id' => 'integer',
        'store_id' => 'integer',
        'code' => 'string',
        'key' => 'string',
        'value' => 'string',
        'serialized' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
