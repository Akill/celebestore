<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_custom_field_customer_group
 * @package App\Models
 * @version August 26, 2017, 7:38 am UTC
 *
 * @property integer customer_group_id
 * @property boolean required
 */
class oc_custom_field_customer_group extends Model
{
    use SoftDeletes;

    public $table = 'oc_custom_field_customer_group';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'customer_group_id',
        'required'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_group_id' => 'integer',
        'required' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
