<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_zone_to_geo_zone
 * @package App\Models
 * @version August 26, 2017, 8:03 am UTC
 *
 * @property integer country_id
 * @property integer zone_id
 * @property integer geo_zone_id
 * @property string|\Carbon\Carbon date_added
 * @property string|\Carbon\Carbon date_modified
 */
class oc_zone_to_geo_zone extends Model
{
    use SoftDeletes;

    public $table = 'oc_zone_to_geo_zone';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'country_id',
        'zone_id',
        'geo_zone_id',
        'date_added',
        'date_modified'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'zone_to_geo_zone_id' => 'integer',
        'country_id' => 'integer',
        'zone_id' => 'integer',
        'geo_zone_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
