<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_option_value
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer product_option_id
 * @property integer product_id
 * @property integer option_id
 * @property integer option_value_id
 * @property integer quantity
 * @property boolean subtract
 * @property decimal price
 * @property string price_prefix
 * @property integer points
 * @property string points_prefix
 * @property decimal weight
 * @property string weight_prefix
 */
class oc_product_option_value extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_option_value';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'product_option_id',
        'product_id',
        'option_id',
        'option_value_id',
        'quantity',
        'subtract',
        'price',
        'price_prefix',
        'points',
        'points_prefix',
        'weight',
        'weight_prefix'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_option_value_id' => 'integer',
        'product_option_id' => 'integer',
        'product_id' => 'integer',
        'option_id' => 'integer',
        'option_value_id' => 'integer',
        'quantity' => 'integer',
        'subtract' => 'boolean',
        'price_prefix' => 'string',
        'points' => 'integer',
        'points_prefix' => 'string',
        'weight_prefix' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
