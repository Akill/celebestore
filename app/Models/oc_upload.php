<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_upload
 * @package App\Models
 * @version August 26, 2017, 8:02 am UTC
 *
 * @property string name
 * @property string filename
 * @property string code
 * @property string|\Carbon\Carbon date_added
 */
class oc_upload extends Model
{
    use SoftDeletes;

    public $table = 'oc_upload';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'filename',
        'code',
        'date_added'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'upload_id' => 'integer',
        'name' => 'string',
        'filename' => 'string',
        'code' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
