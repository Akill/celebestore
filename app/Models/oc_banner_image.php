<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_banner_image
 * @package App\Models
 * @version August 26, 2017, 7:37 am UTC
 *
 * @property integer banner_id
 * @property string link
 * @property string image
 * @property integer sort_order
 */
class oc_banner_image extends Model
{
    use SoftDeletes;

    public $table = 'oc_banner_image';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'banner_id',
        'link',
        'image',
        'sort_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'banner_id' => 'integer',
        'link' => 'string',
        'image' => 'string',
        'sort_order' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
