<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_language
 * @package App\Models
 * @version August 26, 2017, 8:00 am UTC
 *
 * @property string name
 * @property string code
 * @property string locale
 * @property string image
 * @property string directory
 * @property integer sort_order
 * @property boolean status
 */
class oc_language extends Model
{
    use SoftDeletes;

    public $table = 'oc_language';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'code',
        'locale',
        'image',
        'directory',
        'sort_order',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'language_id' => 'integer',
        'name' => 'string',
        'code' => 'string',
        'locale' => 'string',
        'image' => 'string',
        'directory' => 'string',
        'sort_order' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
