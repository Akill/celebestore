<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class oc_product_recurring
 * @package App\Models
 * @version August 26, 2017, 8:01 am UTC
 *
 * @property integer recurring_id
 * @property integer customer_group_id
 */
class oc_product_recurring extends Model
{
    use SoftDeletes;

    public $table = 'oc_product_recurring';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'recurring_id',
        'customer_group_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'product_id' => 'integer',
        'recurring_id' => 'integer',
        'customer_group_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
