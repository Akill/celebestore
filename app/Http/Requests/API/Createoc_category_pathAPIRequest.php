<?php

namespace App\Http\Requests\API;

use App\Models\oc_category_path;
use InfyOm\Generator\Request\APIRequest;

class Createoc_category_pathAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_category_path::$rules;
    }
}
