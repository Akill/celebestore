<?php

namespace App\Http\Requests\API;

use App\Models\oc_return_reason;
use InfyOm\Generator\Request\APIRequest;

class Createoc_return_reasonAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_return_reason::$rules;
    }
}
