<?php

namespace App\Http\Requests\API;

use App\Models\oc_url_alias;
use InfyOm\Generator\Request\APIRequest;

class Createoc_url_aliasAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_url_alias::$rules;
    }
}
