<?php

namespace App\Http\Requests\API;

use App\Models\oc_option_value_description;
use InfyOm\Generator\Request\APIRequest;

class Updateoc_option_value_descriptionAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_option_value_description::$rules;
    }
}
