<?php

namespace App\Http\Requests\API;

use App\Models\oc_tax_rule;
use InfyOm\Generator\Request\APIRequest;

class Updateoc_tax_ruleAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_tax_rule::$rules;
    }
}
