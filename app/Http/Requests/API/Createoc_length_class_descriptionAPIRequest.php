<?php

namespace App\Http\Requests\API;

use App\Models\oc_length_class_description;
use InfyOm\Generator\Request\APIRequest;

class Createoc_length_class_descriptionAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_length_class_description::$rules;
    }
}
