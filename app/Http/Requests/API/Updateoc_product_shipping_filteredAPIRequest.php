<?php

namespace App\Http\Requests\API;

use App\Models\oc_product_shipping_filtered;
use InfyOm\Generator\Request\APIRequest;

class Updateoc_product_shipping_filteredAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_product_shipping_filtered::$rules;
    }
}
