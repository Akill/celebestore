<?php

namespace App\Http\Requests\API;

use App\Models\oc_customer_transaction;
use InfyOm\Generator\Request\APIRequest;

class Updateoc_customer_transactionAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return oc_customer_transaction::$rules;
    }
}
