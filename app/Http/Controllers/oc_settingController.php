<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_settingRequest;
use App\Http\Requests\Updateoc_settingRequest;
use App\Repositories\oc_settingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_settingController extends AppBaseController
{
    /** @var  oc_settingRepository */
    private $ocSettingRepository;

    public function __construct(oc_settingRepository $ocSettingRepo)
    {
        $this->ocSettingRepository = $ocSettingRepo;
    }

    /**
     * Display a listing of the oc_setting.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocSettingRepository->pushCriteria(new RequestCriteria($request));
        $ocSettings = $this->ocSettingRepository->paginate(20);

        return view('oc_settings.index')
            ->with('ocSettings', $ocSettings);
    }

    /**
     * Show the form for creating a new oc_setting.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_settings.create');
    }

    /**
     * Store a newly created oc_setting in storage.
     *
     * @param Createoc_settingRequest $request
     *
     * @return Response
     */
    public function store(Createoc_settingRequest $request)
    {
        $input = $request->all();

        $ocSetting = $this->ocSettingRepository->create($input);

        Flash::success('Oc Setting saved successfully.');

        return redirect(route('ocSettings.index'));
    }

    /**
     * Display the specified oc_setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            Flash::error('Oc Setting not found');

            return redirect(route('ocSettings.index'));
        }

        return view('oc_settings.show')->with('ocSetting', $ocSetting);
    }

    /**
     * Show the form for editing the specified oc_setting.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            Flash::error('Oc Setting not found');

            return redirect(route('ocSettings.index'));
        }

        return view('oc_settings.edit')->with('ocSetting', $ocSetting);
    }

    /**
     * Update the specified oc_setting in storage.
     *
     * @param  int              $id
     * @param Updateoc_settingRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_settingRequest $request)
    {
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            Flash::error('Oc Setting not found');

            return redirect(route('ocSettings.index'));
        }

        $ocSetting = $this->ocSettingRepository->update($request->all(), $id);

        Flash::success('Oc Setting updated successfully.');

        return redirect(route('ocSettings.index'));
    }

    /**
     * Remove the specified oc_setting from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            Flash::error('Oc Setting not found');

            return redirect(route('ocSettings.index'));
        }

        $this->ocSettingRepository->delete($id);

        Flash::success('Oc Setting deleted successfully.');

        return redirect(route('ocSettings.index'));
    }
}
