<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_user_groupRequest;
use App\Http\Requests\Updateoc_user_groupRequest;
use App\Repositories\oc_user_groupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_user_groupController extends AppBaseController
{
    /** @var  oc_user_groupRepository */
    private $ocUserGroupRepository;

    public function __construct(oc_user_groupRepository $ocUserGroupRepo)
    {
        $this->ocUserGroupRepository = $ocUserGroupRepo;
    }

    /**
     * Display a listing of the oc_user_group.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUserGroupRepository->pushCriteria(new RequestCriteria($request));
        $ocUserGroups = $this->ocUserGroupRepository->paginate(20);

        return view('oc_user_groups.index')
            ->with('ocUserGroups', $ocUserGroups);
    }

    /**
     * Show the form for creating a new oc_user_group.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_user_groups.create');
    }

    /**
     * Store a newly created oc_user_group in storage.
     *
     * @param Createoc_user_groupRequest $request
     *
     * @return Response
     */
    public function store(Createoc_user_groupRequest $request)
    {
        $input = $request->all();

        $ocUserGroup = $this->ocUserGroupRepository->create($input);

        Flash::success('Oc User Group saved successfully.');

        return redirect(route('ocUserGroups.index'));
    }

    /**
     * Display the specified oc_user_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            Flash::error('Oc User Group not found');

            return redirect(route('ocUserGroups.index'));
        }

        return view('oc_user_groups.show')->with('ocUserGroup', $ocUserGroup);
    }

    /**
     * Show the form for editing the specified oc_user_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            Flash::error('Oc User Group not found');

            return redirect(route('ocUserGroups.index'));
        }

        return view('oc_user_groups.edit')->with('ocUserGroup', $ocUserGroup);
    }

    /**
     * Update the specified oc_user_group in storage.
     *
     * @param  int              $id
     * @param Updateoc_user_groupRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_user_groupRequest $request)
    {
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            Flash::error('Oc User Group not found');

            return redirect(route('ocUserGroups.index'));
        }

        $ocUserGroup = $this->ocUserGroupRepository->update($request->all(), $id);

        Flash::success('Oc User Group updated successfully.');

        return redirect(route('ocUserGroups.index'));
    }

    /**
     * Remove the specified oc_user_group from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            Flash::error('Oc User Group not found');

            return redirect(route('ocUserGroups.index'));
        }

        $this->ocUserGroupRepository->delete($id);

        Flash::success('Oc User Group deleted successfully.');

        return redirect(route('ocUserGroups.index'));
    }
}
