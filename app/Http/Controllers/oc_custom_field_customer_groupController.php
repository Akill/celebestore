<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_custom_field_customer_groupRequest;
use App\Http\Requests\Updateoc_custom_field_customer_groupRequest;
use App\Repositories\oc_custom_field_customer_groupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_custom_field_customer_groupController extends AppBaseController
{
    /** @var  oc_custom_field_customer_groupRepository */
    private $ocCustomFieldCustomerGroupRepository;

    public function __construct(oc_custom_field_customer_groupRepository $ocCustomFieldCustomerGroupRepo)
    {
        $this->ocCustomFieldCustomerGroupRepository = $ocCustomFieldCustomerGroupRepo;
    }

    /**
     * Display a listing of the oc_custom_field_customer_group.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldCustomerGroupRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomFieldCustomerGroups = $this->ocCustomFieldCustomerGroupRepository->paginate(20);

        return view('oc_custom_field_customer_groups.index')
            ->with('ocCustomFieldCustomerGroups', $ocCustomFieldCustomerGroups);
    }

    /**
     * Show the form for creating a new oc_custom_field_customer_group.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_custom_field_customer_groups.create');
    }

    /**
     * Store a newly created oc_custom_field_customer_group in storage.
     *
     * @param Createoc_custom_field_customer_groupRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_customer_groupRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->create($input);

        Flash::success('Oc Custom Field Customer Group saved successfully.');

        return redirect(route('ocCustomFieldCustomerGroups.index'));
    }

    /**
     * Display the specified oc_custom_field_customer_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            Flash::error('Oc Custom Field Customer Group not found');

            return redirect(route('ocCustomFieldCustomerGroups.index'));
        }

        return view('oc_custom_field_customer_groups.show')->with('ocCustomFieldCustomerGroup', $ocCustomFieldCustomerGroup);
    }

    /**
     * Show the form for editing the specified oc_custom_field_customer_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            Flash::error('Oc Custom Field Customer Group not found');

            return redirect(route('ocCustomFieldCustomerGroups.index'));
        }

        return view('oc_custom_field_customer_groups.edit')->with('ocCustomFieldCustomerGroup', $ocCustomFieldCustomerGroup);
    }

    /**
     * Update the specified oc_custom_field_customer_group in storage.
     *
     * @param  int              $id
     * @param Updateoc_custom_field_customer_groupRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_customer_groupRequest $request)
    {
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            Flash::error('Oc Custom Field Customer Group not found');

            return redirect(route('ocCustomFieldCustomerGroups.index'));
        }

        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->update($request->all(), $id);

        Flash::success('Oc Custom Field Customer Group updated successfully.');

        return redirect(route('ocCustomFieldCustomerGroups.index'));
    }

    /**
     * Remove the specified oc_custom_field_customer_group from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            Flash::error('Oc Custom Field Customer Group not found');

            return redirect(route('ocCustomFieldCustomerGroups.index'));
        }

        $this->ocCustomFieldCustomerGroupRepository->delete($id);

        Flash::success('Oc Custom Field Customer Group deleted successfully.');

        return redirect(route('ocCustomFieldCustomerGroups.index'));
    }
}
