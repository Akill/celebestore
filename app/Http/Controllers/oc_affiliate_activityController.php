<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_affiliate_activityRequest;
use App\Http\Requests\Updateoc_affiliate_activityRequest;
use App\Repositories\oc_affiliate_activityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_affiliate_activityController extends AppBaseController
{
    /** @var  oc_affiliate_activityRepository */
    private $ocAffiliateActivityRepository;

    public function __construct(oc_affiliate_activityRepository $ocAffiliateActivityRepo)
    {
        $this->ocAffiliateActivityRepository = $ocAffiliateActivityRepo;
    }

    /**
     * Display a listing of the oc_affiliate_activity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateActivityRepository->pushCriteria(new RequestCriteria($request));
        $ocAffiliateActivities = $this->ocAffiliateActivityRepository->paginate(20);

        return view('oc_affiliate_activities.index')
            ->with('ocAffiliateActivities', $ocAffiliateActivities);
    }

    /**
     * Show the form for creating a new oc_affiliate_activity.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_affiliate_activities.create');
    }

    /**
     * Store a newly created oc_affiliate_activity in storage.
     *
     * @param Createoc_affiliate_activityRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_activityRequest $request)
    {
        $input = $request->all();

        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->create($input);

        Flash::success('Oc Affiliate Activity saved successfully.');

        return redirect(route('ocAffiliateActivities.index'));
    }

    /**
     * Display the specified oc_affiliate_activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            Flash::error('Oc Affiliate Activity not found');

            return redirect(route('ocAffiliateActivities.index'));
        }

        return view('oc_affiliate_activities.show')->with('ocAffiliateActivity', $ocAffiliateActivity);
    }

    /**
     * Show the form for editing the specified oc_affiliate_activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            Flash::error('Oc Affiliate Activity not found');

            return redirect(route('ocAffiliateActivities.index'));
        }

        return view('oc_affiliate_activities.edit')->with('ocAffiliateActivity', $ocAffiliateActivity);
    }

    /**
     * Update the specified oc_affiliate_activity in storage.
     *
     * @param  int              $id
     * @param Updateoc_affiliate_activityRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_activityRequest $request)
    {
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            Flash::error('Oc Affiliate Activity not found');

            return redirect(route('ocAffiliateActivities.index'));
        }

        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->update($request->all(), $id);

        Flash::success('Oc Affiliate Activity updated successfully.');

        return redirect(route('ocAffiliateActivities.index'));
    }

    /**
     * Remove the specified oc_affiliate_activity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            Flash::error('Oc Affiliate Activity not found');

            return redirect(route('ocAffiliateActivities.index'));
        }

        $this->ocAffiliateActivityRepository->delete($id);

        Flash::success('Oc Affiliate Activity deleted successfully.');

        return redirect(route('ocAffiliateActivities.index'));
    }
}
