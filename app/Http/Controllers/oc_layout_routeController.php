<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_layout_routeRequest;
use App\Http\Requests\Updateoc_layout_routeRequest;
use App\Repositories\oc_layout_routeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_layout_routeController extends AppBaseController
{
    /** @var  oc_layout_routeRepository */
    private $ocLayoutRouteRepository;

    public function __construct(oc_layout_routeRepository $ocLayoutRouteRepo)
    {
        $this->ocLayoutRouteRepository = $ocLayoutRouteRepo;
    }

    /**
     * Display a listing of the oc_layout_route.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLayoutRouteRepository->pushCriteria(new RequestCriteria($request));
        $ocLayoutRoutes = $this->ocLayoutRouteRepository->paginate(20);

        return view('oc_layout_routes.index')
            ->with('ocLayoutRoutes', $ocLayoutRoutes);
    }

    /**
     * Show the form for creating a new oc_layout_route.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_layout_routes.create');
    }

    /**
     * Store a newly created oc_layout_route in storage.
     *
     * @param Createoc_layout_routeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_layout_routeRequest $request)
    {
        $input = $request->all();

        $ocLayoutRoute = $this->ocLayoutRouteRepository->create($input);

        Flash::success('Oc Layout Route saved successfully.');

        return redirect(route('ocLayoutRoutes.index'));
    }

    /**
     * Display the specified oc_layout_route.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            Flash::error('Oc Layout Route not found');

            return redirect(route('ocLayoutRoutes.index'));
        }

        return view('oc_layout_routes.show')->with('ocLayoutRoute', $ocLayoutRoute);
    }

    /**
     * Show the form for editing the specified oc_layout_route.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            Flash::error('Oc Layout Route not found');

            return redirect(route('ocLayoutRoutes.index'));
        }

        return view('oc_layout_routes.edit')->with('ocLayoutRoute', $ocLayoutRoute);
    }

    /**
     * Update the specified oc_layout_route in storage.
     *
     * @param  int              $id
     * @param Updateoc_layout_routeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_layout_routeRequest $request)
    {
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            Flash::error('Oc Layout Route not found');

            return redirect(route('ocLayoutRoutes.index'));
        }

        $ocLayoutRoute = $this->ocLayoutRouteRepository->update($request->all(), $id);

        Flash::success('Oc Layout Route updated successfully.');

        return redirect(route('ocLayoutRoutes.index'));
    }

    /**
     * Remove the specified oc_layout_route from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            Flash::error('Oc Layout Route not found');

            return redirect(route('ocLayoutRoutes.index'));
        }

        $this->ocLayoutRouteRepository->delete($id);

        Flash::success('Oc Layout Route deleted successfully.');

        return redirect(route('ocLayoutRoutes.index'));
    }
}
