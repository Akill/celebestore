<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_specialRequest;
use App\Http\Requests\Updateoc_product_specialRequest;
use App\Repositories\oc_product_specialRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_specialController extends AppBaseController
{
    /** @var  oc_product_specialRepository */
    private $ocProductSpecialRepository;

    public function __construct(oc_product_specialRepository $ocProductSpecialRepo)
    {
        $this->ocProductSpecialRepository = $ocProductSpecialRepo;
    }

    /**
     * Display a listing of the oc_product_special.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductSpecialRepository->pushCriteria(new RequestCriteria($request));
        $ocProductSpecials = $this->ocProductSpecialRepository->paginate(20);

        return view('oc_product_specials.index')
            ->with('ocProductSpecials', $ocProductSpecials);
    }

    /**
     * Show the form for creating a new oc_product_special.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_specials.create');
    }

    /**
     * Store a newly created oc_product_special in storage.
     *
     * @param Createoc_product_specialRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_specialRequest $request)
    {
        $input = $request->all();

        $ocProductSpecial = $this->ocProductSpecialRepository->create($input);

        Flash::success('Oc Product Special saved successfully.');

        return redirect(route('ocProductSpecials.index'));
    }

    /**
     * Display the specified oc_product_special.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            Flash::error('Oc Product Special not found');

            return redirect(route('ocProductSpecials.index'));
        }

        return view('oc_product_specials.show')->with('ocProductSpecial', $ocProductSpecial);
    }

    /**
     * Show the form for editing the specified oc_product_special.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            Flash::error('Oc Product Special not found');

            return redirect(route('ocProductSpecials.index'));
        }

        return view('oc_product_specials.edit')->with('ocProductSpecial', $ocProductSpecial);
    }

    /**
     * Update the specified oc_product_special in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_specialRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_specialRequest $request)
    {
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            Flash::error('Oc Product Special not found');

            return redirect(route('ocProductSpecials.index'));
        }

        $ocProductSpecial = $this->ocProductSpecialRepository->update($request->all(), $id);

        Flash::success('Oc Product Special updated successfully.');

        return redirect(route('ocProductSpecials.index'));
    }

    /**
     * Remove the specified oc_product_special from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            Flash::error('Oc Product Special not found');

            return redirect(route('ocProductSpecials.index'));
        }

        $this->ocProductSpecialRepository->delete($id);

        Flash::success('Oc Product Special deleted successfully.');

        return redirect(route('ocProductSpecials.index'));
    }
}
