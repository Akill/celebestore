<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_productRequest;
use App\Http\Requests\Updateoc_order_productRequest;
use App\Repositories\oc_order_productRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_productController extends AppBaseController
{
    /** @var  oc_order_productRepository */
    private $ocOrderProductRepository;

    public function __construct(oc_order_productRepository $ocOrderProductRepo)
    {
        $this->ocOrderProductRepository = $ocOrderProductRepo;
    }

    /**
     * Display a listing of the oc_order_product.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderProductRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderProducts = $this->ocOrderProductRepository->paginate(20);

        return view('oc_order_products.index')
            ->with('ocOrderProducts', $ocOrderProducts);
    }

    /**
     * Show the form for creating a new oc_order_product.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_products.create');
    }

    /**
     * Store a newly created oc_order_product in storage.
     *
     * @param Createoc_order_productRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_productRequest $request)
    {
        $input = $request->all();

        $ocOrderProduct = $this->ocOrderProductRepository->create($input);

        Flash::success('Oc Order Product saved successfully.');

        return redirect(route('ocOrderProducts.index'));
    }

    /**
     * Display the specified oc_order_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            Flash::error('Oc Order Product not found');

            return redirect(route('ocOrderProducts.index'));
        }

        return view('oc_order_products.show')->with('ocOrderProduct', $ocOrderProduct);
    }

    /**
     * Show the form for editing the specified oc_order_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            Flash::error('Oc Order Product not found');

            return redirect(route('ocOrderProducts.index'));
        }

        return view('oc_order_products.edit')->with('ocOrderProduct', $ocOrderProduct);
    }

    /**
     * Update the specified oc_order_product in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_productRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_productRequest $request)
    {
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            Flash::error('Oc Order Product not found');

            return redirect(route('ocOrderProducts.index'));
        }

        $ocOrderProduct = $this->ocOrderProductRepository->update($request->all(), $id);

        Flash::success('Oc Order Product updated successfully.');

        return redirect(route('ocOrderProducts.index'));
    }

    /**
     * Remove the specified oc_order_product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            Flash::error('Oc Order Product not found');

            return redirect(route('ocOrderProducts.index'));
        }

        $this->ocOrderProductRepository->delete($id);

        Flash::success('Oc Order Product deleted successfully.');

        return redirect(route('ocOrderProducts.index'));
    }
}
