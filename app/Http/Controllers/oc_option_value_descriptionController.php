<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_option_value_descriptionRequest;
use App\Http\Requests\Updateoc_option_value_descriptionRequest;
use App\Repositories\oc_option_value_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_option_value_descriptionController extends AppBaseController
{
    /** @var  oc_option_value_descriptionRepository */
    private $ocOptionValueDescriptionRepository;

    public function __construct(oc_option_value_descriptionRepository $ocOptionValueDescriptionRepo)
    {
        $this->ocOptionValueDescriptionRepository = $ocOptionValueDescriptionRepo;
    }

    /**
     * Display a listing of the oc_option_value_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionValueDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocOptionValueDescriptions = $this->ocOptionValueDescriptionRepository->paginate(20);

        return view('oc_option_value_descriptions.index')
            ->with('ocOptionValueDescriptions', $ocOptionValueDescriptions);
    }

    /**
     * Show the form for creating a new oc_option_value_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_option_value_descriptions.create');
    }

    /**
     * Store a newly created oc_option_value_description in storage.
     *
     * @param Createoc_option_value_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_option_value_descriptionRequest $request)
    {
        $input = $request->all();

        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->create($input);

        Flash::success('Oc Option Value Description saved successfully.');

        return redirect(route('ocOptionValueDescriptions.index'));
    }

    /**
     * Display the specified oc_option_value_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            Flash::error('Oc Option Value Description not found');

            return redirect(route('ocOptionValueDescriptions.index'));
        }

        return view('oc_option_value_descriptions.show')->with('ocOptionValueDescription', $ocOptionValueDescription);
    }

    /**
     * Show the form for editing the specified oc_option_value_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            Flash::error('Oc Option Value Description not found');

            return redirect(route('ocOptionValueDescriptions.index'));
        }

        return view('oc_option_value_descriptions.edit')->with('ocOptionValueDescription', $ocOptionValueDescription);
    }

    /**
     * Update the specified oc_option_value_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_option_value_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_option_value_descriptionRequest $request)
    {
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            Flash::error('Oc Option Value Description not found');

            return redirect(route('ocOptionValueDescriptions.index'));
        }

        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Option Value Description updated successfully.');

        return redirect(route('ocOptionValueDescriptions.index'));
    }

    /**
     * Remove the specified oc_option_value_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            Flash::error('Oc Option Value Description not found');

            return redirect(route('ocOptionValueDescriptions.index'));
        }

        $this->ocOptionValueDescriptionRepository->delete($id);

        Flash::success('Oc Option Value Description deleted successfully.');

        return redirect(route('ocOptionValueDescriptions.index'));
    }
}
