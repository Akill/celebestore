<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_historyRequest;
use App\Http\Requests\Updateoc_order_historyRequest;
use App\Repositories\oc_order_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_historyController extends AppBaseController
{
    /** @var  oc_order_historyRepository */
    private $ocOrderHistoryRepository;

    public function __construct(oc_order_historyRepository $ocOrderHistoryRepo)
    {
        $this->ocOrderHistoryRepository = $ocOrderHistoryRepo;
    }

    /**
     * Display a listing of the oc_order_history.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderHistoryRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderHistories = $this->ocOrderHistoryRepository->paginate(20);

        return view('oc_order_histories.index')
            ->with('ocOrderHistories', $ocOrderHistories);
    }

    /**
     * Show the form for creating a new oc_order_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_histories.create');
    }

    /**
     * Store a newly created oc_order_history in storage.
     *
     * @param Createoc_order_historyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_historyRequest $request)
    {
        $input = $request->all();

        $ocOrderHistory = $this->ocOrderHistoryRepository->create($input);

        Flash::success('Oc Order History saved successfully.');

        return redirect(route('ocOrderHistories.index'));
    }

    /**
     * Display the specified oc_order_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            Flash::error('Oc Order History not found');

            return redirect(route('ocOrderHistories.index'));
        }

        return view('oc_order_histories.show')->with('ocOrderHistory', $ocOrderHistory);
    }

    /**
     * Show the form for editing the specified oc_order_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            Flash::error('Oc Order History not found');

            return redirect(route('ocOrderHistories.index'));
        }

        return view('oc_order_histories.edit')->with('ocOrderHistory', $ocOrderHistory);
    }

    /**
     * Update the specified oc_order_history in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_historyRequest $request)
    {
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            Flash::error('Oc Order History not found');

            return redirect(route('ocOrderHistories.index'));
        }

        $ocOrderHistory = $this->ocOrderHistoryRepository->update($request->all(), $id);

        Flash::success('Oc Order History updated successfully.');

        return redirect(route('ocOrderHistories.index'));
    }

    /**
     * Remove the specified oc_order_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            Flash::error('Oc Order History not found');

            return redirect(route('ocOrderHistories.index'));
        }

        $this->ocOrderHistoryRepository->delete($id);

        Flash::success('Oc Order History deleted successfully.');

        return redirect(route('ocOrderHistories.index'));
    }
}
