<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_filter_descriptionRequest;
use App\Http\Requests\Updateoc_filter_descriptionRequest;
use App\Repositories\oc_filter_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_filter_descriptionController extends AppBaseController
{
    /** @var  oc_filter_descriptionRepository */
    private $ocFilterDescriptionRepository;

    public function __construct(oc_filter_descriptionRepository $ocFilterDescriptionRepo)
    {
        $this->ocFilterDescriptionRepository = $ocFilterDescriptionRepo;
    }

    /**
     * Display a listing of the oc_filter_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocFilterDescriptions = $this->ocFilterDescriptionRepository->paginate(20);

        return view('oc_filter_descriptions.index')
            ->with('ocFilterDescriptions', $ocFilterDescriptions);
    }

    /**
     * Show the form for creating a new oc_filter_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_filter_descriptions.create');
    }

    /**
     * Store a newly created oc_filter_description in storage.
     *
     * @param Createoc_filter_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filter_descriptionRequest $request)
    {
        $input = $request->all();

        $ocFilterDescription = $this->ocFilterDescriptionRepository->create($input);

        Flash::success('Oc Filter Description saved successfully.');

        return redirect(route('ocFilterDescriptions.index'));
    }

    /**
     * Display the specified oc_filter_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            Flash::error('Oc Filter Description not found');

            return redirect(route('ocFilterDescriptions.index'));
        }

        return view('oc_filter_descriptions.show')->with('ocFilterDescription', $ocFilterDescription);
    }

    /**
     * Show the form for editing the specified oc_filter_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            Flash::error('Oc Filter Description not found');

            return redirect(route('ocFilterDescriptions.index'));
        }

        return view('oc_filter_descriptions.edit')->with('ocFilterDescription', $ocFilterDescription);
    }

    /**
     * Update the specified oc_filter_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_filter_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filter_descriptionRequest $request)
    {
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            Flash::error('Oc Filter Description not found');

            return redirect(route('ocFilterDescriptions.index'));
        }

        $ocFilterDescription = $this->ocFilterDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Filter Description updated successfully.');

        return redirect(route('ocFilterDescriptions.index'));
    }

    /**
     * Remove the specified oc_filter_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            Flash::error('Oc Filter Description not found');

            return redirect(route('ocFilterDescriptions.index'));
        }

        $this->ocFilterDescriptionRepository->delete($id);

        Flash::success('Oc Filter Description deleted successfully.');

        return redirect(route('ocFilterDescriptions.index'));
    }
}
