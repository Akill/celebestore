<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_transactionRequest;
use App\Http\Requests\Updateoc_customer_transactionRequest;
use App\Repositories\oc_customer_transactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_transactionController extends AppBaseController
{
    /** @var  oc_customer_transactionRepository */
    private $ocCustomerTransactionRepository;

    public function __construct(oc_customer_transactionRepository $ocCustomerTransactionRepo)
    {
        $this->ocCustomerTransactionRepository = $ocCustomerTransactionRepo;
    }

    /**
     * Display a listing of the oc_customer_transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerTransactionRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerTransactions = $this->ocCustomerTransactionRepository->paginate(20);

        return view('oc_customer_transactions.index')
            ->with('ocCustomerTransactions', $ocCustomerTransactions);
    }

    /**
     * Show the form for creating a new oc_customer_transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_transactions.create');
    }

    /**
     * Store a newly created oc_customer_transaction in storage.
     *
     * @param Createoc_customer_transactionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_transactionRequest $request)
    {
        $input = $request->all();

        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->create($input);

        Flash::success('Oc Customer Transaction saved successfully.');

        return redirect(route('ocCustomerTransactions.index'));
    }

    /**
     * Display the specified oc_customer_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            Flash::error('Oc Customer Transaction not found');

            return redirect(route('ocCustomerTransactions.index'));
        }

        return view('oc_customer_transactions.show')->with('ocCustomerTransaction', $ocCustomerTransaction);
    }

    /**
     * Show the form for editing the specified oc_customer_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            Flash::error('Oc Customer Transaction not found');

            return redirect(route('ocCustomerTransactions.index'));
        }

        return view('oc_customer_transactions.edit')->with('ocCustomerTransaction', $ocCustomerTransaction);
    }

    /**
     * Update the specified oc_customer_transaction in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_transactionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_transactionRequest $request)
    {
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            Flash::error('Oc Customer Transaction not found');

            return redirect(route('ocCustomerTransactions.index'));
        }

        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->update($request->all(), $id);

        Flash::success('Oc Customer Transaction updated successfully.');

        return redirect(route('ocCustomerTransactions.index'));
    }

    /**
     * Remove the specified oc_customer_transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            Flash::error('Oc Customer Transaction not found');

            return redirect(route('ocCustomerTransactions.index'));
        }

        $this->ocCustomerTransactionRepository->delete($id);

        Flash::success('Oc Customer Transaction deleted successfully.');

        return redirect(route('ocCustomerTransactions.index'));
    }
}
