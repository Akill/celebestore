<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_voucherRequest;
use App\Http\Requests\Updateoc_order_voucherRequest;
use App\Repositories\oc_order_voucherRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_voucherController extends AppBaseController
{
    /** @var  oc_order_voucherRepository */
    private $ocOrderVoucherRepository;

    public function __construct(oc_order_voucherRepository $ocOrderVoucherRepo)
    {
        $this->ocOrderVoucherRepository = $ocOrderVoucherRepo;
    }

    /**
     * Display a listing of the oc_order_voucher.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderVoucherRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderVouchers = $this->ocOrderVoucherRepository->paginate(20);

        return view('oc_order_vouchers.index')
            ->with('ocOrderVouchers', $ocOrderVouchers);
    }

    /**
     * Show the form for creating a new oc_order_voucher.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_vouchers.create');
    }

    /**
     * Store a newly created oc_order_voucher in storage.
     *
     * @param Createoc_order_voucherRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_voucherRequest $request)
    {
        $input = $request->all();

        $ocOrderVoucher = $this->ocOrderVoucherRepository->create($input);

        Flash::success('Oc Order Voucher saved successfully.');

        return redirect(route('ocOrderVouchers.index'));
    }

    /**
     * Display the specified oc_order_voucher.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            Flash::error('Oc Order Voucher not found');

            return redirect(route('ocOrderVouchers.index'));
        }

        return view('oc_order_vouchers.show')->with('ocOrderVoucher', $ocOrderVoucher);
    }

    /**
     * Show the form for editing the specified oc_order_voucher.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            Flash::error('Oc Order Voucher not found');

            return redirect(route('ocOrderVouchers.index'));
        }

        return view('oc_order_vouchers.edit')->with('ocOrderVoucher', $ocOrderVoucher);
    }

    /**
     * Update the specified oc_order_voucher in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_voucherRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_voucherRequest $request)
    {
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            Flash::error('Oc Order Voucher not found');

            return redirect(route('ocOrderVouchers.index'));
        }

        $ocOrderVoucher = $this->ocOrderVoucherRepository->update($request->all(), $id);

        Flash::success('Oc Order Voucher updated successfully.');

        return redirect(route('ocOrderVouchers.index'));
    }

    /**
     * Remove the specified oc_order_voucher from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            Flash::error('Oc Order Voucher not found');

            return redirect(route('ocOrderVouchers.index'));
        }

        $this->ocOrderVoucherRepository->delete($id);

        Flash::success('Oc Order Voucher deleted successfully.');

        return redirect(route('ocOrderVouchers.index'));
    }
}
