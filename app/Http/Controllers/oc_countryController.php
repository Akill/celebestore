<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_countryRequest;
use App\Http\Requests\Updateoc_countryRequest;
use App\Repositories\oc_countryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_countryController extends AppBaseController
{
    /** @var  oc_countryRepository */
    private $ocCountryRepository;

    public function __construct(oc_countryRepository $ocCountryRepo)
    {
        $this->ocCountryRepository = $ocCountryRepo;
    }

    /**
     * Display a listing of the oc_country.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCountryRepository->pushCriteria(new RequestCriteria($request));
        $ocCountries = $this->ocCountryRepository->paginate(20);

        return view('oc_countries.index')
            ->with('ocCountries', $ocCountries);
    }

    /**
     * Show the form for creating a new oc_country.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_countries.create');
    }

    /**
     * Store a newly created oc_country in storage.
     *
     * @param Createoc_countryRequest $request
     *
     * @return Response
     */
    public function store(Createoc_countryRequest $request)
    {
        $input = $request->all();

        $ocCountry = $this->ocCountryRepository->create($input);

        Flash::success('Oc Country saved successfully.');

        return redirect(route('ocCountries.index'));
    }

    /**
     * Display the specified oc_country.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            Flash::error('Oc Country not found');

            return redirect(route('ocCountries.index'));
        }

        return view('oc_countries.show')->with('ocCountry', $ocCountry);
    }

    /**
     * Show the form for editing the specified oc_country.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            Flash::error('Oc Country not found');

            return redirect(route('ocCountries.index'));
        }

        return view('oc_countries.edit')->with('ocCountry', $ocCountry);
    }

    /**
     * Update the specified oc_country in storage.
     *
     * @param  int              $id
     * @param Updateoc_countryRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_countryRequest $request)
    {
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            Flash::error('Oc Country not found');

            return redirect(route('ocCountries.index'));
        }

        $ocCountry = $this->ocCountryRepository->update($request->all(), $id);

        Flash::success('Oc Country updated successfully.');

        return redirect(route('ocCountries.index'));
    }

    /**
     * Remove the specified oc_country from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            Flash::error('Oc Country not found');

            return redirect(route('ocCountries.index'));
        }

        $this->ocCountryRepository->delete($id);

        Flash::success('Oc Country deleted successfully.');

        return redirect(route('ocCountries.index'));
    }
}
