<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_discountRequest;
use App\Http\Requests\Updateoc_product_discountRequest;
use App\Repositories\oc_product_discountRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_discountController extends AppBaseController
{
    /** @var  oc_product_discountRepository */
    private $ocProductDiscountRepository;

    public function __construct(oc_product_discountRepository $ocProductDiscountRepo)
    {
        $this->ocProductDiscountRepository = $ocProductDiscountRepo;
    }

    /**
     * Display a listing of the oc_product_discount.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductDiscountRepository->pushCriteria(new RequestCriteria($request));
        $ocProductDiscounts = $this->ocProductDiscountRepository->all();

        return view('oc_product_discounts.index')
            ->with('ocProductDiscounts', $ocProductDiscounts);
    }

    /**
     * Show the form for creating a new oc_product_discount.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_discounts.create');
    }

    /**
     * Store a newly created oc_product_discount in storage.
     *
     * @param Createoc_product_discountRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_discountRequest $request)
    {
        $input = $request->all();

        $ocProductDiscount = $this->ocProductDiscountRepository->create($input);

        Flash::success('Oc Product Discount saved successfully.');

        return redirect(route('ocProductDiscounts.index'));
    }

    /**
     * Display the specified oc_product_discount.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            Flash::error('Oc Product Discount not found');

            return redirect(route('ocProductDiscounts.index'));
        }

        return view('oc_product_discounts.show')->with('ocProductDiscount', $ocProductDiscount);
    }

    /**
     * Show the form for editing the specified oc_product_discount.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            Flash::error('Oc Product Discount not found');

            return redirect(route('ocProductDiscounts.index'));
        }

        return view('oc_product_discounts.edit')->with('ocProductDiscount', $ocProductDiscount);
    }

    /**
     * Update the specified oc_product_discount in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_discountRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_discountRequest $request)
    {
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            Flash::error('Oc Product Discount not found');

            return redirect(route('ocProductDiscounts.index'));
        }

        $ocProductDiscount = $this->ocProductDiscountRepository->update($request->all(), $id);

        Flash::success('Oc Product Discount updated successfully.');

        return redirect(route('ocProductDiscounts.index'));
    }

    /**
     * Remove the specified oc_product_discount from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            Flash::error('Oc Product Discount not found');

            return redirect(route('ocProductDiscounts.index'));
        }

        $this->ocProductDiscountRepository->delete($id);

        Flash::success('Oc Product Discount deleted successfully.');

        return redirect(route('ocProductDiscounts.index'));
    }
}
