<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_eventRequest;
use App\Http\Requests\Updateoc_eventRequest;
use App\Repositories\oc_eventRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_eventController extends AppBaseController
{
    /** @var  oc_eventRepository */
    private $ocEventRepository;

    public function __construct(oc_eventRepository $ocEventRepo)
    {
        $this->ocEventRepository = $ocEventRepo;
    }

    /**
     * Display a listing of the oc_event.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocEventRepository->pushCriteria(new RequestCriteria($request));
        $ocEvents = $this->ocEventRepository->paginate(20);

        return view('oc_events.index')
            ->with('ocEvents', $ocEvents);
    }

    /**
     * Show the form for creating a new oc_event.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_events.create');
    }

    /**
     * Store a newly created oc_event in storage.
     *
     * @param Createoc_eventRequest $request
     *
     * @return Response
     */
    public function store(Createoc_eventRequest $request)
    {
        $input = $request->all();

        $ocEvent = $this->ocEventRepository->create($input);

        Flash::success('Oc Event saved successfully.');

        return redirect(route('ocEvents.index'));
    }

    /**
     * Display the specified oc_event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            Flash::error('Oc Event not found');

            return redirect(route('ocEvents.index'));
        }

        return view('oc_events.show')->with('ocEvent', $ocEvent);
    }

    /**
     * Show the form for editing the specified oc_event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            Flash::error('Oc Event not found');

            return redirect(route('ocEvents.index'));
        }

        return view('oc_events.edit')->with('ocEvent', $ocEvent);
    }

    /**
     * Update the specified oc_event in storage.
     *
     * @param  int              $id
     * @param Updateoc_eventRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_eventRequest $request)
    {
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            Flash::error('Oc Event not found');

            return redirect(route('ocEvents.index'));
        }

        $ocEvent = $this->ocEventRepository->update($request->all(), $id);

        Flash::success('Oc Event updated successfully.');

        return redirect(route('ocEvents.index'));
    }

    /**
     * Remove the specified oc_event from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            Flash::error('Oc Event not found');

            return redirect(route('ocEvents.index'));
        }

        $this->ocEventRepository->delete($id);

        Flash::success('Oc Event deleted successfully.');

        return redirect(route('ocEvents.index'));
    }
}
