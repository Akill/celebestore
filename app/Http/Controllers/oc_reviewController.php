<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_reviewRequest;
use App\Http\Requests\Updateoc_reviewRequest;
use App\Repositories\oc_reviewRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_reviewController extends AppBaseController
{
    /** @var  oc_reviewRepository */
    private $ocReviewRepository;

    public function __construct(oc_reviewRepository $ocReviewRepo)
    {
        $this->ocReviewRepository = $ocReviewRepo;
    }

    /**
     * Display a listing of the oc_review.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReviewRepository->pushCriteria(new RequestCriteria($request));
        $ocReviews = $this->ocReviewRepository->paginate(20);

        return view('oc_reviews.index')
            ->with('ocReviews', $ocReviews);
    }

    /**
     * Show the form for creating a new oc_review.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_reviews.create');
    }

    /**
     * Store a newly created oc_review in storage.
     *
     * @param Createoc_reviewRequest $request
     *
     * @return Response
     */
    public function store(Createoc_reviewRequest $request)
    {
        $input = $request->all();

        $ocReview = $this->ocReviewRepository->create($input);

        Flash::success('Oc Review saved successfully.');

        return redirect(route('ocReviews.index'));
    }

    /**
     * Display the specified oc_review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            Flash::error('Oc Review not found');

            return redirect(route('ocReviews.index'));
        }

        return view('oc_reviews.show')->with('ocReview', $ocReview);
    }

    /**
     * Show the form for editing the specified oc_review.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            Flash::error('Oc Review not found');

            return redirect(route('ocReviews.index'));
        }

        return view('oc_reviews.edit')->with('ocReview', $ocReview);
    }

    /**
     * Update the specified oc_review in storage.
     *
     * @param  int              $id
     * @param Updateoc_reviewRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_reviewRequest $request)
    {
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            Flash::error('Oc Review not found');

            return redirect(route('ocReviews.index'));
        }

        $ocReview = $this->ocReviewRepository->update($request->all(), $id);

        Flash::success('Oc Review updated successfully.');

        return redirect(route('ocReviews.index'));
    }

    /**
     * Remove the specified oc_review from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            Flash::error('Oc Review not found');

            return redirect(route('ocReviews.index'));
        }

        $this->ocReviewRepository->delete($id);

        Flash::success('Oc Review deleted successfully.');

        return redirect(route('ocReviews.index'));
    }
}
