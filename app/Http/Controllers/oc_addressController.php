<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_addressRequest;
use App\Http\Requests\Updateoc_addressRequest;
use App\Repositories\oc_addressRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_addressController extends AppBaseController
{
    /** @var  oc_addressRepository */
    private $ocAddressRepository;

    public function __construct(oc_addressRepository $ocAddressRepo)
    {
        $this->ocAddressRepository = $ocAddressRepo;
    }

    /**
     * Display a listing of the oc_address.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAddressRepository->pushCriteria(new RequestCriteria($request));
        $ocAddresses = $this->ocAddressRepository->paginate(20);

        return view('oc_addresses.index')
            ->with('ocAddresses', $ocAddresses);
    }

    /**
     * Show the form for creating a new oc_address.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_addresses.create');
    }

    /**
     * Store a newly created oc_address in storage.
     *
     * @param Createoc_addressRequest $request
     *
     * @return Response
     */
    public function store(Createoc_addressRequest $request)
    {
        $input = $request->all();

        $ocAddress = $this->ocAddressRepository->create($input);

        Flash::success('Oc Address saved successfully.');

        return redirect(route('ocAddresses.index'));
    }

    /**
     * Display the specified oc_address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            Flash::error('Oc Address not found');

            return redirect(route('ocAddresses.index'));
        }

        return view('oc_addresses.show')->with('ocAddress', $ocAddress);
    }

    /**
     * Show the form for editing the specified oc_address.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            Flash::error('Oc Address not found');

            return redirect(route('ocAddresses.index'));
        }

        return view('oc_addresses.edit')->with('ocAddress', $ocAddress);
    }

    /**
     * Update the specified oc_address in storage.
     *
     * @param  int              $id
     * @param Updateoc_addressRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_addressRequest $request)
    {
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            Flash::error('Oc Address not found');

            return redirect(route('ocAddresses.index'));
        }

        $ocAddress = $this->ocAddressRepository->update($request->all(), $id);

        Flash::success('Oc Address updated successfully.');

        return redirect(route('ocAddresses.index'));
    }

    /**
     * Remove the specified oc_address from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            Flash::error('Oc Address not found');

            return redirect(route('ocAddresses.index'));
        }

        $this->ocAddressRepository->delete($id);

        Flash::success('Oc Address deleted successfully.');

        return redirect(route('ocAddresses.index'));
    }
}
