<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_moduleRequest;
use App\Http\Requests\Updateoc_moduleRequest;
use App\Repositories\oc_moduleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_moduleController extends AppBaseController
{
    /** @var  oc_moduleRepository */
    private $ocModuleRepository;

    public function __construct(oc_moduleRepository $ocModuleRepo)
    {
        $this->ocModuleRepository = $ocModuleRepo;
    }

    /**
     * Display a listing of the oc_module.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocModuleRepository->pushCriteria(new RequestCriteria($request));
        $ocModules = $this->ocModuleRepository->paginate(20);

        return view('oc_modules.index')
            ->with('ocModules', $ocModules);
    }

    /**
     * Show the form for creating a new oc_module.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_modules.create');
    }

    /**
     * Store a newly created oc_module in storage.
     *
     * @param Createoc_moduleRequest $request
     *
     * @return Response
     */
    public function store(Createoc_moduleRequest $request)
    {
        $input = $request->all();

        $ocModule = $this->ocModuleRepository->create($input);

        Flash::success('Oc Module saved successfully.');

        return redirect(route('ocModules.index'));
    }

    /**
     * Display the specified oc_module.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            Flash::error('Oc Module not found');

            return redirect(route('ocModules.index'));
        }

        return view('oc_modules.show')->with('ocModule', $ocModule);
    }

    /**
     * Show the form for editing the specified oc_module.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            Flash::error('Oc Module not found');

            return redirect(route('ocModules.index'));
        }

        return view('oc_modules.edit')->with('ocModule', $ocModule);
    }

    /**
     * Update the specified oc_module in storage.
     *
     * @param  int              $id
     * @param Updateoc_moduleRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_moduleRequest $request)
    {
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            Flash::error('Oc Module not found');

            return redirect(route('ocModules.index'));
        }

        $ocModule = $this->ocModuleRepository->update($request->all(), $id);

        Flash::success('Oc Module updated successfully.');

        return redirect(route('ocModules.index'));
    }

    /**
     * Remove the specified oc_module from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            Flash::error('Oc Module not found');

            return redirect(route('ocModules.index'));
        }

        $this->ocModuleRepository->delete($id);

        Flash::success('Oc Module deleted successfully.');

        return redirect(route('ocModules.index'));
    }
}
