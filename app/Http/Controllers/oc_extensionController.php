<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_extensionRequest;
use App\Http\Requests\Updateoc_extensionRequest;
use App\Repositories\oc_extensionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_extensionController extends AppBaseController
{
    /** @var  oc_extensionRepository */
    private $ocExtensionRepository;

    public function __construct(oc_extensionRepository $ocExtensionRepo)
    {
        $this->ocExtensionRepository = $ocExtensionRepo;
    }

    /**
     * Display a listing of the oc_extension.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocExtensionRepository->pushCriteria(new RequestCriteria($request));
        $ocExtensions = $this->ocExtensionRepository->paginate(20);

        return view('oc_extensions.index')
            ->with('ocExtensions', $ocExtensions);
    }

    /**
     * Show the form for creating a new oc_extension.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_extensions.create');
    }

    /**
     * Store a newly created oc_extension in storage.
     *
     * @param Createoc_extensionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_extensionRequest $request)
    {
        $input = $request->all();

        $ocExtension = $this->ocExtensionRepository->create($input);

        Flash::success('Oc Extension saved successfully.');

        return redirect(route('ocExtensions.index'));
    }

    /**
     * Display the specified oc_extension.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            Flash::error('Oc Extension not found');

            return redirect(route('ocExtensions.index'));
        }

        return view('oc_extensions.show')->with('ocExtension', $ocExtension);
    }

    /**
     * Show the form for editing the specified oc_extension.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            Flash::error('Oc Extension not found');

            return redirect(route('ocExtensions.index'));
        }

        return view('oc_extensions.edit')->with('ocExtension', $ocExtension);
    }

    /**
     * Update the specified oc_extension in storage.
     *
     * @param  int              $id
     * @param Updateoc_extensionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_extensionRequest $request)
    {
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            Flash::error('Oc Extension not found');

            return redirect(route('ocExtensions.index'));
        }

        $ocExtension = $this->ocExtensionRepository->update($request->all(), $id);

        Flash::success('Oc Extension updated successfully.');

        return redirect(route('ocExtensions.index'));
    }

    /**
     * Remove the specified oc_extension from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            Flash::error('Oc Extension not found');

            return redirect(route('ocExtensions.index'));
        }

        $this->ocExtensionRepository->delete($id);

        Flash::success('Oc Extension deleted successfully.');

        return redirect(route('ocExtensions.index'));
    }
}
