<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_descriptionRequest;
use App\Http\Requests\Updateoc_product_descriptionRequest;
use App\Repositories\oc_product_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_descriptionController extends AppBaseController
{
    /** @var  oc_product_descriptionRepository */
    private $ocProductDescriptionRepository;

    public function __construct(oc_product_descriptionRepository $ocProductDescriptionRepo)
    {
        $this->ocProductDescriptionRepository = $ocProductDescriptionRepo;
    }

    /**
     * Display a listing of the oc_product_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocProductDescriptions = $this->ocProductDescriptionRepository->all();

        return view('oc_product_descriptions.index')
            ->with('ocProductDescriptions', $ocProductDescriptions);
    }

    /**
     * Show the form for creating a new oc_product_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_descriptions.create');
    }

    /**
     * Store a newly created oc_product_description in storage.
     *
     * @param Createoc_product_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_descriptionRequest $request)
    {
        $input = $request->all();

        $ocProductDescription = $this->ocProductDescriptionRepository->create($input);

        Flash::success('Oc Product Description saved successfully.');

        return redirect(route('ocProductDescriptions.index'));
    }

    /**
     * Display the specified oc_product_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            Flash::error('Oc Product Description not found');

            return redirect(route('ocProductDescriptions.index'));
        }

        return view('oc_product_descriptions.show')->with('ocProductDescription', $ocProductDescription);
    }

    /**
     * Show the form for editing the specified oc_product_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            Flash::error('Oc Product Description not found');

            return redirect(route('ocProductDescriptions.index'));
        }

        return view('oc_product_descriptions.edit')->with('ocProductDescription', $ocProductDescription);
    }

    /**
     * Update the specified oc_product_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_descriptionRequest $request)
    {
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            Flash::error('Oc Product Description not found');

            return redirect(route('ocProductDescriptions.index'));
        }

        $ocProductDescription = $this->ocProductDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Product Description updated successfully.');

        return redirect(route('ocProductDescriptions.index'));
    }

    /**
     * Remove the specified oc_product_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            Flash::error('Oc Product Description not found');

            return redirect(route('ocProductDescriptions.index'));
        }

        $this->ocProductDescriptionRepository->delete($id);

        Flash::success('Oc Product Description deleted successfully.');

        return redirect(route('ocProductDescriptions.index'));
    }
}
