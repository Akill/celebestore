<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_ipRequest;
use App\Http\Requests\Updateoc_customer_ipRequest;
use App\Repositories\oc_customer_ipRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_ipController extends AppBaseController
{
    /** @var  oc_customer_ipRepository */
    private $ocCustomerIpRepository;

    public function __construct(oc_customer_ipRepository $ocCustomerIpRepo)
    {
        $this->ocCustomerIpRepository = $ocCustomerIpRepo;
    }

    /**
     * Display a listing of the oc_customer_ip.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerIpRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerIps = $this->ocCustomerIpRepository->paginate(20);

        return view('oc_customer_ips.index')
            ->with('ocCustomerIps', $ocCustomerIps);
    }

    /**
     * Show the form for creating a new oc_customer_ip.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_ips.create');
    }

    /**
     * Store a newly created oc_customer_ip in storage.
     *
     * @param Createoc_customer_ipRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_ipRequest $request)
    {
        $input = $request->all();

        $ocCustomerIp = $this->ocCustomerIpRepository->create($input);

        Flash::success('Oc Customer Ip saved successfully.');

        return redirect(route('ocCustomerIps.index'));
    }

    /**
     * Display the specified oc_customer_ip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            Flash::error('Oc Customer Ip not found');

            return redirect(route('ocCustomerIps.index'));
        }

        return view('oc_customer_ips.show')->with('ocCustomerIp', $ocCustomerIp);
    }

    /**
     * Show the form for editing the specified oc_customer_ip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            Flash::error('Oc Customer Ip not found');

            return redirect(route('ocCustomerIps.index'));
        }

        return view('oc_customer_ips.edit')->with('ocCustomerIp', $ocCustomerIp);
    }

    /**
     * Update the specified oc_customer_ip in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_ipRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_ipRequest $request)
    {
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            Flash::error('Oc Customer Ip not found');

            return redirect(route('ocCustomerIps.index'));
        }

        $ocCustomerIp = $this->ocCustomerIpRepository->update($request->all(), $id);

        Flash::success('Oc Customer Ip updated successfully.');

        return redirect(route('ocCustomerIps.index'));
    }

    /**
     * Remove the specified oc_customer_ip from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            Flash::error('Oc Customer Ip not found');

            return redirect(route('ocCustomerIps.index'));
        }

        $this->ocCustomerIpRepository->delete($id);

        Flash::success('Oc Customer Ip deleted successfully.');

        return redirect(route('ocCustomerIps.index'));
    }
}
