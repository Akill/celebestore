<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_return_historyRequest;
use App\Http\Requests\Updateoc_return_historyRequest;
use App\Repositories\oc_return_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_return_historyController extends AppBaseController
{
    /** @var  oc_return_historyRepository */
    private $ocReturnHistoryRepository;

    public function __construct(oc_return_historyRepository $ocReturnHistoryRepo)
    {
        $this->ocReturnHistoryRepository = $ocReturnHistoryRepo;
    }

    /**
     * Display a listing of the oc_return_history.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnHistoryRepository->pushCriteria(new RequestCriteria($request));
        $ocReturnHistories = $this->ocReturnHistoryRepository->paginate(20);

        return view('oc_return_histories.index')
            ->with('ocReturnHistories', $ocReturnHistories);
    }

    /**
     * Show the form for creating a new oc_return_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_return_histories.create');
    }

    /**
     * Store a newly created oc_return_history in storage.
     *
     * @param Createoc_return_historyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_return_historyRequest $request)
    {
        $input = $request->all();

        $ocReturnHistory = $this->ocReturnHistoryRepository->create($input);

        Flash::success('Oc Return History saved successfully.');

        return redirect(route('ocReturnHistories.index'));
    }

    /**
     * Display the specified oc_return_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            Flash::error('Oc Return History not found');

            return redirect(route('ocReturnHistories.index'));
        }

        return view('oc_return_histories.show')->with('ocReturnHistory', $ocReturnHistory);
    }

    /**
     * Show the form for editing the specified oc_return_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            Flash::error('Oc Return History not found');

            return redirect(route('ocReturnHistories.index'));
        }

        return view('oc_return_histories.edit')->with('ocReturnHistory', $ocReturnHistory);
    }

    /**
     * Update the specified oc_return_history in storage.
     *
     * @param  int              $id
     * @param Updateoc_return_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_return_historyRequest $request)
    {
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            Flash::error('Oc Return History not found');

            return redirect(route('ocReturnHistories.index'));
        }

        $ocReturnHistory = $this->ocReturnHistoryRepository->update($request->all(), $id);

        Flash::success('Oc Return History updated successfully.');

        return redirect(route('ocReturnHistories.index'));
    }

    /**
     * Remove the specified oc_return_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            Flash::error('Oc Return History not found');

            return redirect(route('ocReturnHistories.index'));
        }

        $this->ocReturnHistoryRepository->delete($id);

        Flash::success('Oc Return History deleted successfully.');

        return redirect(route('ocReturnHistories.index'));
    }
}
