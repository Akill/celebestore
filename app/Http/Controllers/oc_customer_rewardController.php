<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_rewardRequest;
use App\Http\Requests\Updateoc_customer_rewardRequest;
use App\Repositories\oc_customer_rewardRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_rewardController extends AppBaseController
{
    /** @var  oc_customer_rewardRepository */
    private $ocCustomerRewardRepository;

    public function __construct(oc_customer_rewardRepository $ocCustomerRewardRepo)
    {
        $this->ocCustomerRewardRepository = $ocCustomerRewardRepo;
    }

    /**
     * Display a listing of the oc_customer_reward.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerRewardRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerRewards = $this->ocCustomerRewardRepository->paginate(20);

        return view('oc_customer_rewards.index')
            ->with('ocCustomerRewards', $ocCustomerRewards);
    }

    /**
     * Show the form for creating a new oc_customer_reward.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_rewards.create');
    }

    /**
     * Store a newly created oc_customer_reward in storage.
     *
     * @param Createoc_customer_rewardRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_rewardRequest $request)
    {
        $input = $request->all();

        $ocCustomerReward = $this->ocCustomerRewardRepository->create($input);

        Flash::success('Oc Customer Reward saved successfully.');

        return redirect(route('ocCustomerRewards.index'));
    }

    /**
     * Display the specified oc_customer_reward.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            Flash::error('Oc Customer Reward not found');

            return redirect(route('ocCustomerRewards.index'));
        }

        return view('oc_customer_rewards.show')->with('ocCustomerReward', $ocCustomerReward);
    }

    /**
     * Show the form for editing the specified oc_customer_reward.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            Flash::error('Oc Customer Reward not found');

            return redirect(route('ocCustomerRewards.index'));
        }

        return view('oc_customer_rewards.edit')->with('ocCustomerReward', $ocCustomerReward);
    }

    /**
     * Update the specified oc_customer_reward in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_rewardRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_rewardRequest $request)
    {
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            Flash::error('Oc Customer Reward not found');

            return redirect(route('ocCustomerRewards.index'));
        }

        $ocCustomerReward = $this->ocCustomerRewardRepository->update($request->all(), $id);

        Flash::success('Oc Customer Reward updated successfully.');

        return redirect(route('ocCustomerRewards.index'));
    }

    /**
     * Remove the specified oc_customer_reward from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            Flash::error('Oc Customer Reward not found');

            return redirect(route('ocCustomerRewards.index'));
        }

        $this->ocCustomerRewardRepository->delete($id);

        Flash::success('Oc Customer Reward deleted successfully.');

        return redirect(route('ocCustomerRewards.index'));
    }
}
