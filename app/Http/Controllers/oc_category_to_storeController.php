<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_category_to_storeRequest;
use App\Http\Requests\Updateoc_category_to_storeRequest;
use App\Repositories\oc_category_to_storeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_category_to_storeController extends AppBaseController
{
    /** @var  oc_category_to_storeRepository */
    private $ocCategoryToStoreRepository;

    public function __construct(oc_category_to_storeRepository $ocCategoryToStoreRepo)
    {
        $this->ocCategoryToStoreRepository = $ocCategoryToStoreRepo;
    }

    /**
     * Display a listing of the oc_category_to_store.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryToStoreRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryToStores = $this->ocCategoryToStoreRepository->paginate(20);

        return view('oc_category_to_stores.index')
            ->with('ocCategoryToStores', $ocCategoryToStores);
    }

    /**
     * Show the form for creating a new oc_category_to_store.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_category_to_stores.create');
    }

    /**
     * Store a newly created oc_category_to_store in storage.
     *
     * @param Createoc_category_to_storeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_to_storeRequest $request)
    {
        $input = $request->all();

        $ocCategoryToStore = $this->ocCategoryToStoreRepository->create($input);

        Flash::success('Oc Category To Store saved successfully.');

        return redirect(route('ocCategoryToStores.index'));
    }

    /**
     * Display the specified oc_category_to_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            Flash::error('Oc Category To Store not found');

            return redirect(route('ocCategoryToStores.index'));
        }

        return view('oc_category_to_stores.show')->with('ocCategoryToStore', $ocCategoryToStore);
    }

    /**
     * Show the form for editing the specified oc_category_to_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            Flash::error('Oc Category To Store not found');

            return redirect(route('ocCategoryToStores.index'));
        }

        return view('oc_category_to_stores.edit')->with('ocCategoryToStore', $ocCategoryToStore);
    }

    /**
     * Update the specified oc_category_to_store in storage.
     *
     * @param  int              $id
     * @param Updateoc_category_to_storeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_to_storeRequest $request)
    {
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            Flash::error('Oc Category To Store not found');

            return redirect(route('ocCategoryToStores.index'));
        }

        $ocCategoryToStore = $this->ocCategoryToStoreRepository->update($request->all(), $id);

        Flash::success('Oc Category To Store updated successfully.');

        return redirect(route('ocCategoryToStores.index'));
    }

    /**
     * Remove the specified oc_category_to_store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            Flash::error('Oc Category To Store not found');

            return redirect(route('ocCategoryToStores.index'));
        }

        $this->ocCategoryToStoreRepository->delete($id);

        Flash::success('Oc Category To Store deleted successfully.');

        return redirect(route('ocCategoryToStores.index'));
    }
}
