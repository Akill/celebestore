<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_to_layoutRequest;
use App\Http\Requests\Updateoc_product_to_layoutRequest;
use App\Repositories\oc_product_to_layoutRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_to_layoutController extends AppBaseController
{
    /** @var  oc_product_to_layoutRepository */
    private $ocProductToLayoutRepository;

    public function __construct(oc_product_to_layoutRepository $ocProductToLayoutRepo)
    {
        $this->ocProductToLayoutRepository = $ocProductToLayoutRepo;
    }

    /**
     * Display a listing of the oc_product_to_layout.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToLayoutRepository->pushCriteria(new RequestCriteria($request));
        $ocProductToLayouts = $this->ocProductToLayoutRepository->all();

        return view('oc_product_to_layouts.index')
            ->with('ocProductToLayouts', $ocProductToLayouts);
    }

    /**
     * Show the form for creating a new oc_product_to_layout.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_to_layouts.create');
    }

    /**
     * Store a newly created oc_product_to_layout in storage.
     *
     * @param Createoc_product_to_layoutRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_layoutRequest $request)
    {
        $input = $request->all();

        $ocProductToLayout = $this->ocProductToLayoutRepository->create($input);

        Flash::success('Oc Product To Layout saved successfully.');

        return redirect(route('ocProductToLayouts.index'));
    }

    /**
     * Display the specified oc_product_to_layout.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            Flash::error('Oc Product To Layout not found');

            return redirect(route('ocProductToLayouts.index'));
        }

        return view('oc_product_to_layouts.show')->with('ocProductToLayout', $ocProductToLayout);
    }

    /**
     * Show the form for editing the specified oc_product_to_layout.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            Flash::error('Oc Product To Layout not found');

            return redirect(route('ocProductToLayouts.index'));
        }

        return view('oc_product_to_layouts.edit')->with('ocProductToLayout', $ocProductToLayout);
    }

    /**
     * Update the specified oc_product_to_layout in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_to_layoutRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_layoutRequest $request)
    {
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            Flash::error('Oc Product To Layout not found');

            return redirect(route('ocProductToLayouts.index'));
        }

        $ocProductToLayout = $this->ocProductToLayoutRepository->update($request->all(), $id);

        Flash::success('Oc Product To Layout updated successfully.');

        return redirect(route('ocProductToLayouts.index'));
    }

    /**
     * Remove the specified oc_product_to_layout from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            Flash::error('Oc Product To Layout not found');

            return redirect(route('ocProductToLayouts.index'));
        }

        $this->ocProductToLayoutRepository->delete($id);

        Flash::success('Oc Product To Layout deleted successfully.');

        return redirect(route('ocProductToLayouts.index'));
    }
}
