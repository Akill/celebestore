<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_affiliate_transactionRequest;
use App\Http\Requests\Updateoc_affiliate_transactionRequest;
use App\Repositories\oc_affiliate_transactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_affiliate_transactionController extends AppBaseController
{
    /** @var  oc_affiliate_transactionRepository */
    private $ocAffiliateTransactionRepository;

    public function __construct(oc_affiliate_transactionRepository $ocAffiliateTransactionRepo)
    {
        $this->ocAffiliateTransactionRepository = $ocAffiliateTransactionRepo;
    }

    /**
     * Display a listing of the oc_affiliate_transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateTransactionRepository->pushCriteria(new RequestCriteria($request));
        $ocAffiliateTransactions = $this->ocAffiliateTransactionRepository->paginate(20);

        return view('oc_affiliate_transactions.index')
            ->with('ocAffiliateTransactions', $ocAffiliateTransactions);
    }

    /**
     * Show the form for creating a new oc_affiliate_transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_affiliate_transactions.create');
    }

    /**
     * Store a newly created oc_affiliate_transaction in storage.
     *
     * @param Createoc_affiliate_transactionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_transactionRequest $request)
    {
        $input = $request->all();

        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->create($input);

        Flash::success('Oc Affiliate Transaction saved successfully.');

        return redirect(route('ocAffiliateTransactions.index'));
    }

    /**
     * Display the specified oc_affiliate_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            Flash::error('Oc Affiliate Transaction not found');

            return redirect(route('ocAffiliateTransactions.index'));
        }

        return view('oc_affiliate_transactions.show')->with('ocAffiliateTransaction', $ocAffiliateTransaction);
    }

    /**
     * Show the form for editing the specified oc_affiliate_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            Flash::error('Oc Affiliate Transaction not found');

            return redirect(route('ocAffiliateTransactions.index'));
        }

        return view('oc_affiliate_transactions.edit')->with('ocAffiliateTransaction', $ocAffiliateTransaction);
    }

    /**
     * Update the specified oc_affiliate_transaction in storage.
     *
     * @param  int              $id
     * @param Updateoc_affiliate_transactionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_transactionRequest $request)
    {
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            Flash::error('Oc Affiliate Transaction not found');

            return redirect(route('ocAffiliateTransactions.index'));
        }

        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->update($request->all(), $id);

        Flash::success('Oc Affiliate Transaction updated successfully.');

        return redirect(route('ocAffiliateTransactions.index'));
    }

    /**
     * Remove the specified oc_affiliate_transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            Flash::error('Oc Affiliate Transaction not found');

            return redirect(route('ocAffiliateTransactions.index'));
        }

        $this->ocAffiliateTransactionRepository->delete($id);

        Flash::success('Oc Affiliate Transaction deleted successfully.');

        return redirect(route('ocAffiliateTransactions.index'));
    }
}
