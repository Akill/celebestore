<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_informationRequest;
use App\Http\Requests\Updateoc_informationRequest;
use App\Repositories\oc_informationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_informationController extends AppBaseController
{
    /** @var  oc_informationRepository */
    private $ocInformationRepository;

    public function __construct(oc_informationRepository $ocInformationRepo)
    {
        $this->ocInformationRepository = $ocInformationRepo;
    }

    /**
     * Display a listing of the oc_information.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationRepository->pushCriteria(new RequestCriteria($request));
        $ocInformations = $this->ocInformationRepository->paginate(20);

        return view('oc_informations.index')
            ->with('ocInformations', $ocInformations);
    }

    /**
     * Show the form for creating a new oc_information.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_informations.create');
    }

    /**
     * Store a newly created oc_information in storage.
     *
     * @param Createoc_informationRequest $request
     *
     * @return Response
     */
    public function store(Createoc_informationRequest $request)
    {
        $input = $request->all();

        $ocInformation = $this->ocInformationRepository->create($input);

        Flash::success('Oc Information saved successfully.');

        return redirect(route('ocInformations.index'));
    }

    /**
     * Display the specified oc_information.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            Flash::error('Oc Information not found');

            return redirect(route('ocInformations.index'));
        }

        return view('oc_informations.show')->with('ocInformation', $ocInformation);
    }

    /**
     * Show the form for editing the specified oc_information.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            Flash::error('Oc Information not found');

            return redirect(route('ocInformations.index'));
        }

        return view('oc_informations.edit')->with('ocInformation', $ocInformation);
    }

    /**
     * Update the specified oc_information in storage.
     *
     * @param  int              $id
     * @param Updateoc_informationRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_informationRequest $request)
    {
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            Flash::error('Oc Information not found');

            return redirect(route('ocInformations.index'));
        }

        $ocInformation = $this->ocInformationRepository->update($request->all(), $id);

        Flash::success('Oc Information updated successfully.');

        return redirect(route('ocInformations.index'));
    }

    /**
     * Remove the specified oc_information from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            Flash::error('Oc Information not found');

            return redirect(route('ocInformations.index'));
        }

        $this->ocInformationRepository->delete($id);

        Flash::success('Oc Information deleted successfully.');

        return redirect(route('ocInformations.index'));
    }
}
