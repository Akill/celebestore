<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_affiliateRequest;
use App\Http\Requests\Updateoc_affiliateRequest;
use App\Repositories\oc_affiliateRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_affiliateController extends AppBaseController
{
    /** @var  oc_affiliateRepository */
    private $ocAffiliateRepository;

    public function __construct(oc_affiliateRepository $ocAffiliateRepo)
    {
        $this->ocAffiliateRepository = $ocAffiliateRepo;
    }

    /**
     * Display a listing of the oc_affiliate.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateRepository->pushCriteria(new RequestCriteria($request));
        $ocAffiliates = $this->ocAffiliateRepository->paginate(20);

        return view('oc_affiliates.index')
            ->with('ocAffiliates', $ocAffiliates);
    }

    /**
     * Show the form for creating a new oc_affiliate.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_affiliates.create');
    }

    /**
     * Store a newly created oc_affiliate in storage.
     *
     * @param Createoc_affiliateRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliateRequest $request)
    {
        $input = $request->all();

        $ocAffiliate = $this->ocAffiliateRepository->create($input);

        Flash::success('Oc Affiliate saved successfully.');

        return redirect(route('ocAffiliates.index'));
    }

    /**
     * Display the specified oc_affiliate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            Flash::error('Oc Affiliate not found');

            return redirect(route('ocAffiliates.index'));
        }

        return view('oc_affiliates.show')->with('ocAffiliate', $ocAffiliate);
    }

    /**
     * Show the form for editing the specified oc_affiliate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            Flash::error('Oc Affiliate not found');

            return redirect(route('ocAffiliates.index'));
        }

        return view('oc_affiliates.edit')->with('ocAffiliate', $ocAffiliate);
    }

    /**
     * Update the specified oc_affiliate in storage.
     *
     * @param  int              $id
     * @param Updateoc_affiliateRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliateRequest $request)
    {
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            Flash::error('Oc Affiliate not found');

            return redirect(route('ocAffiliates.index'));
        }

        $ocAffiliate = $this->ocAffiliateRepository->update($request->all(), $id);

        Flash::success('Oc Affiliate updated successfully.');

        return redirect(route('ocAffiliates.index'));
    }

    /**
     * Remove the specified oc_affiliate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            Flash::error('Oc Affiliate not found');

            return redirect(route('ocAffiliates.index'));
        }

        $this->ocAffiliateRepository->delete($id);

        Flash::success('Oc Affiliate deleted successfully.');

        return redirect(route('ocAffiliates.index'));
    }
}
