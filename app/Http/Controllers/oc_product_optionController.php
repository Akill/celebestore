<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_optionRequest;
use App\Http\Requests\Updateoc_product_optionRequest;
use App\Repositories\oc_product_optionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_optionController extends AppBaseController
{
    /** @var  oc_product_optionRepository */
    private $ocProductOptionRepository;

    public function __construct(oc_product_optionRepository $ocProductOptionRepo)
    {
        $this->ocProductOptionRepository = $ocProductOptionRepo;
    }

    /**
     * Display a listing of the oc_product_option.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductOptionRepository->pushCriteria(new RequestCriteria($request));
        $ocProductOptions = $this->ocProductOptionRepository->paginate(20);

        return view('oc_product_options.index')
            ->with('ocProductOptions', $ocProductOptions);
    }

    /**
     * Show the form for creating a new oc_product_option.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_options.create');
    }

    /**
     * Store a newly created oc_product_option in storage.
     *
     * @param Createoc_product_optionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_optionRequest $request)
    {
        $input = $request->all();

        $ocProductOption = $this->ocProductOptionRepository->create($input);

        Flash::success('Oc Product Option saved successfully.');

        return redirect(route('ocProductOptions.index'));
    }

    /**
     * Display the specified oc_product_option.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            Flash::error('Oc Product Option not found');

            return redirect(route('ocProductOptions.index'));
        }

        return view('oc_product_options.show')->with('ocProductOption', $ocProductOption);
    }

    /**
     * Show the form for editing the specified oc_product_option.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            Flash::error('Oc Product Option not found');

            return redirect(route('ocProductOptions.index'));
        }

        return view('oc_product_options.edit')->with('ocProductOption', $ocProductOption);
    }

    /**
     * Update the specified oc_product_option in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_optionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_optionRequest $request)
    {
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            Flash::error('Oc Product Option not found');

            return redirect(route('ocProductOptions.index'));
        }

        $ocProductOption = $this->ocProductOptionRepository->update($request->all(), $id);

        Flash::success('Oc Product Option updated successfully.');

        return redirect(route('ocProductOptions.index'));
    }

    /**
     * Remove the specified oc_product_option from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            Flash::error('Oc Product Option not found');

            return redirect(route('ocProductOptions.index'));
        }

        $this->ocProductOptionRepository->delete($id);

        Flash::success('Oc Product Option deleted successfully.');

        return redirect(route('ocProductOptions.index'));
    }
}
