<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_recurringRequest;
use App\Http\Requests\Updateoc_recurringRequest;
use App\Repositories\oc_recurringRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_recurringController extends AppBaseController
{
    /** @var  oc_recurringRepository */
    private $ocRecurringRepository;

    public function __construct(oc_recurringRepository $ocRecurringRepo)
    {
        $this->ocRecurringRepository = $ocRecurringRepo;
    }

    /**
     * Display a listing of the oc_recurring.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocRecurringRepository->pushCriteria(new RequestCriteria($request));
        $ocRecurrings = $this->ocRecurringRepository->paginate(20);

        return view('oc_recurrings.index')
            ->with('ocRecurrings', $ocRecurrings);
    }

    /**
     * Show the form for creating a new oc_recurring.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_recurrings.create');
    }

    /**
     * Store a newly created oc_recurring in storage.
     *
     * @param Createoc_recurringRequest $request
     *
     * @return Response
     */
    public function store(Createoc_recurringRequest $request)
    {
        $input = $request->all();

        $ocRecurring = $this->ocRecurringRepository->create($input);

        Flash::success('Oc Recurring saved successfully.');

        return redirect(route('ocRecurrings.index'));
    }

    /**
     * Display the specified oc_recurring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            Flash::error('Oc Recurring not found');

            return redirect(route('ocRecurrings.index'));
        }

        return view('oc_recurrings.show')->with('ocRecurring', $ocRecurring);
    }

    /**
     * Show the form for editing the specified oc_recurring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            Flash::error('Oc Recurring not found');

            return redirect(route('ocRecurrings.index'));
        }

        return view('oc_recurrings.edit')->with('ocRecurring', $ocRecurring);
    }

    /**
     * Update the specified oc_recurring in storage.
     *
     * @param  int              $id
     * @param Updateoc_recurringRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_recurringRequest $request)
    {
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            Flash::error('Oc Recurring not found');

            return redirect(route('ocRecurrings.index'));
        }

        $ocRecurring = $this->ocRecurringRepository->update($request->all(), $id);

        Flash::success('Oc Recurring updated successfully.');

        return redirect(route('ocRecurrings.index'));
    }

    /**
     * Remove the specified oc_recurring from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            Flash::error('Oc Recurring not found');

            return redirect(route('ocRecurrings.index'));
        }

        $this->ocRecurringRepository->delete($id);

        Flash::success('Oc Recurring deleted successfully.');

        return redirect(route('ocRecurrings.index'));
    }
}
