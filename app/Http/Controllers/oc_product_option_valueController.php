<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_option_valueRequest;
use App\Http\Requests\Updateoc_product_option_valueRequest;
use App\Repositories\oc_product_option_valueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_option_valueController extends AppBaseController
{
    /** @var  oc_product_option_valueRepository */
    private $ocProductOptionValueRepository;

    public function __construct(oc_product_option_valueRepository $ocProductOptionValueRepo)
    {
        $this->ocProductOptionValueRepository = $ocProductOptionValueRepo;
    }

    /**
     * Display a listing of the oc_product_option_value.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductOptionValueRepository->pushCriteria(new RequestCriteria($request));
        $ocProductOptionValues = $this->ocProductOptionValueRepository->paginate(20);

        return view('oc_product_option_values.index')
            ->with('ocProductOptionValues', $ocProductOptionValues);
    }

    /**
     * Show the form for creating a new oc_product_option_value.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_option_values.create');
    }

    /**
     * Store a newly created oc_product_option_value in storage.
     *
     * @param Createoc_product_option_valueRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_option_valueRequest $request)
    {
        $input = $request->all();

        $ocProductOptionValue = $this->ocProductOptionValueRepository->create($input);

        Flash::success('Oc Product Option Value saved successfully.');

        return redirect(route('ocProductOptionValues.index'));
    }

    /**
     * Display the specified oc_product_option_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            Flash::error('Oc Product Option Value not found');

            return redirect(route('ocProductOptionValues.index'));
        }

        return view('oc_product_option_values.show')->with('ocProductOptionValue', $ocProductOptionValue);
    }

    /**
     * Show the form for editing the specified oc_product_option_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            Flash::error('Oc Product Option Value not found');

            return redirect(route('ocProductOptionValues.index'));
        }

        return view('oc_product_option_values.edit')->with('ocProductOptionValue', $ocProductOptionValue);
    }

    /**
     * Update the specified oc_product_option_value in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_option_valueRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_option_valueRequest $request)
    {
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            Flash::error('Oc Product Option Value not found');

            return redirect(route('ocProductOptionValues.index'));
        }

        $ocProductOptionValue = $this->ocProductOptionValueRepository->update($request->all(), $id);

        Flash::success('Oc Product Option Value updated successfully.');

        return redirect(route('ocProductOptionValues.index'));
    }

    /**
     * Remove the specified oc_product_option_value from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            Flash::error('Oc Product Option Value not found');

            return redirect(route('ocProductOptionValues.index'));
        }

        $this->ocProductOptionValueRepository->delete($id);

        Flash::success('Oc Product Option Value deleted successfully.');

        return redirect(route('ocProductOptionValues.index'));
    }
}
