<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_zoneRequest;
use App\Http\Requests\Updateoc_zoneRequest;
use App\Repositories\oc_zoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_zoneController extends AppBaseController
{
    /** @var  oc_zoneRepository */
    private $ocZoneRepository;

    public function __construct(oc_zoneRepository $ocZoneRepo)
    {
        $this->ocZoneRepository = $ocZoneRepo;
    }

    /**
     * Display a listing of the oc_zone.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocZoneRepository->pushCriteria(new RequestCriteria($request));
        $ocZones = $this->ocZoneRepository->paginate(20);

        return view('oc_zones.index')
            ->with('ocZones', $ocZones);
    }

    /**
     * Show the form for creating a new oc_zone.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_zones.create');
    }

    /**
     * Store a newly created oc_zone in storage.
     *
     * @param Createoc_zoneRequest $request
     *
     * @return Response
     */
    public function store(Createoc_zoneRequest $request)
    {
        $input = $request->all();

        $ocZone = $this->ocZoneRepository->create($input);

        Flash::success('Oc Zone saved successfully.');

        return redirect(route('ocZones.index'));
    }

    /**
     * Display the specified oc_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            Flash::error('Oc Zone not found');

            return redirect(route('ocZones.index'));
        }

        return view('oc_zones.show')->with('ocZone', $ocZone);
    }

    /**
     * Show the form for editing the specified oc_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            Flash::error('Oc Zone not found');

            return redirect(route('ocZones.index'));
        }

        return view('oc_zones.edit')->with('ocZone', $ocZone);
    }

    /**
     * Update the specified oc_zone in storage.
     *
     * @param  int              $id
     * @param Updateoc_zoneRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_zoneRequest $request)
    {
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            Flash::error('Oc Zone not found');

            return redirect(route('ocZones.index'));
        }

        $ocZone = $this->ocZoneRepository->update($request->all(), $id);

        Flash::success('Oc Zone updated successfully.');

        return redirect(route('ocZones.index'));
    }

    /**
     * Remove the specified oc_zone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            Flash::error('Oc Zone not found');

            return redirect(route('ocZones.index'));
        }

        $this->ocZoneRepository->delete($id);

        Flash::success('Oc Zone deleted successfully.');

        return redirect(route('ocZones.index'));
    }
}
