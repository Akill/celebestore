<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_zone_to_geo_zoneRequest;
use App\Http\Requests\Updateoc_zone_to_geo_zoneRequest;
use App\Repositories\oc_zone_to_geo_zoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_zone_to_geo_zoneController extends AppBaseController
{
    /** @var  oc_zone_to_geo_zoneRepository */
    private $ocZoneToGeoZoneRepository;

    public function __construct(oc_zone_to_geo_zoneRepository $ocZoneToGeoZoneRepo)
    {
        $this->ocZoneToGeoZoneRepository = $ocZoneToGeoZoneRepo;
    }

    /**
     * Display a listing of the oc_zone_to_geo_zone.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocZoneToGeoZoneRepository->pushCriteria(new RequestCriteria($request));
        $ocZoneToGeoZones = $this->ocZoneToGeoZoneRepository->paginate(20);

        return view('oc_zone_to_geo_zones.index')
            ->with('ocZoneToGeoZones', $ocZoneToGeoZones);
    }

    /**
     * Show the form for creating a new oc_zone_to_geo_zone.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_zone_to_geo_zones.create');
    }

    /**
     * Store a newly created oc_zone_to_geo_zone in storage.
     *
     * @param Createoc_zone_to_geo_zoneRequest $request
     *
     * @return Response
     */
    public function store(Createoc_zone_to_geo_zoneRequest $request)
    {
        $input = $request->all();

        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->create($input);

        Flash::success('Oc Zone To Geo Zone saved successfully.');

        return redirect(route('ocZoneToGeoZones.index'));
    }

    /**
     * Display the specified oc_zone_to_geo_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            Flash::error('Oc Zone To Geo Zone not found');

            return redirect(route('ocZoneToGeoZones.index'));
        }

        return view('oc_zone_to_geo_zones.show')->with('ocZoneToGeoZone', $ocZoneToGeoZone);
    }

    /**
     * Show the form for editing the specified oc_zone_to_geo_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            Flash::error('Oc Zone To Geo Zone not found');

            return redirect(route('ocZoneToGeoZones.index'));
        }

        return view('oc_zone_to_geo_zones.edit')->with('ocZoneToGeoZone', $ocZoneToGeoZone);
    }

    /**
     * Update the specified oc_zone_to_geo_zone in storage.
     *
     * @param  int              $id
     * @param Updateoc_zone_to_geo_zoneRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_zone_to_geo_zoneRequest $request)
    {
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            Flash::error('Oc Zone To Geo Zone not found');

            return redirect(route('ocZoneToGeoZones.index'));
        }

        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->update($request->all(), $id);

        Flash::success('Oc Zone To Geo Zone updated successfully.');

        return redirect(route('ocZoneToGeoZones.index'));
    }

    /**
     * Remove the specified oc_zone_to_geo_zone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            Flash::error('Oc Zone To Geo Zone not found');

            return redirect(route('ocZoneToGeoZones.index'));
        }

        $this->ocZoneToGeoZoneRepository->delete($id);

        Flash::success('Oc Zone To Geo Zone deleted successfully.');

        return redirect(route('ocZoneToGeoZones.index'));
    }
}
