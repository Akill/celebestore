<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_attributeRequest;
use App\Http\Requests\Updateoc_attributeRequest;
use App\Repositories\oc_attributeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_attributeController extends AppBaseController
{
    /** @var  oc_attributeRepository */
    private $ocAttributeRepository;

    public function __construct(oc_attributeRepository $ocAttributeRepo)
    {
        $this->ocAttributeRepository = $ocAttributeRepo;
    }

    /**
     * Display a listing of the oc_attribute.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeRepository->pushCriteria(new RequestCriteria($request));
        $ocAttributes = $this->ocAttributeRepository->paginate(20);

        return view('oc_attributes.index')
            ->with('ocAttributes', $ocAttributes);
    }

    /**
     * Show the form for creating a new oc_attribute.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_attributes.create');
    }

    /**
     * Store a newly created oc_attribute in storage.
     *
     * @param Createoc_attributeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attributeRequest $request)
    {
        $input = $request->all();

        $ocAttribute = $this->ocAttributeRepository->create($input);

        Flash::success('Oc Attribute saved successfully.');

        return redirect(route('ocAttributes.index'));
    }

    /**
     * Display the specified oc_attribute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            Flash::error('Oc Attribute not found');

            return redirect(route('ocAttributes.index'));
        }

        return view('oc_attributes.show')->with('ocAttribute', $ocAttribute);
    }

    /**
     * Show the form for editing the specified oc_attribute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            Flash::error('Oc Attribute not found');

            return redirect(route('ocAttributes.index'));
        }

        return view('oc_attributes.edit')->with('ocAttribute', $ocAttribute);
    }

    /**
     * Update the specified oc_attribute in storage.
     *
     * @param  int              $id
     * @param Updateoc_attributeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attributeRequest $request)
    {
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            Flash::error('Oc Attribute not found');

            return redirect(route('ocAttributes.index'));
        }

        $ocAttribute = $this->ocAttributeRepository->update($request->all(), $id);

        Flash::success('Oc Attribute updated successfully.');

        return redirect(route('ocAttributes.index'));
    }

    /**
     * Remove the specified oc_attribute from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            Flash::error('Oc Attribute not found');

            return redirect(route('ocAttributes.index'));
        }

        $this->ocAttributeRepository->delete($id);

        Flash::success('Oc Attribute deleted successfully.');

        return redirect(route('ocAttributes.index'));
    }
}
