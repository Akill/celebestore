<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_coupon_categoryRequest;
use App\Http\Requests\Updateoc_coupon_categoryRequest;
use App\Repositories\oc_coupon_categoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_coupon_categoryController extends AppBaseController
{
    /** @var  oc_coupon_categoryRepository */
    private $ocCouponCategoryRepository;

    public function __construct(oc_coupon_categoryRepository $ocCouponCategoryRepo)
    {
        $this->ocCouponCategoryRepository = $ocCouponCategoryRepo;
    }

    /**
     * Display a listing of the oc_coupon_category.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCouponCategories = $this->ocCouponCategoryRepository->paginate(20);

        return view('oc_coupon_categories.index')
            ->with('ocCouponCategories', $ocCouponCategories);
    }

    /**
     * Show the form for creating a new oc_coupon_category.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_coupon_categories.create');
    }

    /**
     * Store a newly created oc_coupon_category in storage.
     *
     * @param Createoc_coupon_categoryRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_categoryRequest $request)
    {
        $input = $request->all();

        $ocCouponCategory = $this->ocCouponCategoryRepository->create($input);

        Flash::success('Oc Coupon Category saved successfully.');

        return redirect(route('ocCouponCategories.index'));
    }

    /**
     * Display the specified oc_coupon_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            Flash::error('Oc Coupon Category not found');

            return redirect(route('ocCouponCategories.index'));
        }

        return view('oc_coupon_categories.show')->with('ocCouponCategory', $ocCouponCategory);
    }

    /**
     * Show the form for editing the specified oc_coupon_category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            Flash::error('Oc Coupon Category not found');

            return redirect(route('ocCouponCategories.index'));
        }

        return view('oc_coupon_categories.edit')->with('ocCouponCategory', $ocCouponCategory);
    }

    /**
     * Update the specified oc_coupon_category in storage.
     *
     * @param  int              $id
     * @param Updateoc_coupon_categoryRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_categoryRequest $request)
    {
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            Flash::error('Oc Coupon Category not found');

            return redirect(route('ocCouponCategories.index'));
        }

        $ocCouponCategory = $this->ocCouponCategoryRepository->update($request->all(), $id);

        Flash::success('Oc Coupon Category updated successfully.');

        return redirect(route('ocCouponCategories.index'));
    }

    /**
     * Remove the specified oc_coupon_category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            Flash::error('Oc Coupon Category not found');

            return redirect(route('ocCouponCategories.index'));
        }

        $this->ocCouponCategoryRepository->delete($id);

        Flash::success('Oc Coupon Category deleted successfully.');

        return redirect(route('ocCouponCategories.index'));
    }
}
