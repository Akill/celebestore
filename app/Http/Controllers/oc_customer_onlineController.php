<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_onlineRequest;
use App\Http\Requests\Updateoc_customer_onlineRequest;
use App\Repositories\oc_customer_onlineRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_onlineController extends AppBaseController
{
    /** @var  oc_customer_onlineRepository */
    private $ocCustomerOnlineRepository;

    public function __construct(oc_customer_onlineRepository $ocCustomerOnlineRepo)
    {
        $this->ocCustomerOnlineRepository = $ocCustomerOnlineRepo;
    }

    /**
     * Display a listing of the oc_customer_online.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerOnlineRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerOnlines = $this->ocCustomerOnlineRepository->paginate(20);

        return view('oc_customer_onlines.index')
            ->with('ocCustomerOnlines', $ocCustomerOnlines);
    }

    /**
     * Show the form for creating a new oc_customer_online.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_onlines.create');
    }

    /**
     * Store a newly created oc_customer_online in storage.
     *
     * @param Createoc_customer_onlineRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_onlineRequest $request)
    {
        $input = $request->all();

        $ocCustomerOnline = $this->ocCustomerOnlineRepository->create($input);

        Flash::success('Oc Customer Online saved successfully.');

        return redirect(route('ocCustomerOnlines.index'));
    }

    /**
     * Display the specified oc_customer_online.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            Flash::error('Oc Customer Online not found');

            return redirect(route('ocCustomerOnlines.index'));
        }

        return view('oc_customer_onlines.show')->with('ocCustomerOnline', $ocCustomerOnline);
    }

    /**
     * Show the form for editing the specified oc_customer_online.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            Flash::error('Oc Customer Online not found');

            return redirect(route('ocCustomerOnlines.index'));
        }

        return view('oc_customer_onlines.edit')->with('ocCustomerOnline', $ocCustomerOnline);
    }

    /**
     * Update the specified oc_customer_online in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_onlineRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_onlineRequest $request)
    {
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            Flash::error('Oc Customer Online not found');

            return redirect(route('ocCustomerOnlines.index'));
        }

        $ocCustomerOnline = $this->ocCustomerOnlineRepository->update($request->all(), $id);

        Flash::success('Oc Customer Online updated successfully.');

        return redirect(route('ocCustomerOnlines.index'));
    }

    /**
     * Remove the specified oc_customer_online from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            Flash::error('Oc Customer Online not found');

            return redirect(route('ocCustomerOnlines.index'));
        }

        $this->ocCustomerOnlineRepository->delete($id);

        Flash::success('Oc Customer Online deleted successfully.');

        return redirect(route('ocCustomerOnlines.index'));
    }
}
