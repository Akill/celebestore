<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_loginRequest;
use App\Http\Requests\Updateoc_customer_loginRequest;
use App\Repositories\oc_customer_loginRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_loginController extends AppBaseController
{
    /** @var  oc_customer_loginRepository */
    private $ocCustomerLoginRepository;

    public function __construct(oc_customer_loginRepository $ocCustomerLoginRepo)
    {
        $this->ocCustomerLoginRepository = $ocCustomerLoginRepo;
    }

    /**
     * Display a listing of the oc_customer_login.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerLoginRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerLogins = $this->ocCustomerLoginRepository->paginate(20);

        return view('oc_customer_logins.index')
            ->with('ocCustomerLogins', $ocCustomerLogins);
    }

    /**
     * Show the form for creating a new oc_customer_login.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_logins.create');
    }

    /**
     * Store a newly created oc_customer_login in storage.
     *
     * @param Createoc_customer_loginRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_loginRequest $request)
    {
        $input = $request->all();

        $ocCustomerLogin = $this->ocCustomerLoginRepository->create($input);

        Flash::success('Oc Customer Login saved successfully.');

        return redirect(route('ocCustomerLogins.index'));
    }

    /**
     * Display the specified oc_customer_login.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            Flash::error('Oc Customer Login not found');

            return redirect(route('ocCustomerLogins.index'));
        }

        return view('oc_customer_logins.show')->with('ocCustomerLogin', $ocCustomerLogin);
    }

    /**
     * Show the form for editing the specified oc_customer_login.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            Flash::error('Oc Customer Login not found');

            return redirect(route('ocCustomerLogins.index'));
        }

        return view('oc_customer_logins.edit')->with('ocCustomerLogin', $ocCustomerLogin);
    }

    /**
     * Update the specified oc_customer_login in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_loginRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_loginRequest $request)
    {
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            Flash::error('Oc Customer Login not found');

            return redirect(route('ocCustomerLogins.index'));
        }

        $ocCustomerLogin = $this->ocCustomerLoginRepository->update($request->all(), $id);

        Flash::success('Oc Customer Login updated successfully.');

        return redirect(route('ocCustomerLogins.index'));
    }

    /**
     * Remove the specified oc_customer_login from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            Flash::error('Oc Customer Login not found');

            return redirect(route('ocCustomerLogins.index'));
        }

        $this->ocCustomerLoginRepository->delete($id);

        Flash::success('Oc Customer Login deleted successfully.');

        return redirect(route('ocCustomerLogins.index'));
    }
}
