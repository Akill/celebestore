<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_totalRequest;
use App\Http\Requests\Updateoc_order_totalRequest;
use App\Repositories\oc_order_totalRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_totalController extends AppBaseController
{
    /** @var  oc_order_totalRepository */
    private $ocOrderTotalRepository;

    public function __construct(oc_order_totalRepository $ocOrderTotalRepo)
    {
        $this->ocOrderTotalRepository = $ocOrderTotalRepo;
    }

    /**
     * Display a listing of the oc_order_total.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderTotalRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderTotals = $this->ocOrderTotalRepository->paginate(20);

        return view('oc_order_totals.index')
            ->with('ocOrderTotals', $ocOrderTotals);
    }

    /**
     * Show the form for creating a new oc_order_total.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_totals.create');
    }

    /**
     * Store a newly created oc_order_total in storage.
     *
     * @param Createoc_order_totalRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_totalRequest $request)
    {
        $input = $request->all();

        $ocOrderTotal = $this->ocOrderTotalRepository->create($input);

        Flash::success('Oc Order Total saved successfully.');

        return redirect(route('ocOrderTotals.index'));
    }

    /**
     * Display the specified oc_order_total.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            Flash::error('Oc Order Total not found');

            return redirect(route('ocOrderTotals.index'));
        }

        return view('oc_order_totals.show')->with('ocOrderTotal', $ocOrderTotal);
    }

    /**
     * Show the form for editing the specified oc_order_total.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            Flash::error('Oc Order Total not found');

            return redirect(route('ocOrderTotals.index'));
        }

        return view('oc_order_totals.edit')->with('ocOrderTotal', $ocOrderTotal);
    }

    /**
     * Update the specified oc_order_total in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_totalRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_totalRequest $request)
    {
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            Flash::error('Oc Order Total not found');

            return redirect(route('ocOrderTotals.index'));
        }

        $ocOrderTotal = $this->ocOrderTotalRepository->update($request->all(), $id);

        Flash::success('Oc Order Total updated successfully.');

        return redirect(route('ocOrderTotals.index'));
    }

    /**
     * Remove the specified oc_order_total from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            Flash::error('Oc Order Total not found');

            return redirect(route('ocOrderTotals.index'));
        }

        $this->ocOrderTotalRepository->delete($id);

        Flash::success('Oc Order Total deleted successfully.');

        return redirect(route('ocOrderTotals.index'));
    }
}
