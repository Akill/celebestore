<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_download_descriptionRequest;
use App\Http\Requests\Updateoc_download_descriptionRequest;
use App\Repositories\oc_download_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_download_descriptionController extends AppBaseController
{
    /** @var  oc_download_descriptionRepository */
    private $ocDownloadDescriptionRepository;

    public function __construct(oc_download_descriptionRepository $ocDownloadDescriptionRepo)
    {
        $this->ocDownloadDescriptionRepository = $ocDownloadDescriptionRepo;
    }

    /**
     * Display a listing of the oc_download_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocDownloadDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocDownloadDescriptions = $this->ocDownloadDescriptionRepository->paginate(20);

        return view('oc_download_descriptions.index')
            ->with('ocDownloadDescriptions', $ocDownloadDescriptions);
    }

    /**
     * Show the form for creating a new oc_download_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_download_descriptions.create');
    }

    /**
     * Store a newly created oc_download_description in storage.
     *
     * @param Createoc_download_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_download_descriptionRequest $request)
    {
        $input = $request->all();

        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->create($input);

        Flash::success('Oc Download Description saved successfully.');

        return redirect(route('ocDownloadDescriptions.index'));
    }

    /**
     * Display the specified oc_download_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            Flash::error('Oc Download Description not found');

            return redirect(route('ocDownloadDescriptions.index'));
        }

        return view('oc_download_descriptions.show')->with('ocDownloadDescription', $ocDownloadDescription);
    }

    /**
     * Show the form for editing the specified oc_download_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            Flash::error('Oc Download Description not found');

            return redirect(route('ocDownloadDescriptions.index'));
        }

        return view('oc_download_descriptions.edit')->with('ocDownloadDescription', $ocDownloadDescription);
    }

    /**
     * Update the specified oc_download_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_download_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_download_descriptionRequest $request)
    {
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            Flash::error('Oc Download Description not found');

            return redirect(route('ocDownloadDescriptions.index'));
        }

        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Download Description updated successfully.');

        return redirect(route('ocDownloadDescriptions.index'));
    }

    /**
     * Remove the specified oc_download_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            Flash::error('Oc Download Description not found');

            return redirect(route('ocDownloadDescriptions.index'));
        }

        $this->ocDownloadDescriptionRepository->delete($id);

        Flash::success('Oc Download Description deleted successfully.');

        return redirect(route('ocDownloadDescriptions.index'));
    }
}
