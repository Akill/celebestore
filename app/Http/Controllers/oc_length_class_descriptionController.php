<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_length_class_descriptionRequest;
use App\Http\Requests\Updateoc_length_class_descriptionRequest;
use App\Repositories\oc_length_class_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_length_class_descriptionController extends AppBaseController
{
    /** @var  oc_length_class_descriptionRepository */
    private $ocLengthClassDescriptionRepository;

    public function __construct(oc_length_class_descriptionRepository $ocLengthClassDescriptionRepo)
    {
        $this->ocLengthClassDescriptionRepository = $ocLengthClassDescriptionRepo;
    }

    /**
     * Display a listing of the oc_length_class_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLengthClassDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocLengthClassDescriptions = $this->ocLengthClassDescriptionRepository->paginate(20);

        return view('oc_length_class_descriptions.index')
            ->with('ocLengthClassDescriptions', $ocLengthClassDescriptions);
    }

    /**
     * Show the form for creating a new oc_length_class_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_length_class_descriptions.create');
    }

    /**
     * Store a newly created oc_length_class_description in storage.
     *
     * @param Createoc_length_class_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_length_class_descriptionRequest $request)
    {
        $input = $request->all();

        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->create($input);

        Flash::success('Oc Length Class Description saved successfully.');

        return redirect(route('ocLengthClassDescriptions.index'));
    }

    /**
     * Display the specified oc_length_class_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            Flash::error('Oc Length Class Description not found');

            return redirect(route('ocLengthClassDescriptions.index'));
        }

        return view('oc_length_class_descriptions.show')->with('ocLengthClassDescription', $ocLengthClassDescription);
    }

    /**
     * Show the form for editing the specified oc_length_class_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            Flash::error('Oc Length Class Description not found');

            return redirect(route('ocLengthClassDescriptions.index'));
        }

        return view('oc_length_class_descriptions.edit')->with('ocLengthClassDescription', $ocLengthClassDescription);
    }

    /**
     * Update the specified oc_length_class_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_length_class_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_length_class_descriptionRequest $request)
    {
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            Flash::error('Oc Length Class Description not found');

            return redirect(route('ocLengthClassDescriptions.index'));
        }

        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Length Class Description updated successfully.');

        return redirect(route('ocLengthClassDescriptions.index'));
    }

    /**
     * Remove the specified oc_length_class_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            Flash::error('Oc Length Class Description not found');

            return redirect(route('ocLengthClassDescriptions.index'));
        }

        $this->ocLengthClassDescriptionRepository->delete($id);

        Flash::success('Oc Length Class Description deleted successfully.');

        return redirect(route('ocLengthClassDescriptions.index'));
    }
}
