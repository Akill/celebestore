<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_modificationRequest;
use App\Http\Requests\Updateoc_modificationRequest;
use App\Repositories\oc_modificationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_modificationController extends AppBaseController
{
    /** @var  oc_modificationRepository */
    private $ocModificationRepository;

    public function __construct(oc_modificationRepository $ocModificationRepo)
    {
        $this->ocModificationRepository = $ocModificationRepo;
    }

    /**
     * Display a listing of the oc_modification.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocModificationRepository->pushCriteria(new RequestCriteria($request));
        $ocModifications = $this->ocModificationRepository->paginate(20);

        return view('oc_modifications.index')
            ->with('ocModifications', $ocModifications);
    }

    /**
     * Show the form for creating a new oc_modification.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_modifications.create');
    }

    /**
     * Store a newly created oc_modification in storage.
     *
     * @param Createoc_modificationRequest $request
     *
     * @return Response
     */
    public function store(Createoc_modificationRequest $request)
    {
        $input = $request->all();

        $ocModification = $this->ocModificationRepository->create($input);

        Flash::success('Oc Modification saved successfully.');

        return redirect(route('ocModifications.index'));
    }

    /**
     * Display the specified oc_modification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            Flash::error('Oc Modification not found');

            return redirect(route('ocModifications.index'));
        }

        return view('oc_modifications.show')->with('ocModification', $ocModification);
    }

    /**
     * Show the form for editing the specified oc_modification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            Flash::error('Oc Modification not found');

            return redirect(route('ocModifications.index'));
        }

        return view('oc_modifications.edit')->with('ocModification', $ocModification);
    }

    /**
     * Update the specified oc_modification in storage.
     *
     * @param  int              $id
     * @param Updateoc_modificationRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_modificationRequest $request)
    {
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            Flash::error('Oc Modification not found');

            return redirect(route('ocModifications.index'));
        }

        $ocModification = $this->ocModificationRepository->update($request->all(), $id);

        Flash::success('Oc Modification updated successfully.');

        return redirect(route('ocModifications.index'));
    }

    /**
     * Remove the specified oc_modification from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            Flash::error('Oc Modification not found');

            return redirect(route('ocModifications.index'));
        }

        $this->ocModificationRepository->delete($id);

        Flash::success('Oc Modification deleted successfully.');

        return redirect(route('ocModifications.index'));
    }
}
