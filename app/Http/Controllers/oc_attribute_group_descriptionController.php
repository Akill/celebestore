<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_attribute_group_descriptionRequest;
use App\Http\Requests\Updateoc_attribute_group_descriptionRequest;
use App\Repositories\oc_attribute_group_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_attribute_group_descriptionController extends AppBaseController
{
    /** @var  oc_attribute_group_descriptionRepository */
    private $ocAttributeGroupDescriptionRepository;

    public function __construct(oc_attribute_group_descriptionRepository $ocAttributeGroupDescriptionRepo)
    {
        $this->ocAttributeGroupDescriptionRepository = $ocAttributeGroupDescriptionRepo;
    }

    /**
     * Display a listing of the oc_attribute_group_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeGroupDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocAttributeGroupDescriptions = $this->ocAttributeGroupDescriptionRepository->paginate(20);

        return view('oc_attribute_group_descriptions.index')
            ->with('ocAttributeGroupDescriptions', $ocAttributeGroupDescriptions);
    }

    /**
     * Show the form for creating a new oc_attribute_group_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_attribute_group_descriptions.create');
    }

    /**
     * Store a newly created oc_attribute_group_description in storage.
     *
     * @param Createoc_attribute_group_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_group_descriptionRequest $request)
    {
        $input = $request->all();

        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->create($input);

        Flash::success('Oc Attribute Group Description saved successfully.');

        return redirect(route('ocAttributeGroupDescriptions.index'));
    }

    /**
     * Display the specified oc_attribute_group_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            Flash::error('Oc Attribute Group Description not found');

            return redirect(route('ocAttributeGroupDescriptions.index'));
        }

        return view('oc_attribute_group_descriptions.show')->with('ocAttributeGroupDescription', $ocAttributeGroupDescription);
    }

    /**
     * Show the form for editing the specified oc_attribute_group_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            Flash::error('Oc Attribute Group Description not found');

            return redirect(route('ocAttributeGroupDescriptions.index'));
        }

        return view('oc_attribute_group_descriptions.edit')->with('ocAttributeGroupDescription', $ocAttributeGroupDescription);
    }

    /**
     * Update the specified oc_attribute_group_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_attribute_group_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_group_descriptionRequest $request)
    {
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            Flash::error('Oc Attribute Group Description not found');

            return redirect(route('ocAttributeGroupDescriptions.index'));
        }

        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Attribute Group Description updated successfully.');

        return redirect(route('ocAttributeGroupDescriptions.index'));
    }

    /**
     * Remove the specified oc_attribute_group_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            Flash::error('Oc Attribute Group Description not found');

            return redirect(route('ocAttributeGroupDescriptions.index'));
        }

        $this->ocAttributeGroupDescriptionRepository->delete($id);

        Flash::success('Oc Attribute Group Description deleted successfully.');

        return redirect(route('ocAttributeGroupDescriptions.index'));
    }
}
