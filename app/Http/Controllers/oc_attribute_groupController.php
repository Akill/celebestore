<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_attribute_groupRequest;
use App\Http\Requests\Updateoc_attribute_groupRequest;
use App\Repositories\oc_attribute_groupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_attribute_groupController extends AppBaseController
{
    /** @var  oc_attribute_groupRepository */
    private $ocAttributeGroupRepository;

    public function __construct(oc_attribute_groupRepository $ocAttributeGroupRepo)
    {
        $this->ocAttributeGroupRepository = $ocAttributeGroupRepo;
    }

    /**
     * Display a listing of the oc_attribute_group.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeGroupRepository->pushCriteria(new RequestCriteria($request));
        $ocAttributeGroups = $this->ocAttributeGroupRepository->paginate(20);

        return view('oc_attribute_groups.index')
            ->with('ocAttributeGroups', $ocAttributeGroups);
    }

    /**
     * Show the form for creating a new oc_attribute_group.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_attribute_groups.create');
    }

    /**
     * Store a newly created oc_attribute_group in storage.
     *
     * @param Createoc_attribute_groupRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_groupRequest $request)
    {
        $input = $request->all();

        $ocAttributeGroup = $this->ocAttributeGroupRepository->create($input);

        Flash::success('Oc Attribute Group saved successfully.');

        return redirect(route('ocAttributeGroups.index'));
    }

    /**
     * Display the specified oc_attribute_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            Flash::error('Oc Attribute Group not found');

            return redirect(route('ocAttributeGroups.index'));
        }

        return view('oc_attribute_groups.show')->with('ocAttributeGroup', $ocAttributeGroup);
    }

    /**
     * Show the form for editing the specified oc_attribute_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            Flash::error('Oc Attribute Group not found');

            return redirect(route('ocAttributeGroups.index'));
        }

        return view('oc_attribute_groups.edit')->with('ocAttributeGroup', $ocAttributeGroup);
    }

    /**
     * Update the specified oc_attribute_group in storage.
     *
     * @param  int              $id
     * @param Updateoc_attribute_groupRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_groupRequest $request)
    {
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            Flash::error('Oc Attribute Group not found');

            return redirect(route('ocAttributeGroups.index'));
        }

        $ocAttributeGroup = $this->ocAttributeGroupRepository->update($request->all(), $id);

        Flash::success('Oc Attribute Group updated successfully.');

        return redirect(route('ocAttributeGroups.index'));
    }

    /**
     * Remove the specified oc_attribute_group from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            Flash::error('Oc Attribute Group not found');

            return redirect(route('ocAttributeGroups.index'));
        }

        $this->ocAttributeGroupRepository->delete($id);

        Flash::success('Oc Attribute Group deleted successfully.');

        return redirect(route('ocAttributeGroups.index'));
    }
}
