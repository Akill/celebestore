<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_uploadRequest;
use App\Http\Requests\Updateoc_uploadRequest;
use App\Repositories\oc_uploadRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_uploadController extends AppBaseController
{
    /** @var  oc_uploadRepository */
    private $ocUploadRepository;

    public function __construct(oc_uploadRepository $ocUploadRepo)
    {
        $this->ocUploadRepository = $ocUploadRepo;
    }

    /**
     * Display a listing of the oc_upload.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUploadRepository->pushCriteria(new RequestCriteria($request));
        $ocUploads = $this->ocUploadRepository->paginate(20);

        return view('oc_uploads.index')
            ->with('ocUploads', $ocUploads);
    }

    /**
     * Show the form for creating a new oc_upload.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_uploads.create');
    }

    /**
     * Store a newly created oc_upload in storage.
     *
     * @param Createoc_uploadRequest $request
     *
     * @return Response
     */
    public function store(Createoc_uploadRequest $request)
    {
        $input = $request->all();

        $ocUpload = $this->ocUploadRepository->create($input);

        Flash::success('Oc Upload saved successfully.');

        return redirect(route('ocUploads.index'));
    }

    /**
     * Display the specified oc_upload.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            Flash::error('Oc Upload not found');

            return redirect(route('ocUploads.index'));
        }

        return view('oc_uploads.show')->with('ocUpload', $ocUpload);
    }

    /**
     * Show the form for editing the specified oc_upload.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            Flash::error('Oc Upload not found');

            return redirect(route('ocUploads.index'));
        }

        return view('oc_uploads.edit')->with('ocUpload', $ocUpload);
    }

    /**
     * Update the specified oc_upload in storage.
     *
     * @param  int              $id
     * @param Updateoc_uploadRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_uploadRequest $request)
    {
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            Flash::error('Oc Upload not found');

            return redirect(route('ocUploads.index'));
        }

        $ocUpload = $this->ocUploadRepository->update($request->all(), $id);

        Flash::success('Oc Upload updated successfully.');

        return redirect(route('ocUploads.index'));
    }

    /**
     * Remove the specified oc_upload from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            Flash::error('Oc Upload not found');

            return redirect(route('ocUploads.index'));
        }

        $this->ocUploadRepository->delete($id);

        Flash::success('Oc Upload deleted successfully.');

        return redirect(route('ocUploads.index'));
    }
}
