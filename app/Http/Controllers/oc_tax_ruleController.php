<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_tax_ruleRequest;
use App\Http\Requests\Updateoc_tax_ruleRequest;
use App\Repositories\oc_tax_ruleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_tax_ruleController extends AppBaseController
{
    /** @var  oc_tax_ruleRepository */
    private $ocTaxRuleRepository;

    public function __construct(oc_tax_ruleRepository $ocTaxRuleRepo)
    {
        $this->ocTaxRuleRepository = $ocTaxRuleRepo;
    }

    /**
     * Display a listing of the oc_tax_rule.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxRuleRepository->pushCriteria(new RequestCriteria($request));
        $ocTaxRules = $this->ocTaxRuleRepository->paginate(20);

        return view('oc_tax_rules.index')
            ->with('ocTaxRules', $ocTaxRules);
    }

    /**
     * Show the form for creating a new oc_tax_rule.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_tax_rules.create');
    }

    /**
     * Store a newly created oc_tax_rule in storage.
     *
     * @param Createoc_tax_ruleRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_ruleRequest $request)
    {
        $input = $request->all();

        $ocTaxRule = $this->ocTaxRuleRepository->create($input);

        Flash::success('Oc Tax Rule saved successfully.');

        return redirect(route('ocTaxRules.index'));
    }

    /**
     * Display the specified oc_tax_rule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            Flash::error('Oc Tax Rule not found');

            return redirect(route('ocTaxRules.index'));
        }

        return view('oc_tax_rules.show')->with('ocTaxRule', $ocTaxRule);
    }

    /**
     * Show the form for editing the specified oc_tax_rule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            Flash::error('Oc Tax Rule not found');

            return redirect(route('ocTaxRules.index'));
        }

        return view('oc_tax_rules.edit')->with('ocTaxRule', $ocTaxRule);
    }

    /**
     * Update the specified oc_tax_rule in storage.
     *
     * @param  int              $id
     * @param Updateoc_tax_ruleRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_ruleRequest $request)
    {
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            Flash::error('Oc Tax Rule not found');

            return redirect(route('ocTaxRules.index'));
        }

        $ocTaxRule = $this->ocTaxRuleRepository->update($request->all(), $id);

        Flash::success('Oc Tax Rule updated successfully.');

        return redirect(route('ocTaxRules.index'));
    }

    /**
     * Remove the specified oc_tax_rule from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            Flash::error('Oc Tax Rule not found');

            return redirect(route('ocTaxRules.index'));
        }

        $this->ocTaxRuleRepository->delete($id);

        Flash::success('Oc Tax Rule deleted successfully.');

        return redirect(route('ocTaxRules.index'));
    }
}
