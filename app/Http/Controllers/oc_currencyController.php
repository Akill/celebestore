<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_currencyRequest;
use App\Http\Requests\Updateoc_currencyRequest;
use App\Repositories\oc_currencyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_currencyController extends AppBaseController
{
    /** @var  oc_currencyRepository */
    private $ocCurrencyRepository;

    public function __construct(oc_currencyRepository $ocCurrencyRepo)
    {
        $this->ocCurrencyRepository = $ocCurrencyRepo;
    }

    /**
     * Display a listing of the oc_currency.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCurrencyRepository->pushCriteria(new RequestCriteria($request));
        $ocCurrencies = $this->ocCurrencyRepository->paginate(20);

        return view('oc_currencies.index')
            ->with('ocCurrencies', $ocCurrencies);
    }

    /**
     * Show the form for creating a new oc_currency.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_currencies.create');
    }

    /**
     * Store a newly created oc_currency in storage.
     *
     * @param Createoc_currencyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_currencyRequest $request)
    {
        $input = $request->all();

        $ocCurrency = $this->ocCurrencyRepository->create($input);

        Flash::success('Oc Currency saved successfully.');

        return redirect(route('ocCurrencies.index'));
    }

    /**
     * Display the specified oc_currency.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            Flash::error('Oc Currency not found');

            return redirect(route('ocCurrencies.index'));
        }

        return view('oc_currencies.show')->with('ocCurrency', $ocCurrency);
    }

    /**
     * Show the form for editing the specified oc_currency.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            Flash::error('Oc Currency not found');

            return redirect(route('ocCurrencies.index'));
        }

        return view('oc_currencies.edit')->with('ocCurrency', $ocCurrency);
    }

    /**
     * Update the specified oc_currency in storage.
     *
     * @param  int              $id
     * @param Updateoc_currencyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_currencyRequest $request)
    {
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            Flash::error('Oc Currency not found');

            return redirect(route('ocCurrencies.index'));
        }

        $ocCurrency = $this->ocCurrencyRepository->update($request->all(), $id);

        Flash::success('Oc Currency updated successfully.');

        return redirect(route('ocCurrencies.index'));
    }

    /**
     * Remove the specified oc_currency from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            Flash::error('Oc Currency not found');

            return redirect(route('ocCurrencies.index'));
        }

        $this->ocCurrencyRepository->delete($id);

        Flash::success('Oc Currency deleted successfully.');

        return redirect(route('ocCurrencies.index'));
    }
}
