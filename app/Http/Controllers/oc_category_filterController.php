<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_category_filterRequest;
use App\Http\Requests\Updateoc_category_filterRequest;
use App\Repositories\oc_category_filterRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_category_filterController extends AppBaseController
{
    /** @var  oc_category_filterRepository */
    private $ocCategoryFilterRepository;

    public function __construct(oc_category_filterRepository $ocCategoryFilterRepo)
    {
        $this->ocCategoryFilterRepository = $ocCategoryFilterRepo;
    }

    /**
     * Display a listing of the oc_category_filter.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryFilterRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryFilters = $this->ocCategoryFilterRepository->paginate(20);

        return view('oc_category_filters.index')
            ->with('ocCategoryFilters', $ocCategoryFilters);
    }

    /**
     * Show the form for creating a new oc_category_filter.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_category_filters.create');
    }

    /**
     * Store a newly created oc_category_filter in storage.
     *
     * @param Createoc_category_filterRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_filterRequest $request)
    {
        $input = $request->all();

        $ocCategoryFilter = $this->ocCategoryFilterRepository->create($input);

        Flash::success('Oc Category Filter saved successfully.');

        return redirect(route('ocCategoryFilters.index'));
    }

    /**
     * Display the specified oc_category_filter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            Flash::error('Oc Category Filter not found');

            return redirect(route('ocCategoryFilters.index'));
        }

        return view('oc_category_filters.show')->with('ocCategoryFilter', $ocCategoryFilter);
    }

    /**
     * Show the form for editing the specified oc_category_filter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            Flash::error('Oc Category Filter not found');

            return redirect(route('ocCategoryFilters.index'));
        }

        return view('oc_category_filters.edit')->with('ocCategoryFilter', $ocCategoryFilter);
    }

    /**
     * Update the specified oc_category_filter in storage.
     *
     * @param  int              $id
     * @param Updateoc_category_filterRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_filterRequest $request)
    {
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            Flash::error('Oc Category Filter not found');

            return redirect(route('ocCategoryFilters.index'));
        }

        $ocCategoryFilter = $this->ocCategoryFilterRepository->update($request->all(), $id);

        Flash::success('Oc Category Filter updated successfully.');

        return redirect(route('ocCategoryFilters.index'));
    }

    /**
     * Remove the specified oc_category_filter from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            Flash::error('Oc Category Filter not found');

            return redirect(route('ocCategoryFilters.index'));
        }

        $this->ocCategoryFilterRepository->delete($id);

        Flash::success('Oc Category Filter deleted successfully.');

        return redirect(route('ocCategoryFilters.index'));
    }
}
