<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_custom_field_valueRequest;
use App\Http\Requests\Updateoc_custom_field_valueRequest;
use App\Repositories\oc_custom_field_valueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_custom_field_valueController extends AppBaseController
{
    /** @var  oc_custom_field_valueRepository */
    private $ocCustomFieldValueRepository;

    public function __construct(oc_custom_field_valueRepository $ocCustomFieldValueRepo)
    {
        $this->ocCustomFieldValueRepository = $ocCustomFieldValueRepo;
    }

    /**
     * Display a listing of the oc_custom_field_value.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldValueRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomFieldValues = $this->ocCustomFieldValueRepository->paginate(20);

        return view('oc_custom_field_values.index')
            ->with('ocCustomFieldValues', $ocCustomFieldValues);
    }

    /**
     * Show the form for creating a new oc_custom_field_value.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_custom_field_values.create');
    }

    /**
     * Store a newly created oc_custom_field_value in storage.
     *
     * @param Createoc_custom_field_valueRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_valueRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->create($input);

        Flash::success('Oc Custom Field Value saved successfully.');

        return redirect(route('ocCustomFieldValues.index'));
    }

    /**
     * Display the specified oc_custom_field_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            Flash::error('Oc Custom Field Value not found');

            return redirect(route('ocCustomFieldValues.index'));
        }

        return view('oc_custom_field_values.show')->with('ocCustomFieldValue', $ocCustomFieldValue);
    }

    /**
     * Show the form for editing the specified oc_custom_field_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            Flash::error('Oc Custom Field Value not found');

            return redirect(route('ocCustomFieldValues.index'));
        }

        return view('oc_custom_field_values.edit')->with('ocCustomFieldValue', $ocCustomFieldValue);
    }

    /**
     * Update the specified oc_custom_field_value in storage.
     *
     * @param  int              $id
     * @param Updateoc_custom_field_valueRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_valueRequest $request)
    {
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            Flash::error('Oc Custom Field Value not found');

            return redirect(route('ocCustomFieldValues.index'));
        }

        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->update($request->all(), $id);

        Flash::success('Oc Custom Field Value updated successfully.');

        return redirect(route('ocCustomFieldValues.index'));
    }

    /**
     * Remove the specified oc_custom_field_value from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            Flash::error('Oc Custom Field Value not found');

            return redirect(route('ocCustomFieldValues.index'));
        }

        $this->ocCustomFieldValueRepository->delete($id);

        Flash::success('Oc Custom Field Value deleted successfully.');

        return redirect(route('ocCustomFieldValues.index'));
    }
}
