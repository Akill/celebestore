<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_tax_rateRequest;
use App\Http\Requests\Updateoc_tax_rateRequest;
use App\Repositories\oc_tax_rateRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_tax_rateController extends AppBaseController
{
    /** @var  oc_tax_rateRepository */
    private $ocTaxRateRepository;

    public function __construct(oc_tax_rateRepository $ocTaxRateRepo)
    {
        $this->ocTaxRateRepository = $ocTaxRateRepo;
    }

    /**
     * Display a listing of the oc_tax_rate.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxRateRepository->pushCriteria(new RequestCriteria($request));
        $ocTaxRates = $this->ocTaxRateRepository->paginate(20);

        return view('oc_tax_rates.index')
            ->with('ocTaxRates', $ocTaxRates);
    }

    /**
     * Show the form for creating a new oc_tax_rate.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_tax_rates.create');
    }

    /**
     * Store a newly created oc_tax_rate in storage.
     *
     * @param Createoc_tax_rateRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_rateRequest $request)
    {
        $input = $request->all();

        $ocTaxRate = $this->ocTaxRateRepository->create($input);

        Flash::success('Oc Tax Rate saved successfully.');

        return redirect(route('ocTaxRates.index'));
    }

    /**
     * Display the specified oc_tax_rate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            Flash::error('Oc Tax Rate not found');

            return redirect(route('ocTaxRates.index'));
        }

        return view('oc_tax_rates.show')->with('ocTaxRate', $ocTaxRate);
    }

    /**
     * Show the form for editing the specified oc_tax_rate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            Flash::error('Oc Tax Rate not found');

            return redirect(route('ocTaxRates.index'));
        }

        return view('oc_tax_rates.edit')->with('ocTaxRate', $ocTaxRate);
    }

    /**
     * Update the specified oc_tax_rate in storage.
     *
     * @param  int              $id
     * @param Updateoc_tax_rateRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_rateRequest $request)
    {
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            Flash::error('Oc Tax Rate not found');

            return redirect(route('ocTaxRates.index'));
        }

        $ocTaxRate = $this->ocTaxRateRepository->update($request->all(), $id);

        Flash::success('Oc Tax Rate updated successfully.');

        return redirect(route('ocTaxRates.index'));
    }

    /**
     * Remove the specified oc_tax_rate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            Flash::error('Oc Tax Rate not found');

            return redirect(route('ocTaxRates.index'));
        }

        $this->ocTaxRateRepository->delete($id);

        Flash::success('Oc Tax Rate deleted successfully.');

        return redirect(route('ocTaxRates.index'));
    }
}
