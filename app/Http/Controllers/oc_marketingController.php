<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_marketingRequest;
use App\Http\Requests\Updateoc_marketingRequest;
use App\Repositories\oc_marketingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_marketingController extends AppBaseController
{
    /** @var  oc_marketingRepository */
    private $ocMarketingRepository;

    public function __construct(oc_marketingRepository $ocMarketingRepo)
    {
        $this->ocMarketingRepository = $ocMarketingRepo;
    }

    /**
     * Display a listing of the oc_marketing.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocMarketingRepository->pushCriteria(new RequestCriteria($request));
        $ocMarketings = $this->ocMarketingRepository->paginate(20);

        return view('oc_marketings.index')
            ->with('ocMarketings', $ocMarketings);
    }

    /**
     * Show the form for creating a new oc_marketing.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_marketings.create');
    }

    /**
     * Store a newly created oc_marketing in storage.
     *
     * @param Createoc_marketingRequest $request
     *
     * @return Response
     */
    public function store(Createoc_marketingRequest $request)
    {
        $input = $request->all();

        $ocMarketing = $this->ocMarketingRepository->create($input);

        Flash::success('Oc Marketing saved successfully.');

        return redirect(route('ocMarketings.index'));
    }

    /**
     * Display the specified oc_marketing.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            Flash::error('Oc Marketing not found');

            return redirect(route('ocMarketings.index'));
        }

        return view('oc_marketings.show')->with('ocMarketing', $ocMarketing);
    }

    /**
     * Show the form for editing the specified oc_marketing.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            Flash::error('Oc Marketing not found');

            return redirect(route('ocMarketings.index'));
        }

        return view('oc_marketings.edit')->with('ocMarketing', $ocMarketing);
    }

    /**
     * Update the specified oc_marketing in storage.
     *
     * @param  int              $id
     * @param Updateoc_marketingRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_marketingRequest $request)
    {
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            Flash::error('Oc Marketing not found');

            return redirect(route('ocMarketings.index'));
        }

        $ocMarketing = $this->ocMarketingRepository->update($request->all(), $id);

        Flash::success('Oc Marketing updated successfully.');

        return redirect(route('ocMarketings.index'));
    }

    /**
     * Remove the specified oc_marketing from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            Flash::error('Oc Marketing not found');

            return redirect(route('ocMarketings.index'));
        }

        $this->ocMarketingRepository->delete($id);

        Flash::success('Oc Marketing deleted successfully.');

        return redirect(route('ocMarketings.index'));
    }
}
