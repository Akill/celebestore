<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_coupon_productRequest;
use App\Http\Requests\Updateoc_coupon_productRequest;
use App\Repositories\oc_coupon_productRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_coupon_productController extends AppBaseController
{
    /** @var  oc_coupon_productRepository */
    private $ocCouponProductRepository;

    public function __construct(oc_coupon_productRepository $ocCouponProductRepo)
    {
        $this->ocCouponProductRepository = $ocCouponProductRepo;
    }

    /**
     * Display a listing of the oc_coupon_product.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponProductRepository->pushCriteria(new RequestCriteria($request));
        $ocCouponProducts = $this->ocCouponProductRepository->paginate(20);

        return view('oc_coupon_products.index')
            ->with('ocCouponProducts', $ocCouponProducts);
    }

    /**
     * Show the form for creating a new oc_coupon_product.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_coupon_products.create');
    }

    /**
     * Store a newly created oc_coupon_product in storage.
     *
     * @param Createoc_coupon_productRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_productRequest $request)
    {
        $input = $request->all();

        $ocCouponProduct = $this->ocCouponProductRepository->create($input);

        Flash::success('Oc Coupon Product saved successfully.');

        return redirect(route('ocCouponProducts.index'));
    }

    /**
     * Display the specified oc_coupon_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            Flash::error('Oc Coupon Product not found');

            return redirect(route('ocCouponProducts.index'));
        }

        return view('oc_coupon_products.show')->with('ocCouponProduct', $ocCouponProduct);
    }

    /**
     * Show the form for editing the specified oc_coupon_product.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            Flash::error('Oc Coupon Product not found');

            return redirect(route('ocCouponProducts.index'));
        }

        return view('oc_coupon_products.edit')->with('ocCouponProduct', $ocCouponProduct);
    }

    /**
     * Update the specified oc_coupon_product in storage.
     *
     * @param  int              $id
     * @param Updateoc_coupon_productRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_productRequest $request)
    {
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            Flash::error('Oc Coupon Product not found');

            return redirect(route('ocCouponProducts.index'));
        }

        $ocCouponProduct = $this->ocCouponProductRepository->update($request->all(), $id);

        Flash::success('Oc Coupon Product updated successfully.');

        return redirect(route('ocCouponProducts.index'));
    }

    /**
     * Remove the specified oc_coupon_product from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            Flash::error('Oc Coupon Product not found');

            return redirect(route('ocCouponProducts.index'));
        }

        $this->ocCouponProductRepository->delete($id);

        Flash::success('Oc Coupon Product deleted successfully.');

        return redirect(route('ocCouponProducts.index'));
    }
}
