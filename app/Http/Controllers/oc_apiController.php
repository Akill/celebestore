<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_apiRequest;
use App\Http\Requests\Updateoc_apiRequest;
use App\Repositories\oc_apiRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_apiController extends AppBaseController
{
    /** @var  oc_apiRepository */
    private $ocApiRepository;

    public function __construct(oc_apiRepository $ocApiRepo)
    {
        $this->ocApiRepository = $ocApiRepo;
    }

    /**
     * Display a listing of the oc_api.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocApiRepository->pushCriteria(new RequestCriteria($request));
        $ocApis = $this->ocApiRepository->paginate(20);

        return view('oc_apis.index')
            ->with('ocApis', $ocApis);
    }

    /**
     * Show the form for creating a new oc_api.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_apis.create');
    }

    /**
     * Store a newly created oc_api in storage.
     *
     * @param Createoc_apiRequest $request
     *
     * @return Response
     */
    public function store(Createoc_apiRequest $request)
    {
        $input = $request->all();

        $ocApi = $this->ocApiRepository->create($input);

        Flash::success('Oc Api saved successfully.');

        return redirect(route('ocApis.index'));
    }

    /**
     * Display the specified oc_api.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            Flash::error('Oc Api not found');

            return redirect(route('ocApis.index'));
        }

        return view('oc_apis.show')->with('ocApi', $ocApi);
    }

    /**
     * Show the form for editing the specified oc_api.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            Flash::error('Oc Api not found');

            return redirect(route('ocApis.index'));
        }

        return view('oc_apis.edit')->with('ocApi', $ocApi);
    }

    /**
     * Update the specified oc_api in storage.
     *
     * @param  int              $id
     * @param Updateoc_apiRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_apiRequest $request)
    {
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            Flash::error('Oc Api not found');

            return redirect(route('ocApis.index'));
        }

        $ocApi = $this->ocApiRepository->update($request->all(), $id);

        Flash::success('Oc Api updated successfully.');

        return redirect(route('ocApis.index'));
    }

    /**
     * Remove the specified oc_api from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            Flash::error('Oc Api not found');

            return redirect(route('ocApis.index'));
        }

        $this->ocApiRepository->delete($id);

        Flash::success('Oc Api deleted successfully.');

        return redirect(route('ocApis.index'));
    }
}
