<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_activityRequest;
use App\Http\Requests\Updateoc_customer_activityRequest;
use App\Repositories\oc_customer_activityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_activityController extends AppBaseController
{
    /** @var  oc_customer_activityRepository */
    private $ocCustomerActivityRepository;

    public function __construct(oc_customer_activityRepository $ocCustomerActivityRepo)
    {
        $this->ocCustomerActivityRepository = $ocCustomerActivityRepo;
    }

    /**
     * Display a listing of the oc_customer_activity.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerActivityRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerActivities = $this->ocCustomerActivityRepository->paginate(20);

        return view('oc_customer_activities.index')
            ->with('ocCustomerActivities', $ocCustomerActivities);
    }

    /**
     * Show the form for creating a new oc_customer_activity.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_activities.create');
    }

    /**
     * Store a newly created oc_customer_activity in storage.
     *
     * @param Createoc_customer_activityRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_activityRequest $request)
    {
        $input = $request->all();

        $ocCustomerActivity = $this->ocCustomerActivityRepository->create($input);

        Flash::success('Oc Customer Activity saved successfully.');

        return redirect(route('ocCustomerActivities.index'));
    }

    /**
     * Display the specified oc_customer_activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            Flash::error('Oc Customer Activity not found');

            return redirect(route('ocCustomerActivities.index'));
        }

        return view('oc_customer_activities.show')->with('ocCustomerActivity', $ocCustomerActivity);
    }

    /**
     * Show the form for editing the specified oc_customer_activity.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            Flash::error('Oc Customer Activity not found');

            return redirect(route('ocCustomerActivities.index'));
        }

        return view('oc_customer_activities.edit')->with('ocCustomerActivity', $ocCustomerActivity);
    }

    /**
     * Update the specified oc_customer_activity in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_activityRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_activityRequest $request)
    {
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            Flash::error('Oc Customer Activity not found');

            return redirect(route('ocCustomerActivities.index'));
        }

        $ocCustomerActivity = $this->ocCustomerActivityRepository->update($request->all(), $id);

        Flash::success('Oc Customer Activity updated successfully.');

        return redirect(route('ocCustomerActivities.index'));
    }

    /**
     * Remove the specified oc_customer_activity from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            Flash::error('Oc Customer Activity not found');

            return redirect(route('ocCustomerActivities.index'));
        }

        $this->ocCustomerActivityRepository->delete($id);

        Flash::success('Oc Customer Activity deleted successfully.');

        return redirect(route('ocCustomerActivities.index'));
    }
}
