<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_recurringRequest;
use App\Http\Requests\Updateoc_order_recurringRequest;
use App\Repositories\oc_order_recurringRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_recurringController extends AppBaseController
{
    /** @var  oc_order_recurringRepository */
    private $ocOrderRecurringRepository;

    public function __construct(oc_order_recurringRepository $ocOrderRecurringRepo)
    {
        $this->ocOrderRecurringRepository = $ocOrderRecurringRepo;
    }

    /**
     * Display a listing of the oc_order_recurring.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRecurringRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderRecurrings = $this->ocOrderRecurringRepository->paginate(20);

        return view('oc_order_recurrings.index')
            ->with('ocOrderRecurrings', $ocOrderRecurrings);
    }

    /**
     * Show the form for creating a new oc_order_recurring.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_recurrings.create');
    }

    /**
     * Store a newly created oc_order_recurring in storage.
     *
     * @param Createoc_order_recurringRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_recurringRequest $request)
    {
        $input = $request->all();

        $ocOrderRecurring = $this->ocOrderRecurringRepository->create($input);

        Flash::success('Oc Order Recurring saved successfully.');

        return redirect(route('ocOrderRecurrings.index'));
    }

    /**
     * Display the specified oc_order_recurring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            Flash::error('Oc Order Recurring not found');

            return redirect(route('ocOrderRecurrings.index'));
        }

        return view('oc_order_recurrings.show')->with('ocOrderRecurring', $ocOrderRecurring);
    }

    /**
     * Show the form for editing the specified oc_order_recurring.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            Flash::error('Oc Order Recurring not found');

            return redirect(route('ocOrderRecurrings.index'));
        }

        return view('oc_order_recurrings.edit')->with('ocOrderRecurring', $ocOrderRecurring);
    }

    /**
     * Update the specified oc_order_recurring in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_recurringRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_recurringRequest $request)
    {
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            Flash::error('Oc Order Recurring not found');

            return redirect(route('ocOrderRecurrings.index'));
        }

        $ocOrderRecurring = $this->ocOrderRecurringRepository->update($request->all(), $id);

        Flash::success('Oc Order Recurring updated successfully.');

        return redirect(route('ocOrderRecurrings.index'));
    }

    /**
     * Remove the specified oc_order_recurring from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            Flash::error('Oc Order Recurring not found');

            return redirect(route('ocOrderRecurrings.index'));
        }

        $this->ocOrderRecurringRepository->delete($id);

        Flash::success('Oc Order Recurring deleted successfully.');

        return redirect(route('ocOrderRecurrings.index'));
    }
}
