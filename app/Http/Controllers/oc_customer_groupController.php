<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_groupRequest;
use App\Http\Requests\Updateoc_customer_groupRequest;
use App\Repositories\oc_customer_groupRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_groupController extends AppBaseController
{
    /** @var  oc_customer_groupRepository */
    private $ocCustomerGroupRepository;

    public function __construct(oc_customer_groupRepository $ocCustomerGroupRepo)
    {
        $this->ocCustomerGroupRepository = $ocCustomerGroupRepo;
    }

    /**
     * Display a listing of the oc_customer_group.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerGroupRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerGroups = $this->ocCustomerGroupRepository->paginate(20);

        return view('oc_customer_groups.index')
            ->with('ocCustomerGroups', $ocCustomerGroups);
    }

    /**
     * Show the form for creating a new oc_customer_group.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_groups.create');
    }

    /**
     * Store a newly created oc_customer_group in storage.
     *
     * @param Createoc_customer_groupRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_groupRequest $request)
    {
        $input = $request->all();

        $ocCustomerGroup = $this->ocCustomerGroupRepository->create($input);

        Flash::success('Oc Customer Group saved successfully.');

        return redirect(route('ocCustomerGroups.index'));
    }

    /**
     * Display the specified oc_customer_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            Flash::error('Oc Customer Group not found');

            return redirect(route('ocCustomerGroups.index'));
        }

        return view('oc_customer_groups.show')->with('ocCustomerGroup', $ocCustomerGroup);
    }

    /**
     * Show the form for editing the specified oc_customer_group.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            Flash::error('Oc Customer Group not found');

            return redirect(route('ocCustomerGroups.index'));
        }

        return view('oc_customer_groups.edit')->with('ocCustomerGroup', $ocCustomerGroup);
    }

    /**
     * Update the specified oc_customer_group in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_groupRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_groupRequest $request)
    {
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            Flash::error('Oc Customer Group not found');

            return redirect(route('ocCustomerGroups.index'));
        }

        $ocCustomerGroup = $this->ocCustomerGroupRepository->update($request->all(), $id);

        Flash::success('Oc Customer Group updated successfully.');

        return redirect(route('ocCustomerGroups.index'));
    }

    /**
     * Remove the specified oc_customer_group from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            Flash::error('Oc Customer Group not found');

            return redirect(route('ocCustomerGroups.index'));
        }

        $this->ocCustomerGroupRepository->delete($id);

        Flash::success('Oc Customer Group deleted successfully.');

        return redirect(route('ocCustomerGroups.index'));
    }
}
