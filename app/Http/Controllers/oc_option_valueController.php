<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_option_valueRequest;
use App\Http\Requests\Updateoc_option_valueRequest;
use App\Repositories\oc_option_valueRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_option_valueController extends AppBaseController
{
    /** @var  oc_option_valueRepository */
    private $ocOptionValueRepository;

    public function __construct(oc_option_valueRepository $ocOptionValueRepo)
    {
        $this->ocOptionValueRepository = $ocOptionValueRepo;
    }

    /**
     * Display a listing of the oc_option_value.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionValueRepository->pushCriteria(new RequestCriteria($request));
        $ocOptionValues = $this->ocOptionValueRepository->paginate(20);

        return view('oc_option_values.index')
            ->with('ocOptionValues', $ocOptionValues);
    }

    /**
     * Show the form for creating a new oc_option_value.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_option_values.create');
    }

    /**
     * Store a newly created oc_option_value in storage.
     *
     * @param Createoc_option_valueRequest $request
     *
     * @return Response
     */
    public function store(Createoc_option_valueRequest $request)
    {
        $input = $request->all();

        $ocOptionValue = $this->ocOptionValueRepository->create($input);

        Flash::success('Oc Option Value saved successfully.');

        return redirect(route('ocOptionValues.index'));
    }

    /**
     * Display the specified oc_option_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            Flash::error('Oc Option Value not found');

            return redirect(route('ocOptionValues.index'));
        }

        return view('oc_option_values.show')->with('ocOptionValue', $ocOptionValue);
    }

    /**
     * Show the form for editing the specified oc_option_value.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            Flash::error('Oc Option Value not found');

            return redirect(route('ocOptionValues.index'));
        }

        return view('oc_option_values.edit')->with('ocOptionValue', $ocOptionValue);
    }

    /**
     * Update the specified oc_option_value in storage.
     *
     * @param  int              $id
     * @param Updateoc_option_valueRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_option_valueRequest $request)
    {
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            Flash::error('Oc Option Value not found');

            return redirect(route('ocOptionValues.index'));
        }

        $ocOptionValue = $this->ocOptionValueRepository->update($request->all(), $id);

        Flash::success('Oc Option Value updated successfully.');

        return redirect(route('ocOptionValues.index'));
    }

    /**
     * Remove the specified oc_option_value from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            Flash::error('Oc Option Value not found');

            return redirect(route('ocOptionValues.index'));
        }

        $this->ocOptionValueRepository->delete($id);

        Flash::success('Oc Option Value deleted successfully.');

        return redirect(route('ocOptionValues.index'));
    }
}
