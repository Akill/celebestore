<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_recurring_transactionRequest;
use App\Http\Requests\Updateoc_order_recurring_transactionRequest;
use App\Repositories\oc_order_recurring_transactionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_recurring_transactionController extends AppBaseController
{
    /** @var  oc_order_recurring_transactionRepository */
    private $ocOrderRecurringTransactionRepository;

    public function __construct(oc_order_recurring_transactionRepository $ocOrderRecurringTransactionRepo)
    {
        $this->ocOrderRecurringTransactionRepository = $ocOrderRecurringTransactionRepo;
    }

    /**
     * Display a listing of the oc_order_recurring_transaction.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRecurringTransactionRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderRecurringTransactions = $this->ocOrderRecurringTransactionRepository->paginate(20);

        return view('oc_order_recurring_transactions.index')
            ->with('ocOrderRecurringTransactions', $ocOrderRecurringTransactions);
    }

    /**
     * Show the form for creating a new oc_order_recurring_transaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_recurring_transactions.create');
    }

    /**
     * Store a newly created oc_order_recurring_transaction in storage.
     *
     * @param Createoc_order_recurring_transactionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_recurring_transactionRequest $request)
    {
        $input = $request->all();

        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->create($input);

        Flash::success('Oc Order Recurring Transaction saved successfully.');

        return redirect(route('ocOrderRecurringTransactions.index'));
    }

    /**
     * Display the specified oc_order_recurring_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            Flash::error('Oc Order Recurring Transaction not found');

            return redirect(route('ocOrderRecurringTransactions.index'));
        }

        return view('oc_order_recurring_transactions.show')->with('ocOrderRecurringTransaction', $ocOrderRecurringTransaction);
    }

    /**
     * Show the form for editing the specified oc_order_recurring_transaction.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            Flash::error('Oc Order Recurring Transaction not found');

            return redirect(route('ocOrderRecurringTransactions.index'));
        }

        return view('oc_order_recurring_transactions.edit')->with('ocOrderRecurringTransaction', $ocOrderRecurringTransaction);
    }

    /**
     * Update the specified oc_order_recurring_transaction in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_recurring_transactionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_recurring_transactionRequest $request)
    {
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            Flash::error('Oc Order Recurring Transaction not found');

            return redirect(route('ocOrderRecurringTransactions.index'));
        }

        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->update($request->all(), $id);

        Flash::success('Oc Order Recurring Transaction updated successfully.');

        return redirect(route('ocOrderRecurringTransactions.index'));
    }

    /**
     * Remove the specified oc_order_recurring_transaction from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            Flash::error('Oc Order Recurring Transaction not found');

            return redirect(route('ocOrderRecurringTransactions.index'));
        }

        $this->ocOrderRecurringTransactionRepository->delete($id);

        Flash::success('Oc Order Recurring Transaction deleted successfully.');

        return redirect(route('ocOrderRecurringTransactions.index'));
    }
}
