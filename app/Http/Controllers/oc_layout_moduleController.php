<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_layout_moduleRequest;
use App\Http\Requests\Updateoc_layout_moduleRequest;
use App\Repositories\oc_layout_moduleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_layout_moduleController extends AppBaseController
{
    /** @var  oc_layout_moduleRepository */
    private $ocLayoutModuleRepository;

    public function __construct(oc_layout_moduleRepository $ocLayoutModuleRepo)
    {
        $this->ocLayoutModuleRepository = $ocLayoutModuleRepo;
    }

    /**
     * Display a listing of the oc_layout_module.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLayoutModuleRepository->pushCriteria(new RequestCriteria($request));
        $ocLayoutModules = $this->ocLayoutModuleRepository->paginate(20);

        return view('oc_layout_modules.index')
            ->with('ocLayoutModules', $ocLayoutModules);
    }

    /**
     * Show the form for creating a new oc_layout_module.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_layout_modules.create');
    }

    /**
     * Store a newly created oc_layout_module in storage.
     *
     * @param Createoc_layout_moduleRequest $request
     *
     * @return Response
     */
    public function store(Createoc_layout_moduleRequest $request)
    {
        $input = $request->all();

        $ocLayoutModule = $this->ocLayoutModuleRepository->create($input);

        Flash::success('Oc Layout Module saved successfully.');

        return redirect(route('ocLayoutModules.index'));
    }

    /**
     * Display the specified oc_layout_module.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            Flash::error('Oc Layout Module not found');

            return redirect(route('ocLayoutModules.index'));
        }

        return view('oc_layout_modules.show')->with('ocLayoutModule', $ocLayoutModule);
    }

    /**
     * Show the form for editing the specified oc_layout_module.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            Flash::error('Oc Layout Module not found');

            return redirect(route('ocLayoutModules.index'));
        }

        return view('oc_layout_modules.edit')->with('ocLayoutModule', $ocLayoutModule);
    }

    /**
     * Update the specified oc_layout_module in storage.
     *
     * @param  int              $id
     * @param Updateoc_layout_moduleRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_layout_moduleRequest $request)
    {
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            Flash::error('Oc Layout Module not found');

            return redirect(route('ocLayoutModules.index'));
        }

        $ocLayoutModule = $this->ocLayoutModuleRepository->update($request->all(), $id);

        Flash::success('Oc Layout Module updated successfully.');

        return redirect(route('ocLayoutModules.index'));
    }

    /**
     * Remove the specified oc_layout_module from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            Flash::error('Oc Layout Module not found');

            return redirect(route('ocLayoutModules.index'));
        }

        $this->ocLayoutModuleRepository->delete($id);

        Flash::success('Oc Layout Module deleted successfully.');

        return redirect(route('ocLayoutModules.index'));
    }
}
