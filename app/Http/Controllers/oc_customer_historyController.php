<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_historyRequest;
use App\Http\Requests\Updateoc_customer_historyRequest;
use App\Repositories\oc_customer_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_historyController extends AppBaseController
{
    /** @var  oc_customer_historyRepository */
    private $ocCustomerHistoryRepository;

    public function __construct(oc_customer_historyRepository $ocCustomerHistoryRepo)
    {
        $this->ocCustomerHistoryRepository = $ocCustomerHistoryRepo;
    }

    /**
     * Display a listing of the oc_customer_history.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerHistoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerHistories = $this->ocCustomerHistoryRepository->paginate(20);

        return view('oc_customer_histories.index')
            ->with('ocCustomerHistories', $ocCustomerHistories);
    }

    /**
     * Show the form for creating a new oc_customer_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_histories.create');
    }

    /**
     * Store a newly created oc_customer_history in storage.
     *
     * @param Createoc_customer_historyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_historyRequest $request)
    {
        $input = $request->all();

        $ocCustomerHistory = $this->ocCustomerHistoryRepository->create($input);

        Flash::success('Oc Customer History saved successfully.');

        return redirect(route('ocCustomerHistories.index'));
    }

    /**
     * Display the specified oc_customer_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            Flash::error('Oc Customer History not found');

            return redirect(route('ocCustomerHistories.index'));
        }

        return view('oc_customer_histories.show')->with('ocCustomerHistory', $ocCustomerHistory);
    }

    /**
     * Show the form for editing the specified oc_customer_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            Flash::error('Oc Customer History not found');

            return redirect(route('ocCustomerHistories.index'));
        }

        return view('oc_customer_histories.edit')->with('ocCustomerHistory', $ocCustomerHistory);
    }

    /**
     * Update the specified oc_customer_history in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_historyRequest $request)
    {
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            Flash::error('Oc Customer History not found');

            return redirect(route('ocCustomerHistories.index'));
        }

        $ocCustomerHistory = $this->ocCustomerHistoryRepository->update($request->all(), $id);

        Flash::success('Oc Customer History updated successfully.');

        return redirect(route('ocCustomerHistories.index'));
    }

    /**
     * Remove the specified oc_customer_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            Flash::error('Oc Customer History not found');

            return redirect(route('ocCustomerHistories.index'));
        }

        $this->ocCustomerHistoryRepository->delete($id);

        Flash::success('Oc Customer History deleted successfully.');

        return redirect(route('ocCustomerHistories.index'));
    }
}
