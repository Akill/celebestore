<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_voucher_historyRequest;
use App\Http\Requests\Updateoc_voucher_historyRequest;
use App\Repositories\oc_voucher_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_voucher_historyController extends AppBaseController
{
    /** @var  oc_voucher_historyRepository */
    private $ocVoucherHistoryRepository;

    public function __construct(oc_voucher_historyRepository $ocVoucherHistoryRepo)
    {
        $this->ocVoucherHistoryRepository = $ocVoucherHistoryRepo;
    }

    /**
     * Display a listing of the oc_voucher_history.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherHistoryRepository->pushCriteria(new RequestCriteria($request));
        $ocVoucherHistories = $this->ocVoucherHistoryRepository->paginate(20);

        return view('oc_voucher_histories.index')
            ->with('ocVoucherHistories', $ocVoucherHistories);
    }

    /**
     * Show the form for creating a new oc_voucher_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_voucher_histories.create');
    }

    /**
     * Store a newly created oc_voucher_history in storage.
     *
     * @param Createoc_voucher_historyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucher_historyRequest $request)
    {
        $input = $request->all();

        $ocVoucherHistory = $this->ocVoucherHistoryRepository->create($input);

        Flash::success('Oc Voucher History saved successfully.');

        return redirect(route('ocVoucherHistories.index'));
    }

    /**
     * Display the specified oc_voucher_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            Flash::error('Oc Voucher History not found');

            return redirect(route('ocVoucherHistories.index'));
        }

        return view('oc_voucher_histories.show')->with('ocVoucherHistory', $ocVoucherHistory);
    }

    /**
     * Show the form for editing the specified oc_voucher_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            Flash::error('Oc Voucher History not found');

            return redirect(route('ocVoucherHistories.index'));
        }

        return view('oc_voucher_histories.edit')->with('ocVoucherHistory', $ocVoucherHistory);
    }

    /**
     * Update the specified oc_voucher_history in storage.
     *
     * @param  int              $id
     * @param Updateoc_voucher_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucher_historyRequest $request)
    {
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            Flash::error('Oc Voucher History not found');

            return redirect(route('ocVoucherHistories.index'));
        }

        $ocVoucherHistory = $this->ocVoucherHistoryRepository->update($request->all(), $id);

        Flash::success('Oc Voucher History updated successfully.');

        return redirect(route('ocVoucherHistories.index'));
    }

    /**
     * Remove the specified oc_voucher_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            Flash::error('Oc Voucher History not found');

            return redirect(route('ocVoucherHistories.index'));
        }

        $this->ocVoucherHistoryRepository->delete($id);

        Flash::success('Oc Voucher History deleted successfully.');

        return redirect(route('ocVoucherHistories.index'));
    }
}
