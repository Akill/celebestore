<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_category_pathAPIRequest;
use App\Http\Requests\API\Updateoc_category_pathAPIRequest;
use App\Models\oc_category_path;
use App\Repositories\oc_category_pathRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_category_pathController
 * @package App\Http\Controllers\API
 */

class oc_category_pathAPIController extends AppBaseController
{
    /** @var  oc_category_pathRepository */
    private $ocCategoryPathRepository;

    public function __construct(oc_category_pathRepository $ocCategoryPathRepo)
    {
        $this->ocCategoryPathRepository = $ocCategoryPathRepo;
    }

    /**
     * Display a listing of the oc_category_path.
     * GET|HEAD /ocCategoryPaths
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryPathRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryPathRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategoryPaths = $this->ocCategoryPathRepository->all();

        return $this->sendResponse($ocCategoryPaths->toArray(), 'Oc Category Paths retrieved successfully');
    }

    /**
     * Store a newly created oc_category_path in storage.
     * POST /ocCategoryPaths
     *
     * @param Createoc_category_pathAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_pathAPIRequest $request)
    {
        $input = $request->all();

        $ocCategoryPaths = $this->ocCategoryPathRepository->create($input);

        return $this->sendResponse($ocCategoryPaths->toArray(), 'Oc Category Path saved successfully');
    }

    /**
     * Display the specified oc_category_path.
     * GET|HEAD /ocCategoryPaths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category_path $ocCategoryPath */
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            return $this->sendError('Oc Category Path not found');
        }

        return $this->sendResponse($ocCategoryPath->toArray(), 'Oc Category Path retrieved successfully');
    }

    /**
     * Update the specified oc_category_path in storage.
     * PUT/PATCH /ocCategoryPaths/{id}
     *
     * @param  int $id
     * @param Updateoc_category_pathAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_pathAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category_path $ocCategoryPath */
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            return $this->sendError('Oc Category Path not found');
        }

        $ocCategoryPath = $this->ocCategoryPathRepository->update($input, $id);

        return $this->sendResponse($ocCategoryPath->toArray(), 'oc_category_path updated successfully');
    }

    /**
     * Remove the specified oc_category_path from storage.
     * DELETE /ocCategoryPaths/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category_path $ocCategoryPath */
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            return $this->sendError('Oc Category Path not found');
        }

        $ocCategoryPath->delete();

        return $this->sendResponse($id, 'Oc Category Path deleted successfully');
    }
}
