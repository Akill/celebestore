<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_eventAPIRequest;
use App\Http\Requests\API\Updateoc_eventAPIRequest;
use App\Models\oc_event;
use App\Repositories\oc_eventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_eventController
 * @package App\Http\Controllers\API
 */

class oc_eventAPIController extends AppBaseController
{
    /** @var  oc_eventRepository */
    private $ocEventRepository;

    public function __construct(oc_eventRepository $ocEventRepo)
    {
        $this->ocEventRepository = $ocEventRepo;
    }

    /**
     * Display a listing of the oc_event.
     * GET|HEAD /ocEvents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocEventRepository->pushCriteria(new RequestCriteria($request));
        $this->ocEventRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocEvents = $this->ocEventRepository->all();

        return $this->sendResponse($ocEvents->toArray(), 'Oc Events retrieved successfully');
    }

    /**
     * Store a newly created oc_event in storage.
     * POST /ocEvents
     *
     * @param Createoc_eventAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_eventAPIRequest $request)
    {
        $input = $request->all();

        $ocEvents = $this->ocEventRepository->create($input);

        return $this->sendResponse($ocEvents->toArray(), 'Oc Event saved successfully');
    }

    /**
     * Display the specified oc_event.
     * GET|HEAD /ocEvents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_event $ocEvent */
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            return $this->sendError('Oc Event not found');
        }

        return $this->sendResponse($ocEvent->toArray(), 'Oc Event retrieved successfully');
    }

    /**
     * Update the specified oc_event in storage.
     * PUT/PATCH /ocEvents/{id}
     *
     * @param  int $id
     * @param Updateoc_eventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_eventAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_event $ocEvent */
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            return $this->sendError('Oc Event not found');
        }

        $ocEvent = $this->ocEventRepository->update($input, $id);

        return $this->sendResponse($ocEvent->toArray(), 'oc_event updated successfully');
    }

    /**
     * Remove the specified oc_event from storage.
     * DELETE /ocEvents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_event $ocEvent */
        $ocEvent = $this->ocEventRepository->findWithoutFail($id);

        if (empty($ocEvent)) {
            return $this->sendError('Oc Event not found');
        }

        $ocEvent->delete();

        return $this->sendResponse($id, 'Oc Event deleted successfully');
    }
}
