<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_moduleAPIRequest;
use App\Http\Requests\API\Updateoc_moduleAPIRequest;
use App\Models\oc_module;
use App\Repositories\oc_moduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_moduleController
 * @package App\Http\Controllers\API
 */

class oc_moduleAPIController extends AppBaseController
{
    /** @var  oc_moduleRepository */
    private $ocModuleRepository;

    public function __construct(oc_moduleRepository $ocModuleRepo)
    {
        $this->ocModuleRepository = $ocModuleRepo;
    }

    /**
     * Display a listing of the oc_module.
     * GET|HEAD /ocModules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocModuleRepository->pushCriteria(new RequestCriteria($request));
        $this->ocModuleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocModules = $this->ocModuleRepository->all();

        return $this->sendResponse($ocModules->toArray(), 'Oc Modules retrieved successfully');
    }

    /**
     * Store a newly created oc_module in storage.
     * POST /ocModules
     *
     * @param Createoc_moduleAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_moduleAPIRequest $request)
    {
        $input = $request->all();

        $ocModules = $this->ocModuleRepository->create($input);

        return $this->sendResponse($ocModules->toArray(), 'Oc Module saved successfully');
    }

    /**
     * Display the specified oc_module.
     * GET|HEAD /ocModules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_module $ocModule */
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            return $this->sendError('Oc Module not found');
        }

        return $this->sendResponse($ocModule->toArray(), 'Oc Module retrieved successfully');
    }

    /**
     * Update the specified oc_module in storage.
     * PUT/PATCH /ocModules/{id}
     *
     * @param  int $id
     * @param Updateoc_moduleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_moduleAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_module $ocModule */
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            return $this->sendError('Oc Module not found');
        }

        $ocModule = $this->ocModuleRepository->update($input, $id);

        return $this->sendResponse($ocModule->toArray(), 'oc_module updated successfully');
    }

    /**
     * Remove the specified oc_module from storage.
     * DELETE /ocModules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_module $ocModule */
        $ocModule = $this->ocModuleRepository->findWithoutFail($id);

        if (empty($ocModule)) {
            return $this->sendError('Oc Module not found');
        }

        $ocModule->delete();

        return $this->sendResponse($id, 'Oc Module deleted successfully');
    }
}
