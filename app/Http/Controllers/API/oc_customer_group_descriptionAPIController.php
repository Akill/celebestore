<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_group_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_customer_group_descriptionAPIRequest;
use App\Models\oc_customer_group_description;
use App\Repositories\oc_customer_group_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_group_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_customer_group_descriptionAPIController extends AppBaseController
{
    /** @var  oc_customer_group_descriptionRepository */
    private $ocCustomerGroupDescriptionRepository;

    public function __construct(oc_customer_group_descriptionRepository $ocCustomerGroupDescriptionRepo)
    {
        $this->ocCustomerGroupDescriptionRepository = $ocCustomerGroupDescriptionRepo;
    }

    /**
     * Display a listing of the oc_customer_group_description.
     * GET|HEAD /ocCustomerGroupDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerGroupDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerGroupDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerGroupDescriptions = $this->ocCustomerGroupDescriptionRepository->all();

        return $this->sendResponse($ocCustomerGroupDescriptions->toArray(), 'Oc Customer Group Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_group_description in storage.
     * POST /ocCustomerGroupDescriptions
     *
     * @param Createoc_customer_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerGroupDescriptions = $this->ocCustomerGroupDescriptionRepository->create($input);

        return $this->sendResponse($ocCustomerGroupDescriptions->toArray(), 'Oc Customer Group Description saved successfully');
    }

    /**
     * Display the specified oc_customer_group_description.
     * GET|HEAD /ocCustomerGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_group_description $ocCustomerGroupDescription */
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            return $this->sendError('Oc Customer Group Description not found');
        }

        return $this->sendResponse($ocCustomerGroupDescription->toArray(), 'Oc Customer Group Description retrieved successfully');
    }

    /**
     * Update the specified oc_customer_group_description in storage.
     * PUT/PATCH /ocCustomerGroupDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_group_description $ocCustomerGroupDescription */
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            return $this->sendError('Oc Customer Group Description not found');
        }

        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocCustomerGroupDescription->toArray(), 'oc_customer_group_description updated successfully');
    }

    /**
     * Remove the specified oc_customer_group_description from storage.
     * DELETE /ocCustomerGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_group_description $ocCustomerGroupDescription */
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            return $this->sendError('Oc Customer Group Description not found');
        }

        $ocCustomerGroupDescription->delete();

        return $this->sendResponse($id, 'Oc Customer Group Description deleted successfully');
    }
}
