<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_filter_group_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_filter_group_descriptionAPIRequest;
use App\Models\oc_filter_group_description;
use App\Repositories\oc_filter_group_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_filter_group_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_filter_group_descriptionAPIController extends AppBaseController
{
    /** @var  oc_filter_group_descriptionRepository */
    private $ocFilterGroupDescriptionRepository;

    public function __construct(oc_filter_group_descriptionRepository $ocFilterGroupDescriptionRepo)
    {
        $this->ocFilterGroupDescriptionRepository = $ocFilterGroupDescriptionRepo;
    }

    /**
     * Display a listing of the oc_filter_group_description.
     * GET|HEAD /ocFilterGroupDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterGroupDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocFilterGroupDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocFilterGroupDescriptions = $this->ocFilterGroupDescriptionRepository->all();

        return $this->sendResponse($ocFilterGroupDescriptions->toArray(), 'Oc Filter Group Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_filter_group_description in storage.
     * POST /ocFilterGroupDescriptions
     *
     * @param Createoc_filter_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filter_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocFilterGroupDescriptions = $this->ocFilterGroupDescriptionRepository->create($input);

        return $this->sendResponse($ocFilterGroupDescriptions->toArray(), 'Oc Filter Group Description saved successfully');
    }

    /**
     * Display the specified oc_filter_group_description.
     * GET|HEAD /ocFilterGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_filter_group_description $ocFilterGroupDescription */
        $ocFilterGroupDescription = $this->ocFilterGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterGroupDescription)) {
            return $this->sendError('Oc Filter Group Description not found');
        }

        return $this->sendResponse($ocFilterGroupDescription->toArray(), 'Oc Filter Group Description retrieved successfully');
    }

    /**
     * Update the specified oc_filter_group_description in storage.
     * PUT/PATCH /ocFilterGroupDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_filter_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filter_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_filter_group_description $ocFilterGroupDescription */
        $ocFilterGroupDescription = $this->ocFilterGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterGroupDescription)) {
            return $this->sendError('Oc Filter Group Description not found');
        }

        $ocFilterGroupDescription = $this->ocFilterGroupDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocFilterGroupDescription->toArray(), 'oc_filter_group_description updated successfully');
    }

    /**
     * Remove the specified oc_filter_group_description from storage.
     * DELETE /ocFilterGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_filter_group_description $ocFilterGroupDescription */
        $ocFilterGroupDescription = $this->ocFilterGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterGroupDescription)) {
            return $this->sendError('Oc Filter Group Description not found');
        }

        $ocFilterGroupDescription->delete();

        return $this->sendResponse($id, 'Oc Filter Group Description deleted successfully');
    }
}
