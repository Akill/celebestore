<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_productAPIRequest;
use App\Http\Requests\API\Updateoc_productAPIRequest;
use App\Models\oc_product;
use App\Repositories\oc_productRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_productController
 * @package App\Http\Controllers\API
 */

class oc_productAPIController extends AppBaseController
{
    /** @var  oc_productRepository */
    private $ocProductRepository;

    public function __construct(oc_productRepository $ocProductRepo)
    {
        $this->ocProductRepository = $ocProductRepo;
    }

    /**
     * Display a listing of the oc_product.
     * GET|HEAD /ocProducts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProducts = $this->ocProductRepository->all();

        return $this->sendResponse($ocProducts->toArray(), 'Oc Products retrieved successfully');
    }

    /**
     * Store a newly created oc_product in storage.
     * POST /ocProducts
     *
     * @param Createoc_productAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_productAPIRequest $request)
    {
        $input = $request->all();

        $ocProducts = $this->ocProductRepository->create($input);

        return $this->sendResponse($ocProducts->toArray(), 'Oc Product saved successfully');
    }

    /**
     * Display the specified oc_product.
     * GET|HEAD /ocProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product $ocProduct */
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            return $this->sendError('Oc Product not found');
        }

        return $this->sendResponse($ocProduct->toArray(), 'Oc Product retrieved successfully');
    }

    /**
     * Update the specified oc_product in storage.
     * PUT/PATCH /ocProducts/{id}
     *
     * @param  int $id
     * @param Updateoc_productAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_productAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product $ocProduct */
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            return $this->sendError('Oc Product not found');
        }

        $ocProduct = $this->ocProductRepository->update($input, $id);

        return $this->sendResponse($ocProduct->toArray(), 'oc_product updated successfully');
    }

    /**
     * Remove the specified oc_product from storage.
     * DELETE /ocProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product $ocProduct */
        $ocProduct = $this->ocProductRepository->findWithoutFail($id);

        if (empty($ocProduct)) {
            return $this->sendError('Oc Product not found');
        }

        $ocProduct->delete();

        return $this->sendResponse($id, 'Oc Product deleted successfully');
    }
}
