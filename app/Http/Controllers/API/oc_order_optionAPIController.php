<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_optionAPIRequest;
use App\Http\Requests\API\Updateoc_order_optionAPIRequest;
use App\Models\oc_order_option;
use App\Repositories\oc_order_optionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_optionController
 * @package App\Http\Controllers\API
 */

class oc_order_optionAPIController extends AppBaseController
{
    /** @var  oc_order_optionRepository */
    private $ocOrderOptionRepository;

    public function __construct(oc_order_optionRepository $ocOrderOptionRepo)
    {
        $this->ocOrderOptionRepository = $ocOrderOptionRepo;
    }

    /**
     * Display a listing of the oc_order_option.
     * GET|HEAD /ocOrderOptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderOptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderOptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderOptions = $this->ocOrderOptionRepository->all();

        return $this->sendResponse($ocOrderOptions->toArray(), 'Oc Order Options retrieved successfully');
    }

    /**
     * Store a newly created oc_order_option in storage.
     * POST /ocOrderOptions
     *
     * @param Createoc_order_optionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_optionAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderOptions = $this->ocOrderOptionRepository->create($input);

        return $this->sendResponse($ocOrderOptions->toArray(), 'Oc Order Option saved successfully');
    }

    /**
     * Display the specified oc_order_option.
     * GET|HEAD /ocOrderOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_option $ocOrderOption */
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            return $this->sendError('Oc Order Option not found');
        }

        return $this->sendResponse($ocOrderOption->toArray(), 'Oc Order Option retrieved successfully');
    }

    /**
     * Update the specified oc_order_option in storage.
     * PUT/PATCH /ocOrderOptions/{id}
     *
     * @param  int $id
     * @param Updateoc_order_optionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_optionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_option $ocOrderOption */
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            return $this->sendError('Oc Order Option not found');
        }

        $ocOrderOption = $this->ocOrderOptionRepository->update($input, $id);

        return $this->sendResponse($ocOrderOption->toArray(), 'oc_order_option updated successfully');
    }

    /**
     * Remove the specified oc_order_option from storage.
     * DELETE /ocOrderOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_option $ocOrderOption */
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            return $this->sendError('Oc Order Option not found');
        }

        $ocOrderOption->delete();

        return $this->sendResponse($id, 'Oc Order Option deleted successfully');
    }
}
