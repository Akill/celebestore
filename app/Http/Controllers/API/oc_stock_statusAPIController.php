<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_stock_statusAPIRequest;
use App\Http\Requests\API\Updateoc_stock_statusAPIRequest;
use App\Models\oc_stock_status;
use App\Repositories\oc_stock_statusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_stock_statusController
 * @package App\Http\Controllers\API
 */

class oc_stock_statusAPIController extends AppBaseController
{
    /** @var  oc_stock_statusRepository */
    private $ocStockStatusRepository;

    public function __construct(oc_stock_statusRepository $ocStockStatusRepo)
    {
        $this->ocStockStatusRepository = $ocStockStatusRepo;
    }

    /**
     * Display a listing of the oc_stock_status.
     * GET|HEAD /ocStockStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocStockStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->ocStockStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocStockStatuses = $this->ocStockStatusRepository->all();

        return $this->sendResponse($ocStockStatuses->toArray(), 'Oc Stock Statuses retrieved successfully');
    }

    /**
     * Store a newly created oc_stock_status in storage.
     * POST /ocStockStatuses
     *
     * @param Createoc_stock_statusAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_stock_statusAPIRequest $request)
    {
        $input = $request->all();

        $ocStockStatuses = $this->ocStockStatusRepository->create($input);

        return $this->sendResponse($ocStockStatuses->toArray(), 'Oc Stock Status saved successfully');
    }

    /**
     * Display the specified oc_stock_status.
     * GET|HEAD /ocStockStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_stock_status $ocStockStatus */
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            return $this->sendError('Oc Stock Status not found');
        }

        return $this->sendResponse($ocStockStatus->toArray(), 'Oc Stock Status retrieved successfully');
    }

    /**
     * Update the specified oc_stock_status in storage.
     * PUT/PATCH /ocStockStatuses/{id}
     *
     * @param  int $id
     * @param Updateoc_stock_statusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_stock_statusAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_stock_status $ocStockStatus */
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            return $this->sendError('Oc Stock Status not found');
        }

        $ocStockStatus = $this->ocStockStatusRepository->update($input, $id);

        return $this->sendResponse($ocStockStatus->toArray(), 'oc_stock_status updated successfully');
    }

    /**
     * Remove the specified oc_stock_status from storage.
     * DELETE /ocStockStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_stock_status $ocStockStatus */
        $ocStockStatus = $this->ocStockStatusRepository->findWithoutFail($id);

        if (empty($ocStockStatus)) {
            return $this->sendError('Oc Stock Status not found');
        }

        $ocStockStatus->delete();

        return $this->sendResponse($id, 'Oc Stock Status deleted successfully');
    }
}
