<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_locationAPIRequest;
use App\Http\Requests\API\Updateoc_locationAPIRequest;
use App\Models\oc_location;
use App\Repositories\oc_locationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_locationController
 * @package App\Http\Controllers\API
 */

class oc_locationAPIController extends AppBaseController
{
    /** @var  oc_locationRepository */
    private $ocLocationRepository;

    public function __construct(oc_locationRepository $ocLocationRepo)
    {
        $this->ocLocationRepository = $ocLocationRepo;
    }

    /**
     * Display a listing of the oc_location.
     * GET|HEAD /ocLocations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLocationRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLocationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLocations = $this->ocLocationRepository->all();

        return $this->sendResponse($ocLocations->toArray(), 'Oc Locations retrieved successfully');
    }

    /**
     * Store a newly created oc_location in storage.
     * POST /ocLocations
     *
     * @param Createoc_locationAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_locationAPIRequest $request)
    {
        $input = $request->all();

        $ocLocations = $this->ocLocationRepository->create($input);

        return $this->sendResponse($ocLocations->toArray(), 'Oc Location saved successfully');
    }

    /**
     * Display the specified oc_location.
     * GET|HEAD /ocLocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_location $ocLocation */
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            return $this->sendError('Oc Location not found');
        }

        return $this->sendResponse($ocLocation->toArray(), 'Oc Location retrieved successfully');
    }

    /**
     * Update the specified oc_location in storage.
     * PUT/PATCH /ocLocations/{id}
     *
     * @param  int $id
     * @param Updateoc_locationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_locationAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_location $ocLocation */
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            return $this->sendError('Oc Location not found');
        }

        $ocLocation = $this->ocLocationRepository->update($input, $id);

        return $this->sendResponse($ocLocation->toArray(), 'oc_location updated successfully');
    }

    /**
     * Remove the specified oc_location from storage.
     * DELETE /ocLocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_location $ocLocation */
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            return $this->sendError('Oc Location not found');
        }

        $ocLocation->delete();

        return $this->sendResponse($id, 'Oc Location deleted successfully');
    }
}
