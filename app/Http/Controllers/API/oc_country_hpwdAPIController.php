<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_country_hpwdAPIRequest;
use App\Http\Requests\API\Updateoc_country_hpwdAPIRequest;
use App\Models\oc_country_hpwd;
use App\Repositories\oc_country_hpwdRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_country_hpwdController
 * @package App\Http\Controllers\API
 */

class oc_country_hpwdAPIController extends AppBaseController
{
    /** @var  oc_country_hpwdRepository */
    private $ocCountryHpwdRepository;

    public function __construct(oc_country_hpwdRepository $ocCountryHpwdRepo)
    {
        $this->ocCountryHpwdRepository = $ocCountryHpwdRepo;
    }

    /**
     * Display a listing of the oc_country_hpwd.
     * GET|HEAD /ocCountryHpwds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCountryHpwdRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCountryHpwdRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCountryHpwds = $this->ocCountryHpwdRepository->all();

        return $this->sendResponse($ocCountryHpwds->toArray(), 'Oc Country Hpwds retrieved successfully');
    }

    /**
     * Store a newly created oc_country_hpwd in storage.
     * POST /ocCountryHpwds
     *
     * @param Createoc_country_hpwdAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_country_hpwdAPIRequest $request)
    {
        $input = $request->all();

        $ocCountryHpwds = $this->ocCountryHpwdRepository->create($input);

        return $this->sendResponse($ocCountryHpwds->toArray(), 'Oc Country Hpwd saved successfully');
    }

    /**
     * Display the specified oc_country_hpwd.
     * GET|HEAD /ocCountryHpwds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_country_hpwd $ocCountryHpwd */
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            return $this->sendError('Oc Country Hpwd not found');
        }

        return $this->sendResponse($ocCountryHpwd->toArray(), 'Oc Country Hpwd retrieved successfully');
    }

    /**
     * Update the specified oc_country_hpwd in storage.
     * PUT/PATCH /ocCountryHpwds/{id}
     *
     * @param  int $id
     * @param Updateoc_country_hpwdAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_country_hpwdAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_country_hpwd $ocCountryHpwd */
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            return $this->sendError('Oc Country Hpwd not found');
        }

        $ocCountryHpwd = $this->ocCountryHpwdRepository->update($input, $id);

        return $this->sendResponse($ocCountryHpwd->toArray(), 'oc_country_hpwd updated successfully');
    }

    /**
     * Remove the specified oc_country_hpwd from storage.
     * DELETE /ocCountryHpwds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_country_hpwd $ocCountryHpwd */
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            return $this->sendError('Oc Country Hpwd not found');
        }

        $ocCountryHpwd->delete();

        return $this->sendResponse($id, 'Oc Country Hpwd deleted successfully');
    }
}
