<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_recurring_transactionAPIRequest;
use App\Http\Requests\API\Updateoc_order_recurring_transactionAPIRequest;
use App\Models\oc_order_recurring_transaction;
use App\Repositories\oc_order_recurring_transactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_recurring_transactionController
 * @package App\Http\Controllers\API
 */

class oc_order_recurring_transactionAPIController extends AppBaseController
{
    /** @var  oc_order_recurring_transactionRepository */
    private $ocOrderRecurringTransactionRepository;

    public function __construct(oc_order_recurring_transactionRepository $ocOrderRecurringTransactionRepo)
    {
        $this->ocOrderRecurringTransactionRepository = $ocOrderRecurringTransactionRepo;
    }

    /**
     * Display a listing of the oc_order_recurring_transaction.
     * GET|HEAD /ocOrderRecurringTransactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRecurringTransactionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderRecurringTransactionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderRecurringTransactions = $this->ocOrderRecurringTransactionRepository->all();

        return $this->sendResponse($ocOrderRecurringTransactions->toArray(), 'Oc Order Recurring Transactions retrieved successfully');
    }

    /**
     * Store a newly created oc_order_recurring_transaction in storage.
     * POST /ocOrderRecurringTransactions
     *
     * @param Createoc_order_recurring_transactionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_recurring_transactionAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderRecurringTransactions = $this->ocOrderRecurringTransactionRepository->create($input);

        return $this->sendResponse($ocOrderRecurringTransactions->toArray(), 'Oc Order Recurring Transaction saved successfully');
    }

    /**
     * Display the specified oc_order_recurring_transaction.
     * GET|HEAD /ocOrderRecurringTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_recurring_transaction $ocOrderRecurringTransaction */
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            return $this->sendError('Oc Order Recurring Transaction not found');
        }

        return $this->sendResponse($ocOrderRecurringTransaction->toArray(), 'Oc Order Recurring Transaction retrieved successfully');
    }

    /**
     * Update the specified oc_order_recurring_transaction in storage.
     * PUT/PATCH /ocOrderRecurringTransactions/{id}
     *
     * @param  int $id
     * @param Updateoc_order_recurring_transactionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_recurring_transactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_recurring_transaction $ocOrderRecurringTransaction */
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            return $this->sendError('Oc Order Recurring Transaction not found');
        }

        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->update($input, $id);

        return $this->sendResponse($ocOrderRecurringTransaction->toArray(), 'oc_order_recurring_transaction updated successfully');
    }

    /**
     * Remove the specified oc_order_recurring_transaction from storage.
     * DELETE /ocOrderRecurringTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_recurring_transaction $ocOrderRecurringTransaction */
        $ocOrderRecurringTransaction = $this->ocOrderRecurringTransactionRepository->findWithoutFail($id);

        if (empty($ocOrderRecurringTransaction)) {
            return $this->sendError('Oc Order Recurring Transaction not found');
        }

        $ocOrderRecurringTransaction->delete();

        return $this->sendResponse($id, 'Oc Order Recurring Transaction deleted successfully');
    }
}
