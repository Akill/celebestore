<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_option_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_option_descriptionAPIRequest;
use App\Models\oc_option_description;
use App\Repositories\oc_option_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_option_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_option_descriptionAPIController extends AppBaseController
{
    /** @var  oc_option_descriptionRepository */
    private $ocOptionDescriptionRepository;

    public function __construct(oc_option_descriptionRepository $ocOptionDescriptionRepo)
    {
        $this->ocOptionDescriptionRepository = $ocOptionDescriptionRepo;
    }

    /**
     * Display a listing of the oc_option_description.
     * GET|HEAD /ocOptionDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOptionDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOptionDescriptions = $this->ocOptionDescriptionRepository->all();

        return $this->sendResponse($ocOptionDescriptions->toArray(), 'Oc Option Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_option_description in storage.
     * POST /ocOptionDescriptions
     *
     * @param Createoc_option_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_option_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocOptionDescriptions = $this->ocOptionDescriptionRepository->create($input);

        return $this->sendResponse($ocOptionDescriptions->toArray(), 'Oc Option Description saved successfully');
    }

    /**
     * Display the specified oc_option_description.
     * GET|HEAD /ocOptionDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_option_description $ocOptionDescription */
        $ocOptionDescription = $this->ocOptionDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionDescription)) {
            return $this->sendError('Oc Option Description not found');
        }

        return $this->sendResponse($ocOptionDescription->toArray(), 'Oc Option Description retrieved successfully');
    }

    /**
     * Update the specified oc_option_description in storage.
     * PUT/PATCH /ocOptionDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_option_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_option_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_option_description $ocOptionDescription */
        $ocOptionDescription = $this->ocOptionDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionDescription)) {
            return $this->sendError('Oc Option Description not found');
        }

        $ocOptionDescription = $this->ocOptionDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocOptionDescription->toArray(), 'oc_option_description updated successfully');
    }

    /**
     * Remove the specified oc_option_description from storage.
     * DELETE /ocOptionDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_option_description $ocOptionDescription */
        $ocOptionDescription = $this->ocOptionDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionDescription)) {
            return $this->sendError('Oc Option Description not found');
        }

        $ocOptionDescription->delete();

        return $this->sendResponse($id, 'Oc Option Description deleted successfully');
    }
}
