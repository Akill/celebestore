<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customerAPIRequest;
use App\Http\Requests\API\Updateoc_customerAPIRequest;
use App\Models\oc_customer;
use App\Repositories\oc_customerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customerController
 * @package App\Http\Controllers\API
 */

class oc_customerAPIController extends AppBaseController
{
    /** @var  oc_customerRepository */
    private $ocCustomerRepository;

    public function __construct(oc_customerRepository $ocCustomerRepo)
    {
        $this->ocCustomerRepository = $ocCustomerRepo;
    }

    /**
     * Display a listing of the oc_customer.
     * GET|HEAD /ocCustomers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomers = $this->ocCustomerRepository->all();

        return $this->sendResponse($ocCustomers->toArray(), 'Oc Customers retrieved successfully');
    }

    /**
     * Store a newly created oc_customer in storage.
     * POST /ocCustomers
     *
     * @param Createoc_customerAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customerAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomers = $this->ocCustomerRepository->create($input);

        return $this->sendResponse($ocCustomers->toArray(), 'Oc Customer saved successfully');
    }

    /**
     * Display the specified oc_customer.
     * GET|HEAD /ocCustomers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer $ocCustomer */
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            return $this->sendError('Oc Customer not found');
        }

        return $this->sendResponse($ocCustomer->toArray(), 'Oc Customer retrieved successfully');
    }

    /**
     * Update the specified oc_customer in storage.
     * PUT/PATCH /ocCustomers/{id}
     *
     * @param  int $id
     * @param Updateoc_customerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customerAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer $ocCustomer */
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            return $this->sendError('Oc Customer not found');
        }

        $ocCustomer = $this->ocCustomerRepository->update($input, $id);

        return $this->sendResponse($ocCustomer->toArray(), 'oc_customer updated successfully');
    }

    /**
     * Remove the specified oc_customer from storage.
     * DELETE /ocCustomers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer $ocCustomer */
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            return $this->sendError('Oc Customer not found');
        }

        $ocCustomer->delete();

        return $this->sendResponse($id, 'Oc Customer deleted successfully');
    }
}
