<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_information_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_information_descriptionAPIRequest;
use App\Models\oc_information_description;
use App\Repositories\oc_information_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_information_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_information_descriptionAPIController extends AppBaseController
{
    /** @var  oc_information_descriptionRepository */
    private $ocInformationDescriptionRepository;

    public function __construct(oc_information_descriptionRepository $ocInformationDescriptionRepo)
    {
        $this->ocInformationDescriptionRepository = $ocInformationDescriptionRepo;
    }

    /**
     * Display a listing of the oc_information_description.
     * GET|HEAD /ocInformationDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocInformationDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocInformationDescriptions = $this->ocInformationDescriptionRepository->all();

        return $this->sendResponse($ocInformationDescriptions->toArray(), 'Oc Information Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_information_description in storage.
     * POST /ocInformationDescriptions
     *
     * @param Createoc_information_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_information_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocInformationDescriptions = $this->ocInformationDescriptionRepository->create($input);

        return $this->sendResponse($ocInformationDescriptions->toArray(), 'Oc Information Description saved successfully');
    }

    /**
     * Display the specified oc_information_description.
     * GET|HEAD /ocInformationDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_information_description $ocInformationDescription */
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            return $this->sendError('Oc Information Description not found');
        }

        return $this->sendResponse($ocInformationDescription->toArray(), 'Oc Information Description retrieved successfully');
    }

    /**
     * Update the specified oc_information_description in storage.
     * PUT/PATCH /ocInformationDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_information_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_information_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_information_description $ocInformationDescription */
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            return $this->sendError('Oc Information Description not found');
        }

        $ocInformationDescription = $this->ocInformationDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocInformationDescription->toArray(), 'oc_information_description updated successfully');
    }

    /**
     * Remove the specified oc_information_description from storage.
     * DELETE /ocInformationDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_information_description $ocInformationDescription */
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            return $this->sendError('Oc Information Description not found');
        }

        $ocInformationDescription->delete();

        return $this->sendResponse($id, 'Oc Information Description deleted successfully');
    }
}
