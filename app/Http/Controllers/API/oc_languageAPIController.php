<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_languageAPIRequest;
use App\Http\Requests\API\Updateoc_languageAPIRequest;
use App\Models\oc_language;
use App\Repositories\oc_languageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_languageController
 * @package App\Http\Controllers\API
 */

class oc_languageAPIController extends AppBaseController
{
    /** @var  oc_languageRepository */
    private $ocLanguageRepository;

    public function __construct(oc_languageRepository $ocLanguageRepo)
    {
        $this->ocLanguageRepository = $ocLanguageRepo;
    }

    /**
     * Display a listing of the oc_language.
     * GET|HEAD /ocLanguages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLanguageRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLanguageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLanguages = $this->ocLanguageRepository->all();

        return $this->sendResponse($ocLanguages->toArray(), 'Oc Languages retrieved successfully');
    }

    /**
     * Store a newly created oc_language in storage.
     * POST /ocLanguages
     *
     * @param Createoc_languageAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_languageAPIRequest $request)
    {
        $input = $request->all();

        $ocLanguages = $this->ocLanguageRepository->create($input);

        return $this->sendResponse($ocLanguages->toArray(), 'Oc Language saved successfully');
    }

    /**
     * Display the specified oc_language.
     * GET|HEAD /ocLanguages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_language $ocLanguage */
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            return $this->sendError('Oc Language not found');
        }

        return $this->sendResponse($ocLanguage->toArray(), 'Oc Language retrieved successfully');
    }

    /**
     * Update the specified oc_language in storage.
     * PUT/PATCH /ocLanguages/{id}
     *
     * @param  int $id
     * @param Updateoc_languageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_languageAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_language $ocLanguage */
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            return $this->sendError('Oc Language not found');
        }

        $ocLanguage = $this->ocLanguageRepository->update($input, $id);

        return $this->sendResponse($ocLanguage->toArray(), 'oc_language updated successfully');
    }

    /**
     * Remove the specified oc_language from storage.
     * DELETE /ocLanguages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_language $ocLanguage */
        $ocLanguage = $this->ocLanguageRepository->findWithoutFail($id);

        if (empty($ocLanguage)) {
            return $this->sendError('Oc Language not found');
        }

        $ocLanguage->delete();

        return $this->sendResponse($id, 'Oc Language deleted successfully');
    }
}
