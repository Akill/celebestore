<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_recurringAPIRequest;
use App\Http\Requests\API\Updateoc_product_recurringAPIRequest;
use App\Models\oc_product_recurring;
use App\Repositories\oc_product_recurringRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_recurringController
 * @package App\Http\Controllers\API
 */

class oc_product_recurringAPIController extends AppBaseController
{
    /** @var  oc_product_recurringRepository */
    private $ocProductRecurringRepository;

    public function __construct(oc_product_recurringRepository $ocProductRecurringRepo)
    {
        $this->ocProductRecurringRepository = $ocProductRecurringRepo;
    }

    /**
     * Display a listing of the oc_product_recurring.
     * GET|HEAD /ocProductRecurrings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRecurringRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductRecurringRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductRecurrings = $this->ocProductRecurringRepository->all();

        return $this->sendResponse($ocProductRecurrings->toArray(), 'Oc Product Recurrings retrieved successfully');
    }

    /**
     * Store a newly created oc_product_recurring in storage.
     * POST /ocProductRecurrings
     *
     * @param Createoc_product_recurringAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_recurringAPIRequest $request)
    {
        $input = $request->all();

        $ocProductRecurrings = $this->ocProductRecurringRepository->create($input);

        return $this->sendResponse($ocProductRecurrings->toArray(), 'Oc Product Recurring saved successfully');
    }

    /**
     * Display the specified oc_product_recurring.
     * GET|HEAD /ocProductRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_recurring $ocProductRecurring */
        $ocProductRecurring = $this->ocProductRecurringRepository->findWithoutFail($id);

        if (empty($ocProductRecurring)) {
            return $this->sendError('Oc Product Recurring not found');
        }

        return $this->sendResponse($ocProductRecurring->toArray(), 'Oc Product Recurring retrieved successfully');
    }

    /**
     * Update the specified oc_product_recurring in storage.
     * PUT/PATCH /ocProductRecurrings/{id}
     *
     * @param  int $id
     * @param Updateoc_product_recurringAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_recurringAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_recurring $ocProductRecurring */
        $ocProductRecurring = $this->ocProductRecurringRepository->findWithoutFail($id);

        if (empty($ocProductRecurring)) {
            return $this->sendError('Oc Product Recurring not found');
        }

        $ocProductRecurring = $this->ocProductRecurringRepository->update($input, $id);

        return $this->sendResponse($ocProductRecurring->toArray(), 'oc_product_recurring updated successfully');
    }

    /**
     * Remove the specified oc_product_recurring from storage.
     * DELETE /ocProductRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_recurring $ocProductRecurring */
        $ocProductRecurring = $this->ocProductRecurringRepository->findWithoutFail($id);

        if (empty($ocProductRecurring)) {
            return $this->sendError('Oc Product Recurring not found');
        }

        $ocProductRecurring->delete();

        return $this->sendResponse($id, 'Oc Product Recurring deleted successfully');
    }
}
