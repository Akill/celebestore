<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_voucher_theme_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_voucher_theme_descriptionAPIRequest;
use App\Models\oc_voucher_theme_description;
use App\Repositories\oc_voucher_theme_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_voucher_theme_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_voucher_theme_descriptionAPIController extends AppBaseController
{
    /** @var  oc_voucher_theme_descriptionRepository */
    private $ocVoucherThemeDescriptionRepository;

    public function __construct(oc_voucher_theme_descriptionRepository $ocVoucherThemeDescriptionRepo)
    {
        $this->ocVoucherThemeDescriptionRepository = $ocVoucherThemeDescriptionRepo;
    }

    /**
     * Display a listing of the oc_voucher_theme_description.
     * GET|HEAD /ocVoucherThemeDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherThemeDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocVoucherThemeDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocVoucherThemeDescriptions = $this->ocVoucherThemeDescriptionRepository->all();

        return $this->sendResponse($ocVoucherThemeDescriptions->toArray(), 'Oc Voucher Theme Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_voucher_theme_description in storage.
     * POST /ocVoucherThemeDescriptions
     *
     * @param Createoc_voucher_theme_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucher_theme_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocVoucherThemeDescriptions = $this->ocVoucherThemeDescriptionRepository->create($input);

        return $this->sendResponse($ocVoucherThemeDescriptions->toArray(), 'Oc Voucher Theme Description saved successfully');
    }

    /**
     * Display the specified oc_voucher_theme_description.
     * GET|HEAD /ocVoucherThemeDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_voucher_theme_description $ocVoucherThemeDescription */
        $ocVoucherThemeDescription = $this->ocVoucherThemeDescriptionRepository->findWithoutFail($id);

        if (empty($ocVoucherThemeDescription)) {
            return $this->sendError('Oc Voucher Theme Description not found');
        }

        return $this->sendResponse($ocVoucherThemeDescription->toArray(), 'Oc Voucher Theme Description retrieved successfully');
    }

    /**
     * Update the specified oc_voucher_theme_description in storage.
     * PUT/PATCH /ocVoucherThemeDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_voucher_theme_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucher_theme_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_voucher_theme_description $ocVoucherThemeDescription */
        $ocVoucherThemeDescription = $this->ocVoucherThemeDescriptionRepository->findWithoutFail($id);

        if (empty($ocVoucherThemeDescription)) {
            return $this->sendError('Oc Voucher Theme Description not found');
        }

        $ocVoucherThemeDescription = $this->ocVoucherThemeDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocVoucherThemeDescription->toArray(), 'oc_voucher_theme_description updated successfully');
    }

    /**
     * Remove the specified oc_voucher_theme_description from storage.
     * DELETE /ocVoucherThemeDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_voucher_theme_description $ocVoucherThemeDescription */
        $ocVoucherThemeDescription = $this->ocVoucherThemeDescriptionRepository->findWithoutFail($id);

        if (empty($ocVoucherThemeDescription)) {
            return $this->sendError('Oc Voucher Theme Description not found');
        }

        $ocVoucherThemeDescription->delete();

        return $this->sendResponse($id, 'Oc Voucher Theme Description deleted successfully');
    }
}
