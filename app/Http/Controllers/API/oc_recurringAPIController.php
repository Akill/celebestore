<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_recurringAPIRequest;
use App\Http\Requests\API\Updateoc_recurringAPIRequest;
use App\Models\oc_recurring;
use App\Repositories\oc_recurringRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_recurringController
 * @package App\Http\Controllers\API
 */

class oc_recurringAPIController extends AppBaseController
{
    /** @var  oc_recurringRepository */
    private $ocRecurringRepository;

    public function __construct(oc_recurringRepository $ocRecurringRepo)
    {
        $this->ocRecurringRepository = $ocRecurringRepo;
    }

    /**
     * Display a listing of the oc_recurring.
     * GET|HEAD /ocRecurrings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocRecurringRepository->pushCriteria(new RequestCriteria($request));
        $this->ocRecurringRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocRecurrings = $this->ocRecurringRepository->all();

        return $this->sendResponse($ocRecurrings->toArray(), 'Oc Recurrings retrieved successfully');
    }

    /**
     * Store a newly created oc_recurring in storage.
     * POST /ocRecurrings
     *
     * @param Createoc_recurringAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_recurringAPIRequest $request)
    {
        $input = $request->all();

        $ocRecurrings = $this->ocRecurringRepository->create($input);

        return $this->sendResponse($ocRecurrings->toArray(), 'Oc Recurring saved successfully');
    }

    /**
     * Display the specified oc_recurring.
     * GET|HEAD /ocRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_recurring $ocRecurring */
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            return $this->sendError('Oc Recurring not found');
        }

        return $this->sendResponse($ocRecurring->toArray(), 'Oc Recurring retrieved successfully');
    }

    /**
     * Update the specified oc_recurring in storage.
     * PUT/PATCH /ocRecurrings/{id}
     *
     * @param  int $id
     * @param Updateoc_recurringAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_recurringAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_recurring $ocRecurring */
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            return $this->sendError('Oc Recurring not found');
        }

        $ocRecurring = $this->ocRecurringRepository->update($input, $id);

        return $this->sendResponse($ocRecurring->toArray(), 'oc_recurring updated successfully');
    }

    /**
     * Remove the specified oc_recurring from storage.
     * DELETE /ocRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_recurring $ocRecurring */
        $ocRecurring = $this->ocRecurringRepository->findWithoutFail($id);

        if (empty($ocRecurring)) {
            return $this->sendError('Oc Recurring not found');
        }

        $ocRecurring->delete();

        return $this->sendResponse($id, 'Oc Recurring deleted successfully');
    }
}
