<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_layout_moduleAPIRequest;
use App\Http\Requests\API\Updateoc_layout_moduleAPIRequest;
use App\Models\oc_layout_module;
use App\Repositories\oc_layout_moduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_layout_moduleController
 * @package App\Http\Controllers\API
 */

class oc_layout_moduleAPIController extends AppBaseController
{
    /** @var  oc_layout_moduleRepository */
    private $ocLayoutModuleRepository;

    public function __construct(oc_layout_moduleRepository $ocLayoutModuleRepo)
    {
        $this->ocLayoutModuleRepository = $ocLayoutModuleRepo;
    }

    /**
     * Display a listing of the oc_layout_module.
     * GET|HEAD /ocLayoutModules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLayoutModuleRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLayoutModuleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLayoutModules = $this->ocLayoutModuleRepository->all();

        return $this->sendResponse($ocLayoutModules->toArray(), 'Oc Layout Modules retrieved successfully');
    }

    /**
     * Store a newly created oc_layout_module in storage.
     * POST /ocLayoutModules
     *
     * @param Createoc_layout_moduleAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_layout_moduleAPIRequest $request)
    {
        $input = $request->all();

        $ocLayoutModules = $this->ocLayoutModuleRepository->create($input);

        return $this->sendResponse($ocLayoutModules->toArray(), 'Oc Layout Module saved successfully');
    }

    /**
     * Display the specified oc_layout_module.
     * GET|HEAD /ocLayoutModules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_layout_module $ocLayoutModule */
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            return $this->sendError('Oc Layout Module not found');
        }

        return $this->sendResponse($ocLayoutModule->toArray(), 'Oc Layout Module retrieved successfully');
    }

    /**
     * Update the specified oc_layout_module in storage.
     * PUT/PATCH /ocLayoutModules/{id}
     *
     * @param  int $id
     * @param Updateoc_layout_moduleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_layout_moduleAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_layout_module $ocLayoutModule */
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            return $this->sendError('Oc Layout Module not found');
        }

        $ocLayoutModule = $this->ocLayoutModuleRepository->update($input, $id);

        return $this->sendResponse($ocLayoutModule->toArray(), 'oc_layout_module updated successfully');
    }

    /**
     * Remove the specified oc_layout_module from storage.
     * DELETE /ocLayoutModules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_layout_module $ocLayoutModule */
        $ocLayoutModule = $this->ocLayoutModuleRepository->findWithoutFail($id);

        if (empty($ocLayoutModule)) {
            return $this->sendError('Oc Layout Module not found');
        }

        $ocLayoutModule->delete();

        return $this->sendResponse($id, 'Oc Layout Module deleted successfully');
    }
}
