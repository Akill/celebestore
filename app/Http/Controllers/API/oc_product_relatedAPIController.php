<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_relatedAPIRequest;
use App\Http\Requests\API\Updateoc_product_relatedAPIRequest;
use App\Models\oc_product_related;
use App\Repositories\oc_product_relatedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_relatedController
 * @package App\Http\Controllers\API
 */

class oc_product_relatedAPIController extends AppBaseController
{
    /** @var  oc_product_relatedRepository */
    private $ocProductRelatedRepository;

    public function __construct(oc_product_relatedRepository $ocProductRelatedRepo)
    {
        $this->ocProductRelatedRepository = $ocProductRelatedRepo;
    }

    /**
     * Display a listing of the oc_product_related.
     * GET|HEAD /ocProductRelateds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRelatedRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductRelatedRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductRelateds = $this->ocProductRelatedRepository->all();

        return $this->sendResponse($ocProductRelateds->toArray(), 'Oc Product Relateds retrieved successfully');
    }

    /**
     * Store a newly created oc_product_related in storage.
     * POST /ocProductRelateds
     *
     * @param Createoc_product_relatedAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_relatedAPIRequest $request)
    {
        $input = $request->all();

        $ocProductRelateds = $this->ocProductRelatedRepository->create($input);

        return $this->sendResponse($ocProductRelateds->toArray(), 'Oc Product Related saved successfully');
    }

    /**
     * Display the specified oc_product_related.
     * GET|HEAD /ocProductRelateds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_related $ocProductRelated */
        $ocProductRelated = $this->ocProductRelatedRepository->findWithoutFail($id);

        if (empty($ocProductRelated)) {
            return $this->sendError('Oc Product Related not found');
        }

        return $this->sendResponse($ocProductRelated->toArray(), 'Oc Product Related retrieved successfully');
    }

    /**
     * Update the specified oc_product_related in storage.
     * PUT/PATCH /ocProductRelateds/{id}
     *
     * @param  int $id
     * @param Updateoc_product_relatedAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_relatedAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_related $ocProductRelated */
        $ocProductRelated = $this->ocProductRelatedRepository->findWithoutFail($id);

        if (empty($ocProductRelated)) {
            return $this->sendError('Oc Product Related not found');
        }

        $ocProductRelated = $this->ocProductRelatedRepository->update($input, $id);

        return $this->sendResponse($ocProductRelated->toArray(), 'oc_product_related updated successfully');
    }

    /**
     * Remove the specified oc_product_related from storage.
     * DELETE /ocProductRelateds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_related $ocProductRelated */
        $ocProductRelated = $this->ocProductRelatedRepository->findWithoutFail($id);

        if (empty($ocProductRelated)) {
            return $this->sendError('Oc Product Related not found');
        }

        $ocProductRelated->delete();

        return $this->sendResponse($id, 'Oc Product Related deleted successfully');
    }
}
