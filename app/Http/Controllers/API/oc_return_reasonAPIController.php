<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_return_reasonAPIRequest;
use App\Http\Requests\API\Updateoc_return_reasonAPIRequest;
use App\Models\oc_return_reason;
use App\Repositories\oc_return_reasonRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_return_reasonController
 * @package App\Http\Controllers\API
 */

class oc_return_reasonAPIController extends AppBaseController
{
    /** @var  oc_return_reasonRepository */
    private $ocReturnReasonRepository;

    public function __construct(oc_return_reasonRepository $ocReturnReasonRepo)
    {
        $this->ocReturnReasonRepository = $ocReturnReasonRepo;
    }

    /**
     * Display a listing of the oc_return_reason.
     * GET|HEAD /ocReturnReasons
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnReasonRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReturnReasonRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReturnReasons = $this->ocReturnReasonRepository->all();

        return $this->sendResponse($ocReturnReasons->toArray(), 'Oc Return Reasons retrieved successfully');
    }

    /**
     * Store a newly created oc_return_reason in storage.
     * POST /ocReturnReasons
     *
     * @param Createoc_return_reasonAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_return_reasonAPIRequest $request)
    {
        $input = $request->all();

        $ocReturnReasons = $this->ocReturnReasonRepository->create($input);

        return $this->sendResponse($ocReturnReasons->toArray(), 'Oc Return Reason saved successfully');
    }

    /**
     * Display the specified oc_return_reason.
     * GET|HEAD /ocReturnReasons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_return_reason $ocReturnReason */
        $ocReturnReason = $this->ocReturnReasonRepository->findWithoutFail($id);

        if (empty($ocReturnReason)) {
            return $this->sendError('Oc Return Reason not found');
        }

        return $this->sendResponse($ocReturnReason->toArray(), 'Oc Return Reason retrieved successfully');
    }

    /**
     * Update the specified oc_return_reason in storage.
     * PUT/PATCH /ocReturnReasons/{id}
     *
     * @param  int $id
     * @param Updateoc_return_reasonAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_return_reasonAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_return_reason $ocReturnReason */
        $ocReturnReason = $this->ocReturnReasonRepository->findWithoutFail($id);

        if (empty($ocReturnReason)) {
            return $this->sendError('Oc Return Reason not found');
        }

        $ocReturnReason = $this->ocReturnReasonRepository->update($input, $id);

        return $this->sendResponse($ocReturnReason->toArray(), 'oc_return_reason updated successfully');
    }

    /**
     * Remove the specified oc_return_reason from storage.
     * DELETE /ocReturnReasons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_return_reason $ocReturnReason */
        $ocReturnReason = $this->ocReturnReasonRepository->findWithoutFail($id);

        if (empty($ocReturnReason)) {
            return $this->sendError('Oc Return Reason not found');
        }

        $ocReturnReason->delete();

        return $this->sendResponse($id, 'Oc Return Reason deleted successfully');
    }
}
