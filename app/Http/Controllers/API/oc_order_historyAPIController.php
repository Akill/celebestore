<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_historyAPIRequest;
use App\Http\Requests\API\Updateoc_order_historyAPIRequest;
use App\Models\oc_order_history;
use App\Repositories\oc_order_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_historyController
 * @package App\Http\Controllers\API
 */

class oc_order_historyAPIController extends AppBaseController
{
    /** @var  oc_order_historyRepository */
    private $ocOrderHistoryRepository;

    public function __construct(oc_order_historyRepository $ocOrderHistoryRepo)
    {
        $this->ocOrderHistoryRepository = $ocOrderHistoryRepo;
    }

    /**
     * Display a listing of the oc_order_history.
     * GET|HEAD /ocOrderHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderHistoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderHistories = $this->ocOrderHistoryRepository->all();

        return $this->sendResponse($ocOrderHistories->toArray(), 'Oc Order Histories retrieved successfully');
    }

    /**
     * Store a newly created oc_order_history in storage.
     * POST /ocOrderHistories
     *
     * @param Createoc_order_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_historyAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderHistories = $this->ocOrderHistoryRepository->create($input);

        return $this->sendResponse($ocOrderHistories->toArray(), 'Oc Order History saved successfully');
    }

    /**
     * Display the specified oc_order_history.
     * GET|HEAD /ocOrderHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_history $ocOrderHistory */
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            return $this->sendError('Oc Order History not found');
        }

        return $this->sendResponse($ocOrderHistory->toArray(), 'Oc Order History retrieved successfully');
    }

    /**
     * Update the specified oc_order_history in storage.
     * PUT/PATCH /ocOrderHistories/{id}
     *
     * @param  int $id
     * @param Updateoc_order_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_history $ocOrderHistory */
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            return $this->sendError('Oc Order History not found');
        }

        $ocOrderHistory = $this->ocOrderHistoryRepository->update($input, $id);

        return $this->sendResponse($ocOrderHistory->toArray(), 'oc_order_history updated successfully');
    }

    /**
     * Remove the specified oc_order_history from storage.
     * DELETE /ocOrderHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_history $ocOrderHistory */
        $ocOrderHistory = $this->ocOrderHistoryRepository->findWithoutFail($id);

        if (empty($ocOrderHistory)) {
            return $this->sendError('Oc Order History not found');
        }

        $ocOrderHistory->delete();

        return $this->sendResponse($id, 'Oc Order History deleted successfully');
    }
}
