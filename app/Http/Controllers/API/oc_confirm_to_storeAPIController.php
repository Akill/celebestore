<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_confirm_to_storeAPIRequest;
use App\Http\Requests\API\Updateoc_confirm_to_storeAPIRequest;
use App\Models\oc_confirm_to_store;
use App\Repositories\oc_confirm_to_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_confirm_to_storeController
 * @package App\Http\Controllers\API
 */

class oc_confirm_to_storeAPIController extends AppBaseController
{
    /** @var  oc_confirm_to_storeRepository */
    private $ocConfirmToStoreRepository;

    public function __construct(oc_confirm_to_storeRepository $ocConfirmToStoreRepo)
    {
        $this->ocConfirmToStoreRepository = $ocConfirmToStoreRepo;
    }

    /**
     * Display a listing of the oc_confirm_to_store.
     * GET|HEAD /ocConfirmToStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocConfirmToStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocConfirmToStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocConfirmToStores = $this->ocConfirmToStoreRepository->all();

        return $this->sendResponse($ocConfirmToStores->toArray(), 'Oc Confirm To Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_confirm_to_store in storage.
     * POST /ocConfirmToStores
     *
     * @param Createoc_confirm_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_confirm_to_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocConfirmToStores = $this->ocConfirmToStoreRepository->create($input);

        return $this->sendResponse($ocConfirmToStores->toArray(), 'Oc Confirm To Store saved successfully');
    }

    /**
     * Display the specified oc_confirm_to_store.
     * GET|HEAD /ocConfirmToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_confirm_to_store $ocConfirmToStore */
        $ocConfirmToStore = $this->ocConfirmToStoreRepository->findWithoutFail($id);

        if (empty($ocConfirmToStore)) {
            return $this->sendError('Oc Confirm To Store not found');
        }

        return $this->sendResponse($ocConfirmToStore->toArray(), 'Oc Confirm To Store retrieved successfully');
    }

    /**
     * Update the specified oc_confirm_to_store in storage.
     * PUT/PATCH /ocConfirmToStores/{id}
     *
     * @param  int $id
     * @param Updateoc_confirm_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_confirm_to_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_confirm_to_store $ocConfirmToStore */
        $ocConfirmToStore = $this->ocConfirmToStoreRepository->findWithoutFail($id);

        if (empty($ocConfirmToStore)) {
            return $this->sendError('Oc Confirm To Store not found');
        }

        $ocConfirmToStore = $this->ocConfirmToStoreRepository->update($input, $id);

        return $this->sendResponse($ocConfirmToStore->toArray(), 'oc_confirm_to_store updated successfully');
    }

    /**
     * Remove the specified oc_confirm_to_store from storage.
     * DELETE /ocConfirmToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_confirm_to_store $ocConfirmToStore */
        $ocConfirmToStore = $this->ocConfirmToStoreRepository->findWithoutFail($id);

        if (empty($ocConfirmToStore)) {
            return $this->sendError('Oc Confirm To Store not found');
        }

        $ocConfirmToStore->delete();

        return $this->sendResponse($id, 'Oc Confirm To Store deleted successfully');
    }
}
