<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_category_filterAPIRequest;
use App\Http\Requests\API\Updateoc_category_filterAPIRequest;
use App\Models\oc_category_filter;
use App\Repositories\oc_category_filterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_category_filterController
 * @package App\Http\Controllers\API
 */

class oc_category_filterAPIController extends AppBaseController
{
    /** @var  oc_category_filterRepository */
    private $ocCategoryFilterRepository;

    public function __construct(oc_category_filterRepository $ocCategoryFilterRepo)
    {
        $this->ocCategoryFilterRepository = $ocCategoryFilterRepo;
    }

    /**
     * Display a listing of the oc_category_filter.
     * GET|HEAD /ocCategoryFilters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryFilterRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryFilterRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategoryFilters = $this->ocCategoryFilterRepository->all();

        return $this->sendResponse($ocCategoryFilters->toArray(), 'Oc Category Filters retrieved successfully');
    }

    /**
     * Store a newly created oc_category_filter in storage.
     * POST /ocCategoryFilters
     *
     * @param Createoc_category_filterAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_filterAPIRequest $request)
    {
        $input = $request->all();

        $ocCategoryFilters = $this->ocCategoryFilterRepository->create($input);

        return $this->sendResponse($ocCategoryFilters->toArray(), 'Oc Category Filter saved successfully');
    }

    /**
     * Display the specified oc_category_filter.
     * GET|HEAD /ocCategoryFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category_filter $ocCategoryFilter */
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            return $this->sendError('Oc Category Filter not found');
        }

        return $this->sendResponse($ocCategoryFilter->toArray(), 'Oc Category Filter retrieved successfully');
    }

    /**
     * Update the specified oc_category_filter in storage.
     * PUT/PATCH /ocCategoryFilters/{id}
     *
     * @param  int $id
     * @param Updateoc_category_filterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_filterAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category_filter $ocCategoryFilter */
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            return $this->sendError('Oc Category Filter not found');
        }

        $ocCategoryFilter = $this->ocCategoryFilterRepository->update($input, $id);

        return $this->sendResponse($ocCategoryFilter->toArray(), 'oc_category_filter updated successfully');
    }

    /**
     * Remove the specified oc_category_filter from storage.
     * DELETE /ocCategoryFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category_filter $ocCategoryFilter */
        $ocCategoryFilter = $this->ocCategoryFilterRepository->findWithoutFail($id);

        if (empty($ocCategoryFilter)) {
            return $this->sendError('Oc Category Filter not found');
        }

        $ocCategoryFilter->delete();

        return $this->sendResponse($id, 'Oc Category Filter deleted successfully');
    }
}
