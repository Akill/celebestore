<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_weight_class_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_weight_class_descriptionAPIRequest;
use App\Models\oc_weight_class_description;
use App\Repositories\oc_weight_class_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_weight_class_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_weight_class_descriptionAPIController extends AppBaseController
{
    /** @var  oc_weight_class_descriptionRepository */
    private $ocWeightClassDescriptionRepository;

    public function __construct(oc_weight_class_descriptionRepository $ocWeightClassDescriptionRepo)
    {
        $this->ocWeightClassDescriptionRepository = $ocWeightClassDescriptionRepo;
    }

    /**
     * Display a listing of the oc_weight_class_description.
     * GET|HEAD /ocWeightClassDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocWeightClassDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocWeightClassDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocWeightClassDescriptions = $this->ocWeightClassDescriptionRepository->all();

        return $this->sendResponse($ocWeightClassDescriptions->toArray(), 'Oc Weight Class Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_weight_class_description in storage.
     * POST /ocWeightClassDescriptions
     *
     * @param Createoc_weight_class_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_weight_class_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocWeightClassDescriptions = $this->ocWeightClassDescriptionRepository->create($input);

        return $this->sendResponse($ocWeightClassDescriptions->toArray(), 'Oc Weight Class Description saved successfully');
    }

    /**
     * Display the specified oc_weight_class_description.
     * GET|HEAD /ocWeightClassDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_weight_class_description $ocWeightClassDescription */
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            return $this->sendError('Oc Weight Class Description not found');
        }

        return $this->sendResponse($ocWeightClassDescription->toArray(), 'Oc Weight Class Description retrieved successfully');
    }

    /**
     * Update the specified oc_weight_class_description in storage.
     * PUT/PATCH /ocWeightClassDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_weight_class_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_weight_class_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_weight_class_description $ocWeightClassDescription */
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            return $this->sendError('Oc Weight Class Description not found');
        }

        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocWeightClassDescription->toArray(), 'oc_weight_class_description updated successfully');
    }

    /**
     * Remove the specified oc_weight_class_description from storage.
     * DELETE /ocWeightClassDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_weight_class_description $ocWeightClassDescription */
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            return $this->sendError('Oc Weight Class Description not found');
        }

        $ocWeightClassDescription->delete();

        return $this->sendResponse($id, 'Oc Weight Class Description deleted successfully');
    }
}
