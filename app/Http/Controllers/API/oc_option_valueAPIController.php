<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_option_valueAPIRequest;
use App\Http\Requests\API\Updateoc_option_valueAPIRequest;
use App\Models\oc_option_value;
use App\Repositories\oc_option_valueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_option_valueController
 * @package App\Http\Controllers\API
 */

class oc_option_valueAPIController extends AppBaseController
{
    /** @var  oc_option_valueRepository */
    private $ocOptionValueRepository;

    public function __construct(oc_option_valueRepository $ocOptionValueRepo)
    {
        $this->ocOptionValueRepository = $ocOptionValueRepo;
    }

    /**
     * Display a listing of the oc_option_value.
     * GET|HEAD /ocOptionValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionValueRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOptionValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOptionValues = $this->ocOptionValueRepository->all();

        return $this->sendResponse($ocOptionValues->toArray(), 'Oc Option Values retrieved successfully');
    }

    /**
     * Store a newly created oc_option_value in storage.
     * POST /ocOptionValues
     *
     * @param Createoc_option_valueAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_option_valueAPIRequest $request)
    {
        $input = $request->all();

        $ocOptionValues = $this->ocOptionValueRepository->create($input);

        return $this->sendResponse($ocOptionValues->toArray(), 'Oc Option Value saved successfully');
    }

    /**
     * Display the specified oc_option_value.
     * GET|HEAD /ocOptionValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_option_value $ocOptionValue */
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            return $this->sendError('Oc Option Value not found');
        }

        return $this->sendResponse($ocOptionValue->toArray(), 'Oc Option Value retrieved successfully');
    }

    /**
     * Update the specified oc_option_value in storage.
     * PUT/PATCH /ocOptionValues/{id}
     *
     * @param  int $id
     * @param Updateoc_option_valueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_option_valueAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_option_value $ocOptionValue */
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            return $this->sendError('Oc Option Value not found');
        }

        $ocOptionValue = $this->ocOptionValueRepository->update($input, $id);

        return $this->sendResponse($ocOptionValue->toArray(), 'oc_option_value updated successfully');
    }

    /**
     * Remove the specified oc_option_value from storage.
     * DELETE /ocOptionValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_option_value $ocOptionValue */
        $ocOptionValue = $this->ocOptionValueRepository->findWithoutFail($id);

        if (empty($ocOptionValue)) {
            return $this->sendError('Oc Option Value not found');
        }

        $ocOptionValue->delete();

        return $this->sendResponse($id, 'Oc Option Value deleted successfully');
    }
}
