<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_option_value_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_option_value_descriptionAPIRequest;
use App\Models\oc_option_value_description;
use App\Repositories\oc_option_value_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_option_value_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_option_value_descriptionAPIController extends AppBaseController
{
    /** @var  oc_option_value_descriptionRepository */
    private $ocOptionValueDescriptionRepository;

    public function __construct(oc_option_value_descriptionRepository $ocOptionValueDescriptionRepo)
    {
        $this->ocOptionValueDescriptionRepository = $ocOptionValueDescriptionRepo;
    }

    /**
     * Display a listing of the oc_option_value_description.
     * GET|HEAD /ocOptionValueDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionValueDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOptionValueDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOptionValueDescriptions = $this->ocOptionValueDescriptionRepository->all();

        return $this->sendResponse($ocOptionValueDescriptions->toArray(), 'Oc Option Value Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_option_value_description in storage.
     * POST /ocOptionValueDescriptions
     *
     * @param Createoc_option_value_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_option_value_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocOptionValueDescriptions = $this->ocOptionValueDescriptionRepository->create($input);

        return $this->sendResponse($ocOptionValueDescriptions->toArray(), 'Oc Option Value Description saved successfully');
    }

    /**
     * Display the specified oc_option_value_description.
     * GET|HEAD /ocOptionValueDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_option_value_description $ocOptionValueDescription */
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            return $this->sendError('Oc Option Value Description not found');
        }

        return $this->sendResponse($ocOptionValueDescription->toArray(), 'Oc Option Value Description retrieved successfully');
    }

    /**
     * Update the specified oc_option_value_description in storage.
     * PUT/PATCH /ocOptionValueDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_option_value_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_option_value_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_option_value_description $ocOptionValueDescription */
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            return $this->sendError('Oc Option Value Description not found');
        }

        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocOptionValueDescription->toArray(), 'oc_option_value_description updated successfully');
    }

    /**
     * Remove the specified oc_option_value_description from storage.
     * DELETE /ocOptionValueDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_option_value_description $ocOptionValueDescription */
        $ocOptionValueDescription = $this->ocOptionValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocOptionValueDescription)) {
            return $this->sendError('Oc Option Value Description not found');
        }

        $ocOptionValueDescription->delete();

        return $this->sendResponse($id, 'Oc Option Value Description deleted successfully');
    }
}
