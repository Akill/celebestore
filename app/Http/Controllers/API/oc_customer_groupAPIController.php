<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_groupAPIRequest;
use App\Http\Requests\API\Updateoc_customer_groupAPIRequest;
use App\Models\oc_customer_group;
use App\Repositories\oc_customer_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_groupController
 * @package App\Http\Controllers\API
 */

class oc_customer_groupAPIController extends AppBaseController
{
    /** @var  oc_customer_groupRepository */
    private $ocCustomerGroupRepository;

    public function __construct(oc_customer_groupRepository $ocCustomerGroupRepo)
    {
        $this->ocCustomerGroupRepository = $ocCustomerGroupRepo;
    }

    /**
     * Display a listing of the oc_customer_group.
     * GET|HEAD /ocCustomerGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerGroups = $this->ocCustomerGroupRepository->all();

        return $this->sendResponse($ocCustomerGroups->toArray(), 'Oc Customer Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_group in storage.
     * POST /ocCustomerGroups
     *
     * @param Createoc_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerGroups = $this->ocCustomerGroupRepository->create($input);

        return $this->sendResponse($ocCustomerGroups->toArray(), 'Oc Customer Group saved successfully');
    }

    /**
     * Display the specified oc_customer_group.
     * GET|HEAD /ocCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_group $ocCustomerGroup */
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            return $this->sendError('Oc Customer Group not found');
        }

        return $this->sendResponse($ocCustomerGroup->toArray(), 'Oc Customer Group retrieved successfully');
    }

    /**
     * Update the specified oc_customer_group in storage.
     * PUT/PATCH /ocCustomerGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_group $ocCustomerGroup */
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            return $this->sendError('Oc Customer Group not found');
        }

        $ocCustomerGroup = $this->ocCustomerGroupRepository->update($input, $id);

        return $this->sendResponse($ocCustomerGroup->toArray(), 'oc_customer_group updated successfully');
    }

    /**
     * Remove the specified oc_customer_group from storage.
     * DELETE /ocCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_group $ocCustomerGroup */
        $ocCustomerGroup = $this->ocCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomerGroup)) {
            return $this->sendError('Oc Customer Group not found');
        }

        $ocCustomerGroup->delete();

        return $this->sendResponse($id, 'Oc Customer Group deleted successfully');
    }
}
