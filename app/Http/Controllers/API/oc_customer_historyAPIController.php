<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_historyAPIRequest;
use App\Http\Requests\API\Updateoc_customer_historyAPIRequest;
use App\Models\oc_customer_history;
use App\Repositories\oc_customer_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_historyController
 * @package App\Http\Controllers\API
 */

class oc_customer_historyAPIController extends AppBaseController
{
    /** @var  oc_customer_historyRepository */
    private $ocCustomerHistoryRepository;

    public function __construct(oc_customer_historyRepository $ocCustomerHistoryRepo)
    {
        $this->ocCustomerHistoryRepository = $ocCustomerHistoryRepo;
    }

    /**
     * Display a listing of the oc_customer_history.
     * GET|HEAD /ocCustomerHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerHistoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerHistories = $this->ocCustomerHistoryRepository->all();

        return $this->sendResponse($ocCustomerHistories->toArray(), 'Oc Customer Histories retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_history in storage.
     * POST /ocCustomerHistories
     *
     * @param Createoc_customer_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_historyAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerHistories = $this->ocCustomerHistoryRepository->create($input);

        return $this->sendResponse($ocCustomerHistories->toArray(), 'Oc Customer History saved successfully');
    }

    /**
     * Display the specified oc_customer_history.
     * GET|HEAD /ocCustomerHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_history $ocCustomerHistory */
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            return $this->sendError('Oc Customer History not found');
        }

        return $this->sendResponse($ocCustomerHistory->toArray(), 'Oc Customer History retrieved successfully');
    }

    /**
     * Update the specified oc_customer_history in storage.
     * PUT/PATCH /ocCustomerHistories/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_history $ocCustomerHistory */
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            return $this->sendError('Oc Customer History not found');
        }

        $ocCustomerHistory = $this->ocCustomerHistoryRepository->update($input, $id);

        return $this->sendResponse($ocCustomerHistory->toArray(), 'oc_customer_history updated successfully');
    }

    /**
     * Remove the specified oc_customer_history from storage.
     * DELETE /ocCustomerHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_history $ocCustomerHistory */
        $ocCustomerHistory = $this->ocCustomerHistoryRepository->findWithoutFail($id);

        if (empty($ocCustomerHistory)) {
            return $this->sendError('Oc Customer History not found');
        }

        $ocCustomerHistory->delete();

        return $this->sendResponse($id, 'Oc Customer History deleted successfully');
    }
}
