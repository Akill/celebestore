<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_recurring_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_recurring_descriptionAPIRequest;
use App\Models\oc_recurring_description;
use App\Repositories\oc_recurring_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_recurring_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_recurring_descriptionAPIController extends AppBaseController
{
    /** @var  oc_recurring_descriptionRepository */
    private $ocRecurringDescriptionRepository;

    public function __construct(oc_recurring_descriptionRepository $ocRecurringDescriptionRepo)
    {
        $this->ocRecurringDescriptionRepository = $ocRecurringDescriptionRepo;
    }

    /**
     * Display a listing of the oc_recurring_description.
     * GET|HEAD /ocRecurringDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocRecurringDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocRecurringDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocRecurringDescriptions = $this->ocRecurringDescriptionRepository->all();

        return $this->sendResponse($ocRecurringDescriptions->toArray(), 'Oc Recurring Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_recurring_description in storage.
     * POST /ocRecurringDescriptions
     *
     * @param Createoc_recurring_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_recurring_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocRecurringDescriptions = $this->ocRecurringDescriptionRepository->create($input);

        return $this->sendResponse($ocRecurringDescriptions->toArray(), 'Oc Recurring Description saved successfully');
    }

    /**
     * Display the specified oc_recurring_description.
     * GET|HEAD /ocRecurringDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_recurring_description $ocRecurringDescription */
        $ocRecurringDescription = $this->ocRecurringDescriptionRepository->findWithoutFail($id);

        if (empty($ocRecurringDescription)) {
            return $this->sendError('Oc Recurring Description not found');
        }

        return $this->sendResponse($ocRecurringDescription->toArray(), 'Oc Recurring Description retrieved successfully');
    }

    /**
     * Update the specified oc_recurring_description in storage.
     * PUT/PATCH /ocRecurringDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_recurring_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_recurring_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_recurring_description $ocRecurringDescription */
        $ocRecurringDescription = $this->ocRecurringDescriptionRepository->findWithoutFail($id);

        if (empty($ocRecurringDescription)) {
            return $this->sendError('Oc Recurring Description not found');
        }

        $ocRecurringDescription = $this->ocRecurringDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocRecurringDescription->toArray(), 'oc_recurring_description updated successfully');
    }

    /**
     * Remove the specified oc_recurring_description from storage.
     * DELETE /ocRecurringDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_recurring_description $ocRecurringDescription */
        $ocRecurringDescription = $this->ocRecurringDescriptionRepository->findWithoutFail($id);

        if (empty($ocRecurringDescription)) {
            return $this->sendError('Oc Recurring Description not found');
        }

        $ocRecurringDescription->delete();

        return $this->sendResponse($id, 'Oc Recurring Description deleted successfully');
    }
}
