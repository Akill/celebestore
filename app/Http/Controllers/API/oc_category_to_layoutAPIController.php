<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_category_to_layoutAPIRequest;
use App\Http\Requests\API\Updateoc_category_to_layoutAPIRequest;
use App\Models\oc_category_to_layout;
use App\Repositories\oc_category_to_layoutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_category_to_layoutController
 * @package App\Http\Controllers\API
 */

class oc_category_to_layoutAPIController extends AppBaseController
{
    /** @var  oc_category_to_layoutRepository */
    private $ocCategoryToLayoutRepository;

    public function __construct(oc_category_to_layoutRepository $ocCategoryToLayoutRepo)
    {
        $this->ocCategoryToLayoutRepository = $ocCategoryToLayoutRepo;
    }

    /**
     * Display a listing of the oc_category_to_layout.
     * GET|HEAD /ocCategoryToLayouts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryToLayoutRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryToLayoutRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategoryToLayouts = $this->ocCategoryToLayoutRepository->all();

        return $this->sendResponse($ocCategoryToLayouts->toArray(), 'Oc Category To Layouts retrieved successfully');
    }

    /**
     * Store a newly created oc_category_to_layout in storage.
     * POST /ocCategoryToLayouts
     *
     * @param Createoc_category_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        $ocCategoryToLayouts = $this->ocCategoryToLayoutRepository->create($input);

        return $this->sendResponse($ocCategoryToLayouts->toArray(), 'Oc Category To Layout saved successfully');
    }

    /**
     * Display the specified oc_category_to_layout.
     * GET|HEAD /ocCategoryToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category_to_layout $ocCategoryToLayout */
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            return $this->sendError('Oc Category To Layout not found');
        }

        return $this->sendResponse($ocCategoryToLayout->toArray(), 'Oc Category To Layout retrieved successfully');
    }

    /**
     * Update the specified oc_category_to_layout in storage.
     * PUT/PATCH /ocCategoryToLayouts/{id}
     *
     * @param  int $id
     * @param Updateoc_category_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category_to_layout $ocCategoryToLayout */
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            return $this->sendError('Oc Category To Layout not found');
        }

        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->update($input, $id);

        return $this->sendResponse($ocCategoryToLayout->toArray(), 'oc_category_to_layout updated successfully');
    }

    /**
     * Remove the specified oc_category_to_layout from storage.
     * DELETE /ocCategoryToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category_to_layout $ocCategoryToLayout */
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            return $this->sendError('Oc Category To Layout not found');
        }

        $ocCategoryToLayout->delete();

        return $this->sendResponse($id, 'Oc Category To Layout deleted successfully');
    }
}
