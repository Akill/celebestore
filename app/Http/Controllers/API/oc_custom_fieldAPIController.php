<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_custom_fieldAPIRequest;
use App\Http\Requests\API\Updateoc_custom_fieldAPIRequest;
use App\Models\oc_custom_field;
use App\Repositories\oc_custom_fieldRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_custom_fieldController
 * @package App\Http\Controllers\API
 */

class oc_custom_fieldAPIController extends AppBaseController
{
    /** @var  oc_custom_fieldRepository */
    private $ocCustomFieldRepository;

    public function __construct(oc_custom_fieldRepository $ocCustomFieldRepo)
    {
        $this->ocCustomFieldRepository = $ocCustomFieldRepo;
    }

    /**
     * Display a listing of the oc_custom_field.
     * GET|HEAD /ocCustomFields
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomFieldRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomFields = $this->ocCustomFieldRepository->all();

        return $this->sendResponse($ocCustomFields->toArray(), 'Oc Custom Fields retrieved successfully');
    }

    /**
     * Store a newly created oc_custom_field in storage.
     * POST /ocCustomFields
     *
     * @param Createoc_custom_fieldAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_fieldAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomFields = $this->ocCustomFieldRepository->create($input);

        return $this->sendResponse($ocCustomFields->toArray(), 'Oc Custom Field saved successfully');
    }

    /**
     * Display the specified oc_custom_field.
     * GET|HEAD /ocCustomFields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_custom_field $ocCustomField */
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            return $this->sendError('Oc Custom Field not found');
        }

        return $this->sendResponse($ocCustomField->toArray(), 'Oc Custom Field retrieved successfully');
    }

    /**
     * Update the specified oc_custom_field in storage.
     * PUT/PATCH /ocCustomFields/{id}
     *
     * @param  int $id
     * @param Updateoc_custom_fieldAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_fieldAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_custom_field $ocCustomField */
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            return $this->sendError('Oc Custom Field not found');
        }

        $ocCustomField = $this->ocCustomFieldRepository->update($input, $id);

        return $this->sendResponse($ocCustomField->toArray(), 'oc_custom_field updated successfully');
    }

    /**
     * Remove the specified oc_custom_field from storage.
     * DELETE /ocCustomFields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_custom_field $ocCustomField */
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            return $this->sendError('Oc Custom Field not found');
        }

        $ocCustomField->delete();

        return $this->sendResponse($id, 'Oc Custom Field deleted successfully');
    }
}
