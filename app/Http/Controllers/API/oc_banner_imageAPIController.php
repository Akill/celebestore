<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_banner_imageAPIRequest;
use App\Http\Requests\API\Updateoc_banner_imageAPIRequest;
use App\Models\oc_banner_image;
use App\Repositories\oc_banner_imageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_banner_imageController
 * @package App\Http\Controllers\API
 */

class oc_banner_imageAPIController extends AppBaseController
{
    /** @var  oc_banner_imageRepository */
    private $ocBannerImageRepository;

    public function __construct(oc_banner_imageRepository $ocBannerImageRepo)
    {
        $this->ocBannerImageRepository = $ocBannerImageRepo;
    }

    /**
     * Display a listing of the oc_banner_image.
     * GET|HEAD /ocBannerImages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerImageRepository->pushCriteria(new RequestCriteria($request));
        $this->ocBannerImageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocBannerImages = $this->ocBannerImageRepository->all();

        return $this->sendResponse($ocBannerImages->toArray(), 'Oc Banner Images retrieved successfully');
    }

    /**
     * Store a newly created oc_banner_image in storage.
     * POST /ocBannerImages
     *
     * @param Createoc_banner_imageAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_banner_imageAPIRequest $request)
    {
        $input = $request->all();

        $ocBannerImages = $this->ocBannerImageRepository->create($input);

        return $this->sendResponse($ocBannerImages->toArray(), 'Oc Banner Image saved successfully');
    }

    /**
     * Display the specified oc_banner_image.
     * GET|HEAD /ocBannerImages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_banner_image $ocBannerImage */
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            return $this->sendError('Oc Banner Image not found');
        }

        return $this->sendResponse($ocBannerImage->toArray(), 'Oc Banner Image retrieved successfully');
    }

    /**
     * Update the specified oc_banner_image in storage.
     * PUT/PATCH /ocBannerImages/{id}
     *
     * @param  int $id
     * @param Updateoc_banner_imageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_banner_imageAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_banner_image $ocBannerImage */
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            return $this->sendError('Oc Banner Image not found');
        }

        $ocBannerImage = $this->ocBannerImageRepository->update($input, $id);

        return $this->sendResponse($ocBannerImage->toArray(), 'oc_banner_image updated successfully');
    }

    /**
     * Remove the specified oc_banner_image from storage.
     * DELETE /ocBannerImages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_banner_image $ocBannerImage */
        $ocBannerImage = $this->ocBannerImageRepository->findWithoutFail($id);

        if (empty($ocBannerImage)) {
            return $this->sendError('Oc Banner Image not found');
        }

        $ocBannerImage->delete();

        return $this->sendResponse($id, 'Oc Banner Image deleted successfully');
    }
}
