<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_tax_rate_to_customer_groupAPIRequest;
use App\Http\Requests\API\Updateoc_tax_rate_to_customer_groupAPIRequest;
use App\Models\oc_tax_rate_to_customer_group;
use App\Repositories\oc_tax_rate_to_customer_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_tax_rate_to_customer_groupController
 * @package App\Http\Controllers\API
 */

class oc_tax_rate_to_customer_groupAPIController extends AppBaseController
{
    /** @var  oc_tax_rate_to_customer_groupRepository */
    private $ocTaxRateToCustomerGroupRepository;

    public function __construct(oc_tax_rate_to_customer_groupRepository $ocTaxRateToCustomerGroupRepo)
    {
        $this->ocTaxRateToCustomerGroupRepository = $ocTaxRateToCustomerGroupRepo;
    }

    /**
     * Display a listing of the oc_tax_rate_to_customer_group.
     * GET|HEAD /ocTaxRateToCustomerGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxRateToCustomerGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocTaxRateToCustomerGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocTaxRateToCustomerGroups = $this->ocTaxRateToCustomerGroupRepository->all();

        return $this->sendResponse($ocTaxRateToCustomerGroups->toArray(), 'Oc Tax Rate To Customer Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_tax_rate_to_customer_group in storage.
     * POST /ocTaxRateToCustomerGroups
     *
     * @param Createoc_tax_rate_to_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_rate_to_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocTaxRateToCustomerGroups = $this->ocTaxRateToCustomerGroupRepository->create($input);

        return $this->sendResponse($ocTaxRateToCustomerGroups->toArray(), 'Oc Tax Rate To Customer Group saved successfully');
    }

    /**
     * Display the specified oc_tax_rate_to_customer_group.
     * GET|HEAD /ocTaxRateToCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_tax_rate_to_customer_group $ocTaxRateToCustomerGroup */
        $ocTaxRateToCustomerGroup = $this->ocTaxRateToCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocTaxRateToCustomerGroup)) {
            return $this->sendError('Oc Tax Rate To Customer Group not found');
        }

        return $this->sendResponse($ocTaxRateToCustomerGroup->toArray(), 'Oc Tax Rate To Customer Group retrieved successfully');
    }

    /**
     * Update the specified oc_tax_rate_to_customer_group in storage.
     * PUT/PATCH /ocTaxRateToCustomerGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_tax_rate_to_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_rate_to_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_tax_rate_to_customer_group $ocTaxRateToCustomerGroup */
        $ocTaxRateToCustomerGroup = $this->ocTaxRateToCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocTaxRateToCustomerGroup)) {
            return $this->sendError('Oc Tax Rate To Customer Group not found');
        }

        $ocTaxRateToCustomerGroup = $this->ocTaxRateToCustomerGroupRepository->update($input, $id);

        return $this->sendResponse($ocTaxRateToCustomerGroup->toArray(), 'oc_tax_rate_to_customer_group updated successfully');
    }

    /**
     * Remove the specified oc_tax_rate_to_customer_group from storage.
     * DELETE /ocTaxRateToCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_tax_rate_to_customer_group $ocTaxRateToCustomerGroup */
        $ocTaxRateToCustomerGroup = $this->ocTaxRateToCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocTaxRateToCustomerGroup)) {
            return $this->sendError('Oc Tax Rate To Customer Group not found');
        }

        $ocTaxRateToCustomerGroup->delete();

        return $this->sendResponse($id, 'Oc Tax Rate To Customer Group deleted successfully');
    }
}
