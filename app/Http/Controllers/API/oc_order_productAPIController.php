<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_productAPIRequest;
use App\Http\Requests\API\Updateoc_order_productAPIRequest;
use App\Models\oc_order_product;
use App\Repositories\oc_order_productRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_productController
 * @package App\Http\Controllers\API
 */

class oc_order_productAPIController extends AppBaseController
{
    /** @var  oc_order_productRepository */
    private $ocOrderProductRepository;

    public function __construct(oc_order_productRepository $ocOrderProductRepo)
    {
        $this->ocOrderProductRepository = $ocOrderProductRepo;
    }

    /**
     * Display a listing of the oc_order_product.
     * GET|HEAD /ocOrderProducts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderProductRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderProductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderProducts = $this->ocOrderProductRepository->all();

        return $this->sendResponse($ocOrderProducts->toArray(), 'Oc Order Products retrieved successfully');
    }

    /**
     * Store a newly created oc_order_product in storage.
     * POST /ocOrderProducts
     *
     * @param Createoc_order_productAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_productAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderProducts = $this->ocOrderProductRepository->create($input);

        return $this->sendResponse($ocOrderProducts->toArray(), 'Oc Order Product saved successfully');
    }

    /**
     * Display the specified oc_order_product.
     * GET|HEAD /ocOrderProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_product $ocOrderProduct */
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            return $this->sendError('Oc Order Product not found');
        }

        return $this->sendResponse($ocOrderProduct->toArray(), 'Oc Order Product retrieved successfully');
    }

    /**
     * Update the specified oc_order_product in storage.
     * PUT/PATCH /ocOrderProducts/{id}
     *
     * @param  int $id
     * @param Updateoc_order_productAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_productAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_product $ocOrderProduct */
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            return $this->sendError('Oc Order Product not found');
        }

        $ocOrderProduct = $this->ocOrderProductRepository->update($input, $id);

        return $this->sendResponse($ocOrderProduct->toArray(), 'oc_order_product updated successfully');
    }

    /**
     * Remove the specified oc_order_product from storage.
     * DELETE /ocOrderProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_product $ocOrderProduct */
        $ocOrderProduct = $this->ocOrderProductRepository->findWithoutFail($id);

        if (empty($ocOrderProduct)) {
            return $this->sendError('Oc Order Product not found');
        }

        $ocOrderProduct->delete();

        return $this->sendResponse($id, 'Oc Order Product deleted successfully');
    }
}
