<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_url_aliasAPIRequest;
use App\Http\Requests\API\Updateoc_url_aliasAPIRequest;
use App\Models\oc_url_alias;
use App\Repositories\oc_url_aliasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_url_aliasController
 * @package App\Http\Controllers\API
 */

class oc_url_aliasAPIController extends AppBaseController
{
    /** @var  oc_url_aliasRepository */
    private $ocUrlAliasRepository;

    public function __construct(oc_url_aliasRepository $ocUrlAliasRepo)
    {
        $this->ocUrlAliasRepository = $ocUrlAliasRepo;
    }

    /**
     * Display a listing of the oc_url_alias.
     * GET|HEAD /ocUrlAliases
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUrlAliasRepository->pushCriteria(new RequestCriteria($request));
        $this->ocUrlAliasRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocUrlAliases = $this->ocUrlAliasRepository->all();

        return $this->sendResponse($ocUrlAliases->toArray(), 'Oc Url Aliases retrieved successfully');
    }

    /**
     * Store a newly created oc_url_alias in storage.
     * POST /ocUrlAliases
     *
     * @param Createoc_url_aliasAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_url_aliasAPIRequest $request)
    {
        $input = $request->all();

        $ocUrlAliases = $this->ocUrlAliasRepository->create($input);

        return $this->sendResponse($ocUrlAliases->toArray(), 'Oc Url Alias saved successfully');
    }

    /**
     * Display the specified oc_url_alias.
     * GET|HEAD /ocUrlAliases/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_url_alias $ocUrlAlias */
        $ocUrlAlias = $this->ocUrlAliasRepository->findWithoutFail($id);

        if (empty($ocUrlAlias)) {
            return $this->sendError('Oc Url Alias not found');
        }

        return $this->sendResponse($ocUrlAlias->toArray(), 'Oc Url Alias retrieved successfully');
    }

    /**
     * Update the specified oc_url_alias in storage.
     * PUT/PATCH /ocUrlAliases/{id}
     *
     * @param  int $id
     * @param Updateoc_url_aliasAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_url_aliasAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_url_alias $ocUrlAlias */
        $ocUrlAlias = $this->ocUrlAliasRepository->findWithoutFail($id);

        if (empty($ocUrlAlias)) {
            return $this->sendError('Oc Url Alias not found');
        }

        $ocUrlAlias = $this->ocUrlAliasRepository->update($input, $id);

        return $this->sendResponse($ocUrlAlias->toArray(), 'oc_url_alias updated successfully');
    }

    /**
     * Remove the specified oc_url_alias from storage.
     * DELETE /ocUrlAliases/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_url_alias $ocUrlAlias */
        $ocUrlAlias = $this->ocUrlAliasRepository->findWithoutFail($id);

        if (empty($ocUrlAlias)) {
            return $this->sendError('Oc Url Alias not found');
        }

        $ocUrlAlias->delete();

        return $this->sendResponse($id, 'Oc Url Alias deleted successfully');
    }
}
