<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_attributeAPIRequest;
use App\Http\Requests\API\Updateoc_product_attributeAPIRequest;
use App\Models\oc_product_attribute;
use App\Repositories\oc_product_attributeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_attributeController
 * @package App\Http\Controllers\API
 */

class oc_product_attributeAPIController extends AppBaseController
{
    /** @var  oc_product_attributeRepository */
    private $ocProductAttributeRepository;

    public function __construct(oc_product_attributeRepository $ocProductAttributeRepo)
    {
        $this->ocProductAttributeRepository = $ocProductAttributeRepo;
    }

    /**
     * Display a listing of the oc_product_attribute.
     * GET|HEAD /ocProductAttributes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductAttributeRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductAttributeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductAttributes = $this->ocProductAttributeRepository->all();

        return $this->sendResponse($ocProductAttributes->toArray(), 'Oc Product Attributes retrieved successfully');
    }

    /**
     * Store a newly created oc_product_attribute in storage.
     * POST /ocProductAttributes
     *
     * @param Createoc_product_attributeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_attributeAPIRequest $request)
    {
        $input = $request->all();

        $ocProductAttributes = $this->ocProductAttributeRepository->create($input);

        return $this->sendResponse($ocProductAttributes->toArray(), 'Oc Product Attribute saved successfully');
    }

    /**
     * Display the specified oc_product_attribute.
     * GET|HEAD /ocProductAttributes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_attribute $ocProductAttribute */
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            return $this->sendError('Oc Product Attribute not found');
        }

        return $this->sendResponse($ocProductAttribute->toArray(), 'Oc Product Attribute retrieved successfully');
    }

    /**
     * Update the specified oc_product_attribute in storage.
     * PUT/PATCH /ocProductAttributes/{id}
     *
     * @param  int $id
     * @param Updateoc_product_attributeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_attributeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_attribute $ocProductAttribute */
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            return $this->sendError('Oc Product Attribute not found');
        }

        $ocProductAttribute = $this->ocProductAttributeRepository->update($input, $id);

        return $this->sendResponse($ocProductAttribute->toArray(), 'oc_product_attribute updated successfully');
    }

    /**
     * Remove the specified oc_product_attribute from storage.
     * DELETE /ocProductAttributes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_attribute $ocProductAttribute */
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            return $this->sendError('Oc Product Attribute not found');
        }

        $ocProductAttribute->delete();

        return $this->sendResponse($id, 'Oc Product Attribute deleted successfully');
    }
}
