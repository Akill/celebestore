<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_tax_ruleAPIRequest;
use App\Http\Requests\API\Updateoc_tax_ruleAPIRequest;
use App\Models\oc_tax_rule;
use App\Repositories\oc_tax_ruleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_tax_ruleController
 * @package App\Http\Controllers\API
 */

class oc_tax_ruleAPIController extends AppBaseController
{
    /** @var  oc_tax_ruleRepository */
    private $ocTaxRuleRepository;

    public function __construct(oc_tax_ruleRepository $ocTaxRuleRepo)
    {
        $this->ocTaxRuleRepository = $ocTaxRuleRepo;
    }

    /**
     * Display a listing of the oc_tax_rule.
     * GET|HEAD /ocTaxRules
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxRuleRepository->pushCriteria(new RequestCriteria($request));
        $this->ocTaxRuleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocTaxRules = $this->ocTaxRuleRepository->all();

        return $this->sendResponse($ocTaxRules->toArray(), 'Oc Tax Rules retrieved successfully');
    }

    /**
     * Store a newly created oc_tax_rule in storage.
     * POST /ocTaxRules
     *
     * @param Createoc_tax_ruleAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_ruleAPIRequest $request)
    {
        $input = $request->all();

        $ocTaxRules = $this->ocTaxRuleRepository->create($input);

        return $this->sendResponse($ocTaxRules->toArray(), 'Oc Tax Rule saved successfully');
    }

    /**
     * Display the specified oc_tax_rule.
     * GET|HEAD /ocTaxRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_tax_rule $ocTaxRule */
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            return $this->sendError('Oc Tax Rule not found');
        }

        return $this->sendResponse($ocTaxRule->toArray(), 'Oc Tax Rule retrieved successfully');
    }

    /**
     * Update the specified oc_tax_rule in storage.
     * PUT/PATCH /ocTaxRules/{id}
     *
     * @param  int $id
     * @param Updateoc_tax_ruleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_ruleAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_tax_rule $ocTaxRule */
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            return $this->sendError('Oc Tax Rule not found');
        }

        $ocTaxRule = $this->ocTaxRuleRepository->update($input, $id);

        return $this->sendResponse($ocTaxRule->toArray(), 'oc_tax_rule updated successfully');
    }

    /**
     * Remove the specified oc_tax_rule from storage.
     * DELETE /ocTaxRules/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_tax_rule $ocTaxRule */
        $ocTaxRule = $this->ocTaxRuleRepository->findWithoutFail($id);

        if (empty($ocTaxRule)) {
            return $this->sendError('Oc Tax Rule not found');
        }

        $ocTaxRule->delete();

        return $this->sendResponse($id, 'Oc Tax Rule deleted successfully');
    }
}
