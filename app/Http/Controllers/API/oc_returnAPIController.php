<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_returnAPIRequest;
use App\Http\Requests\API\Updateoc_returnAPIRequest;
use App\Models\oc_return;
use App\Repositories\oc_returnRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_returnController
 * @package App\Http\Controllers\API
 */

class oc_returnAPIController extends AppBaseController
{
    /** @var  oc_returnRepository */
    private $ocReturnRepository;

    public function __construct(oc_returnRepository $ocReturnRepo)
    {
        $this->ocReturnRepository = $ocReturnRepo;
    }

    /**
     * Display a listing of the oc_return.
     * GET|HEAD /ocReturns
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReturnRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReturns = $this->ocReturnRepository->all();

        return $this->sendResponse($ocReturns->toArray(), 'Oc Returns retrieved successfully');
    }

    /**
     * Store a newly created oc_return in storage.
     * POST /ocReturns
     *
     * @param Createoc_returnAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_returnAPIRequest $request)
    {
        $input = $request->all();

        $ocReturns = $this->ocReturnRepository->create($input);

        return $this->sendResponse($ocReturns->toArray(), 'Oc Return saved successfully');
    }

    /**
     * Display the specified oc_return.
     * GET|HEAD /ocReturns/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_return $ocReturn */
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            return $this->sendError('Oc Return not found');
        }

        return $this->sendResponse($ocReturn->toArray(), 'Oc Return retrieved successfully');
    }

    /**
     * Update the specified oc_return in storage.
     * PUT/PATCH /ocReturns/{id}
     *
     * @param  int $id
     * @param Updateoc_returnAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_returnAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_return $ocReturn */
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            return $this->sendError('Oc Return not found');
        }

        $ocReturn = $this->ocReturnRepository->update($input, $id);

        return $this->sendResponse($ocReturn->toArray(), 'oc_return updated successfully');
    }

    /**
     * Remove the specified oc_return from storage.
     * DELETE /ocReturns/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_return $ocReturn */
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            return $this->sendError('Oc Return not found');
        }

        $ocReturn->delete();

        return $this->sendResponse($id, 'Oc Return deleted successfully');
    }
}
