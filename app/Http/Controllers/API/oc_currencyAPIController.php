<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_currencyAPIRequest;
use App\Http\Requests\API\Updateoc_currencyAPIRequest;
use App\Models\oc_currency;
use App\Repositories\oc_currencyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_currencyController
 * @package App\Http\Controllers\API
 */

class oc_currencyAPIController extends AppBaseController
{
    /** @var  oc_currencyRepository */
    private $ocCurrencyRepository;

    public function __construct(oc_currencyRepository $ocCurrencyRepo)
    {
        $this->ocCurrencyRepository = $ocCurrencyRepo;
    }

    /**
     * Display a listing of the oc_currency.
     * GET|HEAD /ocCurrencies
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCurrencyRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCurrencyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCurrencies = $this->ocCurrencyRepository->all();

        return $this->sendResponse($ocCurrencies->toArray(), 'Oc Currencies retrieved successfully');
    }

    /**
     * Store a newly created oc_currency in storage.
     * POST /ocCurrencies
     *
     * @param Createoc_currencyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_currencyAPIRequest $request)
    {
        $input = $request->all();

        $ocCurrencies = $this->ocCurrencyRepository->create($input);

        return $this->sendResponse($ocCurrencies->toArray(), 'Oc Currency saved successfully');
    }

    /**
     * Display the specified oc_currency.
     * GET|HEAD /ocCurrencies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_currency $ocCurrency */
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            return $this->sendError('Oc Currency not found');
        }

        return $this->sendResponse($ocCurrency->toArray(), 'Oc Currency retrieved successfully');
    }

    /**
     * Update the specified oc_currency in storage.
     * PUT/PATCH /ocCurrencies/{id}
     *
     * @param  int $id
     * @param Updateoc_currencyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_currencyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_currency $ocCurrency */
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            return $this->sendError('Oc Currency not found');
        }

        $ocCurrency = $this->ocCurrencyRepository->update($input, $id);

        return $this->sendResponse($ocCurrency->toArray(), 'oc_currency updated successfully');
    }

    /**
     * Remove the specified oc_currency from storage.
     * DELETE /ocCurrencies/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_currency $ocCurrency */
        $ocCurrency = $this->ocCurrencyRepository->findWithoutFail($id);

        if (empty($ocCurrency)) {
            return $this->sendError('Oc Currency not found');
        }

        $ocCurrency->delete();

        return $this->sendResponse($id, 'Oc Currency deleted successfully');
    }
}
