<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_statusAPIRequest;
use App\Http\Requests\API\Updateoc_order_statusAPIRequest;
use App\Models\oc_order_status;
use App\Repositories\oc_order_statusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_statusController
 * @package App\Http\Controllers\API
 */

class oc_order_statusAPIController extends AppBaseController
{
    /** @var  oc_order_statusRepository */
    private $ocOrderStatusRepository;

    public function __construct(oc_order_statusRepository $ocOrderStatusRepo)
    {
        $this->ocOrderStatusRepository = $ocOrderStatusRepo;
    }

    /**
     * Display a listing of the oc_order_status.
     * GET|HEAD /ocOrderStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderStatuses = $this->ocOrderStatusRepository->all();

        return $this->sendResponse($ocOrderStatuses->toArray(), 'Oc Order Statuses retrieved successfully');
    }

    /**
     * Store a newly created oc_order_status in storage.
     * POST /ocOrderStatuses
     *
     * @param Createoc_order_statusAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_statusAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderStatuses = $this->ocOrderStatusRepository->create($input);

        return $this->sendResponse($ocOrderStatuses->toArray(), 'Oc Order Status saved successfully');
    }

    /**
     * Display the specified oc_order_status.
     * GET|HEAD /ocOrderStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_status $ocOrderStatus */
        $ocOrderStatus = $this->ocOrderStatusRepository->findWithoutFail($id);

        if (empty($ocOrderStatus)) {
            return $this->sendError('Oc Order Status not found');
        }

        return $this->sendResponse($ocOrderStatus->toArray(), 'Oc Order Status retrieved successfully');
    }

    /**
     * Update the specified oc_order_status in storage.
     * PUT/PATCH /ocOrderStatuses/{id}
     *
     * @param  int $id
     * @param Updateoc_order_statusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_statusAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_status $ocOrderStatus */
        $ocOrderStatus = $this->ocOrderStatusRepository->findWithoutFail($id);

        if (empty($ocOrderStatus)) {
            return $this->sendError('Oc Order Status not found');
        }

        $ocOrderStatus = $this->ocOrderStatusRepository->update($input, $id);

        return $this->sendResponse($ocOrderStatus->toArray(), 'oc_order_status updated successfully');
    }

    /**
     * Remove the specified oc_order_status from storage.
     * DELETE /ocOrderStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_status $ocOrderStatus */
        $ocOrderStatus = $this->ocOrderStatusRepository->findWithoutFail($id);

        if (empty($ocOrderStatus)) {
            return $this->sendError('Oc Order Status not found');
        }

        $ocOrderStatus->delete();

        return $this->sendResponse($id, 'Oc Order Status deleted successfully');
    }
}
