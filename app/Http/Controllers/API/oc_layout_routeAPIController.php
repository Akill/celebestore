<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_layout_routeAPIRequest;
use App\Http\Requests\API\Updateoc_layout_routeAPIRequest;
use App\Models\oc_layout_route;
use App\Repositories\oc_layout_routeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_layout_routeController
 * @package App\Http\Controllers\API
 */

class oc_layout_routeAPIController extends AppBaseController
{
    /** @var  oc_layout_routeRepository */
    private $ocLayoutRouteRepository;

    public function __construct(oc_layout_routeRepository $ocLayoutRouteRepo)
    {
        $this->ocLayoutRouteRepository = $ocLayoutRouteRepo;
    }

    /**
     * Display a listing of the oc_layout_route.
     * GET|HEAD /ocLayoutRoutes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLayoutRouteRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLayoutRouteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLayoutRoutes = $this->ocLayoutRouteRepository->all();

        return $this->sendResponse($ocLayoutRoutes->toArray(), 'Oc Layout Routes retrieved successfully');
    }

    /**
     * Store a newly created oc_layout_route in storage.
     * POST /ocLayoutRoutes
     *
     * @param Createoc_layout_routeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_layout_routeAPIRequest $request)
    {
        $input = $request->all();

        $ocLayoutRoutes = $this->ocLayoutRouteRepository->create($input);

        return $this->sendResponse($ocLayoutRoutes->toArray(), 'Oc Layout Route saved successfully');
    }

    /**
     * Display the specified oc_layout_route.
     * GET|HEAD /ocLayoutRoutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_layout_route $ocLayoutRoute */
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            return $this->sendError('Oc Layout Route not found');
        }

        return $this->sendResponse($ocLayoutRoute->toArray(), 'Oc Layout Route retrieved successfully');
    }

    /**
     * Update the specified oc_layout_route in storage.
     * PUT/PATCH /ocLayoutRoutes/{id}
     *
     * @param  int $id
     * @param Updateoc_layout_routeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_layout_routeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_layout_route $ocLayoutRoute */
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            return $this->sendError('Oc Layout Route not found');
        }

        $ocLayoutRoute = $this->ocLayoutRouteRepository->update($input, $id);

        return $this->sendResponse($ocLayoutRoute->toArray(), 'oc_layout_route updated successfully');
    }

    /**
     * Remove the specified oc_layout_route from storage.
     * DELETE /ocLayoutRoutes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_layout_route $ocLayoutRoute */
        $ocLayoutRoute = $this->ocLayoutRouteRepository->findWithoutFail($id);

        if (empty($ocLayoutRoute)) {
            return $this->sendError('Oc Layout Route not found');
        }

        $ocLayoutRoute->delete();

        return $this->sendResponse($id, 'Oc Layout Route deleted successfully');
    }
}
