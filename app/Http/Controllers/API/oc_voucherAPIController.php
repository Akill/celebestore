<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_voucherAPIRequest;
use App\Http\Requests\API\Updateoc_voucherAPIRequest;
use App\Models\oc_voucher;
use App\Repositories\oc_voucherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_voucherController
 * @package App\Http\Controllers\API
 */

class oc_voucherAPIController extends AppBaseController
{
    /** @var  oc_voucherRepository */
    private $ocVoucherRepository;

    public function __construct(oc_voucherRepository $ocVoucherRepo)
    {
        $this->ocVoucherRepository = $ocVoucherRepo;
    }

    /**
     * Display a listing of the oc_voucher.
     * GET|HEAD /ocVouchers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherRepository->pushCriteria(new RequestCriteria($request));
        $this->ocVoucherRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocVouchers = $this->ocVoucherRepository->all();

        return $this->sendResponse($ocVouchers->toArray(), 'Oc Vouchers retrieved successfully');
    }

    /**
     * Store a newly created oc_voucher in storage.
     * POST /ocVouchers
     *
     * @param Createoc_voucherAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucherAPIRequest $request)
    {
        $input = $request->all();

        $ocVouchers = $this->ocVoucherRepository->create($input);

        return $this->sendResponse($ocVouchers->toArray(), 'Oc Voucher saved successfully');
    }

    /**
     * Display the specified oc_voucher.
     * GET|HEAD /ocVouchers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_voucher $ocVoucher */
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            return $this->sendError('Oc Voucher not found');
        }

        return $this->sendResponse($ocVoucher->toArray(), 'Oc Voucher retrieved successfully');
    }

    /**
     * Update the specified oc_voucher in storage.
     * PUT/PATCH /ocVouchers/{id}
     *
     * @param  int $id
     * @param Updateoc_voucherAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucherAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_voucher $ocVoucher */
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            return $this->sendError('Oc Voucher not found');
        }

        $ocVoucher = $this->ocVoucherRepository->update($input, $id);

        return $this->sendResponse($ocVoucher->toArray(), 'oc_voucher updated successfully');
    }

    /**
     * Remove the specified oc_voucher from storage.
     * DELETE /ocVouchers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_voucher $ocVoucher */
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            return $this->sendError('Oc Voucher not found');
        }

        $ocVoucher->delete();

        return $this->sendResponse($id, 'Oc Voucher deleted successfully');
    }
}
