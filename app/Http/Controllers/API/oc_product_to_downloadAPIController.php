<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_to_downloadAPIRequest;
use App\Http\Requests\API\Updateoc_product_to_downloadAPIRequest;
use App\Models\oc_product_to_download;
use App\Repositories\oc_product_to_downloadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_to_downloadController
 * @package App\Http\Controllers\API
 */

class oc_product_to_downloadAPIController extends AppBaseController
{
    /** @var  oc_product_to_downloadRepository */
    private $ocProductToDownloadRepository;

    public function __construct(oc_product_to_downloadRepository $ocProductToDownloadRepo)
    {
        $this->ocProductToDownloadRepository = $ocProductToDownloadRepo;
    }

    /**
     * Display a listing of the oc_product_to_download.
     * GET|HEAD /ocProductToDownloads
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToDownloadRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductToDownloadRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductToDownloads = $this->ocProductToDownloadRepository->all();

        return $this->sendResponse($ocProductToDownloads->toArray(), 'Oc Product To Downloads retrieved successfully');
    }

    /**
     * Store a newly created oc_product_to_download in storage.
     * POST /ocProductToDownloads
     *
     * @param Createoc_product_to_downloadAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_downloadAPIRequest $request)
    {
        $input = $request->all();

        $ocProductToDownloads = $this->ocProductToDownloadRepository->create($input);

        return $this->sendResponse($ocProductToDownloads->toArray(), 'Oc Product To Download saved successfully');
    }

    /**
     * Display the specified oc_product_to_download.
     * GET|HEAD /ocProductToDownloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_to_download $ocProductToDownload */
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            return $this->sendError('Oc Product To Download not found');
        }

        return $this->sendResponse($ocProductToDownload->toArray(), 'Oc Product To Download retrieved successfully');
    }

    /**
     * Update the specified oc_product_to_download in storage.
     * PUT/PATCH /ocProductToDownloads/{id}
     *
     * @param  int $id
     * @param Updateoc_product_to_downloadAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_downloadAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_to_download $ocProductToDownload */
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            return $this->sendError('Oc Product To Download not found');
        }

        $ocProductToDownload = $this->ocProductToDownloadRepository->update($input, $id);

        return $this->sendResponse($ocProductToDownload->toArray(), 'oc_product_to_download updated successfully');
    }

    /**
     * Remove the specified oc_product_to_download from storage.
     * DELETE /ocProductToDownloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_to_download $ocProductToDownload */
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            return $this->sendError('Oc Product To Download not found');
        }

        $ocProductToDownload->delete();

        return $this->sendResponse($id, 'Oc Product To Download deleted successfully');
    }
}
