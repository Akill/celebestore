<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_informationAPIRequest;
use App\Http\Requests\API\Updateoc_informationAPIRequest;
use App\Models\oc_information;
use App\Repositories\oc_informationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_informationController
 * @package App\Http\Controllers\API
 */

class oc_informationAPIController extends AppBaseController
{
    /** @var  oc_informationRepository */
    private $ocInformationRepository;

    public function __construct(oc_informationRepository $ocInformationRepo)
    {
        $this->ocInformationRepository = $ocInformationRepo;
    }

    /**
     * Display a listing of the oc_information.
     * GET|HEAD /ocInformations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationRepository->pushCriteria(new RequestCriteria($request));
        $this->ocInformationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocInformations = $this->ocInformationRepository->all();

        return $this->sendResponse($ocInformations->toArray(), 'Oc Informations retrieved successfully');
    }

    /**
     * Store a newly created oc_information in storage.
     * POST /ocInformations
     *
     * @param Createoc_informationAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_informationAPIRequest $request)
    {
        $input = $request->all();

        $ocInformations = $this->ocInformationRepository->create($input);

        return $this->sendResponse($ocInformations->toArray(), 'Oc Information saved successfully');
    }

    /**
     * Display the specified oc_information.
     * GET|HEAD /ocInformations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_information $ocInformation */
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            return $this->sendError('Oc Information not found');
        }

        return $this->sendResponse($ocInformation->toArray(), 'Oc Information retrieved successfully');
    }

    /**
     * Update the specified oc_information in storage.
     * PUT/PATCH /ocInformations/{id}
     *
     * @param  int $id
     * @param Updateoc_informationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_informationAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_information $ocInformation */
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            return $this->sendError('Oc Information not found');
        }

        $ocInformation = $this->ocInformationRepository->update($input, $id);

        return $this->sendResponse($ocInformation->toArray(), 'oc_information updated successfully');
    }

    /**
     * Remove the specified oc_information from storage.
     * DELETE /ocInformations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_information $ocInformation */
        $ocInformation = $this->ocInformationRepository->findWithoutFail($id);

        if (empty($ocInformation)) {
            return $this->sendError('Oc Information not found');
        }

        $ocInformation->delete();

        return $this->sendResponse($id, 'Oc Information deleted successfully');
    }
}
