<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_to_layoutAPIRequest;
use App\Http\Requests\API\Updateoc_product_to_layoutAPIRequest;
use App\Models\oc_product_to_layout;
use App\Repositories\oc_product_to_layoutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_to_layoutController
 * @package App\Http\Controllers\API
 */

class oc_product_to_layoutAPIController extends AppBaseController
{
    /** @var  oc_product_to_layoutRepository */
    private $ocProductToLayoutRepository;

    public function __construct(oc_product_to_layoutRepository $ocProductToLayoutRepo)
    {
        $this->ocProductToLayoutRepository = $ocProductToLayoutRepo;
    }

    /**
     * Display a listing of the oc_product_to_layout.
     * GET|HEAD /ocProductToLayouts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToLayoutRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductToLayoutRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductToLayouts = $this->ocProductToLayoutRepository->all();

        return $this->sendResponse($ocProductToLayouts->toArray(), 'Oc Product To Layouts retrieved successfully');
    }

    /**
     * Store a newly created oc_product_to_layout in storage.
     * POST /ocProductToLayouts
     *
     * @param Createoc_product_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        $ocProductToLayouts = $this->ocProductToLayoutRepository->create($input);

        return $this->sendResponse($ocProductToLayouts->toArray(), 'Oc Product To Layout saved successfully');
    }

    /**
     * Display the specified oc_product_to_layout.
     * GET|HEAD /ocProductToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_to_layout $ocProductToLayout */
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            return $this->sendError('Oc Product To Layout not found');
        }

        return $this->sendResponse($ocProductToLayout->toArray(), 'Oc Product To Layout retrieved successfully');
    }

    /**
     * Update the specified oc_product_to_layout in storage.
     * PUT/PATCH /ocProductToLayouts/{id}
     *
     * @param  int $id
     * @param Updateoc_product_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_to_layout $ocProductToLayout */
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            return $this->sendError('Oc Product To Layout not found');
        }

        $ocProductToLayout = $this->ocProductToLayoutRepository->update($input, $id);

        return $this->sendResponse($ocProductToLayout->toArray(), 'oc_product_to_layout updated successfully');
    }

    /**
     * Remove the specified oc_product_to_layout from storage.
     * DELETE /ocProductToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_to_layout $ocProductToLayout */
        $ocProductToLayout = $this->ocProductToLayoutRepository->findWithoutFail($id);

        if (empty($ocProductToLayout)) {
            return $this->sendError('Oc Product To Layout not found');
        }

        $ocProductToLayout->delete();

        return $this->sendResponse($id, 'Oc Product To Layout deleted successfully');
    }
}
