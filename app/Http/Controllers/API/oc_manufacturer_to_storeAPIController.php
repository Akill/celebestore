<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_manufacturer_to_storeAPIRequest;
use App\Http\Requests\API\Updateoc_manufacturer_to_storeAPIRequest;
use App\Models\oc_manufacturer_to_store;
use App\Repositories\oc_manufacturer_to_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_manufacturer_to_storeController
 * @package App\Http\Controllers\API
 */

class oc_manufacturer_to_storeAPIController extends AppBaseController
{
    /** @var  oc_manufacturer_to_storeRepository */
    private $ocManufacturerToStoreRepository;

    public function __construct(oc_manufacturer_to_storeRepository $ocManufacturerToStoreRepo)
    {
        $this->ocManufacturerToStoreRepository = $ocManufacturerToStoreRepo;
    }

    /**
     * Display a listing of the oc_manufacturer_to_store.
     * GET|HEAD /ocManufacturerToStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocManufacturerToStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocManufacturerToStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocManufacturerToStores = $this->ocManufacturerToStoreRepository->all();

        return $this->sendResponse($ocManufacturerToStores->toArray(), 'Oc Manufacturer To Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_manufacturer_to_store in storage.
     * POST /ocManufacturerToStores
     *
     * @param Createoc_manufacturer_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_manufacturer_to_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocManufacturerToStores = $this->ocManufacturerToStoreRepository->create($input);

        return $this->sendResponse($ocManufacturerToStores->toArray(), 'Oc Manufacturer To Store saved successfully');
    }

    /**
     * Display the specified oc_manufacturer_to_store.
     * GET|HEAD /ocManufacturerToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_manufacturer_to_store $ocManufacturerToStore */
        $ocManufacturerToStore = $this->ocManufacturerToStoreRepository->findWithoutFail($id);

        if (empty($ocManufacturerToStore)) {
            return $this->sendError('Oc Manufacturer To Store not found');
        }

        return $this->sendResponse($ocManufacturerToStore->toArray(), 'Oc Manufacturer To Store retrieved successfully');
    }

    /**
     * Update the specified oc_manufacturer_to_store in storage.
     * PUT/PATCH /ocManufacturerToStores/{id}
     *
     * @param  int $id
     * @param Updateoc_manufacturer_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_manufacturer_to_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_manufacturer_to_store $ocManufacturerToStore */
        $ocManufacturerToStore = $this->ocManufacturerToStoreRepository->findWithoutFail($id);

        if (empty($ocManufacturerToStore)) {
            return $this->sendError('Oc Manufacturer To Store not found');
        }

        $ocManufacturerToStore = $this->ocManufacturerToStoreRepository->update($input, $id);

        return $this->sendResponse($ocManufacturerToStore->toArray(), 'oc_manufacturer_to_store updated successfully');
    }

    /**
     * Remove the specified oc_manufacturer_to_store from storage.
     * DELETE /ocManufacturerToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_manufacturer_to_store $ocManufacturerToStore */
        $ocManufacturerToStore = $this->ocManufacturerToStoreRepository->findWithoutFail($id);

        if (empty($ocManufacturerToStore)) {
            return $this->sendError('Oc Manufacturer To Store not found');
        }

        $ocManufacturerToStore->delete();

        return $this->sendResponse($id, 'Oc Manufacturer To Store deleted successfully');
    }
}
