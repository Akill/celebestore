<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_filter_groupAPIRequest;
use App\Http\Requests\API\Updateoc_filter_groupAPIRequest;
use App\Models\oc_filter_group;
use App\Repositories\oc_filter_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_filter_groupController
 * @package App\Http\Controllers\API
 */

class oc_filter_groupAPIController extends AppBaseController
{
    /** @var  oc_filter_groupRepository */
    private $ocFilterGroupRepository;

    public function __construct(oc_filter_groupRepository $ocFilterGroupRepo)
    {
        $this->ocFilterGroupRepository = $ocFilterGroupRepo;
    }

    /**
     * Display a listing of the oc_filter_group.
     * GET|HEAD /ocFilterGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocFilterGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocFilterGroups = $this->ocFilterGroupRepository->all();

        return $this->sendResponse($ocFilterGroups->toArray(), 'Oc Filter Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_filter_group in storage.
     * POST /ocFilterGroups
     *
     * @param Createoc_filter_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filter_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocFilterGroups = $this->ocFilterGroupRepository->create($input);

        return $this->sendResponse($ocFilterGroups->toArray(), 'Oc Filter Group saved successfully');
    }

    /**
     * Display the specified oc_filter_group.
     * GET|HEAD /ocFilterGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_filter_group $ocFilterGroup */
        $ocFilterGroup = $this->ocFilterGroupRepository->findWithoutFail($id);

        if (empty($ocFilterGroup)) {
            return $this->sendError('Oc Filter Group not found');
        }

        return $this->sendResponse($ocFilterGroup->toArray(), 'Oc Filter Group retrieved successfully');
    }

    /**
     * Update the specified oc_filter_group in storage.
     * PUT/PATCH /ocFilterGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_filter_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filter_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_filter_group $ocFilterGroup */
        $ocFilterGroup = $this->ocFilterGroupRepository->findWithoutFail($id);

        if (empty($ocFilterGroup)) {
            return $this->sendError('Oc Filter Group not found');
        }

        $ocFilterGroup = $this->ocFilterGroupRepository->update($input, $id);

        return $this->sendResponse($ocFilterGroup->toArray(), 'oc_filter_group updated successfully');
    }

    /**
     * Remove the specified oc_filter_group from storage.
     * DELETE /ocFilterGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_filter_group $ocFilterGroup */
        $ocFilterGroup = $this->ocFilterGroupRepository->findWithoutFail($id);

        if (empty($ocFilterGroup)) {
            return $this->sendError('Oc Filter Group not found');
        }

        $ocFilterGroup->delete();

        return $this->sendResponse($id, 'Oc Filter Group deleted successfully');
    }
}
