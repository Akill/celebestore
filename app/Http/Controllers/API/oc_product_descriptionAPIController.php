<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_product_descriptionAPIRequest;
use App\Models\oc_product_description;
use App\Repositories\oc_product_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_product_descriptionAPIController extends AppBaseController
{
    /** @var  oc_product_descriptionRepository */
    private $ocProductDescriptionRepository;

    public function __construct(oc_product_descriptionRepository $ocProductDescriptionRepo)
    {
        $this->ocProductDescriptionRepository = $ocProductDescriptionRepo;
    }

    /**
     * Display a listing of the oc_product_description.
     * GET|HEAD /ocProductDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductDescriptions = $this->ocProductDescriptionRepository->all();

        return $this->sendResponse($ocProductDescriptions->toArray(), 'Oc Product Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_product_description in storage.
     * POST /ocProductDescriptions
     *
     * @param Createoc_product_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocProductDescriptions = $this->ocProductDescriptionRepository->create($input);

        return $this->sendResponse($ocProductDescriptions->toArray(), 'Oc Product Description saved successfully');
    }

    /**
     * Display the specified oc_product_description.
     * GET|HEAD /ocProductDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_description $ocProductDescription */
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            return $this->sendError('Oc Product Description not found');
        }

        return $this->sendResponse($ocProductDescription->toArray(), 'Oc Product Description retrieved successfully');
    }

    /**
     * Update the specified oc_product_description in storage.
     * PUT/PATCH /ocProductDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_product_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_description $ocProductDescription */
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            return $this->sendError('Oc Product Description not found');
        }

        $ocProductDescription = $this->ocProductDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocProductDescription->toArray(), 'oc_product_description updated successfully');
    }

    /**
     * Remove the specified oc_product_description from storage.
     * DELETE /ocProductDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_description $ocProductDescription */
        $ocProductDescription = $this->ocProductDescriptionRepository->findWithoutFail($id);

        if (empty($ocProductDescription)) {
            return $this->sendError('Oc Product Description not found');
        }

        $ocProductDescription->delete();

        return $this->sendResponse($id, 'Oc Product Description deleted successfully');
    }
}
