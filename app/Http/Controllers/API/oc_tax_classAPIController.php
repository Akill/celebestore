<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_tax_classAPIRequest;
use App\Http\Requests\API\Updateoc_tax_classAPIRequest;
use App\Models\oc_tax_class;
use App\Repositories\oc_tax_classRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_tax_classController
 * @package App\Http\Controllers\API
 */

class oc_tax_classAPIController extends AppBaseController
{
    /** @var  oc_tax_classRepository */
    private $ocTaxClassRepository;

    public function __construct(oc_tax_classRepository $ocTaxClassRepo)
    {
        $this->ocTaxClassRepository = $ocTaxClassRepo;
    }

    /**
     * Display a listing of the oc_tax_class.
     * GET|HEAD /ocTaxClasses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxClassRepository->pushCriteria(new RequestCriteria($request));
        $this->ocTaxClassRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocTaxClasses = $this->ocTaxClassRepository->all();

        return $this->sendResponse($ocTaxClasses->toArray(), 'Oc Tax Classes retrieved successfully');
    }

    /**
     * Store a newly created oc_tax_class in storage.
     * POST /ocTaxClasses
     *
     * @param Createoc_tax_classAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_classAPIRequest $request)
    {
        $input = $request->all();

        $ocTaxClasses = $this->ocTaxClassRepository->create($input);

        return $this->sendResponse($ocTaxClasses->toArray(), 'Oc Tax Class saved successfully');
    }

    /**
     * Display the specified oc_tax_class.
     * GET|HEAD /ocTaxClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_tax_class $ocTaxClass */
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            return $this->sendError('Oc Tax Class not found');
        }

        return $this->sendResponse($ocTaxClass->toArray(), 'Oc Tax Class retrieved successfully');
    }

    /**
     * Update the specified oc_tax_class in storage.
     * PUT/PATCH /ocTaxClasses/{id}
     *
     * @param  int $id
     * @param Updateoc_tax_classAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_classAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_tax_class $ocTaxClass */
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            return $this->sendError('Oc Tax Class not found');
        }

        $ocTaxClass = $this->ocTaxClassRepository->update($input, $id);

        return $this->sendResponse($ocTaxClass->toArray(), 'oc_tax_class updated successfully');
    }

    /**
     * Remove the specified oc_tax_class from storage.
     * DELETE /ocTaxClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_tax_class $ocTaxClass */
        $ocTaxClass = $this->ocTaxClassRepository->findWithoutFail($id);

        if (empty($ocTaxClass)) {
            return $this->sendError('Oc Tax Class not found');
        }

        $ocTaxClass->delete();

        return $this->sendResponse($id, 'Oc Tax Class deleted successfully');
    }
}
