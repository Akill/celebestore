<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_coupon_categoryAPIRequest;
use App\Http\Requests\API\Updateoc_coupon_categoryAPIRequest;
use App\Models\oc_coupon_category;
use App\Repositories\oc_coupon_categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_coupon_categoryController
 * @package App\Http\Controllers\API
 */

class oc_coupon_categoryAPIController extends AppBaseController
{
    /** @var  oc_coupon_categoryRepository */
    private $ocCouponCategoryRepository;

    public function __construct(oc_coupon_categoryRepository $ocCouponCategoryRepo)
    {
        $this->ocCouponCategoryRepository = $ocCouponCategoryRepo;
    }

    /**
     * Display a listing of the oc_coupon_category.
     * GET|HEAD /ocCouponCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCouponCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCouponCategories = $this->ocCouponCategoryRepository->all();

        return $this->sendResponse($ocCouponCategories->toArray(), 'Oc Coupon Categories retrieved successfully');
    }

    /**
     * Store a newly created oc_coupon_category in storage.
     * POST /ocCouponCategories
     *
     * @param Createoc_coupon_categoryAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_categoryAPIRequest $request)
    {
        $input = $request->all();

        $ocCouponCategories = $this->ocCouponCategoryRepository->create($input);

        return $this->sendResponse($ocCouponCategories->toArray(), 'Oc Coupon Category saved successfully');
    }

    /**
     * Display the specified oc_coupon_category.
     * GET|HEAD /ocCouponCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_coupon_category $ocCouponCategory */
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            return $this->sendError('Oc Coupon Category not found');
        }

        return $this->sendResponse($ocCouponCategory->toArray(), 'Oc Coupon Category retrieved successfully');
    }

    /**
     * Update the specified oc_coupon_category in storage.
     * PUT/PATCH /ocCouponCategories/{id}
     *
     * @param  int $id
     * @param Updateoc_coupon_categoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_categoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_coupon_category $ocCouponCategory */
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            return $this->sendError('Oc Coupon Category not found');
        }

        $ocCouponCategory = $this->ocCouponCategoryRepository->update($input, $id);

        return $this->sendResponse($ocCouponCategory->toArray(), 'oc_coupon_category updated successfully');
    }

    /**
     * Remove the specified oc_coupon_category from storage.
     * DELETE /ocCouponCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_coupon_category $ocCouponCategory */
        $ocCouponCategory = $this->ocCouponCategoryRepository->findWithoutFail($id);

        if (empty($ocCouponCategory)) {
            return $this->sendError('Oc Coupon Category not found');
        }

        $ocCouponCategory->delete();

        return $this->sendResponse($id, 'Oc Coupon Category deleted successfully');
    }
}
