<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_couponAPIRequest;
use App\Http\Requests\API\Updateoc_couponAPIRequest;
use App\Models\oc_coupon;
use App\Repositories\oc_couponRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_couponController
 * @package App\Http\Controllers\API
 */

class oc_couponAPIController extends AppBaseController
{
    /** @var  oc_couponRepository */
    private $ocCouponRepository;

    public function __construct(oc_couponRepository $ocCouponRepo)
    {
        $this->ocCouponRepository = $ocCouponRepo;
    }

    /**
     * Display a listing of the oc_coupon.
     * GET|HEAD /ocCoupons
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCouponRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCoupons = $this->ocCouponRepository->all();

        return $this->sendResponse($ocCoupons->toArray(), 'Oc Coupons retrieved successfully');
    }

    /**
     * Store a newly created oc_coupon in storage.
     * POST /ocCoupons
     *
     * @param Createoc_couponAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_couponAPIRequest $request)
    {
        $input = $request->all();

        $ocCoupons = $this->ocCouponRepository->create($input);

        return $this->sendResponse($ocCoupons->toArray(), 'Oc Coupon saved successfully');
    }

    /**
     * Display the specified oc_coupon.
     * GET|HEAD /ocCoupons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_coupon $ocCoupon */
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            return $this->sendError('Oc Coupon not found');
        }

        return $this->sendResponse($ocCoupon->toArray(), 'Oc Coupon retrieved successfully');
    }

    /**
     * Update the specified oc_coupon in storage.
     * PUT/PATCH /ocCoupons/{id}
     *
     * @param  int $id
     * @param Updateoc_couponAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_couponAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_coupon $ocCoupon */
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            return $this->sendError('Oc Coupon not found');
        }

        $ocCoupon = $this->ocCouponRepository->update($input, $id);

        return $this->sendResponse($ocCoupon->toArray(), 'oc_coupon updated successfully');
    }

    /**
     * Remove the specified oc_coupon from storage.
     * DELETE /ocCoupons/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_coupon $ocCoupon */
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            return $this->sendError('Oc Coupon not found');
        }

        $ocCoupon->delete();

        return $this->sendResponse($id, 'Oc Coupon deleted successfully');
    }
}
