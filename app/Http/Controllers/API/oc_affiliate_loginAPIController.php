<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_affiliate_loginAPIRequest;
use App\Http\Requests\API\Updateoc_affiliate_loginAPIRequest;
use App\Models\oc_affiliate_login;
use App\Repositories\oc_affiliate_loginRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_affiliate_loginController
 * @package App\Http\Controllers\API
 */

class oc_affiliate_loginAPIController extends AppBaseController
{
    /** @var  oc_affiliate_loginRepository */
    private $ocAffiliateLoginRepository;

    public function __construct(oc_affiliate_loginRepository $ocAffiliateLoginRepo)
    {
        $this->ocAffiliateLoginRepository = $ocAffiliateLoginRepo;
    }

    /**
     * Display a listing of the oc_affiliate_login.
     * GET|HEAD /ocAffiliateLogins
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateLoginRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAffiliateLoginRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAffiliateLogins = $this->ocAffiliateLoginRepository->all();

        return $this->sendResponse($ocAffiliateLogins->toArray(), 'Oc Affiliate Logins retrieved successfully');
    }

    /**
     * Store a newly created oc_affiliate_login in storage.
     * POST /ocAffiliateLogins
     *
     * @param Createoc_affiliate_loginAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_loginAPIRequest $request)
    {
        $input = $request->all();

        $ocAffiliateLogins = $this->ocAffiliateLoginRepository->create($input);

        return $this->sendResponse($ocAffiliateLogins->toArray(), 'Oc Affiliate Login saved successfully');
    }

    /**
     * Display the specified oc_affiliate_login.
     * GET|HEAD /ocAffiliateLogins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_affiliate_login $ocAffiliateLogin */
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            return $this->sendError('Oc Affiliate Login not found');
        }

        return $this->sendResponse($ocAffiliateLogin->toArray(), 'Oc Affiliate Login retrieved successfully');
    }

    /**
     * Update the specified oc_affiliate_login in storage.
     * PUT/PATCH /ocAffiliateLogins/{id}
     *
     * @param  int $id
     * @param Updateoc_affiliate_loginAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_loginAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_affiliate_login $ocAffiliateLogin */
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            return $this->sendError('Oc Affiliate Login not found');
        }

        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->update($input, $id);

        return $this->sendResponse($ocAffiliateLogin->toArray(), 'oc_affiliate_login updated successfully');
    }

    /**
     * Remove the specified oc_affiliate_login from storage.
     * DELETE /ocAffiliateLogins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_affiliate_login $ocAffiliateLogin */
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            return $this->sendError('Oc Affiliate Login not found');
        }

        $ocAffiliateLogin->delete();

        return $this->sendResponse($id, 'Oc Affiliate Login deleted successfully');
    }
}
