<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_length_class_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_length_class_descriptionAPIRequest;
use App\Models\oc_length_class_description;
use App\Repositories\oc_length_class_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_length_class_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_length_class_descriptionAPIController extends AppBaseController
{
    /** @var  oc_length_class_descriptionRepository */
    private $ocLengthClassDescriptionRepository;

    public function __construct(oc_length_class_descriptionRepository $ocLengthClassDescriptionRepo)
    {
        $this->ocLengthClassDescriptionRepository = $ocLengthClassDescriptionRepo;
    }

    /**
     * Display a listing of the oc_length_class_description.
     * GET|HEAD /ocLengthClassDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLengthClassDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLengthClassDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLengthClassDescriptions = $this->ocLengthClassDescriptionRepository->all();

        return $this->sendResponse($ocLengthClassDescriptions->toArray(), 'Oc Length Class Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_length_class_description in storage.
     * POST /ocLengthClassDescriptions
     *
     * @param Createoc_length_class_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_length_class_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocLengthClassDescriptions = $this->ocLengthClassDescriptionRepository->create($input);

        return $this->sendResponse($ocLengthClassDescriptions->toArray(), 'Oc Length Class Description saved successfully');
    }

    /**
     * Display the specified oc_length_class_description.
     * GET|HEAD /ocLengthClassDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_length_class_description $ocLengthClassDescription */
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            return $this->sendError('Oc Length Class Description not found');
        }

        return $this->sendResponse($ocLengthClassDescription->toArray(), 'Oc Length Class Description retrieved successfully');
    }

    /**
     * Update the specified oc_length_class_description in storage.
     * PUT/PATCH /ocLengthClassDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_length_class_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_length_class_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_length_class_description $ocLengthClassDescription */
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            return $this->sendError('Oc Length Class Description not found');
        }

        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocLengthClassDescription->toArray(), 'oc_length_class_description updated successfully');
    }

    /**
     * Remove the specified oc_length_class_description from storage.
     * DELETE /ocLengthClassDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_length_class_description $ocLengthClassDescription */
        $ocLengthClassDescription = $this->ocLengthClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocLengthClassDescription)) {
            return $this->sendError('Oc Length Class Description not found');
        }

        $ocLengthClassDescription->delete();

        return $this->sendResponse($id, 'Oc Length Class Description deleted successfully');
    }
}
