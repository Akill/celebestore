<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_custom_field_valueAPIRequest;
use App\Http\Requests\API\Updateoc_custom_field_valueAPIRequest;
use App\Models\oc_custom_field_value;
use App\Repositories\oc_custom_field_valueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_custom_field_valueController
 * @package App\Http\Controllers\API
 */

class oc_custom_field_valueAPIController extends AppBaseController
{
    /** @var  oc_custom_field_valueRepository */
    private $ocCustomFieldValueRepository;

    public function __construct(oc_custom_field_valueRepository $ocCustomFieldValueRepo)
    {
        $this->ocCustomFieldValueRepository = $ocCustomFieldValueRepo;
    }

    /**
     * Display a listing of the oc_custom_field_value.
     * GET|HEAD /ocCustomFieldValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldValueRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomFieldValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomFieldValues = $this->ocCustomFieldValueRepository->all();

        return $this->sendResponse($ocCustomFieldValues->toArray(), 'Oc Custom Field Values retrieved successfully');
    }

    /**
     * Store a newly created oc_custom_field_value in storage.
     * POST /ocCustomFieldValues
     *
     * @param Createoc_custom_field_valueAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_valueAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldValues = $this->ocCustomFieldValueRepository->create($input);

        return $this->sendResponse($ocCustomFieldValues->toArray(), 'Oc Custom Field Value saved successfully');
    }

    /**
     * Display the specified oc_custom_field_value.
     * GET|HEAD /ocCustomFieldValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_custom_field_value $ocCustomFieldValue */
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            return $this->sendError('Oc Custom Field Value not found');
        }

        return $this->sendResponse($ocCustomFieldValue->toArray(), 'Oc Custom Field Value retrieved successfully');
    }

    /**
     * Update the specified oc_custom_field_value in storage.
     * PUT/PATCH /ocCustomFieldValues/{id}
     *
     * @param  int $id
     * @param Updateoc_custom_field_valueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_valueAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_custom_field_value $ocCustomFieldValue */
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            return $this->sendError('Oc Custom Field Value not found');
        }

        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->update($input, $id);

        return $this->sendResponse($ocCustomFieldValue->toArray(), 'oc_custom_field_value updated successfully');
    }

    /**
     * Remove the specified oc_custom_field_value from storage.
     * DELETE /ocCustomFieldValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_custom_field_value $ocCustomFieldValue */
        $ocCustomFieldValue = $this->ocCustomFieldValueRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValue)) {
            return $this->sendError('Oc Custom Field Value not found');
        }

        $ocCustomFieldValue->delete();

        return $this->sendResponse($id, 'Oc Custom Field Value deleted successfully');
    }
}
