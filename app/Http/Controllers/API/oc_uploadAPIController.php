<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_uploadAPIRequest;
use App\Http\Requests\API\Updateoc_uploadAPIRequest;
use App\Models\oc_upload;
use App\Repositories\oc_uploadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_uploadController
 * @package App\Http\Controllers\API
 */

class oc_uploadAPIController extends AppBaseController
{
    /** @var  oc_uploadRepository */
    private $ocUploadRepository;

    public function __construct(oc_uploadRepository $ocUploadRepo)
    {
        $this->ocUploadRepository = $ocUploadRepo;
    }

    /**
     * Display a listing of the oc_upload.
     * GET|HEAD /ocUploads
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUploadRepository->pushCriteria(new RequestCriteria($request));
        $this->ocUploadRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocUploads = $this->ocUploadRepository->all();

        return $this->sendResponse($ocUploads->toArray(), 'Oc Uploads retrieved successfully');
    }

    /**
     * Store a newly created oc_upload in storage.
     * POST /ocUploads
     *
     * @param Createoc_uploadAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_uploadAPIRequest $request)
    {
        $input = $request->all();

        $ocUploads = $this->ocUploadRepository->create($input);

        return $this->sendResponse($ocUploads->toArray(), 'Oc Upload saved successfully');
    }

    /**
     * Display the specified oc_upload.
     * GET|HEAD /ocUploads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_upload $ocUpload */
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            return $this->sendError('Oc Upload not found');
        }

        return $this->sendResponse($ocUpload->toArray(), 'Oc Upload retrieved successfully');
    }

    /**
     * Update the specified oc_upload in storage.
     * PUT/PATCH /ocUploads/{id}
     *
     * @param  int $id
     * @param Updateoc_uploadAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_uploadAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_upload $ocUpload */
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            return $this->sendError('Oc Upload not found');
        }

        $ocUpload = $this->ocUploadRepository->update($input, $id);

        return $this->sendResponse($ocUpload->toArray(), 'oc_upload updated successfully');
    }

    /**
     * Remove the specified oc_upload from storage.
     * DELETE /ocUploads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_upload $ocUpload */
        $ocUpload = $this->ocUploadRepository->findWithoutFail($id);

        if (empty($ocUpload)) {
            return $this->sendError('Oc Upload not found');
        }

        $ocUpload->delete();

        return $this->sendResponse($id, 'Oc Upload deleted successfully');
    }
}
