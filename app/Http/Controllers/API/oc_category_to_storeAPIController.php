<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_category_to_storeAPIRequest;
use App\Http\Requests\API\Updateoc_category_to_storeAPIRequest;
use App\Models\oc_category_to_store;
use App\Repositories\oc_category_to_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_category_to_storeController
 * @package App\Http\Controllers\API
 */

class oc_category_to_storeAPIController extends AppBaseController
{
    /** @var  oc_category_to_storeRepository */
    private $ocCategoryToStoreRepository;

    public function __construct(oc_category_to_storeRepository $ocCategoryToStoreRepo)
    {
        $this->ocCategoryToStoreRepository = $ocCategoryToStoreRepo;
    }

    /**
     * Display a listing of the oc_category_to_store.
     * GET|HEAD /ocCategoryToStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryToStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryToStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategoryToStores = $this->ocCategoryToStoreRepository->all();

        return $this->sendResponse($ocCategoryToStores->toArray(), 'Oc Category To Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_category_to_store in storage.
     * POST /ocCategoryToStores
     *
     * @param Createoc_category_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_to_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocCategoryToStores = $this->ocCategoryToStoreRepository->create($input);

        return $this->sendResponse($ocCategoryToStores->toArray(), 'Oc Category To Store saved successfully');
    }

    /**
     * Display the specified oc_category_to_store.
     * GET|HEAD /ocCategoryToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category_to_store $ocCategoryToStore */
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            return $this->sendError('Oc Category To Store not found');
        }

        return $this->sendResponse($ocCategoryToStore->toArray(), 'Oc Category To Store retrieved successfully');
    }

    /**
     * Update the specified oc_category_to_store in storage.
     * PUT/PATCH /ocCategoryToStores/{id}
     *
     * @param  int $id
     * @param Updateoc_category_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_to_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category_to_store $ocCategoryToStore */
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            return $this->sendError('Oc Category To Store not found');
        }

        $ocCategoryToStore = $this->ocCategoryToStoreRepository->update($input, $id);

        return $this->sendResponse($ocCategoryToStore->toArray(), 'oc_category_to_store updated successfully');
    }

    /**
     * Remove the specified oc_category_to_store from storage.
     * DELETE /ocCategoryToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category_to_store $ocCategoryToStore */
        $ocCategoryToStore = $this->ocCategoryToStoreRepository->findWithoutFail($id);

        if (empty($ocCategoryToStore)) {
            return $this->sendError('Oc Category To Store not found');
        }

        $ocCategoryToStore->delete();

        return $this->sendResponse($id, 'Oc Category To Store deleted successfully');
    }
}
