<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_bannerAPIRequest;
use App\Http\Requests\API\Updateoc_bannerAPIRequest;
use App\Models\oc_banner;
use App\Repositories\oc_bannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_bannerController
 * @package App\Http\Controllers\API
 */

class oc_bannerAPIController extends AppBaseController
{
    /** @var  oc_bannerRepository */
    private $ocBannerRepository;

    public function __construct(oc_bannerRepository $ocBannerRepo)
    {
        $this->ocBannerRepository = $ocBannerRepo;
    }

    /**
     * Display a listing of the oc_banner.
     * GET|HEAD /ocBanners
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerRepository->pushCriteria(new RequestCriteria($request));
        $this->ocBannerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocBanners = $this->ocBannerRepository->all();

        return $this->sendResponse($ocBanners->toArray(), 'Oc Banners retrieved successfully');
    }

    /**
     * Store a newly created oc_banner in storage.
     * POST /ocBanners
     *
     * @param Createoc_bannerAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_bannerAPIRequest $request)
    {
        $input = $request->all();

        $ocBanners = $this->ocBannerRepository->create($input);

        return $this->sendResponse($ocBanners->toArray(), 'Oc Banner saved successfully');
    }

    /**
     * Display the specified oc_banner.
     * GET|HEAD /ocBanners/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_banner $ocBanner */
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            return $this->sendError('Oc Banner not found');
        }

        return $this->sendResponse($ocBanner->toArray(), 'Oc Banner retrieved successfully');
    }

    /**
     * Update the specified oc_banner in storage.
     * PUT/PATCH /ocBanners/{id}
     *
     * @param  int $id
     * @param Updateoc_bannerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_bannerAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_banner $ocBanner */
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            return $this->sendError('Oc Banner not found');
        }

        $ocBanner = $this->ocBannerRepository->update($input, $id);

        return $this->sendResponse($ocBanner->toArray(), 'oc_banner updated successfully');
    }

    /**
     * Remove the specified oc_banner from storage.
     * DELETE /ocBanners/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_banner $ocBanner */
        $ocBanner = $this->ocBannerRepository->findWithoutFail($id);

        if (empty($ocBanner)) {
            return $this->sendError('Oc Banner not found');
        }

        $ocBanner->delete();

        return $this->sendResponse($id, 'Oc Banner deleted successfully');
    }
}
