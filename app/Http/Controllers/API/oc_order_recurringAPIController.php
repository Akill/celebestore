<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_recurringAPIRequest;
use App\Http\Requests\API\Updateoc_order_recurringAPIRequest;
use App\Models\oc_order_recurring;
use App\Repositories\oc_order_recurringRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_recurringController
 * @package App\Http\Controllers\API
 */

class oc_order_recurringAPIController extends AppBaseController
{
    /** @var  oc_order_recurringRepository */
    private $ocOrderRecurringRepository;

    public function __construct(oc_order_recurringRepository $ocOrderRecurringRepo)
    {
        $this->ocOrderRecurringRepository = $ocOrderRecurringRepo;
    }

    /**
     * Display a listing of the oc_order_recurring.
     * GET|HEAD /ocOrderRecurrings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRecurringRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderRecurringRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderRecurrings = $this->ocOrderRecurringRepository->all();

        return $this->sendResponse($ocOrderRecurrings->toArray(), 'Oc Order Recurrings retrieved successfully');
    }

    /**
     * Store a newly created oc_order_recurring in storage.
     * POST /ocOrderRecurrings
     *
     * @param Createoc_order_recurringAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_recurringAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderRecurrings = $this->ocOrderRecurringRepository->create($input);

        return $this->sendResponse($ocOrderRecurrings->toArray(), 'Oc Order Recurring saved successfully');
    }

    /**
     * Display the specified oc_order_recurring.
     * GET|HEAD /ocOrderRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_recurring $ocOrderRecurring */
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            return $this->sendError('Oc Order Recurring not found');
        }

        return $this->sendResponse($ocOrderRecurring->toArray(), 'Oc Order Recurring retrieved successfully');
    }

    /**
     * Update the specified oc_order_recurring in storage.
     * PUT/PATCH /ocOrderRecurrings/{id}
     *
     * @param  int $id
     * @param Updateoc_order_recurringAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_recurringAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_recurring $ocOrderRecurring */
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            return $this->sendError('Oc Order Recurring not found');
        }

        $ocOrderRecurring = $this->ocOrderRecurringRepository->update($input, $id);

        return $this->sendResponse($ocOrderRecurring->toArray(), 'oc_order_recurring updated successfully');
    }

    /**
     * Remove the specified oc_order_recurring from storage.
     * DELETE /ocOrderRecurrings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_recurring $ocOrderRecurring */
        $ocOrderRecurring = $this->ocOrderRecurringRepository->findWithoutFail($id);

        if (empty($ocOrderRecurring)) {
            return $this->sendError('Oc Order Recurring not found');
        }

        $ocOrderRecurring->delete();

        return $this->sendResponse($id, 'Oc Order Recurring deleted successfully');
    }
}
