<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_countryAPIRequest;
use App\Http\Requests\API\Updateoc_countryAPIRequest;
use App\Models\oc_country;
use App\Repositories\oc_countryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_countryController
 * @package App\Http\Controllers\API
 */

class oc_countryAPIController extends AppBaseController
{
    /** @var  oc_countryRepository */
    private $ocCountryRepository;

    public function __construct(oc_countryRepository $ocCountryRepo)
    {
        $this->ocCountryRepository = $ocCountryRepo;
    }

    /**
     * Display a listing of the oc_country.
     * GET|HEAD /ocCountries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCountryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCountryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCountries = $this->ocCountryRepository->all();

        return $this->sendResponse($ocCountries->toArray(), 'Oc Countries retrieved successfully');
    }

    /**
     * Store a newly created oc_country in storage.
     * POST /ocCountries
     *
     * @param Createoc_countryAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_countryAPIRequest $request)
    {
        $input = $request->all();

        $ocCountries = $this->ocCountryRepository->create($input);

        return $this->sendResponse($ocCountries->toArray(), 'Oc Country saved successfully');
    }

    /**
     * Display the specified oc_country.
     * GET|HEAD /ocCountries/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_country $ocCountry */
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            return $this->sendError('Oc Country not found');
        }

        return $this->sendResponse($ocCountry->toArray(), 'Oc Country retrieved successfully');
    }

    /**
     * Update the specified oc_country in storage.
     * PUT/PATCH /ocCountries/{id}
     *
     * @param  int $id
     * @param Updateoc_countryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_countryAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_country $ocCountry */
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            return $this->sendError('Oc Country not found');
        }

        $ocCountry = $this->ocCountryRepository->update($input, $id);

        return $this->sendResponse($ocCountry->toArray(), 'oc_country updated successfully');
    }

    /**
     * Remove the specified oc_country from storage.
     * DELETE /ocCountries/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_country $ocCountry */
        $ocCountry = $this->ocCountryRepository->findWithoutFail($id);

        if (empty($ocCountry)) {
            return $this->sendError('Oc Country not found');
        }

        $ocCountry->delete();

        return $this->sendResponse($id, 'Oc Country deleted successfully');
    }
}
