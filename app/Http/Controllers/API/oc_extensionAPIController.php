<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_extensionAPIRequest;
use App\Http\Requests\API\Updateoc_extensionAPIRequest;
use App\Models\oc_extension;
use App\Repositories\oc_extensionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_extensionController
 * @package App\Http\Controllers\API
 */

class oc_extensionAPIController extends AppBaseController
{
    /** @var  oc_extensionRepository */
    private $ocExtensionRepository;

    public function __construct(oc_extensionRepository $ocExtensionRepo)
    {
        $this->ocExtensionRepository = $ocExtensionRepo;
    }

    /**
     * Display a listing of the oc_extension.
     * GET|HEAD /ocExtensions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocExtensionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocExtensionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocExtensions = $this->ocExtensionRepository->all();

        return $this->sendResponse($ocExtensions->toArray(), 'Oc Extensions retrieved successfully');
    }

    /**
     * Store a newly created oc_extension in storage.
     * POST /ocExtensions
     *
     * @param Createoc_extensionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_extensionAPIRequest $request)
    {
        $input = $request->all();

        $ocExtensions = $this->ocExtensionRepository->create($input);

        return $this->sendResponse($ocExtensions->toArray(), 'Oc Extension saved successfully');
    }

    /**
     * Display the specified oc_extension.
     * GET|HEAD /ocExtensions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_extension $ocExtension */
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            return $this->sendError('Oc Extension not found');
        }

        return $this->sendResponse($ocExtension->toArray(), 'Oc Extension retrieved successfully');
    }

    /**
     * Update the specified oc_extension in storage.
     * PUT/PATCH /ocExtensions/{id}
     *
     * @param  int $id
     * @param Updateoc_extensionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_extensionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_extension $ocExtension */
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            return $this->sendError('Oc Extension not found');
        }

        $ocExtension = $this->ocExtensionRepository->update($input, $id);

        return $this->sendResponse($ocExtension->toArray(), 'oc_extension updated successfully');
    }

    /**
     * Remove the specified oc_extension from storage.
     * DELETE /ocExtensions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_extension $ocExtension */
        $ocExtension = $this->ocExtensionRepository->findWithoutFail($id);

        if (empty($ocExtension)) {
            return $this->sendError('Oc Extension not found');
        }

        $ocExtension->delete();

        return $this->sendResponse($id, 'Oc Extension deleted successfully');
    }
}
