<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_attributeAPIRequest;
use App\Http\Requests\API\Updateoc_attributeAPIRequest;
use App\Models\oc_attribute;
use App\Repositories\oc_attributeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_attributeController
 * @package App\Http\Controllers\API
 */

class oc_attributeAPIController extends AppBaseController
{
    /** @var  oc_attributeRepository */
    private $ocAttributeRepository;

    public function __construct(oc_attributeRepository $ocAttributeRepo)
    {
        $this->ocAttributeRepository = $ocAttributeRepo;
    }

    /**
     * Display a listing of the oc_attribute.
     * GET|HEAD /ocAttributes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAttributeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAttributes = $this->ocAttributeRepository->all();

        return $this->sendResponse($ocAttributes->toArray(), 'Oc Attributes retrieved successfully');
    }

    /**
     * Store a newly created oc_attribute in storage.
     * POST /ocAttributes
     *
     * @param Createoc_attributeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attributeAPIRequest $request)
    {
        $input = $request->all();

        $ocAttributes = $this->ocAttributeRepository->create($input);

        return $this->sendResponse($ocAttributes->toArray(), 'Oc Attribute saved successfully');
    }

    /**
     * Display the specified oc_attribute.
     * GET|HEAD /ocAttributes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_attribute $ocAttribute */
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            return $this->sendError('Oc Attribute not found');
        }

        return $this->sendResponse($ocAttribute->toArray(), 'Oc Attribute retrieved successfully');
    }

    /**
     * Update the specified oc_attribute in storage.
     * PUT/PATCH /ocAttributes/{id}
     *
     * @param  int $id
     * @param Updateoc_attributeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attributeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_attribute $ocAttribute */
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            return $this->sendError('Oc Attribute not found');
        }

        $ocAttribute = $this->ocAttributeRepository->update($input, $id);

        return $this->sendResponse($ocAttribute->toArray(), 'oc_attribute updated successfully');
    }

    /**
     * Remove the specified oc_attribute from storage.
     * DELETE /ocAttributes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_attribute $ocAttribute */
        $ocAttribute = $this->ocAttributeRepository->findWithoutFail($id);

        if (empty($ocAttribute)) {
            return $this->sendError('Oc Attribute not found');
        }

        $ocAttribute->delete();

        return $this->sendResponse($id, 'Oc Attribute deleted successfully');
    }
}
