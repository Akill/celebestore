<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_option_valueAPIRequest;
use App\Http\Requests\API\Updateoc_product_option_valueAPIRequest;
use App\Models\oc_product_option_value;
use App\Repositories\oc_product_option_valueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_option_valueController
 * @package App\Http\Controllers\API
 */

class oc_product_option_valueAPIController extends AppBaseController
{
    /** @var  oc_product_option_valueRepository */
    private $ocProductOptionValueRepository;

    public function __construct(oc_product_option_valueRepository $ocProductOptionValueRepo)
    {
        $this->ocProductOptionValueRepository = $ocProductOptionValueRepo;
    }

    /**
     * Display a listing of the oc_product_option_value.
     * GET|HEAD /ocProductOptionValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductOptionValueRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductOptionValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductOptionValues = $this->ocProductOptionValueRepository->all();

        return $this->sendResponse($ocProductOptionValues->toArray(), 'Oc Product Option Values retrieved successfully');
    }

    /**
     * Store a newly created oc_product_option_value in storage.
     * POST /ocProductOptionValues
     *
     * @param Createoc_product_option_valueAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_option_valueAPIRequest $request)
    {
        $input = $request->all();

        $ocProductOptionValues = $this->ocProductOptionValueRepository->create($input);

        return $this->sendResponse($ocProductOptionValues->toArray(), 'Oc Product Option Value saved successfully');
    }

    /**
     * Display the specified oc_product_option_value.
     * GET|HEAD /ocProductOptionValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_option_value $ocProductOptionValue */
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            return $this->sendError('Oc Product Option Value not found');
        }

        return $this->sendResponse($ocProductOptionValue->toArray(), 'Oc Product Option Value retrieved successfully');
    }

    /**
     * Update the specified oc_product_option_value in storage.
     * PUT/PATCH /ocProductOptionValues/{id}
     *
     * @param  int $id
     * @param Updateoc_product_option_valueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_option_valueAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_option_value $ocProductOptionValue */
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            return $this->sendError('Oc Product Option Value not found');
        }

        $ocProductOptionValue = $this->ocProductOptionValueRepository->update($input, $id);

        return $this->sendResponse($ocProductOptionValue->toArray(), 'oc_product_option_value updated successfully');
    }

    /**
     * Remove the specified oc_product_option_value from storage.
     * DELETE /ocProductOptionValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_option_value $ocProductOptionValue */
        $ocProductOptionValue = $this->ocProductOptionValueRepository->findWithoutFail($id);

        if (empty($ocProductOptionValue)) {
            return $this->sendError('Oc Product Option Value not found');
        }

        $ocProductOptionValue->delete();

        return $this->sendResponse($id, 'Oc Product Option Value deleted successfully');
    }
}
