<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_layoutAPIRequest;
use App\Http\Requests\API\Updateoc_layoutAPIRequest;
use App\Models\oc_layout;
use App\Repositories\oc_layoutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_layoutController
 * @package App\Http\Controllers\API
 */

class oc_layoutAPIController extends AppBaseController
{
    /** @var  oc_layoutRepository */
    private $ocLayoutRepository;

    public function __construct(oc_layoutRepository $ocLayoutRepo)
    {
        $this->ocLayoutRepository = $ocLayoutRepo;
    }

    /**
     * Display a listing of the oc_layout.
     * GET|HEAD /ocLayouts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLayoutRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLayoutRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLayouts = $this->ocLayoutRepository->all();

        return $this->sendResponse($ocLayouts->toArray(), 'Oc Layouts retrieved successfully');
    }

    /**
     * Store a newly created oc_layout in storage.
     * POST /ocLayouts
     *
     * @param Createoc_layoutAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_layoutAPIRequest $request)
    {
        $input = $request->all();

        $ocLayouts = $this->ocLayoutRepository->create($input);

        return $this->sendResponse($ocLayouts->toArray(), 'Oc Layout saved successfully');
    }

    /**
     * Display the specified oc_layout.
     * GET|HEAD /ocLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_layout $ocLayout */
        $ocLayout = $this->ocLayoutRepository->findWithoutFail($id);

        if (empty($ocLayout)) {
            return $this->sendError('Oc Layout not found');
        }

        return $this->sendResponse($ocLayout->toArray(), 'Oc Layout retrieved successfully');
    }

    /**
     * Update the specified oc_layout in storage.
     * PUT/PATCH /ocLayouts/{id}
     *
     * @param  int $id
     * @param Updateoc_layoutAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_layoutAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_layout $ocLayout */
        $ocLayout = $this->ocLayoutRepository->findWithoutFail($id);

        if (empty($ocLayout)) {
            return $this->sendError('Oc Layout not found');
        }

        $ocLayout = $this->ocLayoutRepository->update($input, $id);

        return $this->sendResponse($ocLayout->toArray(), 'oc_layout updated successfully');
    }

    /**
     * Remove the specified oc_layout from storage.
     * DELETE /ocLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_layout $ocLayout */
        $ocLayout = $this->ocLayoutRepository->findWithoutFail($id);

        if (empty($ocLayout)) {
            return $this->sendError('Oc Layout not found');
        }

        $ocLayout->delete();

        return $this->sendResponse($id, 'Oc Layout deleted successfully');
    }
}
