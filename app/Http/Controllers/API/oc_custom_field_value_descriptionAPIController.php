<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_custom_field_value_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_custom_field_value_descriptionAPIRequest;
use App\Models\oc_custom_field_value_description;
use App\Repositories\oc_custom_field_value_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_custom_field_value_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_custom_field_value_descriptionAPIController extends AppBaseController
{
    /** @var  oc_custom_field_value_descriptionRepository */
    private $ocCustomFieldValueDescriptionRepository;

    public function __construct(oc_custom_field_value_descriptionRepository $ocCustomFieldValueDescriptionRepo)
    {
        $this->ocCustomFieldValueDescriptionRepository = $ocCustomFieldValueDescriptionRepo;
    }

    /**
     * Display a listing of the oc_custom_field_value_description.
     * GET|HEAD /ocCustomFieldValueDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldValueDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomFieldValueDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomFieldValueDescriptions = $this->ocCustomFieldValueDescriptionRepository->all();

        return $this->sendResponse($ocCustomFieldValueDescriptions->toArray(), 'Oc Custom Field Value Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_custom_field_value_description in storage.
     * POST /ocCustomFieldValueDescriptions
     *
     * @param Createoc_custom_field_value_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_value_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldValueDescriptions = $this->ocCustomFieldValueDescriptionRepository->create($input);

        return $this->sendResponse($ocCustomFieldValueDescriptions->toArray(), 'Oc Custom Field Value Description saved successfully');
    }

    /**
     * Display the specified oc_custom_field_value_description.
     * GET|HEAD /ocCustomFieldValueDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_custom_field_value_description $ocCustomFieldValueDescription */
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            return $this->sendError('Oc Custom Field Value Description not found');
        }

        return $this->sendResponse($ocCustomFieldValueDescription->toArray(), 'Oc Custom Field Value Description retrieved successfully');
    }

    /**
     * Update the specified oc_custom_field_value_description in storage.
     * PUT/PATCH /ocCustomFieldValueDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_custom_field_value_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_value_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_custom_field_value_description $ocCustomFieldValueDescription */
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            return $this->sendError('Oc Custom Field Value Description not found');
        }

        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocCustomFieldValueDescription->toArray(), 'oc_custom_field_value_description updated successfully');
    }

    /**
     * Remove the specified oc_custom_field_value_description from storage.
     * DELETE /ocCustomFieldValueDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_custom_field_value_description $ocCustomFieldValueDescription */
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            return $this->sendError('Oc Custom Field Value Description not found');
        }

        $ocCustomFieldValueDescription->delete();

        return $this->sendResponse($id, 'Oc Custom Field Value Description deleted successfully');
    }
}
