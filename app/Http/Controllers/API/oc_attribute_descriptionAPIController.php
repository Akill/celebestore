<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_attribute_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_attribute_descriptionAPIRequest;
use App\Models\oc_attribute_description;
use App\Repositories\oc_attribute_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_attribute_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_attribute_descriptionAPIController extends AppBaseController
{
    /** @var  oc_attribute_descriptionRepository */
    private $ocAttributeDescriptionRepository;

    public function __construct(oc_attribute_descriptionRepository $ocAttributeDescriptionRepo)
    {
        $this->ocAttributeDescriptionRepository = $ocAttributeDescriptionRepo;
    }

    /**
     * Display a listing of the oc_attribute_description.
     * GET|HEAD /ocAttributeDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAttributeDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAttributeDescriptions = $this->ocAttributeDescriptionRepository->all();

        return $this->sendResponse($ocAttributeDescriptions->toArray(), 'Oc Attribute Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_attribute_description in storage.
     * POST /ocAttributeDescriptions
     *
     * @param Createoc_attribute_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocAttributeDescriptions = $this->ocAttributeDescriptionRepository->create($input);

        return $this->sendResponse($ocAttributeDescriptions->toArray(), 'Oc Attribute Description saved successfully');
    }

    /**
     * Display the specified oc_attribute_description.
     * GET|HEAD /ocAttributeDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_attribute_description $ocAttributeDescription */
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            return $this->sendError('Oc Attribute Description not found');
        }

        return $this->sendResponse($ocAttributeDescription->toArray(), 'Oc Attribute Description retrieved successfully');
    }

    /**
     * Update the specified oc_attribute_description in storage.
     * PUT/PATCH /ocAttributeDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_attribute_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_attribute_description $ocAttributeDescription */
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            return $this->sendError('Oc Attribute Description not found');
        }

        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocAttributeDescription->toArray(), 'oc_attribute_description updated successfully');
    }

    /**
     * Remove the specified oc_attribute_description from storage.
     * DELETE /ocAttributeDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_attribute_description $ocAttributeDescription */
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            return $this->sendError('Oc Attribute Description not found');
        }

        $ocAttributeDescription->delete();

        return $this->sendResponse($id, 'Oc Attribute Description deleted successfully');
    }
}
