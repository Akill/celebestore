<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_storeAPIRequest;
use App\Http\Requests\API\Updateoc_storeAPIRequest;
use App\Models\oc_store;
use App\Repositories\oc_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_storeController
 * @package App\Http\Controllers\API
 */

class oc_storeAPIController extends AppBaseController
{
    /** @var  oc_storeRepository */
    private $ocStoreRepository;

    public function __construct(oc_storeRepository $ocStoreRepo)
    {
        $this->ocStoreRepository = $ocStoreRepo;
    }

    /**
     * Display a listing of the oc_store.
     * GET|HEAD /ocStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocStores = $this->ocStoreRepository->all();

        return $this->sendResponse($ocStores->toArray(), 'Oc Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_store in storage.
     * POST /ocStores
     *
     * @param Createoc_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocStores = $this->ocStoreRepository->create($input);

        return $this->sendResponse($ocStores->toArray(), 'Oc Store saved successfully');
    }

    /**
     * Display the specified oc_store.
     * GET|HEAD /ocStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_store $ocStore */
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            return $this->sendError('Oc Store not found');
        }

        return $this->sendResponse($ocStore->toArray(), 'Oc Store retrieved successfully');
    }

    /**
     * Update the specified oc_store in storage.
     * PUT/PATCH /ocStores/{id}
     *
     * @param  int $id
     * @param Updateoc_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_store $ocStore */
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            return $this->sendError('Oc Store not found');
        }

        $ocStore = $this->ocStoreRepository->update($input, $id);

        return $this->sendResponse($ocStore->toArray(), 'oc_store updated successfully');
    }

    /**
     * Remove the specified oc_store from storage.
     * DELETE /ocStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_store $ocStore */
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            return $this->sendError('Oc Store not found');
        }

        $ocStore->delete();

        return $this->sendResponse($id, 'Oc Store deleted successfully');
    }
}
