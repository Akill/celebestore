<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_to_storeAPIRequest;
use App\Http\Requests\API\Updateoc_product_to_storeAPIRequest;
use App\Models\oc_product_to_store;
use App\Repositories\oc_product_to_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_to_storeController
 * @package App\Http\Controllers\API
 */

class oc_product_to_storeAPIController extends AppBaseController
{
    /** @var  oc_product_to_storeRepository */
    private $ocProductToStoreRepository;

    public function __construct(oc_product_to_storeRepository $ocProductToStoreRepo)
    {
        $this->ocProductToStoreRepository = $ocProductToStoreRepo;
    }

    /**
     * Display a listing of the oc_product_to_store.
     * GET|HEAD /ocProductToStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductToStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductToStores = $this->ocProductToStoreRepository->all();

        return $this->sendResponse($ocProductToStores->toArray(), 'Oc Product To Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_product_to_store in storage.
     * POST /ocProductToStores
     *
     * @param Createoc_product_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocProductToStores = $this->ocProductToStoreRepository->create($input);

        return $this->sendResponse($ocProductToStores->toArray(), 'Oc Product To Store saved successfully');
    }

    /**
     * Display the specified oc_product_to_store.
     * GET|HEAD /ocProductToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_to_store $ocProductToStore */
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            return $this->sendError('Oc Product To Store not found');
        }

        return $this->sendResponse($ocProductToStore->toArray(), 'Oc Product To Store retrieved successfully');
    }

    /**
     * Update the specified oc_product_to_store in storage.
     * PUT/PATCH /ocProductToStores/{id}
     *
     * @param  int $id
     * @param Updateoc_product_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_to_store $ocProductToStore */
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            return $this->sendError('Oc Product To Store not found');
        }

        $ocProductToStore = $this->ocProductToStoreRepository->update($input, $id);

        return $this->sendResponse($ocProductToStore->toArray(), 'oc_product_to_store updated successfully');
    }

    /**
     * Remove the specified oc_product_to_store from storage.
     * DELETE /ocProductToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_to_store $ocProductToStore */
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            return $this->sendError('Oc Product To Store not found');
        }

        $ocProductToStore->delete();

        return $this->sendResponse($id, 'Oc Product To Store deleted successfully');
    }
}
