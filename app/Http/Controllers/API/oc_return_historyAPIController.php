<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_return_historyAPIRequest;
use App\Http\Requests\API\Updateoc_return_historyAPIRequest;
use App\Models\oc_return_history;
use App\Repositories\oc_return_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_return_historyController
 * @package App\Http\Controllers\API
 */

class oc_return_historyAPIController extends AppBaseController
{
    /** @var  oc_return_historyRepository */
    private $ocReturnHistoryRepository;

    public function __construct(oc_return_historyRepository $ocReturnHistoryRepo)
    {
        $this->ocReturnHistoryRepository = $ocReturnHistoryRepo;
    }

    /**
     * Display a listing of the oc_return_history.
     * GET|HEAD /ocReturnHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnHistoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReturnHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReturnHistories = $this->ocReturnHistoryRepository->all();

        return $this->sendResponse($ocReturnHistories->toArray(), 'Oc Return Histories retrieved successfully');
    }

    /**
     * Store a newly created oc_return_history in storage.
     * POST /ocReturnHistories
     *
     * @param Createoc_return_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_return_historyAPIRequest $request)
    {
        $input = $request->all();

        $ocReturnHistories = $this->ocReturnHistoryRepository->create($input);

        return $this->sendResponse($ocReturnHistories->toArray(), 'Oc Return History saved successfully');
    }

    /**
     * Display the specified oc_return_history.
     * GET|HEAD /ocReturnHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_return_history $ocReturnHistory */
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            return $this->sendError('Oc Return History not found');
        }

        return $this->sendResponse($ocReturnHistory->toArray(), 'Oc Return History retrieved successfully');
    }

    /**
     * Update the specified oc_return_history in storage.
     * PUT/PATCH /ocReturnHistories/{id}
     *
     * @param  int $id
     * @param Updateoc_return_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_return_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_return_history $ocReturnHistory */
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            return $this->sendError('Oc Return History not found');
        }

        $ocReturnHistory = $this->ocReturnHistoryRepository->update($input, $id);

        return $this->sendResponse($ocReturnHistory->toArray(), 'oc_return_history updated successfully');
    }

    /**
     * Remove the specified oc_return_history from storage.
     * DELETE /ocReturnHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_return_history $ocReturnHistory */
        $ocReturnHistory = $this->ocReturnHistoryRepository->findWithoutFail($id);

        if (empty($ocReturnHistory)) {
            return $this->sendError('Oc Return History not found');
        }

        $ocReturnHistory->delete();

        return $this->sendResponse($id, 'Oc Return History deleted successfully');
    }
}
