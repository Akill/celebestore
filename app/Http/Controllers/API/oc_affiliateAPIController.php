<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_affiliateAPIRequest;
use App\Http\Requests\API\Updateoc_affiliateAPIRequest;
use App\Models\oc_affiliate;
use App\Repositories\oc_affiliateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_affiliateController
 * @package App\Http\Controllers\API
 */

class oc_affiliateAPIController extends AppBaseController
{
    /** @var  oc_affiliateRepository */
    private $ocAffiliateRepository;

    public function __construct(oc_affiliateRepository $ocAffiliateRepo)
    {
        $this->ocAffiliateRepository = $ocAffiliateRepo;
    }

    /**
     * Display a listing of the oc_affiliate.
     * GET|HEAD /ocAffiliates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAffiliateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAffiliates = $this->ocAffiliateRepository->all();

        return $this->sendResponse($ocAffiliates->toArray(), 'Oc Affiliates retrieved successfully');
    }

    /**
     * Store a newly created oc_affiliate in storage.
     * POST /ocAffiliates
     *
     * @param Createoc_affiliateAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliateAPIRequest $request)
    {
        $input = $request->all();

        $ocAffiliates = $this->ocAffiliateRepository->create($input);

        return $this->sendResponse($ocAffiliates->toArray(), 'Oc Affiliate saved successfully');
    }

    /**
     * Display the specified oc_affiliate.
     * GET|HEAD /ocAffiliates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_affiliate $ocAffiliate */
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            return $this->sendError('Oc Affiliate not found');
        }

        return $this->sendResponse($ocAffiliate->toArray(), 'Oc Affiliate retrieved successfully');
    }

    /**
     * Update the specified oc_affiliate in storage.
     * PUT/PATCH /ocAffiliates/{id}
     *
     * @param  int $id
     * @param Updateoc_affiliateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliateAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_affiliate $ocAffiliate */
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            return $this->sendError('Oc Affiliate not found');
        }

        $ocAffiliate = $this->ocAffiliateRepository->update($input, $id);

        return $this->sendResponse($ocAffiliate->toArray(), 'oc_affiliate updated successfully');
    }

    /**
     * Remove the specified oc_affiliate from storage.
     * DELETE /ocAffiliates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_affiliate $ocAffiliate */
        $ocAffiliate = $this->ocAffiliateRepository->findWithoutFail($id);

        if (empty($ocAffiliate)) {
            return $this->sendError('Oc Affiliate not found');
        }

        $ocAffiliate->delete();

        return $this->sendResponse($id, 'Oc Affiliate deleted successfully');
    }
}
