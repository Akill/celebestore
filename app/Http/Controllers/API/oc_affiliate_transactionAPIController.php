<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_affiliate_transactionAPIRequest;
use App\Http\Requests\API\Updateoc_affiliate_transactionAPIRequest;
use App\Models\oc_affiliate_transaction;
use App\Repositories\oc_affiliate_transactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_affiliate_transactionController
 * @package App\Http\Controllers\API
 */

class oc_affiliate_transactionAPIController extends AppBaseController
{
    /** @var  oc_affiliate_transactionRepository */
    private $ocAffiliateTransactionRepository;

    public function __construct(oc_affiliate_transactionRepository $ocAffiliateTransactionRepo)
    {
        $this->ocAffiliateTransactionRepository = $ocAffiliateTransactionRepo;
    }

    /**
     * Display a listing of the oc_affiliate_transaction.
     * GET|HEAD /ocAffiliateTransactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateTransactionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAffiliateTransactionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAffiliateTransactions = $this->ocAffiliateTransactionRepository->all();

        return $this->sendResponse($ocAffiliateTransactions->toArray(), 'Oc Affiliate Transactions retrieved successfully');
    }

    /**
     * Store a newly created oc_affiliate_transaction in storage.
     * POST /ocAffiliateTransactions
     *
     * @param Createoc_affiliate_transactionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_transactionAPIRequest $request)
    {
        $input = $request->all();

        $ocAffiliateTransactions = $this->ocAffiliateTransactionRepository->create($input);

        return $this->sendResponse($ocAffiliateTransactions->toArray(), 'Oc Affiliate Transaction saved successfully');
    }

    /**
     * Display the specified oc_affiliate_transaction.
     * GET|HEAD /ocAffiliateTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_affiliate_transaction $ocAffiliateTransaction */
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            return $this->sendError('Oc Affiliate Transaction not found');
        }

        return $this->sendResponse($ocAffiliateTransaction->toArray(), 'Oc Affiliate Transaction retrieved successfully');
    }

    /**
     * Update the specified oc_affiliate_transaction in storage.
     * PUT/PATCH /ocAffiliateTransactions/{id}
     *
     * @param  int $id
     * @param Updateoc_affiliate_transactionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_transactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_affiliate_transaction $ocAffiliateTransaction */
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            return $this->sendError('Oc Affiliate Transaction not found');
        }

        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->update($input, $id);

        return $this->sendResponse($ocAffiliateTransaction->toArray(), 'oc_affiliate_transaction updated successfully');
    }

    /**
     * Remove the specified oc_affiliate_transaction from storage.
     * DELETE /ocAffiliateTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_affiliate_transaction $ocAffiliateTransaction */
        $ocAffiliateTransaction = $this->ocAffiliateTransactionRepository->findWithoutFail($id);

        if (empty($ocAffiliateTransaction)) {
            return $this->sendError('Oc Affiliate Transaction not found');
        }

        $ocAffiliateTransaction->delete();

        return $this->sendResponse($id, 'Oc Affiliate Transaction deleted successfully');
    }
}
