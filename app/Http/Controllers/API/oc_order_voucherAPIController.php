<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_voucherAPIRequest;
use App\Http\Requests\API\Updateoc_order_voucherAPIRequest;
use App\Models\oc_order_voucher;
use App\Repositories\oc_order_voucherRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_voucherController
 * @package App\Http\Controllers\API
 */

class oc_order_voucherAPIController extends AppBaseController
{
    /** @var  oc_order_voucherRepository */
    private $ocOrderVoucherRepository;

    public function __construct(oc_order_voucherRepository $ocOrderVoucherRepo)
    {
        $this->ocOrderVoucherRepository = $ocOrderVoucherRepo;
    }

    /**
     * Display a listing of the oc_order_voucher.
     * GET|HEAD /ocOrderVouchers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderVoucherRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderVoucherRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderVouchers = $this->ocOrderVoucherRepository->all();

        return $this->sendResponse($ocOrderVouchers->toArray(), 'Oc Order Vouchers retrieved successfully');
    }

    /**
     * Store a newly created oc_order_voucher in storage.
     * POST /ocOrderVouchers
     *
     * @param Createoc_order_voucherAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_voucherAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderVouchers = $this->ocOrderVoucherRepository->create($input);

        return $this->sendResponse($ocOrderVouchers->toArray(), 'Oc Order Voucher saved successfully');
    }

    /**
     * Display the specified oc_order_voucher.
     * GET|HEAD /ocOrderVouchers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_voucher $ocOrderVoucher */
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            return $this->sendError('Oc Order Voucher not found');
        }

        return $this->sendResponse($ocOrderVoucher->toArray(), 'Oc Order Voucher retrieved successfully');
    }

    /**
     * Update the specified oc_order_voucher in storage.
     * PUT/PATCH /ocOrderVouchers/{id}
     *
     * @param  int $id
     * @param Updateoc_order_voucherAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_voucherAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_voucher $ocOrderVoucher */
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            return $this->sendError('Oc Order Voucher not found');
        }

        $ocOrderVoucher = $this->ocOrderVoucherRepository->update($input, $id);

        return $this->sendResponse($ocOrderVoucher->toArray(), 'oc_order_voucher updated successfully');
    }

    /**
     * Remove the specified oc_order_voucher from storage.
     * DELETE /ocOrderVouchers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_voucher $ocOrderVoucher */
        $ocOrderVoucher = $this->ocOrderVoucherRepository->findWithoutFail($id);

        if (empty($ocOrderVoucher)) {
            return $this->sendError('Oc Order Voucher not found');
        }

        $ocOrderVoucher->delete();

        return $this->sendResponse($id, 'Oc Order Voucher deleted successfully');
    }
}
