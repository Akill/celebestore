<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_reviewAPIRequest;
use App\Http\Requests\API\Updateoc_reviewAPIRequest;
use App\Models\oc_review;
use App\Repositories\oc_reviewRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_reviewController
 * @package App\Http\Controllers\API
 */

class oc_reviewAPIController extends AppBaseController
{
    /** @var  oc_reviewRepository */
    private $ocReviewRepository;

    public function __construct(oc_reviewRepository $ocReviewRepo)
    {
        $this->ocReviewRepository = $ocReviewRepo;
    }

    /**
     * Display a listing of the oc_review.
     * GET|HEAD /ocReviews
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReviewRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReviewRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReviews = $this->ocReviewRepository->all();

        return $this->sendResponse($ocReviews->toArray(), 'Oc Reviews retrieved successfully');
    }

    /**
     * Store a newly created oc_review in storage.
     * POST /ocReviews
     *
     * @param Createoc_reviewAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_reviewAPIRequest $request)
    {
        $input = $request->all();

        $ocReviews = $this->ocReviewRepository->create($input);

        return $this->sendResponse($ocReviews->toArray(), 'Oc Review saved successfully');
    }

    /**
     * Display the specified oc_review.
     * GET|HEAD /ocReviews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_review $ocReview */
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            return $this->sendError('Oc Review not found');
        }

        return $this->sendResponse($ocReview->toArray(), 'Oc Review retrieved successfully');
    }

    /**
     * Update the specified oc_review in storage.
     * PUT/PATCH /ocReviews/{id}
     *
     * @param  int $id
     * @param Updateoc_reviewAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_reviewAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_review $ocReview */
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            return $this->sendError('Oc Review not found');
        }

        $ocReview = $this->ocReviewRepository->update($input, $id);

        return $this->sendResponse($ocReview->toArray(), 'oc_review updated successfully');
    }

    /**
     * Remove the specified oc_review from storage.
     * DELETE /ocReviews/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_review $ocReview */
        $ocReview = $this->ocReviewRepository->findWithoutFail($id);

        if (empty($ocReview)) {
            return $this->sendError('Oc Review not found');
        }

        $ocReview->delete();

        return $this->sendResponse($id, 'Oc Review deleted successfully');
    }
}
