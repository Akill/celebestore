<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_affiliate_activityAPIRequest;
use App\Http\Requests\API\Updateoc_affiliate_activityAPIRequest;
use App\Models\oc_affiliate_activity;
use App\Repositories\oc_affiliate_activityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_affiliate_activityController
 * @package App\Http\Controllers\API
 */

class oc_affiliate_activityAPIController extends AppBaseController
{
    /** @var  oc_affiliate_activityRepository */
    private $ocAffiliateActivityRepository;

    public function __construct(oc_affiliate_activityRepository $ocAffiliateActivityRepo)
    {
        $this->ocAffiliateActivityRepository = $ocAffiliateActivityRepo;
    }

    /**
     * Display a listing of the oc_affiliate_activity.
     * GET|HEAD /ocAffiliateActivities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateActivityRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAffiliateActivityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAffiliateActivities = $this->ocAffiliateActivityRepository->all();

        return $this->sendResponse($ocAffiliateActivities->toArray(), 'Oc Affiliate Activities retrieved successfully');
    }

    /**
     * Store a newly created oc_affiliate_activity in storage.
     * POST /ocAffiliateActivities
     *
     * @param Createoc_affiliate_activityAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_activityAPIRequest $request)
    {
        $input = $request->all();

        $ocAffiliateActivities = $this->ocAffiliateActivityRepository->create($input);

        return $this->sendResponse($ocAffiliateActivities->toArray(), 'Oc Affiliate Activity saved successfully');
    }

    /**
     * Display the specified oc_affiliate_activity.
     * GET|HEAD /ocAffiliateActivities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_affiliate_activity $ocAffiliateActivity */
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            return $this->sendError('Oc Affiliate Activity not found');
        }

        return $this->sendResponse($ocAffiliateActivity->toArray(), 'Oc Affiliate Activity retrieved successfully');
    }

    /**
     * Update the specified oc_affiliate_activity in storage.
     * PUT/PATCH /ocAffiliateActivities/{id}
     *
     * @param  int $id
     * @param Updateoc_affiliate_activityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_activityAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_affiliate_activity $ocAffiliateActivity */
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            return $this->sendError('Oc Affiliate Activity not found');
        }

        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->update($input, $id);

        return $this->sendResponse($ocAffiliateActivity->toArray(), 'oc_affiliate_activity updated successfully');
    }

    /**
     * Remove the specified oc_affiliate_activity from storage.
     * DELETE /ocAffiliateActivities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_affiliate_activity $ocAffiliateActivity */
        $ocAffiliateActivity = $this->ocAffiliateActivityRepository->findWithoutFail($id);

        if (empty($ocAffiliateActivity)) {
            return $this->sendError('Oc Affiliate Activity not found');
        }

        $ocAffiliateActivity->delete();

        return $this->sendResponse($id, 'Oc Affiliate Activity deleted successfully');
    }
}
