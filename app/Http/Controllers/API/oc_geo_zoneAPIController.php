<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_geo_zoneAPIRequest;
use App\Http\Requests\API\Updateoc_geo_zoneAPIRequest;
use App\Models\oc_geo_zone;
use App\Repositories\oc_geo_zoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_geo_zoneController
 * @package App\Http\Controllers\API
 */

class oc_geo_zoneAPIController extends AppBaseController
{
    /** @var  oc_geo_zoneRepository */
    private $ocGeoZoneRepository;

    public function __construct(oc_geo_zoneRepository $ocGeoZoneRepo)
    {
        $this->ocGeoZoneRepository = $ocGeoZoneRepo;
    }

    /**
     * Display a listing of the oc_geo_zone.
     * GET|HEAD /ocGeoZones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocGeoZoneRepository->pushCriteria(new RequestCriteria($request));
        $this->ocGeoZoneRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocGeoZones = $this->ocGeoZoneRepository->all();

        return $this->sendResponse($ocGeoZones->toArray(), 'Oc Geo Zones retrieved successfully');
    }

    /**
     * Store a newly created oc_geo_zone in storage.
     * POST /ocGeoZones
     *
     * @param Createoc_geo_zoneAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_geo_zoneAPIRequest $request)
    {
        $input = $request->all();

        $ocGeoZones = $this->ocGeoZoneRepository->create($input);

        return $this->sendResponse($ocGeoZones->toArray(), 'Oc Geo Zone saved successfully');
    }

    /**
     * Display the specified oc_geo_zone.
     * GET|HEAD /ocGeoZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_geo_zone $ocGeoZone */
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            return $this->sendError('Oc Geo Zone not found');
        }

        return $this->sendResponse($ocGeoZone->toArray(), 'Oc Geo Zone retrieved successfully');
    }

    /**
     * Update the specified oc_geo_zone in storage.
     * PUT/PATCH /ocGeoZones/{id}
     *
     * @param  int $id
     * @param Updateoc_geo_zoneAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_geo_zoneAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_geo_zone $ocGeoZone */
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            return $this->sendError('Oc Geo Zone not found');
        }

        $ocGeoZone = $this->ocGeoZoneRepository->update($input, $id);

        return $this->sendResponse($ocGeoZone->toArray(), 'oc_geo_zone updated successfully');
    }

    /**
     * Remove the specified oc_geo_zone from storage.
     * DELETE /ocGeoZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_geo_zone $ocGeoZone */
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            return $this->sendError('Oc Geo Zone not found');
        }

        $ocGeoZone->delete();

        return $this->sendResponse($id, 'Oc Geo Zone deleted successfully');
    }
}
