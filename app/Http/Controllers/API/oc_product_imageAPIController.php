<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_imageAPIRequest;
use App\Http\Requests\API\Updateoc_product_imageAPIRequest;
use App\Models\oc_product_image;
use App\Repositories\oc_product_imageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_imageController
 * @package App\Http\Controllers\API
 */

class oc_product_imageAPIController extends AppBaseController
{
    /** @var  oc_product_imageRepository */
    private $ocProductImageRepository;

    public function __construct(oc_product_imageRepository $ocProductImageRepo)
    {
        $this->ocProductImageRepository = $ocProductImageRepo;
    }

    /**
     * Display a listing of the oc_product_image.
     * GET|HEAD /ocProductImages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductImageRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductImageRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductImages = $this->ocProductImageRepository->all();

        return $this->sendResponse($ocProductImages->toArray(), 'Oc Product Images retrieved successfully');
    }

    /**
     * Store a newly created oc_product_image in storage.
     * POST /ocProductImages
     *
     * @param Createoc_product_imageAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_imageAPIRequest $request)
    {
        $input = $request->all();

        $ocProductImages = $this->ocProductImageRepository->create($input);

        return $this->sendResponse($ocProductImages->toArray(), 'Oc Product Image saved successfully');
    }

    /**
     * Display the specified oc_product_image.
     * GET|HEAD /ocProductImages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_image $ocProductImage */
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            return $this->sendError('Oc Product Image not found');
        }

        return $this->sendResponse($ocProductImage->toArray(), 'Oc Product Image retrieved successfully');
    }

    /**
     * Update the specified oc_product_image in storage.
     * PUT/PATCH /ocProductImages/{id}
     *
     * @param  int $id
     * @param Updateoc_product_imageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_imageAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_image $ocProductImage */
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            return $this->sendError('Oc Product Image not found');
        }

        $ocProductImage = $this->ocProductImageRepository->update($input, $id);

        return $this->sendResponse($ocProductImage->toArray(), 'oc_product_image updated successfully');
    }

    /**
     * Remove the specified oc_product_image from storage.
     * DELETE /ocProductImages/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_image $ocProductImage */
        $ocProductImage = $this->ocProductImageRepository->findWithoutFail($id);

        if (empty($ocProductImage)) {
            return $this->sendError('Oc Product Image not found');
        }

        $ocProductImage->delete();

        return $this->sendResponse($id, 'Oc Product Image deleted successfully');
    }
}
