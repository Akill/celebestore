<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_voucher_themeAPIRequest;
use App\Http\Requests\API\Updateoc_voucher_themeAPIRequest;
use App\Models\oc_voucher_theme;
use App\Repositories\oc_voucher_themeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_voucher_themeController
 * @package App\Http\Controllers\API
 */

class oc_voucher_themeAPIController extends AppBaseController
{
    /** @var  oc_voucher_themeRepository */
    private $ocVoucherThemeRepository;

    public function __construct(oc_voucher_themeRepository $ocVoucherThemeRepo)
    {
        $this->ocVoucherThemeRepository = $ocVoucherThemeRepo;
    }

    /**
     * Display a listing of the oc_voucher_theme.
     * GET|HEAD /ocVoucherThemes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherThemeRepository->pushCriteria(new RequestCriteria($request));
        $this->ocVoucherThemeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocVoucherThemes = $this->ocVoucherThemeRepository->all();

        return $this->sendResponse($ocVoucherThemes->toArray(), 'Oc Voucher Themes retrieved successfully');
    }

    /**
     * Store a newly created oc_voucher_theme in storage.
     * POST /ocVoucherThemes
     *
     * @param Createoc_voucher_themeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucher_themeAPIRequest $request)
    {
        $input = $request->all();

        $ocVoucherThemes = $this->ocVoucherThemeRepository->create($input);

        return $this->sendResponse($ocVoucherThemes->toArray(), 'Oc Voucher Theme saved successfully');
    }

    /**
     * Display the specified oc_voucher_theme.
     * GET|HEAD /ocVoucherThemes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_voucher_theme $ocVoucherTheme */
        $ocVoucherTheme = $this->ocVoucherThemeRepository->findWithoutFail($id);

        if (empty($ocVoucherTheme)) {
            return $this->sendError('Oc Voucher Theme not found');
        }

        return $this->sendResponse($ocVoucherTheme->toArray(), 'Oc Voucher Theme retrieved successfully');
    }

    /**
     * Update the specified oc_voucher_theme in storage.
     * PUT/PATCH /ocVoucherThemes/{id}
     *
     * @param  int $id
     * @param Updateoc_voucher_themeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucher_themeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_voucher_theme $ocVoucherTheme */
        $ocVoucherTheme = $this->ocVoucherThemeRepository->findWithoutFail($id);

        if (empty($ocVoucherTheme)) {
            return $this->sendError('Oc Voucher Theme not found');
        }

        $ocVoucherTheme = $this->ocVoucherThemeRepository->update($input, $id);

        return $this->sendResponse($ocVoucherTheme->toArray(), 'oc_voucher_theme updated successfully');
    }

    /**
     * Remove the specified oc_voucher_theme from storage.
     * DELETE /ocVoucherThemes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_voucher_theme $ocVoucherTheme */
        $ocVoucherTheme = $this->ocVoucherThemeRepository->findWithoutFail($id);

        if (empty($ocVoucherTheme)) {
            return $this->sendError('Oc Voucher Theme not found');
        }

        $ocVoucherTheme->delete();

        return $this->sendResponse($id, 'Oc Voucher Theme deleted successfully');
    }
}
