<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_downloadAPIRequest;
use App\Http\Requests\API\Updateoc_downloadAPIRequest;
use App\Models\oc_download;
use App\Repositories\oc_downloadRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_downloadController
 * @package App\Http\Controllers\API
 */

class oc_downloadAPIController extends AppBaseController
{
    /** @var  oc_downloadRepository */
    private $ocDownloadRepository;

    public function __construct(oc_downloadRepository $ocDownloadRepo)
    {
        $this->ocDownloadRepository = $ocDownloadRepo;
    }

    /**
     * Display a listing of the oc_download.
     * GET|HEAD /ocDownloads
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocDownloadRepository->pushCriteria(new RequestCriteria($request));
        $this->ocDownloadRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocDownloads = $this->ocDownloadRepository->all();

        return $this->sendResponse($ocDownloads->toArray(), 'Oc Downloads retrieved successfully');
    }

    /**
     * Store a newly created oc_download in storage.
     * POST /ocDownloads
     *
     * @param Createoc_downloadAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_downloadAPIRequest $request)
    {
        $input = $request->all();

        $ocDownloads = $this->ocDownloadRepository->create($input);

        return $this->sendResponse($ocDownloads->toArray(), 'Oc Download saved successfully');
    }

    /**
     * Display the specified oc_download.
     * GET|HEAD /ocDownloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_download $ocDownload */
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            return $this->sendError('Oc Download not found');
        }

        return $this->sendResponse($ocDownload->toArray(), 'Oc Download retrieved successfully');
    }

    /**
     * Update the specified oc_download in storage.
     * PUT/PATCH /ocDownloads/{id}
     *
     * @param  int $id
     * @param Updateoc_downloadAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_downloadAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_download $ocDownload */
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            return $this->sendError('Oc Download not found');
        }

        $ocDownload = $this->ocDownloadRepository->update($input, $id);

        return $this->sendResponse($ocDownload->toArray(), 'oc_download updated successfully');
    }

    /**
     * Remove the specified oc_download from storage.
     * DELETE /ocDownloads/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_download $ocDownload */
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            return $this->sendError('Oc Download not found');
        }

        $ocDownload->delete();

        return $this->sendResponse($id, 'Oc Download deleted successfully');
    }
}
