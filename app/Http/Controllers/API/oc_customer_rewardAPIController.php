<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_rewardAPIRequest;
use App\Http\Requests\API\Updateoc_customer_rewardAPIRequest;
use App\Models\oc_customer_reward;
use App\Repositories\oc_customer_rewardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_rewardController
 * @package App\Http\Controllers\API
 */

class oc_customer_rewardAPIController extends AppBaseController
{
    /** @var  oc_customer_rewardRepository */
    private $ocCustomerRewardRepository;

    public function __construct(oc_customer_rewardRepository $ocCustomerRewardRepo)
    {
        $this->ocCustomerRewardRepository = $ocCustomerRewardRepo;
    }

    /**
     * Display a listing of the oc_customer_reward.
     * GET|HEAD /ocCustomerRewards
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerRewardRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerRewardRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerRewards = $this->ocCustomerRewardRepository->all();

        return $this->sendResponse($ocCustomerRewards->toArray(), 'Oc Customer Rewards retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_reward in storage.
     * POST /ocCustomerRewards
     *
     * @param Createoc_customer_rewardAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_rewardAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerRewards = $this->ocCustomerRewardRepository->create($input);

        return $this->sendResponse($ocCustomerRewards->toArray(), 'Oc Customer Reward saved successfully');
    }

    /**
     * Display the specified oc_customer_reward.
     * GET|HEAD /ocCustomerRewards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_reward $ocCustomerReward */
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            return $this->sendError('Oc Customer Reward not found');
        }

        return $this->sendResponse($ocCustomerReward->toArray(), 'Oc Customer Reward retrieved successfully');
    }

    /**
     * Update the specified oc_customer_reward in storage.
     * PUT/PATCH /ocCustomerRewards/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_rewardAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_rewardAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_reward $ocCustomerReward */
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            return $this->sendError('Oc Customer Reward not found');
        }

        $ocCustomerReward = $this->ocCustomerRewardRepository->update($input, $id);

        return $this->sendResponse($ocCustomerReward->toArray(), 'oc_customer_reward updated successfully');
    }

    /**
     * Remove the specified oc_customer_reward from storage.
     * DELETE /ocCustomerRewards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_reward $ocCustomerReward */
        $ocCustomerReward = $this->ocCustomerRewardRepository->findWithoutFail($id);

        if (empty($ocCustomerReward)) {
            return $this->sendError('Oc Customer Reward not found');
        }

        $ocCustomerReward->delete();

        return $this->sendResponse($id, 'Oc Customer Reward deleted successfully');
    }
}
