<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_modificationAPIRequest;
use App\Http\Requests\API\Updateoc_modificationAPIRequest;
use App\Models\oc_modification;
use App\Repositories\oc_modificationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_modificationController
 * @package App\Http\Controllers\API
 */

class oc_modificationAPIController extends AppBaseController
{
    /** @var  oc_modificationRepository */
    private $ocModificationRepository;

    public function __construct(oc_modificationRepository $ocModificationRepo)
    {
        $this->ocModificationRepository = $ocModificationRepo;
    }

    /**
     * Display a listing of the oc_modification.
     * GET|HEAD /ocModifications
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocModificationRepository->pushCriteria(new RequestCriteria($request));
        $this->ocModificationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocModifications = $this->ocModificationRepository->all();

        return $this->sendResponse($ocModifications->toArray(), 'Oc Modifications retrieved successfully');
    }

    /**
     * Store a newly created oc_modification in storage.
     * POST /ocModifications
     *
     * @param Createoc_modificationAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_modificationAPIRequest $request)
    {
        $input = $request->all();

        $ocModifications = $this->ocModificationRepository->create($input);

        return $this->sendResponse($ocModifications->toArray(), 'Oc Modification saved successfully');
    }

    /**
     * Display the specified oc_modification.
     * GET|HEAD /ocModifications/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_modification $ocModification */
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            return $this->sendError('Oc Modification not found');
        }

        return $this->sendResponse($ocModification->toArray(), 'Oc Modification retrieved successfully');
    }

    /**
     * Update the specified oc_modification in storage.
     * PUT/PATCH /ocModifications/{id}
     *
     * @param  int $id
     * @param Updateoc_modificationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_modificationAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_modification $ocModification */
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            return $this->sendError('Oc Modification not found');
        }

        $ocModification = $this->ocModificationRepository->update($input, $id);

        return $this->sendResponse($ocModification->toArray(), 'oc_modification updated successfully');
    }

    /**
     * Remove the specified oc_modification from storage.
     * DELETE /ocModifications/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_modification $ocModification */
        $ocModification = $this->ocModificationRepository->findWithoutFail($id);

        if (empty($ocModification)) {
            return $this->sendError('Oc Modification not found');
        }

        $ocModification->delete();

        return $this->sendResponse($id, 'Oc Modification deleted successfully');
    }
}
