<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_manufacturerAPIRequest;
use App\Http\Requests\API\Updateoc_manufacturerAPIRequest;
use App\Models\oc_manufacturer;
use App\Repositories\oc_manufacturerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_manufacturerController
 * @package App\Http\Controllers\API
 */

class oc_manufacturerAPIController extends AppBaseController
{
    /** @var  oc_manufacturerRepository */
    private $ocManufacturerRepository;

    public function __construct(oc_manufacturerRepository $ocManufacturerRepo)
    {
        $this->ocManufacturerRepository = $ocManufacturerRepo;
    }

    /**
     * Display a listing of the oc_manufacturer.
     * GET|HEAD /ocManufacturers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocManufacturerRepository->pushCriteria(new RequestCriteria($request));
        $this->ocManufacturerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocManufacturers = $this->ocManufacturerRepository->all();

        return $this->sendResponse($ocManufacturers->toArray(), 'Oc Manufacturers retrieved successfully');
    }

    /**
     * Store a newly created oc_manufacturer in storage.
     * POST /ocManufacturers
     *
     * @param Createoc_manufacturerAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_manufacturerAPIRequest $request)
    {
        $input = $request->all();

        $ocManufacturers = $this->ocManufacturerRepository->create($input);

        return $this->sendResponse($ocManufacturers->toArray(), 'Oc Manufacturer saved successfully');
    }

    /**
     * Display the specified oc_manufacturer.
     * GET|HEAD /ocManufacturers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_manufacturer $ocManufacturer */
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            return $this->sendError('Oc Manufacturer not found');
        }

        return $this->sendResponse($ocManufacturer->toArray(), 'Oc Manufacturer retrieved successfully');
    }

    /**
     * Update the specified oc_manufacturer in storage.
     * PUT/PATCH /ocManufacturers/{id}
     *
     * @param  int $id
     * @param Updateoc_manufacturerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_manufacturerAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_manufacturer $ocManufacturer */
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            return $this->sendError('Oc Manufacturer not found');
        }

        $ocManufacturer = $this->ocManufacturerRepository->update($input, $id);

        return $this->sendResponse($ocManufacturer->toArray(), 'oc_manufacturer updated successfully');
    }

    /**
     * Remove the specified oc_manufacturer from storage.
     * DELETE /ocManufacturers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_manufacturer $ocManufacturer */
        $ocManufacturer = $this->ocManufacturerRepository->findWithoutFail($id);

        if (empty($ocManufacturer)) {
            return $this->sendError('Oc Manufacturer not found');
        }

        $ocManufacturer->delete();

        return $this->sendResponse($id, 'Oc Manufacturer deleted successfully');
    }
}
