<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_settingAPIRequest;
use App\Http\Requests\API\Updateoc_settingAPIRequest;
use App\Models\oc_setting;
use App\Repositories\oc_settingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_settingController
 * @package App\Http\Controllers\API
 */

class oc_settingAPIController extends AppBaseController
{
    /** @var  oc_settingRepository */
    private $ocSettingRepository;

    public function __construct(oc_settingRepository $ocSettingRepo)
    {
        $this->ocSettingRepository = $ocSettingRepo;
    }

    /**
     * Display a listing of the oc_setting.
     * GET|HEAD /ocSettings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocSettingRepository->pushCriteria(new RequestCriteria($request));
        $this->ocSettingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocSettings = $this->ocSettingRepository->all();

        return $this->sendResponse($ocSettings->toArray(), 'Oc Settings retrieved successfully');
    }

    /**
     * Store a newly created oc_setting in storage.
     * POST /ocSettings
     *
     * @param Createoc_settingAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_settingAPIRequest $request)
    {
        $input = $request->all();

        $ocSettings = $this->ocSettingRepository->create($input);

        return $this->sendResponse($ocSettings->toArray(), 'Oc Setting saved successfully');
    }

    /**
     * Display the specified oc_setting.
     * GET|HEAD /ocSettings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_setting $ocSetting */
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            return $this->sendError('Oc Setting not found');
        }

        return $this->sendResponse($ocSetting->toArray(), 'Oc Setting retrieved successfully');
    }

    /**
     * Update the specified oc_setting in storage.
     * PUT/PATCH /ocSettings/{id}
     *
     * @param  int $id
     * @param Updateoc_settingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_settingAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_setting $ocSetting */
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            return $this->sendError('Oc Setting not found');
        }

        $ocSetting = $this->ocSettingRepository->update($input, $id);

        return $this->sendResponse($ocSetting->toArray(), 'oc_setting updated successfully');
    }

    /**
     * Remove the specified oc_setting from storage.
     * DELETE /ocSettings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_setting $ocSetting */
        $ocSetting = $this->ocSettingRepository->findWithoutFail($id);

        if (empty($ocSetting)) {
            return $this->sendError('Oc Setting not found');
        }

        $ocSetting->delete();

        return $this->sendResponse($id, 'Oc Setting deleted successfully');
    }
}
