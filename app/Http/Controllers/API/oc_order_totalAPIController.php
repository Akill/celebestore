<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_totalAPIRequest;
use App\Http\Requests\API\Updateoc_order_totalAPIRequest;
use App\Models\oc_order_total;
use App\Repositories\oc_order_totalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_totalController
 * @package App\Http\Controllers\API
 */

class oc_order_totalAPIController extends AppBaseController
{
    /** @var  oc_order_totalRepository */
    private $ocOrderTotalRepository;

    public function __construct(oc_order_totalRepository $ocOrderTotalRepo)
    {
        $this->ocOrderTotalRepository = $ocOrderTotalRepo;
    }

    /**
     * Display a listing of the oc_order_total.
     * GET|HEAD /ocOrderTotals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderTotalRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderTotalRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderTotals = $this->ocOrderTotalRepository->all();

        return $this->sendResponse($ocOrderTotals->toArray(), 'Oc Order Totals retrieved successfully');
    }

    /**
     * Store a newly created oc_order_total in storage.
     * POST /ocOrderTotals
     *
     * @param Createoc_order_totalAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_totalAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderTotals = $this->ocOrderTotalRepository->create($input);

        return $this->sendResponse($ocOrderTotals->toArray(), 'Oc Order Total saved successfully');
    }

    /**
     * Display the specified oc_order_total.
     * GET|HEAD /ocOrderTotals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_total $ocOrderTotal */
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            return $this->sendError('Oc Order Total not found');
        }

        return $this->sendResponse($ocOrderTotal->toArray(), 'Oc Order Total retrieved successfully');
    }

    /**
     * Update the specified oc_order_total in storage.
     * PUT/PATCH /ocOrderTotals/{id}
     *
     * @param  int $id
     * @param Updateoc_order_totalAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_totalAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_total $ocOrderTotal */
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            return $this->sendError('Oc Order Total not found');
        }

        $ocOrderTotal = $this->ocOrderTotalRepository->update($input, $id);

        return $this->sendResponse($ocOrderTotal->toArray(), 'oc_order_total updated successfully');
    }

    /**
     * Remove the specified oc_order_total from storage.
     * DELETE /ocOrderTotals/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_total $ocOrderTotal */
        $ocOrderTotal = $this->ocOrderTotalRepository->findWithoutFail($id);

        if (empty($ocOrderTotal)) {
            return $this->sendError('Oc Order Total not found');
        }

        $ocOrderTotal->delete();

        return $this->sendResponse($id, 'Oc Order Total deleted successfully');
    }
}
