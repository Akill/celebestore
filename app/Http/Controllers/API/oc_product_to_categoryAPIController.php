<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_to_categoryAPIRequest;
use App\Http\Requests\API\Updateoc_product_to_categoryAPIRequest;
use App\Models\oc_product_to_category;
use App\Repositories\oc_product_to_categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_to_categoryController
 * @package App\Http\Controllers\API
 */

class oc_product_to_categoryAPIController extends AppBaseController
{
    /** @var  oc_product_to_categoryRepository */
    private $ocProductToCategoryRepository;

    public function __construct(oc_product_to_categoryRepository $ocProductToCategoryRepo)
    {
        $this->ocProductToCategoryRepository = $ocProductToCategoryRepo;
    }

    /**
     * Display a listing of the oc_product_to_category.
     * GET|HEAD /ocProductToCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductToCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductToCategories = $this->ocProductToCategoryRepository->all();

        return $this->sendResponse($ocProductToCategories->toArray(), 'Oc Product To Categories retrieved successfully');
    }

    /**
     * Store a newly created oc_product_to_category in storage.
     * POST /ocProductToCategories
     *
     * @param Createoc_product_to_categoryAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_categoryAPIRequest $request)
    {
        $input = $request->all();

        $ocProductToCategories = $this->ocProductToCategoryRepository->create($input);

        return $this->sendResponse($ocProductToCategories->toArray(), 'Oc Product To Category saved successfully');
    }

    /**
     * Display the specified oc_product_to_category.
     * GET|HEAD /ocProductToCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_to_category $ocProductToCategory */
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            return $this->sendError('Oc Product To Category not found');
        }

        return $this->sendResponse($ocProductToCategory->toArray(), 'Oc Product To Category retrieved successfully');
    }

    /**
     * Update the specified oc_product_to_category in storage.
     * PUT/PATCH /ocProductToCategories/{id}
     *
     * @param  int $id
     * @param Updateoc_product_to_categoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_categoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_to_category $ocProductToCategory */
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            return $this->sendError('Oc Product To Category not found');
        }

        $ocProductToCategory = $this->ocProductToCategoryRepository->update($input, $id);

        return $this->sendResponse($ocProductToCategory->toArray(), 'oc_product_to_category updated successfully');
    }

    /**
     * Remove the specified oc_product_to_category from storage.
     * DELETE /ocProductToCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_to_category $ocProductToCategory */
        $ocProductToCategory = $this->ocProductToCategoryRepository->findWithoutFail($id);

        if (empty($ocProductToCategory)) {
            return $this->sendError('Oc Product To Category not found');
        }

        $ocProductToCategory->delete();

        return $this->sendResponse($id, 'Oc Product To Category deleted successfully');
    }
}
