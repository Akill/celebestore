<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_category_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_category_descriptionAPIRequest;
use App\Models\oc_category_description;
use App\Repositories\oc_category_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_category_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_category_descriptionAPIController extends AppBaseController
{
    /** @var  oc_category_descriptionRepository */
    private $ocCategoryDescriptionRepository;

    public function __construct(oc_category_descriptionRepository $ocCategoryDescriptionRepo)
    {
        $this->ocCategoryDescriptionRepository = $ocCategoryDescriptionRepo;
    }

    /**
     * Display a listing of the oc_category_description.
     * GET|HEAD /ocCategoryDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategoryDescriptions = $this->ocCategoryDescriptionRepository->all();

        return $this->sendResponse($ocCategoryDescriptions->toArray(), 'Oc Category Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_category_description in storage.
     * POST /ocCategoryDescriptions
     *
     * @param Createoc_category_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocCategoryDescriptions = $this->ocCategoryDescriptionRepository->create($input);

        return $this->sendResponse($ocCategoryDescriptions->toArray(), 'Oc Category Description saved successfully');
    }

    /**
     * Display the specified oc_category_description.
     * GET|HEAD /ocCategoryDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category_description $ocCategoryDescription */
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            return $this->sendError('Oc Category Description not found');
        }

        return $this->sendResponse($ocCategoryDescription->toArray(), 'Oc Category Description retrieved successfully');
    }

    /**
     * Update the specified oc_category_description in storage.
     * PUT/PATCH /ocCategoryDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_category_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category_description $ocCategoryDescription */
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            return $this->sendError('Oc Category Description not found');
        }

        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocCategoryDescription->toArray(), 'oc_category_description updated successfully');
    }

    /**
     * Remove the specified oc_category_description from storage.
     * DELETE /ocCategoryDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category_description $ocCategoryDescription */
        $ocCategoryDescription = $this->ocCategoryDescriptionRepository->findWithoutFail($id);

        if (empty($ocCategoryDescription)) {
            return $this->sendError('Oc Category Description not found');
        }

        $ocCategoryDescription->delete();

        return $this->sendResponse($id, 'Oc Category Description deleted successfully');
    }
}
