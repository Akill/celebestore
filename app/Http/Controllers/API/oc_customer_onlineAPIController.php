<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_onlineAPIRequest;
use App\Http\Requests\API\Updateoc_customer_onlineAPIRequest;
use App\Models\oc_customer_online;
use App\Repositories\oc_customer_onlineRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_onlineController
 * @package App\Http\Controllers\API
 */

class oc_customer_onlineAPIController extends AppBaseController
{
    /** @var  oc_customer_onlineRepository */
    private $ocCustomerOnlineRepository;

    public function __construct(oc_customer_onlineRepository $ocCustomerOnlineRepo)
    {
        $this->ocCustomerOnlineRepository = $ocCustomerOnlineRepo;
    }

    /**
     * Display a listing of the oc_customer_online.
     * GET|HEAD /ocCustomerOnlines
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerOnlineRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerOnlineRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerOnlines = $this->ocCustomerOnlineRepository->all();

        return $this->sendResponse($ocCustomerOnlines->toArray(), 'Oc Customer Onlines retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_online in storage.
     * POST /ocCustomerOnlines
     *
     * @param Createoc_customer_onlineAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_onlineAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerOnlines = $this->ocCustomerOnlineRepository->create($input);

        return $this->sendResponse($ocCustomerOnlines->toArray(), 'Oc Customer Online saved successfully');
    }

    /**
     * Display the specified oc_customer_online.
     * GET|HEAD /ocCustomerOnlines/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_online $ocCustomerOnline */
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            return $this->sendError('Oc Customer Online not found');
        }

        return $this->sendResponse($ocCustomerOnline->toArray(), 'Oc Customer Online retrieved successfully');
    }

    /**
     * Update the specified oc_customer_online in storage.
     * PUT/PATCH /ocCustomerOnlines/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_onlineAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_onlineAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_online $ocCustomerOnline */
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            return $this->sendError('Oc Customer Online not found');
        }

        $ocCustomerOnline = $this->ocCustomerOnlineRepository->update($input, $id);

        return $this->sendResponse($ocCustomerOnline->toArray(), 'oc_customer_online updated successfully');
    }

    /**
     * Remove the specified oc_customer_online from storage.
     * DELETE /ocCustomerOnlines/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_online $ocCustomerOnline */
        $ocCustomerOnline = $this->ocCustomerOnlineRepository->findWithoutFail($id);

        if (empty($ocCustomerOnline)) {
            return $this->sendError('Oc Customer Online not found');
        }

        $ocCustomerOnline->delete();

        return $this->sendResponse($id, 'Oc Customer Online deleted successfully');
    }
}
