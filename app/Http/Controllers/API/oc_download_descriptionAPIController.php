<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_download_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_download_descriptionAPIRequest;
use App\Models\oc_download_description;
use App\Repositories\oc_download_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_download_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_download_descriptionAPIController extends AppBaseController
{
    /** @var  oc_download_descriptionRepository */
    private $ocDownloadDescriptionRepository;

    public function __construct(oc_download_descriptionRepository $ocDownloadDescriptionRepo)
    {
        $this->ocDownloadDescriptionRepository = $ocDownloadDescriptionRepo;
    }

    /**
     * Display a listing of the oc_download_description.
     * GET|HEAD /ocDownloadDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocDownloadDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocDownloadDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocDownloadDescriptions = $this->ocDownloadDescriptionRepository->all();

        return $this->sendResponse($ocDownloadDescriptions->toArray(), 'Oc Download Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_download_description in storage.
     * POST /ocDownloadDescriptions
     *
     * @param Createoc_download_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_download_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocDownloadDescriptions = $this->ocDownloadDescriptionRepository->create($input);

        return $this->sendResponse($ocDownloadDescriptions->toArray(), 'Oc Download Description saved successfully');
    }

    /**
     * Display the specified oc_download_description.
     * GET|HEAD /ocDownloadDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_download_description $ocDownloadDescription */
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            return $this->sendError('Oc Download Description not found');
        }

        return $this->sendResponse($ocDownloadDescription->toArray(), 'Oc Download Description retrieved successfully');
    }

    /**
     * Update the specified oc_download_description in storage.
     * PUT/PATCH /ocDownloadDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_download_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_download_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_download_description $ocDownloadDescription */
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            return $this->sendError('Oc Download Description not found');
        }

        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocDownloadDescription->toArray(), 'oc_download_description updated successfully');
    }

    /**
     * Remove the specified oc_download_description from storage.
     * DELETE /ocDownloadDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_download_description $ocDownloadDescription */
        $ocDownloadDescription = $this->ocDownloadDescriptionRepository->findWithoutFail($id);

        if (empty($ocDownloadDescription)) {
            return $this->sendError('Oc Download Description not found');
        }

        $ocDownloadDescription->delete();

        return $this->sendResponse($id, 'Oc Download Description deleted successfully');
    }
}
