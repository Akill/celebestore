<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_categoryAPIRequest;
use App\Http\Requests\API\Updateoc_categoryAPIRequest;
use App\Models\oc_category;
use App\Repositories\oc_categoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_categoryController
 * @package App\Http\Controllers\API
 */

class oc_categoryAPIController extends AppBaseController
{
    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    public function __construct(oc_categoryRepository $ocCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
    }

    /**
     * Display a listing of the oc_category.
     * GET|HEAD /ocCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCategories = $this->ocCategoryRepository->all();

        return $this->sendResponse($ocCategories->toArray(), 'Oc Categories retrieved successfully');
    }

    /**
     * Store a newly created oc_category in storage.
     * POST /ocCategories
     *
     * @param Createoc_categoryAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_categoryAPIRequest $request)
    {
        $input = $request->all();

        $ocCategories = $this->ocCategoryRepository->create($input);

        return $this->sendResponse($ocCategories->toArray(), 'Oc Category saved successfully');
    }

    /**
     * Display the specified oc_category.
     * GET|HEAD /ocCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_category $ocCategory */
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            return $this->sendError('Oc Category not found');
        }

        return $this->sendResponse($ocCategory->toArray(), 'Oc Category retrieved successfully');
    }

    /**
     * Update the specified oc_category in storage.
     * PUT/PATCH /ocCategories/{id}
     *
     * @param  int $id
     * @param Updateoc_categoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_categoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_category $ocCategory */
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            return $this->sendError('Oc Category not found');
        }

        $ocCategory = $this->ocCategoryRepository->update($input, $id);

        return $this->sendResponse($ocCategory->toArray(), 'oc_category updated successfully');
    }

    /**
     * Remove the specified oc_category from storage.
     * DELETE /ocCategories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_category $ocCategory */
        $ocCategory = $this->ocCategoryRepository->findWithoutFail($id);

        if (empty($ocCategory)) {
            return $this->sendError('Oc Category not found');
        }

        $ocCategory->delete();

        return $this->sendResponse($id, 'Oc Category deleted successfully');
    }
}
