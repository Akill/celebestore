<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_specialAPIRequest;
use App\Http\Requests\API\Updateoc_product_specialAPIRequest;
use App\Models\oc_product_special;
use App\Repositories\oc_product_specialRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_specialController
 * @package App\Http\Controllers\API
 */

class oc_product_specialAPIController extends AppBaseController
{
    /** @var  oc_product_specialRepository */
    private $ocProductSpecialRepository;

    public function __construct(oc_product_specialRepository $ocProductSpecialRepo)
    {
        $this->ocProductSpecialRepository = $ocProductSpecialRepo;
    }

    /**
     * Display a listing of the oc_product_special.
     * GET|HEAD /ocProductSpecials
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductSpecialRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductSpecialRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductSpecials = $this->ocProductSpecialRepository->all();

        return $this->sendResponse($ocProductSpecials->toArray(), 'Oc Product Specials retrieved successfully');
    }

    /**
     * Store a newly created oc_product_special in storage.
     * POST /ocProductSpecials
     *
     * @param Createoc_product_specialAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_specialAPIRequest $request)
    {
        $input = $request->all();

        $ocProductSpecials = $this->ocProductSpecialRepository->create($input);

        return $this->sendResponse($ocProductSpecials->toArray(), 'Oc Product Special saved successfully');
    }

    /**
     * Display the specified oc_product_special.
     * GET|HEAD /ocProductSpecials/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_special $ocProductSpecial */
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            return $this->sendError('Oc Product Special not found');
        }

        return $this->sendResponse($ocProductSpecial->toArray(), 'Oc Product Special retrieved successfully');
    }

    /**
     * Update the specified oc_product_special in storage.
     * PUT/PATCH /ocProductSpecials/{id}
     *
     * @param  int $id
     * @param Updateoc_product_specialAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_specialAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_special $ocProductSpecial */
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            return $this->sendError('Oc Product Special not found');
        }

        $ocProductSpecial = $this->ocProductSpecialRepository->update($input, $id);

        return $this->sendResponse($ocProductSpecial->toArray(), 'oc_product_special updated successfully');
    }

    /**
     * Remove the specified oc_product_special from storage.
     * DELETE /ocProductSpecials/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_special $ocProductSpecial */
        $ocProductSpecial = $this->ocProductSpecialRepository->findWithoutFail($id);

        if (empty($ocProductSpecial)) {
            return $this->sendError('Oc Product Special not found');
        }

        $ocProductSpecial->delete();

        return $this->sendResponse($id, 'Oc Product Special deleted successfully');
    }
}
