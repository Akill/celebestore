<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_coupon_historyAPIRequest;
use App\Http\Requests\API\Updateoc_coupon_historyAPIRequest;
use App\Models\oc_coupon_history;
use App\Repositories\oc_coupon_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_coupon_historyController
 * @package App\Http\Controllers\API
 */

class oc_coupon_historyAPIController extends AppBaseController
{
    /** @var  oc_coupon_historyRepository */
    private $ocCouponHistoryRepository;

    public function __construct(oc_coupon_historyRepository $ocCouponHistoryRepo)
    {
        $this->ocCouponHistoryRepository = $ocCouponHistoryRepo;
    }

    /**
     * Display a listing of the oc_coupon_history.
     * GET|HEAD /ocCouponHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponHistoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCouponHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCouponHistories = $this->ocCouponHistoryRepository->all();

        return $this->sendResponse($ocCouponHistories->toArray(), 'Oc Coupon Histories retrieved successfully');
    }

    /**
     * Store a newly created oc_coupon_history in storage.
     * POST /ocCouponHistories
     *
     * @param Createoc_coupon_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_historyAPIRequest $request)
    {
        $input = $request->all();

        $ocCouponHistories = $this->ocCouponHistoryRepository->create($input);

        return $this->sendResponse($ocCouponHistories->toArray(), 'Oc Coupon History saved successfully');
    }

    /**
     * Display the specified oc_coupon_history.
     * GET|HEAD /ocCouponHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_coupon_history $ocCouponHistory */
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            return $this->sendError('Oc Coupon History not found');
        }

        return $this->sendResponse($ocCouponHistory->toArray(), 'Oc Coupon History retrieved successfully');
    }

    /**
     * Update the specified oc_coupon_history in storage.
     * PUT/PATCH /ocCouponHistories/{id}
     *
     * @param  int $id
     * @param Updateoc_coupon_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_coupon_history $ocCouponHistory */
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            return $this->sendError('Oc Coupon History not found');
        }

        $ocCouponHistory = $this->ocCouponHistoryRepository->update($input, $id);

        return $this->sendResponse($ocCouponHistory->toArray(), 'oc_coupon_history updated successfully');
    }

    /**
     * Remove the specified oc_coupon_history from storage.
     * DELETE /ocCouponHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_coupon_history $ocCouponHistory */
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            return $this->sendError('Oc Coupon History not found');
        }

        $ocCouponHistory->delete();

        return $this->sendResponse($id, 'Oc Coupon History deleted successfully');
    }
}
