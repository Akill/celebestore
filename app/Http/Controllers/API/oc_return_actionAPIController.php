<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_return_actionAPIRequest;
use App\Http\Requests\API\Updateoc_return_actionAPIRequest;
use App\Models\oc_return_action;
use App\Repositories\oc_return_actionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_return_actionController
 * @package App\Http\Controllers\API
 */

class oc_return_actionAPIController extends AppBaseController
{
    /** @var  oc_return_actionRepository */
    private $ocReturnActionRepository;

    public function __construct(oc_return_actionRepository $ocReturnActionRepo)
    {
        $this->ocReturnActionRepository = $ocReturnActionRepo;
    }

    /**
     * Display a listing of the oc_return_action.
     * GET|HEAD /ocReturnActions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnActionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReturnActionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReturnActions = $this->ocReturnActionRepository->all();

        return $this->sendResponse($ocReturnActions->toArray(), 'Oc Return Actions retrieved successfully');
    }

    /**
     * Store a newly created oc_return_action in storage.
     * POST /ocReturnActions
     *
     * @param Createoc_return_actionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_return_actionAPIRequest $request)
    {
        $input = $request->all();

        $ocReturnActions = $this->ocReturnActionRepository->create($input);

        return $this->sendResponse($ocReturnActions->toArray(), 'Oc Return Action saved successfully');
    }

    /**
     * Display the specified oc_return_action.
     * GET|HEAD /ocReturnActions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_return_action $ocReturnAction */
        $ocReturnAction = $this->ocReturnActionRepository->findWithoutFail($id);

        if (empty($ocReturnAction)) {
            return $this->sendError('Oc Return Action not found');
        }

        return $this->sendResponse($ocReturnAction->toArray(), 'Oc Return Action retrieved successfully');
    }

    /**
     * Update the specified oc_return_action in storage.
     * PUT/PATCH /ocReturnActions/{id}
     *
     * @param  int $id
     * @param Updateoc_return_actionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_return_actionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_return_action $ocReturnAction */
        $ocReturnAction = $this->ocReturnActionRepository->findWithoutFail($id);

        if (empty($ocReturnAction)) {
            return $this->sendError('Oc Return Action not found');
        }

        $ocReturnAction = $this->ocReturnActionRepository->update($input, $id);

        return $this->sendResponse($ocReturnAction->toArray(), 'oc_return_action updated successfully');
    }

    /**
     * Remove the specified oc_return_action from storage.
     * DELETE /ocReturnActions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_return_action $ocReturnAction */
        $ocReturnAction = $this->ocReturnActionRepository->findWithoutFail($id);

        if (empty($ocReturnAction)) {
            return $this->sendError('Oc Return Action not found');
        }

        $ocReturnAction->delete();

        return $this->sendResponse($id, 'Oc Return Action deleted successfully');
    }
}
