<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_attribute_groupAPIRequest;
use App\Http\Requests\API\Updateoc_attribute_groupAPIRequest;
use App\Models\oc_attribute_group;
use App\Repositories\oc_attribute_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_attribute_groupController
 * @package App\Http\Controllers\API
 */

class oc_attribute_groupAPIController extends AppBaseController
{
    /** @var  oc_attribute_groupRepository */
    private $ocAttributeGroupRepository;

    public function __construct(oc_attribute_groupRepository $ocAttributeGroupRepo)
    {
        $this->ocAttributeGroupRepository = $ocAttributeGroupRepo;
    }

    /**
     * Display a listing of the oc_attribute_group.
     * GET|HEAD /ocAttributeGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAttributeGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAttributeGroups = $this->ocAttributeGroupRepository->all();

        return $this->sendResponse($ocAttributeGroups->toArray(), 'Oc Attribute Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_attribute_group in storage.
     * POST /ocAttributeGroups
     *
     * @param Createoc_attribute_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocAttributeGroups = $this->ocAttributeGroupRepository->create($input);

        return $this->sendResponse($ocAttributeGroups->toArray(), 'Oc Attribute Group saved successfully');
    }

    /**
     * Display the specified oc_attribute_group.
     * GET|HEAD /ocAttributeGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_attribute_group $ocAttributeGroup */
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            return $this->sendError('Oc Attribute Group not found');
        }

        return $this->sendResponse($ocAttributeGroup->toArray(), 'Oc Attribute Group retrieved successfully');
    }

    /**
     * Update the specified oc_attribute_group in storage.
     * PUT/PATCH /ocAttributeGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_attribute_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_attribute_group $ocAttributeGroup */
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            return $this->sendError('Oc Attribute Group not found');
        }

        $ocAttributeGroup = $this->ocAttributeGroupRepository->update($input, $id);

        return $this->sendResponse($ocAttributeGroup->toArray(), 'oc_attribute_group updated successfully');
    }

    /**
     * Remove the specified oc_attribute_group from storage.
     * DELETE /ocAttributeGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_attribute_group $ocAttributeGroup */
        $ocAttributeGroup = $this->ocAttributeGroupRepository->findWithoutFail($id);

        if (empty($ocAttributeGroup)) {
            return $this->sendError('Oc Attribute Group not found');
        }

        $ocAttributeGroup->delete();

        return $this->sendResponse($id, 'Oc Attribute Group deleted successfully');
    }
}
