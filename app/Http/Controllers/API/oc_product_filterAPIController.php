<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_filterAPIRequest;
use App\Http\Requests\API\Updateoc_product_filterAPIRequest;
use App\Models\oc_product_filter;
use App\Repositories\oc_product_filterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_filterController
 * @package App\Http\Controllers\API
 */

class oc_product_filterAPIController extends AppBaseController
{
    /** @var  oc_product_filterRepository */
    private $ocProductFilterRepository;

    public function __construct(oc_product_filterRepository $ocProductFilterRepo)
    {
        $this->ocProductFilterRepository = $ocProductFilterRepo;
    }

    /**
     * Display a listing of the oc_product_filter.
     * GET|HEAD /ocProductFilters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductFilterRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductFilterRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductFilters = $this->ocProductFilterRepository->all();

        return $this->sendResponse($ocProductFilters->toArray(), 'Oc Product Filters retrieved successfully');
    }

    /**
     * Store a newly created oc_product_filter in storage.
     * POST /ocProductFilters
     *
     * @param Createoc_product_filterAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_filterAPIRequest $request)
    {
        $input = $request->all();

        $ocProductFilters = $this->ocProductFilterRepository->create($input);

        return $this->sendResponse($ocProductFilters->toArray(), 'Oc Product Filter saved successfully');
    }

    /**
     * Display the specified oc_product_filter.
     * GET|HEAD /ocProductFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_filter $ocProductFilter */
        $ocProductFilter = $this->ocProductFilterRepository->findWithoutFail($id);

        if (empty($ocProductFilter)) {
            return $this->sendError('Oc Product Filter not found');
        }

        return $this->sendResponse($ocProductFilter->toArray(), 'Oc Product Filter retrieved successfully');
    }

    /**
     * Update the specified oc_product_filter in storage.
     * PUT/PATCH /ocProductFilters/{id}
     *
     * @param  int $id
     * @param Updateoc_product_filterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_filterAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_filter $ocProductFilter */
        $ocProductFilter = $this->ocProductFilterRepository->findWithoutFail($id);

        if (empty($ocProductFilter)) {
            return $this->sendError('Oc Product Filter not found');
        }

        $ocProductFilter = $this->ocProductFilterRepository->update($input, $id);

        return $this->sendResponse($ocProductFilter->toArray(), 'oc_product_filter updated successfully');
    }

    /**
     * Remove the specified oc_product_filter from storage.
     * DELETE /ocProductFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_filter $ocProductFilter */
        $ocProductFilter = $this->ocProductFilterRepository->findWithoutFail($id);

        if (empty($ocProductFilter)) {
            return $this->sendError('Oc Product Filter not found');
        }

        $ocProductFilter->delete();

        return $this->sendResponse($id, 'Oc Product Filter deleted successfully');
    }
}
