<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_marketingAPIRequest;
use App\Http\Requests\API\Updateoc_marketingAPIRequest;
use App\Models\oc_marketing;
use App\Repositories\oc_marketingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_marketingController
 * @package App\Http\Controllers\API
 */

class oc_marketingAPIController extends AppBaseController
{
    /** @var  oc_marketingRepository */
    private $ocMarketingRepository;

    public function __construct(oc_marketingRepository $ocMarketingRepo)
    {
        $this->ocMarketingRepository = $ocMarketingRepo;
    }

    /**
     * Display a listing of the oc_marketing.
     * GET|HEAD /ocMarketings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocMarketingRepository->pushCriteria(new RequestCriteria($request));
        $this->ocMarketingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocMarketings = $this->ocMarketingRepository->all();

        return $this->sendResponse($ocMarketings->toArray(), 'Oc Marketings retrieved successfully');
    }

    /**
     * Store a newly created oc_marketing in storage.
     * POST /ocMarketings
     *
     * @param Createoc_marketingAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_marketingAPIRequest $request)
    {
        $input = $request->all();

        $ocMarketings = $this->ocMarketingRepository->create($input);

        return $this->sendResponse($ocMarketings->toArray(), 'Oc Marketing saved successfully');
    }

    /**
     * Display the specified oc_marketing.
     * GET|HEAD /ocMarketings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_marketing $ocMarketing */
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            return $this->sendError('Oc Marketing not found');
        }

        return $this->sendResponse($ocMarketing->toArray(), 'Oc Marketing retrieved successfully');
    }

    /**
     * Update the specified oc_marketing in storage.
     * PUT/PATCH /ocMarketings/{id}
     *
     * @param  int $id
     * @param Updateoc_marketingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_marketingAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_marketing $ocMarketing */
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            return $this->sendError('Oc Marketing not found');
        }

        $ocMarketing = $this->ocMarketingRepository->update($input, $id);

        return $this->sendResponse($ocMarketing->toArray(), 'oc_marketing updated successfully');
    }

    /**
     * Remove the specified oc_marketing from storage.
     * DELETE /ocMarketings/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_marketing $ocMarketing */
        $ocMarketing = $this->ocMarketingRepository->findWithoutFail($id);

        if (empty($ocMarketing)) {
            return $this->sendError('Oc Marketing not found');
        }

        $ocMarketing->delete();

        return $this->sendResponse($id, 'Oc Marketing deleted successfully');
    }
}
