<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_filterAPIRequest;
use App\Http\Requests\API\Updateoc_filterAPIRequest;
use App\Models\oc_filter;
use App\Repositories\oc_filterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_filterController
 * @package App\Http\Controllers\API
 */

class oc_filterAPIController extends AppBaseController
{
    /** @var  oc_filterRepository */
    private $ocFilterRepository;

    public function __construct(oc_filterRepository $ocFilterRepo)
    {
        $this->ocFilterRepository = $ocFilterRepo;
    }

    /**
     * Display a listing of the oc_filter.
     * GET|HEAD /ocFilters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterRepository->pushCriteria(new RequestCriteria($request));
        $this->ocFilterRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocFilters = $this->ocFilterRepository->all();

        return $this->sendResponse($ocFilters->toArray(), 'Oc Filters retrieved successfully');
    }

    /**
     * Store a newly created oc_filter in storage.
     * POST /ocFilters
     *
     * @param Createoc_filterAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filterAPIRequest $request)
    {
        $input = $request->all();

        $ocFilters = $this->ocFilterRepository->create($input);

        return $this->sendResponse($ocFilters->toArray(), 'Oc Filter saved successfully');
    }

    /**
     * Display the specified oc_filter.
     * GET|HEAD /ocFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_filter $ocFilter */
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            return $this->sendError('Oc Filter not found');
        }

        return $this->sendResponse($ocFilter->toArray(), 'Oc Filter retrieved successfully');
    }

    /**
     * Update the specified oc_filter in storage.
     * PUT/PATCH /ocFilters/{id}
     *
     * @param  int $id
     * @param Updateoc_filterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filterAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_filter $ocFilter */
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            return $this->sendError('Oc Filter not found');
        }

        $ocFilter = $this->ocFilterRepository->update($input, $id);

        return $this->sendResponse($ocFilter->toArray(), 'oc_filter updated successfully');
    }

    /**
     * Remove the specified oc_filter from storage.
     * DELETE /ocFilters/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_filter $ocFilter */
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            return $this->sendError('Oc Filter not found');
        }

        $ocFilter->delete();

        return $this->sendResponse($id, 'Oc Filter deleted successfully');
    }
}
