<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_tax_rateAPIRequest;
use App\Http\Requests\API\Updateoc_tax_rateAPIRequest;
use App\Models\oc_tax_rate;
use App\Repositories\oc_tax_rateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_tax_rateController
 * @package App\Http\Controllers\API
 */

class oc_tax_rateAPIController extends AppBaseController
{
    /** @var  oc_tax_rateRepository */
    private $ocTaxRateRepository;

    public function __construct(oc_tax_rateRepository $ocTaxRateRepo)
    {
        $this->ocTaxRateRepository = $ocTaxRateRepo;
    }

    /**
     * Display a listing of the oc_tax_rate.
     * GET|HEAD /ocTaxRates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocTaxRateRepository->pushCriteria(new RequestCriteria($request));
        $this->ocTaxRateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocTaxRates = $this->ocTaxRateRepository->all();

        return $this->sendResponse($ocTaxRates->toArray(), 'Oc Tax Rates retrieved successfully');
    }

    /**
     * Store a newly created oc_tax_rate in storage.
     * POST /ocTaxRates
     *
     * @param Createoc_tax_rateAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_tax_rateAPIRequest $request)
    {
        $input = $request->all();

        $ocTaxRates = $this->ocTaxRateRepository->create($input);

        return $this->sendResponse($ocTaxRates->toArray(), 'Oc Tax Rate saved successfully');
    }

    /**
     * Display the specified oc_tax_rate.
     * GET|HEAD /ocTaxRates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_tax_rate $ocTaxRate */
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            return $this->sendError('Oc Tax Rate not found');
        }

        return $this->sendResponse($ocTaxRate->toArray(), 'Oc Tax Rate retrieved successfully');
    }

    /**
     * Update the specified oc_tax_rate in storage.
     * PUT/PATCH /ocTaxRates/{id}
     *
     * @param  int $id
     * @param Updateoc_tax_rateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_tax_rateAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_tax_rate $ocTaxRate */
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            return $this->sendError('Oc Tax Rate not found');
        }

        $ocTaxRate = $this->ocTaxRateRepository->update($input, $id);

        return $this->sendResponse($ocTaxRate->toArray(), 'oc_tax_rate updated successfully');
    }

    /**
     * Remove the specified oc_tax_rate from storage.
     * DELETE /ocTaxRates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_tax_rate $ocTaxRate */
        $ocTaxRate = $this->ocTaxRateRepository->findWithoutFail($id);

        if (empty($ocTaxRate)) {
            return $this->sendError('Oc Tax Rate not found');
        }

        $ocTaxRate->delete();

        return $this->sendResponse($id, 'Oc Tax Rate deleted successfully');
    }
}
