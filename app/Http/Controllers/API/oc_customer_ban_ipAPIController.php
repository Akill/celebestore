<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_ban_ipAPIRequest;
use App\Http\Requests\API\Updateoc_customer_ban_ipAPIRequest;
use App\Models\oc_customer_ban_ip;
use App\Repositories\oc_customer_ban_ipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_ban_ipController
 * @package App\Http\Controllers\API
 */

class oc_customer_ban_ipAPIController extends AppBaseController
{
    /** @var  oc_customer_ban_ipRepository */
    private $ocCustomerBanIpRepository;

    public function __construct(oc_customer_ban_ipRepository $ocCustomerBanIpRepo)
    {
        $this->ocCustomerBanIpRepository = $ocCustomerBanIpRepo;
    }

    /**
     * Display a listing of the oc_customer_ban_ip.
     * GET|HEAD /ocCustomerBanIps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerBanIpRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerBanIpRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerBanIps = $this->ocCustomerBanIpRepository->all();

        return $this->sendResponse($ocCustomerBanIps->toArray(), 'Oc Customer Ban Ips retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_ban_ip in storage.
     * POST /ocCustomerBanIps
     *
     * @param Createoc_customer_ban_ipAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_ban_ipAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerBanIps = $this->ocCustomerBanIpRepository->create($input);

        return $this->sendResponse($ocCustomerBanIps->toArray(), 'Oc Customer Ban Ip saved successfully');
    }

    /**
     * Display the specified oc_customer_ban_ip.
     * GET|HEAD /ocCustomerBanIps/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_ban_ip $ocCustomerBanIp */
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            return $this->sendError('Oc Customer Ban Ip not found');
        }

        return $this->sendResponse($ocCustomerBanIp->toArray(), 'Oc Customer Ban Ip retrieved successfully');
    }

    /**
     * Update the specified oc_customer_ban_ip in storage.
     * PUT/PATCH /ocCustomerBanIps/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_ban_ipAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_ban_ipAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_ban_ip $ocCustomerBanIp */
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            return $this->sendError('Oc Customer Ban Ip not found');
        }

        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->update($input, $id);

        return $this->sendResponse($ocCustomerBanIp->toArray(), 'oc_customer_ban_ip updated successfully');
    }

    /**
     * Remove the specified oc_customer_ban_ip from storage.
     * DELETE /ocCustomerBanIps/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_ban_ip $ocCustomerBanIp */
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            return $this->sendError('Oc Customer Ban Ip not found');
        }

        $ocCustomerBanIp->delete();

        return $this->sendResponse($id, 'Oc Customer Ban Ip deleted successfully');
    }
}
