<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_optionAPIRequest;
use App\Http\Requests\API\Updateoc_optionAPIRequest;
use App\Models\oc_option;
use App\Repositories\oc_optionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_optionController
 * @package App\Http\Controllers\API
 */

class oc_optionAPIController extends AppBaseController
{
    /** @var  oc_optionRepository */
    private $ocOptionRepository;

    public function __construct(oc_optionRepository $ocOptionRepo)
    {
        $this->ocOptionRepository = $ocOptionRepo;
    }

    /**
     * Display a listing of the oc_option.
     * GET|HEAD /ocOptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOptions = $this->ocOptionRepository->all();

        return $this->sendResponse($ocOptions->toArray(), 'Oc Options retrieved successfully');
    }

    /**
     * Store a newly created oc_option in storage.
     * POST /ocOptions
     *
     * @param Createoc_optionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_optionAPIRequest $request)
    {
        $input = $request->all();

        $ocOptions = $this->ocOptionRepository->create($input);

        return $this->sendResponse($ocOptions->toArray(), 'Oc Option saved successfully');
    }

    /**
     * Display the specified oc_option.
     * GET|HEAD /ocOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_option $ocOption */
        $ocOption = $this->ocOptionRepository->findWithoutFail($id);

        if (empty($ocOption)) {
            return $this->sendError('Oc Option not found');
        }

        return $this->sendResponse($ocOption->toArray(), 'Oc Option retrieved successfully');
    }

    /**
     * Update the specified oc_option in storage.
     * PUT/PATCH /ocOptions/{id}
     *
     * @param  int $id
     * @param Updateoc_optionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_optionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_option $ocOption */
        $ocOption = $this->ocOptionRepository->findWithoutFail($id);

        if (empty($ocOption)) {
            return $this->sendError('Oc Option not found');
        }

        $ocOption = $this->ocOptionRepository->update($input, $id);

        return $this->sendResponse($ocOption->toArray(), 'oc_option updated successfully');
    }

    /**
     * Remove the specified oc_option from storage.
     * DELETE /ocOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_option $ocOption */
        $ocOption = $this->ocOptionRepository->findWithoutFail($id);

        if (empty($ocOption)) {
            return $this->sendError('Oc Option not found');
        }

        $ocOption->delete();

        return $this->sendResponse($id, 'Oc Option deleted successfully');
    }
}
