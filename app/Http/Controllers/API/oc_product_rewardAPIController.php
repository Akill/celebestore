<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_rewardAPIRequest;
use App\Http\Requests\API\Updateoc_product_rewardAPIRequest;
use App\Models\oc_product_reward;
use App\Repositories\oc_product_rewardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_rewardController
 * @package App\Http\Controllers\API
 */

class oc_product_rewardAPIController extends AppBaseController
{
    /** @var  oc_product_rewardRepository */
    private $ocProductRewardRepository;

    public function __construct(oc_product_rewardRepository $ocProductRewardRepo)
    {
        $this->ocProductRewardRepository = $ocProductRewardRepo;
    }

    /**
     * Display a listing of the oc_product_reward.
     * GET|HEAD /ocProductRewards
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRewardRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductRewardRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductRewards = $this->ocProductRewardRepository->all();

        return $this->sendResponse($ocProductRewards->toArray(), 'Oc Product Rewards retrieved successfully');
    }

    /**
     * Store a newly created oc_product_reward in storage.
     * POST /ocProductRewards
     *
     * @param Createoc_product_rewardAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_rewardAPIRequest $request)
    {
        $input = $request->all();

        $ocProductRewards = $this->ocProductRewardRepository->create($input);

        return $this->sendResponse($ocProductRewards->toArray(), 'Oc Product Reward saved successfully');
    }

    /**
     * Display the specified oc_product_reward.
     * GET|HEAD /ocProductRewards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_reward $ocProductReward */
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            return $this->sendError('Oc Product Reward not found');
        }

        return $this->sendResponse($ocProductReward->toArray(), 'Oc Product Reward retrieved successfully');
    }

    /**
     * Update the specified oc_product_reward in storage.
     * PUT/PATCH /ocProductRewards/{id}
     *
     * @param  int $id
     * @param Updateoc_product_rewardAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_rewardAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_reward $ocProductReward */
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            return $this->sendError('Oc Product Reward not found');
        }

        $ocProductReward = $this->ocProductRewardRepository->update($input, $id);

        return $this->sendResponse($ocProductReward->toArray(), 'oc_product_reward updated successfully');
    }

    /**
     * Remove the specified oc_product_reward from storage.
     * DELETE /ocProductRewards/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_reward $ocProductReward */
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            return $this->sendError('Oc Product Reward not found');
        }

        $ocProductReward->delete();

        return $this->sendResponse($id, 'Oc Product Reward deleted successfully');
    }
}
