<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_voucher_historyAPIRequest;
use App\Http\Requests\API\Updateoc_voucher_historyAPIRequest;
use App\Models\oc_voucher_history;
use App\Repositories\oc_voucher_historyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_voucher_historyController
 * @package App\Http\Controllers\API
 */

class oc_voucher_historyAPIController extends AppBaseController
{
    /** @var  oc_voucher_historyRepository */
    private $ocVoucherHistoryRepository;

    public function __construct(oc_voucher_historyRepository $ocVoucherHistoryRepo)
    {
        $this->ocVoucherHistoryRepository = $ocVoucherHistoryRepo;
    }

    /**
     * Display a listing of the oc_voucher_history.
     * GET|HEAD /ocVoucherHistories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherHistoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ocVoucherHistoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocVoucherHistories = $this->ocVoucherHistoryRepository->all();

        return $this->sendResponse($ocVoucherHistories->toArray(), 'Oc Voucher Histories retrieved successfully');
    }

    /**
     * Store a newly created oc_voucher_history in storage.
     * POST /ocVoucherHistories
     *
     * @param Createoc_voucher_historyAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucher_historyAPIRequest $request)
    {
        $input = $request->all();

        $ocVoucherHistories = $this->ocVoucherHistoryRepository->create($input);

        return $this->sendResponse($ocVoucherHistories->toArray(), 'Oc Voucher History saved successfully');
    }

    /**
     * Display the specified oc_voucher_history.
     * GET|HEAD /ocVoucherHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_voucher_history $ocVoucherHistory */
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            return $this->sendError('Oc Voucher History not found');
        }

        return $this->sendResponse($ocVoucherHistory->toArray(), 'Oc Voucher History retrieved successfully');
    }

    /**
     * Update the specified oc_voucher_history in storage.
     * PUT/PATCH /ocVoucherHistories/{id}
     *
     * @param  int $id
     * @param Updateoc_voucher_historyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucher_historyAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_voucher_history $ocVoucherHistory */
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            return $this->sendError('Oc Voucher History not found');
        }

        $ocVoucherHistory = $this->ocVoucherHistoryRepository->update($input, $id);

        return $this->sendResponse($ocVoucherHistory->toArray(), 'oc_voucher_history updated successfully');
    }

    /**
     * Remove the specified oc_voucher_history from storage.
     * DELETE /ocVoucherHistories/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_voucher_history $ocVoucherHistory */
        $ocVoucherHistory = $this->ocVoucherHistoryRepository->findWithoutFail($id);

        if (empty($ocVoucherHistory)) {
            return $this->sendError('Oc Voucher History not found');
        }

        $ocVoucherHistory->delete();

        return $this->sendResponse($id, 'Oc Voucher History deleted successfully');
    }
}
