<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_ipAPIRequest;
use App\Http\Requests\API\Updateoc_customer_ipAPIRequest;
use App\Models\oc_customer_ip;
use App\Repositories\oc_customer_ipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_ipController
 * @package App\Http\Controllers\API
 */

class oc_customer_ipAPIController extends AppBaseController
{
    /** @var  oc_customer_ipRepository */
    private $ocCustomerIpRepository;

    public function __construct(oc_customer_ipRepository $ocCustomerIpRepo)
    {
        $this->ocCustomerIpRepository = $ocCustomerIpRepo;
    }

    /**
     * Display a listing of the oc_customer_ip.
     * GET|HEAD /ocCustomerIps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerIpRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerIpRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerIps = $this->ocCustomerIpRepository->all();

        return $this->sendResponse($ocCustomerIps->toArray(), 'Oc Customer Ips retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_ip in storage.
     * POST /ocCustomerIps
     *
     * @param Createoc_customer_ipAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_ipAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerIps = $this->ocCustomerIpRepository->create($input);

        return $this->sendResponse($ocCustomerIps->toArray(), 'Oc Customer Ip saved successfully');
    }

    /**
     * Display the specified oc_customer_ip.
     * GET|HEAD /ocCustomerIps/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_ip $ocCustomerIp */
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            return $this->sendError('Oc Customer Ip not found');
        }

        return $this->sendResponse($ocCustomerIp->toArray(), 'Oc Customer Ip retrieved successfully');
    }

    /**
     * Update the specified oc_customer_ip in storage.
     * PUT/PATCH /ocCustomerIps/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_ipAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_ipAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_ip $ocCustomerIp */
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            return $this->sendError('Oc Customer Ip not found');
        }

        $ocCustomerIp = $this->ocCustomerIpRepository->update($input, $id);

        return $this->sendResponse($ocCustomerIp->toArray(), 'oc_customer_ip updated successfully');
    }

    /**
     * Remove the specified oc_customer_ip from storage.
     * DELETE /ocCustomerIps/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_ip $ocCustomerIp */
        $ocCustomerIp = $this->ocCustomerIpRepository->findWithoutFail($id);

        if (empty($ocCustomerIp)) {
            return $this->sendError('Oc Customer Ip not found');
        }

        $ocCustomerIp->delete();

        return $this->sendResponse($id, 'Oc Customer Ip deleted successfully');
    }
}
