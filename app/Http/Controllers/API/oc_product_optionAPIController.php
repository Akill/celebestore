<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_optionAPIRequest;
use App\Http\Requests\API\Updateoc_product_optionAPIRequest;
use App\Models\oc_product_option;
use App\Repositories\oc_product_optionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_optionController
 * @package App\Http\Controllers\API
 */

class oc_product_optionAPIController extends AppBaseController
{
    /** @var  oc_product_optionRepository */
    private $ocProductOptionRepository;

    public function __construct(oc_product_optionRepository $ocProductOptionRepo)
    {
        $this->ocProductOptionRepository = $ocProductOptionRepo;
    }

    /**
     * Display a listing of the oc_product_option.
     * GET|HEAD /ocProductOptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductOptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductOptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductOptions = $this->ocProductOptionRepository->all();

        return $this->sendResponse($ocProductOptions->toArray(), 'Oc Product Options retrieved successfully');
    }

    /**
     * Store a newly created oc_product_option in storage.
     * POST /ocProductOptions
     *
     * @param Createoc_product_optionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_optionAPIRequest $request)
    {
        $input = $request->all();

        $ocProductOptions = $this->ocProductOptionRepository->create($input);

        return $this->sendResponse($ocProductOptions->toArray(), 'Oc Product Option saved successfully');
    }

    /**
     * Display the specified oc_product_option.
     * GET|HEAD /ocProductOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_option $ocProductOption */
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            return $this->sendError('Oc Product Option not found');
        }

        return $this->sendResponse($ocProductOption->toArray(), 'Oc Product Option retrieved successfully');
    }

    /**
     * Update the specified oc_product_option in storage.
     * PUT/PATCH /ocProductOptions/{id}
     *
     * @param  int $id
     * @param Updateoc_product_optionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_optionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_option $ocProductOption */
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            return $this->sendError('Oc Product Option not found');
        }

        $ocProductOption = $this->ocProductOptionRepository->update($input, $id);

        return $this->sendResponse($ocProductOption->toArray(), 'oc_product_option updated successfully');
    }

    /**
     * Remove the specified oc_product_option from storage.
     * DELETE /ocProductOptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_option $ocProductOption */
        $ocProductOption = $this->ocProductOptionRepository->findWithoutFail($id);

        if (empty($ocProductOption)) {
            return $this->sendError('Oc Product Option not found');
        }

        $ocProductOption->delete();

        return $this->sendResponse($id, 'Oc Product Option deleted successfully');
    }
}
