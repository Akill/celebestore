<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_userAPIRequest;
use App\Http\Requests\API\Updateoc_userAPIRequest;
use App\Models\oc_user;
use App\Repositories\oc_userRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_userController
 * @package App\Http\Controllers\API
 */

class oc_userAPIController extends AppBaseController
{
    /** @var  oc_userRepository */
    private $ocUserRepository;

    public function __construct(oc_userRepository $ocUserRepo)
    {
        $this->ocUserRepository = $ocUserRepo;
    }

    /**
     * Display a listing of the oc_user.
     * GET|HEAD /ocUsers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUserRepository->pushCriteria(new RequestCriteria($request));
        $this->ocUserRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocUsers = $this->ocUserRepository->all();

        return $this->sendResponse($ocUsers->toArray(), 'Oc Users retrieved successfully');
    }

    /**
     * Store a newly created oc_user in storage.
     * POST /ocUsers
     *
     * @param Createoc_userAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_userAPIRequest $request)
    {
        $input = $request->all();

        $ocUsers = $this->ocUserRepository->create($input);

        return $this->sendResponse($ocUsers->toArray(), 'Oc User saved successfully');
    }

    /**
     * Display the specified oc_user.
     * GET|HEAD /ocUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_user $ocUser */
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            return $this->sendError('Oc User not found');
        }

        return $this->sendResponse($ocUser->toArray(), 'Oc User retrieved successfully');
    }

    /**
     * Update the specified oc_user in storage.
     * PUT/PATCH /ocUsers/{id}
     *
     * @param  int $id
     * @param Updateoc_userAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_userAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_user $ocUser */
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            return $this->sendError('Oc User not found');
        }

        $ocUser = $this->ocUserRepository->update($input, $id);

        return $this->sendResponse($ocUser->toArray(), 'oc_user updated successfully');
    }

    /**
     * Remove the specified oc_user from storage.
     * DELETE /ocUsers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_user $ocUser */
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            return $this->sendError('Oc User not found');
        }

        $ocUser->delete();

        return $this->sendResponse($id, 'Oc User deleted successfully');
    }
}
