<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_loginAPIRequest;
use App\Http\Requests\API\Updateoc_customer_loginAPIRequest;
use App\Models\oc_customer_login;
use App\Repositories\oc_customer_loginRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_loginController
 * @package App\Http\Controllers\API
 */

class oc_customer_loginAPIController extends AppBaseController
{
    /** @var  oc_customer_loginRepository */
    private $ocCustomerLoginRepository;

    public function __construct(oc_customer_loginRepository $ocCustomerLoginRepo)
    {
        $this->ocCustomerLoginRepository = $ocCustomerLoginRepo;
    }

    /**
     * Display a listing of the oc_customer_login.
     * GET|HEAD /ocCustomerLogins
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerLoginRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerLoginRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerLogins = $this->ocCustomerLoginRepository->all();

        return $this->sendResponse($ocCustomerLogins->toArray(), 'Oc Customer Logins retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_login in storage.
     * POST /ocCustomerLogins
     *
     * @param Createoc_customer_loginAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_loginAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerLogins = $this->ocCustomerLoginRepository->create($input);

        return $this->sendResponse($ocCustomerLogins->toArray(), 'Oc Customer Login saved successfully');
    }

    /**
     * Display the specified oc_customer_login.
     * GET|HEAD /ocCustomerLogins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_login $ocCustomerLogin */
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            return $this->sendError('Oc Customer Login not found');
        }

        return $this->sendResponse($ocCustomerLogin->toArray(), 'Oc Customer Login retrieved successfully');
    }

    /**
     * Update the specified oc_customer_login in storage.
     * PUT/PATCH /ocCustomerLogins/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_loginAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_loginAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_login $ocCustomerLogin */
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            return $this->sendError('Oc Customer Login not found');
        }

        $ocCustomerLogin = $this->ocCustomerLoginRepository->update($input, $id);

        return $this->sendResponse($ocCustomerLogin->toArray(), 'oc_customer_login updated successfully');
    }

    /**
     * Remove the specified oc_customer_login from storage.
     * DELETE /ocCustomerLogins/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_login $ocCustomerLogin */
        $ocCustomerLogin = $this->ocCustomerLoginRepository->findWithoutFail($id);

        if (empty($ocCustomerLogin)) {
            return $this->sendError('Oc Customer Login not found');
        }

        $ocCustomerLogin->delete();

        return $this->sendResponse($id, 'Oc Customer Login deleted successfully');
    }
}
