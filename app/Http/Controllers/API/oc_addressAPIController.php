<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_addressAPIRequest;
use App\Http\Requests\API\Updateoc_addressAPIRequest;
use App\Models\oc_address;
use App\Repositories\oc_addressRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_addressController
 * @package App\Http\Controllers\API
 */

class oc_addressAPIController extends AppBaseController
{
    /** @var  oc_addressRepository */
    private $ocAddressRepository;

    public function __construct(oc_addressRepository $ocAddressRepo)
    {
        $this->ocAddressRepository = $ocAddressRepo;
    }

    /**
     * Display a listing of the oc_address.
     * GET|HEAD /ocAddresses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAddressRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAddressRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAddresses = $this->ocAddressRepository->all();

        return $this->sendResponse($ocAddresses->toArray(), 'Oc Addresses retrieved successfully');
    }

    /**
     * Store a newly created oc_address in storage.
     * POST /ocAddresses
     *
     * @param Createoc_addressAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_addressAPIRequest $request)
    {
        $input = $request->all();

        $ocAddresses = $this->ocAddressRepository->create($input);

        return $this->sendResponse($ocAddresses->toArray(), 'Oc Address saved successfully');
    }

    /**
     * Display the specified oc_address.
     * GET|HEAD /ocAddresses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_address $ocAddress */
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            return $this->sendError('Oc Address not found');
        }

        return $this->sendResponse($ocAddress->toArray(), 'Oc Address retrieved successfully');
    }

    /**
     * Update the specified oc_address in storage.
     * PUT/PATCH /ocAddresses/{id}
     *
     * @param  int $id
     * @param Updateoc_addressAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_addressAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_address $ocAddress */
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            return $this->sendError('Oc Address not found');
        }

        $ocAddress = $this->ocAddressRepository->update($input, $id);

        return $this->sendResponse($ocAddress->toArray(), 'oc_address updated successfully');
    }

    /**
     * Remove the specified oc_address from storage.
     * DELETE /ocAddresses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_address $ocAddress */
        $ocAddress = $this->ocAddressRepository->findWithoutFail($id);

        if (empty($ocAddress)) {
            return $this->sendError('Oc Address not found');
        }

        $ocAddress->delete();

        return $this->sendResponse($id, 'Oc Address deleted successfully');
    }
}
