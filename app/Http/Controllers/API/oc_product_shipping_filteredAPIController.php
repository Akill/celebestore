<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_shipping_filteredAPIRequest;
use App\Http\Requests\API\Updateoc_product_shipping_filteredAPIRequest;
use App\Models\oc_product_shipping_filtered;
use App\Repositories\oc_product_shipping_filteredRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_shipping_filteredController
 * @package App\Http\Controllers\API
 */

class oc_product_shipping_filteredAPIController extends AppBaseController
{
    /** @var  oc_product_shipping_filteredRepository */
    private $ocProductShippingFilteredRepository;

    public function __construct(oc_product_shipping_filteredRepository $ocProductShippingFilteredRepo)
    {
        $this->ocProductShippingFilteredRepository = $ocProductShippingFilteredRepo;
    }

    /**
     * Display a listing of the oc_product_shipping_filtered.
     * GET|HEAD /ocProductShippingFiltereds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductShippingFilteredRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductShippingFilteredRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductShippingFiltereds = $this->ocProductShippingFilteredRepository->all();

        return $this->sendResponse($ocProductShippingFiltereds->toArray(), 'Oc Product Shipping Filtereds retrieved successfully');
    }

    /**
     * Store a newly created oc_product_shipping_filtered in storage.
     * POST /ocProductShippingFiltereds
     *
     * @param Createoc_product_shipping_filteredAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_shipping_filteredAPIRequest $request)
    {
        $input = $request->all();

        $ocProductShippingFiltereds = $this->ocProductShippingFilteredRepository->create($input);

        return $this->sendResponse($ocProductShippingFiltereds->toArray(), 'Oc Product Shipping Filtered saved successfully');
    }

    /**
     * Display the specified oc_product_shipping_filtered.
     * GET|HEAD /ocProductShippingFiltereds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_shipping_filtered $ocProductShippingFiltered */
        $ocProductShippingFiltered = $this->ocProductShippingFilteredRepository->findWithoutFail($id);

        if (empty($ocProductShippingFiltered)) {
            return $this->sendError('Oc Product Shipping Filtered not found');
        }

        return $this->sendResponse($ocProductShippingFiltered->toArray(), 'Oc Product Shipping Filtered retrieved successfully');
    }

    /**
     * Update the specified oc_product_shipping_filtered in storage.
     * PUT/PATCH /ocProductShippingFiltereds/{id}
     *
     * @param  int $id
     * @param Updateoc_product_shipping_filteredAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_shipping_filteredAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_shipping_filtered $ocProductShippingFiltered */
        $ocProductShippingFiltered = $this->ocProductShippingFilteredRepository->findWithoutFail($id);

        if (empty($ocProductShippingFiltered)) {
            return $this->sendError('Oc Product Shipping Filtered not found');
        }

        $ocProductShippingFiltered = $this->ocProductShippingFilteredRepository->update($input, $id);

        return $this->sendResponse($ocProductShippingFiltered->toArray(), 'oc_product_shipping_filtered updated successfully');
    }

    /**
     * Remove the specified oc_product_shipping_filtered from storage.
     * DELETE /ocProductShippingFiltereds/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_shipping_filtered $ocProductShippingFiltered */
        $ocProductShippingFiltered = $this->ocProductShippingFilteredRepository->findWithoutFail($id);

        if (empty($ocProductShippingFiltered)) {
            return $this->sendError('Oc Product Shipping Filtered not found');
        }

        $ocProductShippingFiltered->delete();

        return $this->sendResponse($id, 'Oc Product Shipping Filtered deleted successfully');
    }
}
