<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_return_statusAPIRequest;
use App\Http\Requests\API\Updateoc_return_statusAPIRequest;
use App\Models\oc_return_status;
use App\Repositories\oc_return_statusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_return_statusController
 * @package App\Http\Controllers\API
 */

class oc_return_statusAPIController extends AppBaseController
{
    /** @var  oc_return_statusRepository */
    private $ocReturnStatusRepository;

    public function __construct(oc_return_statusRepository $ocReturnStatusRepo)
    {
        $this->ocReturnStatusRepository = $ocReturnStatusRepo;
    }

    /**
     * Display a listing of the oc_return_status.
     * GET|HEAD /ocReturnStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnStatusRepository->pushCriteria(new RequestCriteria($request));
        $this->ocReturnStatusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocReturnStatuses = $this->ocReturnStatusRepository->all();

        return $this->sendResponse($ocReturnStatuses->toArray(), 'Oc Return Statuses retrieved successfully');
    }

    /**
     * Store a newly created oc_return_status in storage.
     * POST /ocReturnStatuses
     *
     * @param Createoc_return_statusAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_return_statusAPIRequest $request)
    {
        $input = $request->all();

        $ocReturnStatuses = $this->ocReturnStatusRepository->create($input);

        return $this->sendResponse($ocReturnStatuses->toArray(), 'Oc Return Status saved successfully');
    }

    /**
     * Display the specified oc_return_status.
     * GET|HEAD /ocReturnStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_return_status $ocReturnStatus */
        $ocReturnStatus = $this->ocReturnStatusRepository->findWithoutFail($id);

        if (empty($ocReturnStatus)) {
            return $this->sendError('Oc Return Status not found');
        }

        return $this->sendResponse($ocReturnStatus->toArray(), 'Oc Return Status retrieved successfully');
    }

    /**
     * Update the specified oc_return_status in storage.
     * PUT/PATCH /ocReturnStatuses/{id}
     *
     * @param  int $id
     * @param Updateoc_return_statusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_return_statusAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_return_status $ocReturnStatus */
        $ocReturnStatus = $this->ocReturnStatusRepository->findWithoutFail($id);

        if (empty($ocReturnStatus)) {
            return $this->sendError('Oc Return Status not found');
        }

        $ocReturnStatus = $this->ocReturnStatusRepository->update($input, $id);

        return $this->sendResponse($ocReturnStatus->toArray(), 'oc_return_status updated successfully');
    }

    /**
     * Remove the specified oc_return_status from storage.
     * DELETE /ocReturnStatuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_return_status $ocReturnStatus */
        $ocReturnStatus = $this->ocReturnStatusRepository->findWithoutFail($id);

        if (empty($ocReturnStatus)) {
            return $this->sendError('Oc Return Status not found');
        }

        $ocReturnStatus->delete();

        return $this->sendResponse($id, 'Oc Return Status deleted successfully');
    }
}
