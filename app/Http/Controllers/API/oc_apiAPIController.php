<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_apiAPIRequest;
use App\Http\Requests\API\Updateoc_apiAPIRequest;
use App\Models\oc_api;
use App\Repositories\oc_apiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_apiController
 * @package App\Http\Controllers\API
 */

class oc_apiAPIController extends AppBaseController
{
    /** @var  oc_apiRepository */
    private $ocApiRepository;

    public function __construct(oc_apiRepository $ocApiRepo)
    {
        $this->ocApiRepository = $ocApiRepo;
    }

    /**
     * Display a listing of the oc_api.
     * GET|HEAD /ocApis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocApiRepository->pushCriteria(new RequestCriteria($request));
        $this->ocApiRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocApis = $this->ocApiRepository->all();

        return $this->sendResponse($ocApis->toArray(), 'Oc Apis retrieved successfully');
    }

    /**
     * Store a newly created oc_api in storage.
     * POST /ocApis
     *
     * @param Createoc_apiAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_apiAPIRequest $request)
    {
        $input = $request->all();

        $ocApis = $this->ocApiRepository->create($input);

        return $this->sendResponse($ocApis->toArray(), 'Oc Api saved successfully');
    }

    /**
     * Display the specified oc_api.
     * GET|HEAD /ocApis/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_api $ocApi */
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            return $this->sendError('Oc Api not found');
        }

        return $this->sendResponse($ocApi->toArray(), 'Oc Api retrieved successfully');
    }

    /**
     * Update the specified oc_api in storage.
     * PUT/PATCH /ocApis/{id}
     *
     * @param  int $id
     * @param Updateoc_apiAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_apiAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_api $ocApi */
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            return $this->sendError('Oc Api not found');
        }

        $ocApi = $this->ocApiRepository->update($input, $id);

        return $this->sendResponse($ocApi->toArray(), 'oc_api updated successfully');
    }

    /**
     * Remove the specified oc_api from storage.
     * DELETE /ocApis/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_api $ocApi */
        $ocApi = $this->ocApiRepository->findWithoutFail($id);

        if (empty($ocApi)) {
            return $this->sendError('Oc Api not found');
        }

        $ocApi->delete();

        return $this->sendResponse($id, 'Oc Api deleted successfully');
    }
}
