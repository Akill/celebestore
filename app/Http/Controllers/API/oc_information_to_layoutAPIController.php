<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_information_to_layoutAPIRequest;
use App\Http\Requests\API\Updateoc_information_to_layoutAPIRequest;
use App\Models\oc_information_to_layout;
use App\Repositories\oc_information_to_layoutRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_information_to_layoutController
 * @package App\Http\Controllers\API
 */

class oc_information_to_layoutAPIController extends AppBaseController
{
    /** @var  oc_information_to_layoutRepository */
    private $ocInformationToLayoutRepository;

    public function __construct(oc_information_to_layoutRepository $ocInformationToLayoutRepo)
    {
        $this->ocInformationToLayoutRepository = $ocInformationToLayoutRepo;
    }

    /**
     * Display a listing of the oc_information_to_layout.
     * GET|HEAD /ocInformationToLayouts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationToLayoutRepository->pushCriteria(new RequestCriteria($request));
        $this->ocInformationToLayoutRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocInformationToLayouts = $this->ocInformationToLayoutRepository->all();

        return $this->sendResponse($ocInformationToLayouts->toArray(), 'Oc Information To Layouts retrieved successfully');
    }

    /**
     * Store a newly created oc_information_to_layout in storage.
     * POST /ocInformationToLayouts
     *
     * @param Createoc_information_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_information_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        $ocInformationToLayouts = $this->ocInformationToLayoutRepository->create($input);

        return $this->sendResponse($ocInformationToLayouts->toArray(), 'Oc Information To Layout saved successfully');
    }

    /**
     * Display the specified oc_information_to_layout.
     * GET|HEAD /ocInformationToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_information_to_layout $ocInformationToLayout */
        $ocInformationToLayout = $this->ocInformationToLayoutRepository->findWithoutFail($id);

        if (empty($ocInformationToLayout)) {
            return $this->sendError('Oc Information To Layout not found');
        }

        return $this->sendResponse($ocInformationToLayout->toArray(), 'Oc Information To Layout retrieved successfully');
    }

    /**
     * Update the specified oc_information_to_layout in storage.
     * PUT/PATCH /ocInformationToLayouts/{id}
     *
     * @param  int $id
     * @param Updateoc_information_to_layoutAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_information_to_layoutAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_information_to_layout $ocInformationToLayout */
        $ocInformationToLayout = $this->ocInformationToLayoutRepository->findWithoutFail($id);

        if (empty($ocInformationToLayout)) {
            return $this->sendError('Oc Information To Layout not found');
        }

        $ocInformationToLayout = $this->ocInformationToLayoutRepository->update($input, $id);

        return $this->sendResponse($ocInformationToLayout->toArray(), 'oc_information_to_layout updated successfully');
    }

    /**
     * Remove the specified oc_information_to_layout from storage.
     * DELETE /ocInformationToLayouts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_information_to_layout $ocInformationToLayout */
        $ocInformationToLayout = $this->ocInformationToLayoutRepository->findWithoutFail($id);

        if (empty($ocInformationToLayout)) {
            return $this->sendError('Oc Information To Layout not found');
        }

        $ocInformationToLayout->delete();

        return $this->sendResponse($id, 'Oc Information To Layout deleted successfully');
    }
}
