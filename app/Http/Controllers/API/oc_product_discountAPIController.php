<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_product_discountAPIRequest;
use App\Http\Requests\API\Updateoc_product_discountAPIRequest;
use App\Models\oc_product_discount;
use App\Repositories\oc_product_discountRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_product_discountController
 * @package App\Http\Controllers\API
 */

class oc_product_discountAPIController extends AppBaseController
{
    /** @var  oc_product_discountRepository */
    private $ocProductDiscountRepository;

    public function __construct(oc_product_discountRepository $ocProductDiscountRepo)
    {
        $this->ocProductDiscountRepository = $ocProductDiscountRepo;
    }

    /**
     * Display a listing of the oc_product_discount.
     * GET|HEAD /ocProductDiscounts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductDiscountRepository->pushCriteria(new RequestCriteria($request));
        $this->ocProductDiscountRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocProductDiscounts = $this->ocProductDiscountRepository->all();

        return $this->sendResponse($ocProductDiscounts->toArray(), 'Oc Product Discounts retrieved successfully');
    }

    /**
     * Store a newly created oc_product_discount in storage.
     * POST /ocProductDiscounts
     *
     * @param Createoc_product_discountAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_discountAPIRequest $request)
    {
        $input = $request->all();

        $ocProductDiscounts = $this->ocProductDiscountRepository->create($input);

        return $this->sendResponse($ocProductDiscounts->toArray(), 'Oc Product Discount saved successfully');
    }

    /**
     * Display the specified oc_product_discount.
     * GET|HEAD /ocProductDiscounts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_product_discount $ocProductDiscount */
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            return $this->sendError('Oc Product Discount not found');
        }

        return $this->sendResponse($ocProductDiscount->toArray(), 'Oc Product Discount retrieved successfully');
    }

    /**
     * Update the specified oc_product_discount in storage.
     * PUT/PATCH /ocProductDiscounts/{id}
     *
     * @param  int $id
     * @param Updateoc_product_discountAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_discountAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_product_discount $ocProductDiscount */
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            return $this->sendError('Oc Product Discount not found');
        }

        $ocProductDiscount = $this->ocProductDiscountRepository->update($input, $id);

        return $this->sendResponse($ocProductDiscount->toArray(), 'oc_product_discount updated successfully');
    }

    /**
     * Remove the specified oc_product_discount from storage.
     * DELETE /ocProductDiscounts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_product_discount $ocProductDiscount */
        $ocProductDiscount = $this->ocProductDiscountRepository->findWithoutFail($id);

        if (empty($ocProductDiscount)) {
            return $this->sendError('Oc Product Discount not found');
        }

        $ocProductDiscount->delete();

        return $this->sendResponse($id, 'Oc Product Discount deleted successfully');
    }
}
