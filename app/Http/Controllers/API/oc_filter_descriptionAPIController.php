<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_filter_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_filter_descriptionAPIRequest;
use App\Models\oc_filter_description;
use App\Repositories\oc_filter_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_filter_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_filter_descriptionAPIController extends AppBaseController
{
    /** @var  oc_filter_descriptionRepository */
    private $ocFilterDescriptionRepository;

    public function __construct(oc_filter_descriptionRepository $ocFilterDescriptionRepo)
    {
        $this->ocFilterDescriptionRepository = $ocFilterDescriptionRepo;
    }

    /**
     * Display a listing of the oc_filter_description.
     * GET|HEAD /ocFilterDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocFilterDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocFilterDescriptions = $this->ocFilterDescriptionRepository->all();

        return $this->sendResponse($ocFilterDescriptions->toArray(), 'Oc Filter Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_filter_description in storage.
     * POST /ocFilterDescriptions
     *
     * @param Createoc_filter_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filter_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocFilterDescriptions = $this->ocFilterDescriptionRepository->create($input);

        return $this->sendResponse($ocFilterDescriptions->toArray(), 'Oc Filter Description saved successfully');
    }

    /**
     * Display the specified oc_filter_description.
     * GET|HEAD /ocFilterDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_filter_description $ocFilterDescription */
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            return $this->sendError('Oc Filter Description not found');
        }

        return $this->sendResponse($ocFilterDescription->toArray(), 'Oc Filter Description retrieved successfully');
    }

    /**
     * Update the specified oc_filter_description in storage.
     * PUT/PATCH /ocFilterDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_filter_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filter_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_filter_description $ocFilterDescription */
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            return $this->sendError('Oc Filter Description not found');
        }

        $ocFilterDescription = $this->ocFilterDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocFilterDescription->toArray(), 'oc_filter_description updated successfully');
    }

    /**
     * Remove the specified oc_filter_description from storage.
     * DELETE /ocFilterDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_filter_description $ocFilterDescription */
        $ocFilterDescription = $this->ocFilterDescriptionRepository->findWithoutFail($id);

        if (empty($ocFilterDescription)) {
            return $this->sendError('Oc Filter Description not found');
        }

        $ocFilterDescription->delete();

        return $this->sendResponse($id, 'Oc Filter Description deleted successfully');
    }
}
