<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_zone_to_geo_zoneAPIRequest;
use App\Http\Requests\API\Updateoc_zone_to_geo_zoneAPIRequest;
use App\Models\oc_zone_to_geo_zone;
use App\Repositories\oc_zone_to_geo_zoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_zone_to_geo_zoneController
 * @package App\Http\Controllers\API
 */

class oc_zone_to_geo_zoneAPIController extends AppBaseController
{
    /** @var  oc_zone_to_geo_zoneRepository */
    private $ocZoneToGeoZoneRepository;

    public function __construct(oc_zone_to_geo_zoneRepository $ocZoneToGeoZoneRepo)
    {
        $this->ocZoneToGeoZoneRepository = $ocZoneToGeoZoneRepo;
    }

    /**
     * Display a listing of the oc_zone_to_geo_zone.
     * GET|HEAD /ocZoneToGeoZones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocZoneToGeoZoneRepository->pushCriteria(new RequestCriteria($request));
        $this->ocZoneToGeoZoneRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocZoneToGeoZones = $this->ocZoneToGeoZoneRepository->all();

        return $this->sendResponse($ocZoneToGeoZones->toArray(), 'Oc Zone To Geo Zones retrieved successfully');
    }

    /**
     * Store a newly created oc_zone_to_geo_zone in storage.
     * POST /ocZoneToGeoZones
     *
     * @param Createoc_zone_to_geo_zoneAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_zone_to_geo_zoneAPIRequest $request)
    {
        $input = $request->all();

        $ocZoneToGeoZones = $this->ocZoneToGeoZoneRepository->create($input);

        return $this->sendResponse($ocZoneToGeoZones->toArray(), 'Oc Zone To Geo Zone saved successfully');
    }

    /**
     * Display the specified oc_zone_to_geo_zone.
     * GET|HEAD /ocZoneToGeoZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_zone_to_geo_zone $ocZoneToGeoZone */
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            return $this->sendError('Oc Zone To Geo Zone not found');
        }

        return $this->sendResponse($ocZoneToGeoZone->toArray(), 'Oc Zone To Geo Zone retrieved successfully');
    }

    /**
     * Update the specified oc_zone_to_geo_zone in storage.
     * PUT/PATCH /ocZoneToGeoZones/{id}
     *
     * @param  int $id
     * @param Updateoc_zone_to_geo_zoneAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_zone_to_geo_zoneAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_zone_to_geo_zone $ocZoneToGeoZone */
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            return $this->sendError('Oc Zone To Geo Zone not found');
        }

        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->update($input, $id);

        return $this->sendResponse($ocZoneToGeoZone->toArray(), 'oc_zone_to_geo_zone updated successfully');
    }

    /**
     * Remove the specified oc_zone_to_geo_zone from storage.
     * DELETE /ocZoneToGeoZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_zone_to_geo_zone $ocZoneToGeoZone */
        $ocZoneToGeoZone = $this->ocZoneToGeoZoneRepository->findWithoutFail($id);

        if (empty($ocZoneToGeoZone)) {
            return $this->sendError('Oc Zone To Geo Zone not found');
        }

        $ocZoneToGeoZone->delete();

        return $this->sendResponse($id, 'Oc Zone To Geo Zone deleted successfully');
    }
}
