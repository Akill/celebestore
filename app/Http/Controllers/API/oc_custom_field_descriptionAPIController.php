<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_custom_field_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_custom_field_descriptionAPIRequest;
use App\Models\oc_custom_field_description;
use App\Repositories\oc_custom_field_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_custom_field_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_custom_field_descriptionAPIController extends AppBaseController
{
    /** @var  oc_custom_field_descriptionRepository */
    private $ocCustomFieldDescriptionRepository;

    public function __construct(oc_custom_field_descriptionRepository $ocCustomFieldDescriptionRepo)
    {
        $this->ocCustomFieldDescriptionRepository = $ocCustomFieldDescriptionRepo;
    }

    /**
     * Display a listing of the oc_custom_field_description.
     * GET|HEAD /ocCustomFieldDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomFieldDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomFieldDescriptions = $this->ocCustomFieldDescriptionRepository->all();

        return $this->sendResponse($ocCustomFieldDescriptions->toArray(), 'Oc Custom Field Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_custom_field_description in storage.
     * POST /ocCustomFieldDescriptions
     *
     * @param Createoc_custom_field_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldDescriptions = $this->ocCustomFieldDescriptionRepository->create($input);

        return $this->sendResponse($ocCustomFieldDescriptions->toArray(), 'Oc Custom Field Description saved successfully');
    }

    /**
     * Display the specified oc_custom_field_description.
     * GET|HEAD /ocCustomFieldDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_custom_field_description $ocCustomFieldDescription */
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            return $this->sendError('Oc Custom Field Description not found');
        }

        return $this->sendResponse($ocCustomFieldDescription->toArray(), 'Oc Custom Field Description retrieved successfully');
    }

    /**
     * Update the specified oc_custom_field_description in storage.
     * PUT/PATCH /ocCustomFieldDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_custom_field_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_custom_field_description $ocCustomFieldDescription */
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            return $this->sendError('Oc Custom Field Description not found');
        }

        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocCustomFieldDescription->toArray(), 'oc_custom_field_description updated successfully');
    }

    /**
     * Remove the specified oc_custom_field_description from storage.
     * DELETE /ocCustomFieldDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_custom_field_description $ocCustomFieldDescription */
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            return $this->sendError('Oc Custom Field Description not found');
        }

        $ocCustomFieldDescription->delete();

        return $this->sendResponse($id, 'Oc Custom Field Description deleted successfully');
    }
}
