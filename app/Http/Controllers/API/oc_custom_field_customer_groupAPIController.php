<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_custom_field_customer_groupAPIRequest;
use App\Http\Requests\API\Updateoc_custom_field_customer_groupAPIRequest;
use App\Models\oc_custom_field_customer_group;
use App\Repositories\oc_custom_field_customer_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_custom_field_customer_groupController
 * @package App\Http\Controllers\API
 */

class oc_custom_field_customer_groupAPIController extends AppBaseController
{
    /** @var  oc_custom_field_customer_groupRepository */
    private $ocCustomFieldCustomerGroupRepository;

    public function __construct(oc_custom_field_customer_groupRepository $ocCustomFieldCustomerGroupRepo)
    {
        $this->ocCustomFieldCustomerGroupRepository = $ocCustomFieldCustomerGroupRepo;
    }

    /**
     * Display a listing of the oc_custom_field_customer_group.
     * GET|HEAD /ocCustomFieldCustomerGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldCustomerGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomFieldCustomerGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomFieldCustomerGroups = $this->ocCustomFieldCustomerGroupRepository->all();

        return $this->sendResponse($ocCustomFieldCustomerGroups->toArray(), 'Oc Custom Field Customer Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_custom_field_customer_group in storage.
     * POST /ocCustomFieldCustomerGroups
     *
     * @param Createoc_custom_field_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldCustomerGroups = $this->ocCustomFieldCustomerGroupRepository->create($input);

        return $this->sendResponse($ocCustomFieldCustomerGroups->toArray(), 'Oc Custom Field Customer Group saved successfully');
    }

    /**
     * Display the specified oc_custom_field_customer_group.
     * GET|HEAD /ocCustomFieldCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_custom_field_customer_group $ocCustomFieldCustomerGroup */
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            return $this->sendError('Oc Custom Field Customer Group not found');
        }

        return $this->sendResponse($ocCustomFieldCustomerGroup->toArray(), 'Oc Custom Field Customer Group retrieved successfully');
    }

    /**
     * Update the specified oc_custom_field_customer_group in storage.
     * PUT/PATCH /ocCustomFieldCustomerGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_custom_field_customer_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_customer_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_custom_field_customer_group $ocCustomFieldCustomerGroup */
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            return $this->sendError('Oc Custom Field Customer Group not found');
        }

        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->update($input, $id);

        return $this->sendResponse($ocCustomFieldCustomerGroup->toArray(), 'oc_custom_field_customer_group updated successfully');
    }

    /**
     * Remove the specified oc_custom_field_customer_group from storage.
     * DELETE /ocCustomFieldCustomerGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_custom_field_customer_group $ocCustomFieldCustomerGroup */
        $ocCustomFieldCustomerGroup = $this->ocCustomFieldCustomerGroupRepository->findWithoutFail($id);

        if (empty($ocCustomFieldCustomerGroup)) {
            return $this->sendError('Oc Custom Field Customer Group not found');
        }

        $ocCustomFieldCustomerGroup->delete();

        return $this->sendResponse($id, 'Oc Custom Field Customer Group deleted successfully');
    }
}
