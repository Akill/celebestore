<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_weight_classAPIRequest;
use App\Http\Requests\API\Updateoc_weight_classAPIRequest;
use App\Models\oc_weight_class;
use App\Repositories\oc_weight_classRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_weight_classController
 * @package App\Http\Controllers\API
 */

class oc_weight_classAPIController extends AppBaseController
{
    /** @var  oc_weight_classRepository */
    private $ocWeightClassRepository;

    public function __construct(oc_weight_classRepository $ocWeightClassRepo)
    {
        $this->ocWeightClassRepository = $ocWeightClassRepo;
    }

    /**
     * Display a listing of the oc_weight_class.
     * GET|HEAD /ocWeightClasses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocWeightClassRepository->pushCriteria(new RequestCriteria($request));
        $this->ocWeightClassRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocWeightClasses = $this->ocWeightClassRepository->all();

        return $this->sendResponse($ocWeightClasses->toArray(), 'Oc Weight Classes retrieved successfully');
    }

    /**
     * Store a newly created oc_weight_class in storage.
     * POST /ocWeightClasses
     *
     * @param Createoc_weight_classAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_weight_classAPIRequest $request)
    {
        $input = $request->all();

        $ocWeightClasses = $this->ocWeightClassRepository->create($input);

        return $this->sendResponse($ocWeightClasses->toArray(), 'Oc Weight Class saved successfully');
    }

    /**
     * Display the specified oc_weight_class.
     * GET|HEAD /ocWeightClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_weight_class $ocWeightClass */
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            return $this->sendError('Oc Weight Class not found');
        }

        return $this->sendResponse($ocWeightClass->toArray(), 'Oc Weight Class retrieved successfully');
    }

    /**
     * Update the specified oc_weight_class in storage.
     * PUT/PATCH /ocWeightClasses/{id}
     *
     * @param  int $id
     * @param Updateoc_weight_classAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_weight_classAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_weight_class $ocWeightClass */
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            return $this->sendError('Oc Weight Class not found');
        }

        $ocWeightClass = $this->ocWeightClassRepository->update($input, $id);

        return $this->sendResponse($ocWeightClass->toArray(), 'oc_weight_class updated successfully');
    }

    /**
     * Remove the specified oc_weight_class from storage.
     * DELETE /ocWeightClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_weight_class $ocWeightClass */
        $ocWeightClass = $this->ocWeightClassRepository->findWithoutFail($id);

        if (empty($ocWeightClass)) {
            return $this->sendError('Oc Weight Class not found');
        }

        $ocWeightClass->delete();

        return $this->sendResponse($id, 'Oc Weight Class deleted successfully');
    }
}
