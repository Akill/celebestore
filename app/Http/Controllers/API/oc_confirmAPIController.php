<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_confirmAPIRequest;
use App\Http\Requests\API\Updateoc_confirmAPIRequest;
use App\Models\oc_confirm;
use App\Repositories\oc_confirmRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_confirmController
 * @package App\Http\Controllers\API
 */

class oc_confirmAPIController extends AppBaseController
{
    /** @var  oc_confirmRepository */
    private $ocConfirmRepository;

    public function __construct(oc_confirmRepository $ocConfirmRepo)
    {
        $this->ocConfirmRepository = $ocConfirmRepo;
    }

    /**
     * Display a listing of the oc_confirm.
     * GET|HEAD /ocConfirms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocConfirmRepository->pushCriteria(new RequestCriteria($request));
        $this->ocConfirmRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocConfirms = $this->ocConfirmRepository->all();

        return $this->sendResponse($ocConfirms->toArray(), 'Oc Confirms retrieved successfully');
    }

    /**
     * Store a newly created oc_confirm in storage.
     * POST /ocConfirms
     *
     * @param Createoc_confirmAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_confirmAPIRequest $request)
    {
        $input = $request->all();

        $ocConfirms = $this->ocConfirmRepository->create($input);

        return $this->sendResponse($ocConfirms->toArray(), 'Oc Confirm saved successfully');
    }

    /**
     * Display the specified oc_confirm.
     * GET|HEAD /ocConfirms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_confirm $ocConfirm */
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            return $this->sendError('Oc Confirm not found');
        }

        return $this->sendResponse($ocConfirm->toArray(), 'Oc Confirm retrieved successfully');
    }

    /**
     * Update the specified oc_confirm in storage.
     * PUT/PATCH /ocConfirms/{id}
     *
     * @param  int $id
     * @param Updateoc_confirmAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_confirmAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_confirm $ocConfirm */
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            return $this->sendError('Oc Confirm not found');
        }

        $ocConfirm = $this->ocConfirmRepository->update($input, $id);

        return $this->sendResponse($ocConfirm->toArray(), 'oc_confirm updated successfully');
    }

    /**
     * Remove the specified oc_confirm from storage.
     * DELETE /ocConfirms/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_confirm $ocConfirm */
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            return $this->sendError('Oc Confirm not found');
        }

        $ocConfirm->delete();

        return $this->sendResponse($id, 'Oc Confirm deleted successfully');
    }
}
