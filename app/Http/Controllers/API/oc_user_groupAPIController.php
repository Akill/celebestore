<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_user_groupAPIRequest;
use App\Http\Requests\API\Updateoc_user_groupAPIRequest;
use App\Models\oc_user_group;
use App\Repositories\oc_user_groupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_user_groupController
 * @package App\Http\Controllers\API
 */

class oc_user_groupAPIController extends AppBaseController
{
    /** @var  oc_user_groupRepository */
    private $ocUserGroupRepository;

    public function __construct(oc_user_groupRepository $ocUserGroupRepo)
    {
        $this->ocUserGroupRepository = $ocUserGroupRepo;
    }

    /**
     * Display a listing of the oc_user_group.
     * GET|HEAD /ocUserGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUserGroupRepository->pushCriteria(new RequestCriteria($request));
        $this->ocUserGroupRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocUserGroups = $this->ocUserGroupRepository->all();

        return $this->sendResponse($ocUserGroups->toArray(), 'Oc User Groups retrieved successfully');
    }

    /**
     * Store a newly created oc_user_group in storage.
     * POST /ocUserGroups
     *
     * @param Createoc_user_groupAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_user_groupAPIRequest $request)
    {
        $input = $request->all();

        $ocUserGroups = $this->ocUserGroupRepository->create($input);

        return $this->sendResponse($ocUserGroups->toArray(), 'Oc User Group saved successfully');
    }

    /**
     * Display the specified oc_user_group.
     * GET|HEAD /ocUserGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_user_group $ocUserGroup */
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            return $this->sendError('Oc User Group not found');
        }

        return $this->sendResponse($ocUserGroup->toArray(), 'Oc User Group retrieved successfully');
    }

    /**
     * Update the specified oc_user_group in storage.
     * PUT/PATCH /ocUserGroups/{id}
     *
     * @param  int $id
     * @param Updateoc_user_groupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_user_groupAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_user_group $ocUserGroup */
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            return $this->sendError('Oc User Group not found');
        }

        $ocUserGroup = $this->ocUserGroupRepository->update($input, $id);

        return $this->sendResponse($ocUserGroup->toArray(), 'oc_user_group updated successfully');
    }

    /**
     * Remove the specified oc_user_group from storage.
     * DELETE /ocUserGroups/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_user_group $ocUserGroup */
        $ocUserGroup = $this->ocUserGroupRepository->findWithoutFail($id);

        if (empty($ocUserGroup)) {
            return $this->sendError('Oc User Group not found');
        }

        $ocUserGroup->delete();

        return $this->sendResponse($id, 'Oc User Group deleted successfully');
    }
}
