<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_attribute_group_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_attribute_group_descriptionAPIRequest;
use App\Models\oc_attribute_group_description;
use App\Repositories\oc_attribute_group_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_attribute_group_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_attribute_group_descriptionAPIController extends AppBaseController
{
    /** @var  oc_attribute_group_descriptionRepository */
    private $ocAttributeGroupDescriptionRepository;

    public function __construct(oc_attribute_group_descriptionRepository $ocAttributeGroupDescriptionRepo)
    {
        $this->ocAttributeGroupDescriptionRepository = $ocAttributeGroupDescriptionRepo;
    }

    /**
     * Display a listing of the oc_attribute_group_description.
     * GET|HEAD /ocAttributeGroupDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeGroupDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocAttributeGroupDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocAttributeGroupDescriptions = $this->ocAttributeGroupDescriptionRepository->all();

        return $this->sendResponse($ocAttributeGroupDescriptions->toArray(), 'Oc Attribute Group Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_attribute_group_description in storage.
     * POST /ocAttributeGroupDescriptions
     *
     * @param Createoc_attribute_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocAttributeGroupDescriptions = $this->ocAttributeGroupDescriptionRepository->create($input);

        return $this->sendResponse($ocAttributeGroupDescriptions->toArray(), 'Oc Attribute Group Description saved successfully');
    }

    /**
     * Display the specified oc_attribute_group_description.
     * GET|HEAD /ocAttributeGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_attribute_group_description $ocAttributeGroupDescription */
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            return $this->sendError('Oc Attribute Group Description not found');
        }

        return $this->sendResponse($ocAttributeGroupDescription->toArray(), 'Oc Attribute Group Description retrieved successfully');
    }

    /**
     * Update the specified oc_attribute_group_description in storage.
     * PUT/PATCH /ocAttributeGroupDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_attribute_group_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_group_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_attribute_group_description $ocAttributeGroupDescription */
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            return $this->sendError('Oc Attribute Group Description not found');
        }

        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocAttributeGroupDescription->toArray(), 'oc_attribute_group_description updated successfully');
    }

    /**
     * Remove the specified oc_attribute_group_description from storage.
     * DELETE /ocAttributeGroupDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_attribute_group_description $ocAttributeGroupDescription */
        $ocAttributeGroupDescription = $this->ocAttributeGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeGroupDescription)) {
            return $this->sendError('Oc Attribute Group Description not found');
        }

        $ocAttributeGroupDescription->delete();

        return $this->sendResponse($id, 'Oc Attribute Group Description deleted successfully');
    }
}
