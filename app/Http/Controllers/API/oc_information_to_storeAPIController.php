<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_information_to_storeAPIRequest;
use App\Http\Requests\API\Updateoc_information_to_storeAPIRequest;
use App\Models\oc_information_to_store;
use App\Repositories\oc_information_to_storeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_information_to_storeController
 * @package App\Http\Controllers\API
 */

class oc_information_to_storeAPIController extends AppBaseController
{
    /** @var  oc_information_to_storeRepository */
    private $ocInformationToStoreRepository;

    public function __construct(oc_information_to_storeRepository $ocInformationToStoreRepo)
    {
        $this->ocInformationToStoreRepository = $ocInformationToStoreRepo;
    }

    /**
     * Display a listing of the oc_information_to_store.
     * GET|HEAD /ocInformationToStores
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationToStoreRepository->pushCriteria(new RequestCriteria($request));
        $this->ocInformationToStoreRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocInformationToStores = $this->ocInformationToStoreRepository->all();

        return $this->sendResponse($ocInformationToStores->toArray(), 'Oc Information To Stores retrieved successfully');
    }

    /**
     * Store a newly created oc_information_to_store in storage.
     * POST /ocInformationToStores
     *
     * @param Createoc_information_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_information_to_storeAPIRequest $request)
    {
        $input = $request->all();

        $ocInformationToStores = $this->ocInformationToStoreRepository->create($input);

        return $this->sendResponse($ocInformationToStores->toArray(), 'Oc Information To Store saved successfully');
    }

    /**
     * Display the specified oc_information_to_store.
     * GET|HEAD /ocInformationToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_information_to_store $ocInformationToStore */
        $ocInformationToStore = $this->ocInformationToStoreRepository->findWithoutFail($id);

        if (empty($ocInformationToStore)) {
            return $this->sendError('Oc Information To Store not found');
        }

        return $this->sendResponse($ocInformationToStore->toArray(), 'Oc Information To Store retrieved successfully');
    }

    /**
     * Update the specified oc_information_to_store in storage.
     * PUT/PATCH /ocInformationToStores/{id}
     *
     * @param  int $id
     * @param Updateoc_information_to_storeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_information_to_storeAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_information_to_store $ocInformationToStore */
        $ocInformationToStore = $this->ocInformationToStoreRepository->findWithoutFail($id);

        if (empty($ocInformationToStore)) {
            return $this->sendError('Oc Information To Store not found');
        }

        $ocInformationToStore = $this->ocInformationToStoreRepository->update($input, $id);

        return $this->sendResponse($ocInformationToStore->toArray(), 'oc_information_to_store updated successfully');
    }

    /**
     * Remove the specified oc_information_to_store from storage.
     * DELETE /ocInformationToStores/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_information_to_store $ocInformationToStore */
        $ocInformationToStore = $this->ocInformationToStoreRepository->findWithoutFail($id);

        if (empty($ocInformationToStore)) {
            return $this->sendError('Oc Information To Store not found');
        }

        $ocInformationToStore->delete();

        return $this->sendResponse($id, 'Oc Information To Store deleted successfully');
    }
}
