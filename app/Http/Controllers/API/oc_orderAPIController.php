<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_orderAPIRequest;
use App\Http\Requests\API\Updateoc_orderAPIRequest;
use App\Models\oc_order;
use App\Repositories\oc_orderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_orderController
 * @package App\Http\Controllers\API
 */

class oc_orderAPIController extends AppBaseController
{
    /** @var  oc_orderRepository */
    private $ocOrderRepository;

    public function __construct(oc_orderRepository $ocOrderRepo)
    {
        $this->ocOrderRepository = $ocOrderRepo;
    }

    /**
     * Display a listing of the oc_order.
     * GET|HEAD /ocOrders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrders = $this->ocOrderRepository->all();

        return $this->sendResponse($ocOrders->toArray(), 'Oc Orders retrieved successfully');
    }

    /**
     * Store a newly created oc_order in storage.
     * POST /ocOrders
     *
     * @param Createoc_orderAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_orderAPIRequest $request)
    {
        $input = $request->all();

        $ocOrders = $this->ocOrderRepository->create($input);

        return $this->sendResponse($ocOrders->toArray(), 'Oc Order saved successfully');
    }

    /**
     * Display the specified oc_order.
     * GET|HEAD /ocOrders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order $ocOrder */
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            return $this->sendError('Oc Order not found');
        }

        return $this->sendResponse($ocOrder->toArray(), 'Oc Order retrieved successfully');
    }

    /**
     * Update the specified oc_order in storage.
     * PUT/PATCH /ocOrders/{id}
     *
     * @param  int $id
     * @param Updateoc_orderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_orderAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order $ocOrder */
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            return $this->sendError('Oc Order not found');
        }

        $ocOrder = $this->ocOrderRepository->update($input, $id);

        return $this->sendResponse($ocOrder->toArray(), 'oc_order updated successfully');
    }

    /**
     * Remove the specified oc_order from storage.
     * DELETE /ocOrders/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order $ocOrder */
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            return $this->sendError('Oc Order not found');
        }

        $ocOrder->delete();

        return $this->sendResponse($id, 'Oc Order deleted successfully');
    }
}
