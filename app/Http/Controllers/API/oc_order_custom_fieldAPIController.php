<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_order_custom_fieldAPIRequest;
use App\Http\Requests\API\Updateoc_order_custom_fieldAPIRequest;
use App\Models\oc_order_custom_field;
use App\Repositories\oc_order_custom_fieldRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_order_custom_fieldController
 * @package App\Http\Controllers\API
 */

class oc_order_custom_fieldAPIController extends AppBaseController
{
    /** @var  oc_order_custom_fieldRepository */
    private $ocOrderCustomFieldRepository;

    public function __construct(oc_order_custom_fieldRepository $ocOrderCustomFieldRepo)
    {
        $this->ocOrderCustomFieldRepository = $ocOrderCustomFieldRepo;
    }

    /**
     * Display a listing of the oc_order_custom_field.
     * GET|HEAD /ocOrderCustomFields
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderCustomFieldRepository->pushCriteria(new RequestCriteria($request));
        $this->ocOrderCustomFieldRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocOrderCustomFields = $this->ocOrderCustomFieldRepository->all();

        return $this->sendResponse($ocOrderCustomFields->toArray(), 'Oc Order Custom Fields retrieved successfully');
    }

    /**
     * Store a newly created oc_order_custom_field in storage.
     * POST /ocOrderCustomFields
     *
     * @param Createoc_order_custom_fieldAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_custom_fieldAPIRequest $request)
    {
        $input = $request->all();

        $ocOrderCustomFields = $this->ocOrderCustomFieldRepository->create($input);

        return $this->sendResponse($ocOrderCustomFields->toArray(), 'Oc Order Custom Field saved successfully');
    }

    /**
     * Display the specified oc_order_custom_field.
     * GET|HEAD /ocOrderCustomFields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_order_custom_field $ocOrderCustomField */
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            return $this->sendError('Oc Order Custom Field not found');
        }

        return $this->sendResponse($ocOrderCustomField->toArray(), 'Oc Order Custom Field retrieved successfully');
    }

    /**
     * Update the specified oc_order_custom_field in storage.
     * PUT/PATCH /ocOrderCustomFields/{id}
     *
     * @param  int $id
     * @param Updateoc_order_custom_fieldAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_custom_fieldAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_order_custom_field $ocOrderCustomField */
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            return $this->sendError('Oc Order Custom Field not found');
        }

        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->update($input, $id);

        return $this->sendResponse($ocOrderCustomField->toArray(), 'oc_order_custom_field updated successfully');
    }

    /**
     * Remove the specified oc_order_custom_field from storage.
     * DELETE /ocOrderCustomFields/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_order_custom_field $ocOrderCustomField */
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            return $this->sendError('Oc Order Custom Field not found');
        }

        $ocOrderCustomField->delete();

        return $this->sendResponse($id, 'Oc Order Custom Field deleted successfully');
    }
}
