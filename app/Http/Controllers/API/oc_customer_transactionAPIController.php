<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_transactionAPIRequest;
use App\Http\Requests\API\Updateoc_customer_transactionAPIRequest;
use App\Models\oc_customer_transaction;
use App\Repositories\oc_customer_transactionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_transactionController
 * @package App\Http\Controllers\API
 */

class oc_customer_transactionAPIController extends AppBaseController
{
    /** @var  oc_customer_transactionRepository */
    private $ocCustomerTransactionRepository;

    public function __construct(oc_customer_transactionRepository $ocCustomerTransactionRepo)
    {
        $this->ocCustomerTransactionRepository = $ocCustomerTransactionRepo;
    }

    /**
     * Display a listing of the oc_customer_transaction.
     * GET|HEAD /ocCustomerTransactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerTransactionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerTransactionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerTransactions = $this->ocCustomerTransactionRepository->all();

        return $this->sendResponse($ocCustomerTransactions->toArray(), 'Oc Customer Transactions retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_transaction in storage.
     * POST /ocCustomerTransactions
     *
     * @param Createoc_customer_transactionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_transactionAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerTransactions = $this->ocCustomerTransactionRepository->create($input);

        return $this->sendResponse($ocCustomerTransactions->toArray(), 'Oc Customer Transaction saved successfully');
    }

    /**
     * Display the specified oc_customer_transaction.
     * GET|HEAD /ocCustomerTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_transaction $ocCustomerTransaction */
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            return $this->sendError('Oc Customer Transaction not found');
        }

        return $this->sendResponse($ocCustomerTransaction->toArray(), 'Oc Customer Transaction retrieved successfully');
    }

    /**
     * Update the specified oc_customer_transaction in storage.
     * PUT/PATCH /ocCustomerTransactions/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_transactionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_transactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_transaction $ocCustomerTransaction */
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            return $this->sendError('Oc Customer Transaction not found');
        }

        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->update($input, $id);

        return $this->sendResponse($ocCustomerTransaction->toArray(), 'oc_customer_transaction updated successfully');
    }

    /**
     * Remove the specified oc_customer_transaction from storage.
     * DELETE /ocCustomerTransactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_transaction $ocCustomerTransaction */
        $ocCustomerTransaction = $this->ocCustomerTransactionRepository->findWithoutFail($id);

        if (empty($ocCustomerTransaction)) {
            return $this->sendError('Oc Customer Transaction not found');
        }

        $ocCustomerTransaction->delete();

        return $this->sendResponse($id, 'Oc Customer Transaction deleted successfully');
    }
}
