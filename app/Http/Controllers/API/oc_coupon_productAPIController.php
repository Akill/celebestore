<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_coupon_productAPIRequest;
use App\Http\Requests\API\Updateoc_coupon_productAPIRequest;
use App\Models\oc_coupon_product;
use App\Repositories\oc_coupon_productRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_coupon_productController
 * @package App\Http\Controllers\API
 */

class oc_coupon_productAPIController extends AppBaseController
{
    /** @var  oc_coupon_productRepository */
    private $ocCouponProductRepository;

    public function __construct(oc_coupon_productRepository $ocCouponProductRepo)
    {
        $this->ocCouponProductRepository = $ocCouponProductRepo;
    }

    /**
     * Display a listing of the oc_coupon_product.
     * GET|HEAD /ocCouponProducts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponProductRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCouponProductRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCouponProducts = $this->ocCouponProductRepository->all();

        return $this->sendResponse($ocCouponProducts->toArray(), 'Oc Coupon Products retrieved successfully');
    }

    /**
     * Store a newly created oc_coupon_product in storage.
     * POST /ocCouponProducts
     *
     * @param Createoc_coupon_productAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_productAPIRequest $request)
    {
        $input = $request->all();

        $ocCouponProducts = $this->ocCouponProductRepository->create($input);

        return $this->sendResponse($ocCouponProducts->toArray(), 'Oc Coupon Product saved successfully');
    }

    /**
     * Display the specified oc_coupon_product.
     * GET|HEAD /ocCouponProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_coupon_product $ocCouponProduct */
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            return $this->sendError('Oc Coupon Product not found');
        }

        return $this->sendResponse($ocCouponProduct->toArray(), 'Oc Coupon Product retrieved successfully');
    }

    /**
     * Update the specified oc_coupon_product in storage.
     * PUT/PATCH /ocCouponProducts/{id}
     *
     * @param  int $id
     * @param Updateoc_coupon_productAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_productAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_coupon_product $ocCouponProduct */
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            return $this->sendError('Oc Coupon Product not found');
        }

        $ocCouponProduct = $this->ocCouponProductRepository->update($input, $id);

        return $this->sendResponse($ocCouponProduct->toArray(), 'oc_coupon_product updated successfully');
    }

    /**
     * Remove the specified oc_coupon_product from storage.
     * DELETE /ocCouponProducts/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_coupon_product $ocCouponProduct */
        $ocCouponProduct = $this->ocCouponProductRepository->findWithoutFail($id);

        if (empty($ocCouponProduct)) {
            return $this->sendError('Oc Coupon Product not found');
        }

        $ocCouponProduct->delete();

        return $this->sendResponse($id, 'Oc Coupon Product deleted successfully');
    }
}
