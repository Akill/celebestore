<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_length_classAPIRequest;
use App\Http\Requests\API\Updateoc_length_classAPIRequest;
use App\Models\oc_length_class;
use App\Repositories\oc_length_classRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_length_classController
 * @package App\Http\Controllers\API
 */

class oc_length_classAPIController extends AppBaseController
{
    /** @var  oc_length_classRepository */
    private $ocLengthClassRepository;

    public function __construct(oc_length_classRepository $ocLengthClassRepo)
    {
        $this->ocLengthClassRepository = $ocLengthClassRepo;
    }

    /**
     * Display a listing of the oc_length_class.
     * GET|HEAD /ocLengthClasses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLengthClassRepository->pushCriteria(new RequestCriteria($request));
        $this->ocLengthClassRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocLengthClasses = $this->ocLengthClassRepository->all();

        return $this->sendResponse($ocLengthClasses->toArray(), 'Oc Length Classes retrieved successfully');
    }

    /**
     * Store a newly created oc_length_class in storage.
     * POST /ocLengthClasses
     *
     * @param Createoc_length_classAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_length_classAPIRequest $request)
    {
        $input = $request->all();

        $ocLengthClasses = $this->ocLengthClassRepository->create($input);

        return $this->sendResponse($ocLengthClasses->toArray(), 'Oc Length Class saved successfully');
    }

    /**
     * Display the specified oc_length_class.
     * GET|HEAD /ocLengthClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_length_class $ocLengthClass */
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            return $this->sendError('Oc Length Class not found');
        }

        return $this->sendResponse($ocLengthClass->toArray(), 'Oc Length Class retrieved successfully');
    }

    /**
     * Update the specified oc_length_class in storage.
     * PUT/PATCH /ocLengthClasses/{id}
     *
     * @param  int $id
     * @param Updateoc_length_classAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_length_classAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_length_class $ocLengthClass */
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            return $this->sendError('Oc Length Class not found');
        }

        $ocLengthClass = $this->ocLengthClassRepository->update($input, $id);

        return $this->sendResponse($ocLengthClass->toArray(), 'oc_length_class updated successfully');
    }

    /**
     * Remove the specified oc_length_class from storage.
     * DELETE /ocLengthClasses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_length_class $ocLengthClass */
        $ocLengthClass = $this->ocLengthClassRepository->findWithoutFail($id);

        if (empty($ocLengthClass)) {
            return $this->sendError('Oc Length Class not found');
        }

        $ocLengthClass->delete();

        return $this->sendResponse($id, 'Oc Length Class deleted successfully');
    }
}
