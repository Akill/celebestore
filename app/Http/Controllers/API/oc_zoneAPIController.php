<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_zoneAPIRequest;
use App\Http\Requests\API\Updateoc_zoneAPIRequest;
use App\Models\oc_zone;
use App\Repositories\oc_zoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_zoneController
 * @package App\Http\Controllers\API
 */

class oc_zoneAPIController extends AppBaseController
{
    /** @var  oc_zoneRepository */
    private $ocZoneRepository;

    public function __construct(oc_zoneRepository $ocZoneRepo)
    {
        $this->ocZoneRepository = $ocZoneRepo;
    }

    /**
     * Display a listing of the oc_zone.
     * GET|HEAD /ocZones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocZoneRepository->pushCriteria(new RequestCriteria($request));
        $this->ocZoneRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocZones = $this->ocZoneRepository->all();

        return $this->sendResponse($ocZones->toArray(), 'Oc Zones retrieved successfully');
    }

    /**
     * Store a newly created oc_zone in storage.
     * POST /ocZones
     *
     * @param Createoc_zoneAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_zoneAPIRequest $request)
    {
        $input = $request->all();

        $ocZones = $this->ocZoneRepository->create($input);

        return $this->sendResponse($ocZones->toArray(), 'Oc Zone saved successfully');
    }

    /**
     * Display the specified oc_zone.
     * GET|HEAD /ocZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_zone $ocZone */
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            return $this->sendError('Oc Zone not found');
        }

        return $this->sendResponse($ocZone->toArray(), 'Oc Zone retrieved successfully');
    }

    /**
     * Update the specified oc_zone in storage.
     * PUT/PATCH /ocZones/{id}
     *
     * @param  int $id
     * @param Updateoc_zoneAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_zoneAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_zone $ocZone */
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            return $this->sendError('Oc Zone not found');
        }

        $ocZone = $this->ocZoneRepository->update($input, $id);

        return $this->sendResponse($ocZone->toArray(), 'oc_zone updated successfully');
    }

    /**
     * Remove the specified oc_zone from storage.
     * DELETE /ocZones/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_zone $ocZone */
        $ocZone = $this->ocZoneRepository->findWithoutFail($id);

        if (empty($ocZone)) {
            return $this->sendError('Oc Zone not found');
        }

        $ocZone->delete();

        return $this->sendResponse($id, 'Oc Zone deleted successfully');
    }
}
