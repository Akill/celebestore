<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_banner_image_descriptionAPIRequest;
use App\Http\Requests\API\Updateoc_banner_image_descriptionAPIRequest;
use App\Models\oc_banner_image_description;
use App\Repositories\oc_banner_image_descriptionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_banner_image_descriptionController
 * @package App\Http\Controllers\API
 */

class oc_banner_image_descriptionAPIController extends AppBaseController
{
    /** @var  oc_banner_image_descriptionRepository */
    private $ocBannerImageDescriptionRepository;

    public function __construct(oc_banner_image_descriptionRepository $ocBannerImageDescriptionRepo)
    {
        $this->ocBannerImageDescriptionRepository = $ocBannerImageDescriptionRepo;
    }

    /**
     * Display a listing of the oc_banner_image_description.
     * GET|HEAD /ocBannerImageDescriptions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerImageDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $this->ocBannerImageDescriptionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocBannerImageDescriptions = $this->ocBannerImageDescriptionRepository->all();

        return $this->sendResponse($ocBannerImageDescriptions->toArray(), 'Oc Banner Image Descriptions retrieved successfully');
    }

    /**
     * Store a newly created oc_banner_image_description in storage.
     * POST /ocBannerImageDescriptions
     *
     * @param Createoc_banner_image_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_banner_image_descriptionAPIRequest $request)
    {
        $input = $request->all();

        $ocBannerImageDescriptions = $this->ocBannerImageDescriptionRepository->create($input);

        return $this->sendResponse($ocBannerImageDescriptions->toArray(), 'Oc Banner Image Description saved successfully');
    }

    /**
     * Display the specified oc_banner_image_description.
     * GET|HEAD /ocBannerImageDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_banner_image_description $ocBannerImageDescription */
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            return $this->sendError('Oc Banner Image Description not found');
        }

        return $this->sendResponse($ocBannerImageDescription->toArray(), 'Oc Banner Image Description retrieved successfully');
    }

    /**
     * Update the specified oc_banner_image_description in storage.
     * PUT/PATCH /ocBannerImageDescriptions/{id}
     *
     * @param  int $id
     * @param Updateoc_banner_image_descriptionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_banner_image_descriptionAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_banner_image_description $ocBannerImageDescription */
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            return $this->sendError('Oc Banner Image Description not found');
        }

        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->update($input, $id);

        return $this->sendResponse($ocBannerImageDescription->toArray(), 'oc_banner_image_description updated successfully');
    }

    /**
     * Remove the specified oc_banner_image_description from storage.
     * DELETE /ocBannerImageDescriptions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_banner_image_description $ocBannerImageDescription */
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            return $this->sendError('Oc Banner Image Description not found');
        }

        $ocBannerImageDescription->delete();

        return $this->sendResponse($id, 'Oc Banner Image Description deleted successfully');
    }
}
