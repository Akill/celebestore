<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\Createoc_customer_activityAPIRequest;
use App\Http\Requests\API\Updateoc_customer_activityAPIRequest;
use App\Models\oc_customer_activity;
use App\Repositories\oc_customer_activityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class oc_customer_activityController
 * @package App\Http\Controllers\API
 */

class oc_customer_activityAPIController extends AppBaseController
{
    /** @var  oc_customer_activityRepository */
    private $ocCustomerActivityRepository;

    public function __construct(oc_customer_activityRepository $ocCustomerActivityRepo)
    {
        $this->ocCustomerActivityRepository = $ocCustomerActivityRepo;
    }

    /**
     * Display a listing of the oc_customer_activity.
     * GET|HEAD /ocCustomerActivities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerActivityRepository->pushCriteria(new RequestCriteria($request));
        $this->ocCustomerActivityRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ocCustomerActivities = $this->ocCustomerActivityRepository->all();

        return $this->sendResponse($ocCustomerActivities->toArray(), 'Oc Customer Activities retrieved successfully');
    }

    /**
     * Store a newly created oc_customer_activity in storage.
     * POST /ocCustomerActivities
     *
     * @param Createoc_customer_activityAPIRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_activityAPIRequest $request)
    {
        $input = $request->all();

        $ocCustomerActivities = $this->ocCustomerActivityRepository->create($input);

        return $this->sendResponse($ocCustomerActivities->toArray(), 'Oc Customer Activity saved successfully');
    }

    /**
     * Display the specified oc_customer_activity.
     * GET|HEAD /ocCustomerActivities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var oc_customer_activity $ocCustomerActivity */
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            return $this->sendError('Oc Customer Activity not found');
        }

        return $this->sendResponse($ocCustomerActivity->toArray(), 'Oc Customer Activity retrieved successfully');
    }

    /**
     * Update the specified oc_customer_activity in storage.
     * PUT/PATCH /ocCustomerActivities/{id}
     *
     * @param  int $id
     * @param Updateoc_customer_activityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_activityAPIRequest $request)
    {
        $input = $request->all();

        /** @var oc_customer_activity $ocCustomerActivity */
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            return $this->sendError('Oc Customer Activity not found');
        }

        $ocCustomerActivity = $this->ocCustomerActivityRepository->update($input, $id);

        return $this->sendResponse($ocCustomerActivity->toArray(), 'oc_customer_activity updated successfully');
    }

    /**
     * Remove the specified oc_customer_activity from storage.
     * DELETE /ocCustomerActivities/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var oc_customer_activity $ocCustomerActivity */
        $ocCustomerActivity = $this->ocCustomerActivityRepository->findWithoutFail($id);

        if (empty($ocCustomerActivity)) {
            return $this->sendError('Oc Customer Activity not found');
        }

        $ocCustomerActivity->delete();

        return $this->sendResponse($id, 'Oc Customer Activity deleted successfully');
    }
}
