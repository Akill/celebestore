<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_banner_image_descriptionRequest;
use App\Http\Requests\Updateoc_banner_image_descriptionRequest;
use App\Repositories\oc_banner_image_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_banner_image_descriptionController extends AppBaseController
{
    /** @var  oc_banner_image_descriptionRepository */
    private $ocBannerImageDescriptionRepository;

    public function __construct(oc_banner_image_descriptionRepository $ocBannerImageDescriptionRepo)
    {
        $this->ocBannerImageDescriptionRepository = $ocBannerImageDescriptionRepo;
    }

    /**
     * Display a listing of the oc_banner_image_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocBannerImageDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocBannerImageDescriptions = $this->ocBannerImageDescriptionRepository->paginate(20);

        return view('oc_banner_image_descriptions.index')
            ->with('ocBannerImageDescriptions', $ocBannerImageDescriptions);
    }

    /**
     * Show the form for creating a new oc_banner_image_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_banner_image_descriptions.create');
    }

    /**
     * Store a newly created oc_banner_image_description in storage.
     *
     * @param Createoc_banner_image_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_banner_image_descriptionRequest $request)
    {
        $input = $request->all();

        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->create($input);

        Flash::success('Oc Banner Image Description saved successfully.');

        return redirect(route('ocBannerImageDescriptions.index'));
    }

    /**
     * Display the specified oc_banner_image_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            Flash::error('Oc Banner Image Description not found');

            return redirect(route('ocBannerImageDescriptions.index'));
        }

        return view('oc_banner_image_descriptions.show')->with('ocBannerImageDescription', $ocBannerImageDescription);
    }

    /**
     * Show the form for editing the specified oc_banner_image_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            Flash::error('Oc Banner Image Description not found');

            return redirect(route('ocBannerImageDescriptions.index'));
        }

        return view('oc_banner_image_descriptions.edit')->with('ocBannerImageDescription', $ocBannerImageDescription);
    }

    /**
     * Update the specified oc_banner_image_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_banner_image_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_banner_image_descriptionRequest $request)
    {
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            Flash::error('Oc Banner Image Description not found');

            return redirect(route('ocBannerImageDescriptions.index'));
        }

        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Banner Image Description updated successfully.');

        return redirect(route('ocBannerImageDescriptions.index'));
    }

    /**
     * Remove the specified oc_banner_image_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocBannerImageDescription = $this->ocBannerImageDescriptionRepository->findWithoutFail($id);

        if (empty($ocBannerImageDescription)) {
            Flash::error('Oc Banner Image Description not found');

            return redirect(route('ocBannerImageDescriptions.index'));
        }

        $this->ocBannerImageDescriptionRepository->delete($id);

        Flash::success('Oc Banner Image Description deleted successfully.');

        return redirect(route('ocBannerImageDescriptions.index'));
    }
}
