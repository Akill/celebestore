<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_userRequest;
use App\Http\Requests\Updateoc_userRequest;
use App\Repositories\oc_userRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_userController extends AppBaseController
{
    /** @var  oc_userRepository */
    private $ocUserRepository;

    public function __construct(oc_userRepository $ocUserRepo)
    {
        $this->ocUserRepository = $ocUserRepo;
    }

    /**
     * Display a listing of the oc_user.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocUserRepository->pushCriteria(new RequestCriteria($request));
        $ocUsers = $this->ocUserRepository->paginate(20);

        return view('oc_users.index')
            ->with('ocUsers', $ocUsers);
    }

    /**
     * Show the form for creating a new oc_user.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_users.create');
    }

    /**
     * Store a newly created oc_user in storage.
     *
     * @param Createoc_userRequest $request
     *
     * @return Response
     */
    public function store(Createoc_userRequest $request)
    {
        $input = $request->all();

        $ocUser = $this->ocUserRepository->create($input);

        Flash::success('Oc User saved successfully.');

        return redirect(route('ocUsers.index'));
    }

    /**
     * Display the specified oc_user.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            Flash::error('Oc User not found');

            return redirect(route('ocUsers.index'));
        }

        return view('oc_users.show')->with('ocUser', $ocUser);
    }

    /**
     * Show the form for editing the specified oc_user.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            Flash::error('Oc User not found');

            return redirect(route('ocUsers.index'));
        }

        return view('oc_users.edit')->with('ocUser', $ocUser);
    }

    /**
     * Update the specified oc_user in storage.
     *
     * @param  int              $id
     * @param Updateoc_userRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_userRequest $request)
    {
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            Flash::error('Oc User not found');

            return redirect(route('ocUsers.index'));
        }

        $ocUser = $this->ocUserRepository->update($request->all(), $id);

        Flash::success('Oc User updated successfully.');

        return redirect(route('ocUsers.index'));
    }

    /**
     * Remove the specified oc_user from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocUser = $this->ocUserRepository->findWithoutFail($id);

        if (empty($ocUser)) {
            Flash::error('Oc User not found');

            return redirect(route('ocUsers.index'));
        }

        $this->ocUserRepository->delete($id);

        Flash::success('Oc User deleted successfully.');

        return redirect(route('ocUsers.index'));
    }
}
