<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_downloadRequest;
use App\Http\Requests\Updateoc_downloadRequest;
use App\Repositories\oc_downloadRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_downloadController extends AppBaseController
{
    /** @var  oc_downloadRepository */
    private $ocDownloadRepository;

    public function __construct(oc_downloadRepository $ocDownloadRepo)
    {
        $this->ocDownloadRepository = $ocDownloadRepo;
    }

    /**
     * Display a listing of the oc_download.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocDownloadRepository->pushCriteria(new RequestCriteria($request));
        $ocDownloads = $this->ocDownloadRepository->paginate(20);

        return view('oc_downloads.index')
            ->with('ocDownloads', $ocDownloads);
    }

    /**
     * Show the form for creating a new oc_download.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_downloads.create');
    }

    /**
     * Store a newly created oc_download in storage.
     *
     * @param Createoc_downloadRequest $request
     *
     * @return Response
     */
    public function store(Createoc_downloadRequest $request)
    {
        $input = $request->all();

        $ocDownload = $this->ocDownloadRepository->create($input);

        Flash::success('Oc Download saved successfully.');

        return redirect(route('ocDownloads.index'));
    }

    /**
     * Display the specified oc_download.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            Flash::error('Oc Download not found');

            return redirect(route('ocDownloads.index'));
        }

        return view('oc_downloads.show')->with('ocDownload', $ocDownload);
    }

    /**
     * Show the form for editing the specified oc_download.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            Flash::error('Oc Download not found');

            return redirect(route('ocDownloads.index'));
        }

        return view('oc_downloads.edit')->with('ocDownload', $ocDownload);
    }

    /**
     * Update the specified oc_download in storage.
     *
     * @param  int              $id
     * @param Updateoc_downloadRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_downloadRequest $request)
    {
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            Flash::error('Oc Download not found');

            return redirect(route('ocDownloads.index'));
        }

        $ocDownload = $this->ocDownloadRepository->update($request->all(), $id);

        Flash::success('Oc Download updated successfully.');

        return redirect(route('ocDownloads.index'));
    }

    /**
     * Remove the specified oc_download from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocDownload = $this->ocDownloadRepository->findWithoutFail($id);

        if (empty($ocDownload)) {
            Flash::error('Oc Download not found');

            return redirect(route('ocDownloads.index'));
        }

        $this->ocDownloadRepository->delete($id);

        Flash::success('Oc Download deleted successfully.');

        return redirect(route('ocDownloads.index'));
    }
}
