<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_returnRequest;
use App\Http\Requests\Updateoc_returnRequest;
use App\Repositories\oc_returnRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_returnController extends AppBaseController
{
    /** @var  oc_returnRepository */
    private $ocReturnRepository;

    public function __construct(oc_returnRepository $ocReturnRepo)
    {
        $this->ocReturnRepository = $ocReturnRepo;
    }

    /**
     * Display a listing of the oc_return.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocReturnRepository->pushCriteria(new RequestCriteria($request));
        $ocReturns = $this->ocReturnRepository->paginate(20);

        return view('oc_returns.index')
            ->with('ocReturns', $ocReturns);
    }

    /**
     * Show the form for creating a new oc_return.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_returns.create');
    }

    /**
     * Store a newly created oc_return in storage.
     *
     * @param Createoc_returnRequest $request
     *
     * @return Response
     */
    public function store(Createoc_returnRequest $request)
    {
        $input = $request->all();

        $ocReturn = $this->ocReturnRepository->create($input);

        Flash::success('Oc Return saved successfully.');

        return redirect(route('ocReturns.index'));
    }

    /**
     * Display the specified oc_return.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            Flash::error('Oc Return not found');

            return redirect(route('ocReturns.index'));
        }

        return view('oc_returns.show')->with('ocReturn', $ocReturn);
    }

    /**
     * Show the form for editing the specified oc_return.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            Flash::error('Oc Return not found');

            return redirect(route('ocReturns.index'));
        }

        return view('oc_returns.edit')->with('ocReturn', $ocReturn);
    }

    /**
     * Update the specified oc_return in storage.
     *
     * @param  int              $id
     * @param Updateoc_returnRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_returnRequest $request)
    {
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            Flash::error('Oc Return not found');

            return redirect(route('ocReturns.index'));
        }

        $ocReturn = $this->ocReturnRepository->update($request->all(), $id);

        Flash::success('Oc Return updated successfully.');

        return redirect(route('ocReturns.index'));
    }

    /**
     * Remove the specified oc_return from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocReturn = $this->ocReturnRepository->findWithoutFail($id);

        if (empty($ocReturn)) {
            Flash::error('Oc Return not found');

            return redirect(route('ocReturns.index'));
        }

        $this->ocReturnRepository->delete($id);

        Flash::success('Oc Return deleted successfully.');

        return redirect(route('ocReturns.index'));
    }
}
