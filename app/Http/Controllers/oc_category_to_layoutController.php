<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_category_to_layoutRequest;
use App\Http\Requests\Updateoc_category_to_layoutRequest;
use App\Repositories\oc_category_to_layoutRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_category_to_layoutController extends AppBaseController
{
    /** @var  oc_category_to_layoutRepository */
    private $ocCategoryToLayoutRepository;

    public function __construct(oc_category_to_layoutRepository $ocCategoryToLayoutRepo)
    {
        $this->ocCategoryToLayoutRepository = $ocCategoryToLayoutRepo;
    }

    /**
     * Display a listing of the oc_category_to_layout.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryToLayoutRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryToLayouts = $this->ocCategoryToLayoutRepository->paginate(20);

        return view('oc_category_to_layouts.index')
            ->with('ocCategoryToLayouts', $ocCategoryToLayouts);
    }

    /**
     * Show the form for creating a new oc_category_to_layout.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_category_to_layouts.create');
    }

    /**
     * Store a newly created oc_category_to_layout in storage.
     *
     * @param Createoc_category_to_layoutRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_to_layoutRequest $request)
    {
        $input = $request->all();

        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->create($input);

        Flash::success('Oc Category To Layout saved successfully.');

        return redirect(route('ocCategoryToLayouts.index'));
    }

    /**
     * Display the specified oc_category_to_layout.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            Flash::error('Oc Category To Layout not found');

            return redirect(route('ocCategoryToLayouts.index'));
        }

        return view('oc_category_to_layouts.show')->with('ocCategoryToLayout', $ocCategoryToLayout);
    }

    /**
     * Show the form for editing the specified oc_category_to_layout.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            Flash::error('Oc Category To Layout not found');

            return redirect(route('ocCategoryToLayouts.index'));
        }

        return view('oc_category_to_layouts.edit')->with('ocCategoryToLayout', $ocCategoryToLayout);
    }

    /**
     * Update the specified oc_category_to_layout in storage.
     *
     * @param  int              $id
     * @param Updateoc_category_to_layoutRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_to_layoutRequest $request)
    {
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            Flash::error('Oc Category To Layout not found');

            return redirect(route('ocCategoryToLayouts.index'));
        }

        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->update($request->all(), $id);

        Flash::success('Oc Category To Layout updated successfully.');

        return redirect(route('ocCategoryToLayouts.index'));
    }

    /**
     * Remove the specified oc_category_to_layout from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategoryToLayout = $this->ocCategoryToLayoutRepository->findWithoutFail($id);

        if (empty($ocCategoryToLayout)) {
            Flash::error('Oc Category To Layout not found');

            return redirect(route('ocCategoryToLayouts.index'));
        }

        $this->ocCategoryToLayoutRepository->delete($id);

        Flash::success('Oc Category To Layout deleted successfully.');

        return redirect(route('ocCategoryToLayouts.index'));
    }
}
