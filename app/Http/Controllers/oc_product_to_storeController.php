<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_to_storeRequest;
use App\Http\Requests\Updateoc_product_to_storeRequest;
use App\Repositories\oc_product_to_storeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_to_storeController extends AppBaseController
{
    /** @var  oc_product_to_storeRepository */
    private $ocProductToStoreRepository;

    public function __construct(oc_product_to_storeRepository $ocProductToStoreRepo)
    {
        $this->ocProductToStoreRepository = $ocProductToStoreRepo;
    }

    /**
     * Display a listing of the oc_product_to_store.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToStoreRepository->pushCriteria(new RequestCriteria($request));
        $ocProductToStores = $this->ocProductToStoreRepository->all();

        return view('oc_product_to_stores.index')
            ->with('ocProductToStores', $ocProductToStores);
    }

    /**
     * Show the form for creating a new oc_product_to_store.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_to_stores.create');
    }

    /**
     * Store a newly created oc_product_to_store in storage.
     *
     * @param Createoc_product_to_storeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_storeRequest $request)
    {
        $input = $request->all();

        $ocProductToStore = $this->ocProductToStoreRepository->create($input);

        Flash::success('Oc Product To Store saved successfully.');

        return redirect(route('ocProductToStores.index'));
    }

    /**
     * Display the specified oc_product_to_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            Flash::error('Oc Product To Store not found');

            return redirect(route('ocProductToStores.index'));
        }

        return view('oc_product_to_stores.show')->with('ocProductToStore', $ocProductToStore);
    }

    /**
     * Show the form for editing the specified oc_product_to_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            Flash::error('Oc Product To Store not found');

            return redirect(route('ocProductToStores.index'));
        }

        return view('oc_product_to_stores.edit')->with('ocProductToStore', $ocProductToStore);
    }

    /**
     * Update the specified oc_product_to_store in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_to_storeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_storeRequest $request)
    {
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            Flash::error('Oc Product To Store not found');

            return redirect(route('ocProductToStores.index'));
        }

        $ocProductToStore = $this->ocProductToStoreRepository->update($request->all(), $id);

        Flash::success('Oc Product To Store updated successfully.');

        return redirect(route('ocProductToStores.index'));
    }

    /**
     * Remove the specified oc_product_to_store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductToStore = $this->ocProductToStoreRepository->findWithoutFail($id);

        if (empty($ocProductToStore)) {
            Flash::error('Oc Product To Store not found');

            return redirect(route('ocProductToStores.index'));
        }

        $this->ocProductToStoreRepository->delete($id);

        Flash::success('Oc Product To Store deleted successfully.');

        return redirect(route('ocProductToStores.index'));
    }
}
