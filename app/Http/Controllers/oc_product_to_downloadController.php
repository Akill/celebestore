<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_to_downloadRequest;
use App\Http\Requests\Updateoc_product_to_downloadRequest;
use App\Repositories\oc_product_to_downloadRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_to_downloadController extends AppBaseController
{
    /** @var  oc_product_to_downloadRepository */
    private $ocProductToDownloadRepository;

    public function __construct(oc_product_to_downloadRepository $ocProductToDownloadRepo)
    {
        $this->ocProductToDownloadRepository = $ocProductToDownloadRepo;
    }

    /**
     * Display a listing of the oc_product_to_download.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductToDownloadRepository->pushCriteria(new RequestCriteria($request));
        $ocProductToDownloads = $this->ocProductToDownloadRepository->all();

        return view('oc_product_to_downloads.index')
            ->with('ocProductToDownloads', $ocProductToDownloads);
    }

    /**
     * Show the form for creating a new oc_product_to_download.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_to_downloads.create');
    }

    /**
     * Store a newly created oc_product_to_download in storage.
     *
     * @param Createoc_product_to_downloadRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_to_downloadRequest $request)
    {
        $input = $request->all();

        $ocProductToDownload = $this->ocProductToDownloadRepository->create($input);

        Flash::success('Oc Product To Download saved successfully.');

        return redirect(route('ocProductToDownloads.index'));
    }

    /**
     * Display the specified oc_product_to_download.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            Flash::error('Oc Product To Download not found');

            return redirect(route('ocProductToDownloads.index'));
        }

        return view('oc_product_to_downloads.show')->with('ocProductToDownload', $ocProductToDownload);
    }

    /**
     * Show the form for editing the specified oc_product_to_download.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            Flash::error('Oc Product To Download not found');

            return redirect(route('ocProductToDownloads.index'));
        }

        return view('oc_product_to_downloads.edit')->with('ocProductToDownload', $ocProductToDownload);
    }

    /**
     * Update the specified oc_product_to_download in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_to_downloadRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_to_downloadRequest $request)
    {
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            Flash::error('Oc Product To Download not found');

            return redirect(route('ocProductToDownloads.index'));
        }

        $ocProductToDownload = $this->ocProductToDownloadRepository->update($request->all(), $id);

        Flash::success('Oc Product To Download updated successfully.');

        return redirect(route('ocProductToDownloads.index'));
    }

    /**
     * Remove the specified oc_product_to_download from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductToDownload = $this->ocProductToDownloadRepository->findWithoutFail($id);

        if (empty($ocProductToDownload)) {
            Flash::error('Oc Product To Download not found');

            return redirect(route('ocProductToDownloads.index'));
        }

        $this->ocProductToDownloadRepository->delete($id);

        Flash::success('Oc Product To Download deleted successfully.');

        return redirect(route('ocProductToDownloads.index'));
    }
}
