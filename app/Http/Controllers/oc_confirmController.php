<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_confirmRequest;
use App\Http\Requests\Updateoc_confirmRequest;
use App\Repositories\oc_confirmRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_confirmController extends AppBaseController
{
    /** @var  oc_confirmRepository */
    private $ocConfirmRepository;

    public function __construct(oc_confirmRepository $ocConfirmRepo)
    {
        $this->ocConfirmRepository = $ocConfirmRepo;
    }

    /**
     * Display a listing of the oc_confirm.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocConfirmRepository->pushCriteria(new RequestCriteria($request));
        $ocConfirms = $this->ocConfirmRepository->paginate(20);

        return view('oc_confirms.index')
            ->with('ocConfirms', $ocConfirms);
    }

    /**
     * Show the form for creating a new oc_confirm.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_confirms.create');
    }

    /**
     * Store a newly created oc_confirm in storage.
     *
     * @param Createoc_confirmRequest $request
     *
     * @return Response
     */
    public function store(Createoc_confirmRequest $request)
    {
        $input = $request->all();

        $ocConfirm = $this->ocConfirmRepository->create($input);

        Flash::success('Oc Confirm saved successfully.');

        return redirect(route('ocConfirms.index'));
    }

    /**
     * Display the specified oc_confirm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            Flash::error('Oc Confirm not found');

            return redirect(route('ocConfirms.index'));
        }

        return view('oc_confirms.show')->with('ocConfirm', $ocConfirm);
    }

    /**
     * Show the form for editing the specified oc_confirm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            Flash::error('Oc Confirm not found');

            return redirect(route('ocConfirms.index'));
        }

        return view('oc_confirms.edit')->with('ocConfirm', $ocConfirm);
    }

    /**
     * Update the specified oc_confirm in storage.
     *
     * @param  int              $id
     * @param Updateoc_confirmRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_confirmRequest $request)
    {
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            Flash::error('Oc Confirm not found');

            return redirect(route('ocConfirms.index'));
        }

        $ocConfirm = $this->ocConfirmRepository->update($request->all(), $id);

        Flash::success('Oc Confirm updated successfully.');

        return redirect(route('ocConfirms.index'));
    }

    /**
     * Remove the specified oc_confirm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocConfirm = $this->ocConfirmRepository->findWithoutFail($id);

        if (empty($ocConfirm)) {
            Flash::error('Oc Confirm not found');

            return redirect(route('ocConfirms.index'));
        }

        $this->ocConfirmRepository->delete($id);

        Flash::success('Oc Confirm deleted successfully.');

        return redirect(route('ocConfirms.index'));
    }
}
