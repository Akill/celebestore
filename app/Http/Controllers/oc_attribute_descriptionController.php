<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_attribute_descriptionRequest;
use App\Http\Requests\Updateoc_attribute_descriptionRequest;
use App\Repositories\oc_attribute_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_attribute_descriptionController extends AppBaseController
{
    /** @var  oc_attribute_descriptionRepository */
    private $ocAttributeDescriptionRepository;

    public function __construct(oc_attribute_descriptionRepository $ocAttributeDescriptionRepo)
    {
        $this->ocAttributeDescriptionRepository = $ocAttributeDescriptionRepo;
    }

    /**
     * Display a listing of the oc_attribute_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAttributeDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocAttributeDescriptions = $this->ocAttributeDescriptionRepository->paginate(20);

        return view('oc_attribute_descriptions.index')
            ->with('ocAttributeDescriptions', $ocAttributeDescriptions);
    }

    /**
     * Show the form for creating a new oc_attribute_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_attribute_descriptions.create');
    }

    /**
     * Store a newly created oc_attribute_description in storage.
     *
     * @param Createoc_attribute_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_attribute_descriptionRequest $request)
    {
        $input = $request->all();

        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->create($input);

        Flash::success('Oc Attribute Description saved successfully.');

        return redirect(route('ocAttributeDescriptions.index'));
    }

    /**
     * Display the specified oc_attribute_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            Flash::error('Oc Attribute Description not found');

            return redirect(route('ocAttributeDescriptions.index'));
        }

        return view('oc_attribute_descriptions.show')->with('ocAttributeDescription', $ocAttributeDescription);
    }

    /**
     * Show the form for editing the specified oc_attribute_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            Flash::error('Oc Attribute Description not found');

            return redirect(route('ocAttributeDescriptions.index'));
        }

        return view('oc_attribute_descriptions.edit')->with('ocAttributeDescription', $ocAttributeDescription);
    }

    /**
     * Update the specified oc_attribute_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_attribute_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_attribute_descriptionRequest $request)
    {
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            Flash::error('Oc Attribute Description not found');

            return redirect(route('ocAttributeDescriptions.index'));
        }

        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Attribute Description updated successfully.');

        return redirect(route('ocAttributeDescriptions.index'));
    }

    /**
     * Remove the specified oc_attribute_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAttributeDescription = $this->ocAttributeDescriptionRepository->findWithoutFail($id);

        if (empty($ocAttributeDescription)) {
            Flash::error('Oc Attribute Description not found');

            return redirect(route('ocAttributeDescriptions.index'));
        }

        $this->ocAttributeDescriptionRepository->delete($id);

        Flash::success('Oc Attribute Description deleted successfully.');

        return redirect(route('ocAttributeDescriptions.index'));
    }
}
