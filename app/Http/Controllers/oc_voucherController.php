<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_voucherRequest;
use App\Http\Requests\Updateoc_voucherRequest;
use App\Repositories\oc_voucherRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_voucherController extends AppBaseController
{
    /** @var  oc_voucherRepository */
    private $ocVoucherRepository;

    public function __construct(oc_voucherRepository $ocVoucherRepo)
    {
        $this->ocVoucherRepository = $ocVoucherRepo;
    }

    /**
     * Display a listing of the oc_voucher.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocVoucherRepository->pushCriteria(new RequestCriteria($request));
        $ocVouchers = $this->ocVoucherRepository->paginate(20);

        return view('oc_vouchers.index')
            ->with('ocVouchers', $ocVouchers);
    }

    /**
     * Show the form for creating a new oc_voucher.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_vouchers.create');
    }

    /**
     * Store a newly created oc_voucher in storage.
     *
     * @param Createoc_voucherRequest $request
     *
     * @return Response
     */
    public function store(Createoc_voucherRequest $request)
    {
        $input = $request->all();

        $ocVoucher = $this->ocVoucherRepository->create($input);

        Flash::success('Oc Voucher saved successfully.');

        return redirect(route('ocVouchers.index'));
    }

    /**
     * Display the specified oc_voucher.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            Flash::error('Oc Voucher not found');

            return redirect(route('ocVouchers.index'));
        }

        return view('oc_vouchers.show')->with('ocVoucher', $ocVoucher);
    }

    /**
     * Show the form for editing the specified oc_voucher.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            Flash::error('Oc Voucher not found');

            return redirect(route('ocVouchers.index'));
        }

        return view('oc_vouchers.edit')->with('ocVoucher', $ocVoucher);
    }

    /**
     * Update the specified oc_voucher in storage.
     *
     * @param  int              $id
     * @param Updateoc_voucherRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_voucherRequest $request)
    {
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            Flash::error('Oc Voucher not found');

            return redirect(route('ocVouchers.index'));
        }

        $ocVoucher = $this->ocVoucherRepository->update($request->all(), $id);

        Flash::success('Oc Voucher updated successfully.');

        return redirect(route('ocVouchers.index'));
    }

    /**
     * Remove the specified oc_voucher from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocVoucher = $this->ocVoucherRepository->findWithoutFail($id);

        if (empty($ocVoucher)) {
            Flash::error('Oc Voucher not found');

            return redirect(route('ocVouchers.index'));
        }

        $this->ocVoucherRepository->delete($id);

        Flash::success('Oc Voucher deleted successfully.');

        return redirect(route('ocVouchers.index'));
    }
}
