<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customerRequest;
use App\Http\Requests\Updateoc_customerRequest;
use App\Repositories\oc_customerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customerController extends AppBaseController
{
    /** @var  oc_customerRepository */
    private $ocCustomerRepository;

    public function __construct(oc_customerRepository $ocCustomerRepo)
    {
        $this->ocCustomerRepository = $ocCustomerRepo;
    }

    /**
     * Display a listing of the oc_customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomers = $this->ocCustomerRepository->paginate(20);

        return view('oc_customers.index')
            ->with('ocCustomers', $ocCustomers);
    }

    /**
     * Show the form for creating a new oc_customer.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customers.create');
    }

    /**
     * Store a newly created oc_customer in storage.
     *
     * @param Createoc_customerRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customerRequest $request)
    {
        $input = $request->all();

        $ocCustomer = $this->ocCustomerRepository->create($input);

        Flash::success('Oc Customer saved successfully.');

        return redirect(route('ocCustomers.index'));
    }

    /**
     * Display the specified oc_customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            Flash::error('Oc Customer not found');

            return redirect(route('ocCustomers.index'));
        }

        return view('oc_customers.show')->with('ocCustomer', $ocCustomer);
    }

    /**
     * Show the form for editing the specified oc_customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            Flash::error('Oc Customer not found');

            return redirect(route('ocCustomers.index'));
        }

        return view('oc_customers.edit')->with('ocCustomer', $ocCustomer);
    }

    /**
     * Update the specified oc_customer in storage.
     *
     * @param  int              $id
     * @param Updateoc_customerRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customerRequest $request)
    {
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            Flash::error('Oc Customer not found');

            return redirect(route('ocCustomers.index'));
        }

        $ocCustomer = $this->ocCustomerRepository->update($request->all(), $id);

        Flash::success('Oc Customer updated successfully.');

        return redirect(route('ocCustomers.index'));
    }

    /**
     * Remove the specified oc_customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomer = $this->ocCustomerRepository->findWithoutFail($id);

        if (empty($ocCustomer)) {
            Flash::error('Oc Customer not found');

            return redirect(route('ocCustomers.index'));
        }

        $this->ocCustomerRepository->delete($id);

        Flash::success('Oc Customer deleted successfully.');

        return redirect(route('ocCustomers.index'));
    }
}
