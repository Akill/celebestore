<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_couponRequest;
use App\Http\Requests\Updateoc_couponRequest;
use App\Repositories\oc_couponRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_couponController extends AppBaseController
{
    /** @var  oc_couponRepository */
    private $ocCouponRepository;

    public function __construct(oc_couponRepository $ocCouponRepo)
    {
        $this->ocCouponRepository = $ocCouponRepo;
    }

    /**
     * Display a listing of the oc_coupon.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponRepository->pushCriteria(new RequestCriteria($request));
        $ocCoupons = $this->ocCouponRepository->paginate(20);

        return view('oc_coupons.index')
            ->with('ocCoupons', $ocCoupons);
    }

    /**
     * Show the form for creating a new oc_coupon.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_coupons.create');
    }

    /**
     * Store a newly created oc_coupon in storage.
     *
     * @param Createoc_couponRequest $request
     *
     * @return Response
     */
    public function store(Createoc_couponRequest $request)
    {
        $input = $request->all();

        $ocCoupon = $this->ocCouponRepository->create($input);

        Flash::success('Oc Coupon saved successfully.');

        return redirect(route('ocCoupons.index'));
    }

    /**
     * Display the specified oc_coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            Flash::error('Oc Coupon not found');

            return redirect(route('ocCoupons.index'));
        }

        return view('oc_coupons.show')->with('ocCoupon', $ocCoupon);
    }

    /**
     * Show the form for editing the specified oc_coupon.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            Flash::error('Oc Coupon not found');

            return redirect(route('ocCoupons.index'));
        }

        return view('oc_coupons.edit')->with('ocCoupon', $ocCoupon);
    }

    /**
     * Update the specified oc_coupon in storage.
     *
     * @param  int              $id
     * @param Updateoc_couponRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_couponRequest $request)
    {
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            Flash::error('Oc Coupon not found');

            return redirect(route('ocCoupons.index'));
        }

        $ocCoupon = $this->ocCouponRepository->update($request->all(), $id);

        Flash::success('Oc Coupon updated successfully.');

        return redirect(route('ocCoupons.index'));
    }

    /**
     * Remove the specified oc_coupon from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCoupon = $this->ocCouponRepository->findWithoutFail($id);

        if (empty($ocCoupon)) {
            Flash::error('Oc Coupon not found');

            return redirect(route('ocCoupons.index'));
        }

        $this->ocCouponRepository->delete($id);

        Flash::success('Oc Coupon deleted successfully.');

        return redirect(route('ocCoupons.index'));
    }
}
