<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_filterRequest;
use App\Http\Requests\Updateoc_filterRequest;
use App\Repositories\oc_filterRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_filterController extends AppBaseController
{
    /** @var  oc_filterRepository */
    private $ocFilterRepository;

    public function __construct(oc_filterRepository $ocFilterRepo)
    {
        $this->ocFilterRepository = $ocFilterRepo;
    }

    /**
     * Display a listing of the oc_filter.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocFilterRepository->pushCriteria(new RequestCriteria($request));
        $ocFilters = $this->ocFilterRepository->paginate(20);

        return view('oc_filters.index')
            ->with('ocFilters', $ocFilters);
    }

    /**
     * Show the form for creating a new oc_filter.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_filters.create');
    }

    /**
     * Store a newly created oc_filter in storage.
     *
     * @param Createoc_filterRequest $request
     *
     * @return Response
     */
    public function store(Createoc_filterRequest $request)
    {
        $input = $request->all();

        $ocFilter = $this->ocFilterRepository->create($input);

        Flash::success('Oc Filter saved successfully.');

        return redirect(route('ocFilters.index'));
    }

    /**
     * Display the specified oc_filter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            Flash::error('Oc Filter not found');

            return redirect(route('ocFilters.index'));
        }

        return view('oc_filters.show')->with('ocFilter', $ocFilter);
    }

    /**
     * Show the form for editing the specified oc_filter.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            Flash::error('Oc Filter not found');

            return redirect(route('ocFilters.index'));
        }

        return view('oc_filters.edit')->with('ocFilter', $ocFilter);
    }

    /**
     * Update the specified oc_filter in storage.
     *
     * @param  int              $id
     * @param Updateoc_filterRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_filterRequest $request)
    {
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            Flash::error('Oc Filter not found');

            return redirect(route('ocFilters.index'));
        }

        $ocFilter = $this->ocFilterRepository->update($request->all(), $id);

        Flash::success('Oc Filter updated successfully.');

        return redirect(route('ocFilters.index'));
    }

    /**
     * Remove the specified oc_filter from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocFilter = $this->ocFilterRepository->findWithoutFail($id);

        if (empty($ocFilter)) {
            Flash::error('Oc Filter not found');

            return redirect(route('ocFilters.index'));
        }

        $this->ocFilterRepository->delete($id);

        Flash::success('Oc Filter deleted successfully.');

        return redirect(route('ocFilters.index'));
    }
}
