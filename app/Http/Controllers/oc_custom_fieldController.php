<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_custom_fieldRequest;
use App\Http\Requests\Updateoc_custom_fieldRequest;
use App\Repositories\oc_custom_fieldRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_custom_fieldController extends AppBaseController
{
    /** @var  oc_custom_fieldRepository */
    private $ocCustomFieldRepository;

    public function __construct(oc_custom_fieldRepository $ocCustomFieldRepo)
    {
        $this->ocCustomFieldRepository = $ocCustomFieldRepo;
    }

    /**
     * Display a listing of the oc_custom_field.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomFields = $this->ocCustomFieldRepository->paginate(20);

        return view('oc_custom_fields.index')
            ->with('ocCustomFields', $ocCustomFields);
    }

    /**
     * Show the form for creating a new oc_custom_field.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_custom_fields.create');
    }

    /**
     * Store a newly created oc_custom_field in storage.
     *
     * @param Createoc_custom_fieldRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_fieldRequest $request)
    {
        $input = $request->all();

        $ocCustomField = $this->ocCustomFieldRepository->create($input);

        Flash::success('Oc Custom Field saved successfully.');

        return redirect(route('ocCustomFields.index'));
    }

    /**
     * Display the specified oc_custom_field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            Flash::error('Oc Custom Field not found');

            return redirect(route('ocCustomFields.index'));
        }

        return view('oc_custom_fields.show')->with('ocCustomField', $ocCustomField);
    }

    /**
     * Show the form for editing the specified oc_custom_field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            Flash::error('Oc Custom Field not found');

            return redirect(route('ocCustomFields.index'));
        }

        return view('oc_custom_fields.edit')->with('ocCustomField', $ocCustomField);
    }

    /**
     * Update the specified oc_custom_field in storage.
     *
     * @param  int              $id
     * @param Updateoc_custom_fieldRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_fieldRequest $request)
    {
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            Flash::error('Oc Custom Field not found');

            return redirect(route('ocCustomFields.index'));
        }

        $ocCustomField = $this->ocCustomFieldRepository->update($request->all(), $id);

        Flash::success('Oc Custom Field updated successfully.');

        return redirect(route('ocCustomFields.index'));
    }

    /**
     * Remove the specified oc_custom_field from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomField = $this->ocCustomFieldRepository->findWithoutFail($id);

        if (empty($ocCustomField)) {
            Flash::error('Oc Custom Field not found');

            return redirect(route('ocCustomFields.index'));
        }

        $this->ocCustomFieldRepository->delete($id);

        Flash::success('Oc Custom Field deleted successfully.');

        return redirect(route('ocCustomFields.index'));
    }
}
