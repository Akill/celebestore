<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_storeRequest;
use App\Http\Requests\Updateoc_storeRequest;
use App\Repositories\oc_storeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_storeController extends AppBaseController
{
    /** @var  oc_storeRepository */
    private $ocStoreRepository;

    public function __construct(oc_storeRepository $ocStoreRepo)
    {
        $this->ocStoreRepository = $ocStoreRepo;
    }

    /**
     * Display a listing of the oc_store.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocStoreRepository->pushCriteria(new RequestCriteria($request));
        $ocStores = $this->ocStoreRepository->paginate(20);

        return view('oc_stores.index')
            ->with('ocStores', $ocStores);
    }

    /**
     * Show the form for creating a new oc_store.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_stores.create');
    }

    /**
     * Store a newly created oc_store in storage.
     *
     * @param Createoc_storeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_storeRequest $request)
    {
        $input = $request->all();

        $ocStore = $this->ocStoreRepository->create($input);

        Flash::success('Oc Store saved successfully.');

        return redirect(route('ocStores.index'));
    }

    /**
     * Display the specified oc_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            Flash::error('Oc Store not found');

            return redirect(route('ocStores.index'));
        }

        return view('oc_stores.show')->with('ocStore', $ocStore);
    }

    /**
     * Show the form for editing the specified oc_store.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            Flash::error('Oc Store not found');

            return redirect(route('ocStores.index'));
        }

        return view('oc_stores.edit')->with('ocStore', $ocStore);
    }

    /**
     * Update the specified oc_store in storage.
     *
     * @param  int              $id
     * @param Updateoc_storeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_storeRequest $request)
    {
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            Flash::error('Oc Store not found');

            return redirect(route('ocStores.index'));
        }

        $ocStore = $this->ocStoreRepository->update($request->all(), $id);

        Flash::success('Oc Store updated successfully.');

        return redirect(route('ocStores.index'));
    }

    /**
     * Remove the specified oc_store from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocStore = $this->ocStoreRepository->findWithoutFail($id);

        if (empty($ocStore)) {
            Flash::error('Oc Store not found');

            return redirect(route('ocStores.index'));
        }

        $this->ocStoreRepository->delete($id);

        Flash::success('Oc Store deleted successfully.');

        return redirect(route('ocStores.index'));
    }
}
