<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_rewardRequest;
use App\Http\Requests\Updateoc_product_rewardRequest;
use App\Repositories\oc_product_rewardRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_rewardController extends AppBaseController
{
    /** @var  oc_product_rewardRepository */
    private $ocProductRewardRepository;

    public function __construct(oc_product_rewardRepository $ocProductRewardRepo)
    {
        $this->ocProductRewardRepository = $ocProductRewardRepo;
    }

    /**
     * Display a listing of the oc_product_reward.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductRewardRepository->pushCriteria(new RequestCriteria($request));
        $ocProductRewards = $this->ocProductRewardRepository->paginate(20);

        return view('oc_product_rewards.index')
            ->with('ocProductRewards', $ocProductRewards);
    }

    /**
     * Show the form for creating a new oc_product_reward.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_rewards.create');
    }

    /**
     * Store a newly created oc_product_reward in storage.
     *
     * @param Createoc_product_rewardRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_rewardRequest $request)
    {
        $input = $request->all();

        $ocProductReward = $this->ocProductRewardRepository->create($input);

        Flash::success('Oc Product Reward saved successfully.');

        return redirect(route('ocProductRewards.index'));
    }

    /**
     * Display the specified oc_product_reward.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            Flash::error('Oc Product Reward not found');

            return redirect(route('ocProductRewards.index'));
        }

        return view('oc_product_rewards.show')->with('ocProductReward', $ocProductReward);
    }

    /**
     * Show the form for editing the specified oc_product_reward.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            Flash::error('Oc Product Reward not found');

            return redirect(route('ocProductRewards.index'));
        }

        return view('oc_product_rewards.edit')->with('ocProductReward', $ocProductReward);
    }

    /**
     * Update the specified oc_product_reward in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_rewardRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_rewardRequest $request)
    {
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            Flash::error('Oc Product Reward not found');

            return redirect(route('ocProductRewards.index'));
        }

        $ocProductReward = $this->ocProductRewardRepository->update($request->all(), $id);

        Flash::success('Oc Product Reward updated successfully.');

        return redirect(route('ocProductRewards.index'));
    }

    /**
     * Remove the specified oc_product_reward from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductReward = $this->ocProductRewardRepository->findWithoutFail($id);

        if (empty($ocProductReward)) {
            Flash::error('Oc Product Reward not found');

            return redirect(route('ocProductRewards.index'));
        }

        $this->ocProductRewardRepository->delete($id);

        Flash::success('Oc Product Reward deleted successfully.');

        return redirect(route('ocProductRewards.index'));
    }
}
