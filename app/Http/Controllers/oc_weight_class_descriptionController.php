<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_weight_class_descriptionRequest;
use App\Http\Requests\Updateoc_weight_class_descriptionRequest;
use App\Repositories\oc_weight_class_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_weight_class_descriptionController extends AppBaseController
{
    /** @var  oc_weight_class_descriptionRepository */
    private $ocWeightClassDescriptionRepository;

    public function __construct(oc_weight_class_descriptionRepository $ocWeightClassDescriptionRepo)
    {
        $this->ocWeightClassDescriptionRepository = $ocWeightClassDescriptionRepo;
    }

    /**
     * Display a listing of the oc_weight_class_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocWeightClassDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocWeightClassDescriptions = $this->ocWeightClassDescriptionRepository->paginate(20);

        return view('oc_weight_class_descriptions.index')
            ->with('ocWeightClassDescriptions', $ocWeightClassDescriptions);
    }

    /**
     * Show the form for creating a new oc_weight_class_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_weight_class_descriptions.create');
    }

    /**
     * Store a newly created oc_weight_class_description in storage.
     *
     * @param Createoc_weight_class_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_weight_class_descriptionRequest $request)
    {
        $input = $request->all();

        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->create($input);

        Flash::success('Oc Weight Class Description saved successfully.');

        return redirect(route('ocWeightClassDescriptions.index'));
    }

    /**
     * Display the specified oc_weight_class_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            Flash::error('Oc Weight Class Description not found');

            return redirect(route('ocWeightClassDescriptions.index'));
        }

        return view('oc_weight_class_descriptions.show')->with('ocWeightClassDescription', $ocWeightClassDescription);
    }

    /**
     * Show the form for editing the specified oc_weight_class_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            Flash::error('Oc Weight Class Description not found');

            return redirect(route('ocWeightClassDescriptions.index'));
        }

        return view('oc_weight_class_descriptions.edit')->with('ocWeightClassDescription', $ocWeightClassDescription);
    }

    /**
     * Update the specified oc_weight_class_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_weight_class_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_weight_class_descriptionRequest $request)
    {
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            Flash::error('Oc Weight Class Description not found');

            return redirect(route('ocWeightClassDescriptions.index'));
        }

        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Weight Class Description updated successfully.');

        return redirect(route('ocWeightClassDescriptions.index'));
    }

    /**
     * Remove the specified oc_weight_class_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocWeightClassDescription = $this->ocWeightClassDescriptionRepository->findWithoutFail($id);

        if (empty($ocWeightClassDescription)) {
            Flash::error('Oc Weight Class Description not found');

            return redirect(route('ocWeightClassDescriptions.index'));
        }

        $this->ocWeightClassDescriptionRepository->delete($id);

        Flash::success('Oc Weight Class Description deleted successfully.');

        return redirect(route('ocWeightClassDescriptions.index'));
    }
}
