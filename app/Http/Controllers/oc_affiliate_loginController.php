<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_affiliate_loginRequest;
use App\Http\Requests\Updateoc_affiliate_loginRequest;
use App\Repositories\oc_affiliate_loginRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_affiliate_loginController extends AppBaseController
{
    /** @var  oc_affiliate_loginRepository */
    private $ocAffiliateLoginRepository;

    public function __construct(oc_affiliate_loginRepository $ocAffiliateLoginRepo)
    {
        $this->ocAffiliateLoginRepository = $ocAffiliateLoginRepo;
    }

    /**
     * Display a listing of the oc_affiliate_login.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocAffiliateLoginRepository->pushCriteria(new RequestCriteria($request));
        $ocAffiliateLogins = $this->ocAffiliateLoginRepository->paginate(20);

        return view('oc_affiliate_logins.index')
            ->with('ocAffiliateLogins', $ocAffiliateLogins);
    }

    /**
     * Show the form for creating a new oc_affiliate_login.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_affiliate_logins.create');
    }

    /**
     * Store a newly created oc_affiliate_login in storage.
     *
     * @param Createoc_affiliate_loginRequest $request
     *
     * @return Response
     */
    public function store(Createoc_affiliate_loginRequest $request)
    {
        $input = $request->all();

        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->create($input);

        Flash::success('Oc Affiliate Login saved successfully.');

        return redirect(route('ocAffiliateLogins.index'));
    }

    /**
     * Display the specified oc_affiliate_login.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            Flash::error('Oc Affiliate Login not found');

            return redirect(route('ocAffiliateLogins.index'));
        }

        return view('oc_affiliate_logins.show')->with('ocAffiliateLogin', $ocAffiliateLogin);
    }

    /**
     * Show the form for editing the specified oc_affiliate_login.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            Flash::error('Oc Affiliate Login not found');

            return redirect(route('ocAffiliateLogins.index'));
        }

        return view('oc_affiliate_logins.edit')->with('ocAffiliateLogin', $ocAffiliateLogin);
    }

    /**
     * Update the specified oc_affiliate_login in storage.
     *
     * @param  int              $id
     * @param Updateoc_affiliate_loginRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_affiliate_loginRequest $request)
    {
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            Flash::error('Oc Affiliate Login not found');

            return redirect(route('ocAffiliateLogins.index'));
        }

        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->update($request->all(), $id);

        Flash::success('Oc Affiliate Login updated successfully.');

        return redirect(route('ocAffiliateLogins.index'));
    }

    /**
     * Remove the specified oc_affiliate_login from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocAffiliateLogin = $this->ocAffiliateLoginRepository->findWithoutFail($id);

        if (empty($ocAffiliateLogin)) {
            Flash::error('Oc Affiliate Login not found');

            return redirect(route('ocAffiliateLogins.index'));
        }

        $this->ocAffiliateLoginRepository->delete($id);

        Flash::success('Oc Affiliate Login deleted successfully.');

        return redirect(route('ocAffiliateLogins.index'));
    }
}
