<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_group_descriptionRequest;
use App\Http\Requests\Updateoc_customer_group_descriptionRequest;
use App\Repositories\oc_customer_group_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_group_descriptionController extends AppBaseController
{
    /** @var  oc_customer_group_descriptionRepository */
    private $ocCustomerGroupDescriptionRepository;

    public function __construct(oc_customer_group_descriptionRepository $ocCustomerGroupDescriptionRepo)
    {
        $this->ocCustomerGroupDescriptionRepository = $ocCustomerGroupDescriptionRepo;
    }

    /**
     * Display a listing of the oc_customer_group_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerGroupDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerGroupDescriptions = $this->ocCustomerGroupDescriptionRepository->all();

        return view('oc_customer_group_descriptions.index')
            ->with('ocCustomerGroupDescriptions', $ocCustomerGroupDescriptions);
    }

    /**
     * Show the form for creating a new oc_customer_group_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_group_descriptions.create');
    }

    /**
     * Store a newly created oc_customer_group_description in storage.
     *
     * @param Createoc_customer_group_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_group_descriptionRequest $request)
    {
        $input = $request->all();

        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->create($input);

        Flash::success('Oc Customer Group Description saved successfully.');

        return redirect(route('ocCustomerGroupDescriptions.index'));
    }

    /**
     * Display the specified oc_customer_group_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            Flash::error('Oc Customer Group Description not found');

            return redirect(route('ocCustomerGroupDescriptions.index'));
        }

        return view('oc_customer_group_descriptions.show')->with('ocCustomerGroupDescription', $ocCustomerGroupDescription);
    }

    /**
     * Show the form for editing the specified oc_customer_group_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            Flash::error('Oc Customer Group Description not found');

            return redirect(route('ocCustomerGroupDescriptions.index'));
        }

        return view('oc_customer_group_descriptions.edit')->with('ocCustomerGroupDescription', $ocCustomerGroupDescription);
    }

    /**
     * Update the specified oc_customer_group_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_group_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_group_descriptionRequest $request)
    {
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            Flash::error('Oc Customer Group Description not found');

            return redirect(route('ocCustomerGroupDescriptions.index'));
        }

        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Customer Group Description updated successfully.');

        return redirect(route('ocCustomerGroupDescriptions.index'));
    }

    /**
     * Remove the specified oc_customer_group_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerGroupDescription = $this->ocCustomerGroupDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomerGroupDescription)) {
            Flash::error('Oc Customer Group Description not found');

            return redirect(route('ocCustomerGroupDescriptions.index'));
        }

        $this->ocCustomerGroupDescriptionRepository->delete($id);

        Flash::success('Oc Customer Group Description deleted successfully.');

        return redirect(route('ocCustomerGroupDescriptions.index'));
    }
}
