<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_information_descriptionRequest;
use App\Http\Requests\Updateoc_information_descriptionRequest;
use App\Repositories\oc_information_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_information_descriptionController extends AppBaseController
{
    /** @var  oc_information_descriptionRepository */
    private $ocInformationDescriptionRepository;

    public function __construct(oc_information_descriptionRepository $ocInformationDescriptionRepo)
    {
        $this->ocInformationDescriptionRepository = $ocInformationDescriptionRepo;
    }

    /**
     * Display a listing of the oc_information_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocInformationDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocInformationDescriptions = $this->ocInformationDescriptionRepository->paginate(20);

        return view('oc_information_descriptions.index')
            ->with('ocInformationDescriptions', $ocInformationDescriptions);
    }

    /**
     * Show the form for creating a new oc_information_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_information_descriptions.create');
    }

    /**
     * Store a newly created oc_information_description in storage.
     *
     * @param Createoc_information_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_information_descriptionRequest $request)
    {
        $input = $request->all();

        $ocInformationDescription = $this->ocInformationDescriptionRepository->create($input);

        Flash::success('Oc Information Description saved successfully.');

        return redirect(route('ocInformationDescriptions.index'));
    }

    /**
     * Display the specified oc_information_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            Flash::error('Oc Information Description not found');

            return redirect(route('ocInformationDescriptions.index'));
        }

        return view('oc_information_descriptions.show')->with('ocInformationDescription', $ocInformationDescription);
    }

    /**
     * Show the form for editing the specified oc_information_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            Flash::error('Oc Information Description not found');

            return redirect(route('ocInformationDescriptions.index'));
        }

        return view('oc_information_descriptions.edit')->with('ocInformationDescription', $ocInformationDescription);
    }

    /**
     * Update the specified oc_information_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_information_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_information_descriptionRequest $request)
    {
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            Flash::error('Oc Information Description not found');

            return redirect(route('ocInformationDescriptions.index'));
        }

        $ocInformationDescription = $this->ocInformationDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Information Description updated successfully.');

        return redirect(route('ocInformationDescriptions.index'));
    }

    /**
     * Remove the specified oc_information_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocInformationDescription = $this->ocInformationDescriptionRepository->findWithoutFail($id);

        if (empty($ocInformationDescription)) {
            Flash::error('Oc Information Description not found');

            return redirect(route('ocInformationDescriptions.index'));
        }

        $this->ocInformationDescriptionRepository->delete($id);

        Flash::success('Oc Information Description deleted successfully.');

        return redirect(route('ocInformationDescriptions.index'));
    }
}
