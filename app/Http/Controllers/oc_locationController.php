<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_locationRequest;
use App\Http\Requests\Updateoc_locationRequest;
use App\Repositories\oc_locationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_locationController extends AppBaseController
{
    /** @var  oc_locationRepository */
    private $ocLocationRepository;

    public function __construct(oc_locationRepository $ocLocationRepo)
    {
        $this->ocLocationRepository = $ocLocationRepo;
    }

    /**
     * Display a listing of the oc_location.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocLocationRepository->pushCriteria(new RequestCriteria($request));
        $ocLocations = $this->ocLocationRepository->paginate(20);

        return view('oc_locations.index')
            ->with('ocLocations', $ocLocations);
    }

    /**
     * Show the form for creating a new oc_location.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_locations.create');
    }

    /**
     * Store a newly created oc_location in storage.
     *
     * @param Createoc_locationRequest $request
     *
     * @return Response
     */
    public function store(Createoc_locationRequest $request)
    {
        $input = $request->all();

        $ocLocation = $this->ocLocationRepository->create($input);

        Flash::success('Oc Location saved successfully.');

        return redirect(route('ocLocations.index'));
    }

    /**
     * Display the specified oc_location.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            Flash::error('Oc Location not found');

            return redirect(route('ocLocations.index'));
        }

        return view('oc_locations.show')->with('ocLocation', $ocLocation);
    }

    /**
     * Show the form for editing the specified oc_location.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            Flash::error('Oc Location not found');

            return redirect(route('ocLocations.index'));
        }

        return view('oc_locations.edit')->with('ocLocation', $ocLocation);
    }

    /**
     * Update the specified oc_location in storage.
     *
     * @param  int              $id
     * @param Updateoc_locationRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_locationRequest $request)
    {
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            Flash::error('Oc Location not found');

            return redirect(route('ocLocations.index'));
        }

        $ocLocation = $this->ocLocationRepository->update($request->all(), $id);

        Flash::success('Oc Location updated successfully.');

        return redirect(route('ocLocations.index'));
    }

    /**
     * Remove the specified oc_location from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocLocation = $this->ocLocationRepository->findWithoutFail($id);

        if (empty($ocLocation)) {
            Flash::error('Oc Location not found');

            return redirect(route('ocLocations.index'));
        }

        $this->ocLocationRepository->delete($id);

        Flash::success('Oc Location deleted successfully.');

        return redirect(route('ocLocations.index'));
    }
}
