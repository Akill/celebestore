<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_optionRequest;
use App\Http\Requests\Updateoc_order_optionRequest;
use App\Repositories\oc_order_optionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_optionController extends AppBaseController
{
    /** @var  oc_order_optionRepository */
    private $ocOrderOptionRepository;

    public function __construct(oc_order_optionRepository $ocOrderOptionRepo)
    {
        $this->ocOrderOptionRepository = $ocOrderOptionRepo;
    }

    /**
     * Display a listing of the oc_order_option.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderOptionRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderOptions = $this->ocOrderOptionRepository->paginate(20);

        return view('oc_order_options.index')
            ->with('ocOrderOptions', $ocOrderOptions);
    }

    /**
     * Show the form for creating a new oc_order_option.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_options.create');
    }

    /**
     * Store a newly created oc_order_option in storage.
     *
     * @param Createoc_order_optionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_optionRequest $request)
    {
        $input = $request->all();

        $ocOrderOption = $this->ocOrderOptionRepository->create($input);

        Flash::success('Oc Order Option saved successfully.');

        return redirect(route('ocOrderOptions.index'));
    }

    /**
     * Display the specified oc_order_option.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            Flash::error('Oc Order Option not found');

            return redirect(route('ocOrderOptions.index'));
        }

        return view('oc_order_options.show')->with('ocOrderOption', $ocOrderOption);
    }

    /**
     * Show the form for editing the specified oc_order_option.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            Flash::error('Oc Order Option not found');

            return redirect(route('ocOrderOptions.index'));
        }

        return view('oc_order_options.edit')->with('ocOrderOption', $ocOrderOption);
    }

    /**
     * Update the specified oc_order_option in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_optionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_optionRequest $request)
    {
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            Flash::error('Oc Order Option not found');

            return redirect(route('ocOrderOptions.index'));
        }

        $ocOrderOption = $this->ocOrderOptionRepository->update($request->all(), $id);

        Flash::success('Oc Order Option updated successfully.');

        return redirect(route('ocOrderOptions.index'));
    }

    /**
     * Remove the specified oc_order_option from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderOption = $this->ocOrderOptionRepository->findWithoutFail($id);

        if (empty($ocOrderOption)) {
            Flash::error('Oc Order Option not found');

            return redirect(route('ocOrderOptions.index'));
        }

        $this->ocOrderOptionRepository->delete($id);

        Flash::success('Oc Order Option deleted successfully.');

        return redirect(route('ocOrderOptions.index'));
    }
}
