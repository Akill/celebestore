<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_category_pathRequest;
use App\Http\Requests\Updateoc_category_pathRequest;
use App\Repositories\oc_category_pathRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_category_pathController extends AppBaseController
{
    /** @var  oc_category_pathRepository */
    private $ocCategoryPathRepository;

    public function __construct(oc_category_pathRepository $ocCategoryPathRepo)
    {
        $this->ocCategoryPathRepository = $ocCategoryPathRepo;
    }

    /**
     * Display a listing of the oc_category_path.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCategoryPathRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryPaths = $this->ocCategoryPathRepository->paginate(20);

        return view('oc_category_paths.index')
            ->with('ocCategoryPaths', $ocCategoryPaths);
    }

    /**
     * Show the form for creating a new oc_category_path.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_category_paths.create');
    }

    /**
     * Store a newly created oc_category_path in storage.
     *
     * @param Createoc_category_pathRequest $request
     *
     * @return Response
     */
    public function store(Createoc_category_pathRequest $request)
    {
        $input = $request->all();

        $ocCategoryPath = $this->ocCategoryPathRepository->create($input);

        Flash::success('Oc Category Path saved successfully.');

        return redirect(route('ocCategoryPaths.index'));
    }

    /**
     * Display the specified oc_category_path.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            Flash::error('Oc Category Path not found');

            return redirect(route('ocCategoryPaths.index'));
        }

        return view('oc_category_paths.show')->with('ocCategoryPath', $ocCategoryPath);
    }

    /**
     * Show the form for editing the specified oc_category_path.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            Flash::error('Oc Category Path not found');

            return redirect(route('ocCategoryPaths.index'));
        }

        return view('oc_category_paths.edit')->with('ocCategoryPath', $ocCategoryPath);
    }

    /**
     * Update the specified oc_category_path in storage.
     *
     * @param  int              $id
     * @param Updateoc_category_pathRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_category_pathRequest $request)
    {
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            Flash::error('Oc Category Path not found');

            return redirect(route('ocCategoryPaths.index'));
        }

        $ocCategoryPath = $this->ocCategoryPathRepository->update($request->all(), $id);

        Flash::success('Oc Category Path updated successfully.');

        return redirect(route('ocCategoryPaths.index'));
    }

    /**
     * Remove the specified oc_category_path from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCategoryPath = $this->ocCategoryPathRepository->findWithoutFail($id);

        if (empty($ocCategoryPath)) {
            Flash::error('Oc Category Path not found');

            return redirect(route('ocCategoryPaths.index'));
        }

        $this->ocCategoryPathRepository->delete($id);

        Flash::success('Oc Category Path deleted successfully.');

        return redirect(route('ocCategoryPaths.index'));
    }
}
