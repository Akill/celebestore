<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_order_custom_fieldRequest;
use App\Http\Requests\Updateoc_order_custom_fieldRequest;
use App\Repositories\oc_order_custom_fieldRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_order_custom_fieldController extends AppBaseController
{
    /** @var  oc_order_custom_fieldRepository */
    private $ocOrderCustomFieldRepository;

    public function __construct(oc_order_custom_fieldRepository $ocOrderCustomFieldRepo)
    {
        $this->ocOrderCustomFieldRepository = $ocOrderCustomFieldRepo;
    }

    /**
     * Display a listing of the oc_order_custom_field.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderCustomFieldRepository->pushCriteria(new RequestCriteria($request));
        $ocOrderCustomFields = $this->ocOrderCustomFieldRepository->paginate(20);

        return view('oc_order_custom_fields.index')
            ->with('ocOrderCustomFields', $ocOrderCustomFields);
    }

    /**
     * Show the form for creating a new oc_order_custom_field.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_order_custom_fields.create');
    }

    /**
     * Store a newly created oc_order_custom_field in storage.
     *
     * @param Createoc_order_custom_fieldRequest $request
     *
     * @return Response
     */
    public function store(Createoc_order_custom_fieldRequest $request)
    {
        $input = $request->all();

        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->create($input);

        Flash::success('Oc Order Custom Field saved successfully.');

        return redirect(route('ocOrderCustomFields.index'));
    }

    /**
     * Display the specified oc_order_custom_field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            Flash::error('Oc Order Custom Field not found');

            return redirect(route('ocOrderCustomFields.index'));
        }

        return view('oc_order_custom_fields.show')->with('ocOrderCustomField', $ocOrderCustomField);
    }

    /**
     * Show the form for editing the specified oc_order_custom_field.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            Flash::error('Oc Order Custom Field not found');

            return redirect(route('ocOrderCustomFields.index'));
        }

        return view('oc_order_custom_fields.edit')->with('ocOrderCustomField', $ocOrderCustomField);
    }

    /**
     * Update the specified oc_order_custom_field in storage.
     *
     * @param  int              $id
     * @param Updateoc_order_custom_fieldRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_order_custom_fieldRequest $request)
    {
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            Flash::error('Oc Order Custom Field not found');

            return redirect(route('ocOrderCustomFields.index'));
        }

        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->update($request->all(), $id);

        Flash::success('Oc Order Custom Field updated successfully.');

        return redirect(route('ocOrderCustomFields.index'));
    }

    /**
     * Remove the specified oc_order_custom_field from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrderCustomField = $this->ocOrderCustomFieldRepository->findWithoutFail($id);

        if (empty($ocOrderCustomField)) {
            Flash::error('Oc Order Custom Field not found');

            return redirect(route('ocOrderCustomFields.index'));
        }

        $this->ocOrderCustomFieldRepository->delete($id);

        Flash::success('Oc Order Custom Field deleted successfully.');

        return redirect(route('ocOrderCustomFields.index'));
    }
}
