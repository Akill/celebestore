<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_product_attributeRequest;
use App\Http\Requests\Updateoc_product_attributeRequest;
use App\Repositories\oc_product_attributeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_product_attributeController extends AppBaseController
{
    /** @var  oc_product_attributeRepository */
    private $ocProductAttributeRepository;

    public function __construct(oc_product_attributeRepository $ocProductAttributeRepo)
    {
        $this->ocProductAttributeRepository = $ocProductAttributeRepo;
    }

    /**
     * Display a listing of the oc_product_attribute.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocProductAttributeRepository->pushCriteria(new RequestCriteria($request));
        $ocProductAttributes = $this->ocProductAttributeRepository->all();

        return view('oc_product_attributes.index')
            ->with('ocProductAttributes', $ocProductAttributes);
    }

    /**
     * Show the form for creating a new oc_product_attribute.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_product_attributes.create');
    }

    /**
     * Store a newly created oc_product_attribute in storage.
     *
     * @param Createoc_product_attributeRequest $request
     *
     * @return Response
     */
    public function store(Createoc_product_attributeRequest $request)
    {
        $input = $request->all();

        $ocProductAttribute = $this->ocProductAttributeRepository->create($input);

        Flash::success('Oc Product Attribute saved successfully.');

        return redirect(route('ocProductAttributes.index'));
    }

    /**
     * Display the specified oc_product_attribute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            Flash::error('Oc Product Attribute not found');

            return redirect(route('ocProductAttributes.index'));
        }

        return view('oc_product_attributes.show')->with('ocProductAttribute', $ocProductAttribute);
    }

    /**
     * Show the form for editing the specified oc_product_attribute.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            Flash::error('Oc Product Attribute not found');

            return redirect(route('ocProductAttributes.index'));
        }

        return view('oc_product_attributes.edit')->with('ocProductAttribute', $ocProductAttribute);
    }

    /**
     * Update the specified oc_product_attribute in storage.
     *
     * @param  int              $id
     * @param Updateoc_product_attributeRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_product_attributeRequest $request)
    {
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            Flash::error('Oc Product Attribute not found');

            return redirect(route('ocProductAttributes.index'));
        }

        $ocProductAttribute = $this->ocProductAttributeRepository->update($request->all(), $id);

        Flash::success('Oc Product Attribute updated successfully.');

        return redirect(route('ocProductAttributes.index'));
    }

    /**
     * Remove the specified oc_product_attribute from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocProductAttribute = $this->ocProductAttributeRepository->findWithoutFail($id);

        if (empty($ocProductAttribute)) {
            Flash::error('Oc Product Attribute not found');

            return redirect(route('ocProductAttributes.index'));
        }

        $this->ocProductAttributeRepository->delete($id);

        Flash::success('Oc Product Attribute deleted successfully.');

        return redirect(route('ocProductAttributes.index'));
    }
}
