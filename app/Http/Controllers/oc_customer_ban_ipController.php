<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_customer_ban_ipRequest;
use App\Http\Requests\Updateoc_customer_ban_ipRequest;
use App\Repositories\oc_customer_ban_ipRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_customer_ban_ipController extends AppBaseController
{
    /** @var  oc_customer_ban_ipRepository */
    private $ocCustomerBanIpRepository;

    public function __construct(oc_customer_ban_ipRepository $ocCustomerBanIpRepo)
    {
        $this->ocCustomerBanIpRepository = $ocCustomerBanIpRepo;
    }

    /**
     * Display a listing of the oc_customer_ban_ip.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomerBanIpRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomerBanIps = $this->ocCustomerBanIpRepository->paginate(20);

        return view('oc_customer_ban_ips.index')
            ->with('ocCustomerBanIps', $ocCustomerBanIps);
    }

    /**
     * Show the form for creating a new oc_customer_ban_ip.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_customer_ban_ips.create');
    }

    /**
     * Store a newly created oc_customer_ban_ip in storage.
     *
     * @param Createoc_customer_ban_ipRequest $request
     *
     * @return Response
     */
    public function store(Createoc_customer_ban_ipRequest $request)
    {
        $input = $request->all();

        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->create($input);

        Flash::success('Oc Customer Ban Ip saved successfully.');

        return redirect(route('ocCustomerBanIps.index'));
    }

    /**
     * Display the specified oc_customer_ban_ip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            Flash::error('Oc Customer Ban Ip not found');

            return redirect(route('ocCustomerBanIps.index'));
        }

        return view('oc_customer_ban_ips.show')->with('ocCustomerBanIp', $ocCustomerBanIp);
    }

    /**
     * Show the form for editing the specified oc_customer_ban_ip.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            Flash::error('Oc Customer Ban Ip not found');

            return redirect(route('ocCustomerBanIps.index'));
        }

        return view('oc_customer_ban_ips.edit')->with('ocCustomerBanIp', $ocCustomerBanIp);
    }

    /**
     * Update the specified oc_customer_ban_ip in storage.
     *
     * @param  int              $id
     * @param Updateoc_customer_ban_ipRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_customer_ban_ipRequest $request)
    {
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            Flash::error('Oc Customer Ban Ip not found');

            return redirect(route('ocCustomerBanIps.index'));
        }

        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->update($request->all(), $id);

        Flash::success('Oc Customer Ban Ip updated successfully.');

        return redirect(route('ocCustomerBanIps.index'));
    }

    /**
     * Remove the specified oc_customer_ban_ip from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomerBanIp = $this->ocCustomerBanIpRepository->findWithoutFail($id);

        if (empty($ocCustomerBanIp)) {
            Flash::error('Oc Customer Ban Ip not found');

            return redirect(route('ocCustomerBanIps.index'));
        }

        $this->ocCustomerBanIpRepository->delete($id);

        Flash::success('Oc Customer Ban Ip deleted successfully.');

        return redirect(route('ocCustomerBanIps.index'));
    }
}
