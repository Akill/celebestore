<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_coupon_historyRequest;
use App\Http\Requests\Updateoc_coupon_historyRequest;
use App\Repositories\oc_coupon_historyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_coupon_historyController extends AppBaseController
{
    /** @var  oc_coupon_historyRepository */
    private $ocCouponHistoryRepository;

    public function __construct(oc_coupon_historyRepository $ocCouponHistoryRepo)
    {
        $this->ocCouponHistoryRepository = $ocCouponHistoryRepo;
    }

    /**
     * Display a listing of the oc_coupon_history.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCouponHistoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCouponHistories = $this->ocCouponHistoryRepository->paginate(20);

        return view('oc_coupon_histories.index')
            ->with('ocCouponHistories', $ocCouponHistories);
    }

    /**
     * Show the form for creating a new oc_coupon_history.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_coupon_histories.create');
    }

    /**
     * Store a newly created oc_coupon_history in storage.
     *
     * @param Createoc_coupon_historyRequest $request
     *
     * @return Response
     */
    public function store(Createoc_coupon_historyRequest $request)
    {
        $input = $request->all();

        $ocCouponHistory = $this->ocCouponHistoryRepository->create($input);

        Flash::success('Oc Coupon History saved successfully.');

        return redirect(route('ocCouponHistories.index'));
    }

    /**
     * Display the specified oc_coupon_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            Flash::error('Oc Coupon History not found');

            return redirect(route('ocCouponHistories.index'));
        }

        return view('oc_coupon_histories.show')->with('ocCouponHistory', $ocCouponHistory);
    }

    /**
     * Show the form for editing the specified oc_coupon_history.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            Flash::error('Oc Coupon History not found');

            return redirect(route('ocCouponHistories.index'));
        }

        return view('oc_coupon_histories.edit')->with('ocCouponHistory', $ocCouponHistory);
    }

    /**
     * Update the specified oc_coupon_history in storage.
     *
     * @param  int              $id
     * @param Updateoc_coupon_historyRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_coupon_historyRequest $request)
    {
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            Flash::error('Oc Coupon History not found');

            return redirect(route('ocCouponHistories.index'));
        }

        $ocCouponHistory = $this->ocCouponHistoryRepository->update($request->all(), $id);

        Flash::success('Oc Coupon History updated successfully.');

        return redirect(route('ocCouponHistories.index'));
    }

    /**
     * Remove the specified oc_coupon_history from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCouponHistory = $this->ocCouponHistoryRepository->findWithoutFail($id);

        if (empty($ocCouponHistory)) {
            Flash::error('Oc Coupon History not found');

            return redirect(route('ocCouponHistories.index'));
        }

        $this->ocCouponHistoryRepository->delete($id);

        Flash::success('Oc Coupon History deleted successfully.');

        return redirect(route('ocCouponHistories.index'));
    }
}
