<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_custom_field_descriptionRequest;
use App\Http\Requests\Updateoc_custom_field_descriptionRequest;
use App\Repositories\oc_custom_field_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_custom_field_descriptionController extends AppBaseController
{
    /** @var  oc_custom_field_descriptionRepository */
    private $ocCustomFieldDescriptionRepository;

    public function __construct(oc_custom_field_descriptionRepository $ocCustomFieldDescriptionRepo)
    {
        $this->ocCustomFieldDescriptionRepository = $ocCustomFieldDescriptionRepo;
    }

    /**
     * Display a listing of the oc_custom_field_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomFieldDescriptions = $this->ocCustomFieldDescriptionRepository->paginate(20);

        return view('oc_custom_field_descriptions.index')
            ->with('ocCustomFieldDescriptions', $ocCustomFieldDescriptions);
    }

    /**
     * Show the form for creating a new oc_custom_field_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_custom_field_descriptions.create');
    }

    /**
     * Store a newly created oc_custom_field_description in storage.
     *
     * @param Createoc_custom_field_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_descriptionRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->create($input);

        Flash::success('Oc Custom Field Description saved successfully.');

        return redirect(route('ocCustomFieldDescriptions.index'));
    }

    /**
     * Display the specified oc_custom_field_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            Flash::error('Oc Custom Field Description not found');

            return redirect(route('ocCustomFieldDescriptions.index'));
        }

        return view('oc_custom_field_descriptions.show')->with('ocCustomFieldDescription', $ocCustomFieldDescription);
    }

    /**
     * Show the form for editing the specified oc_custom_field_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            Flash::error('Oc Custom Field Description not found');

            return redirect(route('ocCustomFieldDescriptions.index'));
        }

        return view('oc_custom_field_descriptions.edit')->with('ocCustomFieldDescription', $ocCustomFieldDescription);
    }

    /**
     * Update the specified oc_custom_field_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_custom_field_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_descriptionRequest $request)
    {
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            Flash::error('Oc Custom Field Description not found');

            return redirect(route('ocCustomFieldDescriptions.index'));
        }

        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Custom Field Description updated successfully.');

        return redirect(route('ocCustomFieldDescriptions.index'));
    }

    /**
     * Remove the specified oc_custom_field_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomFieldDescription = $this->ocCustomFieldDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldDescription)) {
            Flash::error('Oc Custom Field Description not found');

            return redirect(route('ocCustomFieldDescriptions.index'));
        }

        $this->ocCustomFieldDescriptionRepository->delete($id);

        Flash::success('Oc Custom Field Description deleted successfully.');

        return redirect(route('ocCustomFieldDescriptions.index'));
    }
}
