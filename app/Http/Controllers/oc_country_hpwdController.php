<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_country_hpwdRequest;
use App\Http\Requests\Updateoc_country_hpwdRequest;
use App\Repositories\oc_country_hpwdRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_country_hpwdController extends AppBaseController
{
    /** @var  oc_country_hpwdRepository */
    private $ocCountryHpwdRepository;

    public function __construct(oc_country_hpwdRepository $ocCountryHpwdRepo)
    {
        $this->ocCountryHpwdRepository = $ocCountryHpwdRepo;
    }

    /**
     * Display a listing of the oc_country_hpwd.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCountryHpwdRepository->pushCriteria(new RequestCriteria($request));
        $ocCountryHpwds = $this->ocCountryHpwdRepository->paginate(20);

        return view('oc_country_hpwds.index')
            ->with('ocCountryHpwds', $ocCountryHpwds);
    }

    /**
     * Show the form for creating a new oc_country_hpwd.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_country_hpwds.create');
    }

    /**
     * Store a newly created oc_country_hpwd in storage.
     *
     * @param Createoc_country_hpwdRequest $request
     *
     * @return Response
     */
    public function store(Createoc_country_hpwdRequest $request)
    {
        $input = $request->all();

        $ocCountryHpwd = $this->ocCountryHpwdRepository->create($input);

        Flash::success('Oc Country Hpwd saved successfully.');

        return redirect(route('ocCountryHpwds.index'));
    }

    /**
     * Display the specified oc_country_hpwd.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            Flash::error('Oc Country Hpwd not found');

            return redirect(route('ocCountryHpwds.index'));
        }

        return view('oc_country_hpwds.show')->with('ocCountryHpwd', $ocCountryHpwd);
    }

    /**
     * Show the form for editing the specified oc_country_hpwd.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            Flash::error('Oc Country Hpwd not found');

            return redirect(route('ocCountryHpwds.index'));
        }

        return view('oc_country_hpwds.edit')->with('ocCountryHpwd', $ocCountryHpwd);
    }

    /**
     * Update the specified oc_country_hpwd in storage.
     *
     * @param  int              $id
     * @param Updateoc_country_hpwdRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_country_hpwdRequest $request)
    {
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            Flash::error('Oc Country Hpwd not found');

            return redirect(route('ocCountryHpwds.index'));
        }

        $ocCountryHpwd = $this->ocCountryHpwdRepository->update($request->all(), $id);

        Flash::success('Oc Country Hpwd updated successfully.');

        return redirect(route('ocCountryHpwds.index'));
    }

    /**
     * Remove the specified oc_country_hpwd from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCountryHpwd = $this->ocCountryHpwdRepository->findWithoutFail($id);

        if (empty($ocCountryHpwd)) {
            Flash::error('Oc Country Hpwd not found');

            return redirect(route('ocCountryHpwds.index'));
        }

        $this->ocCountryHpwdRepository->delete($id);

        Flash::success('Oc Country Hpwd deleted successfully.');

        return redirect(route('ocCountryHpwds.index'));
    }
}
