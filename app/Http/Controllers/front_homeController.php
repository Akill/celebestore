<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Http\Requests\Createoc_category_descriptionRequest;
use App\Http\Requests\Updateoc_category_descriptionRequest;
use App\Repositories\oc_category_descriptionRepository;

use App\Http\Requests\Createoc_categoryRequest;
use App\Http\Requests\Updateoc_categoryRequest;
use App\Repositories\oc_categoryRepository;

use App\Http\Requests\Createoc_product_to_categoryRequest;
use App\Http\Requests\Updateoc_product_to_categoryRequest;
use App\Repositories\oc_product_to_categoryRepository;

class front_homeController extends Controller
{
    /** @var  oc_category_descriptionRepository */
    private $ocCategoryDescriptionRepository;

    /** @var  oc_categoryRepository */
    private $ocCategoryRepository;

    /** @var  oc_product_to_categoryRepository */
    private $ocProductToCategoryRepository;

    public function __construct(oc_category_descriptionRepository $ocCategoryDescriptionRepo, oc_categoryRepository $ocCategoryRepo, oc_product_to_categoryRepository $ocProductToCategoryRepo)
    {
        $this->ocCategoryRepository = $ocCategoryRepo;
        $this->ocCategoryDescriptionRepository = $ocCategoryDescriptionRepo;
        $this->ocProductToCategoryRepository = $ocProductToCategoryRepo;
    }

    /**
     * Display a listing of the front_home.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
    	$this->ocCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ocCategories = $this->ocCategoryRepository->paginate(20);
        $ocCategories4 = $this->ocCategoryRepository->paginate(20);

        $this->ocCategoryDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocCategoryDescriptions = $this->ocCategoryDescriptionRepository->paginate(20);

        return view('contents.home.index',['ocCategories' => $ocCategories,
        	'ocCategories4' => $ocCategories4,
        	'ocCategoryDescriptions' => $ocCategoryDescriptions]);
    }
}
