<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_custom_field_value_descriptionRequest;
use App\Http\Requests\Updateoc_custom_field_value_descriptionRequest;
use App\Repositories\oc_custom_field_value_descriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_custom_field_value_descriptionController extends AppBaseController
{
    /** @var  oc_custom_field_value_descriptionRepository */
    private $ocCustomFieldValueDescriptionRepository;

    public function __construct(oc_custom_field_value_descriptionRepository $ocCustomFieldValueDescriptionRepo)
    {
        $this->ocCustomFieldValueDescriptionRepository = $ocCustomFieldValueDescriptionRepo;
    }

    /**
     * Display a listing of the oc_custom_field_value_description.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocCustomFieldValueDescriptionRepository->pushCriteria(new RequestCriteria($request));
        $ocCustomFieldValueDescriptions = $this->ocCustomFieldValueDescriptionRepository->paginate(20);

        return view('oc_custom_field_value_descriptions.index')
            ->with('ocCustomFieldValueDescriptions', $ocCustomFieldValueDescriptions);
    }

    /**
     * Show the form for creating a new oc_custom_field_value_description.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_custom_field_value_descriptions.create');
    }

    /**
     * Store a newly created oc_custom_field_value_description in storage.
     *
     * @param Createoc_custom_field_value_descriptionRequest $request
     *
     * @return Response
     */
    public function store(Createoc_custom_field_value_descriptionRequest $request)
    {
        $input = $request->all();

        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->create($input);

        Flash::success('Oc Custom Field Value Description saved successfully.');

        return redirect(route('ocCustomFieldValueDescriptions.index'));
    }

    /**
     * Display the specified oc_custom_field_value_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            Flash::error('Oc Custom Field Value Description not found');

            return redirect(route('ocCustomFieldValueDescriptions.index'));
        }

        return view('oc_custom_field_value_descriptions.show')->with('ocCustomFieldValueDescription', $ocCustomFieldValueDescription);
    }

    /**
     * Show the form for editing the specified oc_custom_field_value_description.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            Flash::error('Oc Custom Field Value Description not found');

            return redirect(route('ocCustomFieldValueDescriptions.index'));
        }

        return view('oc_custom_field_value_descriptions.edit')->with('ocCustomFieldValueDescription', $ocCustomFieldValueDescription);
    }

    /**
     * Update the specified oc_custom_field_value_description in storage.
     *
     * @param  int              $id
     * @param Updateoc_custom_field_value_descriptionRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_custom_field_value_descriptionRequest $request)
    {
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            Flash::error('Oc Custom Field Value Description not found');

            return redirect(route('ocCustomFieldValueDescriptions.index'));
        }

        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->update($request->all(), $id);

        Flash::success('Oc Custom Field Value Description updated successfully.');

        return redirect(route('ocCustomFieldValueDescriptions.index'));
    }

    /**
     * Remove the specified oc_custom_field_value_description from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocCustomFieldValueDescription = $this->ocCustomFieldValueDescriptionRepository->findWithoutFail($id);

        if (empty($ocCustomFieldValueDescription)) {
            Flash::error('Oc Custom Field Value Description not found');

            return redirect(route('ocCustomFieldValueDescriptions.index'));
        }

        $this->ocCustomFieldValueDescriptionRepository->delete($id);

        Flash::success('Oc Custom Field Value Description deleted successfully.');

        return redirect(route('ocCustomFieldValueDescriptions.index'));
    }
}
