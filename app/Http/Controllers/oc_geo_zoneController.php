<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_geo_zoneRequest;
use App\Http\Requests\Updateoc_geo_zoneRequest;
use App\Repositories\oc_geo_zoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_geo_zoneController extends AppBaseController
{
    /** @var  oc_geo_zoneRepository */
    private $ocGeoZoneRepository;

    public function __construct(oc_geo_zoneRepository $ocGeoZoneRepo)
    {
        $this->ocGeoZoneRepository = $ocGeoZoneRepo;
    }

    /**
     * Display a listing of the oc_geo_zone.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocGeoZoneRepository->pushCriteria(new RequestCriteria($request));
        $ocGeoZones = $this->ocGeoZoneRepository->paginate(20);

        return view('oc_geo_zones.index')
            ->with('ocGeoZones', $ocGeoZones);
    }

    /**
     * Show the form for creating a new oc_geo_zone.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_geo_zones.create');
    }

    /**
     * Store a newly created oc_geo_zone in storage.
     *
     * @param Createoc_geo_zoneRequest $request
     *
     * @return Response
     */
    public function store(Createoc_geo_zoneRequest $request)
    {
        $input = $request->all();

        $ocGeoZone = $this->ocGeoZoneRepository->create($input);

        Flash::success('Oc Geo Zone saved successfully.');

        return redirect(route('ocGeoZones.index'));
    }

    /**
     * Display the specified oc_geo_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            Flash::error('Oc Geo Zone not found');

            return redirect(route('ocGeoZones.index'));
        }

        return view('oc_geo_zones.show')->with('ocGeoZone', $ocGeoZone);
    }

    /**
     * Show the form for editing the specified oc_geo_zone.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            Flash::error('Oc Geo Zone not found');

            return redirect(route('ocGeoZones.index'));
        }

        return view('oc_geo_zones.edit')->with('ocGeoZone', $ocGeoZone);
    }

    /**
     * Update the specified oc_geo_zone in storage.
     *
     * @param  int              $id
     * @param Updateoc_geo_zoneRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_geo_zoneRequest $request)
    {
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            Flash::error('Oc Geo Zone not found');

            return redirect(route('ocGeoZones.index'));
        }

        $ocGeoZone = $this->ocGeoZoneRepository->update($request->all(), $id);

        Flash::success('Oc Geo Zone updated successfully.');

        return redirect(route('ocGeoZones.index'));
    }

    /**
     * Remove the specified oc_geo_zone from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocGeoZone = $this->ocGeoZoneRepository->findWithoutFail($id);

        if (empty($ocGeoZone)) {
            Flash::error('Oc Geo Zone not found');

            return redirect(route('ocGeoZones.index'));
        }

        $this->ocGeoZoneRepository->delete($id);

        Flash::success('Oc Geo Zone deleted successfully.');

        return redirect(route('ocGeoZones.index'));
    }
}
