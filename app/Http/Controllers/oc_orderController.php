<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createoc_orderRequest;
use App\Http\Requests\Updateoc_orderRequest;
use App\Repositories\oc_orderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class oc_orderController extends AppBaseController
{
    /** @var  oc_orderRepository */
    private $ocOrderRepository;

    public function __construct(oc_orderRepository $ocOrderRepo)
    {
        $this->ocOrderRepository = $ocOrderRepo;
    }

    /**
     * Display a listing of the oc_order.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ocOrderRepository->pushCriteria(new RequestCriteria($request));
        $ocOrders = $this->ocOrderRepository->paginate(20);

        return view('oc_orders.index')
            ->with('ocOrders', $ocOrders);
    }

    /**
     * Show the form for creating a new oc_order.
     *
     * @return Response
     */
    public function create()
    {
        return view('oc_orders.create');
    }

    /**
     * Store a newly created oc_order in storage.
     *
     * @param Createoc_orderRequest $request
     *
     * @return Response
     */
    public function store(Createoc_orderRequest $request)
    {
        $input = $request->all();

        $ocOrder = $this->ocOrderRepository->create($input);

        Flash::success('Oc Order saved successfully.');

        return redirect(route('ocOrders.index'));
    }

    /**
     * Display the specified oc_order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            Flash::error('Oc Order not found');

            return redirect(route('ocOrders.index'));
        }

        return view('oc_orders.show')->with('ocOrder', $ocOrder);
    }

    /**
     * Show the form for editing the specified oc_order.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            Flash::error('Oc Order not found');

            return redirect(route('ocOrders.index'));
        }

        return view('oc_orders.edit')->with('ocOrder', $ocOrder);
    }

    /**
     * Update the specified oc_order in storage.
     *
     * @param  int              $id
     * @param Updateoc_orderRequest $request
     *
     * @return Response
     */
    public function update($id, Updateoc_orderRequest $request)
    {
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            Flash::error('Oc Order not found');

            return redirect(route('ocOrders.index'));
        }

        $ocOrder = $this->ocOrderRepository->update($request->all(), $id);

        Flash::success('Oc Order updated successfully.');

        return redirect(route('ocOrders.index'));
    }

    /**
     * Remove the specified oc_order from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ocOrder = $this->ocOrderRepository->findWithoutFail($id);

        if (empty($ocOrder)) {
            Flash::error('Oc Order not found');

            return redirect(route('ocOrders.index'));
        }

        $this->ocOrderRepository->delete($id);

        Flash::success('Oc Order deleted successfully.');

        return redirect(route('ocOrders.index'));
    }
}
